package hr.fer.zemris.java.hw14.util;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * PropertyFetch is a utility class that reads a property file
 * and stores its contents, enabling easy access.
 * 
 * @author Miroslav Bićanić
 */
public class PropertyFetch {
	/**
	 * A map pairing the property keys to property values
	 */
	private Map<String, String> entries;
	
	/**
	 * Constructor for a PropertyFetch
	 * @param p the path to the properties file
	 */
	public PropertyFetch(Path p) {
		entries = new HashMap<>();
		try {
			parse(Files.readAllLines(p, StandardCharsets.UTF_8));
		} catch (IOException e) {
			throw new RuntimeException(e.getMessage());
		}
	}

	/**
	 * Parses each line transforming it into a key-value pair
	 * and adds it to the internal map.
	 * 
	 * @param lines the property lines to parse
	 */
	private void parse(List<String> lines) {
		for(String l : lines) {
			if(l.isEmpty() || l.startsWith("#")) {
				continue;
			}
			String[] entry = l.split("\\s*=\\s*");
			this.entries.put(entry[0], entry[1]);
		}
	}
	
	/**
	 * Returns the value associated with the given {@code prop}
	 * key.
	 * 
	 * @param prop the key whose value is requested
	 * @return the value associated with the key
	 */
	public String fetch(String prop) {
		return entries.get(prop);
	}
	
	/**
	 * Returns an unmodifiable map of all properties stored in
	 * this PropertyFetch.
	 * 
	 * @return a map of all properties
	 */
	public Map<String,String> allEntries(){
		return Collections.unmodifiableMap(entries);
	}
}
