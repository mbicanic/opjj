package hr.fer.zemris.java.hw14.servlets;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import hr.fer.zemris.java.hw14.dao.DAOProvider;
import hr.fer.zemris.java.hw14.models.Poll;

/**
 * IndexServlet is a specification of a {@link HttpServlet} which
 * gets all available polls from the database and dispatches them
 * to a JSP displaying them as a list.
 * 
 * @author Miroslav Bićanić
 */
public class IndexServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		List<Poll> polls = DAOProvider.getDao().getAllPolls();
		req.setAttribute("polls", polls);
		req.getRequestDispatcher("/WEB-INF/index.jsp").forward(req, resp);
	}
}
