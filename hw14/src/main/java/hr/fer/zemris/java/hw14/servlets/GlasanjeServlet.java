package hr.fer.zemris.java.hw14.servlets;

import java.io.IOException;
import java.util.Collections;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import hr.fer.zemris.java.hw14.dao.DAOProvider;
import hr.fer.zemris.java.hw14.models.Option;
import hr.fer.zemris.java.hw14.models.Poll;

/**
 * GlasanjeServlet is a specification of a {@link HttpServlet} which
 * loads the options from the database for the poll whose ID is given 
 * to this servlet as a paremeter and dispatches the processing to a JSP.
 * 
 * @author Miroslav Bićanić
 */
public class GlasanjeServlet extends AbstractGlasanje {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		//get and check the validity of the given parameter
		long id = parseOrSendError(req.getParameter("pollID"), req, resp);
		if(id==-1 || !recordExists("Polls", id, req, resp)) {
			return;
		}
		
		//load and sort the options by id
		List<Option> options = DAOProvider.getDao().getOptionsForPoll(id);
		Collections.sort(options, (o1, o2)->(int)(o1.getId()-o2.getId()));
		List<Poll> polls = DAOProvider.getDao().getAllPolls();
		
		//find the currently used poll
		Poll currentPoll = null;
		for(Poll p : polls) {
			if(p.getId()==id) {
				currentPoll=p;
			}
		}
		
		//prepare and dispatch the request
		req.setAttribute("options", options);
		req.setAttribute("currentPoll", currentPoll);
		req.getRequestDispatcher("/WEB-INF/pages/glasanjeIndex.jsp").forward(req, resp);
	}
}
