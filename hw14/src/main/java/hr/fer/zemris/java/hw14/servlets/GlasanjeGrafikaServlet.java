package hr.fer.zemris.java.hw14.servlets;

import java.io.IOException;
import java.util.Collections;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartUtils;
import org.jfree.chart.JFreeChart;
import org.jfree.data.general.DefaultPieDataset;

import hr.fer.zemris.java.hw14.dao.DAOProvider;
import hr.fer.zemris.java.hw14.models.Option;

public class GlasanjeGrafikaServlet extends AbstractGlasanje {
	private static final long serialVersionUID = 1L;
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		long pollid = parseOrSendError(req.getParameter("pollID"), req, resp);
		if(pollid==-1 || !recordExists("Polls", pollid, req, resp)) {
			return;
		}
		List<Option> results = DAOProvider.getDao().getOptionsForPoll(pollid);
		Collections.sort(results, (o1, o2)->o2.getVotes()-o1.getVotes());
		
		DefaultPieDataset dpd = new DefaultPieDataset();
		for(Option o : results) {
			dpd.setValue(o.getTitle(), o.getVotes());
		}
		JFreeChart jfc = ChartFactory.createPieChart("Rezultati glasanja", dpd);
		resp.setContentType("image/png");
		ChartUtils.writeChartAsPNG(resp.getOutputStream(), jfc, 400, 300);
	}
}
