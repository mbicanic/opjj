package hr.fer.zemris.java.hw14.models;

/**
 * Simple object modelling a database entry for a poll in the
 * voting app, providing with getters for attributes.
 * 
 * @author Miroslav Bićanić
 */
public class Poll {
	/** The title of the poll */
	private String title;
	/** The message of the poll */
	private String message;
	/** The id of the poll */
	private long id;
	
	/**
	 * A constructor for a poll
	 * @param title the title of the poll
	 * @param message the message the poll should display about itself
	 * @param id the id of the poll
	 */
	public Poll(String title, String message, long id) {
		this.title = title;
		this.message = message;
		this.id = id;
	}
	
	public String getTitle() {
		return title;
	}
	public String getMessage() {
		return message;
	}
	public long getId() {
		return id;
	}
}
