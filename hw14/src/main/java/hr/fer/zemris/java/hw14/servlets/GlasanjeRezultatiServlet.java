package hr.fer.zemris.java.hw14.servlets;

import java.io.IOException;
import java.util.Collections;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import hr.fer.zemris.java.hw14.dao.DAOProvider;
import hr.fer.zemris.java.hw14.models.Option;

/**
 * GlasanjeRezultatiServlet is a specification of a {@link HttpServlet}
 * which reads the voting results from a file and stores two maps
 * representing the results as the attributes of the {@link HttpServletRequest}.
 * 
 * @author Miroslav Bićanić
 */
public class GlasanjeRezultatiServlet extends AbstractGlasanje {
	private static final long serialVersionUID = 1L;
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		//get and check the validity of the given parameters
		long pollid = parseOrSendError(req.getParameter("pollID"), req, resp);
		if(pollid==-1 || !recordExists("Polls", pollid, req, resp)) {
			return;
		}
		
		//load and sort results and best-ranking options
		List<Option> results = DAOProvider.getDao().getOptionsForPoll(pollid);
		Collections.sort(results, (o1, o2)->o2.getVotes()-o1.getVotes());
		List<Option> winners = DAOProvider.getDao().getBestRankingOptions(pollid);
		Collections.sort(winners, (o1, o2)->(int)(o2.getId()-o1.getId()));
		
		//prepare and dispatch processing to a JSP
		req.setAttribute("results", results);
		req.setAttribute("best", winners);
		req.setAttribute("pollID", pollid);
		req.getRequestDispatcher("/WEB-INF/pages/glasanjeRez.jsp").forward(req, resp);
	}
}
