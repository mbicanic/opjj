package hr.fer.zemris.java.hw14.dao;

import java.util.List;

import hr.fer.zemris.java.hw14.models.Option;
import hr.fer.zemris.java.hw14.models.Poll;

/**
 * Interface towards the subsystem for data persistence.
 * 
 * @author marcupic
 */
public interface DAO {
	/**
	 * Returns a list of {@link Option} objects representing all
	 * options registered to the poll with the given {@code pollid}.
	 * 
	 * @param pollid the id of the poll whose options to fetch
	 * @return a list of {@link Option} objects associated with the poll
	 */
	List<Option> getOptionsForPoll(long pollid);
	
	/**
	 * Returns a list of all {@link Poll} objects representing 
	 * different polls.
	 * 
	 * @return a list of {@link Poll} objects for all existing polls
	 */
	List<Poll> getAllPolls();
	
	/**
	 * Adds a vote to the option with the given {@code optionID}
	 * @param optionID the id of the option to which to add a vote
	 */
	void addVoteTo(long optionID);
	
	/**
	 * Returns a list of all {@link Option} objects from the poll
	 * with the given {@code pollid} that have the most votes in
	 * that poll.
	 * 
	 * @param pollid the id of the poll whose best ranking options to fetch
	 * @return a list of best ranking {@link Option} objects in the poll
	 */
	List<Option> getBestRankingOptions(long pollid);

	/**
	 * Returns true if a record with the given ID exists in the
	 * table {@code tableName}.
	 * 
	 * @param id the id of the record to look for
	 * @return true if the record exists; false otherwise
	 */
	boolean recordExists(String tableName, long id);
}