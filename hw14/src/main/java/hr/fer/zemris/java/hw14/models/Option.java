package hr.fer.zemris.java.hw14.models;

/**
 * Simple object modelling a database entry for an option to vote 
 * for in the voting app, providing with getters for attributes.
 * 
 * @author Miroslav Bićanić
 */
public class Option {
	/** The title of the option - its name */
	private String title;
	/** The link of the option - pointing to something related to the option */
	private String link;
	/** The number of votes this option received */
	private int votes;
	/** The id of the option */
	private long id;
	
	/**
	 * Constructor for an Option object
	 * 
	 * @param title the title of the option
	 * @param link the link of the option pointing to a related resource on the web
	 * @param votes the amount of votes the option has at construction
	 * @param id the id of the option
	 */
	public Option(String title, String link, int votes, long id) {
		super();
		this.title = title;
		this.link = link;
		this.votes = votes;
		this.id = id;
	}
	
	public String getTitle() {
		return title;
	}
	public String getLink() {
		return link;
	}
	public int getVotes() {
		return votes;
	}
	public long getId() {
		return id;
	}
}
