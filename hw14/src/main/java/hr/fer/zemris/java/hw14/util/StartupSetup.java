package hr.fer.zemris.java.hw14.util;

import java.beans.PropertyVetoException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import javax.sql.DataSource;

import com.mchange.v2.c3p0.ComboPooledDataSource;
import com.mchange.v2.c3p0.DataSources;

/**
 * This class models a {@link WebListener} which creates the 
 * voting database if it does not exist. If it exists but is 
 * empty, its values are initialized. 
 * 
 * @author Miroslav Bićanić
 */
@WebListener
public class StartupSetup implements ServletContextListener {

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void contextInitialized(ServletContextEvent sce) {
		String filename = sce.getServletContext().getRealPath("/WEB-INF/dbsettings.properties");
		PropertyFetch pf = new PropertyFetch(Paths.get(filename));
		
		ComboPooledDataSource cpds = new ComboPooledDataSource();
		try {
			cpds.setDriverClass("org.apache.derby.jdbc.ClientDriver");
		} catch (PropertyVetoException e1) {
			throw new RuntimeException("Pogreška prilikom inicijalizacije poola.", e1);
		}
		cpds.setJdbcUrl(buildURL(pf));
		cpds.setUser(pf.fetch("user"));
		cpds.setPassword(pf.fetch("password"));
		initializeTables(cpds, sce);

		sce.getServletContext().setAttribute("voting.dbpool", cpds);
	}
	
	/**
	 * Sets up a connection and then creates and initializes the Polls and 
	 * PollOptions tables in the database.
	 * 
	 * @param cpds the {@link DataSource} object used to obtain a connection
	 * @param sce the {@link ServletContextEvent} containing the application-scope context
	 */
	private void initializeTables(ComboPooledDataSource cpds, ServletContextEvent sce) {
		Connection con = null;
		try {
			con = cpds.getConnection();
			List<String> polls = getLinesFrom(sce.getServletContext().getRealPath("/WEB-INF/polls.init"));
			String query = "CREATE TABLE Polls (id BIGINT PRIMARY KEY GENERATED ALWAYS AS IDENTITY,"
					+ "title VARCHAR(150) NOT NULL, message CLOB(2048) NOT NULL)";
			String optionQuery = "CREATE TABLE PollOptions (id BIGINT PRIMARY KEY GENERATED ALWAYS AS IDENTITY,"
					+ "optionTitle VARCHAR(100) NOT NULL, optionLink VARCHAR(150) NOT NULL,"
					+ "pollID BIGINT, votesCount BIGINT, FOREIGN KEY (pollID) REFERENCES Polls(id))";

			boolean pollsTableAlreadyExisted = createTableIfNonexistant(con, query);
			createTableIfNonexistant(con, optionQuery); 
			
			String insert = "INSERT INTO Polls (title, message) VALUES (?,?)";
			for(String poll : polls) {
				String[] split = poll.split("\t");
				//using the flag to avoid sending a lot of SQL requests through 
				//#pollInitialized(String, Connection, ServletContextEvent) if
				//this is the first time the Polls table was created
				if(!pollsTableAlreadyExisted || !pollInitialized(split[0], con, sce)) {
					addPoll(insert, split[0], split[1], con, sce);
				}
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * Tries to create a table using the given {@code query} through
	 * the given {@code con} {@link Connection} object.
	 * 
	 * If creation succeeds, it means the database didn't exist, and
	 * the method returns false.
	 * If creation fails because the table already exists (SQL error
	 * code "X0Y32"), then this method returns true.
	 * 
	 * @param con the {@link Connection} to the database
	 * @param query the query to create the table
	 * @return true if the table already existed; false otherwise
	 * @throws RuntimeException if creation fails for any other reason other than X0Y32
	 */
	private boolean createTableIfNonexistant(Connection con, String query) {
		try(PreparedStatement pst = con.prepareStatement(query)){
			pst.executeUpdate();
			return false;
		} catch (SQLException e) {
			if(e.getSQLState().equals("X0Y32")) {
				return true;
			}
			throw new RuntimeException(e);
		}
	}
	
	/**
	 * Checks if a record with the name {@code pollName} exists in the
	 * Polls table of the database. If it does not, this method returns
	 * false. Otherwise, the method adds all options related to this
	 * poll into the PollOptions table and returns true.
	 * 
	 * @param pollName the title of the poll in the Polls table
	 * @param con the {@link Connection} to the database
	 * @param sce the {@link ServletContextEvent} containing the application-scope context
	 * @return true if the poll record exists in the Polls table; false otherwise
	 */
	private boolean pollInitialized(String pollName, Connection con, ServletContextEvent sce) {
		//fetching the poll with the title from the definition file
		String query = "SELECT id FROM Polls WHERE title=?";
		int pollid = 0;
		try(PreparedStatement pst = con.prepareStatement(query)){
			pst.setString(1, pollName);
			ResultSet rst = pst.executeQuery();
			if(!rst.next()) {
				return false; //poll does not exist at all
			} else {
				pollid = rst.getInt(1);
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
		//poll does exist: add options that may be missing
		addMissingOptions(pollid, pollName, con, sce);
		return true;
	}
	
	/**
	 * Adds a poll into the Polls table of the database, after which it
	 * delegates adding all of the options belonging to that poll to
	 * {@link #addAllOptions(int, String, Connection, ServletContextEvent)}
	 * 
	 * @param insert the parametrized SQL insert statement to use for adding the poll
	 * @param title the title of the poll
	 * @param message the message of the poll
	 * @param con the {@link Connection} to the database
	 * @param sce the {@link ServletContextEvent} containing the application-scope context
	 */
	private void addPoll(String insert, String title, String message, Connection con, ServletContextEvent sce) {
		try(PreparedStatement pst = con.prepareStatement(insert, Statement.RETURN_GENERATED_KEYS)) {
			pst.setString(1, title);
			pst.setString(2, message);
			int noar = pst.executeUpdate();
			if(noar!=1) {
				throw new RuntimeException();
			}
			ResultSet rst = pst.getGeneratedKeys();
			if(rst.next()) {
				addMissingOptions(rst.getInt(1), title, con, sce);
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}
	
	/**
	 * Adds all options that belong to a certain poll to the database,
	 * under the given {@code pollid}, using the {@code table} (the
	 * name of the poll) as the name of the configuration file containing
	 * the options.
	 * Only the options that aren't already both present and linked to the
	 * given {@code pollid} will be added.
	 * 
	 * @param pollid the id of the poll to which the options to add belong to
	 * @param table the name of the poll to which the options belong to
	 * @param con the {@link Connection} to the database
	 * @param sce the {@link ServletContextEvent} containing the application-scope context
	 */
	private void addMissingOptions(int pollid, String pollName, Connection con, ServletContextEvent sce) {
		List<String> lines = getLinesFrom(sce.getServletContext().getRealPath("/WEB-INF/"+pollName.toLowerCase()+".init"));
		String selectQuery = "SELECT * FROM PollOptions WHERE optionTitle=? AND pollid=?";
		String insertQuery = "INSERT INTO PollOptions (optionTitle, optionLink, pollID, votesCount) VALUES (?,?,?,0)";
		
		for(String line : lines) {
			String[] split = line.split("\t");
			try(PreparedStatement pst = con.prepareStatement(selectQuery)) {
				pst.setString(1, split[0]);
				pst.setInt(2, pollid);
				ResultSet rset = pst.executeQuery();
				if(!rset.next()) {
					try(PreparedStatement psetInsert = con.prepareStatement(insertQuery)) {
						psetInsert.setString(1, split[0]);
						psetInsert.setString(2, split[1]);
						psetInsert.setInt(3, pollid);
						int noar = psetInsert.executeUpdate();
						if(noar!=1) {
							throw new RuntimeException();
						}
					}
				}
			} catch (SQLException e) {
				throw new RuntimeException(e);
			}
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void contextDestroyed(ServletContextEvent sce) {
		ComboPooledDataSource cpds = (ComboPooledDataSource)sce.getServletContext().getAttribute("hr.fer.zemris.dbpool");
		if(cpds!=null) {
			try {
				DataSources.destroy(cpds);
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * Builds the URL from the properties stored in the given
	 * {@code pf} object.
	 * 
	 * @param pf the {@link PropertyFetch} object containing properties
	 * @return a string representing the url for connecting to the database
	 */
	private String buildURL(PropertyFetch pf) {
		StringBuilder url = new StringBuilder(50);
		url.append("jdbc:derby://").append(pf.fetch("host"));
		url.append(":").append(pf.fetch("port")).append("/");
		url.append(pf.fetch("name"));
		return url.toString();
	}
	
	/**
	 * Reads all the lines from a file with the given {@code filename},
	 * masking the possible {@link IOException} into a {@link RuntimeException}.
	 * 
	 * @param filename the name of the file
	 * @return a list of lines in the file
	 * @throws RuntimeException masking an {@link IOException} if it occurs
	 */
	private List<String> getLinesFrom(String filename){
		try {
			return Files.readAllLines(Paths.get(filename));
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}
}
