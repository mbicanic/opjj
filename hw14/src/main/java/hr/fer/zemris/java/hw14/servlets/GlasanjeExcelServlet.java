package hr.fer.zemris.java.hw14.servlets;

import java.io.IOException;
import java.util.Collections;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;


import hr.fer.zemris.java.hw14.dao.DAOProvider;
import hr.fer.zemris.java.hw14.models.Option;

/**
 * GlasanjeExcelServlet is a specification of a {@link HttpServlet}
 * which creates an excel table (.xls) filled with the results of
 * voting for bands.
 * 
 * @author Miroslav Bićanić
 */
public class GlasanjeExcelServlet extends AbstractGlasanje {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		long pollid = parseOrSendError(req.getParameter("pollID"), req, resp);
		if(pollid==-1 || !recordExists("Polls", pollid, req, resp)) {
			return;
		}
		List<Option> results = DAOProvider.getDao().getOptionsForPoll(pollid);
		Collections.sort(results, (o1, o2)->o2.getVotes()-o1.getVotes());
		
		HSSFWorkbook hwb = createWorkbook(results);
		
		resp.setContentType("application/vnd.ms-excel");
		resp.setHeader("Content-Disposition", "attachment; filename=\"results.xls\"");
		hwb.write(resp.getOutputStream());
	}
	
	private HSSFWorkbook createWorkbook(List<Option> results) {
		HSSFWorkbook hwb = new HSSFWorkbook();
		HSSFSheet sheet = hwb.createSheet("Rezultati");
		HSSFRow header = sheet.createRow(0);
		header.createCell(0).setCellValue("ID");
		header.createCell(1).setCellValue("Naslov");
		header.createCell(2).setCellValue("Broj glasova");
		
		int rowNumber = 1;
		for(Option o : results) {
			HSSFRow row = sheet.createRow(rowNumber++);
			row.createCell(0).setCellValue(o.getId());
			row.createCell(1).setCellValue(o.getTitle());
			row.createCell(2).setCellValue(o.getVotes());
		}
		return hwb;
	}
}
