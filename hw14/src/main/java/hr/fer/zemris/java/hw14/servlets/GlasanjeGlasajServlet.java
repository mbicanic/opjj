package hr.fer.zemris.java.hw14.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import hr.fer.zemris.java.hw14.dao.DAOProvider;

/**
 * GlasanjeGlasajServlet is a specification of a {@link HttpServlet}
 * which processes a user's vote and stores it in the database.
 * 
 * @author Miroslav Bićanić
 */
public class GlasanjeGlasajServlet extends AbstractGlasanje {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		//get and check given voteID and pollID parameters
		long id = parseOrSendError(req.getParameter("voteID"), req, resp);
		long pollid = parseOrSendError(req.getParameter("pollID"), req, resp);
		if(id==-1 || pollid==-1) {
			return;
		}
		if(!recordExists("Polls", pollid, req, resp) 
				|| !recordExists("PollOptions", id, req, resp)) {
			return;
		}
		
		//add vote and redirect processing to another servlet
		DAOProvider.getDao().addVoteTo(id);
		resp.sendRedirect(resp.encodeRedirectURL(
				req.getContextPath()+"/servleti/glasanje-rezultati?pollID="+pollid));
	}
}
