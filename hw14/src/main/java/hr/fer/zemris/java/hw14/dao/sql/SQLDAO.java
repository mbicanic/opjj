package hr.fer.zemris.java.hw14.dao.sql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import hr.fer.zemris.java.hw14.dao.DAO;
import hr.fer.zemris.java.hw14.models.Option;
import hr.fer.zemris.java.hw14.models.Poll;

/**
 * Ovo je implementacija podsustava DAO uporabom tehnologije SQL. Ova
 * konkretna implementacija očekuje da joj veza stoji na raspolaganju
 * preko {@link SQLConnectionProvider} razreda, što znači da bi netko
 * prije no što izvođenje dođe do ove točke to trebao tamo postaviti.
 * U web-aplikacijama tipično rješenje je konfigurirati jedan filter 
 * koji će presresti pozive servleta i prije toga ovdje ubaciti jednu
 * vezu iz connection-poola, a po zavrsetku obrade je maknuti.
 *  
 * @author marcupic
 */
public class SQLDAO implements DAO {

	@Override
	public List<Option> getOptionsForPoll(long pollid) {
		List<Option> options = new ArrayList<>();
		Connection con = SQLConnectionProvider.getConnection();
		try(PreparedStatement pst = con.prepareStatement("SELECT id, optionTitle, optionLink, votesCount FROM PollOptions WHERE pollid=?")){
			pst.setLong(1, pollid);
			ResultSet rst = pst.executeQuery();
			while(rst.next()) {
				Option o = new Option(
						rst.getString("optionTitle"),
						rst.getString("optionLink"),
						rst.getInt("votesCount"),
						rst.getLong("id"));
				options.add(o);
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
		return options;
	}

	@Override
	public List<Poll> getAllPolls() {
		List<Poll> polls = new ArrayList<>();
		Connection con = SQLConnectionProvider.getConnection();
		try(PreparedStatement pst = con.prepareStatement("SELECT id, title, message FROM Polls")) {
			ResultSet rst = pst.executeQuery();
			while(rst.next()) {
				Poll p = new Poll(rst.getString("title"),
						rst.getString("message"),
						rst.getLong("id"));
				polls.add(p);
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
		return polls;
	}

	@Override
	public void addVoteTo(long optionID) {
		Connection con = SQLConnectionProvider.getConnection();
		String query = "UPDATE PollOptions SET votesCount = votesCount+1 WHERE id=?";
		try(PreparedStatement pst = con.prepareStatement(query)){
			pst.setLong(1, optionID);
			int noar = pst.executeUpdate();
			if(noar!=1) {
				throw new RuntimeException();
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public List<Option> getBestRankingOptions(long pollid) {
		Connection con = SQLConnectionProvider.getConnection();
		String query = "SELECT id, optionTitle, optionLink, votesCount FROM PollOptions WHERE pollid=? AND votesCount = "
				+ "(SELECT MAX(votesCount) FROM PollOptions WHERE pollid=?)";
		List<Option> winners = new ArrayList<>();
		try(PreparedStatement pst = con.prepareStatement(query)) {
			pst.setLong(1, pollid);
			pst.setLong(2, pollid);
			ResultSet rst = pst.executeQuery();
			while(rst.next()) {
				Option o = new Option(
						rst.getString("optionTitle"),
						rst.getString("optionLink"),
						rst.getInt("votesCount"),
						rst.getLong("id"));
				winners.add(o);
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
		return winners;
	}

	@Override
	public boolean recordExists(String tableName, long id) {
		Connection con = SQLConnectionProvider.getConnection();
		String query = "SELECT * FROM "+tableName+" WHERE id=?";
		try(PreparedStatement pst = con.prepareStatement(query)) {
			pst.setLong(1, id);
			ResultSet rst = pst.executeQuery();
			return rst.next();
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}
}