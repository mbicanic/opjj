package hr.fer.zemris.java.hw14.servlets;

import java.io.IOException;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import hr.fer.zemris.java.hw14.dao.DAOProvider;

/**
 * AbstractGlasanje is a semi-specification of a {@link HttpServlet},
 * defining the common methods for all voting-related servlets.
 * 
 * @author Miroslav Bićanić
 */
public class AbstractGlasanje extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * Parses the given {@code parameter} into a long number, redirecting
	 * the processing to an error page if the parsing fails.
	 * 
	 * @param parameter the parameter to parse
	 * @param req the {@link HttpServletRequest} of the request
	 * @param resp the {@link HttpServletResponse} of the request
	 * @return the parsed number, or -1 if parsing fails
	 * @throws IOException if sending the error fails
	 */
	protected long parseOrSendError(String parameter, HttpServletRequest req, HttpServletResponse resp) throws IOException {
		try{
			return Long.parseLong(parameter);
		} catch(NumberFormatException ex) {
			sendError(req, resp);
			return -1;
		}
	}
	
	/**
	 * Checks if a record exists in the given {@code table} with the
	 * given {@code id} in the database.
	 * 
	 * @param table the name of the table in which to look for the record
	 * @param id the id of the record to look for
	 * @param req the {@link HttpServletRequest} of the request
	 * @param resp the {@link HttpServletResponse} of the request
	 * @return true if the record exists; false otherwise
	 * @throws IOException if sending the error fails
	 */
	protected boolean recordExists(String table, long id, HttpServletRequest req, HttpServletResponse resp) throws IOException {
		if(!DAOProvider.getDao().recordExists(table, id)) {
			sendError(req, resp);
			return false;
		}
		return true;
	}
	
	/**
	 * Redirects the user to an {@link ErrorServlet} which will display a page 
	 * informing the user that an error occurred.
	 */
	protected void sendError(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		resp.sendRedirect(resp.encodeRedirectURL(
					req.getContextPath() + "/servleti/error"));
		/*
		 * Redirection is used because the processing of the request stops here,
		 * no dispatching should be done.
		 */
	}
}
