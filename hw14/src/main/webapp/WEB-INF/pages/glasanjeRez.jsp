<%@page import="java.util.Map"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" session="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Rezultati</title>
</head>
<body>
	<h1> Rezultati glasanja </h1>
	<table border="1">
	<tr><td>Opcija</td><td>Broj glasova</td></tr>
	<c:forEach var="e" items="${requestScope.results}">
		<tr><td> ${e.title} </td><td> ${e.votes} </td></tr>
	</c:forEach>
	</table>
	
	<h2> Grafički prikaz rezultata </h2>
	<img alt="Pie-chart" src="glasanje-grafika?pollID=${requestScope.pollID}" width="400" height="300">
	
	<h2> Rezultati u XLS formatu </h2>
	<p> Rezultati u XLS formatu dostupni su <a href="glasanje-xls?pollID=${requestScope.pollID}">ovdje</a></p>
	
	<h2> Razno </h2>
	<p> Poveznice vezane uz opcije sa najviše glasova:</p>
	<ul>
		<c:forEach var="e" items="${requestScope.best}">
			<li> <a href="${e.link}"> ${e.title} </a> </li>
		</c:forEach>
	</ul>
	
</body>
</html>