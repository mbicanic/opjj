<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" session="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Glasanje</title>
</head>
<body>
	<h1> Glasanje </h1>
	<h3> ${currentPoll.message}  </h3>
	<ol>
	<c:forEach var="e" items="${options}">
		<li> <a href="glasanje-glasaj?pollID=${currentPoll.id}&voteID=${e.id}"> ${e.title} </a></li>
	</c:forEach>
	</ol>
</body>
</html>