<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" session="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Error</title>
</head>
<body>
	<h2> Došlo je do pogreške u radu aplikacije </h2>
	<p> Provjerite sljedeće: </p>
	<ul>
	<li> Jeste li zadali ispravan ID </li>
	<li> Jeste li zadali ID postojeće ankete? </li>
	<li> Jeste li glasali za postojeću opciju? </li>
	</ul>
	<hr>
	<a href="servleti/index.html"> Povratak na naslovnicu </a>
</body>
</html>