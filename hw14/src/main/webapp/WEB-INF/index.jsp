<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" session="true" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
    
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Početna</title>
</head>
<body>
	<h1> Ankete </h1>
	<h3> Popis anketa: </h3>
	<ul>
	<c:forEach var="e" items="${polls}">
		<li> ${e.message} <a href="glasanje?pollID=${e.id}"> ovdje </a></li>
	</c:forEach>
	</ul>
</body>
</html>