package hr.fer.zemris.java.hw16.jvdraw.objects.visitor;

import java.awt.Graphics2D;

import hr.fer.zemris.java.hw16.jvdraw.objects.Circle;
import hr.fer.zemris.java.hw16.jvdraw.objects.FilledCircle;
import hr.fer.zemris.java.hw16.jvdraw.objects.GeometricalObject;
import hr.fer.zemris.java.hw16.jvdraw.objects.Line;

/**
 * GeometricalObjectPainter is an implementation of a {@link GeometricalObjectVisitor}
 * which paints all {@link GeometricalObject} objects, regardless of their subtype.
 * 
 * @author Miroslav Bićanić
 */
public class GeometricalObjectPainter implements GeometricalObjectVisitor {

	/**
	 * The {@link Graphics2D} used to draw
	 */
	private Graphics2D g2d;
	
	/**
	 * Constructor for a {@link GeometricalObjectPainter}
	 * @param g2d the {@link Graphics2D} to use while drawing
	 */
	public GeometricalObjectPainter(Graphics2D g2d) {
		this.g2d = g2d;
	}
	
	/**
	 * @param g2d the {@link Graphics2D} to be used for drawing
	 */
	public void setGraphics(Graphics2D g2d) {
		this.g2d = g2d;
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public void visit(Line line) {
		g2d.setColor(line.getColor());
		g2d.drawLine(
				line.getStartPoint().x,
				line.getStartPoint().y,
				line.getEndPoint().x,
				line.getEndPoint().y);
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public void visit(Circle circle) {
		g2d.setColor(circle.getRimColor());
		g2d.drawOval(circle.getCenter().x - circle.getRadius(), 
				circle.getCenter().y - circle.getRadius(),
				circle.getRadius()*2, circle.getRadius()*2);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void visit(FilledCircle fcircle) {
		g2d.setColor(fcircle.getInsideColor());
		g2d.fillOval(fcircle.getCenter().x - fcircle.getRadius(), 
				fcircle.getCenter().y - fcircle.getRadius(),
				fcircle.getRadius()*2, fcircle.getRadius()*2);
		visit((Circle)fcircle);
	}
}
