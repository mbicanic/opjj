package hr.fer.zemris.java.hw16.jvdraw.objects;

import javax.swing.JPanel;

/**
 * GeometricalObjectEditor is a {@link JPanel} providing the user
 * with a visual interface to modify properties of a certain
 * {@link GeometricalObject}.
 * 
 * @author Miroslav Bićanić
 */
public abstract class GeometricalObjectEditor extends JPanel {
	private static final long serialVersionUID = 1L;
	
	/**
	 * Checks if the submitted values after object editing are
	 * acceptable.
	 * @throws IllegalArgumentException
	 */
	public abstract void checkEditing();
	/**
	 * Accepts the submitted values, setting them as the new values
	 * of the edited {@link GeometricalObject}.
	 */
	public abstract void acceptEditing();
	
	/**
	 * Checks if the given {@code value} is parsable as a number.
	 * If yes, the {@code int} value of it is returned. Otherwise,
	 * an {@link IllegalArgumentException} is thrown. <br>
	 * 
	 * @param value the value to be checked
	 * @param name the name of the parameter whose value is changed
	 * @return the integer value of the given {@code value}
	 */
	protected int test(String value, String name) {
		int val;
		try {
			val = Integer.parseInt(value);
		} catch (NumberFormatException ex) {
			throw new IllegalArgumentException(name+" value is not parsable as a number: "+value);
		}
		return val;
	}
}
