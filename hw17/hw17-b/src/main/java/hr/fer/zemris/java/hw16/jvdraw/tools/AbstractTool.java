package hr.fer.zemris.java.hw16.jvdraw.tools;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.event.MouseEvent;

import javax.swing.JToggleButton;

import hr.fer.zemris.java.hw16.jvdraw.components.IColorProvider;
import hr.fer.zemris.java.hw16.jvdraw.components.JDrawingCanvas;
import hr.fer.zemris.java.hw16.jvdraw.model.DrawingModel;
import hr.fer.zemris.java.hw16.jvdraw.objects.GeometricalObject;
import hr.fer.zemris.java.hw16.jvdraw.objects.visitor.GeometricalObjectPainter;

/**
 * AbstractTool is an abstract specification of a {@link JToggleButton}
 * and an abstract implementation of a {@link Tool}, bridging the gap
 * and reducing redundant coding between the {@link Tool} interface and
 * its actual implementations.
 * 
 * @author Miroslav Bićanić
 */
public abstract class AbstractTool extends JToggleButton implements Tool {
	private static final long serialVersionUID = 1L;
	
	/**
	 * The underlying {@link DrawingModel} 
	 */
	protected DrawingModel dm;
	/**
	 * An {@link IColorProvider} providing with the foreground color
	 */
	protected IColorProvider fgColor;
	/**
	 * A {@link JDrawingCanvas} on which the image is drawn
	 */
	protected JDrawingCanvas canvas;
	
	/**
	 * The {@link GeometricalObject} that is being drawn
	 */
	protected GeometricalObject object;
	
	/**
	 * A {@link GeometricalObjectPainter} used to paint the object in creation
	 */
	protected GeometricalObjectPainter GOP = new GeometricalObjectPainter(null);
	
	/**
	 * Constructor for an AbstractTool
	 * @param dm the underlying {@link DrawingModel}
	 * @param foreground the {@link IColorProvider} providing with the foreground {@link Color}
	 * @param canvas the {@link JDrawingCanvas} on which the image is drawn
	 * @param label the name of the tool, to be displayed on the {@link JToggleButton}'s surface
	 * @param d the preferred {@link Dimension} of the {@link JToggleButton}
	 */
	public AbstractTool(DrawingModel dm, IColorProvider foreground, JDrawingCanvas canvas, String label, Dimension d) {
		this.dm = dm;
		this.fgColor = foreground;
		this.canvas = canvas;
		
		this.setText(label);
		this.setPreferredSize(d);
	}
	
	/**
	 * Invoked whenever the mouse was clicked and there was no 
	 * {@link #object} being created at that point.
	 * @param e the {@link MouseEvent} describing the event
	 */
	protected abstract void onFirstClick(MouseEvent e);
	/**
	 * Invoked whenever the mouse was clicked and there already
	 * was an {@link #object} being created at that point.
	 * @param e the {@link MouseEvent} describing the event
	 */
	protected abstract void onSecondClick(MouseEvent e);
	/**
	 * Invoked whenever the mouse was moved over a certain
	 * component.
	 * @param e the {@link MouseEvent} describing the event
	 */
	protected abstract void onMove(MouseEvent e);

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void mouseClicked(MouseEvent e) {
		if(object==null) {
			onFirstClick(e);
		} else {
			onSecondClick(e);
		}
	}
	/**
	 * {@inheritDoc}
	 */
	@Override
	public void mouseMoved(MouseEvent e) {
		if(object!=null) {
			onMove(e);
		}
	}
	/**
	 * {@inheritDoc}
	 */
	@Override
	public void paint(Graphics2D g2d) {
		if(object!=null) {
			GOP.setGraphics(g2d);
			object.accept(GOP);
		}
	}
	
	/**
	 * {@inheritDoc}<br>
	 * At this level, this method is not used as the objects are defined 
	 * with click-move-click, not of press-drag-release.
	 */
	@Override
	public void mousePressed(MouseEvent e) {}
	/**
	 * {@inheritDoc}<br>
	 * At this level, this method is not used as the objects are defined 
	 * with click-move-click, not of press-drag-release.
	 */
	@Override
	public void mouseReleased(MouseEvent e) {}
	/**
	 * {@inheritDoc}<br>
	 * At this level, this method is not used as the objects are defined 
	 * with click-move-click, not of press-drag-release.
	 */
	@Override
	public void mouseDragged(MouseEvent e) {}

	

}
