package hr.fer.zemris.java.hw16.jvdraw;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.GridLayout;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import javax.imageio.ImageIO;
import javax.swing.AbstractAction;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.filechooser.FileNameExtensionFilter;

import hr.fer.zemris.java.hw16.jvdraw.model.DrawingModel;
import hr.fer.zemris.java.hw16.jvdraw.model.DrawingModelImpl;
import hr.fer.zemris.java.hw16.jvdraw.model.DrawingModelListener;
import hr.fer.zemris.java.hw16.jvdraw.objects.GeometricalObject;
import hr.fer.zemris.java.hw16.jvdraw.objects.visitor.GeometricalObjectBBCalculator;
import hr.fer.zemris.java.hw16.jvdraw.objects.visitor.GeometricalObjectJVDCreator;
import hr.fer.zemris.java.hw16.jvdraw.objects.visitor.GeometricalObjectPainter;

/**
 * JVDrawSkeleton is a specification of a JFrame and an implementation
 * of a {@link DrawingModelListener}. It contains the skeletal methods
 * and objects that aren't strictly related to the GUI itself, and such
 * organisation is done to relieve the {@link JVDraw} class.
 * 
 * @author Miroslav Bićanić
 */
public abstract class JVDrawSkeleton extends JFrame implements DrawingModelListener {
	private static final long serialVersionUID = 1L;
	
	/** The underlying {@link DrawingModel} for the program */
	protected DrawingModel dm;
	/**
	 * The {@link Path} where the currently open document is savet */
	private Path currentDocument;
	
	/**
	 * Default constructor for a {@link JVDrawSkeleton}
	 */
	public JVDrawSkeleton() {
		this.dm = new DrawingModelImpl();
		dm.addDrawingModelListener(this);
	}
	
	/**
	 * An {@link AbstractAction} representing the action of saving
	 * a document.
	 * This type of saving is an <i> identity save </i> where the
	 * document is saved as itself (if it has a path associated
	 * with itself). This is the opposite of the situation with
	 * the {@link #saveAs} {@link AbstractAction}.
	 */
	protected AbstractAction save = new AbstractAction() {
		private static final long serialVersionUID = 1L;
		@Override
		public void actionPerformed(ActionEvent e) {
			saveMethod(true);
		}
	};
	/**
	 * An {@link AbstractAction} representing the action of saving
	 * a document as another/a new document.
	 */
	protected AbstractAction saveAs = new AbstractAction() {
		private static final long serialVersionUID = 1L;
		@Override
		public void actionPerformed(ActionEvent e) {
			saveMethod(false);
		}
	};
	/**
	 * An {@link AbstractAction} representing the action of exporting
	 * the drawn image into a couple of different image file formats.
	 */
	protected AbstractAction export = new AbstractAction() {
		private static final long serialVersionUID = 1L;
		@Override
		public void actionPerformed(ActionEvent e) {
			exportMethod();
		}
	};
	/**
	 * An {@link AbstractAction} representing the action of loading
	 * an image from a .jvd file by reading the definitions.
	 */
	protected AbstractAction load = new AbstractAction() {
		private static final long serialVersionUID = 1L;
		@Override
		public void actionPerformed(ActionEvent e) {
			openMethod();
		}
	};
	/**
	 * An {@link AbstractAction} which terminates the application,
	 * after being permitted by the user if there are unsaved changes.
	 */
	protected AbstractAction exit = new AbstractAction() {
		private static final long serialVersionUID = 1L;
		@Override
		public void actionPerformed(ActionEvent e) {
			handleClosing();
		}
	};
	
	/**
	 * Handles the process of closing a window, including checking
	 * for unsaved changes.
	 */
	protected void handleClosing() {
		if(dm.isModified()) {
			if(askForUnsaved()) {
				dispose();
			} else {
				return;
			}
		} else {
			dispose();
		}
	}

	/**
	 * Pops a dialog asking the user if he wants to save unsaved changes.
	 * @return true if the user decidedly agreed to save, or agreed to not save and forget about it (YES/NO); 
	 * 			false if the user decided to give up (CANCEL)
	 */
	private boolean askForUnsaved() {
		int option = JOptionPane.showConfirmDialog(this, "Do you want to save changes?", "Changes!", 
				JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE, null);
		if(option == JOptionPane.YES_OPTION) {
			saveMethod(true);
			JOptionPane.showMessageDialog(this, "Document saved!");
			return true;
		} else if(option == JOptionPane.CANCEL_OPTION) {
			JOptionPane.showMessageDialog(this, "Operation cancelled!");
			return false;
		} else {
			JOptionPane.showMessageDialog(this, "Document not saved!");
			return true;
		}
	}
	
	/**
	 * Method handling the process of opening a file from the disk.
	 */
	private void openMethod() {
		if(dm.isModified()) {
			if(!askForUnsaved()) {
				return;
			}
		}
		Path p = chooseOpenDocument();
		if(p==null) {
			return;
		}
		JVDLoader loader = new JVDLoader(p);
		List<GeometricalObject> objects = loader.getObjects();
		dm.clear();
		for(GeometricalObject o : objects) {
			dm.add(o);
		}
		dm.clearModifiedFlag();
		this.currentDocument = p;
	}
	
	/**
	 * Pops a file chooser dialog asking the user to choose a file 
	 * to open, offering only .jvd files.
	 * @return a {@link Path} to the chosen file; <code>null</code> if operation aborted
	 */
	private Path chooseOpenDocument() {
		JFileChooser jfc = new JFileChooser();
		jfc.setFileFilter(new FileNameExtensionFilter("JVD files", "jvd"));
		while(true) {
			if(jfc.showOpenDialog(this)==JFileChooser.APPROVE_OPTION) {
				String path = jfc.getSelectedFile().getAbsolutePath();
				Path p = Paths.get(path);
				if(!Files.exists(p)) {
					if(JOptionPane.showConfirmDialog(this, "File doesn't exist. Try again?")==JOptionPane.OK_OPTION) {
						continue;
					} else {
						return null;
					}
				}
				return p;
			} else {
				return null;
			}
		}
	}
	
	/**
	 * Method handling the process of saving a file to disk
	 * @param identitySave flag indicating if the file is saved as itself,
	 * 			or as a new document
	 */
	private void saveMethod(boolean identitySave) {
		if(currentDocument==null || !identitySave) {
			Path p = chooseSaveDocument();
			if(p==null) {
				return;
			}
			currentDocument = p;
		}
		writeToPath(currentDocument);
		save.setEnabled(false);
		dm.clearModifiedFlag();
		return;
	}
	
	/**
	 * Pops a file chooser dialog asking the user to choose a file to which
	 * to save.
	 * @return a {@link Path} to the chosen file; <code>null</code> if operation aborted
	 */
	private Path chooseSaveDocument() {
		JFileChooser jfc = new JFileChooser();
		Path p = null;
		while(true) {
			if(jfc.showSaveDialog(this)==JFileChooser.APPROVE_OPTION) {
				String path = jfc.getSelectedFile().getAbsolutePath();
				if(!path.endsWith(".jvd")) {
					int option = JOptionPane.showConfirmDialog(this, 
							"Given path does not have a .jvd extension. Do you want for it to be added?");
					if(JOptionPane.OK_OPTION == option) {
						path+=".jvd";
					} else if (JOptionPane.NO_OPTION == option){
						continue;
					} else {
						return null;
					}
				}
				p = Paths.get(path);
				if(Files.exists(p)) {
					if(JOptionPane.showConfirmDialog(this, "Do you want to overwrite file?")!=JOptionPane.OK_OPTION) {
						continue;
					}
					return p;
				}
				return p;
			} else {
				return null;
			}
		}
	}
	
	/**
	 * Creates the contents of the file based on the currently present
	 * {@link GeometricalObject}s in the program, and writes them to 
	 * the given {@link Path}
	 * @param p the {@link Path} to which to write the contents
	 */
	private void writeToPath(Path p) {
		int size = dm.getSize();
		GeometricalObjectJVDCreator jvdc = new GeometricalObjectJVDCreator(new StringBuilder(size*40));
		for(int i=0; i<size; i++) {
			dm.getObject(i).accept(jvdc);
		}
		try {
			Files.writeString(currentDocument, jvdc.getJVD());
		} catch (IOException e1) {
			JOptionPane.showMessageDialog(this, "An error occurred while saving.");
		}
	}
	
	/**
	 * Method handling the process of exporting the drawn image as
	 * a JPG, PNG or GIF image.
	 */
	private void exportMethod() {
		//calculating the bounding box
		GeometricalObjectBBCalculator gobbc = new GeometricalObjectBBCalculator();
		int size = dm.getSize();
		for(int i=0; i<size; i++) {
			dm.getObject(i).accept(gobbc);
		}
		Rectangle bbox = gobbc.getBoundingBox();
		BufferedImage image = new BufferedImage(bbox.width, bbox.height, BufferedImage.TYPE_3BYTE_BGR);
		
		//painting the image
		Graphics2D g = (Graphics2D)image.getGraphics();
		g.translate(-bbox.x, -bbox.y);
		g.setColor(Color.WHITE);
		g.fillRect(bbox.x, bbox.y, bbox.width, bbox.height);
		GeometricalObjectPainter gop = new GeometricalObjectPainter(g);
		for(int i=0; i<size; i++) {
			dm.getObject(i).accept(gop);
		}
		g.dispose();
		
		//starting the export process - choose extension
		JComboBox<String> jcb = new JComboBox<>(new String[] {"png", "gif", "jpg"});
		JPanel message = new JPanel(new GridLayout(0,2));
		message.add(new JLabel("Format:"));
		message.add(jcb);
		int option = JOptionPane.showConfirmDialog(this,  message, "Choose image format", JOptionPane.OK_CANCEL_OPTION);
		if(option == JOptionPane.CANCEL_OPTION) {
			return;
		}
		String extension = jcb.getSelectedItem().toString();
		
		//choosing a file and saving to it
		JFileChooser jfc = new JFileChooser();
		if(jfc.showSaveDialog(this)==JFileChooser.APPROVE_OPTION) {
			String path = jfc.getSelectedFile().toPath().toAbsolutePath().toString();
			if(!path.endsWith("."+extension)) {
				path+= "."+extension;
			}
			Path p = Paths.get(path);
			if(Files.exists(p)) {
				if(JOptionPane.showConfirmDialog(this, "Do you want to overwrite file?")!=JOptionPane.OK_OPTION) {
					return;
				}
			}
			try {
				ImageIO.write(image, extension, p.toFile());
			} catch (IOException e1) {
				JOptionPane.showMessageDialog(this, "Error occured while exporting.");
			}
		}
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public void objectsAdded(DrawingModel source, int index0, int index1) {
		export.setEnabled(true);
		save.setEnabled(true);
		saveAs.setEnabled(true);
	}
	/**
	 * {@inheritDoc}
	 */
	@Override
	public void objectsChanged(DrawingModel source, int index0, int index1) {
		save.setEnabled(true);
		saveAs.setEnabled(true);
	}
	/**
	 * {@inheritDoc}
	 */
	@Override
	public void objectsRemoved(DrawingModel source, int index0, int index1) {
		boolean value = source.getSize()==0;
		save.setEnabled(!value);
		saveAs.setEnabled(!value);
		export.setEnabled(!value);
	}
}
