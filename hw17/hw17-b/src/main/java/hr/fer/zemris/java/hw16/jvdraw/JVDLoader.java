package hr.fer.zemris.java.hw16.jvdraw;

import java.awt.Color;
import java.awt.Point;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

import hr.fer.zemris.java.hw16.jvdraw.objects.Circle;
import hr.fer.zemris.java.hw16.jvdraw.objects.FilledCircle;
import hr.fer.zemris.java.hw16.jvdraw.objects.GeometricalObject;
import hr.fer.zemris.java.hw16.jvdraw.objects.Line;

/**
 * JVDLoader is a class used to open and load .jvd files associated
 * with the {@link JVDraw} app.
 * 
 * @author Miroslav Bićanić
 */
public class JVDLoader {
	/**
	 * A {@link List} of lines in the .jvd file
	 */
	private List<String> lines;
	/**
	 * A {@link List} of {@link GeometricalObject}s built from the file
	 * definitions
	 */
	private List<GeometricalObject> objects;
	
	/**
	 * Constructor for a JVDLoader
	 * @param p {@link Path} to the .jvd file
	 */
	public JVDLoader(Path p) {
		try {
			lines = Files.readAllLines(p);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
		objects = new ArrayList<>();
		parse(lines);
	}
	
	/**
	 * @return the {@link List} of all {@link GeometricalObject}s created
	 * by interpreting the given .jvd file
	 */
	public List<GeometricalObject> getObjects(){
		return objects;
	}
	
	/**
	 * Parses the document, creating {@link GeometricalObject}s using
	 * their definitions from the .jvd file.
	 * @param lines the lines of the file
	 */
	private void parse(List<String> lines) {
		for(String line : lines) {
			String[] split = line.split(" ");
			switch(split[0]) {
			case "LINE":
				objects.add(parseLine(split));
				break;
			case "CIRCLE":
				objects.add(parseCircle(split));
				break;
			case "FCIRCLE":
				objects.add(parseFCircle(split));
				break;
			default:
				throw new RuntimeException("Incorrect file format: "+line);
			}
		}
	}
	
	/**
	 * Parses one line of text and creates a {@link Line} using the parameters
	 * from the definition from the file.
	 * @param split the line of text split by spaces.
	 * @return the created {@link Line}
	 */
	private Line parseLine(String[] split) {
		Point start = new Point(Integer.parseInt(split[1]), Integer.parseInt(split[2]));
		Point end = new Point(Integer.parseInt(split[3]), Integer.parseInt(split[4]));
		Color color = new Color(Integer.parseInt(split[5]),
				Integer.parseInt(split[6]), 
				Integer.parseInt(split[7]));
		Line l = new Line(start, end, color);
		return l;
	}
	/**
	 * Parses one line of text and creates a {@link Circle} using the parameters
	 * from the definition from the file.
	 * @param split the line of text split by spaces.
	 * @return the created {@link Circle}
	 */
	private Circle parseCircle(String[] split) {
		Point center = new Point(Integer.parseInt(split[1]), Integer.parseInt(split[2]));
		int radius = Integer.parseInt(split[3]);
		Color color = new Color(Integer.parseInt(split[4]),
				Integer.parseInt(split[5]), 
				Integer.parseInt(split[6]));
		Circle c = new Circle(center, radius, color);
		return c;
	}
	/**
	 * Parses one line of text and creates a {@link FilledCircle} using 
	 * the parameters from the definition from the file.
	 * @param split the line of text split by spaces.
	 * @return the created {@link FilledCircle}
	 */
	private FilledCircle parseFCircle(String[] split) {
		Circle c = parseCircle(split);
		Color colorIn = new Color(Integer.parseInt(split[7]),
				Integer.parseInt(split[8]), 
				Integer.parseInt(split[9]));
		FilledCircle fc = new FilledCircle(c, colorIn);
		return fc;
	}	
}
