package hr.fer.zemris.java.hw16.jvdraw.model;

import java.util.ArrayList;
import java.util.List;

import hr.fer.zemris.java.hw16.jvdraw.objects.GeometricalObject;
import hr.fer.zemris.java.hw16.jvdraw.objects.GeometricalObjectListener;

/**
 * DrawingModelImpl is an implementation of a {@link DrawingModel}, as well
 * as a {@link GeometricalObjectListener}.
 * 
 * @author Miroslav Bićanić
 */
public class DrawingModelImpl implements DrawingModel, GeometricalObjectListener {

	/**
	 * A {@link List} of {@link GeometricalObject} objects present in this model
	 */
	private List<GeometricalObject> objects = new ArrayList<>();
	/**
	 * A {@link List} of {@link DrawingModelListener} objects listening to changes
	 * to the model
	 */
	private List<DrawingModelListener> listeners = new ArrayList<>();
	/**
	 * A flag indicating if the model has been modified
	 */
	private boolean modified = false;
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getSize() {
		return objects.size();
	}
	/**
	 * {@inheritDoc}
	 */
	@Override
	public GeometricalObject getObject(int index) {
		return objects.get(index);
	}
	/**
	 * {@inheritDoc}
	 */
	@Override
	public int indexOf(GeometricalObject object) {
		return objects.indexOf(object);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void add(GeometricalObject object) {
		object.addGeometricalObjectListener(this);
		this.objects.add(object);
		modified = true;
		notifyInserted(objects.size()-1);
	}
	/**
	 * {@inheritDoc}
	 */
	@Override
	public void remove(GeometricalObject object) {
		object.removeGeometricalObjectListener(this);
		int index = objects.indexOf(object);
		this.objects.remove(object);
		modified = true;
		notifyRemoved(index, index);
	}
	/**
	 * {@inheritDoc}
	 */
	@Override
	public void changeOrder(GeometricalObject object, int offset) {
		int newPosition = objects.indexOf(object) + offset;
		if(newPosition<0||newPosition>=objects.size()) {
			return;
		}
		remove(object);
		objects.add(newPosition, object);
		object.addGeometricalObjectListener(this);
		modified = true;
		notifyInserted(newPosition);
	}
	/**
	 * {@inheritDoc}
	 */
	@Override
	public void clear() {
		int lastIndex = objects.size() - 1;
		objects.clear();
		modified = true;
		if(lastIndex>=0) {
			notifyRemoved(0, lastIndex);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isModified() {
		return modified;
	}
	/**
	 * {@inheritDoc}
	 */
	@Override
	public void clearModifiedFlag() {
		modified = false;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void addDrawingModelListener(DrawingModelListener l) {
		this.listeners.add(l);
	}
	/**
	 * {@inheritDoc}
	 */
	@Override
	public void removeDrawingModelListener(DrawingModelListener l) {
		this.listeners.remove(l);
	}
	
	/**
	 * Notifies all registered {@link DrawingModelListener}s that
	 * objects have been removed from the model from index {@code start}
	 * to index {@code end}
	 * @param start the index of the first removed element
	 * @param end the index of the last removed element
	 */
	private void notifyRemoved(int start, int end) {
		for(DrawingModelListener l : listeners) {
			l.objectsRemoved(this, start, end);
		}
	}
	/**
	 * Notifies all registered {@link DrawingModelListener}s that an
	 * object has been inserted from the model at position {@code index}
	 * @param index the index of the inserted element
	 */
	private void notifyInserted(int index) {
		for(DrawingModelListener l : listeners) {
			l.objectsAdded(this, index, index);
		}
	}

	/**
	 * {@inheritDoc}<br>
	 * If a {@link GeometricalObject} changes, the modified flag of the
	 * model is raised to true, and the notification is forwarded to all
	 * listeners of the model.
	 */
	@Override
	public void geometricalObjectChanged(GeometricalObject o) {
		this.modified = true;
		for(DrawingModelListener l : listeners) {
			l.objectsChanged(this, objects.indexOf(o), objects.indexOf(o));
		}
	}
}
