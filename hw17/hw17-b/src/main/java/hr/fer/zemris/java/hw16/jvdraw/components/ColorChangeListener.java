package hr.fer.zemris.java.hw16.jvdraw.components;

import java.awt.Color;

/**
 * ColorChangeListener is an interface for objects that are interested
 * in an {@link IColorProvider} changing its {@link Color}
 * 
 * @author Miroslav Bićanić
 */
public interface ColorChangeListener {
	/**
	 * Method called whenever an {@link IColorProvider} changes its color
	 * @param source the {@link IColorProvider} which changed its color
	 * @param oldColor the {@link Color} the {@link IColorProvider} had prior to changing it
	 * @param newColor the new {@link Color} of the {@link IColorProvider}
	 */
	void newColorSelected(IColorProvider source, Color oldColor, Color newColor);
}
