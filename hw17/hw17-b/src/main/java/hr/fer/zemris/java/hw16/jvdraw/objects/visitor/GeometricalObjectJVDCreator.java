package hr.fer.zemris.java.hw16.jvdraw.objects.visitor;

import hr.fer.zemris.java.hw16.jvdraw.objects.Circle;
import hr.fer.zemris.java.hw16.jvdraw.objects.FilledCircle;
import hr.fer.zemris.java.hw16.jvdraw.objects.GeometricalObject;
import hr.fer.zemris.java.hw16.jvdraw.objects.Line;

/**
 * GeometricalObjectJVDCreator is an implementation of a {@link GeometricalObjectVisitor}
 * which builds a string of text containing a line for each visited {@link GeometricalObject}.
 * 
 * Each line contains parameters of the corresponding {@link GeometricalObject}.
 * 
 * The string of text is later used to save the image as a .jvd file.
 * 
 * @author Miroslav Bićanić
 */
public class GeometricalObjectJVDCreator implements GeometricalObjectVisitor {
	/** A {@link StringBuilder} used to build the string */
	private StringBuilder sb;
	
	/**
	 * Constructor for a {@link GeometricalObjectJVDCreator}
	 * @param sb a {@link StringBuilder} to build the text
	 */
	public GeometricalObjectJVDCreator(StringBuilder sb) {
		this.sb = sb;
	}

	/**
	 * Once the string of text is built, this method returns it
	 * @return the built string of text
	 */
	public String getJVD() {
		String result = sb.toString();
		return result.substring(0, result.length()-1); //removing last '\n'
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public void visit(Line line) {
		sb.append("LINE ").append(line.getStartPoint().x).append(" ")
			.append(line.getStartPoint().y).append(" ").append(line.getEndPoint().x)
			.append(" ").append(line.getEndPoint().y).append(" ").append(line.getColor().getRed())
			.append(" ").append(line.getColor().getGreen()).append(" ").append(line.getColor().getBlue());
		sb.append("\n");
	}
	/**
	 * {@inheritDoc}
	 */
	@Override
	public void visit(Circle circle) {
		commonCircle(circle);
		sb.append("\n");
	}
	/**
	 * {@inheritDoc}
	 */
	@Override
	public void visit(FilledCircle fcircle) {
		sb.append("F");
		commonCircle((Circle)fcircle);
		sb.append(" ").append(fcircle.getInsideColor().getRed())
			.append(" ").append(fcircle.getInsideColor().getGreen()).append(" ")
			.append(fcircle.getInsideColor().getBlue());
		sb.append("\n");
	}
	
	/**
	 * Builds a part of the text that is common for both {@link Circle}
	 * and {@link FilledCircle} objects.
	 * 
	 * @param circle a {@link Circle} from which to build the common part
	 */
	private void commonCircle(Circle circle) {
		sb.append("CIRCLE ").append(circle.getCenter().x).append(" ").append(circle.getCenter().y)
		.append(" ").append(circle.getRadius()).append(" ").append(circle.getRimColor().getRed())
		.append(" ").append(circle.getRimColor().getGreen()).append(" ").append(circle.getRimColor().getBlue());
	}
}
