package hr.fer.zemris.java.hw16.jvdraw.objects.visitor;

import hr.fer.zemris.java.hw16.jvdraw.objects.Circle;
import hr.fer.zemris.java.hw16.jvdraw.objects.FilledCircle;
import hr.fer.zemris.java.hw16.jvdraw.objects.Line;

/**
 * GeometricalObjectVisitor is an interface for objects that perform 
 * a common operation over multiple different {@link GeometricalObject}s,
 * knowing exactly how to deal with every kind of an object.
 * 
 * @author Miroslav Bićanić
 */
public interface GeometricalObjectVisitor {
	/**
	 * Method called whenever the visitor encounters a {@link Line}
	 * @param line the {@link Line} to visit
	 */
	void visit(Line line);
	/**
	 * Method called whenever the visitor encounters a {@link Circle}
	 * @param circle the {@link Circle} to visit
	 */
	void visit(Circle circle);
	/**
	 * Method called whenever the visitor encounters a {@link FilledCircle}
	 * @param fcircle the {@link FilledCircle} to visit
	 */
	void visit(FilledCircle fcircle);
}
