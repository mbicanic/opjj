package hr.fer.zemris.java.hw16.jvdraw.components;

import java.awt.Color;

/**
 * IColorProvider is an interface for objects that can provide with
 * {@link Color}s. <br>
 * <br>
 * IColorProviders are subjects in the observer design pattern, thus
 * providing with {@link ColorChangeListener} registration and 
 * deregistration methods.
 * 
 * @author Miroslav Bićanić
 */
public interface IColorProvider {
	/**
	 * Returns the {@link Color} that this IColorProvider currently uses
	 * @return the currenlty used {@link Color}
	 */
	Color getCurrentColor();
	/**
	 * Adds a {@link ColorChangeListener} to this {@link IColorProvider}
	 * @param l the {@link ColorChangeListener} to add
	 */
	void addColorChangeListener(ColorChangeListener l);
	/**
	 * Removes a {@link ColorChangeListener} from this {@link IColorProvider}
	 * @param l the {@link ColorChangeListener} to remove
	 */
	void removeColorChangeListener(ColorChangeListener l);
}
