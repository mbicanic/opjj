package hr.fer.zemris.java.hw16.jvdraw.components;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.util.function.Supplier;

import javax.swing.JComponent;

import hr.fer.zemris.java.hw16.jvdraw.JVDraw;
import hr.fer.zemris.java.hw16.jvdraw.model.DrawingModel;
import hr.fer.zemris.java.hw16.jvdraw.model.DrawingModelListener;
import hr.fer.zemris.java.hw16.jvdraw.objects.visitor.GeometricalObjectPainter;
import hr.fer.zemris.java.hw16.jvdraw.tools.Tool;

/**
 * JDrawingCanvas is a {@link JComponent} implementing the {@link DrawingModelListener}
 * interface. It allows the user to draw {@link GeometricalObject}s on the surface of
 * the component.
 * 
 * @author Miroslav Bićanić
 */
public class JDrawingCanvas extends JComponent implements DrawingModelListener {
	private static final long serialVersionUID = 1L;
	/**
	 * The underlying {@link DrawingModel}
	 */
	private DrawingModel dm;
	/**
	 * A {@link Supplier} of {@link Tool} objects so that repainting
	 * the component can be forwarded to the currently selected tool
	 * in {@link JVDraw}, in case it has anytning to add.
	 */
	private Supplier<Tool> supplier;
	
	/**
	 * A constructor for a {@link JDrawingCanvas}
	 * @param dm the underlying {@link DrawingModel}
	 * @param toolSource a {@link Supplier} of {@link Tool}s to enable complete repaint
	 */
	public JDrawingCanvas(DrawingModel dm, Supplier<Tool> toolSource) {
		this.dm = dm;
		this.supplier = toolSource;
		this.setPreferredSize(new Dimension(500,500));
		dm.addDrawingModelListener(this);
	}
	
	/**
	 * The component is painted by painting each {@link GeometricalObject} from
	 * the {@link DrawingModel} in order, and then allowing the currently selected
	 * {@link Tool} in {@link JVDraw} to add something to the image.
	 */
	@Override
	protected void paintComponent(Graphics g) {
		Graphics2D g2 = (Graphics2D)g;
		
		g2.setColor(Color.WHITE);
		g2.fillRect(0, 0, this.getWidth(), this.getHeight());
		
		int size = dm.getSize();
		GeometricalObjectPainter gop = new GeometricalObjectPainter(g2);
		for(int i = 0; i<size; i++) {
			dm.getObject(i).accept(gop);
		}
		supplier.get().paint(g2);
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public void objectsRemoved(DrawingModel source, int index0, int index1) {
		repaint();
	}
	/**
	 * {@inheritDoc}
	 */
	@Override
	public void objectsChanged(DrawingModel source, int index0, int index1) {
		repaint();
	}
	/**
	 * {@inheritDoc}
	 */
	@Override
	public void objectsAdded(DrawingModel source, int index0, int index1) {
		repaint();				
	}
}
