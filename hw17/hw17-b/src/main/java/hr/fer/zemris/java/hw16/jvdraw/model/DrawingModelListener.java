package hr.fer.zemris.java.hw16.jvdraw.model;

import hr.fer.zemris.java.hw16.jvdraw.objects.GeometricalObject;

/**
 * DrawingModelListener is an interface for all objects interested
 * in changes in the {@link DrawingModel}.
 * 
 * @author Miroslav Bićanić
 */
public interface DrawingModelListener {
	/**
	 * Method invoked whenever {@link GeometricalObject}s have been added to the model
	 * @param source the {@link DrawingModel} to which the {@link GeometricalObject}s have been added
	 * @param index0 the index of the first added {@link GeometricalObject}
	 * @param index1 the index of the last added {@link GeometricalObject}
	 */
	void objectsAdded(DrawingModel source, int index0, int index1);
	/**
	 * Method invoked whenever {@link GeometricalObject}s have been removed from the model
	 * @param source the {@link DrawingModel} from which the {@link GeometricalObject}s have been removed
	 * @param index0 the index of the first removed {@link GeometricalObject}
	 * @param index1 the index of the last removed {@link GeometricalObject}
	 */
	void objectsRemoved(DrawingModel source, int index0, int index1);
	/**
	 * Method invoked whenever {@link GeometricalObject}s have been changed in the model
	 * @param source the {@link DrawingModel} in which the {@link GeometricalObject}s have been changed
	 * @param index0 the index of the first changed {@link GeometricalObject}
	 * @param index1 the index of the last changed {@link GeometricalObject}
	 */
	void objectsChanged(DrawingModel source, int index0, int index1);
}
