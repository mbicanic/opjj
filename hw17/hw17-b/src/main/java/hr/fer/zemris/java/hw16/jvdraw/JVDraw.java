package hr.fer.zemris.java.hw16.jvdraw;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.Action;
import javax.swing.ButtonGroup;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JToolBar;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;

import hr.fer.zemris.java.hw16.jvdraw.components.ColorLabel;
import hr.fer.zemris.java.hw16.jvdraw.components.JColorArea;
import hr.fer.zemris.java.hw16.jvdraw.components.JDrawingCanvas;
import hr.fer.zemris.java.hw16.jvdraw.model.DrawingModel;
import hr.fer.zemris.java.hw16.jvdraw.model.DrawingObjectListModel;
import hr.fer.zemris.java.hw16.jvdraw.objects.GeometricalObject;
import hr.fer.zemris.java.hw16.jvdraw.objects.GeometricalObjectEditor;
import hr.fer.zemris.java.hw16.jvdraw.tools.EmptyCircleTool;
import hr.fer.zemris.java.hw16.jvdraw.tools.FilledCircleTool;
import hr.fer.zemris.java.hw16.jvdraw.tools.LineTool;
import hr.fer.zemris.java.hw16.jvdraw.tools.Tool;

/**
 * JVDraw is a program enabling the user to paint {@link GeometricalObject}s
 * onto a canvas. <br>
 * <br>
 * Each drawn {@link GeometricalObject} has its own defined position
 * in the {@link DrawingModel}, which enables object layering. 
 * At all times, all drawn objects are shown in a list on the side through
 * which all their properties can be modified, and they can be deleted, 
 * pushed to back or brought to front<br>
 * <br>
 * Drawn images can either be saved as .jvd files, enabling further
 * editing, or exported as a JPG, PNG or GIF image.
 * 
 * @author Miroslav Bićanić
 */
public class JVDraw extends JVDrawSkeleton {
	private static final long serialVersionUID = 1L;
	
	private JDrawingCanvas canvas;
	private JList<GeometricalObject> objectList;
	private Tool currentTool;
	
	private final MouseAdapter dispatcher = new MouseAdapter() {
		@Override
		public void mouseClicked(MouseEvent e) {
			currentTool.mouseClicked(e);
		}
		@Override
		public void mouseMoved(MouseEvent e) {
			currentTool.mouseMoved(e);
		}
	};
	
	/**
	 * Default constructor for JVDraw
	 */
	public JVDraw() {
		super();
		setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
		setLocation(20,20);
		setTitle("JVDraw");
		this.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				handleClosing();
			}
		});
		initGUI();
		pack();
	}

	/**
	 * Initializes the GUI of the program
	 */
	private void initGUI() {
		Container cp = getContentPane();
		cp.setLayout(new BorderLayout());

		createMenuBar();
		
		canvas = new JDrawingCanvas(dm, ()->this.currentTool);
		canvas.addMouseListener(dispatcher);
		canvas.addMouseMotionListener(dispatcher);
		cp.add(canvas, BorderLayout.CENTER);
		
		JColorArea fg = new JColorArea(Color.BLACK);
		JColorArea bg = new JColorArea(Color.WHITE);
		cp.add(createToolbar(fg, bg), BorderLayout.PAGE_START);

		ColorLabel status = new ColorLabel(fg, bg);
		cp.add(status, BorderLayout.PAGE_END);
		
		setUpObjectList();
		cp.add(new JScrollPane(objectList), BorderLayout.LINE_END);
	}

	/**
	 * Creates a {@link JMenuBar}, fills it with actions and sets it
	 * to JVDraw {@link JFrame}.
	 */
	private void createMenuBar() {
		JMenuBar bar = new JMenuBar();
		JMenu file = new JMenu("File");
		bar.add(file);

		this.load.putValue(Action.NAME, "Open");
		this.load.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("control O"));
		this.load.setEnabled(true);
		JMenuItem load = new JMenuItem(this.load);
		file.add(load);
		
		this.save.putValue(Action.NAME, "Save");
		this.save.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("control S"));
		this.save.setEnabled(false);
		JMenuItem save = new JMenuItem(this.save);
		file.add(save);
		
		this.saveAs.putValue(Action.NAME, "Save as");
		this.saveAs.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("control shift S"));
		this.saveAs.setEnabled(false);
		JMenuItem saveAsItem = new JMenuItem(this.saveAs);
		file.add(saveAsItem);
		file.addSeparator();
		
		this.export.putValue(Action.NAME, "Export");
		this.export.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("control E"));
		this.export.setEnabled(false);
		JMenuItem export = new JMenuItem(this.export);
		file.add(export);
		file.addSeparator();
		
		this.exit.putValue(Action.NAME, "Exit");
		this.exit.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("control W"));
		this.exit.setEnabled(true);
		JMenuItem exit = new JMenuItem(this.exit);
		file.add(exit);
				
		setJMenuBar(bar);
	}

	/**
	 * Creates a {@link JToolBar} providing the user with tools to choose
	 * a foreground and a background color, and one of the 3 mutually exclusive
	 * tools: drawing a line, drawing a circle and drawing a filled circle.
	 * 
	 * @param fg a {@link JColorArea} determining the foreground
	 * @param bg a {@link JColorArea} determining the background
	 * @return the created {@link JToolBar}
	 */
	private JToolBar createToolbar(JColorArea fg, JColorArea bg) {
		JToolBar jtb = new JToolBar();
		jtb.setLayout(new FlowLayout(FlowLayout.LEFT));
		jtb.add(fg);
		jtb.add(bg);
		jtb.addSeparator();
		
		LineTool line = new LineTool(dm, fg, canvas);
		line.addActionListener((e)->this.currentTool = line);
		EmptyCircleTool circle = new EmptyCircleTool(dm, fg, canvas);
		circle.addActionListener((e)->this.currentTool = circle);
		FilledCircleTool fCircle = new FilledCircleTool(dm, fg, bg, canvas);
		fCircle.addActionListener((e)->this.currentTool = fCircle);
		
		ButtonGroup toolGroup = new ButtonGroup();
		toolGroup.add(line);
		toolGroup.add(circle);
		toolGroup.add(fCircle);
		line.setSelected(true);
		currentTool = line;
		
		jtb.add(line);
		jtb.add(circle);
		jtb.add(fCircle);
		return jtb;
	}
	
	/**
	 * Creates and initializes the {@link #objectList} {@link JList},
	 * registering a {@link MouseListener} and a {@link KeyListener}
	 * to it.
	 */
	private void setUpObjectList() {
		objectList = new JList<GeometricalObject>(new DrawingObjectListModel(dm));
		objectList.setPreferredSize(new Dimension(200, 500));
		objectList.setBackground(new Color(235, 235, 235));
		
		objectList.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if(e.getClickCount()==2) {
					editGeometricalObject(objectList.getSelectedValue());
				}
			}
		});
		objectList.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if(objectList.hasFocus()) {
					handleKeyPressed(e);
				}
			}
		});
	}
	
	/**
	 * Handles the editing of the given {@link GeometricalObject} 
	 * @param obj the {@link GeometricalObject} to edit
	 */
	private void editGeometricalObject(GeometricalObject obj) {
		GeometricalObjectEditor goe = obj.createGeometricalObjectEditor();
		if(JOptionPane.showConfirmDialog(this, goe, "Change object", 
				JOptionPane.OK_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE)== JOptionPane.OK_OPTION) {
			try {
				goe.checkEditing();
				goe.acceptEditing();
			} catch (IllegalArgumentException ex) {
				JOptionPane.showMessageDialog(this, ex.getMessage());
			}
		}
	}

	/**
	 * Method handling a key being pressed when the {@link JList}
	 * had focus. The only keys with functionality are: <ul>
	 * 	<li> <b> + (plus) </b> - brings the object to front </li>
	 * 	<li> <b> - (minus) </b> - pushes the object back </li>
	 * 	<li> <b> DEL </b> - deletes the object </li> </ul?>
	 * @param e
	 */
	private void handleKeyPressed(KeyEvent e) {
		GeometricalObject obj = objectList.getSelectedValue();
		if(obj==null) {
			return;
		}
		int key = e.getKeyCode();
		if(key==KeyEvent.VK_DELETE) {
			dm.remove(obj);
		} else if(key==KeyEvent.VK_PLUS || key==KeyEvent.VK_ADD) {
			dm.changeOrder(obj, 1);
			objectList.setSelectedValue(obj, true);
		} else if(key==KeyEvent.VK_MINUS || key==KeyEvent.VK_SUBTRACT) {
			dm.changeOrder(obj, -1);
			objectList.setSelectedValue(obj, true);
		}
	}

	/**
	 * The entry point of the program
	 * @param args command-line arguments: not used
	 */
	public static void main(String[] args) {
		SwingUtilities.invokeLater(()->{
			new JVDraw().setVisible(true);
		});
	}
}
