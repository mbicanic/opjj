package hr.fer.zemris.java.hw16.jvdraw.objects;

import java.awt.Color;
import java.awt.GridLayout;
import java.awt.Point;

import javax.swing.JLabel;
import javax.swing.JTextField;

import hr.fer.zemris.java.hw16.jvdraw.components.JColorArea;
import hr.fer.zemris.java.hw16.jvdraw.objects.visitor.GeometricalObjectVisitor;

/**
 * Circle is a specification of a {@link GeometricalObject} representing
 * a circle in 2-dimensional space. <br>
 * <br>
 * A Circle is fully defined with its rim color, center {@link Point} and 
 * radius.
 * 
 * @author Miroslav Bićanić
 */
public class Circle extends GeometricalObject {
	protected Point center;
	protected int radius;
	protected Color rimColor;
	
	/**
	 * Default constructor for a Circle
	 */
	public Circle() {}
	
	/**
	 * A constructor for a Circle
	 * @param center the center {@link Point} of the circle
	 * @param radius the radius of the circle
	 * @param rimColor the {@link Color} of the rim of the circle
	 */
	public Circle(Point center, int radius, Color rimColor) {
		this.center = center;
		this.radius = radius;
		this.rimColor = rimColor;
	}
	
	/**
	 * @return the center {@link Point} of this Circle
	 */
	public Point getCenter() {
		return center;
	}
	/**
	 * @param center the center {@link Point} of this Circle
	 */
	public void setCenter(Point center) {
		this.center = center;
	}
	/**
	 * @return the radius of this Circle
	 */
	public int getRadius() {
		return radius;
	}
	/**
	 * @param radius the radius of this Circle
	 */
	public void setRadius(int radius) {
		this.radius = radius;
	}
	/**
	 * @return the rim {@link Color} of this Circle
	 */
	public Color getRimColor() {
		return rimColor;
	}
	/**
	 * @param rimColor the rim {@link Color} of this Circle
	 */
	public void setRimColor(Color rimColor) {
		this.rimColor = rimColor;
	}
	
	/**
	 * Returns a string representation of the circle, to be used in
	 * the list of drawn objects.
	 */
	@Override
	public String toString() {
		return "Circle "+formatPoint(center)+", "+radius;
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public void accept(GeometricalObjectVisitor v) {
		v.visit(this);
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public GeometricalObjectEditor createGeometricalObjectEditor() {
		return new EmptyCircleEditor(this);
	}
	
	/**
	 * EmptyCircleEditor is a specifiation of a {@link GeometricalObjectEditor},
	 * providing the user with a visual interface to change properties of an 
	 * empty {@link Circle}.
	 * 
	 * @author Miroslav Bićanić
	 */
	private static class EmptyCircleEditor extends GeometricalObjectEditor {
		private static final long serialVersionUID = 1L;
		
		/** The {@link Circle} that is being edited by this EmptyCircleEditor */
		private Circle circle;
		/** A {@link JTextField} to edit the center {@link Point}'s X coordinate */
		private JTextField centerX;
		/** A {@link JTextField} to edit the center {@link Point}'s Y coordinate */
		private JTextField centerY;
		/** A {@link JTextField} to edit the radius of the {@link #circle} */
		private JTextField radius;
		/** A {@link JColorArea} to edit the rim {@link Color} of the {@link #circle} */
		private JColorArea foreground;
		
		/**
		 * Constructor for an EmptyCircleEditor
		 * @param c the {@link Circle} this EmptyCircleEditor will edit
		 */
		public EmptyCircleEditor(Circle c) {
			this.circle = c;
			setLayout(new GridLayout(4, 2));
			centerX = new JTextField();
			centerY = new JTextField();
			radius = new JTextField();
			foreground = new JColorArea(circle.getRimColor());
			initPanelGUI();
		}

		/**
		 * Initializes the GUI components of the LineEditor.
		 */
		private void initPanelGUI() {
			centerX.setText(Integer.valueOf(circle.getCenter().x).toString());
			centerY.setText(Integer.valueOf(circle.getCenter().y).toString());
			radius.setText(Integer.valueOf(circle.getRadius()).toString());
			
			add(new JLabel("Start X:"));
			add(centerX);
			add(new JLabel("Start Y:"));
			add(centerY);
			add(new JLabel("Radius:"));
			add(radius);
			
			add(new JLabel("Rim color:"));
			add(foreground);
		}
		
		/**
		 * {@inheritDoc}
		 */
		@Override
		public void checkEditing() {
			test(centerX.getText(), "Center X");
			test(centerY.getText(), "Center Y");
			int rad = test(radius.getText(), "Radius");
			if(rad < 0) {
				throw new IllegalArgumentException("Radius cannot be negative.");
			}
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public void acceptEditing() {
			int xC = test(centerX.getText(), "Center X");
			int yC = test(centerY.getText(), "Center Y");
			int rad = test(radius.getText(), "Radius");
			
			circle.setCenter(new Point(xC, yC));
			circle.setRadius(rad);
			circle.setRimColor(foreground.getCurrentColor());
			circle.notifyChanged();
		}
	}
}
