package hr.fer.zemris.java.hw16.jvdraw.tools;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.event.MouseEvent;

import hr.fer.zemris.java.hw16.jvdraw.components.IColorProvider;
import hr.fer.zemris.java.hw16.jvdraw.components.JDrawingCanvas;
import hr.fer.zemris.java.hw16.jvdraw.model.DrawingModel;
import hr.fer.zemris.java.hw16.jvdraw.objects.FilledCircle;

/**
 * FilledCircleTool is a concrete implementation of an {@link AbstractTool}
 * used for drawing filled circles during the user's creation of a filled circle
 * (between the first and the second click).
 * 
 * @author Miroslav Bićanić
 */
public class FilledCircleTool extends AbstractTool {
	private static final long serialVersionUID = 1L;
	/**
	 * An {@link IColorProvider} providing with the background {@link Color}
	 */
	private IColorProvider bgColor;
	
	/**
	 * Constructor for an EmptyCircleTool
	 * @param dm the underlying {@link DrawingModel}
	 * @param fg the {@link IColorProvider} providing with the foreground {@link Color}
	 * @param bg the {@link IColorProvider} providing with the background {@link Color}
	 * @param canvas the {@link JDrawingCanvas} on which to paint
	 */
	public FilledCircleTool(DrawingModel dm, IColorProvider fg, IColorProvider bg, JDrawingCanvas canvas) {
		super(dm, fg, canvas, "Filled circle", new Dimension(80, 20));
		this.bgColor = bg;
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void onFirstClick(MouseEvent e) {
		FilledCircle fc = new FilledCircle();
		fc.setRimColor(fgColor.getCurrentColor());
		fc.setInsideColor(bgColor.getCurrentColor());
		fc.setCenter(e.getPoint());
		fc.setRadius(0);
		this.object = fc;
	}
	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void onSecondClick(MouseEvent e) {
		FilledCircle fc = (FilledCircle)this.object;
		fc.setRadius(calculateRadius(fc.getCenter(), e.getPoint()));
		dm.add(fc);
		this.object = null;
	}
	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void onMove(MouseEvent e) {
		((FilledCircle)this.object).setRadius(calculateRadius(
				((FilledCircle)this.object).getCenter(), e.getPoint()));
		canvas.repaint();
	}

	/**
	 * Calculates the radius of the circle using the fixed {@code center} 
	 * {@link Point} and its distance to the {@code point} {@link Point},
	 * which is usually the position of the mouse.
	 * 
	 * @param center the center {@link Point} of the circle
	 * @param point any other {@link Point}
	 * @return the radius of the circle
	 */
	private int calculateRadius(Point center, Point point) {
		return (int)Math.round(Point.distance(center.getX(), center.getY(), point.getX(), point.getY()));
	}
}
