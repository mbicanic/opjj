package hr.fer.zemris.java.hw17.trazilica;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import hr.fer.zemris.java.hw17.trazilica.commands.Command;
import hr.fer.zemris.java.hw17.trazilica.models.DocumentVector;
import hr.fer.zemris.java.hw17.trazilica.models.RangList;
import hr.fer.zemris.java.hw17.trazilica.models.Vocabulary;

/**
 * Environment is a class modelling the environment of the 
 * {@link Konzola} program.<br>
 * 
 * The environment is defined with: <ul>
 * 	<li> A {@link Vocabulary} storing all words the program recognizes </li>
 *	<li> A {@link DocumentVector} object for each document in the file base </li>
 *	<li> An IDF {@link DocumentVector} used for calculating similarities </li>
 *	</ul>
 *
 * In order to use the Environment, {@link Command} objects must be 
 * registered to it while mapping them to a name to make them accessible.
 * 
 * The Environment also stores the results of the last executed query.
 * 
 * @author Miroslav Bićanić
 */
public class Environment {
	/** The {@link Vocabulary} used by the program */
	private Vocabulary vocab;
	/** The {@link List} of {@link DocumentVector} objects representing each document */
	private List<DocumentVector> vectors;
	/** The IDF {@link DocumentVector} */
	private DocumentVector IDFVector;
	/** The results of the last executed query, stored as a {@link RangList} */
	private RangList queryResults;
	
	/** A {@link Map} mapping {@link Command} objects to their names */
	private Map<String, Command> commands;
	
	/**
	 * Constructor for an Environment
	 * @param vocab the {@link Vocabulary} of the program
	 * @param docs the {@link List} of {@link DocumentVector}
	 * @param IDF the inverse-document-frequency {@link DocumentVector}
	 */
	public Environment(Vocabulary vocab, List<DocumentVector> docs, DocumentVector IDF) {
		this.vocab = vocab;
		this.vectors = docs;
		this.IDFVector = IDF;
		this.commands = new HashMap<>();
	}

	/**
	 * @return the {@link Vocabulary} of the program
	 */
	public Vocabulary getVocab() {
		return vocab;
	}

	/**
	 * @return the {@link List} of {@link DocumentVector} objects
	 */
	public List<DocumentVector> getVectors() {
		return vectors;
	}

	/**
	 * @return the IDF {@link DocumentVector}
	 */
	public DocumentVector getIDFVector() {
		return IDFVector;
	}

	/**
	 * @return the {@link RangList} representing the last query results
	 */
	public RangList getQueryResults() {
		return queryResults;
	}

	/**
	 * @param init the {@link RangList} representing the last query results
	 */
	public void setQueryResults(RangList init) {
		this.queryResults = init;
	}
	
	/**
	 * Adds the {@link Command} {@code c} into this Environment's
	 * map of commands, storing it using the given {@code name} 
	 * as the key.
	 * @param name the name of the {@link Command}, used as a key
	 * @param c the {@link Command} to register
	 */
	public void registerCommand(String name, Command c) {
		this.commands.put(name, c);
	}
	
	/**
	 * @param name the name of the {@link Command} to look for
	 * @return the {@link Command} stored under the given name; 
	 * 			<code>null</code> if such a {@link Command} doesn't exist
	 */
	public Command getCommand(String name) {
		return this.commands.get(name);
	}
}
