package hr.fer.zemris.java.hw17.trazilica.init;

import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import hr.fer.zemris.java.hw17.trazilica.models.DocumentVector;
import hr.fer.zemris.java.hw17.trazilica.models.Vocabulary;
import hr.fer.zemris.java.hw17.trazilica.util.WordExtractor;

/**
 * VectorVisitor is a specification of a {@link SimpleFileVisitor}
 * which builds a {@link DocumentVector} object for each visited 
 * file and an additional IDF {@link DocumentVector}, which for each
 * word counts the number of documents it appeared in.
 * 
 * @author Miroslav Bićanić
 */
public class VectorVisitor extends SimpleFileVisitor<Path> {
	/** The already built {@link Vocabulary} */
	private Vocabulary vocabulary;
	
	/** 
	 * The {@link List} of {@link DocumentVector} objects to which
	 * a new element is added for each visited file.
	 */
	private List<DocumentVector> vectors;
	/** The IDF {@link DocumentVector} */
	private DocumentVector IDF;
	
	/**
	 * Constructor for a VectorVisitor
	 * @param vocab the built Vocabulary containing all words
	 */
	public VectorVisitor(Vocabulary vocab) {
		this.vectors = new ArrayList<>();
		this.vocabulary = vocab;
		this.IDF = new DocumentVector(vocabulary.createBlankMap());
	}
	
	/**
	 * @return the IDF {@link DocumentVector} built in the process of
	 * 			visiting the files
	 */
	public DocumentVector getIDF() {
		return IDF;
	}
	
	/**
	 * @return the {@link List} of {@link DocumentVector} objects
	 * 			representing each visited file
	 */
	public List<DocumentVector> getVectors() {
		for(DocumentVector dv : vectors) {
			dv.transformByIDF(IDF);
		}
		return vectors;
	}
	
	/**
	 * Extracts all words from the visited file, builds a {@link DocumentVector}
	 * out of them and updates the IDF {@link DocumentVector}.
	 */
	@Override
	public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
		WordExtractor ext = new WordExtractor(file, vocabulary.getStopwords());
		Set<String> wordSet = ext.getWordset();
		for(String word : wordSet) {
			IDF.incrementComponent(word);
		}
		this.vectors.add(new DocumentVector(file, ext.getWordlist(), this.vocabulary));
		return FileVisitResult.CONTINUE;
	}
}
