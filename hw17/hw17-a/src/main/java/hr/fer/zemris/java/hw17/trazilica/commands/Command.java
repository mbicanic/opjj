package hr.fer.zemris.java.hw17.trazilica.commands;

import hr.fer.zemris.java.hw17.trazilica.Environment;

/**
 * Command is an interface for representing commands which
 * can execute themselves given an {@link Environment} in
 * which to execute.
 * 
 * @author Miroslav Bićanić
 */
public interface Command {
	/**
	 * Executes the command in the given {@link Environment} using
	 * the given {@code args} as parameters for its execution.
	 * @param env the {@link Environment} in which to execute the command
	 * @param args the arguments for the command
	 */
	public void executeCommand(Environment env, String...args);
}
