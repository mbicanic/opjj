package hr.fer.zemris.java.hw17.trazilica.init;

import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.Set;

import hr.fer.zemris.java.hw17.trazilica.models.Vocabulary;
import hr.fer.zemris.java.hw17.trazilica.util.WordExtractor;

/**
 * VocabularyVisitor is a specification of a {@link SimpleFileVisitor}
 * which builds a vocabulary from the contents of the visited files.
 * 
 * @author Miroslav Bićanić
 */
public class VocabularyVisitor extends SimpleFileVisitor<Path> {
	/** The {@link Vocabulary} being built */
	private Vocabulary vocabulary;
	
	/**
	 * Constructor for a VocabularyVisitor
	 * @param stopwords a {@link Set} of words to be ignored
	 */
	public VocabularyVisitor(Set<String> stopwords) {
		this.vocabulary = new Vocabulary(stopwords);
	}
	
	/**
	 * @return the built {@link Vocabulary}
	 */
	public Vocabulary getVocabulary(){
		return vocabulary;
	}
	
	/**
	 * Extracts all the words from the visited file and adds them
	 * to the {@link #vocabulary}
	 */
	@Override
	public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
		WordExtractor ext = new WordExtractor(file, vocabulary.getStopwords());
		vocabulary.addAll(ext.getWordset());
		return FileVisitResult.CONTINUE;
	}
}
