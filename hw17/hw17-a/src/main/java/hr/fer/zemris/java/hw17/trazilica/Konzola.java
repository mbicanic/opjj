package hr.fer.zemris.java.hw17.trazilica;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Scanner;
import java.util.Set;

import hr.fer.zemris.java.hw17.trazilica.commands.Command;
import hr.fer.zemris.java.hw17.trazilica.commands.QueryCommand;
import hr.fer.zemris.java.hw17.trazilica.commands.ResultCommand;
import hr.fer.zemris.java.hw17.trazilica.commands.TypeCommand;
import hr.fer.zemris.java.hw17.trazilica.init.VectorVisitor;
import hr.fer.zemris.java.hw17.trazilica.init.VocabularyVisitor;
import hr.fer.zemris.java.hw17.trazilica.models.DocumentVector;
import hr.fer.zemris.java.hw17.trazilica.models.Vocabulary;

/**
 * Konzola is a simple program simulating a search engine.
 * <br>
 * In order to correctly run, a path to a directory which
 * contains the document base (or further directories, in
 * which the document base is located) must be given to the
 * program at startup.
 * <br><br>
 * The program supports 4 commands: <ul>
 * <li> {@code query} - takes an arbitrary number of arguments.
 * 		Performs a query using the given arguments as keywords
 * 		for the search. The 10 best results are output to the
 * 		standard out at the end of this command's execution </li>
 * <li> {@code type} - takes exactly one argument: an index.
 * 		Outputs the content of the index-th file in the list
 * 		of files output by the most recently executed query
 * 		command </li>
 * <li> {@code results} - takes no arguments.
 * 		Outputs the results of the last executed query in the
 * 		same format as the original query did it. </li>
 * <li> {@code exit} - terminates the application. </li> </ul>
 * 
 * @author Miroslav Bićanić
 */
public class Konzola {
	
	/**
	 * Entry point of the program, in which the arguments are
	 * checked and the {@link Environment} is initialized.
	 * @param args one argument: a path to a directory containing the document base
	 */
	public static void main(String[] args) {
		if(args.length!=1) {
			System.err.println("Očekivao sam stazu do direktorija sa dokumentima.");
			return;
		}
		Path directory = Paths.get(args[0]);
		if(!Files.exists(directory) || !Files.isDirectory(directory)) {
			System.err.println("Predana staza nije ispravna.");
			return;
		}

		Environment env = setUpEnvironment(directory);
		if(env==null) {
			return;
		}
		env.registerCommand("query", new QueryCommand());
		env.registerCommand("type", new TypeCommand());
		env.registerCommand("results", new ResultCommand());
		
		mainLoop(env);
	}

	/**
	 * Creates and initializes an Environment for the execution
	 * of the program.
	 * 
	 * @param directory the directory containing the document base
	 * @return the build {@link Environment}
	 */
	private static Environment setUpEnvironment(Path directory) {
		try{
			List<String> stopwordList = Files.readAllLines(Paths.get("hrvatski_stoprijeci.txt"), StandardCharsets.UTF_8);
			Set<String> stopwords = new HashSet<String>();
			for(String s : stopwordList) {
				if(!s.isEmpty()) {
					stopwords.add(s.trim());
				}
			}
			
			VocabularyVisitor vv = new VocabularyVisitor(stopwords);
			Files.walkFileTree(directory, vv);
			Vocabulary vocabulary = vv.getVocabulary(); 
			System.out.println("Veličina vokabulara: "+vocabulary.size());
			
			VectorVisitor vecVisit = new VectorVisitor(vocabulary);
			Files.walkFileTree(directory, vecVisit);
			DocumentVector IDF = vecVisit.getIDF();
			List<DocumentVector> documents = vecVisit.getVectors();
			return new Environment(vocabulary,documents,IDF);
		} catch(IOException ex) {
			System.err.println("Dogodila se greška prilikom učitavanja dokumenata.");
			return null;
		}
	}
	
	/**
	 * The main loop of the program, handling the execution of commands.
	 * @param env the Environment in which to execute commands
	 */
	private static void mainLoop(Environment env) {
		Scanner sc = new Scanner(System.in);
		while(true) {
			System.out.printf("Unesi naredbu > ");
			String[] params = sc.nextLine().split("\\s+");
			if(params[0].toLowerCase().equals("exit")) {
				break;
			}
			Command c = env.getCommand(params[0]);
			if(c==null) {
				System.out.println("Nepoznata naredba.");
				continue;
			}
			c.executeCommand(env, Arrays.copyOfRange(params, 1, params.length));
		}
		sc.close();
	}
}
