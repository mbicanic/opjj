package hr.fer.zemris.java.hw17.trazilica.models;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Vocabulary is a class extending the functionality of a 
 * {@link HashSet} over the {@link String} domain.
 * 
 * In addition to inherited functionality, it stores the
 * stopwords and provides utility methods related to it.
 * 
 * @author Miroslav Bićanić
 */
public class Vocabulary extends HashSet<String> {
	private static final long serialVersionUID = 1L;
	
	/** The {@link Set} of words that should be ignored */
	private Set<String> stopwords;
	
	/**
	 * A constructor for a Vocabulary
	 * @param stopwords the {@link Set} of words that should be ignored
	 */
	public Vocabulary(Set<String> stopwords) {
		super();
		this.stopwords = stopwords;
	}

	/**
	 * Creates a {@link Map} mapping Strings to Doubles with all
	 * values initialized to 0.
	 * <br>
	 * It is used in the creation of new {@link DocumentVector} 
	 * objects.
	 * 
	 * @return a {@link Map} with the values initialized to 0
	 */
	public Map<String, Double> createBlankMap(){
		Map<String, Double> blank = new HashMap<String, Double>();
		for(String s : this) {
			blank.put(s, 0.);
		}
		return blank;
	}
	
	/**
	 * @return the {@link Set} of words to be ignored
	 */
	public Set<String> getStopwords() {
		return stopwords;
	}
	
	/**
	 * Creates a new {@link List} of words using all the words
	 * from the given {@code data} List of words that are also
	 * present in this Vocabulary.
	 * 
	 * @param data the {@link List} of words to filter
	 * @return a {@link List} containing elements that exist in
	 * 		both the given {@link List} and this Vocabulary
	 */
	public List<String> filterIrrelevant(List<String> data){
		List<String> filtered = new LinkedList<>();
		for(String s : data) {
			String lower = s.toLowerCase();
			if(this.contains(lower)) {
				filtered.add(lower);
			}
		}
		return filtered;
	}
}
