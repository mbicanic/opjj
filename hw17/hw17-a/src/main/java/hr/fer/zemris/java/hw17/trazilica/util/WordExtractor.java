package hr.fer.zemris.java.hw17.trazilica.util;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

/**
 * WordExtractor is a utility class used for extracting words
 * from a file.<br>
 * <br>
 * Any consecutive string of characters for which {@link Character#isAlphabetic(int)}
 * returns {@code true} is considered a word. The word stops at the first character
 * for which the method returns {@code false}, and all such characters are ignored.
 * 
 * @author Miroslav Bićanić
 */
public class WordExtractor {
	/** The array of characters in the document */
	private char[] data;
	/** The position of the next character to read from the array */
	private int index;
	
	/** A {@link Set} of words that should be ignored */
	private Set<String> stopwords;
	/** A {@link List} of words found in the document */
	private List<String> wordList;
	/** A {@link Set} of words found in the document */
	private Set<String> wordSet;
	
	/**
	 * Constructor for a WordExtractor
	 * @param file the source file whose contents to read
	 * @param stopwords the words that should be ignored while reading
	 */
	public WordExtractor(Path file, Set<String> stopwords) {
		try {
			this.data = Files.readString(file, StandardCharsets.UTF_8).toCharArray();
		} catch(IOException ex) {
			throw new RuntimeException(ex);
		}
		this.index = 0;
		this.stopwords = stopwords;
		this.wordList = new LinkedList<>();
		this.wordSet = new HashSet<>();
		parse();
	}
	
	/**
	 * @return the {@link List} of extracted words
	 */
	public List<String> getWordlist() {
		return wordList;
	}
	/**
	 * @return the {@link Set} of extracted words
	 */
	public Set<String> getWordset() {
		return wordSet;
	}

	/**
	 * Parses the characters from the array one by one, skipping
	 * non-alphabetic ones, and building words from sequences of
	 * alphabetic ones.
	 */
	private void parse() {
		skipNonalphabetic();
		while(index < data.length) {
			if(Character.isAlphabetic(data[index])) {
				buildWord();
			} else {
				skipNonalphabetic();
			}
		}
	}

	/**
	 * Appends characters from the array as long as the character
	 * is alphabetic.<br>
	 * Once a non-alphabetic character is found, the word is built.
	 * If the {@link #stopwords} set does not contain the built word,
	 * the word is added to both the list and the set of words.
	 */
	private void buildWord() {
		StringBuilder sb = new StringBuilder(10);
		while(index < data.length && Character.isAlphabetic(data[index])) {
			sb.append(data[index++]);
		}
		String result = sb.toString().toLowerCase();
		if(stopwords.contains(result)) {
			return;
		}
		wordList.add(result);
		wordSet.add(result);
	}

	/**
	 * Skips non-alphabetic characters in the array.
	 */
	private void skipNonalphabetic() {
		while(index < data.length && !Character.isAlphabetic(data[index])) {
			index++;
		}
	}
}
