package hr.fer.zemris.java.hw17.trazilica.models;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;;

/**
 * RangList is a specification of an {@link ArrayList}, containing
 * {@link Map.Entry} objects as elements, with each object mapping
 * a {@link DocumentVector} to a double number.
 * 
 * When adding entries to the RangList, the number mapped to the
 * {@link DocumentVector} must be positive. Otherwise, the entry
 * is ignored as it does not contribute to the RangList.
 * 
 * @author Miroslav Bićanić
 */
public class RangList extends ArrayList<Map.Entry<DocumentVector, Double>> {
	private static final long serialVersionUID = 1L;
	
	/**
	 * Adds the given {@link DocumentVector} to the RangList as a single
	 * element, paired with the given {@code value}.
	 * 
	 * If the value is equal to zero, it is ignored and not added to
	 * the RangList.
	 * 
	 * @param dv the {@link DocumentVector} for which the entry is made
	 * @param value the value to assign to the given document vector
	 */
	public void addResult(DocumentVector dv, Double value){
		if((double)value <= 0.0) {
			return;
		}
		this.add(new Entry(dv, value));
	}
	
	/**
	 * Returns a RangList containing only the 10 entries from this RangList
	 * that have the largest number paired to them, sorted in descending order.
	 * 
	 * The returned RangList may contain less than 10 values if there isn't
	 * a sufficient amount of entries with a positive number assigned.
	 * 
	 * @return a new RangList containing the 10 best elements
	 */
	public RangList getTenBest(){
		List<Map.Entry<DocumentVector,Double>> copy = new ArrayList<>();
		for(Map.Entry<DocumentVector,Double> e : this) {
			copy.add(e);
		}
		Collections.sort(copy, (e1, e2) -> {
			if(e1.getValue() > e2.getValue()) {
				return -1;
			} else if(e1.getValue() < e2.getValue()) {
				return 1;
			} else {
				return 0;
			}
		});
		RangList bestTen = new RangList();
		for(int i=0, n=copy.size(); i<10 && i<n; i++) {
			bestTen.addResult(copy.get(i).getKey(), copy.get(i).getValue());
		}
		return bestTen;
	}
	
	/**
	 * Outputs all entries of the RangList to the standard out in
	 * the format:
	 * <code> [position] (value) filename </code>
	 */
	public void outputResults() {
		int i=0;
		for(Map.Entry<DocumentVector,Double> e : this) {
			System.out.printf("[%d] (%.4f) %s%n", 
					i, 
					e.getValue(), 
					e.getKey().getFilePath());
			i++;
		}
	}
	
	/**
	 * Entry is a simple helper class representing a {@link Map.Entry}.
	 * 
	 * @author Miroslav Bićanić
	 */
	private static class Entry implements Map.Entry<DocumentVector, Double> {
		private DocumentVector key;
		private Double value;
		
		public Entry(DocumentVector key, Double value) {
			this.key = key;
			this.value = value;
		}
		
		@Override
		public DocumentVector getKey() {
			return key;
		}

		@Override
		public Double getValue() {
			return value;
		}

		@Override
		public Double setValue(Double value) {
			throw new UnsupportedOperationException();
		}
	}
}
