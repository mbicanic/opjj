package hr.fer.zemris.java.hw17.trazilica.commands;

import hr.fer.zemris.java.hw17.trazilica.Environment;
import hr.fer.zemris.java.hw17.trazilica.models.DocumentVector;

/**
 * TypeCommand is an implementation of a {@link Command}
 * which outputs the contents of a file into the standard
 * out.
 * 
 * It expects exactly one argument - the index of the file 
 * whose contents to output. The index is relative to the 
 * list of files output by the last query command.
 * 
 * If no argument is given, or it's given but invalid, or
 * given but out of range, or no query has been executed
 * prior to calling this command, an error message is 
 * output and execution is returned to the main program.
 * 
 * @author Miroslav Bićanić
 */
public class TypeCommand implements Command {

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void executeCommand(Environment env, String... args) {
		if(args.length!=1) {
			System.out.println("Ispis [type] očekuje jedan indeks kao argument.");
			return;
		}
		int value;
		try {
			value = Integer.parseInt(args[0]);
		} catch(NumberFormatException ex) {
			System.out.println("Neispravna vrsta argumenta za [type].");
			return;
		}
		if(env.getQueryResults()==null) {
			System.out.println("Nemoguće pozvati [type] prije nego se izvrši neki upit [query].");
			return;
		}
		if(value<0 || value>=env.getQueryResults().size()) {
			System.out.println("Indeks je izvan valjanog raspona.");
			return;
		}
		DocumentVector dv = env.getQueryResults().get(value).getKey();
		
		System.out.println("----------------------------------------------");
		System.out.println(dv.getFilePath());
		System.out.println("----------------------------------------------");
		System.out.println(dv.getFileContent());
		System.out.println("----------------------------------------------");
	}

}
