package hr.fer.zemris.java.hw17.trazilica.commands;

import hr.fer.zemris.java.hw17.trazilica.Environment;

/**
 * ResultCommand is an implementation of a {@link Command}
 * which tries to output the results of the last executed
 * query exactly like the original query output them.
 * 
 * In case it's called before any query is performed, it
 * outputs an error message and returns the execution to
 * the main program.
 * 
 * @author Miroslav Bićanić
 */
public class ResultCommand implements Command {

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void executeCommand(Environment env, String... args) {
		if(args.length!=0) {
			System.out.println("Naredba [results] ne očekuje nikakve argumente.");
			return;
		}
		if(env.getQueryResults()==null) {
			System.out.println("Nemoguće pozvati [type] prije nego se izvrši neki upit [query].");
			return;
		}
		env.getQueryResults().outputResults();
	}
}
