package hr.fer.zemris.java.hw17.trazilica.models;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.Map;

/**
 * DocumentVector is a class modelling a document as a bag of words.
 * 
 * The vector has as many dimensions as there are words in the 
 * {@link Vocabulary}, so the vector components in each dimension
 * of the DocumentVector represent some function of frequency of
 * the word in that dimension.
 * 
 * @author Miroslav Bićanić
 */
public class DocumentVector {
	/** The {@link Path} associated with the DocumentVector */
	private Path document;
	/** The vector carrying the vector components in each dimension */
	private Map<String, Double> vector;
	
	/**
	 * A constructor for a DocumentVector
	 * @param p the {@link Path} to the source file
	 * @param wordsInFile a {@link List} of all words in the file
	 * @param vocabulary the {@link Vocabulary} containing all words
	 */
	public DocumentVector(Path p, List<String> wordsInFile, Vocabulary vocabulary) {
		this.document = p;
		this.vector = vocabulary.createBlankMap();
		for(String s : wordsInFile) {
			vector.compute(s, (k,v)->vector.get(k)+1);
		}
	}
	
	/**
	 * Constructor for a DocumentVector that is not associated
	 * to any path (such as the IDF vector).
	 * 
	 * @param initVector the initialization {@link Map} to be used as 
	 * 		the underlying vector
	 */
	public DocumentVector(Map<String, Double> initVector) {
		this.vector = initVector;
	}
	
	/**
	 * If the DocumentVector has a {@link Path} associated to it,
	 * this method returns the full path as a string.
	 * 
	 * @return the full path to the document represented by this DocumentVector
	 * @throws UnsupportedOperationException if this DocumentVector doesn't have a file
	 */
	public String getFilePath() {
		if(document==null) {
			throw new UnsupportedOperationException("No file associated with this DocumentVector.");
		}
		return document.toAbsolutePath().toString();
	}
	
	/**
	 * If the DocumentVector has a {@link Path} associated to it,
	 * this method returns the whole contents of the file as a single
	 * string.
	 * 
	 * @return the contents of the document represented by this DocumentVector
	 * @throws UnsupportedOperationException if this DocumentVector doesn't have a file
	 */
	public String getFileContent() {
		if(document==null) {
			throw new UnsupportedOperationException("No file associated with this DocumentVector.");
		}
		try {
			return Files.readString(document, StandardCharsets.UTF_8);
		} catch (IOException ex) {
			throw new RuntimeException(ex);
		}
	}
	
	/**
	 * Returns the value of the vector component for the given
	 * {@code component}.
	 * @param component the word whose vector component to return
	 * @return the value of the vector component
	 */
	public Double getComponent(String component) {
		return vector.get(component);
	}

	/**
	 * Sets the vector component {@code component}'s value to the
	 * given {@code value}
	 * @param component the component whose value to set
	 * @param value the value to set to the component
	 */
	public void setComponent(String component, Double value) {
		if(vector.containsKey(component)) {
			vector.put(component, value);
		}
	}
	
	/**
	 * Increments the vector component {@code component} by 1.
	 * Useful if the DocumentVector is representing a frequency.
	 * 
	 * @param component the component whose value to increment by 1
	 */
	public void incrementComponent(String component) {
		if(vector.containsKey(component)) {
			vector.compute(component, (k,v)->vector.get(k)+1);
		}
	}
	
	/**
	 * Computes and returns the norm of the DocumentVector.
	 * @return the norm of the vector
	 */
	public Double norm() {
		double res = 0;
		for(String s : vector.keySet()) {
			res += Math.pow(vector.get(s), 2);
		}
		return Math.sqrt(res);
	}
	
	/**
	 * Performs a dot-product operation of this vector and the
	 * given {@code other} vector.
	 * 
	 * @param other the {@link DocumentVector} with which to perform the dot-product
	 * @return the {@link Double} result of the dot-product operation
	 */
	public Double dot(DocumentVector other) {
		double res = 0;
		for(String s : vector.keySet()) {
			res += this.vector.get(s) * other.getComponent(s);
		}
		return res;
	}
	
	/**
	 * Calculates and returns the similarity between this DocumentVector
	 * and the given {@code other} DocumentVector.
	 * <br>
	 * The similarity is calculated as the cosine between these two
	 * vectors in n-dimensional space, where n is the size of the
	 * vocabulary.
	 * 
	 * @param other the vector with which to compare this vector
	 * @return the similarity between the two vectors as a {@link Double} number
	 */
	public Double similarity(DocumentVector other) {
		return this.dot(other) / (this.norm() * other.norm());
	}
	
	/**
	 * If this DocumentVector currently represents documents as a 
	 * frequency vector, with each word being paired to a number 
	 * of appearances in the document, this method transforms it
	 * to a TF-IDF vector using the given IDF DocumentVector.
	 * 
	 * @param idf the Inverse-Document-Frequency vector of the document base
	 */
	public void transformByIDF(DocumentVector idf) {
		for(String s : vector.keySet()) {
			double idfValue = Math.log10((double)vector.keySet().size() / idf.getComponent(s));
			vector.compute(s, (k,v)->vector.get(k)*idfValue);
		}
	}
}
