package hr.fer.zemris.java.hw17.trazilica.commands;

import java.util.Arrays;
import java.util.List;

import hr.fer.zemris.java.hw17.trazilica.Environment;
import hr.fer.zemris.java.hw17.trazilica.models.DocumentVector;
import hr.fer.zemris.java.hw17.trazilica.models.RangList;

/**
 * QueryCommand is an implementation of a {@link Command}
 * which performs a query for most similar documents using
 * the given arguments as keywords to look for in the 
 * document.
 * 
 * Once the query is finished, the results are output to
 * the standard out.
 * 
 * @author Miroslav Bićanić
 */
public class QueryCommand implements Command {

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void executeCommand(Environment env, String... args) {
		List<String> keywords = env.getVocab().filterIrrelevant(Arrays.asList(args));
		if(keywords.size()==0) {
			System.out.println("Upit ne sadrži riječi iz rječnika.");
			return;
		}
		System.out.printf("Upit je: [");
		System.out.printf("%s", String.join(", ", keywords));
		System.out.println("]\nNajboljih 10 rezultata: ");
		
		DocumentVector queryVector = new DocumentVector(env.getVocab().createBlankMap());
		for(String s : keywords) {
			queryVector.incrementComponent(s);
		}
		queryVector.transformByIDF(env.getIDFVector());
		
		RangList list = new RangList();
		for(DocumentVector dv : env.getVectors()) {
			list.addResult(dv, dv.similarity(queryVector));
		}
		env.setQueryResults(list.getTenBest());
		
		list.getTenBest().outputResults();
	}
}
