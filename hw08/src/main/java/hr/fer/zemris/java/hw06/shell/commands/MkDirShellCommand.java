package hr.fer.zemris.java.hw06.shell.commands;

import static hr.fer.zemris.java.hw06.shell.commands.Utility.*;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.List;

import hr.fer.zemris.java.hw06.shell.Environment;
import hr.fer.zemris.java.hw06.shell.ShellCommand;
import hr.fer.zemris.java.hw06.shell.ShellStatus;

/**
 * MkDirShellCommand is a command that creates one or multiple
 * directories in the file system.
 * 
 * To correctly execute, it needs exactly one argument - a path
 * to a non-existant directory that will be created by this
 * command.
 * 
 * Giving it any other number of arguments or an illegal argument
 * (such as an existing folder) stops the execution of the command
 * and returns the control back to the Shell that called it.
 * 
 * Correct example showing the syntax:
 * 	{@code mkdir "C:\Existing Folder\New Folder Within It"}
 * 
 * @author Miroslav Bićanić
 */
public class MkDirShellCommand implements ShellCommand {

	/** Name of the MkDirShellCommand */
	private static final String MKDIR_CMD = "mkdir";
	
	/**
	 * {@inheritDoc}
	 * @param arguments One valid path to a non-existant directory
	 * @return Always returns ShellStatus.CONTINUE
	 */
	@Override
	public ShellStatus executeCommand(Environment env, String arguments) {
		try {
			ArgumentParser parser = new ArgumentParser(arguments, env, MKDIR_CMD);
			Path newDir = parser.nextPath();
			parser.noMoreArguments();
			
			if(Files.exists(newDir)) {
				env.writeln("Given directory already exists!");
				return ShellStatus.CONTINUE;
			}
			Files.createDirectories(newDir);
			env.writeln("Created directory: "+ newDir.toString());
		} catch (IllegalArgumentException ex) {
			env.writeln(ex.getMessage());
		} catch (IOException ex) {
			env.writeln(IO_ERROR);
		}
		return ShellStatus.CONTINUE;
	}
	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getCommandName() {
		return MKDIR_CMD;
	}
	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<String> getCommandDescription() {
		return Arrays.asList(
				"Creates the directory structure as specified by the path given",
				"as the argument.",
				"This command takes exactly one argument - the path to the",
				"directory to create. If multiple directories in the given",
				"path do not exist, all will be created.",
				"",
				"Syntax:",
				"> mkdir [NEW_DIR_PATH]",
				""
		);
	}
}
