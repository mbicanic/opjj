package hr.fer.zemris.java.hw08.shell.builder;

/**
 * The exception to throw whenever a NameBuilderParser
 * encounters an error.
 * 
 * @author Miroslav Bićanić
 */
public class NameBuilderParserException extends RuntimeException {
	private static final long serialVersionUID = 1L;
	
	/**
	 * The default constructor for the exception
	 */
	public NameBuilderParserException() {
		super();
	}
	/**
	 * A constructor for a NameBuilderParserException taking
	 * a message describing the problem that caused the exception.
	 * 
	 * @param message The description of the cause of the exception
	 */
	public NameBuilderParserException(String message) {
		super(message);
	}
}
