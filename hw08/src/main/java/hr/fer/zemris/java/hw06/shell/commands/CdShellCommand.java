package hr.fer.zemris.java.hw06.shell.commands;

import java.nio.file.Path;
import java.util.Arrays;
import java.util.List;

import hr.fer.zemris.java.hw06.shell.Environment;
import hr.fer.zemris.java.hw06.shell.ShellCommand;
import hr.fer.zemris.java.hw06.shell.ShellStatus;

/**
 * CdShellCommand is a command capable of changing the
 * current working directory of the shell.
 * 
 * To correctly execute, it must receive exactly one argument 
 * representing a valid path to an existing directory that 
 * will become the new working directory.
 * 
 * Giving it any other number of arguments or an invalid argument 
 * (e.g. nonexistant directory) will stop the execution of the 
 * command, returning the control back to the Shell from which 
 * it was initially called. 
 * 
 * Correct examples showing the syntax:
 * 	{@code cd C:\Users\Documents\}
 * 		-- changes the working directory to C:\Users\Documents
 * 	{@code cd "My Folder"}
 * 		-- if the working directory was X, and directory 
 * 			"My Folder" exists in it, changes the working
 * 			directory to "X\My Folder"
 *
 * @author Miroslav Bićanić
 */
public class CdShellCommand implements ShellCommand {

	/** Name of the CdShellCommand */
	private static final String CD_CMD = "cd";
	
	/**
	 * {@inheritDoc}
	 * 
	 * @param arguments The path to the new working directory
	 * @return Always returns ShellStatus.CONTINUE
	 */
	@Override
	public ShellStatus executeCommand(Environment env, String arguments) {
		try {
			ArgumentParser parser = new ArgumentParser(arguments, env, CD_CMD);
			Path dest = parser.nextDirectory();
			parser.noMoreArguments();
			env.setCurrentDirectory(dest);
		} catch (IllegalArgumentException ex) {
			env.writeln(ex.getMessage());
		}
		return ShellStatus.CONTINUE;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getCommandName() {
		return CD_CMD;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<String> getCommandDescription() {
		return Arrays.asList(
			"Changes the current working directory to the one",
			"given as an argument of this command.",
			"",
			"It takes exactly one argument - a valid path to an",
			"existing directory in the file system.",
			"",
			"Syntax:",
			"> cd [PATH_TO_DIR]"
		);
	}

}
