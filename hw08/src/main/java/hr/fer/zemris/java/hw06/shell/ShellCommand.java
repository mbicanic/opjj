package hr.fer.zemris.java.hw06.shell;

import java.util.List;

/**
 * ShellCommand is an interface for all the commands that
 * MyShell supports.
 * 
 * @author Miroslav Bićanić
 */
public interface ShellCommand {
	/**
	 * Executes this command in the given environment, using the
	 * given arguments.
	 * 
	 * @param env The environment in which to execute the command
	 * @param arguments The arguments for this command
	 * @return The status after the execution of the command
	 */
	ShellStatus executeCommand(Environment env, String arguments);
	/**
	 * @return The name of this command
	 */
	String getCommandName();
	/**
	 * This method returns a {@link List} of lines describing
	 * the usage and functionality of this command.
	 * 
	 * @return A list of description lines
	 */
	List<String> getCommandDescription();
}
