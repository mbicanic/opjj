package hr.fer.zemris.java.hw06.shell.commands;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import hr.fer.zemris.java.hw06.shell.Environment;

/**
 * ArgumentParser is a class that parses a given string
 * containing multiple arguments either separated by
 * whitespace, or surrounded by quotes.
 * 
 * The parser returns arguments on demand (one by one),
 * offering methods that interpret the string argument
 * as either a Path to a file, a Path to a directory or
 * a regular String argument.
 * 
 * Only the commands that use this parser know in what
 * order should the arguments be given, and of whay type
 * should they be, which is why the parser works on demand.
 * 
 * For the same reason, the class is set to package-private
 * - no other classes are supposed to use ArgumentParser.
 * 
 * @author Miroslav Bićanić
 */
class ArgumentParser {
	/** The ArgumentLexer that tokenizes the arguments into strings */
	ArgumentLexer l;
	/** The Environment in which the execution is happening */
	Environment env;
	/** The command that created the parser */
	String command;
	
	/**
	 * A constructor for an ArgumentParser
	 * @param arguments The string containing the arguments to parse
	 * @param env The Environment in which the execution is happening
	 * @param command The command from which the parser was created
	 */
	ArgumentParser(String arguments, Environment env, String command) {
		l = new ArgumentLexer(arguments);
		this.env = env;
		this.command = command;
	}
	
	/**
	 * Parses the next argument in sequence as a path to an
	 * existing directory.
	 * 
	 * @return A Path to the user-specified directory
	 */
	Path nextDirectory() {
		Path dir = nextPath();
		if(!Files.isDirectory(dir)) {
			throw new IllegalArgumentException("Given path is not a path to a directory.");
		}
		return dir;
	}
	/**
	 * Parses the next argument in sequence as a path to an
	 * existing file.
	 * 
	 * @return A Path to the user-specified file
	 */
	Path nextFile() {
		Path file = nextPath();
		if(!Files.isRegularFile(file)) {
			throw new IllegalArgumentException("Given path is not a path to a file.");
		}
		return file;
	}
	/**
	 * Parses the next argument in sequence as a general
	 * path.
	 * 
	 * @return A Path to whatever the user specified
	 */
	Path nextPath() {
		String path = nextMandatoryArgument();
		return env.getCurrentDirectory().resolve(Paths.get(path));
	}
	/**
	 * Parses the next argument as a regular String, returning
	 * null if the no more arguments are present.
	 * 
	 * @return The next argument in sequence
	 */
	String nextArgument() {
		return l.next();
	}
	/**
	 * Parses the next argument as a regular String, throwing
	 * an exception if no more arguments are present.
	 * 
	 * @return The next argument in sequence
	 * @throws IllegalArgumentException if an argument wasn't present
	 */
	String nextMandatoryArgument() {
		String s = l.next();
		if(s==null) {
			throw new IllegalArgumentException("Too few arguments given to command '"+command+"'.");
		}
		return s;
	}
	/**
	 * Method to call once all expected arguments have been parsed
	 * and there should be no more arguments present.
	 * 
	 * @throws IllegalArgumentExcception if there were more arguments present
	 */
	void noMoreArguments() {
		if(l.next()!=null) {
			throw new IllegalArgumentException("Too many arguments given to command '"+command+"'.");
		}
	}
	
}
