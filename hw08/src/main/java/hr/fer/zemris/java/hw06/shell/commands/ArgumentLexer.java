package hr.fer.zemris.java.hw06.shell.commands;

import static hr.fer.zemris.java.hw06.shell.commands.Utility.tnIAE;

import java.util.Objects;

/**
 * ArgumentLexer is a very simple text tokenizer that generates
 * only Strings.
 * 
 * Normally, this lexer reads text character by character, 
 * generating a String token whenever a whitespace is reached.
 * 
 * Strings can also be enclosed in double quotes, which
 * makes the lexer ignore all the whitespaces and generate
 * one string token for the whole text between quotes.
 * While inside a quote-enclosed string, escaping mechanisms
 * exist, supporting two escape sequences: \\ as \ and \" as "
 * 
 * Please note, if a text is enclosed in quotes, the closing
 * quote must either be the last character in the text, or
 * followed by at least one space. Thus, "C:\file.txt".txt
 * is illegal and will throw an exception.
 * 
 * @author Miroslav Bićanić
 */
class ArgumentLexer {
	/**
	 * The double quote character
	 */
	private static final char QUOTE = '"';
	/**
	 * The backslash character
	 */
	private static final char BACKSLASH = '\\';
	
	/**
	 * The character array comprising the text to be tokenized
	 */
	private char[] data;
	/**
	 * The index of the first unprocessed character in the array
	 */
	private int current;
	
	/**
	 * A constructor for a ArgumentLexer
	 * @param arguments The text to be tokenized
	 */
	ArgumentLexer(String arguments) {
		data = (Objects.requireNonNull(arguments).trim()).toCharArray();
		current=0;
	}
	
	/**
	 * Reads and returns the next String in the received text.
	 * @return The next string
	 */
	String next() {
		if(current>=data.length) {
			return null;
		}
		skipBlanks();
		if(data[current]==QUOTE) {
			return getQuotedString();
		} else {
			return getNormalString();
		}
	}
	
	/**
	 * Returns all the text enclosed in quotes as one string,
	 * allowing the supported escape sequences.
	 * @return The text that was surrounded by quotes.
	 */
	private String getQuotedString() {
		StringBuilder sb = new StringBuilder(100);
		current++;
		while(current < data.length && data[current]!=QUOTE) {
			if(data[current]==BACKSLASH) {
				current++;
				escapeAttempt(sb);
				continue;
			}
			sb.append(data[current++]);
		}
		if(current>=data.length) {
			tnIAE("Given path had an opener quote, but wasn't closed.");
		}
		current++;
		if(!(current>= data.length || Character.isWhitespace(data[current]))) {
			tnIAE("Quote-enclosed string must be followed by end of line or a space.");
		} 
		return sb.toString();
	}
	/**
	 * Checks if the character after the backslash is a valid
	 * character for an escape sequence, escaping it if yes and
	 * appending both the backslash and the current character if
	 * no.
	 * 
	 * @param sb A StringBuilder storing the String built so far
	 */
	private void escapeAttempt(StringBuilder sb) {
		if(current<data.length) {
			if(data[current]==QUOTE || data[current]==BACKSLASH) {
				sb.append(data[current++]);
			} else {
				sb.append(BACKSLASH);
				sb.append(data[current++]);
			}
		}	
	}
	/**
	 * Appends characters until the first whitespace or end of text
	 * has been reached, and returns the formed String.
	 * 
	 * @return A string from the original text
	 */
	private String getNormalString() {
		StringBuilder sb = new StringBuilder(100);
		while(current<data.length && !Character.isWhitespace(data[current])) {
			sb.append(data[current++]);
		}
		return sb.toString();
	}
	/**
	 * Skips whitespace characters in the array of characters.
	 */
	private void skipBlanks() {
		while(current<data.length && Character.isWhitespace(data[current])) {
			current++;
		}
	}
}
