package hr.fer.zemris.java.hw08.shell.builder;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import static hr.fer.zemris.java.hw08.shell.builder.DefaultNameBuilders.*;
/**
 * NameBuilderParser parses a renaming pattern and creates
 * a NameBuilder that builds the new name based on the
 * renaming pattern and the values in the old name.
 * 
 * @author Miroslav Bićanić
 */
public class NameBuilderParser {
	/** A character array representing the renaming pattern */
	private char[] arr;
	/** The index of the first unprocessed character in the pattern */
	private int index=0;
	/** The resulting NameBuilder */
	private NameBuilder composite;
	
	/**
	 * A constructor for a NameBuilderParser
	 * @param expression The renaming expression to parse
	 * @throws NullPointerException if given expression is null
	 * @throws NameBuilderParserException if given expression is blank string
	 */
	public NameBuilderParser(String expression) {
		this.arr = Objects.requireNonNull(expression).toCharArray();
		if(expression.isBlank()) {
			throw new NameBuilderParserException("Expression cannot be a blank string!");
		}
		parse();
	}
	/**
	 * @return The resulting NameBuilder that builds a full name
	 * 			based on the given renaming expression
	 */
	public NameBuilder getNameBuilder() {
		 return composite;
	}
	
	/**
	 * Shorthand method to check if if at the current index
	 * a substitution subexpression begins.
	 * @return true if a substitution subexpression is opened, false otherwise
	 */
	private boolean subOpener() {
		return index<arr.length-1 && arr[index]=='$' && arr[index+1]=='{';
	}
	
	/**
	 * Parses the renaming expression, building NameBuilder objects
	 * and adding them to the {@link NameBuilderParser#composite}
	 * NameBuilder.
	 */
	private void parse() {
		List<NameBuilder> list = new ArrayList<>();
		while(index<arr.length) {
			list.add(subOpener() ? parseSubstitute() : parseWord());
		}
		this.composite = composite(list.toArray(new NameBuilder[list.size()]));
	}

	/**
	 * Parses characters until a substitution expression is
	 * reached, creating a NameBuilder that appends the same
	 * character sequence to new names.
	 * 
	 * @return A literal NameBuilder
	 */
	private NameBuilder parseWord() {
		StringBuilder sb = new StringBuilder(20);
		while(index < arr.length) {
			if(subOpener()) {
				break;
			}
			sb.append(arr[index++]);
		}
		return literal(sb.toString());
	}
	/**
	 * Parses a substitution expression creating a NameBuilder
	 * that appends the matched value of the group provided in the 
	 * expression, optionally with further specifications.
	 * 
	 * @return A grouper NameBuilder
	 * @throws NameBuilderParserException if:
	 * 			- the substitution expression isn't closed
	 * 			- group number and specification are delimited by 
	 * 				an unknown character
	 */
	private NameBuilder parseSubstitute() {
		index+=2;
		int groupID = getPositiveNumber(getDigits("Group number"), "Group number");
		if(index<arr.length && arr[index]=='}') {
			index++;
			return grouper(groupID);
		} else if (index<arr.length && arr[index++]==',') {
			String specs = getDigits("Specification");
			if(index>=arr.length || arr[index++]!='}') {
				throw new NameBuilderParserException("Expression must be closed!");
			}
			
			char padding = (specs.charAt(0)=='0' && specs.length()>1 ? '0' : ' ');
			specs = (padding=='0' ? specs.substring(1) : specs);
			int minNumber = getPositiveNumber(specs, "Specification");
			return grouper(groupID, padding, minNumber);
		} else {
			throw new NameBuilderParserException("Invalid character found after group number.");
		}
	}

	/**
	 * @return A String of digits from the substitution expression
	 * @throws NameBuilderParserException if no digits were found
	 */
	private String getDigits(String argName) {
		skipSpaces();
		StringBuilder sb = new StringBuilder(2);
		if(index<arr.length && arr[index]=='-') {
			throw new NameBuilderParserException("'"+argName+"' must be a positive numnber!");
		}
		while(index<arr.length && Character.isDigit(arr[index])) {
			sb.append(arr[index++]);
		}
		skipSpaces();
		String digits = sb.toString();
		if(digits.length()==0) {
			throw new NameBuilderParserException("'"+argName+"' parameter expected but not provided.");
		}
		return sb.toString();
	}
	/**
	 * Helper method that skips spaces until the first non-whitespace
	 * character.
	 */
	private void skipSpaces() {
		while(index<arr.length && Character.isWhitespace(arr[index])) {
			index++;
		}
	}
	/**
	 * Converts a string of digits into a number and checks if 
	 * it is positive.
	 * 
	 * @param num The string of digits
	 * @param name The name of the argument for which these digits are
	 * @return The positive integer value of the string of digits
	 * @throws NameBuilderParserException if the number is negative
	 */
	private int getPositiveNumber(String num, String name) {
		int number;
		number = Integer.parseInt(num);
		if(number<0) {
			throw new NameBuilderParserException("'"+name+"' parameter must be a non-negative number.");
		}
		return number;
	}
}
