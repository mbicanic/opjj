package hr.fer.zemris.java.hw08.shell.builder;

/**
 * NameBuilder is an interface for objects that can
 * build new names for files. The name can be built
 * using parts of the old name accessible through
 * the given {@link FilterResult} or otherwise, e.g.
 * using literal values.
 * 
 * In case the name is to be built from multiple differing
 * segments of the old name or multiple literal values,
 * the same StringBuilder can be given to all of them in
 * order.
 * 
 * @author Miroslav Bićanić
 */
@FunctionalInterface
public interface NameBuilder {
	/**
	 * Adds a part of the name to the given StringBuilder.
	 * 
	 * @param result The FilterResult containing information about building the name
	 * @param sb A StringBuilder through which to build the name
	 */
	void execute(FilterResult result, StringBuilder sb);
}
