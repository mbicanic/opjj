package hr.fer.zemris.java.hw06.shell.commands;

import static hr.fer.zemris.java.hw06.shell.commands.Utility.*;

import java.nio.file.Path;
import java.util.Arrays;
import java.util.List;
import java.util.Stack;

import hr.fer.zemris.java.hw06.shell.Environment;
import hr.fer.zemris.java.hw06.shell.ShellCommand;
import hr.fer.zemris.java.hw06.shell.ShellStatus;

/**
 * DropdShellCommand is a command that pops the last directory
 * stored by the {@link PushdShellCommand}.
 * 
 * To correctly execute, it must be called with no arguments.
 * Giving it any arguments will stop the execution of the 
 * command, returning the control back to the Shell from which 
 * it was initially called. 
 * 
 * If no directories were pushed to the stack yet, or the stack
 * was emptied, the command reports an error and does nothing.
 * 
 * The only way to call this command is:
 * 	{@code dropd}
 * 		-- removes one directory from the stack
 *
 * @author Miroslav Bićanić
 */
public class DropdShellCommand implements ShellCommand {

	/** Name of the DropdShellCommand */
	private static final String DROPD_CMD = "dropd";
	
	/**
	 * {@inheritDoc}
	 * 
	 * @param arguments Must be an empty string for this command to execute correctly
	 * @return Always returns ShellStatus.CONTINUE
	 */
	@SuppressWarnings("unchecked")
	@Override
	public ShellStatus executeCommand(Environment env, String arguments) {
		try {
			if(arguments.length()!=0) {
				tnIAE("Too many arguments given with command: 'dropd'. Expected 0.");
			}
			Object o = env.getSharedData("cdstack");
			if(o==null) {
				tnIAE("Stack of current directories does not exist.");
			}
			Stack<Path> p = (Stack<Path>)o;
			if(p.isEmpty()) {
				tnIAE("Stack of current directories is empty.");
			}
			p.pop();
		} catch (IllegalArgumentException ex) {
			env.writeln(ex.getMessage());
		}
		return ShellStatus.CONTINUE;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getCommandName() {
		return DROPD_CMD;
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<String> getCommandDescription() {
		return Arrays.asList(
				"Removes the last stored directory from the",
				"directory stack.",
				"If the stack is empty or does not exist, the",
				"command prints the error and does nothing.",
				"",
				"This command takes no arguments. Syntax:",
				"> dropd"
			);
	}
}
