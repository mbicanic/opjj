package hr.fer.zemris.java.hw08.shell.builder;

import java.util.regex.Matcher;

/**
 * FilterResult is a class representing the names of
 * files that matched a regular expression pattern.
 * 
 * It supports pattern grouping, and it is possible to 
 * retrieve the value that matched a certain group
 * of the pattern.
 * 
 * @author Miroslav Bićanić
 */
public class FilterResult {
	/** The name of the file that matched the pattern */
	private String filename;
	/** 
	 * A matcher that was used to match the file to the pattern.
	 * Contains the information about pattern groups.
	 */
	private Matcher m;
	
	/**
	 * A constructor for a FilterResult
	 * @param file The filename that matched the pattern
	 * @param matcher The Matcher that matched the filename to a pattern
	 */
	public FilterResult(String file, Matcher matcher) {
		this.m = matcher;
		this.filename = file;
	}
	/**
	 * Returns the number of different groups defined
	 * in the pattern matched by the filename.
	 * 
	 * @return The number of groups
	 */
	public int numberOfGroups() {
		return m.groupCount();
	}
	/**
	 * Returns the specific substring from the filename which 
	 * matched the index-th group of the pattern.
	 * 
	 * @param index The index of the group whose value to retrieve
	 * @return The specific substring matching the group with the given index
	 */
	public String group(int index) {
		if(index<0 || index>m.groupCount()) {
			throw new IllegalArgumentException("Cannot fetch group "+index+" from string matching only "+m.groupCount()+" groups.");
		}
		return m.group(index);
	}
	
	/**
	 * Returns a string containing the filename and its
	 * correspondence to all groups defined in the pattern
	 * it matched, in the format 
	 * {@code GROUP NUMBER: value [GROUP NUMBER: value...]}
	 * @return
	 */
	public String groupOutput() {
		StringBuilder sb = new StringBuilder(50);
		sb.append(filename);
		for(int i = 0, n = numberOfGroups(); i<=n; i++) {
			sb.append(String.format(" %d: ", i));
			sb.append(group(i));
		}
		return sb.toString();
	}
	@Override
	public String toString() {
		return filename;
	}
	
}
