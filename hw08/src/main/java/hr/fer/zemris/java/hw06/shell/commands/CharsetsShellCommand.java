package hr.fer.zemris.java.hw06.shell.commands;

import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.List;

import hr.fer.zemris.java.hw06.shell.Environment;
import hr.fer.zemris.java.hw06.shell.ShellCommand;
import hr.fer.zemris.java.hw06.shell.ShellStatus;

/**
 * CharsetsShellCommand is a command that outputs all the
 * charsets (code pages) that are currently available on
 * the used platform.
 * 
 * This command takes no arguments, and giving it any arguments
 * will stop the execution of the command, returning the control 
 * back to the Shell from which it was initially called.
 * 
 * The only way to call this command is:
 * 	{@code charsets}
 * 		-- outputs a list of all supported charsets on the platform
 * 
 * @author Miroslav Bićanić
 */
public class CharsetsShellCommand implements ShellCommand {

	/** Name of the CharsetsShellCommand */
	private static final String CHARS_CMD = "charsets";
	
	/**
	 * {@inheritDoc}
	 * 
	 * @param arguments Must be an empty string for this command to execute correctly
	 * @return Always returns ShellStatus.CONTINUE
	 */
	@Override
	public ShellStatus executeCommand(Environment env, String arguments) {
		if(arguments.length()!=0) {
			env.writeln("Too many arguments given with command: 'charsets'. Expected 0.");
			return ShellStatus.CONTINUE;
		}
		Charset.availableCharsets().keySet()
		.forEach((charsetName)->env.writeln(charsetName));
		return ShellStatus.CONTINUE;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getCommandName() {
		return CHARS_CMD;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<String> getCommandDescription() {
		return Arrays.asList(
				"Lists all the charsets (code pages) available for use",
				"on this platform.",
				"",
				"This command takes no arguments. Syntax:",
				"> charsets",
				""
			);
	}

}
