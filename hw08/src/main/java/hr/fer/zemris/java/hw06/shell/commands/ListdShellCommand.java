package hr.fer.zemris.java.hw06.shell.commands;

import static hr.fer.zemris.java.hw06.shell.commands.Utility.*;

import java.nio.file.Path;
import java.util.Arrays;
import java.util.List;
import java.util.Stack;

import hr.fer.zemris.java.hw06.shell.Environment;
import hr.fer.zemris.java.hw06.shell.ShellCommand;
import hr.fer.zemris.java.hw06.shell.ShellStatus;

/**
 * ListdShellCommand is a command that prints the paths to
 * all directories currently stored on the directory stack.
 * 
 * To correctly execute, it must be called with no arguments.
 * Giving it any arguments will stop the execution of the 
 * command, returning the control back to the Shell from which 
 * it was initially called. 
 * 
 * The only way to call this command is:
 * 	{@code listd}
 * 		-- prints all directories from the stack to the terminal
 *
 * @author Miroslav Bićanić
 */
public class ListdShellCommand implements ShellCommand {

	/** Name of the ListdShellCommand */
	private static final String LISTD_CMD = "listd";
	
	/**
	 * {@inheritDoc}
	 * 
	 * @param arguments Must be an empty string for this command to execute correctly
	 * @return Always returns ShellStatus.CONTINUE
	 */
	@SuppressWarnings("unchecked")
	@Override
	public ShellStatus executeCommand(Environment env, String arguments) {
		try {
			if(arguments.length()!=0) {
				tnIAE("Too many arguments given with command: 'listd'. Expected 0.");
			}
			Object o = env.getSharedData("cdstack");
			if(o==null) {
				tnIAE("Stack of current directories does not exist.");
			}
			Stack<Path> s = (Stack<Path>)o;
			if(s.isEmpty()) {
				tnIAE("No stored directories.");
			}
			s.forEach((path)->env.writeln(path.toString()));
		} catch (IllegalArgumentException ex) {
			env.writeln(ex.getMessage());
		}
		return ShellStatus.CONTINUE;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getCommandName() {
		return LISTD_CMD;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<String> getCommandDescription() {
		return Arrays.asList(
			"Prints all directories stored on the directory stack.",
			"If a stack is empty or non-existant, prints a message",
			"saying so.",
			"",
			"Syntax:",
			"> listd"
				);
	}
}
