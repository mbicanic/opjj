package hr.fer.zemris.java.hw06.shell.commands;

import java.util.Arrays;
import java.util.List;

import hr.fer.zemris.java.hw06.shell.Environment;
import hr.fer.zemris.java.hw06.shell.ShellCommand;
import hr.fer.zemris.java.hw06.shell.ShellStatus;

/**
 * PwdShellCommand is a command that prints the path to the
 * current working directory to the terminal.
 * 
 * To correctly execute, it must be called with no arguments.
 * Giving it any arguments will stop the execution of the 
 * command, returning the control back to the Shell from which 
 * it was initially called. 
 * 
 * The only way to call this command is:
 * 	{@code pwd}
 * 		-- prints the current working directory to the terminal
 *
 * @author Miroslav Bićanić
 */
public class PwdShellCommand implements ShellCommand {

	/** Name of the PwdShellCommand */
	private static final String PWD_CMD = "pwd";
	
	/**
	 * {@inheritDoc}
	 * 
	 * @param arguments Must be an empty string for this command to execute correctly
	 * @return Always returns ShellStatus.CONTINUE
	 */
	@Override
	public ShellStatus executeCommand(Environment env, String arguments) {
		if(arguments.length()!=0) {
			env.writeln("Too many arguments given with command: 'pwd'. Expected 0.");
			return ShellStatus.CONTINUE;
		}
		env.writeln("Current directory: "+env.getCurrentDirectory().toString());
		return ShellStatus.CONTINUE;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getCommandName() {
		return PWD_CMD;
	}
	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<String> getCommandDescription() {
		return Arrays.asList(
			"Prints the path to the current working directory.",
			"This command takes no arguments. Syntax:",
			"> pwd");
	}

}
