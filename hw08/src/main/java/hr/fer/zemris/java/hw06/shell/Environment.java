package hr.fer.zemris.java.hw06.shell;

import java.nio.file.Path;
import java.util.SortedMap;

/**
 * An interface for the environment in which a MyShell
 * operates.
 * 
 * An environment is the mediator between the shell itself,
 * {@link ShellCommand} objects and the user, meaning all 
 * communication is done through an Environment.
 *
 * Every environment also must provide symbols for:
 * 	1) a prompt,
 * 	2) a user's announcment that a command will span
 * 		multiple lines
 * 	3) the beginning of a line not interpreted as new, 
 * 		but as a part of the previous line
 * 		(to differentiate them from the otherwise 
 * 		outputted prompt symbol)
 * 
 * The environment must ensure that these symbols can 
 * be changed by the user at any point.
 * 
 * The environment must enable changing the working 
 * directory, as well as provide a data structure shared
 * by multiple commands.
 * 
 * @author Miroslav Bićanić
 */
public interface Environment {
	
	/**
	 * Reads a line from the source.
	 * @return The line that was read
	 * @throws ShellIOException If the line could not have been read
	 */
	String readLine() throws ShellIOException;  
	/**
	 * Writes text to the destination, without adding
	 * a new line after the written text.
	 * 
	 * @param text The text to write to the destination
	 * @throws ShellIOException If the text could not have been written
	 */
	void write(String text) throws ShellIOException;
	/**
	 * Writes text to the destination, adding a new line
	 * after the written text.
	 * 
	 * @param text The text to write to the destination
	 * @throws ShellIOException If the text could not have been written
	 */
	void writeln(String text) throws ShellIOException;
	/**
	 * Returns a {@link SortedMap} of all {@link ShellCommand} objects for 
	 * MyShell registered in this Environment, with the keys being
	 * the names of the commands, and values the commands themselves.
	 * 
	 * @return A Map of the registered commands
	 */
	SortedMap<String, ShellCommand> commands();  
	
	/**
	 * Returns the currently used symbol that indicates that a line
	 * is part of a multiline statement, and not a new statement.
	 * 
	 * @return The currently used multiline character
	 */
	Character getMultilineSymbol();  
	/**
	 * @param symbol The new symbol for marking every additional line in a statement
	 */
	void setMultilineSymbol(Character symbol);  
	/**
	 * Returns the currently used symbol that indicates that the
	 * user is being prompted to enter a statement or parameter.
	 * 
	 * @return The currently used prompt character
	 */
	Character getPromptSymbol();  
	/**
	 * @param symbol The new symbol for prompting the user
	 */
	void setPromptSymbol(Character symbol);  
	/**
	 * Returns the currently used symbol with which the user
	 * announces that his statement will span into the next line.
	 * 
	 * @return The currently used character to announce spanning more lines
	 */
	Character getMorelinesSymbol();  
	/**
	 * @param symbol The new symbol to announce spanning more lines
	 */
	void setMorelinesSymbol(Character symbol);
	
	/**
	 * @return The current working directory of the shell
	 */
	Path getCurrentDirectory();
	/**
	 * @param path The new working directory for the shell
	 */
	void setCurrentDirectory(Path path);
	/**
	 * @param key The key under which the data is stored
	 * @return The value under the given key in the shared data
	 */
	Object getSharedData(String key);
	/**
	 * @param key The key under which to store new data
	 * @param value The data to store under the given key
	 */
	void setSharedData(String key, Object value);
}
