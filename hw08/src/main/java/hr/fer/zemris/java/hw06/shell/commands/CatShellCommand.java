package hr.fer.zemris.java.hw06.shell.commands;

import static hr.fer.zemris.java.hw06.shell.commands.Utility.*;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.IllegalCharsetNameException;
import java.nio.charset.UnsupportedCharsetException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.List;

import hr.fer.zemris.java.hw06.shell.Environment;
import hr.fer.zemris.java.hw06.shell.ShellCommand;
import hr.fer.zemris.java.hw06.shell.ShellStatus;

/**
 * CatShellCommand is a command capable of opening a file,
 * reading it with the desired charset and outputting the
 * content of the file to a given Environment.
 * 
 * To correctly execute, it must receive at least one 
 * argument representing a valid path to an existing file 
 * whose content it should read and output.
 * 
 * Additionally, a second argument can be given, defining
 * the name of a charset (code page) to use when reading
 * the file. If this argument is not provided, the platform's
 * default charset is used.
 * 
 * Giving it any other number of arguments or an invalid argument 
 * (e.g. nonexistant file) will stop the execution of the command, 
 * returning the control back to the Shell from which it was 
 * initially called. 
 * If given an invalid charset, the user is asked for permission to
 * output the content using the default charset.
 * 
 * Correct examples showing the syntax:
 * 	{@code cat C:\Users\Documents\example.txt}
 * 		-- outputs content of 'example.txt' using default charset
 * 	{@code cat "C:\Sample\Test Results\testOutput.txt"}
 * 		-- outputs content of 'testOutput.txt' using default charset
 * 	{@code cat ..\..\file.txt UTF-8}
 * 		-- outputs content of 'file.txt' using UTF-8 charset
 * 
 * @see CharsetsShellCommand More information on charsets
 * @author Miroslav Bićanić
 */
public class CatShellCommand implements ShellCommand {
	
	/** Name of the CatShellCommand */
	private static final String CAT_CMD = "cat";
	
	/**
	 * {@inheritDoc}
	 * 
	 * @param arguments The path to the file to read, optionally also a charset parameter
	 * @return Always returns ShellStatus.CONTINUE
	 */
	@Override
	public ShellStatus executeCommand(Environment env, String arguments) {
		try {
			ArgumentParser parser = new ArgumentParser(arguments, env, CAT_CMD);
			Path file = parser.nextFile();
			Charset charset = Charset.defaultCharset();
			String cst = parser.nextArgument();
			if(cst!=null) {
				try {
					charset = Charset.forName(cst);
				} catch (IllegalCharsetNameException | UnsupportedCharsetException ex) {
					boolean answer = queryYesNo(
							"Invalid charset name used for reading file. Type 'charsets' to see the list of available charsets.", 
							"Do you wish to output the file content using the default charset?",
							env);
					if(!answer) {
						return ShellStatus.CONTINUE;
					}
				}
			}
			parser.noMoreArguments();
			
			env.writeln(file.getFileName().toString()+":\n");
			Files.readAllLines(file, charset).stream()
			.forEach((l)->env.writeln(l));
			env.writeln("");
		} catch (IllegalArgumentException ex) {
			env.writeln(ex.getMessage());
		} catch (IOException ex) {
			env.writeln(IO_ERROR);
		}
		return ShellStatus.CONTINUE;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getCommandName() {
		return CAT_CMD;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<String> getCommandDescription() {
		return Arrays.asList(
				"Outputs the content of a given file optionally using a",
				"specified charset.",
				"",
				"This command requires at least one argument - a valid path",
				"to an existing file that is to be read. Additionally, a",
				"desired charset can be given as the second argument, which",
				"makes the command read the file using the specified charset,",
				"if such a charset exists.",
				"For a list of available charsets on this platform, type",
				"'charsets'.",
				"",
				"Syntax:",
				"> cat [PATH_TO_FILE] [CHARSET]?",
				""
			);
	}
}
