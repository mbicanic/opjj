package hr.fer.zemris.java.hw06.shell.commands;

import java.util.Arrays;
import java.util.List;

import hr.fer.zemris.java.hw06.shell.Environment;
import hr.fer.zemris.java.hw06.shell.ShellCommand;
import hr.fer.zemris.java.hw06.shell.ShellStatus;

/**
 * ExitShellCommand is a command that terminates the execution
 * of the program.
 * 
 * It is the only command that can return ShellStatus.TERMINATE,
 * thus terminating the program.
 * 
 * The command takes no arguments, and giving it any arguments
 * will stop the execution of the command, returning the control
 * back to the Shell from which the command was called. This
 * means that the program will not be terminated in such a 
 * scenario.
 * 
 * The only correct way to call this method is:
 * 	{@code exit}
 * 		-- terminates the execution of the whole program
 * 
 * @author Miroslav Bićanić
 */
public class ExitShellCommand implements ShellCommand {

	/** Name of the ExitShellCommand */
	private static final String EXIT_CMD = "exit";
	
	/**
	 * {@inheritDoc}
	 * @param arguments Must be an empty string for this command to execute correctly
	 * @return ShellStatus.TERMINATE if method was called correctly,
	 * 			ShellStatus.CONTINUE otherwise, allowing the program to continue running
	 */
	@Override
	public ShellStatus executeCommand(Environment env, String arguments) {
		if(arguments.length()!=0) {
			env.writeln("Too many arguments given with command: 'exit'. Expected 0.");
			return ShellStatus.CONTINUE;
		}
		return ShellStatus.TERMINATE;
	}
	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getCommandName() {
		return EXIT_CMD;
	}
	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<String> getCommandDescription() {
		return Arrays.asList(
				"Terminates the execution of the program.",
				"This command takes no arguments. Syntax:",
				"> exit",
				""
		);
	}

}
