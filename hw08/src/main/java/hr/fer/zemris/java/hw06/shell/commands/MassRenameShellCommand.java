package hr.fer.zemris.java.hw06.shell.commands;

import static hr.fer.zemris.java.hw06.shell.commands.Utility.*;

import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import hr.fer.zemris.java.hw06.shell.Environment;
import hr.fer.zemris.java.hw06.shell.ShellCommand;
import hr.fer.zemris.java.hw06.shell.ShellStatus;
import hr.fer.zemris.java.hw08.shell.builder.FilterResult;
import hr.fer.zemris.java.hw08.shell.builder.NameBuilder;
import hr.fer.zemris.java.hw08.shell.builder.NameBuilderParser;
import hr.fer.zemris.java.hw08.shell.builder.NameBuilderParserException;

/**
 * MassRenameShellCommand is a command that enables renaming
 * or moving multiple files at once.
 * 
 * The command takes four or five arguments:
 * 	DIR1	-	The source directory from which to retrieve files
 * 				to rename or move
 * 	DIR2	-	The destination directory to which to move the
 * 				renamed files. It can be the same as the source
 * 				directory.
 * 	COMMAND -	One of the four subcommands listed below
 * 	MASK	-	A regular expression pattern used to select only
 * 				the matching files from DIR1
 * 	RENAME	-	A regular expresion defining the rules for renaming
 * 				the files from DIR1.
 * 				Required only if the subcommand is 'execute'.
 * Giving the command any other number of arguments, or an illegal argument
 * stops the command's execution and returns the control to the Shell from
 * which the command was called.
 * 
 * The regular expressions are written using the standard rules.
 * They can be enclosed in quotes, in which case escape sequences
 * exist according to the rules of {@link MyShell}.
 * An example of a valid regular expressions:
 * 		{@code literal-[d\+]-[ABC]*def[\d]?}
 * 			-- Matches any string that is made of
 * 				1) "literal-"
 * 				2) 1 or more digits
 * 				3) "-"
 * 				4) 0 or more letters A, B or C
 * 				5) "def"
 * 				6) one optional digit
 * 
 * If the MASK pattern contains parts surrounded by round brackets
 * (), then a group is defined for each part enclosed by brackets,
 * along with an implied group encompassing the whole pattern.
 * An example of group division of a pattern:
 * 	{@code literal-([d\+])-([ABC]*)def([\d]?)}
 * 		-- Matches equally as previous example, but defines four groups:
 * 			0: The implied group, matching the whole pattern as in
 * 				the previous example
 * 			1: The first bracket - [\d+], matches 1 or more digit
 * 			2: The second bracket - [ABC]*, matches 0 or more letters
 * 				A, B or C
 * 			3: The third bracket - [\d]?, matches 0 or one digit
 * 		-- With the previous pattern, the string
 * 			{@code literal-1231-ACCABABdef1} would be divided like so:
 * 			0: literal-1231-ACCABABdef1
 * 			1:	1231
 * 			2: ACCABAB
 * 			3: 1
 * 
 * The four supported subcommands are:
 * 	1) filter 	- Prints all files in given directory that match 
 * 					the given pattern
 * 	2) groups 	- Prints all files in given directory that match
 * 					the given pattern, along with indexes of all
 * 					groups provided in the pattern paired with the
 * 					substrings of the file name that matched the 
 * 					group
 * 	3) show	 	- Prints how the matching files would be renamed
 * 					using the second pattern the user provided
 * 	4) execute	- Renames the files using the second pattern the
 * 					user provided and moves them if the destination
 * 					directory is different than the source directory.
 * 
 * When using the 'show' or 'execute' subcommand, if the MASK pattern
 * contains groups, it is possible to reference the matched values of
 * those groups in the RENAME pattern using substitution subexpressions.
 * 
 * A substitution subexpression must be written in the format:
 * {@code ${GROUP NUMBER[, SPECIFICATION]} } where the SPECIFICATION 
 * is optional.
 * NUMBER OF GROUP must be a non-negative number representing the index
 * of the referenced group in the MASK pattern.
 * SPECIFICATION is a non-negative number defining how wide should the 
 * value minimally be, and with what character to pad if the value is
 * shorter. 
 * If the first digit of the SPECIFICATION is a 0 and it is followed 
 * by at least one more digit, the padding character is '0'.
 * Otherwise, the padding character is a space (' '). 
 * The minimal width is defined by the number represented by the 
 * SPECIFICATION (with the leading 0 removed in case of 0-padding).
 * 
 * An example of using group referencing:
 * {@code massrename dir1 dir2 show liter-([GRP1]+)-(\d+).txt result-${2, 05}-${1, 5}.txt}
 * 		-- moves all files that match the first pattern from dir1 to dir2.
 * 			An example file "liter-RPG1-12.txt" would be renamed to:
 * 			"result-00012- RPG1.txt"
 * 
 * @author Miroslav Bićanić
 */
public class MassRenameShellCommand implements ShellCommand {

	/** Name of the MassRenameShellCommand */
	private static final String MASS_CMD = "massrename";
	
	/**
	 * {@inheritDoc}
	 * 
	 * @param arguments Four or five arguments as described for this command
	 * @return Always returns ShellStatus.CONTINUE
	 */
	@Override
	public ShellStatus executeCommand(Environment env, String arguments) {
		try {
			ArgumentParser parser = new ArgumentParser(arguments, env, MASS_CMD);
			Path srcDir = parser.nextDirectory();
			Path destDir = parser.nextDirectory();
			String command = parser.nextMandatoryArgument();
			String mask = parser.nextMandatoryArgument();
			
			switch(command) {
			case "filter":
				parser.noMoreArguments();
				filterCommand(env, srcDir, mask);
				return ShellStatus.CONTINUE;
			case "groups":
				parser.noMoreArguments();
				groupCommand(env, srcDir, mask);
				return ShellStatus.CONTINUE;
			}
			
			String expression = parser.nextMandatoryArgument();
			parser.noMoreArguments();
			switch(command) {
			case "show":
				showCommand(env, srcDir, mask, expression);
				break;
			case "execute":
				execCommand(env, srcDir, destDir, mask, expression);
				break;
			default:
				tnIAE("Unrecognized subcommand used for 'massrename': '"+command+"'.");
			}
		} catch (IllegalArgumentException | NameBuilderParserException ex) {
			env.writeln(ex.getMessage());
		} catch (IOException ex) {
			env.writeln(IO_ERROR);
		}
		return ShellStatus.CONTINUE;
	}

	/**
	 * Selects all files from {@code srcDir} whose names match
	 * the given {@code mask} pattern and prints their names through
	 * the given Environment.
	 * 
	 * @param env An environment through which to communicate
	 * @param srcDir The directory in which to look for matching files
	 * @param mask The regular expression pattern with which to match filenames
	 * @throws IOException see: {@link MassRenameShellCommand#filter(Path, String)}
	 */
	private void filterCommand(Environment env, Path srcDir, String mask) throws IOException {
		List<FilterResult> l = filter(srcDir, mask);
		l.forEach((fr)->env.writeln(fr.toString()));
	}
	
	/**
	 * Selects all files from {@code srcDir} whose names match
	 * the given {@code mask} pattern and prints their names through
	 * the given Environment, along with how they matched
	 * specific groups of the pattern.
	 * 
	 * @param env An environment through which to communicate
	 * @param srcDir The directory in which to look for matching files
	 * @param mask The regular expression pattern with which to match filenames
	 * @throws IOException see: {@link MassRenameShellCommand#filter(Path, String)}
	 */
	private void groupCommand(Environment env, Path srcDir, String mask) throws IOException {
		List<FilterResult> l = filter(srcDir, mask);
		l.forEach((fr)->env.writeln(fr.groupOutput()));
	}
	
	/**
	 * Selects all files from {@code srcDir} whose names match the 
	 * given {@code mask} pattern and prints their names through the 
	 * given Environment, along with how they would be renamed using 
	 * the {@code expression} pattern.
	 * 
	 * @param env An environment through which to communicate
	 * @param srcDir The directory in which to look for matching files
	 * @param mask The regular expression pattern with which to match filenames
	 * @param expression The regular expression pattern to use for renaming files
	 * @throws IOException see: {@link MassRenameShellCommand#filter(Path, String)}
	 */
	private void showCommand(Environment env, Path srcDir, String mask, String expression) throws IOException {
		List<FilterResult> oldNames = filter(srcDir, mask);
		List<String> newNames = rename(oldNames, expression);
		for(int i = 0, n = oldNames.size(); i<n; i++) {
			env.writeln(String.format("%s => %s", oldNames.get(i), newNames.get(i)));
		}
	}
	
	/**
	 * Selects all files from {@code srcDir} whose names match the 
	 * given {@code mask} pattern and moves them from {@code srcDir} 
	 * to {@code destDir} renaming them using the {@code expression} 
	 * pattern in the process.
	 * 
	 * The full path to the old file is printed through the 
	 * environment paired with the full path to the moved file.
	 * 
	 * @param env An environment through which to communicate
	 * @param srcDir The directory in which to look for matching files
	 * @param destDir The directory to which to move renamed matching files
	 * @param mask The regular expression pattern with which to match filenames
	 * @param expression The regular expression pattern to use for renaming files
	 * @throws IOException see: {@link MassRenameShellCommand#filter(Path, String)}
	 */
	private void execCommand(Environment env, Path srcDir, Path destDir, String mask, String expression) throws IOException {
		List<FilterResult> oldNames = filter(srcDir, mask);
		List<String> newNames = rename(oldNames, expression);
		for(int i = 0, n = oldNames.size(); i<n; i++) {
			Path source = Paths.get(srcDir.toString(), oldNames.get(i).toString());
			Path destination = Paths.get(destDir.toString(), newNames.get(i));
			Files.move(source, destination);
			env.writeln(String.format("%s => %s", source.toString(), destination.toString()));
		}
	}

	/**
	 * Runs through all regular files in the given directory 
	 * {@code dir} and returns a list of {@link FilterResult}
	 * objects representing all files that match the given
	 * {@code pattern}.
	 * 
	 * @param dir The directory in which to look for files
	 * @param pattern The pattern with which to match filenames
	 * @return A list of FilterResults that represent matching files
	 * @throws IOException if a {@link DirectoryStream} could not have been opened
	 */
	private static List<FilterResult> filter(Path dir, String pattern) throws IOException{
		DirectoryStream<Path> dst = Files.newDirectoryStream(dir);
		Pattern regex = Pattern.compile(pattern);
		List<FilterResult> list = new ArrayList<>();
		
		for(Path p : dst) {
			if(!Files.isRegularFile(p)) {
				continue;
			}
			String filename = p.getFileName().toString();
			Matcher m = regex.matcher(filename);
			if(m.matches()) {
				list.add(new FilterResult(filename, m));
			}
		}
		return list;
	}
	
	/**
	 * Computes the new names for all files given in the
	 * {@code oldNames} list of {@link FilterResult} objects
	 * using the given {@code expression}, returning them
	 * as a list of strings.
	 * 
	 * @param oldNames The files to rename using the expression
	 * @param expression The expression to use when renaming
	 * @return A list of new names for the given files
	 */
	private List<String> rename(List<FilterResult> oldNames, String expression) {
		List<String> newNames = new ArrayList<>();
		NameBuilderParser nbp = new NameBuilderParser(expression);
		NameBuilder builder = nbp.getNameBuilder();
		for(FilterResult file : oldNames) {
			StringBuilder sb = new StringBuilder(30);
			builder.execute(file, sb);
			newNames.add(sb.toString());
		}
		return newNames;
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getCommandName() {
		return MASS_CMD;
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<String> getCommandDescription() {
		return Arrays.asList(
				"This command supports filtering files by name,",
				"pattern grouping of filenames, and renaming and",
				"moving of multiple files at once.",
				"",
				"The command must receive a source directory and",
				"a destination directory (even if it is not used)",
				"along with a subcommand which can be one of the",
				"following:",
				"\t1) filter - prints all files that match",
				"\t\tthe given MASK pattern",
				"\t2) groups - prints all files that match",
				"\t\tthe given MASK pattern, along with",
				"\t\thow the filename matches all specific",
				"\t\tgroups of the pattern",
				"\t3) show - prints all files matching the",
				"\t\tgiven MASK pattern, along with how",
				"\t\tthey would be renamed using the given",
				"\t\tRENAME pattern",
				"\t4) execute - moves or renames all files",
				"\t\tthat match the given MASK pattern from",
				"\t\tDIR1 to DIR2 using the given RENAME",
				"\t\tpattern. DIR1 and DIR2 can be the same",
				"\t\tdirectory.",
				"",
				"If the used subcommand is 'filter' or 'groups' the",
				"RENAME pattern must not be present, and if the used",
				"subcommand is 'show' or 'execute', the RENAME pattern",
				"must be present.",
				"All other arguments are always required.",
				"",
				"The MASK argument is a standard regular expression,",
				"defining groups with the use of round brackets ().",
				"The RENAME pattern can contain a substitution expr-",
				"ession, defined like this:",
				"\t${GROUP NUMBER, SPECIFICATION}",
				"where GROUP NUMBER is mandatory and tells which",
				"group's matched value to retrieve.",
				"The SPECIFICATION parameter can define the minimal",
				"length for the retrieved matched value, and a character",
				"to pad with if the length is shorter.",
				"If provided, the SPECIFICATION parameter must be placed",
				"after the GROUP NUMBER, separated by a comma (,).",
				"If the SPECIFICATION begins with a 0, and has at least",
				"one more digit, the padding character is '0'. Otherwise,",
				"the padding character is a space (' ').",
				"The minimal length is defined as the number represented",
				"by the SPECIFICATION.",
				"",
				"Command syntax:",
				"> massrename [DIR1] [DIR2] [COMMAND] [MASK] [RENAME]?",
				"[RENAME] syntax:",
				" ${[GROUP_ID] [, SPECIFICATION]?}"
			);
	}

}
