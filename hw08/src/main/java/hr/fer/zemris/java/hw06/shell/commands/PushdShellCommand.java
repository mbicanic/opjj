package hr.fer.zemris.java.hw06.shell.commands;


import java.nio.file.Path;
import java.util.Arrays;
import java.util.List;
import java.util.Stack;

import hr.fer.zemris.java.hw06.shell.Environment;
import hr.fer.zemris.java.hw06.shell.ShellCommand;
import hr.fer.zemris.java.hw06.shell.ShellStatus;

import static hr.fer.zemris.java.hw06.shell.commands.Utility.DIR_STACK;

/**
 * PushdShellCommand is a command capable of changing the
 * current working directory of the shell, while storing the
 * previous working directory.
 * 
 * To correctly execute, it must receive exactly one argument 
 * representing a valid path to an existing directory that 
 * will become the new working directory.
 * 
 * Giving it any other number of arguments or an invalid argument 
 * (e.g. nonexistant directory) will stop the execution of the 
 * command, returning the control back to the Shell from which 
 * it was initially called. 
 * 
 * Assuming the current working directory is "C:\CurrentDir\"
 * the correct examples showing the syntax are:
 * 	{@code pushd C:\Users\Documents\}
 * 		-- changes the working directory to C:\Users\Documents,
 * 			stores C:\CurrentDir\ on a stack to enable access later
 * 	{@code pushd "My Folder"}
 * 		-- with the working directory now being C:\Users\Documents
 * 			and assuming "My Folder" exists in it, this will change
 * 			the working directory to "C:\Users\Documents\My Folder",
 * 			and store "C:\Users\Documents" on a stack to enable
 * 			access later.
 *
 * @author Miroslav Bićanić
 */
public class PushdShellCommand implements ShellCommand {

	/** Name of the PushdShellCommand */
	private static final String PUSHD_CMD = "pushd";
	
	/**
	 * {@inheritDoc}
	 * 
	 * @param arguments The path to the new working directory
	 * @return Always returns ShellStatus.CONTINUE
	 */
	@SuppressWarnings("unchecked")
	@Override
	public ShellStatus executeCommand(Environment env, String arguments) {
		try {
			ArgumentParser parser = new ArgumentParser(arguments, env, PUSHD_CMD);
			Path newDir = parser.nextDirectory();
			parser.noMoreArguments();
			
			Object o = env.getSharedData(DIR_STACK);
			Stack<Path> stk = (o==null ? new Stack<>() : (Stack<Path>)o);
			stk.push(env.getCurrentDirectory());
			env.setSharedData("cdstack", stk);
			
			env.setCurrentDirectory(newDir);
		} catch (IllegalArgumentException ex) {
			env.writeln(ex.getMessage());
		}
		return ShellStatus.CONTINUE;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getCommandName() {
		return PUSHD_CMD;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<String> getCommandDescription() {
		return Arrays.asList(
			"Pushes the current working directory to the",
			"directory stack, and sets the working directory",
			"to the directory given as the argument.",
			"",
			"This command takes exactly one argument - a valid",
			"path to an existing directory that will become the",
			"new working directory.",
			"",
			"Syntax:",
			"> pushd [PATH_TO_DIR]"
		);
	}
}
