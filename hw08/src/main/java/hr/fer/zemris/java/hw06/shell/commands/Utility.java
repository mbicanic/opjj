package hr.fer.zemris.java.hw06.shell.commands;

import java.util.Arrays;
import java.util.List;

import hr.fer.zemris.java.hw06.shell.Environment;

/**
 * Utility class provides various static utility methods and
 * constants used by classes within this package.
 * 
 * @author Miroslav Bićanić
 */
class Utility {
	/** This message is outputted any time an IOException is caught */
	static final String IO_ERROR = "I/O Exception occurred during file manipulation.";
	/** 
	 * The key under which pushed working directories are stored
	 * in the shared data.
	 */
	static final String DIR_STACK = "cdstack";
	
	/**
	 * Name of the Environment symbolic character for prompts.
	 */
	static final String PROMPT = "PROMPT";
	/**
	 * Name of the Environment symbolic character for a
	 * request for a command to span multiple lines.
	 */
	static final String MORE = "MORELINES";
	/**
	 * Name of the Environment symbolic character for the
	 * beginning of a line that is interpreted as a continuation
	 * of the previous line.
	 */
	static final String MULTI = "MULTILINE";
	
	/**
	 * A list of hexadecimal digits, where the index of each digit
	 * corresponds to its decimal value.
	 */
	private static List<Character> hexDigits = Arrays.asList('0', '1', '2', '3', '4', '5', '6', '7',
			'8', '9', 'A', 'B', 'C', 'D', 'E', 'F');

	/**
	 * Shorthand method to throw an IllegalArgumentException.
	 * @param message The message describing the error that caused the exception
	 */
	static void tnIAE(String message) {
		throw new IllegalArgumentException(message);
	}
	
	/**
	 * Prompts the user to answer a yes/no question.
	 * Each question is comprised of an information message informing
	 * the user of the reason for asking the question and the question
	 * itself.
	 * 
	 * @param message The reason the question is asked
	 * @param query The question itself
	 * @param env An environment through which to communicate with the user
	 * @return true only if the answer was positive, false otherwise
	 */
	static boolean queryYesNo(String message, String query, Environment env) {
		env.writeln(message);
		env.writeln(query+" [Y/N or yes/no]");
		env.write(env.getPromptSymbol()+" ");
		String ans = env.readLine().trim().toLowerCase();
		if(ans.equals("n") || ans.equals("no")) {
			return false;
		} else if(ans.equals("y")||ans.equals("yes")) {
			return true;
		} else {
			env.writeln("Expected a Y/N or yes/no answer.");;
			return false;
		}
	}
	
	/**
	 * Converts one byte to a hexadecimal digit couple
	 * @param b The byte to convert
	 * @return A pair of hex digits
	 */
	static String byteToHex(byte b) {
		int left = (b&0xF0)>>4;
		int right = (b&0x0F);
		return getHexDigit(left) + getHexDigit(right);
	}
	/**
	 * Returns the hexadecimal digit corresponding to the decimal
	 * number given to the method. The decimal number must be
	 * in the range [0,15], so that a corresponding hex digit
	 * woukld exist.
	 * 
	 * @param number The number from the [0,15] interval to convert to hex
	 * @return One hex digit representing the given number
	 */
	static String getHexDigit(int number) {
		if(number<0 || number>15) {
			tnIAE("Cannot convert "+number+" to one hex digit.!");
		}
		return hexDigits.get(number).toString();
	}
	/**
	 * Converts an arbitrary decimal number into a hexadecimal
	 * number.
	 * 
	 * @param number The number to convert to hexadecimal
	 * @return The hexadecimal representation of the number
	 */
	static String decToHex(int number) {
		StringBuilder sb = new StringBuilder(8);
		while(number>=0) {
			sb.append(getHexDigit(number%16));
			number/=16;
			if(number==0) {
				break;
			}
		}
		return sb.reverse().toString();
	}
}
