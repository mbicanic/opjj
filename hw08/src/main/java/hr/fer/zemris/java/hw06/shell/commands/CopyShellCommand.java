package hr.fer.zemris.java.hw06.shell.commands;

import static hr.fer.zemris.java.hw06.shell.commands.Utility.*;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;

import hr.fer.zemris.java.hw06.shell.Environment;
import hr.fer.zemris.java.hw06.shell.ShellCommand;
import hr.fer.zemris.java.hw06.shell.ShellStatus;

/**
 * CopyShellCommand is a command that reads a file and copies
 * its content into another file (existing or not).
 * 
 * To correctly execute, it must receive exactly two arguments:
 * 	1) A valid path to an existing file to be copied - source
 * 	2) A valid path to a file (existing or not) located in an
 * 		existing directory, or to an existing directory - destination
 * Giving the command any other number of arguments, or an illegal argument
 * stops the command's execution and returns the control to the Shell from
 * which the command was called.
 * 
 * If the destination file does not exist, it will be created. 
 * If it exists, the command will ask for user's permission to overwrite 
 * its content.
 * When the second given argument is a path to an existing folder,
 * the created file will be named the same as the source file.
 * 
 * To clarify, let's say we already have the following structure
 * in our file system:
 * 
 * src					[DIR]
 * |--Folder One		[DIR]
 * |  |--example.txt	[FILE]
 * |  |--InsideFolder	[DIR]
 * |--FolderB			[DIR]
 * |--file.txt			[FILE]
 * 
 * Following command examples illustrate how the command works,
 * simultaneously showing the accepted syntax (working with the
 * directory structure from above):
 *
 * {@code copy src/file.txt "src/Folder One/InsideFolder/myFile.txt}
 * 		-- copies the content of 'file.txt' to a newly created 'myFile.txt'
 * 			in the already existing directory structure
 * {@code copy src/file.txt src/FolderB}
 * 		-- copies the content of 'file.txt' to a new file called 'file.txt'
 * 			in the already existing directory structure
 * {@code copy src/file.txt "src/Folder One/example.txt"}
 * 		-- copies the content of 'file.txt' to the existing 'example.txt'
 * 			if the user permits it by typing 'Y' or 'yes' when prompted
 * {@code copy src/file.txt src/FolderC}
 * 		-- copies the content of 'file.txt' to a new file called 'FolderC'.
 * 			The created file does not have an extension, and creating such
 * 			files is not advised - the user will be asked to agree with 
 * 			creating such a file.
 * 
 * The structure after these commands will look like this:
 * 
 * src						[DIR]
 * |--Folder One			[DIR]
 * |  |--example.txt		[FILE]	//overwritten if user permitted
 * |  |--InsideFolder		[DIR]
 * |     |--myFile.txt		[FILE]
 * |--FolderB				[DIR]
 * |  |--file.txt			[FILE]
 * |--file.txt				[FILE]
 * |--FolderC				[FILE]	//only if the user permitted
 *
 * Please note: if a file without an extension is present in the file system,
 * 				a folder of the same name cannot be created next to it.
 *
 * @author Miroslav Bićanić
 */
public class CopyShellCommand implements ShellCommand {
	
	/** 
	 * The size of the buffer array used to read from
	 * the source and write to the destination.
	 */
	private static final int BUFFER = 4096;
	
	/** Name of the CopyShellCommand */
	private static final String COPY_CMD = "copy";
	
	/**
	 * {@inheritDoc}
	 * @param arguments A path to a source file and a path to a destination file or directory
	 * @return Always returns ShellStatus.CONTINUE
	 */
	@Override
	public ShellStatus executeCommand(Environment env, String arguments) {
		try {
			ArgumentParser parser = new ArgumentParser(arguments, env, COPY_CMD);
			Path srcFile = parser.nextFile();
			String destName = parser.nextMandatoryArgument();
			parser.noMoreArguments();
			
			Path destFile = getTrueDestination(destName, srcFile, env);
			checkExtension(destFile.getFileName().toString(), env);
			copyContent(srcFile, destFile);
			
			env.writeln("Successfully copied!");
		} catch (IllegalArgumentException ex) {
			env.writeln(ex.getMessage());
		} catch (IOException ex) {
			env.writeln(IO_ERROR);
		}
		return ShellStatus.CONTINUE;
	} 

	/**
	 * Checks if the desired destination exists, and updates the path with
	 * the source's filename if the destination is a directory.
	 * 
	 * @param destName The destination path desired by the user
	 * @param src The path to the source file
	 * @param env An environment thorugh which to communicate
	 * @return A destination path ready for receiving the copied content
	 * @throws IOException - see method checkExistence
	 */
	private Path getTrueDestination(String destName, Path src, Environment env) throws IOException {
		Path tempDest = env.getCurrentDirectory().resolve(Paths.get(destName));
		if(!Files.exists(tempDest.getParent())) {
			tnIAE("Given directory structure does not exist.");
		}
		if(destName.endsWith(File.separator) || destName.endsWith("/")) { //universal separator
			if(!Files.exists(tempDest)) {
				tnIAE("Given directory structure does not exist.");
			}
		}
		if(Files.isDirectory(tempDest)) {
			tempDest = Paths.get(tempDest.toString(), src.getFileName().toString());
		}
		return checkExistence(tempDest, env);
	}

	/**
	 * Checks if the file itself exists, and asks for user's
	 * permission to overwrite it if it does.
	 * 
	 * @param dest The destination file
	 * @param env The environment through which communication with the user is realized
	 * @return A Path pointing to the destination file
	 * @throws IOException if user agreed to overwrite, but deleting the existing
	 * 						file could not be completed
	 * @throws IllegalArgumentException if the user responded incorrectly, or refused
	 * 			permission to overwrite a file
	 */
	private Path checkExistence(Path dest, Environment env) throws IOException {
		if(Files.exists(dest)) {
			boolean answer = queryYesNo(
					"Destination file '"+dest.getFileName()+"' already exists.",
					"Do you want to overwrite it?",
					env);
			if(!answer) {
				tnIAE("File not copied.");
			}
			Files.delete(dest);
		}
		return dest;
	}

	/**
	 * Checks if the desired filename contains an extension,
	 * and asks the user's permission to create such a file
	 * if it doesn't.
	 * 
	 * @param filename The filename to check
	 * @param env The environment through which to communicate
	 * @throws IllegalArgumentException if the user denies permission,
	 * 			or his answer is illegal.
	 */
	private void checkExtension(String filename, Environment env) {
		if(!filename.contains(".")) {
			boolean ans = queryYesNo(
					"Creating files without extensions is not recommended.",
					"Do you want to proceed?",
					env);
			if(!ans) {
				tnIAE("File not copied.");
			}
		}
	}

	/**
	 * Copies the byte content from the {@code srcFile} to the 
	 * {@code destFile} using a buffered byte array.
	 * 
	 * @param srcFile The file from which to copy content
	 * @param destFile The file to which to copy content
	 * @throws IOException if either the input or output stream
	 * 			could not have been opened 
	 */
	private void copyContent(Path srcFile, Path destFile) throws IOException {
		try(OutputStream os = Files.newOutputStream(destFile);
				InputStream is = Files.newInputStream(srcFile)) {
			byte[] buff = new byte[BUFFER];
			while(true) {
				int r = is.read(buff);
				if(r<1) break;
				os.write(buff, 0, r);;
			}
		}
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getCommandName() {
		return COPY_CMD;
	}
	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<String> getCommandDescription() {
		return Arrays.asList(
				"Copies the content from the source file and creates a",
				"new file with equal content.",
				"",
				"It takes two arguments - a valid path to an existing file",
				"that is the source (and MUST be a file, directory copying",
				"not supported).",
				"The second argument must be a path to the destination file",
				"or directory:",
				"1) If given a path to a non-existant file in an existing",
				"\tdirectory structure, the file is created in the structure",
				"2) If given a path to an already existing file, the user is",
				"\tasked for permission to overwrite it.",
				"3) If given a path to an existing directory, then the source",
				"\tfile is copied to the given destination directory, using the",
				"\tsame name as the original source file.",
				"",
				"Non-existing directories will not be created by this command.",
				"",
				"Syntax:",
				"> copy [SOURCE FILE] [DESTINATION FILE | DESTINATION DIRECTORY]",
				""
			);
	}

}
