package hr.fer.zemris.java.hw06.shell.commands;

import static hr.fer.zemris.java.hw06.shell.commands.Utility.*;

import java.util.Arrays;
import java.util.List;

import hr.fer.zemris.java.hw06.shell.Environment;
import hr.fer.zemris.java.hw06.shell.ShellCommand;
import hr.fer.zemris.java.hw06.shell.ShellStatus;

/**
 * SymbolShellCommand is a command enabling the user to edit
 * the symbolic characters of the shell.
 * 
 * To correctly execute it needs at least one argument 
 * representing one of the features that have symbolic characters.
 * In this case, the currently used symbolic character for
 * that feature is outputted.
 * 
 * Additionally, a second argument can be given after the first one,
 * representing a new character for the specified feature.
 * If the new value for a feature is already used by another feature,
 * it will not be updated.
 * 
 * Giving it any other number of arguments or invalid arguments
 * stops the execution of the command and returns the control back
 * to the shell that called it.
 * 
 * Correct examples showing the syntax:
 * 	{@code symbol PROMPT}
 * 		-- outputs the current PROMPT symbolic character
 * 	{@code symbol MORELINES !}
 * 		-- changes the symbolic character value of MORELINES
 * 			to a '!'
 * 
 * @author Miroslav Bićanić
 */
public class SymbolShellCommand implements ShellCommand {

	/** Name of the SymbolShellCommand */
	private static final String SYM_CMD = "symbol";
	
	/**
	 * {@inheritDoc}
	 * @param arguments The name of the feature whose symbol to display or update
	 * 					if followed by a second argument that is a character.
	 * @return Always returns ShellStatus.CONTINUE
	 */
	@Override
	public ShellStatus executeCommand(Environment env, String arguments) {
		ArgumentParser parser = new ArgumentParser(arguments, env, SYM_CMD);
		String name = parser.nextMandatoryArgument();
		String sym = parser.nextArgument();
		parser.noMoreArguments();
		
		Character oldSymbol = getSymbol(name, env);
		if(sym==null) {
			env.writeln("Symbol for "+name+" is "+oldSymbol+".");
			return ShellStatus.CONTINUE;
		}
		
		setSymbol(name, oldSymbol.toString(), sym, env);
		return ShellStatus.CONTINUE;
	}

	/**
	 * Returns the currently used symbolic character for the given
	 * {@code feature} in the given Environment {@code env}.
	 * 
	 * @param feature The name of the feature whose symbol to return
	 * @param env The environment storing the current symbol for the feature
	 * @return The currently used symbol for the given feature
	 * @throws IllegalArgumentException if the given feature is not recognized
	 */
	private Character getSymbol(String feature, Environment env) {
		switch(feature) {
		case PROMPT:
			return env.getPromptSymbol();
		case MULTI:
			return env.getMultilineSymbol();
		case MORE:
			return env.getMorelinesSymbol();
		default:
			tnIAE("Unrecognized name of the symbol.");
			return null;
		}
	}
	
	/**
	 * Sets the symbolic character for the given {@code feature} to the
	 * {@code givenSymbol} in the given Environment {@code env}.
	 * 
	 * @param feature The name of the feature whose symbol to update
	 * @param oldSymbol The previously used symbol for that feature
	 * @param givenSymbol The new symbol for that feature
	 * @param env The environment storing the symbols for features
	 * @throws IllegalArgumentException if the {@code givenSymbol} isn't a string with length = 1.
	 */
	private void setSymbol(String feature, String oldSymbol, String givenSymbol, Environment env) {
		if(givenSymbol.length()!=0) {
			tnIAE("New symbol for "+feature+" must be exactly one character long.");
		}
		Character newSymbol = givenSymbol.charAt(0);
		switch(feature) {
		case PROMPT:
			env.setPromptSymbol(newSymbol);
			break;
		case MULTI:
			env.setMultilineSymbol(newSymbol);
			break;
		case MORE:
			env.setMorelinesSymbol(newSymbol);
			break;
			//no default: since it would already be caught in getSymbol().
		}
		env.writeln(String.format("Symbol for %s changed from '%s' to '%s'.", feature, oldSymbol, givenSymbol));
	}


	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getCommandName() {
		return SYM_CMD;
	}
	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<String> getCommandDescription() {
		return Arrays.asList(
				"This command retrieves MyShell's special characters",
				"or updates them to a new value, depending on how the",
				"command is called.",
				"The command can be called with one argument that must",
				"be either 'PROMPT', 'MULTILINE' or 'MORELINES', which",
				"outputs the symbol currently in use for the selected",
				"feature.",
				"If after the feature name, a single character symbol",
				"is given, then that feature's symbol is updated to the",
				"given symbol.",
				"",
				"Examples:",
				"symbol PROMPT   \\\\outputs the current symbol for PROMPT",
				"symbol PROMPT # \\\\updates the symbol for PROMPT to '#'",
				"",
				"Syntax:",
				"> symbol [FEATURE] [NEW CHARACTER]?",
				""
			);
	}
}
