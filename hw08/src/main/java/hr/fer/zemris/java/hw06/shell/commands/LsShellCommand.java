package hr.fer.zemris.java.hw06.shell.commands;

import static hr.fer.zemris.java.hw06.shell.commands.Utility.*;

import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.nio.file.attribute.BasicFileAttributeView;
import java.nio.file.attribute.BasicFileAttributes;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import hr.fer.zemris.java.hw06.shell.Environment;
import hr.fer.zemris.java.hw06.shell.ShellCommand;
import hr.fer.zemris.java.hw06.shell.ShellStatus;

/**
 * LsShellCommand is a command that outputs the content of
 * a given directory, displaying the properties of each file
 * or directory in it. 
 * Unlike the 'tree' command, it does not traverse the whole 
 * structure recursively.
 * 
 * To correctly execute, it must receive exactly one argument
 * - a valid path to an existing directory, whose content it
 * will list.
 * 
 * Giving it any other number of arguments stops the execution
 * of the command, returning the control back to the Shell that
 * called the command.
 * 
 * Correct examples showing the syntax:
 * 	{@code ls C:\Documents\Folder1\}
 * 		-- outputs all files and folders present in Folder1, along with
 * 			their properties
 * 	{@code ls "C:\Example Folder\Temp"}
 * 		-- outputs all files and folders present in Temp, along with
 * 			their properties.
 * 
 * The output is formatted as follows:
 * {@code [ATTRS] [SIZE] [DATE OF CREATION] [NAME]}
 * 
 * The [ATTRS] field represents a string of four characters:
 * 	'd' - indicates that it is a directory
 * 	'r' - indicates that it is readable
 * 	'w' - indicates that it is writable
 * 	'x' - indicates that it is executable
 * The corresponding characters are written if the condition is met.
 * If not, a '-' character is written instead of the letter.
 * 
 * Size of both files and folders is expressed in bytes, however 
 * please note that the shown size of a folder is not the size of
 * the folder and all its contents. Instead, it only represents the
 * size of that folder's record in the platform's file system.
 * 
 * Date of creation is outputted in the format:
 * {@code YYYY-MM-DD hh:mm:ss}
 * 
 * @author Miroslav Bićanić
 */
public class LsShellCommand implements ShellCommand {
	/** Name of the LsShellCommand */
	private static final String LS_CMD = "ls";
	
	/**
	 * {@inheritDoc}
	 * @param arguments One valid path to an existing directory
	 * @return Always returns ShellStatus.CONTINUE
	 */
	@Override
	public ShellStatus executeCommand(Environment env, String arguments) {
		try {
			ArgumentParser parser = new ArgumentParser(arguments, env, LS_CMD);
			Path dir = parser.nextDirectory();
			parser.noMoreArguments();
			
			DirectoryStream<Path> dst = Files.newDirectoryStream(dir);
			for(Path path : dst) {
				env.writeln(getAttributes(path).getFormatted());
			}
			dst.close();
		} catch (IllegalArgumentException ex) {
			env.writeln(ex.getMessage());
			return ShellStatus.CONTINUE;
		} catch (IOException ex) {
			env.writeln(IO_ERROR);
		}
		return ShellStatus.CONTINUE;
	}
	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getCommandName() {
		return LS_CMD;
	}
	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<String> getCommandDescription() {
		return Arrays.asList(
				"This command lists the contents of the given directory,",
				"with attributes of each entry shown alongside it.",
				"It takes exactly one argument - path to a directory.",
				"",
				"The output list is in the format:",
				"[PROPERTIES] [SIZE(BYTES)] [DATE OF CREATION] [NAME]",
				"",
				"Syntax:",
				"> ls [PATH_TO_DIRECTORY]",
				""
		);
	}
	
	/**
	 * Factory method for an Attributes object, building
	 * it from a given path.
	 * 
	 * @param p The path to a file system entry whose attributes
	 * 			to fetch
	 * @return An Attributes object containing all relevant information
	 * 			about the file
	 * @throws IllegalArgumentException if creation of Attributes from the
	 * 				given Path was unsuccessful
	 */
	private Attributes getAttributes(Path p) {
		try {
			return new Attributes(p);
		} catch (IOException ex) {
			tnIAE(IO_ERROR);
			return null; //will never execute, but Eclipse does not recognize tnIAE as an exception.
		}
	}
	
	/**
	 * Helper class storing the attributes of an entry in the file system:
	 * 	1) The file's properties ('drwx')
	 * 	2) The file's size
	 * 	3) The date of creation of the file
	 * 	4) The name of the file
	 * 
	 * @author Miroslav Bićanić
	 */
	private static class Attributes {
		/**
		 * Properties of the entry:
		 * 	(d)irectory, (r)eadable, (w)ritable, e(x)ecutable
		 */
		private String properties;
		/**
		 * The size of the entry in the file system, expressed in bytes
		 */
		private long size;
		/**
		 * The date of creation of the entry in the file system.
		 */
		private String date;
		/**
		 * The name of the entry in the file system
		 */
		private String name;
		
		/**
		 * A constructor for Attributes
		 * @param p The path to a file system entry whose attributes
		 * 				will be stored
		 * @throws IOException - if an I/O error occurs while reading
		 * 							the size of the entry
		 */
		private Attributes(Path p) throws IOException {
			this.date = getTimestamp(p);
			this.name = p.getFileName().toString();
			this.properties = getProperties(p);
			this.size = Files.size(p);
		}
		/**
		 * Retrieves the timestamp of the creation of an entry in the 
		 * file system.
		 * 
		 * @param p The path to the file system entry
		 * @return A timestamp of its creation
		 * @throws IOException - if the creation time could not have been read
		 */
		private String getTimestamp(Path p) throws IOException {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			BasicFileAttributeView faView = Files.getFileAttributeView(p, BasicFileAttributeView.class, LinkOption.NOFOLLOW_LINKS);
			BasicFileAttributes attrs = faView.readAttributes();
			return sdf.format(new Date(attrs.creationTime().toMillis()));
		}
		/**
		 * Reads the four properties of a given file system entry
		 * and builds a string representation of them.
		 * 
		 * @param p A path to a file system entry
		 * @return The properties of the entry
		 */
		private String getProperties(Path p) {
			StringBuilder sb = new StringBuilder(4);
			sb.append((Files.isDirectory(p) ? 'd' : '-'))
			.append((Files.isReadable(p) ? 'r' : '-'))
			.append((Files.isWritable(p) ? 'w' : '-'))
			.append((Files.isExecutable(p) ? 'x' : '-'));
			return sb.toString();
		}
		/**
		 * Formats all the stored attributes into a String ready
		 * to be outputted.
		 * 
		 * @return A string representing all the data for one file system entry
		 */
		private String getFormatted() {
			StringBuilder sb = new StringBuilder(100);
			sb.append(properties).append(" ");
			
			String strSize = Long.toString(size);
			sb.append(" ".repeat(10-strSize.length())).append(strSize)
			.append(" ").append(date).append(" ").append(name);
			return sb.toString();
		}
	}
}
