package hr.fer.zemris.java.hw06.shell.commands;

import static hr.fer.zemris.java.hw06.shell.commands.Utility.*;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.List;
import java.util.Stack;

import hr.fer.zemris.java.hw06.shell.*;

/**
 * PopdShellCommand is a command that pops the last directory
 * stored by the {@link PushdShellCommand}, setting it as the
 * current working directory if it wasn't removed in the meantime.
 * 
 * To correctly execute, it must be called with no arguments.
 * Giving it any arguments will stop the execution of the 
 * command, returning the control back to the Shell from which 
 * it was initially called. 
 * 
 * If no directories were pushed to the stack yet, or the stack
 * was emptied, the command reports an error and does nothing.
 * 
 * The only way to call this command is:
 * 	{@code popd}
 * 		-- removes one directory from the stack and sets it
 * 			as current working directory if possible.
 *
 * @author Miroslav Bićanić
 */
public class PopdShellCommand implements ShellCommand {

	/** Name of the PopdShellCommand */
	private static final String POPD_CMD = "popd";
	
	/**
	 * {@inheritDoc}
	 * 
	 * @param arguments Must be an empty string for this command to execute correctly
	 * @return Always returns ShellStatus.CONTINUE
	 */
	@SuppressWarnings("unchecked")
	@Override
	public ShellStatus executeCommand(Environment env, String arguments) {
		try {
			if(arguments.length()!=0) {
				tnIAE("Too many arguments given with command: 'popd'. Expected 0.");
			}
			Object o = env.getSharedData(DIR_STACK);
			if(o==null) {
				tnIAE("Stack of current directories does not exist.");
			}
			Stack<Path> p = (Stack<Path>)o;
			if(p.isEmpty()) {
				tnIAE("Stack of current directories is empty.");
			}
			Path popped = p.pop();
			if(Files.isDirectory(popped)) {
				env.setCurrentDirectory(popped);
			}
		} catch (IllegalArgumentException ex) {
			env.writeln(ex.getMessage());
		}
		return ShellStatus.CONTINUE;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getCommandName() {
		return POPD_CMD;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<String> getCommandDescription() {
		return Arrays.asList(
			"Pops one directory from the directories stack,",
			"setting it as the current working directory if",
			"it still exists.",
			"",
			"This command takes no arguments. Syntax:",
			"> popd"
		);
	}

}
