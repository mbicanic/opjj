package hr.fer.zemris.java.hw08.shell.builder;

/**
 * DefaultNameBuilders offers several factory methods
 * for different implementations of NameBuilder objects.
 * 
 * @author Miroslav Bićanić
 */
public class DefaultNameBuilders {

	/**
	 * A factory method creating a NameBuilder that builds
	 * a name by appending a constant string value to it.
	 * 
	 * @param t The constant to append to the name being built
	 * @return The NameBuilder which will append the given constant
	 */
	public static NameBuilder literal(String t) {
		return (rec, sb) -> sb.append(t);
	}
	/**
	 * A factory method creating a NameBuilder that builds
	 * a name by appending the matched value of the group
	 * at position {@code index} from the old name.
	 * 
	 * @param index The index of the pattern group whose value
	 * 				to take from the old name
	 * @return The NameBuilder which will append the group value
	 */
	public static NameBuilder grouper(int index) {
		return (rec,sb)->sb.append(rec.group(index));
	}
	/**
	 * A factory method creating a NameBuilder that builds
	 * a name by appending the matched value of the group
	 * at position {@code index} from the old name.
	 * 
	 * It does so allowing more specificatons:
	 * {@code minWidth} decides what is the minimal width the
	 * 		retrieved group value must occupy. In case the value
	 * 		is larger than the number, it will occupy as much as
	 * 		it needs. If it is shorter, the value will be preceeded
	 * 		by the padding character.
	 * {@code padding} is the character to use when padding, and it
	 * 		can be either a '0' or a space (' ').
	 * 
	 * @param index The index of the pattern group whose value
	 * 				to take from the old name
	 * @param padding The character to pad group values which are too short
	 * @param minWidth The minimal width of the group value
	 * @return The NameBuilder which will append the group value
	 */
	public static NameBuilder grouper(int index, char padding, int minWidth) {
		return (rec,sb)-> {
			String value = rec.group(index);
			int i = minWidth-value.length();
			while(i>0) {
				sb.append(padding);
				i--;
			}
			sb.append(value);
		};
	}
	/**
	 * A factory method for a composite NameBuilder which
	 * contains a NameBuilder for each literal and group
	 * segment of the renaming pattern.
	 * 
	 * @param builders All NameBuilders required to build a name
	 * 			according to the parsed renaming expression
	 * @return A composite NameBuilder building the full name
	 */
	public static NameBuilder composite(NameBuilder...builders) {
		return (rec,sb)->{
			for(NameBuilder n : builders) {
				n.execute(rec, sb);
			}
		};
	}
}
