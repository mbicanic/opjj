
package hr.fer.zemris.java.hw06.shell.commands;

import static hr.fer.zemris.java.hw06.shell.commands.Utility.*;

import java.util.Arrays;
import java.util.List;

import hr.fer.zemris.java.hw06.shell.Environment;
import hr.fer.zemris.java.hw06.shell.ShellCommand;
import hr.fer.zemris.java.hw06.shell.ShellStatus;

/**
 * HelpShellCommand is a command that outputs a list of all
 * available commands or information about a specific command.
 * 
 * When the command is called with no arguments, a list of
 * commands is outputted.
 * If a single argument is provided, it can only be one of
 * the commands listed by the help command called with no
 * arguments (written identically the same).
 * 
 * Giving it any other number of arguments stops its execution
 * and returns control to the Shell from which it was called.
 * 
 * @author Miroslav Bićanić
 */
public class HelpShellCommand implements ShellCommand {

	/** Name of the HelpShellCommand */
	private static final String HELP_CMD = "help";
	
	/**
	 * {@inheritDoc}
	 * @param arguments Empty, or one command name whose description to output
	 * @return Always returns ShellStatus.CONTINUE
	 */
	@Override
	public ShellStatus executeCommand(Environment env, String arguments) {
		try {
			ArgumentParser parser = new ArgumentParser(arguments, env, HELP_CMD);
			String cmd = parser.nextArgument();
			if(cmd==null) {
				env.writeln("List of supported commands:");
				env.commands().entrySet().stream()
				.forEach((entry)->env.writeln("\t-"+entry.getValue().getCommandName()));
				return ShellStatus.CONTINUE;
			}
			parser.noMoreArguments();
			
			ShellCommand command = env.commands().get(cmd);
			if(command==null) {
				tnIAE("Invalid argument for 'help' command: '"+cmd+"' - given command does not exist.");
			}
			
			env.writeln(command.getCommandName()+":");
			command.getCommandDescription().forEach((line)->env.writeln("\t"+line));
		} catch (IllegalArgumentException ex) {
			env.writeln(ex.getMessage());
		} 
		return ShellStatus.CONTINUE;
	}
	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getCommandName() {
		return HELP_CMD;
	}
	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<String> getCommandDescription() {
		return Arrays.asList(
				"Lists all the available commands if called with no arguments,",
				"or information about a specific command whose name is given as",
				"an argument.",
				"",
				"If called with an argument, the given argument must be a valid",
				"command name, written exactly as it would be written when calling",
				"help with no arguments. This will output a description of the",
				"command, along with the correct syntax of the specified command.",
				"",
				"When showing a syntax of a command, values surrounded with square",
				"brackets [ ] aren't to be written literally - they only show what",
				"should be written in place of it. If the bracket-surrounded value",
				"is followed by a question mark '?', it means the argument is",
				"optional. If within the square brackets two different values are",
				"separated by an '|' symbol, it means either one of them can be given.",
				"The syntax will always include the default prompt symbol '>', to",
				"indicate that the command must be written into a new prompt.",
				"",
				"For example, the syntax for this command would be written like this:",
				"> help [COMMAND_NAME]?",
				""
			);
	}

}
