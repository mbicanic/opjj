package searching.demo;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import searching.algorithms.Node;
import searching.algorithms.SearchUtil;
import searching.slagalica.KonfiguracijaSlagalice;
import searching.slagalica.Slagalica;
import searching.slagalica.gui.SlagalicaViewer;

/**
 * Demonstration program for our simple AI solving
 * a puzzle using various state search algorithms.
 * 
 * If a solution is found (there are unsolvable 
 * puzzles), the solution is printed to the console,
 * and a graphical interface is opened that shows
 * a controllable animation of the solving process.
 * 
 * Some solvable combinations:
 * 135724680	-	18 moves
 * 230146758	-	6 moves
 * 
 * An unsolvable combination:
 * 164502873
 * 
 * @author Miroslav Bićanić
 */
public class SlagalicaMain {
	/**
	 * The entry point of the program
	 * @param args A string of 9 digits containing all digits [0-9]
	 */
	public static void main(String[] args) {
		try {
			checkArgs(args);
		} catch (IllegalArgumentException ex) {
			System.out.println(ex.getMessage());
			return;
		}
		
		int[] conf = convert(args[0]);
		Slagalica slagalica = new Slagalica(new KonfiguracijaSlagalice(conf));
		Node<KonfiguracijaSlagalice> rješenje = SearchUtil.bfsv(slagalica, slagalica, slagalica);
		
		if(rješenje==null) {
			System.out.println("Nisam uspio pronaći rješenje.");
		} else {
			System.out.println("Imam rješenje. Broj poteza je: " + rješenje.getCost());
			List<KonfiguracijaSlagalice> lista = new ArrayList<>();
			Node<KonfiguracijaSlagalice> trenutni = rješenje;
			while(trenutni != null) {
				lista.add(trenutni.getState());
				trenutni = trenutni.getParent();
			}
			Collections.reverse(lista);
			lista.stream().forEach(k -> {
				System.out.println(k);
				System.out.println();
			});
			SlagalicaViewer.display(rješenje);
		}
	}

	/**
	 * Checks the arguments given to the program through
	 * the command line, throwing an IllegalArgumentException
	 * if they are incorrect.
	 * 
	 * @param args The arguments given through the command line
	 */
	private static void checkArgs(String[] args) {
		if(args.length!=1) {
			throw new IllegalArgumentException("Expected one argument containing 9 digits for configuration.");
		}
		if(args[0].length()!=9) {
			throw new IllegalArgumentException("The argument must be 9 digits long.");
		}
		if(!containsAll(args[0])) {
			throw new IllegalArgumentException("The argument must contain all 9 digits [0-9].");
		}
	}

	/**
	 * Converts a string of 9 digits into an integer
	 * array of the same length, keeping the original
	 * positions of digits in the array.
	 * 
	 * @param string The string to convert 
	 * @return An integer array of digits from the string
	 */
	private static int[] convert(String string) {
		int[] res = new int[9];
		for(int i = 0; i<9; i++) {
			res[i] = Integer.parseInt(string.substring(i,i+1));
		}
		return res;
	}

	/**
	 * Checks if a string contains all 9 digits [0-9].
	 * @param string The string to check
	 * @return true if all 9 digits are present, false otherwise
	 */
	private static boolean containsAll(String string) {
		boolean[] corresponding = new boolean[9];
		for(int i=0; i<9; i++) {
			if(Character.isDigit(string.charAt(i))) {
				corresponding[Integer.parseInt(string.substring(i,i+1))] = true;
			} else {
				return false;
			}
		}
		for(boolean b : corresponding) {
			if(!b) {
				return false;
			}
		}
		return true;
	}
}
