package searching.slagalica;

import java.util.Arrays;

//JAVADOC NA HRVATSKOM ZBOG PROPISANOG HRVATSKOG NAZIVA RAZREDA
//Sam zadatak je zadan tako da kombinira hrvatski i engleski 
//npr getPolje()...

/**
 * KonfiguracijaSlagalice je razred koji predstavlja jednu
 * instancu slagalice u nekom trenutku, odnosno njen 
 * razmještaj elemenata.
 * 
 * @author Miroslav Bićanić
 */
public class KonfiguracijaSlagalice {
	/** 
	 * Deveteročlano polje brojeva koje predstavlja
	 * polja slagalice
	 */
	private int[] config;
	
	/**
	 * Konstruktor za KonfiguracijuSlagalice
	 * @param config Početna konfiguracija slagalice
	 */
	public KonfiguracijaSlagalice(int[] config) {
		this.config = config;
	}
	/**
	 * Vraća kopiju polja brojeva koja predstavlja trenutnu
	 * konfiguraciju slagalice.
	 * 
	 * @return Kopija polja brojeva
	 */
	public int[] getPolje() {
		return Arrays.copyOf(config, config.length);
	}
	/**
	 * Vraća indeks na kojem se nalazi broj 0 u polju, što
	 * predstavlja prazno polje na slagalici.
	 * 
	 * @return Indeks praznog polja u slagalici
	 */
	public int indexOfSpace() {
		int i;
		for(i = 0; i < config.length; i++) {
			if(config[i]==0) {
				break;
			}
		}
		return i;
	}
	/**
	 * Vraća prikaz trenutne konfiguracije slagalice
	 * u obliku 3x3 matrice u kojoj je prazno polje
	 * prikazano znakom '*'.
	 */
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder(20);
		int ios = indexOfSpace();
		for(int i = 0; i < config.length; i++) {
			String separator = ((i+1)%3==0 ? "\n" : " ");
			sb.append((i==ios) ? "*" : config[i]).append(separator);
		}
		String res = sb.toString();
		return res.substring(0, res.length()-1); //uklanjanje zadnjeg novog reda
	}
	/**
	 * Dvije konfiguracije su jednake samo ako su im
	 * svi brojevi na odgovarajućim indeksima jednaki.
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof KonfiguracijaSlagalice))
			return false;
		KonfiguracijaSlagalice other = (KonfiguracijaSlagalice) obj;
		return Arrays.equals(config, other.config);
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Arrays.hashCode(config);
		return result;
	}
}
