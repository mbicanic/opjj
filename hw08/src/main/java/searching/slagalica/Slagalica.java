package searching.slagalica;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;

import searching.algorithms.Transition;

//JAVADOC NA HRVATSKOM ZBOG PROPISANOG HRVATSKOG NAZIVA RAZREDA
//Sam zadatak je zadan tako da kombinira hrvatski i engleski 
//npr getPolje()...

/**
 * Slagalica je razred koji implementira sva četiri
 * potrebna sučelja za algoritme pretraživanja prostora.
 * 
 * Kao sučelje Supplier, ovaj razred metodom {@link Slagalica#get()}
 * vraća polaznu konfiguraciju slagalice.
 * 
 * Kao sučelje Function, ovaj razred metodom 
 * {@link Slagalica#apply(KonfiguracijaSlagalice)} vraća listu svih
 * mogućih prijelaza u nove konfiguracije iz trenutne konfiguracije.
 * 
 * Kao sučelje Predicate, ovaj razred metodom 
 * {@link Slagalica#test(KonfiguracijaSlagalice)} vraća true ako je
 * slagalica riješena, odnosno false ako nije.
 * 
 * Slagalica se smatra riješenom kada su brojevi postavljeni 
 * uzlazno s lijeva na desno, od gore prema dolje, počevši od 
 * broja 1 u gornjem lijevom uglu, a prazno polje mora biti 
 * posljednje u donjem desnom uglu.
 * 
 * @author Miroslav Bićanić
 *
 */
public class Slagalica implements
			Supplier<KonfiguracijaSlagalice>,
			Function<KonfiguracijaSlagalice, List<Transition<KonfiguracijaSlagalice>>>,
			Predicate<KonfiguracijaSlagalice> {

	/** Početna konfiguracija slagalice */
	private KonfiguracijaSlagalice initial;
	
	/**
	 * Konstruktor za Slagalicu
	 * @param init Početna konfiguracija slagalice
	 */
	public Slagalica(KonfiguracijaSlagalice init) {
		this.initial = init;
	}

	@Override
	public KonfiguracijaSlagalice get() {
		return initial;
	}
	
	@Override
	public boolean test(KonfiguracijaSlagalice t) {
		int[] arr = t.getPolje();
		if(t.indexOfSpace()!=arr.length-1) {
			return false;
		}
		for(int i = 0; i<arr.length-1; i++) {
			if(arr[i]!=i+1) {
				return false;
			}
		}
		return true;
	}

	@Override
	public List<Transition<KonfiguracijaSlagalice>> apply(KonfiguracijaSlagalice t) {
		List<Transition<KonfiguracijaSlagalice>> list = new ArrayList<>();
		int space = t.indexOfSpace();
		List<Integer> indices = setUpIndices(space);
		for(Integer i : indices) {
			list.add(swap(t.getPolje(), i, t.indexOfSpace()));
		}
		return list;
	}

	/**
	 * Vraća Transition u KonfiguracijuSlagalice čije je polje 
	 * brojeva kao i polje trenutne slagalice, uz zamjenu mjesta
	 * nule (praznog polja) i nekog od njenih susjeda (gledajući
	 * dvodimenzionalno).
	 * 
	 * @param arr Polje u kojem se zamjenjuju mjesta
	 * @param toSwap Indeks broja kojeg se mijenja nulom
	 * @param space Indeks na kojem se nalazi nula
	 * @return Transition koji predstavlja prijelaz u novu konfiguraciju slagalice
	 */
	private Transition<KonfiguracijaSlagalice> swap(int[] arr, int toSwap, int space) {
		int temp = arr[toSwap];
		arr[toSwap] = 0;
		arr[space] = temp;
		return new Transition<>(new KonfiguracijaSlagalice(arr), 1.0);
	}

	/**
	 * Vraća listu brojeva koji predstavljaju indekse susjeda
	 * praznog polja.
	 * 
	 * Lista može imati dva člana, ako je prazno polje u uglu,
	 * tri člana ako je prazno polje uz rub u sredini, te četiri
	 * člana ako je prazno polje u samoj sredini.
	 * 
	 * @param space Indeks praznog polja u trenutnoj konfiguraciji
	 * @return Lista brojeva koji predstavljaju indekse susjeda praznog polja
	 */
	private List<Integer> setUpIndices(int space) {
		List<Integer> indices = new ArrayList<>();
		if(space%3==0) {
			if(space>2) {
				indices.add(space-3);
			}
			if(space<6) {
				indices.add(space+3);
			}
			indices.add(space+1);
		} else if(space%3==1) {
			if(space>2) {
				indices.add(space-3);
			}
			if(space<6) {
				indices.add(space+3);
			}
			indices.add(space+1);
			indices.add(space-1);
		} else {
			if(space>2) {
				indices.add(space-3);
			}
			if(space<6) {
				indices.add(space+3);
			}
			indices.add(space-1);
		}
		return indices;
	}

}
