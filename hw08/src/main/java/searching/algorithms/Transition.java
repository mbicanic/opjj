package searching.algorithms;

/**
 * Transition is a class modelling a transition from
 * one state to another, tracking the cost of the
 * transition.
 * 
 * @author Miroslav Bićanić
 * @param <S> The type of the state
 */
public class Transition<S> {
	/** The state reached by the transition */
	private S state;
	/** The cost of the transition */
	private double cost;
	
	/**
	 * A constructor for a Transition
	 * @param state The state reached by this transition
	 * @param cost The cost of this transition
	 */
	public Transition(S state, double cost) {
		this.state = state;
		this.cost = cost;
	}
	/**
	 * @return This Transition's state
	 */
	public S getState() {
		return state;
	}
	/**
	 * @return This Transition's cost
	 */
	public double getCost() {
		return cost;
	}
}
