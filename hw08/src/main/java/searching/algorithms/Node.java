package searching.algorithms;

/**
 * Node is a class that wraps one state, keeping track
 * of the total cost required to get to this state, as
 * well as the Node from which this Node was reached.
 * 
 * @author Miroslav Bićanić
 * @param <S> The type of the state
 */
public class Node<S> {
	/** The current state of this node */
	private S currentState;
	/** The accumulated cost of all transitions leading to this node. */
	private double cost;
	/** 
	 * The node containing the state from which this state was reached  
	 */
	private Node<S> parent;
	
	/**
	 * A constructor for a Node
	 * 
	 * @param parent The node from which this node was reached
	 * @param state The state to be wrapped in this node
	 * @param cost The total cost of reaching this node
	 */
	public Node(Node<S> parent, S state, double cost) {
		this.currentState = state;
		this.parent = parent;
		this.cost = cost;
	}
	
	/**
	 * @return this Node's current state
	 */
	public S getState() {
		return currentState;
	}
	/**
	 * @return the total cost of reaching this state
	 */
	public double getCost() {
		return cost;
	}
	/**
	 * @return the node from which this node was reached
	 */
	public Node<S> getParent(){
		return parent;
	}
}

