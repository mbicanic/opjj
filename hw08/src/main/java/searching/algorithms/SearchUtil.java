package searching.algorithms;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;

/**
 * SearchUtil is a class providing with implementations
 * of two subspace search algorithms - BFS and BFSV.
 * 
 * All algorithms rely completely on strategies and can be used
 * in a variety of problems to solve.
 * 
 * @author Miroslav Bićanić
 */
public class SearchUtil {
	/**
	 * An implementation of a breadth-first-search algorithm.
	 * 
	 * @param s0 A Supplier giving the state to start from
	 * @param goal A Predicate telling if a state is acceptable
	 * @param succ A Function defining all new states to which to
	 * 				transition from the current state
	 * @param <S> the type representing a state
	 */
	public static <S> Node<S> bfs(
			Supplier<S> s0,
			Function<S, List<Transition<S>>> succ,
			Predicate<S> goal) {
		List<Node<S>> toTraverse = new LinkedList<>();
		toTraverse.add(new Node<>(null, s0.get(), 0));
		while(!toTraverse.isEmpty()) {
			Node<S> ni = toTraverse.get(0);
			toTraverse.remove(0);
			if(goal.test(ni.getState())) return ni;
			List<Transition<S>> list = succ.apply(ni.getState());
			for(Transition<S> t : list) {
				toTraverse.add(new Node<>(ni, t.getState(), ni.getCost()+t.getCost()));
			}
		}
		return null;
	}
	/**
	 * An implementation of a breadth-first-search algorithm
	 * that doesn't explore the same state twice.
	 * 
	 * @param s0 A Supplier giving the state to start from
	 * @param goal A Predicate telling if a state is acceptable
	 * @param succ A Function defining all new states to which to
	 * 				transition from the current state
	 * @param <S> the type representing a state
	 */
	public static <S> Node<S> bfsv(
			Supplier<S> s0,
			Function<S, List<Transition<S>>> succ,
			Predicate<S> goal) {
		List<Node<S>> toTraverse = new LinkedList<>();
		Set<S> visited = new HashSet<>();
		toTraverse.add(new Node<>(null, s0.get(), 0));
		visited.add(s0.get());
		
		while(!toTraverse.isEmpty()) {
			Node<S> ni = toTraverse.get(0);
			toTraverse.remove(0);
			if(goal.test(ni.getState())) return ni;
			List<Transition<S>> list = succ.apply(ni.getState());
			for(Transition<S> t : list) {
				if(!visited.contains(t.getState())) {
					toTraverse.add(new Node<>(ni, t.getState(), ni.getCost()+t.getCost()));
					visited.add(t.getState());
				}
			}
		}
		return null;
	}
}
