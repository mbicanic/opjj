package coloring.demo;

import java.util.Arrays;

import coloring.algorithms.Coloring;
import coloring.algorithms.Pixel;
import coloring.algorithms.SubspaceExploreUtil;
import marcupic.opjj.statespace.coloring.FillAlgorithm;
import marcupic.opjj.statespace.coloring.FillApp;
import marcupic.opjj.statespace.coloring.Picture;

/**
 * A demonstration program showing how our state-space search
 * algorithms work on the example of the color-fill operation
 * in photo editing.
 * 
 * @author Miroslav Bićanić
 */
public class Bojanje2 {
	/**
	 * The entry point of the program
	 * @param args Not used
	 */
	public static void main(String[] args) {
		FillApp.run(FillApp.OWL, Arrays.asList(bfs, dfs, bfsv));  // ili FillApp.ROSE
	}
	
	/**
	 * A FillAlgorithm implementation searching the state-space
	 * using a breadth-first-search algorithm.
	 */
	public static final FillAlgorithm bfs = new FillAlgorithm() {
		
		@Override
		public String getAlgorithmTitle() {
			return "Moj bfs!";
		}
		
		@Override
		public void fill(int x, int y, int color, Picture picture) {
			Coloring col = new Coloring(new Pixel(x,y), picture, color);
			SubspaceExploreUtil.bfs(col,col,col,col);
		}	 
	};
	/**
	 * A FillAlgorithm implementation searching the state-space
	 * using a depth-first-search algorithm.
	 */
	public static final FillAlgorithm dfs = new FillAlgorithm() {
		
		@Override
		public String getAlgorithmTitle() {
			return "Moj dfs!";
		}
		
		@Override
		public void fill(int x, int y, int color, Picture picture) {
			Coloring col = new Coloring(new Pixel(x,y), picture, color);
			SubspaceExploreUtil.dfs(col,col,col,col);
		}	 
	};
	/**
	 * A FillAlgorithm implementation searching the state-space
	 * using a breadth-first-search algorithm, remembering visited
	 * states and visiting only new neighbors.
	 */
	public static final FillAlgorithm bfsv = new FillAlgorithm() {
		
		@Override
		public String getAlgorithmTitle() {
			return "Moj bfsv!";
		}
		
		@Override
		public void fill(int x, int y, int color, Picture picture) {
			Coloring col = new Coloring(new Pixel(x,y), picture, color);
			SubspaceExploreUtil.bfsv(col,col,col,col);
		}	 
	};
}
