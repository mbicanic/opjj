package coloring.demo;

import marcupic.opjj.statespace.coloring.FillApp;

/**
 * A demonstration program showing how different
 * state-space algorithms do their search, on the
 * example of the color-fill operation in photo
 * editing software.
 * 
 * @author Miroslav Bićanić
 */
public class Bojanje1 {
	/**
	 * The entry point of the program
	 * @param args Not used
	 */
	public static void main(String[] args) {
		FillApp.run(FillApp.OWL, null);  // ili FillApp.ROSE
	}
}
