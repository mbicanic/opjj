package coloring.algorithms;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;

import marcupic.opjj.statespace.coloring.Picture;

/**
 * Coloring is a class implementing all four interfaces
 * required by subspace exploration algorithms in 
 * {@link SubspaceExploreUtil}.
 * 
 * As a Supplier, this class returns the initial pixel
 * representing the state to begin the fill from.
 * 
 * As a Predicate, this class returns true if the found
 * pixel is of the same color as the reference pixel.
 * 
 * As a Consumer, this class paints a pixel to this class'
 * fill color.
 * 
 * As a Function, this class returns a list of pixels 
 * neighboring the given pixel.
 * 
 * @author Miroslav Bićanić
 */
public class Coloring  implements Function<Pixel, List<Pixel>>, Predicate<Pixel>, Supplier<Pixel>, Consumer<Pixel>{
	/** The reference to the pixel the user clicked */
	Pixel reference;
	/** The picture which is being color-filled */
	Picture picture;
	/** The color with which to fill the area */
	int fillColor;
	/** The color of the reference pixel prior to the operation */
	int refColor;
	
	/**
	 * A constructor for a Coloring object
	 * @param reference The reference to the pixel the user clicked
	 * @param picture The picture whose area is being color-filled
	 * @param fillColor The color with which to fill the area
	 */
	public Coloring(Pixel reference, Picture picture, int fillColor) {
		this.reference = reference;
		this.picture = picture;
		this.fillColor = fillColor;
		this.refColor = picture.getPixelColor(reference.getX(), reference.getY());
	}
	
	@Override
	public Pixel get() {
		return reference;
	}

	@Override
	public boolean test(Pixel t) {
		if(fillColor!=refColor) { //solution to infinite loop
			return picture.getPixelColor(t.getX(), t.getY())==refColor;
		}
		return false;
	}

	@Override
	public List<Pixel> apply(Pixel t) {
		List<Pixel> pixels = new ArrayList<>();
		Pixel up = new Pixel(t.getX(), t.getY()-1);
		Pixel down = new Pixel(t.getX(), t.getY()+1);
		Pixel left = new Pixel(t.getX()-1, t.getY());
		Pixel right = new Pixel(t.getX()+1, t.getY());
		checkAdd(up, pixels);
		checkAdd(right, pixels);
		checkAdd(down, pixels);
		checkAdd(left, pixels);
		return pixels;
	}

	@Override
	public void accept(Pixel t) {
		picture.setPixelColor(t.getX(), t.getY(), fillColor);
	}

	/**
	 * Checks if the computed pixel is within the dimensions
	 * of this class' picture.
	 * 
	 * @param p The pixel to check
	 * @param pixels A list of pixels to which to add legal pixels
	 */
	private void checkAdd(Pixel p, List<Pixel> pixels) {
		if(p.getX()>0 && p.getX()<picture.getWidth() && p.getY()>0 && p.getY()<picture.getHeight()) {
			pixels.add(p);
		}
	}
}
