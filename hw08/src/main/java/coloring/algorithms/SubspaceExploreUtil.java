package coloring.algorithms;

import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;

/**
 * SubspaceExploreUtil is a class providing with implementations
 * of multiple subspace search algorithms.
 * 
 * All algorithms rely completely on strategies and can be used
 * in a variety of problems to solve.
 * 
 * @author Miroslav Bićanić
 */
public class SubspaceExploreUtil {
	/**
	 * An implementation of a breadth-first-search algorithm.
	 * 
	 * @param s0 A Supplier giving the state to start from
	 * @param process A Consumer defining what to do if a found
	 * 				state is acceptable
	 * @param succ A Function defining all new states to which to
	 * 				transition from the current state
	 * @param acceptable A Predicate telling if a state is acceptable
	 * @param <S> the type representing a state
	 */
	public static <S> void bfs(
			Supplier<S> s0,
			Consumer<S> process,
			Function<S,List<S>> succ,   
			Predicate<S> acceptable
			) {
		
		LinkedList<S> toTraverse = new LinkedList<>();
		toTraverse.add(s0.get());
		while(!toTraverse.isEmpty()) {
			S elem = toTraverse.get(0);
			toTraverse.remove(0);
			if(!acceptable.test(elem)) continue;
			process.accept(elem);
			toTraverse.addAll(succ.apply(elem));
		}
	}
	
	/**
	 * An implementation of a depth-first-search algorithm.
	 * 
	 * @param s0 A Supplier giving the state to start from
	 * @param process A Consumer defining what to do if a found
	 * 				state is acceptable
	 * @param succ A Function defining all new states to which to
	 * 				transition from the current state
	 * @param acceptable A Predicate telling if a state is acceptable
	 * @param <S> the type representing a state
	 */
	public static <S> void dfs(
			Supplier<S> s0,
			Consumer<S> process,
			Function<S,List<S>> succ,   
			Predicate<S> acceptable
			) {
		LinkedList<S> toTraverse = new LinkedList<>();
		toTraverse.add(s0.get());
		while(!toTraverse.isEmpty()) {
			S elem = toTraverse.get(0);
			toTraverse.remove(0);
			if(!acceptable.test(elem)) continue;
			process.accept(elem);
			toTraverse.addAll(0, succ.apply(elem));
		}
	}
	
	/**
	 * An optimized implementation of a breadth-first-search 
	 * algorithm that doesn't explore the same state twice.
	 * 
	 * @param s0 A Supplier giving the state to start from
	 * @param process A Consumer defining what to do if a found
	 * 				state is acceptable
	 * @param succ A Function defining all new states to which to
	 * 				transition from the current state
	 * @param acceptable A Predicate telling if a state is acceptable
	 * @param <S> the type representing a state
	 */
	public static <S> void bfsv(
			Supplier<S> s0,
			Consumer<S> process,
			Function<S,List<S>> succ,   
			Predicate<S> acceptable
			) {
		LinkedList<S> toTraverse = new LinkedList<>();
		toTraverse.add(s0.get());
		Set<S> visited = new HashSet<>();
		visited.add(s0.get());
		
		while(!toTraverse.isEmpty()) {
			S elem = toTraverse.get(0);
			toTraverse.remove(0);
			if(!acceptable.test(elem)) continue;
			process.accept(elem);
			List<S> children = succ.apply(elem);
			Iterator<S> i = children.iterator();
			while(i.hasNext()) {
				S s = i.next();
				if(visited.contains(s)) {
					i.remove();
				}
			}
			toTraverse.addAll(children);
			visited.addAll(children);
		}
	}
}
