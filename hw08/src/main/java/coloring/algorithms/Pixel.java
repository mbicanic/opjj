package coloring.algorithms;

import java.util.Objects;

/**
 * A class representing a single pixel with its
 * x and y coordinates stretching from (0,0) in the
 * top left corner, to the (width,height) of the 
 * picture in which the pixel is located in the bottom
 * right corner.
 * 
 * @author Miroslav Bićanić
 */
public class Pixel {
	/** The x coordinate of the pixel */
	private int x;
	/** The y coordinate of the pixel */
	private int y;
	
	/**
	 * A constructor for a pixel
	 * @param x The x coordinate of the pixel
	 * @param y The y coordinate of the pixel
	 */
	public Pixel(int x, int y) {
		if(x<0 || y<0) {
			throw new IllegalArgumentException("Dimensions must be non-negative.");
		}
		this.x = x;
		this.y = y;
	}
	/**
	 * Two pixels are considered equal if their coordinates
	 * are equal.
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Pixel))
			return false;
		Pixel other = (Pixel) obj;
		return x == other.x && y == other.y;
	}
	@Override
	public int hashCode() {
		return Objects.hash(x, y);
	}
	/**
	 * @return A string in the format {@code (x,y)}
	 */
	@Override
	public String toString() {
		return "("+x+","+y+")";
	}
	/**
	 * @return The x coordinate of the pixel
	 */
	public int getX() {
		return x;
	}
	/**
	 * @return The y coordinate of the pixel
	 */
	public int getY() {
		return y;
	}
	
}
