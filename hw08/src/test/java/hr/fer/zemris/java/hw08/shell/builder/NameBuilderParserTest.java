package hr.fer.zemris.java.hw08.shell.builder;

import static org.junit.jupiter.api.Assertions.*;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.junit.jupiter.api.Test;

class NameBuilderParserTest {

	private FilterResult[] get() {
		String[] names = new String[] {
				"document-12-xyx-2019",
				"document-1-xxx-1998",
				"document-123-y-2005"
			};
		FilterResult[] res = new FilterResult[3];
		String pattern = "document-(\\d+)-([xy]*)-(\\d+)";
		Pattern regex = Pattern.compile(pattern);
		for(int i = 0; i<3; i++) {
			Matcher m = regex.matcher(names[i]);
			if(m.matches()) {
				res[i] = new FilterResult(names[i], m);
			}
		}
		return res;
	}
	@SuppressWarnings("unused")
	private void assertExpectedException(String argument, String message) {
		try {
			NameBuilderParser nbp = new NameBuilderParser(argument);
		} catch (NameBuilderParserException ex) {
			assertTrue(ex.getMessage().startsWith(message));
		}
	}
	
	@Test
	void testNullOrEmpty() {
		assertThrows(NullPointerException.class, ()-> new NameBuilderParser(null));
		assertThrows(NameBuilderParserException.class, ()-> new NameBuilderParser(""));
		assertThrows(NameBuilderParserException.class, ()-> new NameBuilderParser("  "));
	}
	
	@Test
	void testLiteral() {
		NameBuilderParser nbp = new NameBuilderParser("word");
		NameBuilder nb = nbp.getNameBuilder();
		FilterResult[] frs = get();
		for(FilterResult f : frs) {
			StringBuilder sb = new StringBuilder(5);
			nb.execute(f, sb);
			assertEquals("word", sb.toString());
		}
	}
	
	@Test
	void testIncorrectSub() {
		assertExpectedException(" ${", "'Group number' parameter expected");
		assertExpectedException(" ${,", "'Group number' parameter expected");
		assertExpectedException(" ${a", "'Group number' parameter expected");
		assertExpectedException(" ${-4", "'Group number' must be a");
		assertExpectedException(" ${2:", "Invalid character found");
		assertExpectedException(" ${  2  ,", "'Specification' parameter expected");
		assertExpectedException(" ${  2  , -2", "'Specification' must be a ");
		assertExpectedException(" ${  2  ,  a", "'Specification' parameter expected");
		assertExpectedException(" ${  2  ,  }", "'Specification' parameter expected");
		assertExpectedException(" ${  2  ,  01 a", "Expression must be");
	}
	
	@Test
	void testSubstitution() {
		String sub1 = "${1}-${1,04}-${1,4}-${1,2}";
		NameBuilderParser nbp = new NameBuilderParser(sub1);
		NameBuilder nb = nbp.getNameBuilder();
		FilterResult[] filters = get();
		String[] expected = new String[] {
				"12-0012-  12-12",
				"1-0001-   1- 1",
				"123-0123- 123-123"
		};
				
		for(int i = 0; i < 3; i++) {
			StringBuilder sb = new StringBuilder(15);
			nb.execute(filters[i], sb);
			assertEquals(expected[i], sb.toString());
		}
	}
	
	@Test
	void testExamples() {
		String sub1 = "doc-${1}-${2,5}-${  3  ,  4}.txt";
		NameBuilderParser nbp = new NameBuilderParser(sub1);
		NameBuilder nb = nbp.getNameBuilder();
		FilterResult[] filters = get();
		String[] expected = new String[] {
				"doc-12-  xyx-2019.txt",
				"doc-1-  xxx-1998.txt",
				"doc-123-    y-2005.txt"
		};
				
		for(int i = 0; i < 3; i++) {
			StringBuilder sb = new StringBuilder(15);
			nb.execute(filters[i], sb);
			assertEquals(expected[i], sb.toString());
		}
	}
	
}
