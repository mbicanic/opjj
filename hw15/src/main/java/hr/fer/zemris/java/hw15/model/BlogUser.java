package hr.fer.zemris.java.hw15.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * BlogUser is an {@link Entity} modelling a user of the blog.
 * <br>
 * It is modelled as a class with attributes, containing
 * getters and setters for each attribute - a Java Bean.
 * <br>
 * The attributes associated with a BlogUser are: <ul>
 * <li>	ID - the ID of the BlogUser, used as the primary key </li>
 * <li> FirstName - the first name of the BlogUser </li>
 * <li> LastName - the last name of the BlogUser </li>
 * <li> Nick - the nickname/username of the BlogUser </li>
 * <li> Email - the e-mail of the BlogUser </li>
 * <li> PasswordHash - a crypted form of the BlogUser's password </li> </ul>
 * 
 * This class defines three {@link NamedQuery} objects, for faster
 * SQL execution: <ul>
 * <li> {@code user.entries} - query for all {@link BlogEntry} objects
 * 		belonging to a certain {@link BlogUser}, where the user is 
 * 		specified through the {@code creator} parameter of the query. </li>
 * <li> {@code user.users} - query for all registered users of the blog </li>
 * <li> {@code user.nick} - query for a {@link BlogUser} with a given nickname,
 * 		where the nickname is specified through the {@code nick} parameter
 * 		of the query </li> </ul>
 * 
 * @author Miroslav Bićanić
 */
@NamedQueries({
	@NamedQuery(name="user.entries", query="select e from BlogEntry as e where e.creator=:creator"),
	@NamedQuery(name="user.users", query="select u from BlogUser as u"),
	@NamedQuery(name="user.nick", query="select u from BlogUser as u where u.nick=:nick")
})
@Entity
@Table(name="users")
public class BlogUser {
	
	@Id @GeneratedValue
	private Long id;
	
	@Column(length=50, nullable=false)
	private String firstName;
	
	@Column(length=50, nullable=false)
	private String lastName;
	
	@Column(length=50, nullable=false)
	private String nick;
	
	@Column(length=100, nullable=false)
	private String email;
	
	@Column(length=40)
	private String passwordHash;
	
	@OneToMany(mappedBy="creator",fetch=FetchType.LAZY, cascade=CascadeType.PERSIST, orphanRemoval=true)
	private List<BlogEntry> entries = new ArrayList<>();
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getNick() {
		return nick;
	}
	public void setNick(String nick) {
		this.nick = nick;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPasswordHash() {
		return passwordHash;
	}
	public void setPasswordHash(String passwordHash) {
		this.passwordHash = passwordHash;
	}
	public List<BlogEntry> getEntries(){
		return entries;
	}
	public void setEntries(List<BlogEntry> entries) {
		this.entries = entries;
	}
	@Override
	public int hashCode() {
		return Objects.hash(id, nick);
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof BlogUser))
			return false;
		BlogUser other = (BlogUser) obj;
		return Objects.equals(id, other.id);
	}
}
