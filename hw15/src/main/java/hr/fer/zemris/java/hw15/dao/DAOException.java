package hr.fer.zemris.java.hw15.dao;

/**
 * DAOException is an exception that should be thrown whenever
 * an error occurs in communication with the data persistence
 * layer.
 * 
 * @author marcupic
 */
public class DAOException extends RuntimeException {
	private static final long serialVersionUID = 1L;

	public DAOException(String message, Throwable cause) {
		super(message, cause);
	}

	public DAOException(String message) {
		super(message);
	}
}