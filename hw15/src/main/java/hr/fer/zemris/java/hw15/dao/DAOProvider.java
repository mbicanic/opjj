package hr.fer.zemris.java.hw15.dao;

import hr.fer.zemris.java.hw15.dao.jpa.JPADAOImpl;

/**
 * DAOProvider is a class providing with an implementation of
 * an object which can communicate with the data persistence
 * layer.
 * 
 * The class is designed utilizing the Singleton design pattern.
 * 
 * @author marcupic
 */
public class DAOProvider {

	private static DAO dao = new JPADAOImpl();
	
	public static DAO getDAO() {
		return dao;
	}
	
}