package hr.fer.zemris.java.hw15.dao.jpa;

import java.util.List;

import javax.persistence.NoResultException;

import hr.fer.zemris.java.hw15.dao.DAO;
import hr.fer.zemris.java.hw15.dao.DAOException;
import hr.fer.zemris.java.hw15.model.BlogComment;
import hr.fer.zemris.java.hw15.model.BlogEntry;
import hr.fer.zemris.java.hw15.model.BlogUser;

/**
 * An implementation of a {@link DAO}, communicating with the
 * data persistence layer.
 * 
 * @author Miroslav Bićanić
 */
public class JPADAOImpl implements DAO {
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public BlogEntry getBlogEntry(Long id) throws DAOException {
		BlogEntry blogEntry = JPAEMProvider.getEntityManager().find(BlogEntry.class, id);
		return blogEntry;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<BlogUser> getUserList() throws DAOException {
		return JPAEMProvider.getEntityManager()
				.createNamedQuery("user.users", BlogUser.class)
				.getResultList();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<BlogEntry> getEntriesOfUser(BlogUser u) throws DAOException {
		return JPAEMProvider.getEntityManager()
				.createNamedQuery("user.entries", BlogEntry.class)
				.setParameter("creator", u)
				.getResultList();
	}

	/**
	 * {@inheritDoc}
	 * This method returns <code>null</code> if the user does not exist.
	 */
	@Override
	public BlogUser userForNick(String nick) throws DAOException {
		try {
			BlogUser user = JPAEMProvider.getEntityManager()
					.createNamedQuery("user.nick", BlogUser.class)
					.setParameter("nick", nick).getSingleResult();
			return user;
		} catch( NoResultException ex) {
			return null;
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void addUser(BlogUser u) throws DAOException {
		JPAEMProvider.getEntityManager().persist(u);
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public void addEntry(BlogEntry e) throws DAOException {
		JPAEMProvider.getEntityManager().persist(e);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void addComment(BlogComment c) throws DAOException {
		JPAEMProvider.getEntityManager().persist(c);
	}
}