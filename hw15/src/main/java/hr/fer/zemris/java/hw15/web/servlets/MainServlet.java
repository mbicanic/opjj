package hr.fer.zemris.java.hw15.web.servlets;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import hr.fer.zemris.java.hw15.dao.DAOProvider;
import hr.fer.zemris.java.hw15.model.BlogUser;
import hr.fer.zemris.java.hw15.model.bridge.UserForm;

/**
 * MainServlet is a specifiation of a {@link HttpServlet} which
 * is in charge of setting up data for the homepage of J-Blog.
 * 
 * Since the login form is displayed on the homepage, this servlet
 * is also responsible for the authentication and processing of 
 * login attempts.
 * 
 * @author Miroslav Bićanić
 */
public class MainServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		UserForm uf = new UserForm();
		List<BlogUser> users = DAOProvider.getDAO().getUserList();
		req.setCharacterEncoding("UTF-8");
		req.setAttribute("loginForm", uf);
		req.getServletContext().setAttribute("users", users);
		req.getRequestDispatcher("/WEB-INF/index.jsp").forward(req, resp);
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		//POST method means login was attempted
		UserForm uf = new UserForm();
		uf.fillFromRequest(req);
		uf.validateLogin();
		
		if(uf.hasErrors()) {
			req.setAttribute("loginForm", uf);
			req.getRequestDispatcher("/WEB-INF/index.jsp").forward(req, resp);
			return;
		}
		
		BlogUser u = DAOProvider.getDAO().userForNick(uf.getNick());
		if(u==null) {
			uf.setError("nick", "User with given username does not exist");
			req.setAttribute("loginForm", uf);
			req.getRequestDispatcher("/WEB-INF/index.jsp").forward(req, resp);
			return;
		}
		if(!uf.getPwdHash().equals(u.getPasswordHash())) {
			uf.setError("pwd", "Entered password is incorrect");
			req.setAttribute("loginForm", uf);
			req.getRequestDispatcher("/WEB-INF/index.jsp").forward(req, resp);
			return;
		}
		
		req.getSession().setAttribute("current.user.id", u.getId());
		req.getSession().setAttribute("current.user.fn", u.getFirstName());
		req.getSession().setAttribute("current.user.ln", u.getLastName());
		req.getSession().setAttribute("current.user.nick", u.getNick());
		req.getSession().setAttribute("current.user.email", u.getEmail());
		
		resp.sendRedirect(resp.encodeRedirectURL(
				req.getContextPath() + "/servleti/main"));
	}
}
