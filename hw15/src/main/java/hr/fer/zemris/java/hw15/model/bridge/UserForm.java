package hr.fer.zemris.java.hw15.model.bridge;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.regex.Matcher;

import javax.servlet.http.HttpServletRequest;

import hr.fer.zemris.java.hw15.model.BlogEntry;
import hr.fer.zemris.java.hw15.model.BlogUser;

/**
 * UserForm is a representation of a {@link BlogUser} used for
 * storing the user data during registration and login processes,
 * as received through a form. <br>
 * All attributes are stored as their string representations.
 * <br>
 * The class is an implementation of a {@link FormEntityBridge}, 
 * connecting the data submitted through a form and the actual
 * entity it represents.
 * <br>
 * Apart from the described functionality, it provides getters and
 * setters for all attributes, effectively making it a Java Bean.
 * 
 * @see BlogEntry
 * @author Miroslav Bićanić
 */
public class UserForm extends AbstractForm implements FormEntityBridge<BlogUser>{
	private String firstName;
	private String lastName;
	private String nick;
	private String email;
	private String pwdHash;
	
	private boolean passwordTooShort = false;

	/**
	 * Default constructor initializing all the attribute values to
	 * empty strings.
	 */
	public UserForm() {
		this.firstName="";
		this.lastName="";
		this.nick="";
		this.email="";
		this.pwdHash="";
	}

	/**
	 * Inserts an error {@code message} under the given {@code key}
	 * in the errors map.
	 * 
	 * Necessary for UserForm, as its {@link #validate()} and 
	 * {@link #validateLogin()} methods aren't strong enough, so
	 * additional errors may appear later.
	 * 
	 * @param key the key under which to store the error
	 * @param message the message of the error
	 */
	public void setError(String key, String message) {
		errors.put(key, message);
	}
	
	/**
	 * {@inheritDoc}
	 */
	public void fillFromEntity(BlogUser u) {
		this.firstName = u.getFirstName();
		this.lastName = u.getLastName();
		this.nick = u.getNick();
		this.email = u.getEmail();
	}
	
	/**
	 * {@inheritDoc}
	 */
	public void fillFromRequest(HttpServletRequest req) {
		this.firstName = prepare(req.getParameter("fname"));
		this.lastName = prepare(req.getParameter("lname"));
		this.nick = prepare(req.getParameter("nick"));
		this.email = prepare(req.getParameter("email"));
		this.pwdHash = digest(req.getParameter("pwd"));
	}
	
	/**
	 * {@inheritDoc}
	 */
	public void pourToEntity(BlogUser u) {
		u.setFirstName(this.firstName);
		u.setLastName(this.lastName);
		u.setNick(this.nick);
		u.setEmail(this.email);
		u.setPasswordHash(this.pwdHash);
	}

	/**
	 * A login attempt is valid if both the nickname and the 
	 * password were provided, with the password being at least
	 * 6 characters long.
	 */
	public void validateLogin() {
		errors.clear();
		if(this.nick.isEmpty()) {
			errors.put("nick", "Nickname is mandatory");
		}
		if(this.pwdHash.isEmpty()) {
			errors.put("pwd", "Password is mandatory");
		} else {
			if(passwordTooShort) {
				errors.put("pwd",  "Password must be at least 6 characters long.");
			}
		}
	}
	
	/**
	 * {@inheritDoc}
	 * <br>
	 * Submitted data is valid if {@link #validateLogin()} doesn't
	 * find any errors, and also when all other attributes (first 
	 * name, last name and e-mail) are present.
	 * 
	 * Apart from that, the e-mail must be in correct format.
	 */
	public void validate() {
		validateLogin();
		
		if(this.firstName.isEmpty()) {
			errors.put("fname", "First name is mandatory");
		}
		
		if(this.lastName.isEmpty()) {
			errors.put("lname", "Last name is mandatory!");
		}
		
		if(this.email.isEmpty()) {
			errors.put("email", "E-mail is mandatory!");
		} else {
			Matcher m = VALID_EMAIL.matcher(this.email);
			if(!m.find()) {
				errors.put("email", "E-mail is in wrong format.");
			}
		}
	}
	
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastname) {
		this.lastName = lastname;
	}

	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstname) {
		this.firstName = firstname;
	}

	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getNick() {
		return nick;
	}
	public void setNick(String nick) {
		this.nick = nick;
	}
	
	public String getPwdHash() {
		return pwdHash;
	}
	public void setPwdHash(String pwdHash) {
		this.pwdHash = pwdHash;
	}
	
	/**
	 * Calculates the SHA-1 digest of the given {@code pwd} string 
	 * using an instance of a {@link MessageDigest}.
	 * 
	 * @param pwd the password string to digest
	 */
	private String digest(String pwd) {
		if(pwd==null || pwd.length()==0) {
			return "";
		}
		if(pwd.length()<6) {
			passwordTooShort=true;
		}
		try {
			MessageDigest md = MessageDigest.getInstance("SHA-1");
			byte[] digest = md.digest(pwd.getBytes());
			BigInteger no = new BigInteger(1, digest);
			String hash = no.toString(16);
			while(hash.length()<32) {
				hash = "0"+hash;
			}
			return hash;
			
		} catch (NoSuchAlgorithmException ignored) {
			return "";
		}
	}
}