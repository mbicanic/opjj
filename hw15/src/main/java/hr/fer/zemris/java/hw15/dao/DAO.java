package hr.fer.zemris.java.hw15.dao;

import java.util.List;

import hr.fer.zemris.java.hw15.model.BlogComment;
import hr.fer.zemris.java.hw15.model.BlogEntry;
import hr.fer.zemris.java.hw15.model.BlogUser;

/**
 * Interface towards the subsystem for data persistence.
 * 
 * @author Miroslav Bićanić
 */
public interface DAO {

	/**
	 * Queries the data persistence layer for a list of all registered 
	 * users.
	 * @return a {@link List} of registered {@link BlogUser}s 
	 * @throws DAOException if retrieval fails
	 */
	public List<BlogUser> getUserList() throws DAOException;
	
	/**
	 * Queries the data persistence layer for a {@link BlogUser}
	 * with the given {@code nick}.
	 * @param nick the username/nickname of the {@link BlogUser}
	 * @return the {@link BlogUser} with the given {@code nick}
	 * @throws DAOException if retrieval fails
	 */
	public BlogUser userForNick(String nick) throws DAOException;

	/**
	 * Queries the data persistence layer for a {@link BlogEntry}
	 * with the given {@code id}.
	 * @param id the ID of the requested {@link BlogEntry}
	 * @return the {@link BlogEntry} with the given {@code id}
	 * @throws DAOException if retrieval fails
	 */
	public BlogEntry getBlogEntry(Long id) throws DAOException;

	/**
	 * Queries the data persistence layer for all entries whose creator
	 * is the given {@link BlogUser}.
	 * @param u the {@link BlogUser} whose entries to retrieve
	 * @return a {@link List} of {@link BlogEntry} objects belonging to the user
	 * @throws DAOException if retrieval fails
	 */
	public List<BlogEntry> getEntriesOfUser(BlogUser u) throws DAOException;

	/**
	 * Stores the given {@link BlogUser} into the data persistence
	 * layer.
	 * @param u the {@link BlogUser} to store
	 * @throws DAOException if storing fails
	 */
	public void addUser(BlogUser u) throws DAOException;
	
	/**
	 * Stores the given {@link BlogEntry} into the data persistence
	 * layer.
	 * @param u the {@link BlogEntry} to store
	 * @throws DAOException if storing fails
	 */
	public void addEntry(BlogEntry e) throws DAOException;
	
	/**
	 * Stores the given {@link BlogComment} into the data persistence
	 * layer.
	 * @param u the {@link BlogComment} to store
	 * @throws DAOException if storing fails
	 */
	public void addComment(BlogComment c) throws DAOException;
	
}