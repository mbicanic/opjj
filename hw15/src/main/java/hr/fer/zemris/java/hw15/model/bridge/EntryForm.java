package hr.fer.zemris.java.hw15.model.bridge;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import hr.fer.zemris.java.hw15.model.BlogEntry;

/**
 * EntryForm is a representation of a {@link BlogEntry} used for
 * storing the entry's attributes submitted by the user through
 * a html form: the entry's ID, title and content.
 * All attributes are stored as their string representations.
 * <br>
 * The class is an implementation of a {@link FormEntityBridge}, 
 * connecting the data submitted through a form and the actual
 * entity it represents.
 * <br>
 * Apart from the described functionality, it provides getters and
 * setters for all attributes, effectively making it a Java Bean.
 * 
 * @see BlogEntry
 * @author Miroslav Bićanić
 */
public class EntryForm extends AbstractForm implements FormEntityBridge<BlogEntry>{
	private String id;
	private String title;
	private String text;
	
	/**
	 * Default constructor initializing all attributes to
	 * empty strings.
	 */
	public EntryForm() {
		this.id="";
		this.title="";
		this.text="";
	}
	
	/**
	 * {@inheritDoc}
	 */
	public void fillFromEntity(BlogEntry e) {
		this.id = e.getId().toString();
		this.title = e.getTitle();
		this.text = e.getText();
	}
	
	/**
	 * {@inheritDoc}
	 */
	public void fillFromRequest(HttpServletRequest req) {
		this.id = prepare(req.getParameter("id"));
		this.title = prepare(req.getParameter("title"));
		this.text = prepare(req.getParameter("text"));
	}
	
	/**
	 * {@inheritDoc}
	 */
	public void pourToEntity(BlogEntry e) {
		if(this.id.isEmpty()) {
			e.setId(null);
		} else {
			e.setId(Long.valueOf(this.id));
		}
		e.setTitle(this.title);
		e.setText(this.text);
		if(e.getCreatedAt()==null) {
			e.setCreatedAt(new Date(System.currentTimeMillis()));
		}
		e.setLastModifiedAt(new Date(System.currentTimeMillis()));
	}
	
	/**
	 * {@inheritDoc}
	 * <br>
	 * An EntryForm is valid if both the title and text are present.
	 */
	public void validate() {
		if(this.title.isEmpty()) {
			errors.put("title", "Title for a blog post is mandatory");
		}
		if(this.text.isEmpty()) {
			errors.put("text", "A blog post must contain text");
		}
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}
}
