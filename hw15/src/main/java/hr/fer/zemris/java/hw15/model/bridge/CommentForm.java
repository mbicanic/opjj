package hr.fer.zemris.java.hw15.model.bridge;

import java.util.Date;
import java.util.regex.Matcher;

import javax.servlet.http.HttpServletRequest;

import hr.fer.zemris.java.hw15.model.BlogComment;

/**
 * CommentForm is a representation of a {@link BlogComment}
 * used for storing the comment's attributes which are submitted
 * by the user through a html form: the user's e-mail and the
 * content of the comment.
 * All attributes are stored as their string representations.
 * <br>
 * The class is an implementation of a {@link FormEntityBridge}, 
 * connecting the data submitted through a form and the actual
 * entity it represents.
 * <br>
 * Apart from the described functionality, it provides getters and
 * setters for all attributes, effectively making it a Java Bean.
 * 
 * @see BlogComment
 * @author Miroslav Bićanić
 */
public class CommentForm extends AbstractForm implements FormEntityBridge<BlogComment> {
	private String email;
	private String message;
	
	/**
	 * Default constructor, initializing the {@link #email} and
	 * {@link #message} attributes to empty Strings.
	 */
	public CommentForm() {
		this.email="";
		this.message="";
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void fillFromEntity(BlogComment entity) {
		this.email = prepare(entity.getEmail());
		this.message = prepare(entity.getMessage());
	}
	
	/**
	 * {@inheritDoc}
	 */
	public void fillFromRequest(HttpServletRequest req) {
		this.email = prepare(req.getParameter("email"));
		this.message = prepare(req.getParameter("msg"));
	}
	/**
	 * {@inheritDoc}
	 */
	public void pourToEntity(BlogComment c) {
		c.setId(null);
		c.setMessage(this.message);
		c.setEmail(this.email);
		c.setPostedOn(new Date(System.currentTimeMillis()));
	}
	
	/**
	 * {@inheritDoc}
	 * <br>
	 * Data is acceptable if the both the comment content and the
	 * e-mail were provided, but only if the e-mail is in a valid
	 * format.
	 */
	public void validate() {
		if(this.email.isEmpty()) {
			errors.put("email", "E-mail must be provided!");
		} else {
			Matcher m = UserForm.VALID_EMAIL.matcher(this.email);
			if(!m.find()) {
				errors.put("email", "E-mail is in wrong format.");
			}
		}
		if(this.message.isEmpty()) {
			errors.put("msg", "A comment cannot be empty!");
		}
	}
	
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
}
