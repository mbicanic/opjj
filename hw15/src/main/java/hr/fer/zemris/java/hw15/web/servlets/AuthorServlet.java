package hr.fer.zemris.java.hw15.web.servlets;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import hr.fer.zemris.java.hw15.dao.DAOProvider;
import hr.fer.zemris.java.hw15.model.BlogComment;
import hr.fer.zemris.java.hw15.model.BlogEntry;
import hr.fer.zemris.java.hw15.model.BlogUser;
import hr.fer.zemris.java.hw15.model.bridge.CommentForm;
import hr.fer.zemris.java.hw15.model.bridge.EntryForm;

/**
 * AuthorServlet is a specification of a {@link HttpServlet} which
 * handles all actions related to a specific blog author: <ul>
 * <li> Displaying the author's main page with a list of entries </li>
 * <li> Displaying a specific entry of an author </li>
 * <li> Creating a new entry and editing an old one </li> </ul>
 * 
 * It is mapped to URLs starting with {@code /servleti/author/*}.
 * 
 * @author Miroslav Bićanić
 */
public class AuthorServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		process(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		process(req, resp);
	}
	
	/**
	 * Processes a {@link HttpServletRequest}, parsing the full path
	 * info and calling appropriate methods to handle the request 
	 * further.
	 * 
	 * @param req the {@link HttpServletRequest} of the request
	 * @param resp the {@link HttpServletResponse} for the request
	 */
	private void process(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
		String[] context = getContext(req, resp);	
		if(context==null) {
			return;
		}
		BlogUser author = DAOProvider.getDAO().userForNick(context[0]);
		if(author==null) {
			req.setAttribute("error", "User with the nickname '"+context[0]+"' does not exist.");
			req.getRequestDispatcher("/WEB-INF/pages/error.jsp").forward(req, resp);
			return;
		}
		if(context.length==1) {
			authorPage(req, resp, author);
			return;
		} 
		if(context[1].equals("new")) {
			newEntry(req, resp, author);
		} else if (context[1].equals("edit")) {
			editEntry(req, resp, author);
		} else {
			displayEntry(req, resp, author, context[1]);
		}
	}
	
	/**
	 * Returns a string array containing segments of the URL of the
	 * {@link HttpServletRequest}, describing which action should
	 * be performed by the servlet.
	 * @param req the {@link HttpServletRequest} of the request
	 * @param resp the {@link HttpServletResponse} for the request
	 * @return an array of URL path segments
	 */
	private String[] getContext(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
		String context = req.getPathInfo();
		if(context==null || context.length()<=1) {
			req.setAttribute("error", "Invalid URL given.");
			req.getRequestDispatcher("/WEB-INF/pages/error.jsp").forward(req, resp);
			return null;
		}
		return context.substring(1).split("/");
	}

	/**
	 * Displays the author's main page, containing the links to all
	 * the blog entries of the author.
	 * 
	 * @param req the {@link HttpServletRequest} of the request
	 * @param resp the {@link HttpServletResponse} for the request
	 * @param author the {@link BlogUser} owning that blog
	 */
	private void authorPage(HttpServletRequest req, HttpServletResponse resp, BlogUser author) throws ServletException, IOException {
		List<BlogEntry> entries = DAOProvider.getDAO().getEntriesOfUser(author);
		req.setAttribute("entries", entries);
		req.setAttribute("author", author);
		req.getRequestDispatcher("/WEB-INF/pages/author.jsp").forward(req, resp);
	}

	/**
	 * Displays a single entry belonging to the given {@link BlogUser}
	 * with the given {@code eid} representing the entry ID.
	 * 
	 * If the method of the {@link HttpServletRequest} was POST, that
	 * means a comment has been submitted on the entry page and must
	 * be validated, processed, stored and displayed.
	 * If the method was GET, the entry is simply displayed.
	 * 
	 * @param req the {@link HttpServletRequest} of the request
	 * @param resp the {@link HttpServletResponse} for the request
	 * @param author the {@link BlogUser} owning the entry
	 * @param eid the ID of the required entry
	 */
	private void displayEntry(HttpServletRequest req, HttpServletResponse resp, BlogUser author, String eid) throws IOException, ServletException {
		Long id = Long.parseLong(eid);
		BlogEntry entry = DAOProvider.getDAO().getBlogEntry(id);
		if(entry==null) {
			req.setAttribute("error", "An entry with the given ID does not exist.");
			req.getRequestDispatcher("/WEB-INF/pages/error.jsp").forward(req, resp);
			return;
		}
		if(!entry.getCreator().equals(author)) {
			req.setAttribute("error", "Author '" + author.getNick()
				+ "' is not the author of the requested entry.");
			req.getRequestDispatcher("/WEB-INF/pages/error.jsp").forward(req, resp);
			return;
		}
		req.setAttribute("entry", entry);
		CommentForm form = new CommentForm();
		
		switch(req.getMethod().toUpperCase()) {
		case "POST":
			form.fillFromRequest(req);
			form.validate();
			if(form.hasErrors()) {
				if(req.getSession().getAttribute("current.user.id")!=null) {
					req.setAttribute("readonly", "readonly");
				}
				req.setAttribute("commentForm", form);
				req.getRequestDispatcher("/WEB-INF/pages/entry.jsp").forward(req, resp);
				return;
			}
			BlogComment comment = new BlogComment();
			form.pourToEntity(comment);
			comment.setBlogEntry(entry);
			entry.getComments().add(comment);
			DAOProvider.getDAO().addComment(comment);
			resp.sendRedirect(resp.encodeRedirectURL(
					req.getContextPath() + "/servleti/author/"+author.getNick()+"/"+eid));
			return;
		case "GET":
			if(req.getSession().getAttribute("current.user.id")!=null) {
				form.setEmail(req.getSession().getAttribute("current.user.email").toString());
				req.setAttribute("readonly", "readonly");
			}
			req.setAttribute("commentForm", form);
			req.getRequestDispatcher("/WEB-INF/pages/entry.jsp").forward(req, resp);
			return;
		}
	}
	
	/**
	 * Displays a blank form used to create or edit entries, if the
	 * currently logged in user is authorized to do so.
	 * 
	 * If the method of the {@link HttpServletRequest} was POST, that
	 * means the form was submitted and data must be validated, processed
	 * and stored (or redirected back, if the data has errors).
	 * If the method was GET, a blank form is shown.
	 * 
	 * @param req the {@link HttpServletRequest} of the request
	 * @param resp the {@link HttpServletResponse} for the request
	 * @param author the {@link BlogUser} owning that blog
	 */
	private void newEntry(HttpServletRequest req, HttpServletResponse resp, BlogUser author) throws IOException, ServletException {
		if(!authorize(req, resp, author)) {
			return;
		}
		EntryForm form = new EntryForm();
		req.setAttribute("action", "new");
		if(req.getMethod().equalsIgnoreCase("post")) {
			form.fillFromRequest(req);
			form.validate();
			if(form.hasErrors()) {
				req.setAttribute("entryForm", form);
				req.getRequestDispatcher("/WEB-INF/pages/editEntry.jsp").forward(req, resp);
				return;
			}
			BlogEntry entry = new BlogEntry();
			form.pourToEntity(entry);
			
			entry.setCreator(author);
			author.getEntries().add(entry);
			
			DAOProvider.getDAO().addEntry(entry);
			resp.sendRedirect(resp.encodeRedirectURL(
					req.getContextPath()+"/servleti/author/"+author.getNick()));
		} else {
			req.setAttribute("entryForm", form);
			req.getRequestDispatcher("/WEB-INF/pages/editEntry.jsp").forward(req, resp);
		}
	}

	/**
	 * Displays a form used to create or edit entries with the form
	 * content initialized to the entry that is being edited.
	 * 
	 * This action can be invoked using the POST method, either from
	 * the entry page if the logged in user is also the author, or
	 * from the form for editing entries, if an entry was edited and 
	 * submitted.
	 * Invoking it using the GET method requires adding a parameter
	 * {@code eid} with the value equal to the desired entry ID.
	 * 
	 * In case the source of invoking of this method is the entry page
	 * (or it is requested using GET), the form is simply displayed with 
	 * its values initialized to the values from the existing version of 
	 * the entry.
	 * Otherwise, the source of invoking is the form which contains all
	 * necessary data about the entry. In case the data has errors, the
	 * client is redirected to the entry editing form, with the values
	 * initialized again to the values from the database, not the
	 * submitted form.
	 * 
	 * @param req the {@link HttpServletRequest} of the request
	 * @param resp the {@link HttpServletResponse} for the request
	 * @param author the {@link BlogUser} owning that blog
	 */
	private void editEntry(HttpServletRequest req, HttpServletResponse resp, BlogUser author) throws IOException, ServletException {
		if(!authorize(req, resp, author)) {
			return;
		}
		EntryForm form = new EntryForm();
		String source = req.getParameter("source"); //both ways to access /edit are using POST method
		if(source==null) {	
			//this means the link was manually typed, so there is no way of knowing which entry
			//ID to edit unless it was given as a URL parameter -> GET method
			String eID = req.getParameter("eid");
			if(eID==null) {
				resp.sendRedirect(resp.encodeRedirectURL(
						req.getContextPath() + "/servleti/author/"+author.getNick()));
				return;
			}
			source="link";
		}
		req.setAttribute("action", "edit");
		if(source.equals("link")) {
			Long eID = Long.parseLong(req.getParameter("eid"));
			form.fillFromEntity(DAOProvider.getDAO().getBlogEntry(eID));
			
			req.setAttribute("entryForm", form);
			req.getRequestDispatcher("/WEB-INF/pages/editEntry.jsp").forward(req, resp);
		} else if(source.equals("form")) {
			form.fillFromRequest(req);
			form.validate();
			if(form.hasErrors()) {
				form.fillFromEntity(DAOProvider.getDAO().getBlogEntry(Long.parseLong(form.getId())));
				req.setAttribute("entryForm", form);
				req.getRequestDispatcher("/WEB-INF/pages/editEntry.jsp").forward(req, resp);
				return;
			}
			Long eID = Long.parseLong(form.getId());
			form.pourToEntity(DAOProvider.getDAO().getBlogEntry(eID));
			resp.sendRedirect(resp.encodeRedirectURL(
					req.getContextPath()+"/servleti/author/"+author.getNick()));
		}
	}
	
	/**
	 * Checks if the current user is logged in, and if he is, checks
	 * if he is indeed the given {@code author} {@link BlogUser}.
	 * 
	 * @param req the {@link HttpServletRequest} of the request
	 * @param resp the {@link HttpServletResponse} for the request
	 * @param author the {@link BlogUser} owning the blog or entry
	 * @return true if the user is authorized; false otherwise
	 */
	private boolean authorize(HttpServletRequest req, HttpServletResponse resp, BlogUser author) throws ServletException, IOException {
		Object cuNick = req.getSession().getAttribute("current.user.nick");
		if(cuNick==null) {
			req.setAttribute("error", "Anonymous users cannot create or edit entries. Please log in.");
			req.getRequestDispatcher("/WEB-INF/pages/error.jsp").forward(req, resp);
			return false;
		}
		if(!author.getNick().equals(cuNick)) {
			req.setAttribute("error", "Nice try, but being logged in as user '"
					+cuNick+"' does not authorize you to create or edit entries on behalf of '"
					+author.getNick()+"'.");
			req.getRequestDispatcher("/WEB-INF/pages/error.jsp").forward(req, resp);
			return false;
		}
		return true;
	}
}
