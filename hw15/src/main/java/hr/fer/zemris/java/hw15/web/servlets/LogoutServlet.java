package hr.fer.zemris.java.hw15.web.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * LogoutServlet is a specification of a {@link HttpServlet} which
 * invalidates the current session, deleting all stored data and
 * effectively logging the user out.
 * 
 * @author Miroslav Bićanić
 */
public class LogoutServlet extends HttpServlet{
	private static final long serialVersionUID = 1L;
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		req.getSession().invalidate();
		resp.sendRedirect(resp.encodeRedirectURL(
				req.getContextPath() + "/servleti/main"));
	}
}
