package hr.fer.zemris.java.hw15.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * BlogComment is an {@link Entity} modelling a blog comment.
 * <br>
 * It is modelled as a class with attributes, containing
 * getters and setters for each attribute - a Java Bean.
 * <br>
 * The attributes associated with a BlogComment are: <ul>
 * <li>	ID - the ID of the BlogComment, used as the primary key </li>
 * <li> BlogEntry - {@link BlogEntry} on which this comment is posted </li>
 * <li> Email - the e-mail of the user which left the comment </li>
 * <li> Text - the content of the BlogComment </li>
 * <li> PostedOn - a {@link Date} timestamp at which the comment was posted </li> </ul>
 * 
 * @author Miroslav Bićanić
 */
@Entity
@Table(name="blog_comments")
public class BlogComment {
	
	private Long id;
	private BlogEntry blogEntry;
	private String email;
	private String message;
	private Date postedOn;
	
	@Id @GeneratedValue
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}

	@ManyToOne
	@JoinColumn(nullable=false)
	public BlogEntry getBlogEntry() {
		return blogEntry;
	}
	public void setBlogEntry(BlogEntry blogEntry) {
		this.blogEntry = blogEntry;
	}

	@Column(length=100,nullable=false)
	public String getEmail() {
		return email;
	}
	public void setEmail(String usersEMail) {
		this.email = usersEMail;
	}

	@Column(length=4096,nullable=false)
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(nullable=false)
	public Date getPostedOn() {
		return postedOn;
	}
	public void setPostedOn(Date postedOn) {
		this.postedOn = postedOn;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BlogComment other = (BlogComment) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
}