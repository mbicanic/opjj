package hr.fer.zemris.java.hw15.model.bridge;

import javax.persistence.Entity;
import javax.servlet.http.HttpServletRequest;

/**
 * FormEntityBridge is an interface for objects that serve
 * as a bridge of information between HTML forms and actual
 * entities in the database.
 * 
 * @author Miroslav Bićanić
 *
 * @param <T> the {@link Entity} represented by the FormEntityBridge
 */
public interface FormEntityBridge<T> {
	
	/**
	 * Initializes the values of this Form with the parameters
	 * of the given {@link HttpServletRequest}.
	 * 
	 * @param req the {@link HttpServletRequest} containing the parameters
	 */
	void fillFromRequest(HttpServletRequest req);
	
	/**
	 * Initializes the values in the entity represented by the 
	 * FormEntityBridge form, using the values stored in this 
	 * FormEntityBridge.
	 * 
	 * @param entity the entity whose values to initialize
	 */
	void fillFromEntity(T entity);
	
	/**
	 * Initializes the given {@code entity} using the values stored
	 * in this FormEntityBridge, representing the type of the given
	 * entity.
	 * 
	 * @param entity the entity to initialize
	 */
	void pourToEntity(T entity);
}
