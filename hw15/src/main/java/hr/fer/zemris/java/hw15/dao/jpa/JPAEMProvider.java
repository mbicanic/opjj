package hr.fer.zemris.java.hw15.dao.jpa;

import javax.persistence.EntityManager;

import hr.fer.zemris.java.hw15.dao.DAOException;

/**
 * Class providing with an implementation of an {@link EntityManager}.
 * 
 * Each {@link EntityManager} is stored in a {@link ThreadLocal} map
 * linking each thread to its own EntityManager.
 * @author Miroslav Bićanić
 *
 */
public class JPAEMProvider {

	/** The ThreadLocal map of EntityManager objects */
	private static ThreadLocal<EntityManager> locals = new ThreadLocal<>();

	/**
	 * Returns this thread's {@link EntityManager} if it exists.
	 * If it doesn't exist, it creates a new one and starts its
	 * transaction.
	 * 
	 * @return this thread's {@link EntityManager}
	 */
	public static EntityManager getEntityManager() {
		EntityManager em = locals.get();
		if(em==null) {
			em = JPAEMFProvider.getEmf().createEntityManager();
			em.getTransaction().begin();
			locals.set(em);
		}
		return em;
	}

	/**
	 * Closes the opened {@link EntityManager} belonging to 
	 * the thread executing it.
	 * 
	 * @throws DAOException if closing fails
	 */
	public static void close() throws DAOException {
		EntityManager em = locals.get();
		if(em==null) {
			return;
		}
		DAOException dex = null;
		try {
			em.getTransaction().commit();
		} catch(Exception ex) {
			dex = new DAOException("Unable to commit transaction.", ex);
		}
		try {
			em.close();
		} catch(Exception ex) {
			if(dex!=null) {
				dex = new DAOException("Unable to close entity manager.", ex);
			}
		}
		locals.remove();
		if(dex!=null) throw dex;
	}
	
}