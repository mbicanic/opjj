package hr.fer.zemris.java.hw15.dao.jpa;

import javax.persistence.EntityManagerFactory;

/**
 * Class providing with an implementation of an
 * {@link EntityManagerFactory}
 * 
 * @author Miroslav Bićanić
 */
public class JPAEMFProvider {

	/** The stored {@link EntityManagerFactory} */
	public static EntityManagerFactory emf;
	
	public static EntityManagerFactory getEmf() {
		return emf;
	}
	
	public static void setEmf(EntityManagerFactory emf) {
		JPAEMFProvider.emf = emf;
	}
}