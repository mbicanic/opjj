package hr.fer.zemris.java.hw15.web.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import hr.fer.zemris.java.hw15.dao.DAOProvider;
import hr.fer.zemris.java.hw15.model.BlogUser;
import hr.fer.zemris.java.hw15.model.bridge.UserForm;

/**
 * RegisterServlet is a specification of a {@link HttpServlet} which
 * handles the registration process.
 * <br>
 * Its action differs depending on whether it was called using the
 * GET or POST methods:<br>
 * If called by the GET method, that means no data is submitted, no
 * data should be checked, and only an empty {@link UserForm} is
 * passed to the .jsp. <br>
 * If called by the POST method, submitted parameters are validated,
 * and if the validation is successful, the user is registered into
 * the database and automatically logged in. Otherwise, the client 
 * is redirected back to the registration .jsp.
 * 
 * @author Miroslav Bićanić
 */
public class RegisterServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		if(req.getSession().getAttribute("current.user.id")!=null) {
			//REGISTRATION REFUSED IF USER IS LOGGED IN
			resp.sendRedirect(resp.encodeRedirectURL(
					req.getContextPath()+ "/servleti/main"));
			return;
		}
		req.setAttribute("formData", new UserForm());
		req.getRequestDispatcher("/WEB-INF/pages/registration.jsp").forward(req, resp);
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		if(req.getSession().getAttribute("current.user.id")!=null) {
			//REGISTRATION REFUSED IF USER IS LOGGED IN
			resp.sendRedirect(resp.encodeRedirectURL(
					req.getContextPath()+ "/servleti/main"));
			return;
		}
		
		UserForm uf = new UserForm();
		uf.fillFromRequest(req);
		uf.validate();
		
		if(uf.hasErrors()) {
			req.setAttribute("formData", uf);
			req.getRequestDispatcher("/WEB-INF/pages/registration.jsp").forward(req, resp);
			return;
		}
		if(DAOProvider.getDAO().userForNick(uf.getNick())!=null) {
			uf.setError("nick", "Selected nickname is already in use");
			req.setAttribute("formData", uf);
			req.getRequestDispatcher("/WEB-INF/pages/registration.jsp").forward(req, resp);
			return;
		}
		
		BlogUser user = new BlogUser();
		uf.pourToEntity(user);
		DAOProvider.getDAO().addUser(user);
		
		req.getSession().setAttribute("current.user.id", user.getId());
		req.getSession().setAttribute("current.user.fn", user.getFirstName());
		req.getSession().setAttribute("current.user.ln", user.getLastName());
		req.getSession().setAttribute("current.user.nick", user.getNick());
		req.getSession().setAttribute("current.user.email", user.getEmail());
		
		resp.sendRedirect(resp.encodeRedirectURL(
				req.getContextPath() + "/servleti/main"));
	}
}
