<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" session="true" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
    
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title> J-Blog - Error </title>
	<style>
		.link-button {
			background: none;
  			border: none;
  			color: blue;
			text-decoration: underline;
			cursor: pointer;
			font-size: 1em;
			font-family: serif;
		}
		.header {
			background-color: #bfd7ff;
			font-family: verdana;
			font-size: 12px;
			height: 40px;
			padding: 5px;
		}
		
		.left {
			float: left;
		}
		
		.right {
			float: right;
		}
	</style>
</head>
<body style="background-color:#e5e5e5">
	<c:choose>
	<c:when test="${empty sessionScope['current.user.id']}">
		<div class="header">
			<a class="left" href="${pageContext.request.contextPath}/servleti/main"> <b>Home</b> </a>
			<p class="right"> You are not logged in. </p>
		</div>
	</c:when>
	<c:otherwise>
		<div class="header">
			<a class="left" href="${pageContext.request.contextPath}/servleti/main"> <b>Home</b> </a><br>
			<p class="left"> 
					User: ${sessionScope['current.user.fn']} ${sessionScope['current.user.ln']} 
			</p>
			<p class="right"> 
				<a href="${pageContext.request.contextPath}/servleti/logout"> Log out </a>
			</p>
		</div>
	</c:otherwise>
	</c:choose>
	
	<h1> Error </h1>
	<h4> ${error} </h4>
</body>
</html>