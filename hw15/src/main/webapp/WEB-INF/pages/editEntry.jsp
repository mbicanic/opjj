<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" session="true" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
    
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>J-Blog - Entry editor</title>
	<style>
		.header {
			background-color: #bfd7ff;
			font-family: verdana;
			font-size: 12px;
			height: 40px;
			padding: 5px;
		}
		.left {
			float: left;
		}
		.right {
			float: right;
		}
	</style>
</head>
<body style="background-color:#e5e5e5">
	<div class="header">
		<a class="left" href="${pageContext.request.contextPath}/servleti/main"> <b>Home</b> </a><br>
		<p class="left"> 
				User: ${sessionScope['current.user.fn']} ${sessionScope['current.user.ln']} 
		</p>
		<p class="right"> 
			<a  href="${pageContext.request.contextPath}/servleti/logout"> Log out </a>
		</p>
	</div>
	
	<form action="${requestScope.action}" method="post" id="content">
			<input type="hidden" name="source" value="form">
			<input type="hidden" name="id" value="${entryForm.id}"><br>
			Title:<br>
			<input type="text" name="title" value="${entryForm.title}" > <br>
			<c:if test="${entryForm.hasError('title')}">
				<p style="color:red"> ${entryForm.fetchError('title')} </p>
			</c:if>
		Content:<br>
			<textarea form="content" name="text" rows="4" cols="50" placeholder="Enter text..">${
			entryForm.text
			}</textarea> <br>
			<c:if test="${entryForm.hasError('text')}">
				<p style="color:red"> ${entryForm.fetchError('text')} </p>
			</c:if>
		<input type="submit" value="Submit">
	</form>

</body>
</html>