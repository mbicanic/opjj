<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" session="true" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
    
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title> J-Blog - ${entry.title} </title>
	<style>
		.link-button {
			background: none;
  			border: none;
  			color: blue;
			text-decoration: underline;
			cursor: pointer;
			font-size: 1em;
			font-family: serif;
		}
		.header {
			background-color: #bfd7ff;
			font-family: verdana;
			font-size: 12px;
			height: 40px;
			padding: 5px;
		}
		
		.left {
			float: left;
		}
		
		.right {
			float: right;
		}
		
		.main {
			text-align: center;
			font-family: verdana;
			font-weight: bold;
			color: blue;
			margin-bottom: 10px;
		}
		
		h3.section {
			font-family: verdana;
			font-weight: bold;
			color: black;
		}
		
		.box {
			padding:10px;
			margin-left:50px;
			margin-right:50px;
			border-radius: 20px;
			border: 2px solid #4286f4;
			background-color: #bfd7ff;	
		}
		
		.listElement {
			margin: 5px;
			font-family: tahoma;
		}
		
		.entry {
			border-radius: 20px;
			border: 2px solid #4286f4;
			background-color: #bfd7ff;
			font-family: tahoma;
			padding: 20px;
			margin-left:50px;
			margin-right:50px;
			margin-top:0px;
		}
	</style>
</head>
<body style="background-color:#e5e5e5">
	<c:choose>
	<c:when test="${empty sessionScope['current.user.id']}">
		<div class="header">
			<a class="left" href="${pageContext.request.contextPath}/servleti/main"> <b>Home</b> </a>
			<p class="right"> You are not logged in. </p>
		</div>
	</c:when>
	<c:otherwise>
		<div class="header">
			<a class="left" href="${pageContext.request.contextPath}/servleti/main"> <b>Home</b> </a><br>
			<p class="left"> 
					User: ${sessionScope['current.user.fn']} ${sessionScope['current.user.ln']} 
			</p>
			<p class="right"> 
				<a href="${pageContext.request.contextPath}/servleti/logout"> Log out </a>
			</p>
		</div>
	</c:otherwise>
	</c:choose>
	
	<hr>
	<h2 class="main"> ${entry.title} </h2>
	<p class="right"><i>Posted on: ${entry.createdAt}</i></p>
	<div style="clear:both"></div>
	
	<p class="entry"> ${entry.text} </p>
	
	<c:if test="${sessionScope['current.user.id'] eq entry.creator.id}">
		<form method="post" action="edit">
			<input type="hidden" name="source" value="link">
			<input type="hidden" name="eid" value="${entry.id}">
			<button type="submit" class="link-button listElement">
				Edit this entry
			</button>
		</form>
	</c:if>
	<hr><br>
	
	<h3 class="section"> Comments: </h3>
	
	<c:choose>
		<c:when test="${empty entry.comments}">
			<p><i>There are no comments for this post</i></p>
		</c:when>
		<c:otherwise>
		<div class="box">
			<c:forEach var="c" items="${entry.comments}">
				<p><b>${c.email}, ${c.postedOn}</b></p>
				<p>${c.message}</p>
				<hr>
			</c:forEach>
		
		</div>
		</c:otherwise>
	</c:choose>
	
	<h3 class="section"> Leave a comment: </h3>
	<div class="box">
	
	<form method="post" action="${entry.id}" id="comment">
		E-mail:<br>
		<input type="text" name="email" value="${commentForm.email}" ${readonly}> <br>
		<c:if test="${commentForm.hasError('email')}">
			<p style="color:red"> ${commentForm.fetchError('email')} </p>
		</c:if>
		Comment:<br>
		<textarea name="msg" form="comment" rows="4" cols="50" placeholder="Enter comment..">${
		commentForm.message
		}</textarea><br>
		<c:if test="${commentForm.hasError('msg')}">
			<p style="color:red"> ${commentForm.fetchError('msg')} </p>
		</c:if>
		<input type="submit" value="Submit">
	</form>
	</div>
		
</body>
</html>