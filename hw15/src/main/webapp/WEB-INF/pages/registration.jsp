<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" session="true" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
    
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>J-Blog - Create an account</title>
	<style>
		.header {
			background-color: #bfd7ff;
			font-family: verdana;
			font-size: 12px;
			height: 40px;
			padding: 5px;
		}
		
		.left {
			float: left;
		}
		
		.right {
			float: right;
		}
	</style>
</head>
<body style="background-color:#e5e5e5">
		<div class="header">
			<a class="left" href="${pageContext.request.contextPath}/servleti/main"> <b>Home</b></a>
			<p class="right"> You are not logged in. </p>
		</div>
	<hr>
	
	<form action="register" method="post">
		First name:<br>
		<input type="text" name="fname" value="${formData.firstName}" > <br>
		<c:if test="${formData.hasError('fname')}">
			<p style="color:red"> ${formData.fetchError('fname')} </p>
		</c:if>
		Last name:<br>
		<input type="text" name="lname" value="${formData.lastName}" > <br>
		<c:if test="${formData.hasError('lname')}">
			<p style="color:red"> ${formData.fetchError('lname')} </p>
		</c:if>
		E-mail:<br>
		<input type="text" name="email" value="${formData.email}" > <br>
		<c:if test="${formData.hasError('email')}">
			<p style="color:red"> ${formData.fetchError('email')} </p>
		</c:if>
		Nickname:<br>
		<input type="text" name="nick" value="${formData.nick}" > <br>
		<c:if test="${formData.hasError('nick')}">
			<p style="color:red"> ${formData.fetchError('nick')} </p>
		</c:if>
		Password:<br>
		<input type="password" name="pwd" value="" > <br>
		<c:if test="${formData.hasError('pwd')}">
			<p style="color:red"> ${formData.fetchError('pwd')} </p>
		</c:if>
		
		<input type="submit" value="Register">
	</form>

</body>
</html>