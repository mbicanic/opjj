<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" session="true" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
    
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>J-Blog - ${author.nick}</title>
	<style>
		.header {
			background-color: #bfd7ff;
			font-family: verdana;
			font-size: 12px;
			height: 40px;
			padding: 5px;
		}
		
		.left {
			float: left;
		}
		
		.right {
			float: right;
		}
		
		h1.main {
			text-align: center;
			font-family: verdana;
			font-weight: bold;
			color: blue;
		}
		
		.box {
			border-radius: 20px;
			border: 2px solid #4286f4;
			background-color: #bfd7ff;	
		}
		
		.listElement {
			margin: 5px;
			font-family: tahoma;
		}
		
	</style>
</head>
<body style="background-color:#e5e5e5">
	<c:choose>
	<c:when test="${empty sessionScope['current.user.id']}">
		<div class="header">
			<a class="left" href="${pageContext.request.contextPath}/servleti/main"> <b>Home</b> </a>
			<p class="right"> You are not logged in. </p>
		</div>
	</c:when>
	<c:otherwise>
		<div class="header">
			<a class="left" href="${pageContext.request.contextPath}/servleti/main"> <b>Home</b> </a><br>
			<p class="left"> 
					User: ${sessionScope['current.user.fn']} ${sessionScope['current.user.ln']} 
			</p>
			<p class="right"> 
				<a  href="${pageContext.request.contextPath}/servleti/logout"> Log out </a>
			</p>
		</div>
	</c:otherwise>
	</c:choose>
	
	<hr>
	<h1 class="main"> ${author.nick}'s blog </h1>
	<hr>
	
	<c:choose>
	<c:when test="${empty entries}">
		<p class="listElement"> <i> ${author.nick} does not have any posts yet. </i> </p>
	</c:when>
	<c:otherwise>
		<div class="box">
			<ul>
				<c:forEach var="e" items="${entries}">
				<li> <a href="${author.nick}/${e.id}" class="listElement"> ${e.title}</a> </li>
				</c:forEach>
			</ul>
		</div>
	</c:otherwise>
	</c:choose>
	
	<c:if test="${sessionScope['current.user.id'] eq author.id}">
		<br>
		<a href="${author.nick}/new" class="listElement"> Add a new blog post! </a>
	</c:if>
</body>
</html>