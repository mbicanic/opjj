<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" session="true" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
    
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>J-Blog - Home</title>
	<style>
		.header {
			background-color: #bfd7ff;
			font-family: verdana;
			font-size: 12px;
			height: 40px;
			padding: 5px;
		}
		
		.left {
			float: left;
		}
		
		.right {
			float: right;
		}
		
		h1.main {
			text-align: center;
			font-family: verdana;
			font-weight: bold;
			color: blue;
		}
		
		h3.section {
			font-family: verdana;
			font-weight: bold;
			color: black;
		}
		
		.box {
			border-radius: 20px;
			border: 2px solid #4286f4;
			background-color: #bfd7ff;	
			width: 500px;
		}
		
		.listElement {
			font-family: tahoma;
			margin: 5px;
		}
		
		
	</style>
</head>
<body style="background-color:#e5e5e5">
	<c:choose>
	<c:when test="${empty sessionScope['current.user.id']}">
		<div class="header">
			<a class="left" href="${pageContext.request.contextPath}/servleti/main"> <b>Home</b> </a>
			<p class="right"> You are not logged in. </p>
		</div>
	</c:when>
	<c:otherwise>
		<div class="header">
			<a class="left" href="${pageContext.request.contextPath}/servleti/main"> <b>Home</b> </a><br>
			<p class="left"> 
					User: ${sessionScope['current.user.fn']} ${sessionScope['current.user.ln']} 
			</p>
			<p class="right"> 
				<a  href="${pageContext.request.contextPath}/servleti/logout"> Log out </a>
			</p>
		</div>
	</c:otherwise>
	</c:choose>
	<hr>
	
	<h1 class="main"> J-Blog </h1>
	<hr>
	<c:if test="${empty sessionScope['current.user.id']}">
		<h3 class="section"> Login </h3>
		<form method="post" action="main">
			Username: <br>
			<input type="text" name="nick" value="${loginForm.nick}"><br><br>
			<c:if test="${loginForm.hasError('nick')}">
				<p style="color:red"> ${loginForm.fetchError('nick')} </p>
			</c:if>
			Password: <br><input type="password" name="pwd"><br><br>
			<c:if test="${loginForm.hasError('pwd')}">
				<p style="color:red"> ${loginForm.fetchError('pwd')} </p>
			</c:if>
			<input type="submit" value="Login">
		</form>
		<br>
		<a href="register"> Create new account </a>
		<hr>
	</c:if>
	
	<h3 class="section"> Registered authors: </h3>
	<div class="box">
	<ul>
	<c:forEach var="u" items="${applicationScope.users}">
		<li class="listElement"> <a href="author/${u.nick}"> ${u.firstName} ${u.lastName} </a> </li>
	</c:forEach>
	</ul>
	</div>
	
</body>
</html>