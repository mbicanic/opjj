package hr.fer.zemris.java.custom.scripting.exec;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class ValueWrapperTest {

	@Test
	void testConstructor() {
		ValueWrapper v = new ValueWrapper();
		assertNull(v.getValue());
		
		v = new ValueWrapper(null);
		assertNull(v.getValue());
		
		v = new ValueWrapper(Boolean.TRUE);
		assertTrue(v.getValue() instanceof Boolean);
		assertTrue((Boolean)v.getValue());
		
		v = new ValueWrapper("String example");
		assertEquals("String example", v.getValue());
		
		v = new ValueWrapper(Integer.valueOf(5));
		assertEquals(5, v.getValue());
	}
	
	@Test
	void testGet() {
		ValueWrapper v = new ValueWrapper(new StringBuilder(6));
		((StringBuilder)v.getValue()).append("sample");
		assertEquals("sample", ((StringBuilder)v.getValue()).toString());
	}
	
	@Test
	void testSet() {
		ValueWrapper v = new ValueWrapper(5);
		v.setValue("sample");
		assertEquals("sample",v.getValue());
	}
	/*
	 * Helper method to run through all arithmetic operations
	 * and assert the correct exception is thrown.
	 */
	void assertExceptions(ValueWrapper arg1, Object arg2, String message) {
		try {
			arg1.add(arg2);
		} catch (RuntimeException ex) {
			assertTrue(ex.getMessage().startsWith(message));
		}
		try {
			arg1.sub(arg2);
		} catch (RuntimeException ex) {
			assertTrue(ex.getMessage().startsWith(message));
		}
		try {
			arg1.mul(arg2);
		} catch (RuntimeException ex) {
			assertTrue(ex.getMessage().startsWith(message));
		}
		try {
			arg1.div(arg2);
		} catch (RuntimeException ex) {
			assertTrue(ex.getMessage().startsWith(message));
		}
		try {
			arg1.numCompare(arg2);
		} catch (RuntimeException ex) {
			assertTrue(ex.getMessage().startsWith(message));
		}
	}
	
	@Test
	void testIllegalArgument(){
		ValueWrapper v1 = new ValueWrapper(Integer.valueOf(5));
		ValueWrapper v2 = new ValueWrapper(Double.valueOf(3.14));
		assertExceptions(v1, Boolean.TRUE, "Cannot perform arithmetic operation");
		assertExceptions(v2, Boolean.TRUE, "Cannot perform arithmetic operation");
		v1.setValue(Boolean.TRUE);
		v2.setValue(Boolean.TRUE);
		assertExceptions(v1, Integer.valueOf(5), "Cannot perform arithmetic operation");
		assertExceptions(v2, Double.valueOf(3.14), "Cannot perform arithmetic operation");
	}
	
	@Test
	void testOneIllegalString() {
		ValueWrapper v = new ValueWrapper(Integer.valueOf(5));
		assertExceptions(v, "unparsable", "Cannot parse 'unparsable' to any");
		v.setValue("unparsable");
		assertExceptions(v, Double.valueOf(3.14), "Cannot parse 'unparsable' to any");
	}
	
	@Test
	void testIntDivisionByZero() {
		ValueWrapper v = new ValueWrapper(Integer.valueOf(5));
		try {
			v.div(0);
		} catch (RuntimeException ex) {
			assertEquals("Cannot divide by zero!", ex.getMessage());
		}
	}
	
	@Test
	void testNullArgument() {
		ValueWrapper v = new ValueWrapper();	
		v.add(5);			//null+5
		assertEquals(5, v.getValue());
		v.add(null);		//5+null
		assertEquals(5, v.getValue());
		v.setValue(2.5);
		v.add(null);		//2.5+null
		assertEquals(2.5, v.getValue());
		v.setValue(null);	
		v.add(6.25);		//null+6.25
		assertEquals(6.25, v.getValue());
		
		v.setValue(null);
		v.add(null);
		assertEquals(0, v.getValue());
	}
	
	@Test
	void testIntWithOthers() {
		ValueWrapper v = new ValueWrapper(5);
		v.sub(2);
		assertEquals(3, v.getValue());
		v.mul("3");
		assertEquals(9, v.getValue());
		v.div(2.0);
		assertEquals(4.5, v.getValue());
		assertEquals(1, v.numCompare(4));
		assertEquals(-1, v.numCompare(4.6));
		assertEquals(0, v.numCompare("45e-1"));
	}
	
	@Test
	void testDoubleWithOthers() {
		ValueWrapper v = new ValueWrapper(12.5);
		v.sub(2);
		assertEquals(10.5, v.getValue());
		v.mul("2");
		assertEquals(21.0, v.getValue());
		v.div(3.0);
		assertEquals(7.0, v.getValue());
		assertEquals(1, v.numCompare(4));
		assertEquals(-1, v.numCompare(8));
		assertEquals(0, v.numCompare("0.7E1"));
	}
	
	@Test
	void testStringConversion() {
		ValueWrapper v = new ValueWrapper();
		v.add("12");
		assertEquals(v.getValue(), 12);
		v.mul("0.5");
		assertEquals(v.getValue(), 6.0);
		v.div("20e-1");
		assertEquals(v.getValue(), 3.0);
		v.sub("1500e-3");
		assertEquals(v.getValue(), 1.5);
	}

}
