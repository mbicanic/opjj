package hr.fer.zemris.java.custom.scripting.exec;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class ObjectMultistackTest {

	@Test
	void testPush() {
		ObjectMultistack mst = new ObjectMultistack();
		assertThrows(NullPointerException.class, ()->mst.push(null, new ValueWrapper(5)));
		assertThrows(NullPointerException.class, ()->mst.push("sampleKey", null));
		
		mst.push("new stack", new ValueWrapper("value"));
		assertEquals(mst.peek("new stack").getValue(), "value");
		
		ValueWrapper v = new ValueWrapper(2005);
		mst.push("year", v);
		mst.push("year", v);
		assertEquals(mst.pop("year"), v);
		assertEquals(mst.pop("year"), v);
		
		mst.push("date", new ValueWrapper("21.1.2011"));
		mst.push("date", new ValueWrapper("27.8.2015"));
		assertEquals(mst.pop("date").getValue(), "27.8.2015");
		assertEquals(mst.pop("date").getValue(), "21.1.2011");
	}
	
	@Test
	void testPop() {
		ObjectMultistack mst = new ObjectMultistack();
		assertThrows(NullPointerException.class, ()->mst.pop(null));
		assertThrows(EmptyMultistackException.class, ()->mst.pop("year"));
		mst.push("year", new ValueWrapper(50));
		mst.push("year", new ValueWrapper(15));
		mst.push("name", new ValueWrapper("sample"));
		assertEquals(mst.pop("year").getValue(), 15);
		assertEquals(mst.pop("year").getValue(), 50);
		assertThrows(EmptyMultistackException.class, ()->mst.pop("year"));
		assertEquals(mst.pop("name").getValue(), "sample");
		assertThrows(EmptyMultistackException.class, ()->mst.pop("name"));
	}
	
	@Test
	void testPeek() {
		ObjectMultistack mst = new ObjectMultistack();
		assertThrows(NullPointerException.class, ()->mst.peek(null));
		assertThrows(EmptyMultistackException.class, ()->mst.peek("year"));
		mst.push("year", new ValueWrapper(50));
		mst.push("year", new ValueWrapper(15));
		mst.push("name", new ValueWrapper("sample"));
		assertEquals(mst.peek("year").getValue(), 15);
		assertEquals(mst.peek("year").getValue(), 15);
		mst.pop("year");
		mst.pop("year");
		assertThrows(EmptyMultistackException.class, ()->mst.peek("year"));
		assertEquals(mst.peek("name").getValue(), "sample");
		mst.pop("name");
		assertThrows(EmptyMultistackException.class, ()->mst.peek("name"));
	}

	@Test
	void testIsEmpty() {
		ObjectMultistack mst = new ObjectMultistack();
		assertThrows(NullPointerException.class, ()->mst.isEmpty(null));
		assertTrue(mst.isEmpty("year"));
		mst.push("year", new ValueWrapper(2005));
		assertFalse(mst.isEmpty("year"));
		mst.pop("year");
		assertTrue(mst.isEmpty("year"));
	}
}
