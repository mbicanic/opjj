package hr.fer.zemris.java.hw07.observer2;

/**
 * A class providing multiple concrete implementations of 
 * IntegerStorageObservers.
 * 
 * @author Miroslav Bićanić
 */
public class Observers {
	
	/**
	 * SquareValue outputs a square of the new value given to an
	 * IntegerStorage whenever the IntegerStorage's value is updated.
	 * @author Miroslav Bićanić
	 */
	public static class SquareValue implements IntegerStorageObserver{
		@Override
		public void valueChanged(IntegerStorageChange isc) {
			int val = isc.getNewValue();
			System.out.printf("Provided new value: %d, square is %d%n",
					val, val*val);
		}
	}
	
	/**
	 * DoubleValue outputs the new value given to an IntegerStorage 
	 * multiplied by two, whenever the IntegerStorage's value is updated.
	 * 
	 * It does so only a given number of times, before unregistering
	 * itself from the IntegerStorage.
	 * 
	 * @author Miroslav Bićanić
	 */
	public static class DoubleValue implements IntegerStorageObserver{
		/** The remaining number of times to perform the action before
		 * unregistering from the IntegerStorage
		 */
		private int remaining;
		
		/**
		 * A constructor for a DoubleValue observer
		 * @param repeat The number of times to perform the action
		 */
		public DoubleValue(int repeat) {
			this.remaining = repeat;
		}
		
		@Override
		public void valueChanged(IntegerStorageChange isc) {
			if(remaining==0) {
				isc.getStorage().removeObserver(this);
				return;
			}
			System.out.println("Double value: "+ (isc.getNewValue()*2));
			remaining--;
		}
	}
	
	/**
	 * ChangeCounter counts the number of times an IntegerStorage at
	 * which it is registered changed its value.
	 * 
	 * @author Miroslav Bićanić
	 */
	public static class ChangeCounter implements IntegerStorageObserver{
		private int changes=0;
		@Override
		public void valueChanged(IntegerStorageChange isc) {
			System.out.println("Number of value changes since tracking: "+ (++changes));
		}
	}
}
