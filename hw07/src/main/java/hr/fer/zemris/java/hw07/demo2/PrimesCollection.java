package hr.fer.zemris.java.hw07.demo2;

import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * PrimesCollection is a collection of prime numbers, containing
 * the first N prime numbers, where N is given at construction.
 * 
 * @author Miroslav Bićanić
 */
public class PrimesCollection implements Iterable<Integer>{
	/** The number of prime numbers to generate. */
	private int amount;
	
	/**
	 * A constructor for a PrimesCollection
	 * @param amount The number of prime numbers to generate
	 */
	public PrimesCollection(int amount) {
		this.amount = amount;
	}
	
	@Override
	public Iterator<Integer> iterator() {
		return new PrimeIterator();
	}
	
	/**
	 * An iterator over the PrimesCollection returning a new
	 * prime as long as the number of returned primes does not
	 * exceed the maximal number defined by the PrimesCollection
	 * creating this iterator.
	 * 
	 * @author Miroslav Bićanić
	 */
	private class PrimeIterator implements Iterator<Integer>{
		
		/** The number of primes this iterator has returned */
		private int returned=0;
		
		/** 
		 * The number this iterator will return on next call of
		 * {@link #next()}. It is by default set to the first
		 * prime number = 2.
		 */
		private int currentNumber=2;
		
		@Override
		public boolean hasNext() {
			return returned<amount;
		}

		@Override
		public Integer next() {
			if(!hasNext()) {
				throw new NoSuchElementException();
			}
			int result = currentNumber;
			currentNumber++;
			while(!isPrime(currentNumber)) {
				currentNumber++;
			}
			returned++;
			return result;
		}
		
		/**
		 * Checks if a number is a prime number, using the simplest
		 * prime number detection algorithm.
		 * 
		 * @param number The number whose primality to check
		 * @return true if the given number is prime, false otherwise
		 */
		private boolean isPrime(int number) {
			if(number%2==0) {
				return false;
			}
			for(int i=3; i<=Math.sqrt(number); i+=2) {
				if(number%i==0) {
					return false;
				}
			}
			return true;
		}
	}
}
