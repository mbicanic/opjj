package hr.fer.zemris.java.hw07.observer1;

/**
 * Represents an observer of {@link IntegerStorage} objects,
 * that do an action when the value of given IntegerStorage
 * changes.
 * 
 * This is a functional interface whose functional method is
 * {@link #valueChanged(IntegerStorage)}
 * 
 * @author Miroslav Bićanić
 */
@FunctionalInterface
public interface IntegerStorageObserver {
	/**
	 * The action this IntegerStorageObserver performs when the
	 * value of the given IntegerStorage changes.
	 * 
	 * @param istorage The IntegerStorage that changed its value
	 */
	void valueChanged(IntegerStorage istorage);
}
