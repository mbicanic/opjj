package hr.fer.zemris.java.hw07.observer2;

/**
 * Represents an observer of {@link IntegerStorage} objects,
 * that do an action when the an IntegerStorage's value
 * changes.
 * 
 * This is a functional interface whose functional method is
 * {@link #valueChanged(IntegerStorage)}
 * 
 * @author Miroslav Bićanić
 */
@FunctionalInterface
public interface IntegerStorageObserver {
	/**
	 * The action this IntegerStorageObserver performs when a change
	 * in an IntegerStorage occurs.
	 * 
	 * @param isc An IntegerStorageChange object representing the change
	 * 				that happened in an IntegerStorage
	 */
	void valueChanged(IntegerStorageChange isc);
}
