package hr.fer.zemris.java.custom.scripting.exec;

/**
 * This exception should be thrown whenever a request
 * to pop or peek from an empty virtual stack occurs.
 * 
 * @author Miroslav Bićanić
 */
public class EmptyMultistackException extends RuntimeException {
	private static final long serialVersionUID = 1L;
	
	/**
	 * Default constructor for an EmptyMultistackException
	 */
	public EmptyMultistackException() {
		super();
	}
	/**
	 * A constructor for an EmptyMultistackException taking
	 * a message describing the error that caused the exception
	 * 
	 * @param message Description of the cause of the exception
	 */
	public EmptyMultistackException(String message) {
		super(message);
	}
}
