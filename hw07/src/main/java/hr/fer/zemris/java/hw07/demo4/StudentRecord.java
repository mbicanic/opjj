package hr.fer.zemris.java.hw07.demo4;

import java.util.Objects;

/**
 * This class models one Student attending one subject in one semester
 * with the following attributes:<br>
 * 
 * 		jmbag			-	the ID number of the student, it is also the key attribute,
 * 							meaning two students are considered equal if their jmbag 
 * 								attributes are equal. <br>
 * 		last name		-	the last name of the student.<br>
 * 		first name		-	the first name of the student.<br>
 *		points-midterm	-	the points the student achieved on the midterm exam.<br>
 *		points-final	-	the points the student acieved on the final exam.<br>
 *		points-lab		-	the points the student achieved on laboratory exercises.<br>
 * 		grade			-	the grade of the student.<br>
 * 
 * @author Miroslav Bićanić
 */
public class StudentRecord {
	/**
	 * The jmbag of the student.
	 */
	private String jmbag;
	/**
	 * The last name of the student.
	 */
	private String lastName;
	/**
	 * The first name of the student.
	 */
	private String firstName;
	/**
	 * The mid-term exam points of the student.
	 */
	private double pointsMid;
	/**
	 * The final exam points of the student.
	 */
	private double pointsFinal;
	/**
	 * The laboratory exercises points of the student.
	 */
	private double pointsLab;
	/**
	 * The grade of the student
	 */
	private int grade;
	
	
	/**
	 * A constructor for a StudentRecord.
	 * 
	 * @param args An array of arguments, containing all data required
	 */
	public StudentRecord(String[] args) {
		/*
		 * I am aware that taking an array of arguments is a terrible idea,
		 * but this class will most likely not be used later, and it makes
		 * converting lines of text into StudentRecords much easier.
		 */
		if(args.length!=7) {
			throw new IllegalArgumentException();
		}
		this.jmbag = args[0];
		this.lastName = args[1];
		this.firstName = args[2];
		try {
			this.pointsMid = Double.parseDouble(args[3]);
			this.pointsFinal = Double.parseDouble(args[4]);
			this.pointsLab = Double.parseDouble(args[5]);
			this.grade = Integer.parseInt(args[6]);
		} catch(NumberFormatException ex) {
			return;
		}
	}
	/**
	 * @return This StudentRecord's first name
	 */
	public String getFirstName() {
		return firstName;
	}
	/**
	 * @return This StudentRecord's last name
	 */
	public String getLastName() {
		return lastName;
	}
	/**
	 * @return This StudentRecord's jmbag
	 */
	public String getJmbag() {
		return jmbag;
	}
	/**
	 * @return This StudentRecord's midterm exams points
	 */
	public double getMidtermPoints() {
		return pointsMid;
	}
	/**
	 * @return This StudentRecord's final exams points
	 */
	public double getFinalPoints() {
		return pointsFinal;
	}
	/**
	 * @return This StudentRecord's laboratory exercises points
	 */
	public double getLabPoints() {
		return pointsLab;
	}
	/**
	 * @return This StudentRecord's grade
	 */
	public int getGrade() {
		return grade;
	}
	/**
	 * Returns the sum of all points this student acquired
	 * @return
	 */
	double pointSum() {
		return pointsMid+pointsFinal+pointsLab;
	}
	
	@Override
	public String toString() {
		return String.format("%s\t%s\t%s\t%.2f\t%.2f\t%.2f\t%d",
				jmbag, lastName, firstName,
				pointsMid, pointsFinal, pointsLab,
				grade);
	}
	
	/**
	 * Two StudentRecords are considered equal when their
	 * jmbag attributes are equal.
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof StudentRecord))
			return false;
		StudentRecord other = (StudentRecord) obj;
		return Objects.equals(jmbag, other.jmbag);
	}
	
	@Override
	public int hashCode() {
		return Objects.hash(jmbag);
	}
}
