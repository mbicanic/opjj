package hr.fer.zemris.java.hw07.observer2;

public class IntegerStorageChange {
	private IntegerStorage storage;
	private int oldValue;
	private int newValue;
	
	public IntegerStorageChange(IntegerStorage s, int old, int changed) {
		this.storage = s;
		this.oldValue = old;
		this.newValue = changed;
	}

	public IntegerStorage getStorage() {
		return storage;
	}

	public int getOldValue() {
		return oldValue;
	}

	public int getNewValue() {
		return newValue;
	}
	
}
