package hr.fer.zemris.java.hw07.observer2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

/**
 * A class holding a primitive integer value, used for
 * demonstration of the <i>observer</i> design pattern.
 * 
 * @author Miroslav Bićanić
 */
public class IntegerStorage {
	
	/** The value held by this IntegerStorage */
	private int value;
	
	/** A list of observers observing this IntegerStorage */
	private List<IntegerStorageObserver> observers;
	
	/**
	 * A constructor for an IntegerStorage
	 * @param initialValue the value for this IntegerStorage
	 */
	public IntegerStorage(int initialValue) {
		this.value = initialValue;
		this.observers = new ArrayList<>();
	}
	
	/**
	 * @return the value in this IntegerStorage
	 */
	public int getValue() {
		return value;
	}
	/**
	 * @param value the new value for this IntegerStorage
	 */
	public void setValue(int value) {
		if(this.value!=value) {
			IntegerStorageChange isc = new IntegerStorageChange(this, this.value, value);
			this.value = value;
			informAll(isc);
		}
	}
	
	/**
	 * Adds an observer to this IntegerStorage's list of observers.
	 * 
	 * The action is done without modifying the current list of
	 * observers, avoiding a ConcurrentModificationException when
	 * an observer is added during iteration over observers.
	 * 
	 * If the observer is already registered to this IntegerStorage,
	 * the method does nothing.
	 * 
	 * @param observer The new observer to add to this IntegerStorage
	 */
	public void addObserver(IntegerStorageObserver observer) {
		Objects.requireNonNull(observer);
		if(observers.contains(observer)) {
			return;
		}
		IntegerStorageObserver[] arr = observers.toArray(
				new IntegerStorageObserver[observers.size()+1]
				);
		arr[observers.size()] = observer;
		observers = Arrays.asList(arr);
	}
	
	/**
	 * Removes an observer from this IntegerStorage's list of observers.
	 * 
	 * The action is done without modifying the current list of
	 * observers, avoiding a ConcurrentModificationException when
	 * an observer is removed during iteration over observers.
	 * 
	 * If the observer is not registered to this IntegerStorage,
	 * the method does nothing.
	 * 
	 * @param observer The observer to remove from this IntegerStorage
	 */
	public void removeObserver(IntegerStorageObserver observer) {
		if(!observers.contains(observer)) {
			return;
		}
		Object[] arr = observers.toArray();
		ArrayList<IntegerStorageObserver> newList = new ArrayList<>();
		for(Object o : arr) {
			if(o.equals(observer)) {
				continue;
			}
			newList.add((IntegerStorageObserver)o);
		}
		observers = newList;
	}
	
	/**
	 * Removes all observers from this IntegerStorage's list of 
	 * observers.
	 * 
	 * The action is done without modifying the current list of
	 * observers, avoiding a ConcurrentModificationException when
	 * it is called during an existing iteration over observers.
	 */
	public void clearObservers() {
		observers = Arrays.asList(new IntegerStorageObserver[] {});
	}
	
	/**
	 * Notifies all registered observers that a modification to
	 * this IntegerStorage's value occurred.
	 */
	private void informAll(IntegerStorageChange isc) {
		if(observers!=null) {
			for(IntegerStorageObserver observer : observers) {
				observer.valueChanged(isc);
			}
		}
	}
}
