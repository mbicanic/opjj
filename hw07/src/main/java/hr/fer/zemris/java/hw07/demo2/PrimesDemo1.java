package hr.fer.zemris.java.hw07.demo2;

/**
 * The first demonstration program showing how does the 
 * implementation of PrimesCollection work.
 * 
 * @author Miroslav Bićanić
 */
public class PrimesDemo1 {
	public static void main(String[] args) {
		PrimesCollection primesCollection = new PrimesCollection(5);
		for(Integer i : primesCollection) {
			System.out.println("Got prime: "+i);
		}
	}
}
