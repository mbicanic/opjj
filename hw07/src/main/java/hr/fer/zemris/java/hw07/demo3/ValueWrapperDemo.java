package hr.fer.zemris.java.hw07.demo3;

import hr.fer.zemris.java.custom.scripting.exec.ValueWrapper;

/**
 * Demonstration program for our implementation of 
 * {@link ValueWrapper}.
 * 
 * @author Miroslav Bićanić
 */
public class ValueWrapperDemo {
	public static void main(String[] args) {
		//first test
		ValueWrapper vv1 = new ValueWrapper(Boolean.valueOf(true));
		try{
			vv1.add(Integer.valueOf(5));  // ==> throws, since current value is boolean
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
		}
		ValueWrapper vv2 = new ValueWrapper(Integer.valueOf(5));
		try{
			vv2.add(Boolean.valueOf(true));  // ==> throws, since the argument value is boolean
		} catch(Exception ex) {
			System.out.println(ex.getMessage());
		}
		
		//second test
		ValueWrapper v1 = new ValueWrapper(null);
		ValueWrapper v2 = new ValueWrapper(null);
		v1.add(v2.getValue());
		System.out.println(v1.getValue());
		System.out.println(v2.getValue());

		ValueWrapper v3 = new ValueWrapper("1.2E1");
		ValueWrapper v4 = new ValueWrapper(Integer.valueOf(1));
		v3.add(v4.getValue());
		System.out.println(v3.getValue());
		System.out.println(v4.getValue());

		ValueWrapper v5 = new ValueWrapper("12");
		ValueWrapper v6 = new ValueWrapper(Integer.valueOf(1));
		v5.add(v6.getValue());
		System.out.println(v5.getValue());
		System.out.println(v6.getValue());

		ValueWrapper v7 = new ValueWrapper("Ankica");
		ValueWrapper v8 = new ValueWrapper(Integer.valueOf(1));
		try {
			v7.add(v8.getValue());
		} catch(Exception ex) {
			System.out.println(ex.getMessage());
		}
	}
}
