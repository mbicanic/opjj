package hr.fer.zemris.java.hw07.demo4;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;
import java.util.stream.Collectors;

//Javadoc na hrvatskom zbog propisanih hrvatskih naziva metoda.

/**
 * Demonstracijski program koji pokazuje korištenje Stream API-a
 * za imitiranje upita prema bazi podataka koja je modelirana 
 * listom {@link StudentRecord} objekata.
 * 
 * @author Miroslav Bićanić
 */
public class StudentDemo {

	/** Znak kojim su razdvojeni parametri studenta u izvornom zapisu */
	private static final String TAB = "\t";
	
	/**
	 * Consumer koji ispisuje vrijednost predanog objekta na
	 * standardni izlaz.
	 * 
	 * Napomena: stvoren kako bi se garbage collector oslobodio od
	 * uklanjanja mnoštva objekata stvorenih lambda izrazima nakon
	 * svake od metoda.
	 */
	private static final Consumer<Object> PRINTER = o -> System.out.println(o);
	
	/**
	 * Ulazna točka za demonstracijski program.
	 * 
	 * @param args nisu korišteni
	 * @throws IOException Ukoliko je datoteka "studenti.txt" nedostupna
	 */
	public static void main(String[] args) throws IOException {
		List<String> lines = Files.readAllLines(Paths.get("./studenti.txt"));
		List<StudentRecord> records = convert(lines);
		
		System.out.println("\nZadatak 1\n=========");
		System.out.println(vratiBodovaViseOd25(records));

		System.out.println("\nZadatak 2\n=========");
		System.out.println(vratiBrojOdlikasa(records));

		System.out.println("\nZadatak 3\n=========");
		vratiListuOdlikasa(records).forEach(PRINTER);

		System.out.println("\nZadatak 4\n=========");
		vratiSortiranuListuOdlikasa(records).forEach(PRINTER);

		System.out.println("\nZadatak 5\n=========");
		vratiPopisNepolozenih(records).forEach(PRINTER);

		System.out.println("\nZadatak 6\n=========");
		razvrstajStudentePoOcjenama(records).entrySet()
		.forEach((entry)->{
			System.out.println("KEY: "+entry.getKey());
			entry.getValue().forEach(PRINTER);
		});

		System.out.println("\nZadatak 7\n=========");
		vratiBrojStudenataPoOcjenama(records).entrySet()
		.forEach((entry)->{
			System.out.printf("%d -> %d%n", entry.getKey(), entry.getValue());
		});

		System.out.println("\nZadatak 8\n=========");
		razvrstajProlazPad(records).entrySet()
		.forEach((entry)->{
			System.out.println(entry.getKey()==Boolean.TRUE ? "PROŠLI: " : "PALI: ");
			entry.getValue().forEach(PRINTER);
		});
		
	}
	
	/**
	 * @param records Lista svih učitanih studenata
	 * @return Mapa studenata razvrstana prema prolazu ili padu predmeta
	 */
	private static Map<Boolean, List<StudentRecord>> razvrstajProlazPad(List<StudentRecord> records) {
		return records.stream()
		.collect(
				Collectors.partitioningBy(sr->sr.getGrade()!=1)
		);
	}
	/**
	 * @param records Lista svih učitanih studenata
	 * @return Mapa koja svakoj ocjeni pridružuje broj studenata sa tom ocjenom
	 */
	private static Map<Integer, Integer> vratiBrojStudenataPoOcjenama(List<StudentRecord> records) {
		return records.stream()
		.collect(
				Collectors.toMap(
						sr->Integer.valueOf(sr.getGrade()),
						sr->Integer.valueOf(1), 
						(i1, i2)-> i1 + i2 )
		);
	}
	
	/**
	 * @param records Lista svih učitanih studenata
	 * @return Mapa koja svakoj ocjeni pridružuje listu svih studenata sa tom ocjenom
	 */
	private static Map<Integer, List<StudentRecord>> razvrstajStudentePoOcjenama(List<StudentRecord> records) {
		return records.stream()
		.collect(
				Collectors.groupingBy((record)->Integer.valueOf(record.getGrade()))
		);
	}
	
	/**
	 * @param records Lista svih učitanih studenata
	 * @return Lista JMBAG-ova svih studenata koji nisu položili predmet
	 */
	private static List<String> vratiPopisNepolozenih(List<StudentRecord> records) {
		return records.stream()
		.filter(record -> record.getGrade()==1)
		.map(StudentRecord::getJmbag)
		.sorted()
		.collect(Collectors.toList());
	}

	/**
	 * @param records Lista svih učitanih studenata
	 * @return Lista svih učenika koji su predmet prošli s ocjenom 5, 
	 * 			sortirana silazno prema zbroju bodova na predmetu.
	 */
	private static List<StudentRecord> vratiSortiranuListuOdlikasa(List<StudentRecord> records) {
		return records.stream()
		.filter(record -> record.getGrade()==5)
		.sorted((rec1, rec2) -> {
			return Double.compare(rec2.pointSum(), rec1.pointSum());
		})
		.collect(Collectors.toList());
	}
	
	/**
	 * @param records Lista svih učitanih studenata
	 * @return Lista svih učenika koji su predmet prošli s ocjenom 5
	 */
	private static List<StudentRecord> vratiListuOdlikasa(List<StudentRecord> records) {
		return records.stream()
		.filter(record -> record.getGrade()==5)
		.collect(Collectors.toList());
	}

	/**
	 * @param records Lista svih učitanih studenata
	 * @return Broj učenika koji su predmet položili s ocjenom 5
	 */
	private static long vratiBrojOdlikasa(List<StudentRecord> records) {
		return records.stream()
		.filter(record -> record.getGrade()==5)
		.count();
	}

	/**
	 * @param records Lista svih učitanih studenata
	 * @return Broj učenika koji imaju ukupno više od 25 ostvarenih
	 * 			bodova na predmetu.
	 */
	private static long vratiBodovaViseOd25(List<StudentRecord> records) {
		return records.stream().filter(record -> record.pointSum()>25)
		.count();
	}

	/**
	 * @param lines Lista stringova gdje svaki element predstavlja jedan
	 * 			tekstualni zapis studenta
	 * @return Lista StudentRecorda izgrađena na temelju predane liste
	 * 			stringova.
	 */
	private static List<StudentRecord> convert(List<String> lines) {
		List<StudentRecord> l = new ArrayList<>();
		lines.stream().forEach((line)->l.add(new StudentRecord(line.split(TAB))));
		return l;
	}
}
