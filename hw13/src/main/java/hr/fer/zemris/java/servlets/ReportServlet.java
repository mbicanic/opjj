package hr.fer.zemris.java.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartUtils;
import org.jfree.chart.JFreeChart;
import org.jfree.data.general.DefaultPieDataset;

/**
 * ReportServlet is a specification of a {@link HttpServlet} which
 * generates a graph in the form of a pie-chart representing results
 * of a survey about OS usage.
 * 
 * @author Miroslav Bićanić
 */
public class ReportServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		DefaultPieDataset dataset = new DefaultPieDataset();
		dataset.setValue("Linux", 20);
		dataset.setValue("Windows", 50);
		dataset.setValue("Mac", 30);
		JFreeChart chart = ChartFactory.createPieChart("OS usage", dataset);
		resp.setContentType("image/png");
		ChartUtils.writeChartAsPNG(resp.getOutputStream(), chart, 400, 300);
	}
}
