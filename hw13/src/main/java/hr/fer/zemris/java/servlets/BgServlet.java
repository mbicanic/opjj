package hr.fer.zemris.java.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * BgServlet is a specification of a {@link HttpServlet} which
 * changes the background color of all pages of this application.
 * 
 * @author Miroslav Bićanić
 */
public class BgServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		Object o = req.getParameter("clr");	//URL parameter
		if(o==null) {
			req.getSession().setAttribute("pickedBgCol", "FFFFFF");	//default session attribute 
		} else {
			req.getSession().setAttribute("pickedBgCol", o.toString()); //specified session attribute
		}
		resp.sendRedirect(resp.encodeRedirectURL(
				req.getContextPath()+"/index.jsp"));	
	}
	
}
