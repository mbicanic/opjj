package hr.fer.zemris.java.servlets;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * TrigServlet is a specification of a {@link HttpServlet} which
 * outputs a table of sine and cosine values for arguments in the
 * range given in the URL parameters.
 * 
 * Default range is [0,360]. If the range end is smaller than the
 * range start, they are swapped. If the range end is larger than
 * range start + 720 (degrees), the range end is set to start+720.
 * 
 * @author Miroslav Bićanić
 */
public class TrigServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		int start = 0;
		int end = 360;
		try {
			start = Integer.parseInt(req.getParameter("a"));
		} catch (NumberFormatException | NullPointerException ignore) {}
		try {
			end = Integer.parseInt(req.getParameter("b"));
		} catch (NumberFormatException | NullPointerException ignore) {}
		
		if(end<start) {
			int temp = end;
			end = start;
			start = temp;
		}
		if(end > start+720) {
			end = start+720;
		}
		Map<Integer, SinCosBean> values = new HashMap<>();
		for(int i = start; i<end; i++) {
			Double sin = Math.sin(i*Math.PI/180);
			Double cos = Math.cos(i*Math.PI/180);
			values.put(i, new SinCosBean(sin, cos));
		}
		
		req.setAttribute("values", values);
		req.setAttribute("a", start);
		req.setAttribute("b", end);
		req.getRequestDispatcher("/WEB-INF/pages/trigonometric.jsp").forward(req, resp);
	}
	
	/**
	 * Class encapsulating the sine and cosine value for a certain
	 * argument into one object, for easier accessing.
	 * 
	 * @author Miroslav Bićanić
	 */
	public static class SinCosBean {
		/** The string representation of the sine value */
		private String sin;
		/** The string representation of the cosine value */
		private String cos;
		
		/**
		 * Constructor for a SinCosBean
		 * @param sin the value of the sine
		 * @param cos the value of the cosine
		 */
		public SinCosBean(Double sin, Double cos) {
			this.sin=sin.toString();
			this.cos=cos.toString();
		}
		/**
		 * @return the string representation of the sine value
		 */
		public String getSin() {
			return sin;
		}
		/**
		 * @return the string representation of the cosine value
		 */
		public String getCos() {
			return cos;
		}
	}
}
