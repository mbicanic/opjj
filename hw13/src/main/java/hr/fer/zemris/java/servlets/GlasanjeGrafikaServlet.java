package hr.fer.zemris.java.servlets;

import java.io.IOException;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartUtils;
import org.jfree.chart.JFreeChart;
import org.jfree.data.general.DefaultPieDataset;

public class GlasanjeGrafikaServlet extends AbstractGlasanje {
	private static final long serialVersionUID = 1L;
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		@SuppressWarnings("unchecked")
		Map<Integer, String> bandNames = (Map<Integer,String>)req.getServletContext().getAttribute("bandNames");
		DefaultPieDataset dataset = createDataset(req, bandNames);
		JFreeChart jfc = ChartFactory.createPieChart("Band popularity", dataset);
		resp.setContentType("image/png");
		ChartUtils.writeChartAsPNG(resp.getOutputStream(), jfc, 400, 300);
	}

	/**
	 * Creates a {@link DefaultPieDataset} filled with the voting results
	 * for each band registered in the web application.
	 * 
	 * @param req the {@link HttpServletRequest} of this request
	 * @param bandNames a map pairing band IDs to the band names
	 * @return a {@link DefaultPieDataset} containing the voting results
	 * @throws IOException if reading from the results file fails
	 */
	private DefaultPieDataset createDataset(HttpServletRequest req, Map<Integer, String> bandNames) throws IOException {
		Map<Integer, Integer> results = getVotingResults(req);
		DefaultPieDataset dpd = new DefaultPieDataset();
		for(Map.Entry<Integer, Integer> e : results.entrySet()) {
			dpd.setValue(bandNames.get(e.getKey()), e.getValue());
		}
		return dpd;
	}
}
