package hr.fer.zemris.java.servlets;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;

/**
 * AbstractGlasanje is a specification of a {@link HttpServlet}
 * containing various utility methods needed by other Servlets
 * in the voting process.
 * 
 * @author Miroslav Bićanić
 */
public abstract class AbstractGlasanje extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	/**
	 * Creates and fills a map that pairs band IDs to the number of 
	 * votes they received.
	 * 
	 * @param req the {@link HttpServletRequest} of this request
	 * @param filename the name of the file storing the results
	 * @return the map pairing band IDs to the received number of votes
	 * @throws IOException if reading from file fails
	 */
	protected Map<Integer, Integer> getVotingResults(HttpServletRequest req) throws IOException {
		String filename = req.getServletContext().getRealPath("/WEB-INF/pages/glasanje-rezultati.txt");
		Path p = Paths.get(filename);
		if(!Files.exists(p)) {
			initResultFile(req);
		}
		List<String> lines = Files.readAllLines(p);
		Map<Integer, Integer> results = new HashMap<>();
		for(String s : lines) {
			String[] split = s.split("\t");
			results.put(Integer.parseInt(split[0]), Integer.parseInt(split[1]));
		}
		return results;
	}
	
	/**
	 * If the results file is missing, this method creates it setting
	 * the number of votes for all bands to 0.
	 * 
	 * @param req the {@link HttpServletRequest} of this request
	 * @param p the path of the file to initialize
	 * @throws IOException if writing to file was unsuccessful
	 */
	protected void initResultFile(HttpServletRequest req) throws IOException {
		@SuppressWarnings("unchecked")
		Map<Integer, String> bands = (Map<Integer, String>)req.getServletContext().getAttribute("bandNames");
		Map<Integer, Integer> results = new LinkedHashMap<>();
		for(Integer i : bands.keySet()) {
			results.put(i, 0);
		}
		writeVotingResults(req, results);
	}
	
	/**
	 * Writes the content of the given {@code results} map into the voting
	 * results file.
	 * 
	 * @param req the {@link HttpServletRequest} that is being processed
	 * @param results the map pairing band IDs to their number of votes
	 * @throws IOException if writing to file was unsuccessful
	 */
	protected void writeVotingResults(HttpServletRequest req, Map<Integer, Integer> results) throws IOException {
		synchronized(HttpServlet.class) {
			String filename = req.getServletContext().getRealPath("/WEB-INF/pages/glasanje-rezultati.txt");
			StringBuilder sb = new StringBuilder(200);
			for(Map.Entry<Integer, Integer> e : results.entrySet()) {
				sb.append(e.getKey()).append("\t").append(e.getValue()).append("\n");
			}
			Files.writeString(Paths.get(filename), sb.toString(), StandardCharsets.UTF_8);
		}
	}
	
	/**
	 * Sorts the received map by values in descending order.
	 * 
	 * @param results the map to sort
	 * @return the same map with entries sorted by values
	 */
	protected Map<Integer, Integer> sortByValue(Map<Integer, Integer> results) {
		return results.entrySet()
				.stream()
				.sorted(Collections.reverseOrder(Map.Entry.comparingByValue()))
				.collect(Collectors.toMap(
							Map.Entry::getKey, 
							Map.Entry::getValue, 
							(e1,e2)->e2, 
							LinkedHashMap::new)
						);
	}
}
