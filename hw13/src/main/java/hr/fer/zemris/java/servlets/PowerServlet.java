package hr.fer.zemris.java.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

/**
 * PowerServlet is a specification of a {@link HttpServlet} which
 * generates an Excel table (.xls) with {@code n} pages, where each 
 * page contains two columns.
 * 
 * The first column contains all numbers from {@code a} to {@code b}.
 * The second column contains the {@code i}-th power of a number in
 * the first column, where i is the page number.
 * 
 * Parameters a, b and n are given as URL parameters, and the following
 * rules apply to them:
 * 	-'a' must be an integer between -100 and 100, inclusively
 * 	-'b' must be an integer between -100 and 100, inclusively
 * 	-'n' must be an integer between 1 and 5, inclusively
 * @author Miroslav Bićanić
 *
 */
public class PowerServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		Object aObj = req.getParameter("a");
		Object bObj = req.getParameter("b");
		Object nObj = req.getParameter("n");
		Integer a = getCheckInt(aObj, -100, 100);
		if(a==null) {
			redirectInvalid(aObj, req, resp);
			return;
		}
		Integer b = getCheckInt(bObj, -100, 100);
		if(b==null) {
			redirectInvalid(bObj, req, resp);
			return;
		}
		Integer n = getCheckInt(nObj, 1, 5);
		if(n==null) {
			redirectInvalid(nObj, req, resp);
			return;
		}
		resp.setContentType("application/vnd.ms-excel");
		resp.setHeader("Content-Disposition", "attachment; filename=\"table.xls\"");
		
		HSSFWorkbook hwb = createExcel(a, b, n);
		hwb.write(resp.getOutputStream());
	}
	
	/**
	 * Checks if the given object is interpretable as an Integer, and
	 * if the Integer is in the legal range, as specified by the
	 * {@code min} and {@code max} arguments.
	 * 
	 * If the object isn't interpretable as an integer, or it is out
	 * of the legal range, this method returns {@code null}.
	 * 
	 * @param o the object to check
	 * @param min the minimum value of the number
	 * @param max the maximum value of the number
	 * @return the parsed number if possible; null otherwise
	 */
	private Integer getCheckInt(Object o, int min, int max) {
		if(o==null) {
			return null;
		}
		Integer val=null;
		try {
			val = Integer.parseInt(o.toString());
		} catch(NumberFormatException ex) {
			return null;
		}
		if(val<min || val>max) {
			return null;
		}
		return val;
	}

	/**
	 * Creates and fills a {@link HSSFWorkbook} with {@code n} pages, 
	 * each of them filled as described by {@link PowerServlet}.
	 * 
	 * @param a the lower boundary of the range
	 * @param b the upper boundary of the range
	 * @param n the number of pages to create in the document
	 * @return a {@link HSSFWorkbook} containing the data
	 */
	private HSSFWorkbook createExcel(Integer a, Integer b, Integer n) {
		HSSFWorkbook hwb = new HSSFWorkbook();
		for(int page=0; page<n; page++) {
			HSSFSheet sheet = hwb.createSheet("Page "+(page+1));
			HSSFRow header = sheet.createRow(0);
			header.createCell(0).setCellValue("a");
			header.createCell(1).setCellValue("a^"+Integer.valueOf(page+1).toString());
			for(Integer i=a; i<=b; i++) {
				HSSFRow row = sheet.createRow(i-a);
				row.createCell(0).setCellValue(i.toString());
				Long pow = Math.round(Math.pow(i, page+1));
				row.createCell(1).setCellValue(pow.toString());
			}
		}
		return hwb;
	}

	/**
	 * Redirects the request processing to an error page which displays
	 * an appropriate message describing the cause of the error.
	 * 
	 * @param o the given object that was the illegal argument
	 * @param req the {@link HttpServletRequest} of this request
	 * @param resp the {@link HttpServletResponse} of this request
	 * @throws ServletException
	 * @throws IOException
	 */
	private void redirectInvalid(Object o, HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String error = o==null ? "<null>" : o.toString();
		req.getRequestDispatcher("/WEB-INF/pages/invalid.jsp?name=Power&err="+error)
			.forward(req, resp);
	}
}
