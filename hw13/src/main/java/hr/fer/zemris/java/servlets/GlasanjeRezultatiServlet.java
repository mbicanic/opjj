package hr.fer.zemris.java.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * GlasanjeRezultatiServlet is a specification of a {@link HttpServlet}
 * which reads the voting results from a file and stores two maps
 * representing the results as the attributes of the {@link HttpServletRequest}.
 * 
 * @author Miroslav Bićanić
 */
public class GlasanjeRezultatiServlet extends AbstractGlasanje {
	private static final long serialVersionUID = 1L;
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		Map<Integer, Integer> sortedResults = sortByValue(getVotingResults(req));
		List<Integer> best = getBest(sortedResults);
		
		req.setAttribute("results", sortedResults);
		req.setAttribute("best", best);
		req.getRequestDispatcher("/WEB-INF/pages/glasanjeRez.jsp").forward(req, resp);
	}

	/**
	 * Returns a map containing only the bands which have the largest number
	 * of votes.
	 * 
	 * @param results the map of all bands paired with the number of votes they have
	 * @return a list of IDs which are paired to the largest number of votes
	 */
	private List<Integer> getBest(Map<Integer, Integer> results) {
		Integer largest = 0;
		for(Integer key : results.keySet()) {
			if(results.get(key)>largest) {
				largest = results.get(key);
			}
		}
		List<Integer> best = new ArrayList<>();
		for(Map.Entry<Integer,Integer> e : results.entrySet()) {
			if(e.getValue().equals(largest)) {
				best.add(e.getKey());
			}
		}
		return best;
	}
}
