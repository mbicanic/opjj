package hr.fer.zemris.java.servlets;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * GlasanjeServlet is a specification of a {@link HttpServlet} which
 * sets up the parameters of the request as specified in the voting
 * definition file of this application, after which the processing
 * is dispatched to a JSP.
 * 
 * @author Miroslav Bićanić
 */
public class GlasanjeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String filename = req.getServletContext().getRealPath("/WEB-INF/glasanje-definicija.txt");
		List<String> lines = Files.readAllLines(Paths.get(filename));
		Map<Integer, String> bands = new TreeMap<>();
		Map<Integer, String> links = new TreeMap<>();
		
		for(String s : lines) {
			String[] split = s.split("\t");
			bands.put(Integer.parseInt(split[0]), split[1]);
			links.put(Integer.parseInt(split[0]), split[2]);
		}
		
		req.getServletContext().setAttribute("bandNames", bands);
		req.getServletContext().setAttribute("bandLinks", links);
		req.getRequestDispatcher("/WEB-INF/pages/glasanjeIndex.jsp").forward(req, resp);
	}
}
