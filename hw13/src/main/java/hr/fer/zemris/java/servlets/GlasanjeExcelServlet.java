package hr.fer.zemris.java.servlets;

import java.io.IOException;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

/**
 * GlasanjeExcelServlet is a specification of a {@link HttpServlet}
 * which creates an excel table (.xls) filled with the results of
 * voting for bands.
 * 
 * @author Miroslav Bićanić
 */
public class GlasanjeExcelServlet extends AbstractGlasanje {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		@SuppressWarnings("unchecked")
		Map<Integer, String> bandNames = (Map<Integer, String>)req.getServletContext().getAttribute("bandNames");
		Map<Integer, Integer> results = getVotingResults(req);
		HSSFWorkbook hwb = createWorkbook(bandNames, results);
		
		resp.setContentType("application/vnd.ms-excel");
		resp.setHeader("Content-Disposition", "attachment; filename=\"results.xls\"");
		hwb.write(resp.getOutputStream());
	}
	
	/**
	 * Creates a {@link HSSFWorkbook} filled with the results of the voting.
	 * 
	 * @param bandNames a map pairing band IDs to band names
	 * @param results a map pairing band IDs to the number of received votes
	 * @return a {@link HSSFWorkbook} representing the table containing the results
	 */
	private HSSFWorkbook createWorkbook(Map<Integer, String> bandNames, Map<Integer, Integer> results) {
		HSSFWorkbook hwb = new HSSFWorkbook();
		HSSFSheet sheet = hwb.createSheet("Results");
		HSSFRow header = sheet.createRow(0);
		header.createCell(0).setCellValue("ID");
		header.createCell(1).setCellValue("Band name");
		header.createCell(2).setCellValue("Votes");
		
		int rowNumber = 1;
		for(Map.Entry<Integer, String> e : bandNames.entrySet()) {
			HSSFRow row = sheet.createRow(rowNumber);
			row.createCell(0).setCellValue(e.getKey());
			row.createCell(1).setCellValue(e.getValue());
			row.createCell(2).setCellValue(results.get(e.getKey()));
			rowNumber++;
		}
		return hwb;
	}
}
