package hr.fer.zemris.java.servlets;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

/**
 * RuntimeListener is a specification of a {@link ServletContextListener}
 * which counts how long has the web application been running.
 * 
 * @author Miroslav Bićanić
 */
@WebListener
public class RuntimeListener implements ServletContextListener {

	@Override
	public void contextInitialized(ServletContextEvent sce) {
		ServletContext sc = sce.getServletContext();
		sc.setAttribute("elapsedTime", (Long)System.currentTimeMillis());
	}

	@Override
	public void contextDestroyed(ServletContextEvent sce) {
		ServletContext sc = sce.getServletContext();
		sc.removeAttribute("elapsedTime");
	}
}
