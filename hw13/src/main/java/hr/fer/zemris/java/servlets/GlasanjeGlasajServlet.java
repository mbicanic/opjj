package hr.fer.zemris.java.servlets;

import java.io.IOException;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * GlasanjeGlasajServlet is a specification of a {@link HttpServlet}
 * which processes a user's vote and stores it in the file containing
 * the results.
 * 
 * @author Miroslav Bićanić
 */
public class GlasanjeGlasajServlet extends AbstractGlasanje {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		Map<Integer, Integer> results = getVotingResults(req);
		Integer votedID = Integer.parseInt(req.getParameter("id").toString());
		if(results.containsKey(votedID)) {
			Integer old = results.get(votedID);
			results.put(votedID, old+1);
		}
		writeVotingResults(req, results);
		
		resp.sendRedirect(resp.encodeRedirectURL(
				req.getContextPath()+"/glasanje-rezultati"));
	}
}
