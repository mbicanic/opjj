<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" session="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Choose color</title>
	<style>
			body {background-color: #<c:out value="${empty sessionScope.pickedBgCol ? 'FFFFFF' : sessionScope.pickedBgCol}" />;}
	</style>
</head>
<body>
	<a href="${pageContext.request.contextPath}/setcolor?clr=FFFFFF">WHITE</a>
	<a href="${pageContext.request.contextPath}/setcolor?clr=FF0000">RED</a>
	<a href="${pageContext.request.contextPath}/setcolor?clr=00FF00">GREEN</a>
	<a href="${pageContext.request.contextPath}/setcolor?clr=00FFFF">CYAN</a>
</body>
</html>