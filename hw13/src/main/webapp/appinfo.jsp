<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" session="true" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
    
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>App info</title>
	<style>
			body {background-color: #<c:out value="${empty sessionScope.pickedBgCol ? 'FFFFFF' : sessionScope.pickedBgCol}" />;}
	</style>
</head>
<body>
	<p> The application has been running for:
	<%
	long start = (Long)request.getServletContext().getAttribute("elapsedTime");
	long dif = System.currentTimeMillis()-start;
	int milli = (int)dif%1000;
	int sec = (int)dif/1000;
	int min = sec/60;
	sec = sec%60;
	int hrs = min/60;
	min = min%60;
	int day = hrs/24;
	hrs = hrs%24;
	String ms = day+" days "+hrs+"h "+min+"m "+sec+"s "+milli+"ms";
	out.print(ms);
	%> </p>
</body>
</html>