<%@ page import="java.util.Random,java.awt.Color" language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" session="true" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>A funny story</title>
	<style>
			body {background-color: #<c:out value="${empty sessionScope.pickedBgCol ? 'FFFFFF' : sessionScope.pickedBgCol}" />;}

			<% Random r = new Random(); %>
			p {
				color: rgb(
					<%= Math.abs(r.nextInt())%256 %>,
					<%= Math.abs(r.nextInt())%256 %>, 
					<%= Math.abs(r.nextInt())%256 %>
				) 
			}	
	</style>
</head>
<body>
<p> 
	In a court in Killarney, deep in Munster, Ireland, this conversation is reported to have taken place:<br>
	<b>Lawyer:</b> 'At the scene of the accident, Mr O'Shea, did you tell the Garda officer that you had never felt better in your life?'<br>
	<b>O'Shea the farmer:</b> 'That's right, sir.'<br>
	<b>Lawyer:</b> 'Well then, Mr O'Shea, how is it that you are now claiming you were seriously injured when my client's car hit your cart?'<br>
	<b>O'Shea the farmer:</b> 'When the Garda arrived, he went over to my horse, who had a broken leg, and shot him. <br>
	Then he went over to Darcy, my dog, who was badly hurt, and shot him. <br>
	Then the policeman came across the road, gun still in hand, looked at me, and said, 'How are you feeling?' <br><br><i>
	I just thought under the circumstances, it was a wise choice of words to say: 'I've never felt better in my life.'  </i>
</p>
</body>
</html>