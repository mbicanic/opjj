<%@page import="java.util.Map"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" session="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Rezultati</title>
	<style>
			body {background-color: #<c:out value="${empty sessionScope.pickedBgCol ? 'FFFFFF' : sessionScope.pickedBgCol}" />;}
	</style>
</head>
<body>
	<h1> Rezultati glasanja </h1>
	<table border="1">
	<tr><td>Bend</td><td>Broj glasova</td></tr>
	<c:forEach var="e" items="${requestScope.results}">
		<tr><td> ${applicationScope.bandNames[e.key]} </td><td> ${e.value} </td></tr>
	</c:forEach>
	</table>
	
	<h2> Grafički prikaz rezultata </h2>
	<img alt="Pie-chart" src="${pageContext.request.contextPath}/glasanje-grafika" width="400" height="300">
	
	<h2> Rezultati u XLS formatu </h2>
	<p> Rezultati u XLS formatu dostupni su <a href="${pageContext.request.contextPath}/glasanje-xls">ovdje</a></p>
	
	<h2> Razno </h2>
	<p> Primjeri pjesama pobjedničkih bendova:</p>
	<ul>
		<c:forEach var="e" items="${requestScope.best}">
			<li> <a href="${applicationScope.bandLinks[e]}"> ${applicationScope.bandNames[e]} </a> </li>
		</c:forEach>
	</ul>
	
</body>
</html>