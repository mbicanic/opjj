<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" session="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Invalid parameter</title>
	<style>
			body {background-color: #<c:out value="${empty sessionScope.pickedBgCol ? 'FFFFFF' : sessionScope.pickedBgCol}" />;}
	</style>
</head>
<body>
	<p> Invalid parameter given to action ${param.name}: ${param.err} </p>
	<a href="${pageContext.request.contextPath}/index.jsp"> Home </a>
</body>
</html>