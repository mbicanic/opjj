<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" session="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Glasanje</title>
	<style>
			body {background-color: #<c:out value="${empty sessionScope.pickedBgCol ? 'FFFFFF' : sessionScope.pickedBgCol}" />;}
	</style>
</head>
<body>
	<h1> Glasanje za omiljeni bend! </h1>
	<p> Od sljedećih bendova, koji Vam je bend najdraži? Kliknite na link kako biste glasali! </p>
	<ol>
	<c:forEach var="e" items="${applicationScope.bandNames}">
		<li> <a href="${pageContext.request.contextPath}/glasanje-glasaj?id=${e.key}"> ${e.value}</a></li>
	</c:forEach>
	</ol>
</body>
</html>