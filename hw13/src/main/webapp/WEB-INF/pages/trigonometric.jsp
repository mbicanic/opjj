<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Trigonometry</title>
	<style>
			body {background-color: #<c:out value="${empty sessionScope.pickedBgCol ? 'FFFFFF' : sessionScope.pickedBgCol}" />;}
	</style>
</head>
<body>
	<h2> Values of sine and cosine functions for all angles between ${requestScope.a} and ${requestScope.b} degrees</h2>
	<table border="1">
	<tr><td>x</td><td>sin(x)</td><td>cos(x)</td></tr>
	<c:forEach var="e" items="${requestScope.values}">
		<tr><td> ${e.key} </td><td> ${e.value.sin}</td><td>${e.value.cos}</td></tr>
	</c:forEach>
	</table>
</body>
</html>