<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" session="true" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
    
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Homepage</title>
	<style>
			body {background-color: #<c:out value="${empty sessionScope.pickedBgCol ? 'FFFFFF' : sessionScope.pickedBgCol}" />;}
	</style>
</head>
<body>
	<a href="${pageContext.request.contextPath}/colors.jsp">Background color chooser</a>
	<hr>
	<a href="${pageContext.request.contextPath}/trigonometric?a=0&b=90">Get default trigonometric</a>
	<form action="trigonometric" method="GET">  
		Početni kut:<br>
		<input type="number" name="a" min="0" max="360" step="1" value="0"><br>  
		Završni kut:<br>
		<input type="number" name="b" min="0" max="360" step="1" value="360"><br>  
		<input type="submit" value="Tabeliraj">
		<input type="reset" value="Reset">
	</form>
	<hr>
	<a href="${pageContext.request.contextPath}/stories/funny.jsp"> Funny story </a>
	<br>
	<a href="${pageContext.request.contextPath}/report.jsp"> OS report </a> 
	<br>
	<a href="${pageContext.request.contextPath}/powers?a=1&b=100&n=3"> Powers </a> 
	<br>
	<a href="${pageContext.request.contextPath}/appinfo.jsp"> Application Info </a>
	<hr>
	<a href="${pageContext.request.contextPath}/glasanje"> Voting </a>
</body>
</html>