package hr.fer.zemris.java.hw06.shell;

/**
 * ShellIOException is thrown whenever MyShell encounters an IO
 * error. An occurrence of this exception causes the program
 * to terminate.
 * 
 * @author Miroslav Bićanić
 */
public class ShellIOException extends RuntimeException {
	private static final long serialVersionUID = 1L;
	
	/**
	 * A default constructor for a ShellIOException
	 */
	public ShellIOException() {
		super();
	}
	/**
	 * A constructor for a ShellIOException taking a message describing
	 * the error that caused the exception
	 * @param msg The description of the cause of the exception
	 */
	public ShellIOException(String msg) {
		super(msg);
	}

}
