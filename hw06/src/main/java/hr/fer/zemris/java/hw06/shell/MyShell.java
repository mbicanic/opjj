package hr.fer.zemris.java.hw06.shell;

import java.util.Collections;
import java.util.Scanner;
import java.util.SortedMap;
import java.util.TreeMap;

import hr.fer.zemris.java.hw06.shell.commands.*;

/**
 * MyShell is a simple command-line shell supporting some basic 
 * operations in the file system.
 * 
 * MyShell operates by receiving commands and the arguments required 
 * by those commands, doing so by prompting the user for input. 
 * Those commands return a {@link ShellStatus}, based on which MyShell 
 * either continues accepting commands, or terminates execution.
 * 
 * For every new prompt, MyShell outputs its Environment's prompt symbol. 
 * If MyShell reads a line from the Environment and the line ends 
 * with the environment's symbol for requesting multiple lines, 
 * it reads the next line as a part of the previous line, doing so 
 * until a line without that symbol at the end is reached.
 * For every new line that is interpreted as a part of its previous
 * line, MyShell's environment's multiline symbol is outputted.
 * 
 * When entering arguments for commands you can enclose an argument
 * in quotes to make sure it is parsed as one argument. This is used
 * when the argument is a path to a file, and that path contains a
 * space. Refer to the following example:
 * {@code cat C:\Documents and Settings\file.txt} 
 * 		-- failed operation, command cat received 3 arguments:
 * 			"C:\Documents", "and" and "Settings\file.txt"
 * {@code cat "C:\Documents and Settings\file.txt"}
 * 		-- successful operation, outputs the content of file.txt
 * 			to standard out.
 * Please note that when an argument is enclosed in quotes, escaping
 * mechanisms are activated, supporting two escape sequences:
 * 	'\\' will be read as a single '\'
 * 	'\"' will be read as a single '"'
 * Because of this, giving a path argument like:
 * 	{@code "C:\Documents\Folder1\"} 
 * will result in an error, since the last quotation mark isn't 
 * interpreted as a closing quote for the argument, but as a part
 * of the string itself.
 * 
 * For a list of supported commands please run MyShell and type 'help'.
 * For details on their usage and syntax type 'help [command name]'.
 * The provided command name must be written equally as it is written by
 * the 'help' command.
 * 
 * @author Miroslav Bićanić
 */
public class MyShell {
	/**
	 * The welcome message printed to the console once on startup
	 */
	private static final String MSG_WELCOME = "Welcome to MyShell v 1.0";
	
	/**
	 * An implementation of an {@link Environment} that takes 
	 * input from a {@link Scanner} (note that a Scanner can be 
	 * opened over any source), and outputs it to standard out.
	 * 
	 * The environment defines default values for the  symbolic 
	 * characters:
	 * 	1) prompt:					'>'
	 * 	2) request morelines:		'\'
	 * 	3) multiline command:		'|'
	 * 
	 * @author Miroslav Bićanić
	 */
	private static class MyEnvironment implements Environment {
		/**
		 * The currently used symbol for a prompt
		 */
		private Character prompt = '>';
		/**
		 * The currently used symbol for a request for multiline
		 * spanning.
		 */
		private Character morelines = '\\';
		/**
		 * The currently used symbol indicator for a line being
		 * part of a multiline command.
		 */
		private Character multiline = '|';
		/**
		 * A Scanner from which to read input
		 */
		private Scanner sc;
		/**
		 * Internal map of commands, mapping the commands' names
		 * to their actual {@link ShellCommand} executable
		 * implementations.
		 */
		private SortedMap<String, ShellCommand> commands;
		
		/**
		 * A constructor for an EnvImpl
		 * @param commands A map mapping command names to their implementations
		 * @param sc A Scanner from which to read input
		 */
		private MyEnvironment(SortedMap<String,ShellCommand> commands, Scanner sc) {
			this.commands = commands;
			this.sc = sc;
		}
		
		/**
		 * {@inheritDoc}
		 * The source for this Environment is defined by the source
		 * of its Scanner.
		 */
		@Override
		public String readLine() throws ShellIOException {
			try {
				return sc.nextLine();
			} catch (Exception ex) {
				throw new ShellIOException("Error occurred while reading line!");
			}
		}
		/**
		 * {@inheritDoc}
		 * The destination of this Environment is standard out.
		 */
		@Override
		public void write(String text) throws ShellIOException {
			System.out.printf(text);
		}
		/**
		 * {@inheritDoc}
		 * The destination of this Environment is standard out.
		 */
		@Override
		public void writeln(String text) throws ShellIOException {
			System.out.println(text);			
		}
		/**
		 * {@inheritDoc}
		 * The map returned by this Environment is unmodifiable
		 * (read only)
		 * @return A read-only map of the registered commands
		 */
		@Override
		public SortedMap<String, ShellCommand> commands() {
			return Collections.unmodifiableSortedMap(this.commands);
		}
		/**
		 * {@inheritDoc}
		 */
		@Override
		public Character getMultilineSymbol() {
			return multiline;
		}
		/**
		 * {@inheritDoc}
		 */
		@Override
		public void setMultilineSymbol(Character symbol) {
			this.multiline = symbol;	
		}
		/**
		 * {@inheritDoc}
		 */
		@Override
		public Character getPromptSymbol() {
			return prompt;
		}
		/**
		 * {@inheritDoc}
		 */
		@Override
		public void setPromptSymbol(Character symbol) {
			this.prompt = symbol;
		}
		/**
		 * {@inheritDoc}
		 */
		@Override
		public Character getMorelinesSymbol() {
			return morelines;
		}
		/**
		 * {@inheritDoc}
		 */
		@Override
		public void setMorelinesSymbol(Character symbol) {
			this.morelines = symbol;
		}
	}
	
	/**
	 * The entry point for the MyShell program, configuring
	 * it and running it.
	 * 
	 * @param args not used by this program
	 */
	public static void main(String[] args) {
		System.out.println(MSG_WELCOME);
		SortedMap<String,ShellCommand> commands = setUpCommands();
		Scanner sc = new Scanner(System.in);
		try {
			execute(new MyEnvironment(commands, sc));	
		} catch (ShellIOException ex) {
			System.out.println("An IO exception has occurred! Terminating...");
		}
		sc.close();
	}
	
	/**
	 * The execution method for MyShell, handling user input
	 * and delegating execution to the user-specified methods.
	 * @param env The Environment in which all execution is done
	 */
	private static void execute(Environment env) {
		ShellStatus status = ShellStatus.CONTINUE;
		do {
			String statement = getFullStatement(env);
			String cmd = extractCommand(statement.toCharArray());
			if(cmd.length()==0) {
				continue;
				//this is done so that the next if block doesn't
				//output a senseless message ("Invalid command entered: ''")
				//The behaviour of Windows' CMD is the same
			}
			String arguments = statement.substring(cmd.length()).trim();
			ShellCommand command = env.commands().get(cmd);
			if(command==null) {
				env.writeln("Invalid command entered: '"+cmd+"'.");
				continue;
			}
			status = command.executeCommand(env, arguments);
		} while (status!=ShellStatus.TERMINATE);
	}
	/**
	 * Extracts the command name from the user-provided
	 * statement, interpreting the first sequence of non-
	 * whitespace characters as the command.
	 * 
	 * @param statement An array of chars representing the user's input
	 * @return The command name that the user specified
	 */
	private static String extractCommand(char[] statement) {
		StringBuilder sb = new StringBuilder(20);
		for(char c : statement) {
			if(Character.isWhitespace(c) || c=='"') {
				break;
			}
			sb.append(c);
		}
		return sb.toString().toLowerCase();
	}

	/**
	 * Reads user input through the environment and concatenates
	 * multiple lines as long as the symbol for a multiline 
	 * request is the last symbol of each line, removing that
	 * character before concatenating.
	 * 
	 * @param env The environment from which to read input
	 * @return The whole command statement
	 */
	private static String getFullStatement(Environment env) {
		StringBuilder sb = new StringBuilder(100);
		env.write(env.getPromptSymbol()+" ");
		while(true) {
			String line = env.readLine();
			if(line.length()==0) {
				break;
			}
			if(line.charAt(line.length()-1)==env.getMorelinesSymbol()) {
				sb.append(line.substring(0,line.length()-1));
				env.write(env.getMultilineSymbol()+" ");
				continue;
			} else {
				sb.append(line);
				break;
			}
		}
		return sb.toString().trim();
	}

	/**
	 * Initializes the map of supported commands by MyShell,
	 * with the command name being the key, and a ShellCommand
	 * implementation of the command.
	 * 
	 * @return A map of command supported by MyShell
	 */
	private static SortedMap<String, ShellCommand> setUpCommands() {
		SortedMap<String, ShellCommand> commands = new TreeMap<String, ShellCommand>();
		commands.put("cat", new CatShellCommand());
		commands.put("charsets", new CharsetsShellCommand());
		commands.put( "copy", new CopyShellCommand());
		commands.put("hexdump", new HexDumpShellCommand());
		commands.put("ls", new LsShellCommand());
		commands.put("mkdir", new MkDirShellCommand());
		commands.put("tree", new TreeShellCommand());
		commands.put("help", new HelpShellCommand());
		commands.put("exit", new ExitShellCommand());
		commands.put("symbol", new SymbolShellCommand());
		return commands;
		//note: command keys purposely left not using static constants because
		//		they're used only here, and in this case it is even more clear
	}
	
}
