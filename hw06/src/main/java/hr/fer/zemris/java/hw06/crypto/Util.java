package hr.fer.zemris.java.hw06.crypto;

import java.util.Arrays;
import java.util.List;

/**
 * Utility class providing with utility methods for 
 * converting bytes to hexadecimal numbers and vice-
 * versa, used by the Crypto class.
 * 
 * @author Miroslav Bićanić
 */
public class Util {
	
	/**
	 * Helper list storing all hexadecimal digits, with their index being
	 * their decimal value.
	 */
	private static List<Character> hexDigits = Arrays.asList('0', '1', '2', '3', '4', '5', '6', '7',
			'8', '9', 'a', 'b', 'c', 'd', 'e', 'f');

	/**
	 * Converts an array of bytes into hexadecimal code, with each
	 * byte corresponding to two hex digits.
	 * The computed hex-code is returned as a string.
	 * 
	 * @param bytearray The array of bytes to convert to hexadecimal
	 * @return A string representation of the byte array reduced to the
	 * 			corresponding hex digits.
	 */
	public static String bytetohex(byte[] bytearray) {
		if(bytearray==null || bytearray.length==0) {
			return new String();
		}
		StringBuilder sb = new StringBuilder(bytearray.length*2);
		for(byte b : bytearray) {
			int left = (b&0xF0)>>4;
			int right = (b&0x0F);
			sb.append(hexDigits.get(left)).append(hexDigits.get(right));
		}
		return sb.toString();
	}
	
	/**
	 * Takes a string of hexadecimal digits and converts it into
	 * a corresponding byte array, converting each two digits into 
	 * one byte.
	 * 
	 * @param keyText The string representation of the hexadecimal digits
	 * @return An array of bytes, corresponding to the given digits
	 */
	public static byte[] hextobyte(String keyText) {
		if(keyText==null || keyText.length()==0) {
			return new byte[] {};
		}
		int n = keyText.length();
		keyText = keyText.toLowerCase();
		if(n%2!=0) {
			throw new IllegalArgumentException("Given hex string must have even length.");
		}
		byte[] arr = new byte[n/2];
		for(int i=0; i<=n-2; i+=2) {
			arr[i/2] = coupleToByte(keyText.substring(i,i+2));
		}
		return arr;
	}
	
	/**
	 * Converts a string of two hexadecimal digits into
	 * one byte.
	 * 
	 * @param hex A string of two hex digits
	 * @return A byte corresponding to the given digits
	 */
	private static byte coupleToByte(String hex) {
		int left = hexDigits.indexOf(hex.charAt(0));
		int right = hexDigits.indexOf(hex.charAt(1));
		byte result = (byte) ((left<<4) | right);
		return result;
	}
}
