package hr.fer.zemris.java.hw06.shell.commands;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;

import static hr.fer.zemris.java.hw06.shell.commands.Utility.*;
import hr.fer.zemris.java.hw06.shell.Environment;
import hr.fer.zemris.java.hw06.shell.ShellCommand;
import hr.fer.zemris.java.hw06.shell.ShellStatus;

/**
 * MkDirShellCommand is a command that creates one or multiple
 * directories in the file system.
 * 
 * To correctly execute, it needs exactly one argument - a path
 * to a non-existant directory that will be created by this
 * command.
 * 
 * Giving it any other number of arguments or an illegal argument
 * (such as an existing folder) stops the execution of the command
 * and returns the control back to the Shell that called it.
 * 
 * Correct example showing the syntax:
 * 	{@code mkdir "C:\Existing Folder\New Folder Within It"}
 * 
 * @author Miroslav Bićanić
 */
public class MkDirShellCommand implements ShellCommand {
	
	/**
	 * {@inheritDoc}
	 * @param arguments One valid path to a non-existant directory
	 * @return Always returns ShellStatus.CONTINUE
	 */
	@Override
	public ShellStatus executeCommand(Environment env, String arguments) {
		try {
			String pathName = getCheckArgCount(arguments, MKDIR_CMD, 1)[0];
			Path p = Paths.get(pathName);
			if(Files.exists(p)) {
				env.writeln("Given directory already exists!");
				return ShellStatus.CONTINUE;
			}
			Files.createDirectories(p);
		} catch (IllegalArgumentException ex) {
			env.writeln(ex.getMessage());
		} catch (IOException ex) {
			env.writeln(IO_ERROR);
		}
		return ShellStatus.CONTINUE;
	}
	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getCommandName() {
		return MKDIR_CMD;
	}
	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<String> getCommandDescription() {
		return Arrays.asList(
				"Creates the directory structure as specified by the path given",
				"as the argument.",
				"This command takes exactly one argument - the path to the",
				"directory to create. If multiple directories in the given",
				"path do not exist, all will be created.",
				"",
				"Syntax:",
				"> mkdir [PATH_TO_NEW_DIRECTORY]",
				""
		);
	}
}
