package hr.fer.zemris.java.hw06.shell.commands;

import java.util.Arrays;
import java.util.List;

import hr.fer.zemris.java.hw06.shell.Environment;
import hr.fer.zemris.java.hw06.shell.ShellCommand;
import hr.fer.zemris.java.hw06.shell.ShellStatus;
import static hr.fer.zemris.java.hw06.shell.commands.Utility.*;

/**
 * SymbolShellCommand is a command enabling the user to edit
 * the symbolic characters of the shell.
 * 
 * To correctly execute it needs at least one argument 
 * representing one of the features that have symbolic characters.
 * In this case, the currently used symbolic character for
 * that feature is outputted.
 * 
 * Additionally, a second argument can be given after the first one,
 * representing a new character for the specified feature.
 * If the new value for a feature is already used by another feature,
 * it will not be updated.
 * 
 * Giving it any other number of arguments or invalid arguments
 * stops the execution of the command and returns the control back
 * to the shell that called it.
 * 
 * Correct examples showing the syntax:
 * 	{@code symbol PROMPT}
 * 		-- outputs the current PROMPT symbolic character
 * 	{@code symbol MORELINES !}
 * 		-- changes the symbolic character value of MORELINES
 * 			to a '!'
 * 
 * @author Miroslav Bićanić
 */
public class SymbolShellCommand implements ShellCommand {
	/**
	 * {@inheritDoc}
	 * @param arguments The name of the feature whose symbol to display or update
	 * 					if followed by a second argument that is a character.
	 * @return Always returns ShellStatus.CONTINUE
	 */
	@Override
	public ShellStatus executeCommand(Environment env, String arguments) {
		String[] args = getArgsPermissive(arguments);
		if(args.length!=1 && args.length!=2) {
			env.writeln("Invalid number of arguments given to command 'symbol'. Expected 1 or 2.");
			return ShellStatus.CONTINUE;
		}
		if(args.length==1) {
			fetchSymbol(args[0], env);
		} else if (args.length==2) {
			updateSymbol(args[0], args[1], env);
		} 
		return ShellStatus.CONTINUE;
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getCommandName() {
		return SYM_CMD;
	}
	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<String> getCommandDescription() {
		return Arrays.asList(
				"This command retrieves MyShell's special characters",
				"or updates them to a new value, depending on how the",
				"command is called.",
				"The command can be called with one argument that must",
				"be either 'PROMPT', 'MULTILINE' or 'MORELINES', which",
				"outputs the symbol currently in use for the selected",
				"feature.",
				"If after the feature name, a single character symbol",
				"is given, then that feature's symbol is updated to the",
				"given symbol.",
				"",
				"Examples:",
				"symbol PROMPT   \\\\outputs the current symbol for PROMPT",
				"symbol PROMPT # \\\\updates the symbol for PROMPT to '#'",
				"",
				"Syntax:",
				"> symbol [FEATURE] [NEW CHARACTER]?",
				""
			);
	}
	
	/**
	 * Retrieves and outputs the currently used symbol for 
	 * a given feature.
	 * 
	 * @param arg The name of the feature whose symbol to output
	 * @param env The Environment from which the symbol is retrieved
	 */
	private void fetchSymbol(String arg, Environment env) {
		switch(arg) {
		case PROMPT:
			env.writeln(buildInfoMsg(PROMPT, env.getPromptSymbol()));
			break;
		case MULTI:
			env.writeln(buildInfoMsg(MULTI, env.getMultilineSymbol()));
			break;
		case MORE:
			env.writeln(buildInfoMsg(MORE, env.getMorelinesSymbol()));
			break;
		default:
			env.writeln("Unrecognized argument given with command 'symbol': "+arg);
		}
	}
	/**
	 * Builds the output message if the command was called
	 * with only one argument.
	 * 
	 * @param ID The name of the feature whose symbol was retrieved
	 * @param symbol The retrieved symbol
	 * @return A full message to display to the user
	 */
	private String buildInfoMsg(String ID, Character symbol) {
		return String.format("Symbol for %s is '%s'", ID, symbol);
	}
	
	/**
	 * Updates the Environment's symbol used for a feature to the
	 * newly given value.
	 * 
	 * @param arg The name of the feature whose symbol to update
	 * @param symbol The new symbol for the feature
	 * @param env The Environment in which to update the symbols
	 */
	private void updateSymbol(String arg, String symbol, Environment env) {
		if(symbol.length()!=1) {
			env.writeln("Invalid second argument for command 'symbol': "+symbol);
			return;
		}
		Character c = symbol.charAt(0);
		switch(arg) {
		case PROMPT:
			if(c==env.getMorelinesSymbol() || c==env.getMultilineSymbol()) {
				tnIAE("Illegal to use the same character for two features!");
			}
			env.writeln(buildUpdateMsg(PROMPT, env.getPromptSymbol(), c));
			env.setPromptSymbol(c);
			break;
		case MULTI:
			if(c==env.getMorelinesSymbol() || c==env.getPromptSymbol()) {
				tnIAE("Illegal to use the same character for two features!");
			}
			env.writeln(buildUpdateMsg(MULTI, env.getMultilineSymbol(), c));
			env.setMultilineSymbol(c);
			break;
		case MORE:
			if(c==env.getPromptSymbol() || c==env.getMultilineSymbol()) {
				tnIAE("Illegal to use the same character for two features!");
			}
			env.writeln(buildUpdateMsg(MORE, env.getMorelinesSymbol(), c));
			env.setMorelinesSymbol(c);
			break;
		default:
			env.writeln("Unrecognized argument given with command 'symbol': "+arg);
		}
	}
	/**
	 * Builds the output message if the command was called
	 * with two arguments.
	 * 
	 * @param ID The name of the feature whose symbol was updated
	 * @param old The previously used character for the given feature
	 * @param new The updated character for the given feature
	 * @return A full message to display to the user
	 */
	private String buildUpdateMsg(String ID, Character old, Character newChar) {
		return String.format("Symbol for %s changed from '%s' to '%s'", ID, old, newChar);
	}
}
