package hr.fer.zemris.java.hw06.shell.commands;

import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.FileVisitor;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.Arrays;
import java.util.List;

import hr.fer.zemris.java.hw06.shell.Environment;
import hr.fer.zemris.java.hw06.shell.ShellCommand;
import hr.fer.zemris.java.hw06.shell.ShellStatus;

import static hr.fer.zemris.java.hw06.shell.commands.Utility.*;

/**
 * TreeShellCommand is a command that outputs the directory
 * structure in a tree format, using the directory given as 
 * an argument as the root of the structure.
 * 
 * The command expects only one argument - a valid path to 
 * an existing directory, used as a starting point for the 
 * tree output.
 * 
 * Giving it any other number of arguments stops the execution
 * of the command and returns control back to the shell that
 * called it.
 * 
 * Correct example showing the syntax:
 * 	{@code tree C:\OPJJ\Homeworks\}
 * 		-- outputs all files and folders in the Homeworks
 * 			folder recursively
 * 
 * @author Miroslav Bićanić
 */
public class TreeShellCommand implements ShellCommand {
	/**
	 * {@inheritDoc}
	 * @param arguments A valid path to an existing folder
	 * @return Always returns ShellStatus.CONTINUE
	 */
	@Override
	public ShellStatus executeCommand(Environment env, String arguments) {
		try {
			Path p = getDirectory(arguments, TREE_CMD, 1);
			Files.walkFileTree(p, new TreeVisitor(env));
		} catch (IllegalArgumentException ex) {
			env.writeln(ex.getMessage());
		} catch (IOException ex) {
			env.writeln(IO_ERROR);
		}
		return ShellStatus.CONTINUE;
	}
	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getCommandName() {
		return TREE_CMD;
	}
	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<String> getCommandDescription() {
		return Arrays.asList(
				"This commands prints the directory structure in a tree format,",
				"with the root being the given directory.",
				"",
				"It takes only one argument: a valid path to an existing directory.",
				"",
				"Syntax:",
				"> tree [PATH_TO_DIRECTORY]",
				""
		);
	}
	
	/**
	 * An implementation of a FileVisitor that traverses the directory 
	 * structure, outputting each entry of the file system, formatted 
	 * to appear like a tree structure.
	 * 
	 * @author Miroslav Bićanić
	 */
	private static class TreeVisitor implements FileVisitor<Path> {
		/**
		 * The tabbing (indentation) that is used to clearly show
		 * which entries belong to which directories.
		 */
		private static final String TABBING = "  ";
		/**
		 * The "depth" of the visited files - the source directory
		 * is at depth zero, all it's direct children are at depth 1,
		 * all direct children of entries at depth 1 are at depth 2
		 * etc.
		 * Used to correctly indent files.
		 */
		private int depth = 0;
		/**
		 * An environment to which to output the directory structure.
		 */
		private Environment env;
		
		/**
		 * Constructor for a TreeVisitor
		 * @param env
		 */
		public TreeVisitor(Environment env) {
			this.env = env;
		}
		/**
		 * {@inheritDoc}
		 * Outputs the correctly indented directory's name and increases 
		 * the depth for one level before entering the directory.
		 */
		@Override
		public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException {
			env.writeln(TABBING.repeat(depth)+dir.getFileName().toString());
			depth++;
			return FileVisitResult.CONTINUE;
		}
		/**
		 * {@inheritDoc}
		 * Outputs the correctly indented name of the file
		 */
		@Override
		public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
			env.writeln(TABBING.repeat(depth)+file.getFileName().toString());
			return FileVisitResult.CONTINUE;
		}
		/**
		 * {@inheritDoc}
		 */
		@Override
		public FileVisitResult visitFileFailed(Path file, IOException exc) throws IOException {
			return FileVisitResult.CONTINUE;
		}
		/**
		 * {@inheritDoc}
		 * Decreases the depth by one level, ensuring further correct indentation.
		 */
		@Override
		public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
			depth--;
			return FileVisitResult.CONTINUE;
		}
	}
}
