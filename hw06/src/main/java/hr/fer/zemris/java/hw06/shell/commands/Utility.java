package hr.fer.zemris.java.hw06.shell.commands;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import hr.fer.zemris.java.hw06.shell.Environment;

/**
 * Utility class provides various static utility methods and
 * constants used by all classes within this package.
 * 
 * @author Miroslav Bićanić
 */
public class Utility {
	/**
	 * This message is outputted any time an IOException is caught
	 */
	static final String IO_ERROR = "I/O Exception occurred during file manipulation.";
	
	/**
	 * Name of the CatShellCommand
	 */
	static final String CAT_CMD = "cat";
	/**
	 * Name of the CopyShellCommand
	 */
	static final String COPY_CMD = "copy";
	/**
	 * Name of the LsShellCommand
	 */
	static final String LS_CMD = "ls";
	/**
	 * Name of the MkDirShellCommand
	 */
	static final String MKDIR_CMD = "mkdir";
	/**
	 * Name of the CharsetsShellCommand
	 */
	static final String CHARS_CMD = "charsets";
	/**
	 * Name of the ExitShellCommand
	 */
	static final String EXIT_CMD = "exit";
	/**
	 * Name of the HelpShellCommand
	 */
	static final String HELP_CMD = "help";
	/**
	 * Name of the HexDumpShellCommand
	 */
	static final String HEX_CMD = "hexdump";
	/**
	 * Name of the SymbolShellCommand
	 */
	static final String SYM_CMD = "symbol";
	/**
	 * Name of the TreeShellCommand
	 */
	static final String TREE_CMD = "tree";
	
	/**
	 * Name of the Environment symbolic character for prompts.
	 */
	static final String PROMPT = "PROMPT";
	/**
	 * Name of the Environment symbolic character for a
	 * request for a command to span multiple lines.
	 */
	static final String MORE = "MORELINES";
	/**
	 * Name of the Environment symbolic character for the
	 * beginning of a line that is interpreted as a continuation
	 * of the previous line.
	 */
	static final String MULTI = "MULTILINE";
	
	/**
	 * A list of hexadecimal digits, where the index of each digit
	 * corresponds to its decimal value.
	 */
	private static List<Character> hexDigits = Arrays.asList('0', '1', '2', '3', '4', '5', '6', '7',
			'8', '9', 'A', 'B', 'C', 'D', 'E', 'F');

	/**
	 * Shorthand method to throw an IllegalArgumentException.
	 * @param message The message describing the error that caused the exception
	 */
	static void tnIAE(String message) {
		throw new IllegalArgumentException(message);
	}
	
	/**
	 * Returns an array of arguments that the user provided,
	 * regardless of their number and type.
	 * 
	 * @param arguments A string containing an arbitrary number of arguments
	 * @return An array of arguments in String format
	 */
	static String[] getArgsPermissive(String arguments) {
		LexUtil lex = new LexUtil(arguments);
		List<String> args = new ArrayList<>();
		while(true) {
			String arg = lex.next();
			if(arg==null) {
				break;
			}
			args.add(arg);
		}
		return args.toArray(new String[args.size()]); //ArrayList:: T[] toArray(T[] arr)
	}
	/**
	 * Returns an array of arguments that the user provided,
	 * checking if the number of arguments matches the expected
	 * number of arguments.
	 * 
	 * @param arguments A string containing all the arguments
	 * @param cmd The command from which this method was called 
	 * @param number The expected number of arguments 
	 * @return An array of arguments if their number matches the expected number
	 * @throws IllegalArgumentException 
	 * 			-if the given number of expected arguments is negative
	 * 			-if the number of arguments doesn't match
	 * @throws NullPointerException if arguments or cmd are null
	 */
	static String[] getCheckArgCount(String arguments, String cmd, int number) {
		if(number<0) {
			tnIAE("Cannot have a negative number of arguments!");
		}
		Objects.requireNonNull(arguments);
		Objects.requireNonNull(cmd);
		String[] args = getArgsPermissive(arguments);
		if(args.length!=number) {
			tnIAE("Too "+(args.length<number ? "few" : "many")+" arguments given to command "+cmd+". Expected "+number);
		}
		return args;
	}
	/**
	 * Returns a Path to a directory by parsing the arguments the
	 * user provided and checking that the number of arguments
	 * is correct.
	 * This method assumes that the path is given as the first 
	 * argument in the string of arguments.
	 * 
	 * @param arguments A string containing all the arguments
	 * @param cmd The name of the command that called this method
	 * @param number The expected number of arguments
	 * @return A Path to the user-specified directory
	 */
	static Path getDirectory(String arguments, String cmd, int number) {
		String[] args = getCheckArgCount(arguments,cmd,	number);
		return getDirectory(args[0]);
	}
	/**
	 * Returns a Path pointing to a directory whose path the user
	 * specified.
	 * 
	 * @param path The path to the directory
	 * @return A Path object pointing to that directory
	 * @throws IllegalArgumentException if the provided path does not point to a directory
	 */
	static Path getDirectory(String path) {
		Path p = Paths.get(path);
		if(!Files.isDirectory(p)) {
			tnIAE("Given path is not a path to a directory!");
		}
		return p;
	}
	
	/**
	 * Returns a Path to a file by parsing the arguments the
	 * user provided and checking that the number of arguments
	 * is correct.
	 * This method assumes that the path is given as the first 
	 * argument in the string of arguments.
	 * 
	 * @param arguments A string containing all the arguments
	 * @param cmd The name of the command that called this method
	 * @param number The expected number of arguments
	 * @return A Path to the user-specified file
	 */
	static Path getFile(String arguments, String cmd, int number) {
		String[] args = getCheckArgCount(arguments, cmd, number);
		return getFile(args[0]);
	}
	/**
	 * Returns a Path pointing to a file whose path the user
	 * specified.
	 * 
	 * @param path The path to the file
	 * @return A Path object pointing to that file
	 * @throws IllegalArgumentException if the provided path does not point to a file
	 */
	static Path getFile(String filename) {
		Path p = Paths.get(filename);
		if(!Files.isRegularFile(p)) {
			tnIAE("Given path is not a path to a file!");
		}
		return p;
	}
	
	/**
	 * Prompts the user to answer a yes/no question.
	 * Each question is comprised of an information message informing
	 * the user of the reason for asking the question, the question
	 * itself and a fail message to be shown if the answer was unacceptable.
	 * @param message The reason the question is asked
	 * @param query The question itself
	 * @param failed The message describing the action of the shell if the answer
	 * 				was unacceptable
	 * @param env An environment through which to communicate with the user
	 * @return true if the answer was yes, false otherwise
	 */
	static boolean queryYesNo(String message, String query, String failed, Environment env) {
		env.writeln(message);
		env.writeln(query+" [Y/N]");
		env.write(env.getPromptSymbol()+" ");
		String ans = env.readLine().trim().toLowerCase();
		if(ans.equals("n")||ans.equals("no")) {
			return false;
		} else if(ans.equals("y")||ans.equals("yes")) {
			return true;
		} else {
			env.writeln("Expected a Y/N or yes/no answer. "+failed);;
			return false;
		}
	}
	
	/**
	 * Converts one byte to a hexadecimal digit couple
	 * @param b The byte to convert
	 * @return A pair of hex digits
	 */
	static String byteToHex(byte b) {
		int left = (b&0xF0)>>4;
		int right = (b&0x0F);
		return getHexDigit(left) + getHexDigit(right);
	}
	/**
	 * Returns the hexadecimal digit corresponding to the decimal
	 * number given to the method. The decimal number must be
	 * in the range [0,15], so that a corresponding hex digit
	 * exists.
	 * 
	 * @param number The number from the [0,15] interval to convert to hex
	 * @return One hex digit representing the given number
	 */
	static String getHexDigit(int number) {
		if(number<0 || number>15) {
			tnIAE("Cannot convert "+number+" to one hex digit.!");
		}
		return hexDigits.get(number).toString();
	}
	/**
	 * Converts an arbitrary decimal number into a hexadecimal
	 * number.
	 * 
	 * @param number The number to convert to hexadecimal
	 * @return The hexadecimal representation of the number
	 */
	static String decToHex(int number) {
		StringBuilder sb = new StringBuilder(8);
		while(number>=0) {
			sb.append(getHexDigit(number%16));
			number/=16;
			if(number==0) {
				break;
			}
		}
		return sb.reverse().toString();
	}
}
