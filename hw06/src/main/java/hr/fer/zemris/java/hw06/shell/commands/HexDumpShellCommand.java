package hr.fer.zemris.java.hw06.shell.commands;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;

import hr.fer.zemris.java.hw06.shell.Environment;
import hr.fer.zemris.java.hw06.shell.ShellCommand;
import hr.fer.zemris.java.hw06.shell.ShellStatus;

import static hr.fer.zemris.java.hw06.shell.commands.Utility.*;

/**
 * HexDumpCommand is a command that reads a given file
 * and outputs its hex dump.
 * 
 * A hex dump is a view of a file showing its content as
 * a series of bytes, where each byte is represented as
 * two hexadecimal digits.
 * 
 * Each line of the hex dump is made of:
 * 	1) A line counter (in hexadecimal)
 * 	2) A series of 16 bytes
 * 	3) A text representation of the 16 bytes
 * For the text representation, only the characters with 
 * byte values between 32 and 127 (inclusively) are displayed,
 * while other characters are replaced with a '.'
 * 
 * To correctly execute, this command needs exactly one
 * argument - a valid path to an existing file.
 * Giving it any other number of arguments or an invalid
 * argument stops the execution of the command and returns
 * the execution back to the Shell that called the command.
 * 
 * Correct usage examples showing the syntax:
 * 	{@code hexdump C:\Files\file.txt}
 * 		-- Outputs the hexdump of file.txt
 * 
 * For example, a hex dump of a file whose content is:
 * "ABCDEFGHIJKLMNOPQRSTUVWXYZ" will be outputted and
 * formatted as:
 * <pre>
 * 00000000: 41 42 43 44 45 46 47 48 | 49 4A 4B 4C 4D 4E 4F 50 | ABCDEFGHIJKLMNOP
 * 00000010: 51 52 53 54 55 56 57 58 | 59 5A                   | QRSTUVWXYZ
 * </pre>
 * 
 * @author Miroslav Bićanić
 *
 */
public class HexDumpShellCommand implements ShellCommand {
	/**
	 * The number of bytes to be outputted in one segment of the output line.
	 */
	private static final int SEGMENT_BYTE_COUNT = 8;
	/**
	 * A symbol with which to replace all bytes with a value
	 * out of the inclusive range [32,127] in the text view
	 * of the hex dump.
	 */
	private static final String IRREGULAR = ".";
	/**
	 * Used to fill the space in the table when there are no
	 * more bytes to dump.
	 */
	private static final String BLANK_BYTE = "  "; 
	/**
	 * {@inheritDoc}
	 * @param arguments A valid path to an existing file whose content is to be dumped
	 * @return Always returns ShellStatus.CONTINUE
	 */
	@Override
	public ShellStatus executeCommand(Environment env, String arguments) {
		try {
			Path p = getFile(arguments, HEX_CMD, 1);
			Formatter f = new Formatter(Files.readAllBytes(p));
			while(f.hasNext()) {
				env.writeln(f.next());
			}
		} catch (IllegalArgumentException ex) {
			env.writeln(ex.getMessage());
		} catch (IOException ex) {
			env.writeln(IO_ERROR);
		}
		return ShellStatus.CONTINUE;
	}
	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getCommandName() {
		return HEX_CMD;
	}
	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<String> getCommandDescription() {
		return Arrays.asList(
			"Outputs the hex dump of a file's content.",
			"Each line of the hex dump includes a byte counter, the",
			"actual hex dump (containing 16 bytes represented by 16",
			"hex digit couples per line), and a text view of those 16",
			"bytes.",
			"",
			"This command requires exactly one argument - a valid path",
			"to an existing file.",
			"",
			"Syntax:",
			"> hexdump [PATH_TO_FILE]",
			""
		);
	}
	
	/**
	 * Helper class for formatting the output of a hex dump.
	 * 
	 * @author Miroslav Bićanić
	 */
	private static class Formatter implements Iterator<String>{
		/**
		 * An array of bytes to format for a hex dump
		 */
		byte[] array;
		/**
		 * The index of the first unprocessed byte
		 */
		int current = 0;
		
		/**
		 * A constructor for a Formatter
		 * @param array
		 */
		public Formatter(byte[] array) {
			this.array = array;
		}
		/**
		 * {@inheritDoc}
		 */
		@Override
		public boolean hasNext() {
			return current<array.length;
		}
		/**
		 * {@inheritDoc}
		 */
		@Override
		public String next() {
			if(!hasNext()) {
				throw new NoSuchElementException();
			}
			StringBuilder sb = new StringBuilder(50);
			return(formatLineNumber()
					+formatBytes(sb)
					+formatBytes(sb)
					+sb.toString());
		}
		
		/**
		 * Formats the next SEGMENT_BYTE_COUNT bytes into as many 
		 * hexadecimal digit couples, each separated by a space, 
		 * followed by a delimiter '|'.
		 * 
		 * Simultaneously, it builds the text view of those bytes in
		 * the given StringBuilder.
		 * 
		 * Output can look like this:
		 * "41 5A 6E 36 49 51 38 AF | "
		 * 
		 * @param content A StringBuilder for building the text view
		 * @return A string
		 */
		private String formatBytes(StringBuilder content) {
			StringBuilder byteSB = new StringBuilder(50);
			for(int i=0; i<SEGMENT_BYTE_COUNT; i++) {
				if(current+i>=array.length) {
					byteSB.append(BLANK_BYTE);
				} else {
					byteSB.append(byteToHex(array[current+i]));
					updateContent(array[current+i], content);
				}
				byteSB.append(" ");
			}
			current+=SEGMENT_BYTE_COUNT;
			byteSB.append("| ");
			return byteSB.toString();
		}
		
		/**
		 * Adds the character representation of the given byte into
		 * the given StringBuilder only if the byte is within the
		 * inclusive [32,127] range.
		 * 
		 * @param c The byte to be converted to a character
		 * @param content A StringBuilder for building the text view
		 */
		private void updateContent(byte c, StringBuilder content) {
			if(c>=32 && c<=127) {
				content.append((char)c);
			} else {
				content.append(IRREGULAR);
			}
		}

		/**
		 * Computes and returns an 8 digit hex representation
		 * of the line number in the hex dump.
		 * The line number represents the number of processed
		 * bytes so far.
		 * 
		 * @return A hexadecimal number representing the number of 
		 * 			processed bytes so far
		 */
		private String formatLineNumber() {
			String hexValue = decToHex(current);
			return "0".repeat(8-hexValue.length())+hexValue+": ";
		}
	}
}
