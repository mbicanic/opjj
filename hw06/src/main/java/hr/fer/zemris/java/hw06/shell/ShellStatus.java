package hr.fer.zemris.java.hw06.shell;

/**
 * ShellStatus enumerates the two possible statuses
 * of a command - CONTINUE and TERMINATE.
 * 
 * @author Miroslav Bićanić
 */
public enum ShellStatus {
	/**
	 * Most commands end in this status, disregarding the
	 * success of their execution, allowing MyShell to 
	 * continue working.
	 */
	CONTINUE,
	/**
	 * Only the 'exit' command ends with this status, signaling
	 * to MyShell that it should end its execution, terminating
	 * the whole program.
	 */
	TERMINATE;
}
