package hr.fer.zemris.java.hw06.crypto;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.spec.AlgorithmParameterSpec;
import java.util.Scanner;
import static hr.fer.zemris.java.hw06.crypto.Util.*;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

/**
 * Crypto class provides the functionality of encrypting,
 * decrypting and checking the SHA digest of files.
 * 
 * The class takes arguments from the command line at 
 * start up, asking the user for additional data at runtime,
 * depending on the chosen operation.
 * 
 * When checking the SHA digest, the program calculates a
 * SHA-256 digest and compares it with the digest the user
 * provided at runtime.
 * To check the SHA digest, the program must be run with
 * two arguments - command 'checksha' and a path to the file
 * to be checked, like so:
 * {@code> Crypto checksha example.txt}
 * 
 * Encryption and decryption are done using the AES crypto-
 * algorithm and a 128-bit encryption key.
 * To encrypt or decrypt files, the program must be run with
 * three arguments - the 'encrypt' or 'decrypt' command, a
 * path to the source file and a path to the destination file.
 * The encryption key as well as the initialization vector
 * for the AES cryptoalgorithm are provided by the user at
 * runtime.
 * 
 * @author Miroslav Bićanić
 */
public class Crypto {
	
	/**
	 * Shorthand method to concisely throw an IllegalArgumentException.
	 * @param message A description of the error that caused the exception.
	 */
	private static void tnIAE(String message) {
		throw new IllegalArgumentException(message);
	}
	
	/**
	 * The entry point of the program.
	 * 
	 * @param args a command (checksha, encrypt, decrypt) followed by
	 * 			one or two paths to files, depending on the command.
	 */
	public static void main(String[] args) {
		if(args.length<2 || args.length>3) {
			System.err.println("Invalid number of arguments given.");
			return;
		}
		try {
			execute(args);
		} catch (IllegalArgumentException ex) {
			System.err.println(ex.getMessage());
		}
	}

	/**
	 * Executing method of the program, doing appropriate actions
	 * depending on the command and arguments the user provided.
	 * 
	 * @param args The arguments the user provided
	 */
	private static void execute(String[] args) {
		String cmd = args[0].toLowerCase();
		if(cmd.equals("encrypt") || cmd.equals("decrypt")) {
			if(args.length!=3) {
				tnIAE("Invalid number of arguments for '"+cmd+"'. Expected source and destination file.");
			}
			
			crypting(args[1], args[2], cmd.equals("encrypt"));
			return;
		}
		else if(cmd.equals("checksha")) {
			if(args.length!=2) {
				tnIAE("Invalid number of arguments for checksha. Expected 1 source file.");
			}
			checkSHA(args[1]);
			return;
		}
		
		tnIAE("Unrecognized command given: '"+cmd+"'.");
	}
	
	/**
	 * Computes the SHA-256 digest of a given file using an instance
	 * of a {@link MessageDigest} and compares it to the expected digest.
	 * 
	 * @param filename The name of the file to calculate the digest of
	 */
	private static void checkSHA(String filename) {
		try(InputStream is = Files.newInputStream(Paths.get(filename))) {
			String expected = queryExpectedSHA(filename);
			MessageDigest md = MessageDigest.getInstance("SHA-256");
			byte[] buff = new byte[4096];
			while(true) {
				int r = is.read(buff);
				if(r<1) break;
				md.update(buff, 0, r);
			}
			processSHAResults(expected, bytetohex(md.digest()), filename);
		} catch( IOException | NoSuchAlgorithmException ex) {
			System.out.println(ex.getMessage());
			return;
		}
	}
	
	/**
	 * Prompts the user to enter the expected SHA-256 digest
	 * of the file being checked, returning the expected digest.
	 * 
	 * @param filename The name of the file that is being checked
	 * @return What the user entered as the expected digest of that file
	 */
	private static String queryExpectedSHA(String filename) {
		Scanner sc = new Scanner(System.in);
		System.out.printf("Please provide expected sha-256 digest for %s:%n> ", filename);
		String s = sc.next();
		sc.close();
		return s;
	}
	
	/**
	 * Compares the expected digest with the one {@link checkSHA}
	 * computed, and then outputs the results to standard out.
	 * 
	 * @param expected The expected digest for the file
	 * @param computed The computed digest of the file
	 * @param filename The name of the file being checked
	 */
	private static void processSHAResults(String expected, String computed, String filename) {
		boolean equ = expected.equals(computed);
		System.out.printf("Digesting completed. Digest of %s %s expected digest.%n", filename, (equ ? "matches the" : "does not match the"));
		if(!equ) {
			System.out.println("Digest was: "+computed);
		}
	}
	
	
	/**
	 * Encrypts or decrypts the source file using the AES cryptoalgorithm
	 * and a 128-bit key, and then writing the encrypted or decrypted data 
	 * into the destination file.
	 * 
	 * @param src The path to the source file
	 * @param dest The path to the destination file (will be created or overwritten if exists)
	 * @param encrypt Decides the mode of crypting - encryption if true, decryption if false
	 */
	private static void crypting(String src, String dest, boolean encrypt) {
		try(InputStream is = Files.newInputStream(Paths.get(src));
				OutputStream os = Files.newOutputStream(Paths.get(dest))) {
			Cipher c = getCipher(encrypt);
			byte[] inBuff = new byte[4096];
			while(true) {
				int r = is.read(inBuff);
				if(r<1) break;
				os.write(c.update(inBuff, 0, r));
			}
			os.write(c.doFinal());
		} catch(IOException ex) {
			System.err.println("IO exception occurred.");
			return;
		} catch (BadPaddingException | IllegalBlockSizeException ex) {
			System.err.println("Exception occurred while using the Cipher object.");
			return;
		} catch (Exception ex) { //see method getCipher() for a list of exceptions being thrown
			System.err.println("Exception occurred while initializing the Cipher object.");
			return;
		}
		System.out.printf(
				"%s completed. Generated file %s based on file %s",
				encrypt ? "Encryption" : "Decryption",
				dest, 
				src
			);
	}
	
	/**
	 * Initializes and returns a {@link Cipher} object capable of encryption or
	 * decryption. The returned Cipher will encrypt or decrypt depending on the
	 * {@code encrypt} value.
	 * 
	 * @param encrypt Decides the operating mode of the Cipher object: 
	 * 					encrypting if true, decrypting if false.
	 * @return A Cipher object ready for encrypting or decrypting
	 * @throws InvalidKeyException If the user-provided initialization key is invalid
	 * @throws InvalidAlgorithmParameterException If the user-provided algorithm parameter is invalid
	 * 												(initialization vector)
	 * @throws NoSuchAlgorithmException If the chosen algorithm for a Cipher does not exist
	 * @throws NoSuchPaddingException If the chosen padding for a Cipher does not exist
	 */
	private static Cipher getCipher(boolean encrypt) throws InvalidKeyException, 
				InvalidAlgorithmParameterException, NoSuchAlgorithmException, NoSuchPaddingException {
		
		String[] params = queryCryptingParameters();
		String keyText = params[0];
		String ivText = params[1];
		
		SecretKeySpec keySpec = new SecretKeySpec(Util.hextobyte(keyText), "AES");
		AlgorithmParameterSpec paramSpec = new IvParameterSpec(hextobyte(ivText));
		Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
		cipher.init(encrypt ? Cipher.ENCRYPT_MODE : Cipher.DECRYPT_MODE, keySpec, paramSpec);
		return cipher;	
	}
	
	/**
	 * This method prompts the user to enter the 16-byte, hex-encoded
	 * password (key) and an initialization vector in the same format.
	 * These parameters are used for the initialization of a Cipher
	 * object.
	 * 
	 * @return An array with two parameters - the key and the initialization vector
	 */
	private static String[] queryCryptingParameters() {
		String[] params = new String[2];
		Scanner sc = new Scanner(System.in);
		System.out.printf("Please provide password as hex-encoded text (16 bytes, i.e. 32 hex-digits):%n> ");
		params[0] = sc.next();
		System.out.printf("Please provide initialization vector as hex-encoded text (32 hex-digits):%n> ");
		params[1] = sc.next();
		sc.close();
		return params;
	}
}
