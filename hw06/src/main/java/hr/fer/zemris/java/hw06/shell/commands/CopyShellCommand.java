package hr.fer.zemris.java.hw06.shell.commands;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;

import hr.fer.zemris.java.hw06.shell.Environment;
import hr.fer.zemris.java.hw06.shell.ShellCommand;
import hr.fer.zemris.java.hw06.shell.ShellStatus;
import static hr.fer.zemris.java.hw06.shell.commands.Utility.*;

/**
 * CopyShellCommand is a command that reads a file and copies
 * its content into another file (existing or not).
 * 
 * To correctly execute, it must receive exactly two arguments:
 * 	1) A valid path to an existing file to be copied - source
 * 	2) A valid path to a file (existing or not) located in an
 * 		existing directory, or to an existing directory - destination
 * 
 * If the destination file does not exist, it will be created. 
 * If it exists, the command will ask for user's permission to overwrite 
 * its content.
 * When the second given argument is a path to an existing folder,
 * the created file will be named the same as the source file.
 * 
 * To clarify, let's say we already have the following structure
 * in our file system:
 * 
 * src					[DIR]
 * |--Folder One		[DIR]
 * |  |--example.txt	[FILE]
 * |  |--InsideFolder	[DIR]
 * |--FolderB			[DIR]
 * |--file.txt			[FILE]
 * 
 * Following command examples illustrate how the command works,
 * simultaneously showing the accepted syntax (working with the
 * directory structure from above):
 *
 * {@code copy src/file.txt "src/Folder One/InsideFolder/myFile.txt}
 * 		-- copies the content of 'file.txt' to a newly created 'myFile.txt'
 * 			in the already existing directory structure
 * {@code copy src/file.txt src/FolderB}
 * 		-- copies the content of 'file.txt' to a new file called 'file.txt'
 * 			in the already existing directory structure
 * {@code copy src/file.txt "src/Folder One/example.txt"}
 * 		-- copies the content of 'file.txt' to the existing 'example.txt'
 * 			if the user permits it by typing 'Y' or 'yes' when prompted
 * {@code copy src/file.txt src/FolderC}
 * 		-- copies the content of 'file.txt' to a new file called 'FolderC'.
 * 			The created file does not have an extension, and creating such
 * 			files is not advised - the user will be asked to agree with 
 * 			creating such a file.
 * 
 * The structure after these commands will look like this:
 * 
 * src						[DIR]
 * |--Folder One			[DIR]
 * |  |--example.txt		[FILE]	//overwritten if user permitted
 * |  |--InsideFolder		[DIR]
 * |     |--myFile.txt		[FILE]
 * |--FolderB				[DIR]
 * |  |--file.txt			[FILE]
 * |--file.txt				[FILE]
 * |--FolderC				[FILE]	//only if the user permitted
 *
 * Please note: if a file without an extension is present in the file system,
 * 				a folder of the same name cannot be created next to it.
 * 
 * Giving the command any other number of arguments, or an illegal argument
 * stops the command's execution and returns the control to the Shell from
 * which the command was called.
 *
 * @author Miroslav Bićanić
 */
public class CopyShellCommand implements ShellCommand {

	/**
	 * {@inheritDoc}
	 * @param arguments A path to a source file and a path to a destination file or directory
	 * @return Always returns ShellStatus.CONTINUE
	 */
	@Override
	public ShellStatus executeCommand(Environment env, String arguments) {
		Path src, dest;
		try {
			String[] args = getCheckArgCount(arguments, COPY_CMD, 2);
			src = getFile(args[0]);
			dest = getTrueDestination(Paths.get(args[1]), src, env);
			Files.write(dest, Files.readAllLines(src));
			env.writeln("Successfully copied!");
			
		} catch (IllegalArgumentException ex) {
			env.writeln(ex.getMessage());
		} catch (IOException ex) {
			env.writeln(IO_ERROR);
		}
		return ShellStatus.CONTINUE;
	} 

	/**
	 * Checks if the desired destination exists, and updates the path with
	 * the source's filename if the destination is a directory.
	 * 
	 * @param desired The destination path desired by the user
	 * @param src The path to the source file
	 * @param env An environment through which to communicate
	 * @return A destination path ready for receiving the copied content
	 * @throws IOException - see method checkExistence
	 */
	private Path getTrueDestination(Path desired, Path src, Environment env) throws IOException {
		Path parent = desired.getParent();
		if(parent==null) {
			return checkExistence(checkExtension(desired,env), env);
		}
		if(!Files.exists(desired.getParent())) {
			tnIAE("Given directory structure does not exist.");
		}
		if(Files.isDirectory(desired)) {
			Path real = Paths.get(desired.toString(), src.getFileName().toString());
			return checkExistence(real, env);
		} else {
			return checkExistence(checkExtension(desired,env), env);
		}
	}
	/**
	 * Checks if the specified destination file has an extension,
	 * asking the user for permission to create it if it doesn't.
	 * 
	 * @param desired The desired destination path provided by the user
	 * @param env An environment thorugh which communication with the user is realized
	 * @return The path to the destination file
	 * @throws IllegalArgumentException if the user responded incorrectly, or refused
	 * 			permission to copy to a file without extension
	 */
	private Path checkExtension(Path desired, Environment env) {
		String name = desired.getFileName().toString();
		if(!name.contains(".")) {
			boolean ans = queryYesNo(
					"You are attempting to copy to a file without an extension.", 
					"Do you want to proceed?",
					"File not copied.",
					env);
			if(!ans) {
				tnIAE(""); //if it was an error, queryYesNo will output, that's why an empty exception is sent
			}
		}
		return desired;
	}

	/**
	 * Checks if the file itself exists, and asks for user's
	 * permission to overwrite it if it does.
	 * 
	 * @param dest The destination file
	 * @param env The environment through which communication with the user is realized
	 * @return A Path pointing to the destination file
	 * @throws IOException if user agreed to overwrite, but deleting the existing
	 * 						file could not be completed
	 * @throws IllegalArgumentException if the user responded incorrectly, or refused
	 * 			permission to overwrite a file
	 */
	private Path checkExistence(Path dest, Environment env) throws IOException {
		if(Files.exists(dest)) {
			boolean answer = queryYesNo(
					"Destination file '"+dest.getFileName()+"' already exists.",
					"Do you want to overwrite it?",
					"File not copied.",
					env);
			if(!answer) {
				tnIAE("");
			}
			Files.delete(dest);
		}
		return dest;
	}
	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getCommandName() {
		return COPY_CMD;
	}
	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<String> getCommandDescription() {
		return Arrays.asList(
				"Copies the content from the source file and creates a",
				"new file with equal content.",
				"",
				"It takes two arguments - a valid path to an existing file",
				"that is the source (and MUST be a file, directory copying",
				"not supported).",
				"The second argument must be a path to the destination file",
				"or directory:",
				"1) If given a path to a non-existant file in an existing",
				"\tdirectory structure, the file is created in the structure",
				"2) If given a path to an already existing file, the user is",
				"\tasked for permission to overwrite it.",
				"3) If given a path to an existing directory, then the source",
				"\tfile is copied to the given destination directory, using the",
				"\tsame name as the original source file.",
				"",
				"Non-existing directories will not be created by this command.",
				"",
				"Syntax:",
				"> copy [SOURCE FILE] [DESTINATION FILE | DESTINATION DIRECTORY]",
				""
			);
	}

}
