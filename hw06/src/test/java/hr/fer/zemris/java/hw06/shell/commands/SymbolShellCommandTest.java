package hr.fer.zemris.java.hw06.shell.commands;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import hr.fer.zemris.java.hw06.shell.Environment;
import hr.fer.zemris.java.hw06.shell.ShellStatus;
import hr.fer.zemris.java.hw06.shell.commands.Helper.DummyEnvironment;

class SymbolShellCommandTest {

	@Test
	void testSymbolSwitching() {
		DummyEnvironment dm = new Helper.DummyEnvironment(true);
		Environment env = (Environment) dm;
		SymbolShellCommand cmd = new SymbolShellCommand();
		assertEquals('>', env.getPromptSymbol());
		assertEquals('|', env.getMultilineSymbol());
		assertEquals('\\', env.getMorelinesSymbol());
		assertEquals(ShellStatus.CONTINUE, cmd.executeCommand(env, "PROMPT ?"));
		assertEquals(ShellStatus.CONTINUE, cmd.executeCommand(env, "MORELINES +"));
		assertEquals(ShellStatus.CONTINUE, cmd.executeCommand(env, "MULTILINE ~"));
		assertEquals('?', env.getPromptSymbol());
		assertEquals('~', env.getMultilineSymbol());
		assertEquals('+', env.getMorelinesSymbol());
	}
}
