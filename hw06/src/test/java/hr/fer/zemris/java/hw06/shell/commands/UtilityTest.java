package hr.fer.zemris.java.hw06.shell.commands;

import static org.junit.jupiter.api.Assertions.*;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

import org.junit.jupiter.api.Test;

import hr.fer.zemris.java.hw06.shell.Environment;
import hr.fer.zemris.java.hw06.shell.commands.Helper.DummyEnvironment;

import static hr.fer.zemris.java.hw06.shell.commands.Utility.*;

class UtilityTest {
	//current working directory is the project directory
	@Test
	void testGetArgsPermissive() {
		String arguments = "This should return an array";
		String[] arr = getArgsPermissive(arguments);
		assertEquals(5, arr.length);
		assertEquals("This", arr[0]);
		assertEquals("should", arr[1]);
		assertEquals("return", arr[2]);
		assertEquals("an", arr[3]);
		assertEquals("array", arr[4]);
		assertThrows(NullPointerException.class, ()->getArgsPermissive(null));
	}
	@Test
	void testGetCheckArgCount() {
		String arguments = "Three arguments present";
		assertThrows(IllegalArgumentException.class, ()->getCheckArgCount(arguments, "test", -1));
		assertThrows(IllegalArgumentException.class, ()->getCheckArgCount(arguments, "test", 2));
		assertThrows(IllegalArgumentException.class, ()->getCheckArgCount(arguments, "test", 4));
		assertThrows(NullPointerException.class, ()->getCheckArgCount(null, "test", 3));
		assertThrows(NullPointerException.class, ()->getCheckArgCount(arguments, null, 3));
		String[] arr = getCheckArgCount(arguments, "test", 3);
		assertEquals("Three", arr[0]);
		assertEquals("arguments", arr[1]);
		assertEquals("present", arr[2]);
	}
	@Test
	void testGetDirectoryString() {
		assertThrows(IllegalArgumentException.class, ()-> getDirectory("nonexistantDir"));
		assertThrows(IllegalArgumentException.class, ()-> getDirectory("pom.xml"));
		assertEquals(Paths.get("src"), getDirectory("src"));
		assertTrue(Files.isDirectory(getDirectory("src")));
	}
	@Test
	void testGetFileString() {
		assertThrows(IllegalArgumentException.class, ()-> getFile("nonexistantFile.txt"));
		assertThrows(IllegalArgumentException.class, ()-> getFile("src"));
		assertEquals(Paths.get("pom.xml"), getFile("pom.xml"));
		assertTrue(Files.isRegularFile(getFile("pom.xml")));
	}
	@Test
	void testQueryYes() {
		DummyEnvironment dm = new Helper.DummyEnvironment(true);
		Environment env = (Environment) dm;
		boolean res = queryYesNo("Message", "Question?", "Failed!", env);
		assertTrue(res);
		List<String> l = dm.getOutput();
		assertEquals(3, l.size());
		assertEquals("Message\n", l.get(0));
		assertEquals("Question? [Y/N]\n", l.get(1));
		assertEquals("> ", l.get(2));
	}
	@Test
	void testQueryNo() {
		DummyEnvironment dm = new Helper.DummyEnvironment(false);
		Environment env = (Environment) dm;
		boolean res = queryYesNo("Message", "Question?", "Failed!", env);
		assertFalse(res);
		List<String> l = dm.getOutput();
		assertEquals(3, l.size());
		assertEquals("Message\n", l.get(0));
		assertEquals("Question? [Y/N]\n", l.get(1));
		assertEquals("> ", l.get(2));
	}
	@Test
	void testB2H() {
		assertEquals(byteToHex((byte)15), "0F");
		assertEquals(byteToHex((byte)64), "40");
		assertEquals(byteToHex((byte)41), "29");
		assertEquals(byteToHex((byte)28), "1C");
	}
	@Test
	void testDec2Hex() {
		assertEquals(decToHex(172), "AC");
		assertEquals(decToHex(255), "FF");
		assertEquals(decToHex(18), "12");
	}

}
