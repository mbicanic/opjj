package hr.fer.zemris.java.hw06.shell.commands;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class LexUtilTest {

	@Test
	void testLexerStart() {
		assertThrows(NullPointerException.class, ()-> new LexUtil(null));
		LexUtil l = new LexUtil("");
		assertEquals(null, l.next());
		l = new LexUtil("       ");
		assertEquals(null, l.next());
	}
	
	@Test
	void testStringGeneration() {
		LexUtil l = new LexUtil("Arguments made of many strings");
		assertEquals("Arguments", l.next());
		assertEquals("made", l.next());
		assertEquals("of", l.next());
		assertEquals("many", l.next());
		assertEquals("strings", l.next());
		assertNull(l.next());
	}
	@Test
	void testQuoteGeneration() {
		LexUtil l = new LexUtil("\"Each\" \"argument\" \"is\" \"enclosed\"");
		assertEquals("Each", l.next());
		assertEquals("argument", l.next());
		assertEquals("is", l.next());
		assertEquals("enclosed", l.next());
		assertNull(l.next());
	}
	@Test
	void testCombined() {
		LexUtil l = new LexUtil("Mixed \"arguments\" are \"appearing\"");
		assertEquals("Mixed", l.next());
		assertEquals("arguments", l.next());
		assertEquals("are", l.next());
		assertEquals("appearing", l.next());
		assertNull(l.next());
	}
	@Test
	void testEscaping() {
		LexUtil l = new LexUtil("\"Ea\\\"ch\" \"arg\\\\um\\ent\" is \"esc\\\"aped\" \"  \\\\\\\"  \"");
		assertEquals("Ea\"ch", l.next());
		assertEquals("arg\\um\\ent", l.next());
		assertEquals("is", l.next());
		assertEquals("esc\"aped", l.next());
		assertEquals("  \\\"  ", l.next());
		assertNull(l.next());
	}
	@Test
	void testUnclosedQuote() {
		LexUtil l = new LexUtil("This text \"is\" \"incorrect");
		assertEquals("This", l.next());
		assertEquals("text", l.next());
		assertEquals("is", l.next());
		assertThrows(IllegalArgumentException.class, ()-> l.next());
	}
	@Test
	void testIncorrectlyClosed() {
		LexUtil l = new LexUtil("This text \"is incorrectly closed\"here");
		assertEquals("This", l.next());
		assertEquals("text", l.next());
		assertThrows(IllegalArgumentException.class, ()->l.next());
	}
}
