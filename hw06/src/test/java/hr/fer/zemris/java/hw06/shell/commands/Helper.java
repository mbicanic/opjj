package hr.fer.zemris.java.hw06.shell.commands;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.SortedMap;
import java.util.TreeMap;

import hr.fer.zemris.java.hw06.shell.Environment;
import hr.fer.zemris.java.hw06.shell.ShellCommand;
import hr.fer.zemris.java.hw06.shell.ShellIOException;

public class Helper {
	//identical to the MyEnvironment used in MyShell
	//but writes to a retrievable list of strings
	public static class DummyEnvironment implements Environment{
		private Character prompt = '>';
		private Character morelines = '\\';
		private Character multiline = '|';
		
		private SortedMap<String, ShellCommand> commands;
		private List<String> output;
		private boolean yes;
	
		public DummyEnvironment(boolean yes) {
			this.commands = setUpCommands();
			this.output = new ArrayList<>();
			this.yes = yes;	//see readLine() method
		}
	
		@Override
		public String readLine() throws ShellIOException {
			return yes ? "yes" : "no";	//to answer to queries
		}
		@Override
		public void write(String text) throws ShellIOException {
			output.add(text);
		}
		@Override
		public void writeln(String text) throws ShellIOException {
			output.add(text+"\n");			
		}
		@Override
		public SortedMap<String, ShellCommand> commands() {
			return Collections.unmodifiableSortedMap(this.commands);
		}
		@Override
		public Character getMultilineSymbol() {
			return multiline;
		}
		@Override
		public void setMultilineSymbol(Character symbol) {
			this.multiline = symbol;	
		}
		@Override
		public Character getPromptSymbol() {
			return prompt;
		}
		@Override
		public void setPromptSymbol(Character symbol) {
			this.prompt = symbol;
		}
		@Override
		public Character getMorelinesSymbol() {
			return morelines;
		}
		@Override
		public void setMorelinesSymbol(Character symbol) {
			this.morelines = symbol;
		}
		
		public List<String> getOutput(){
			return output;
		}
	}
	private static SortedMap<String, ShellCommand> setUpCommands() {
		SortedMap<String, ShellCommand> commands = new TreeMap<String, ShellCommand>();
		commands.put("cat", new CatShellCommand());
		commands.put("charsets", new CharsetsShellCommand());
		commands.put( "copy", new CopyShellCommand());
		commands.put("hexdump", new HexDumpShellCommand());
		commands.put("ls", new LsShellCommand());
		commands.put("mkdir", new MkDirShellCommand());
		commands.put("tree", new TreeShellCommand());
		commands.put("help", new HelpShellCommand());
		commands.put("exit", new ExitShellCommand());
		commands.put("symbol", new SymbolShellCommand());
		return commands;
	}
}
