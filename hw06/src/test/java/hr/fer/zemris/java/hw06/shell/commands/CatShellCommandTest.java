package hr.fer.zemris.java.hw06.shell.commands;

import static org.junit.jupiter.api.Assertions.*;

import java.util.List;

import org.junit.jupiter.api.Test;


import hr.fer.zemris.java.hw06.shell.Environment;
import hr.fer.zemris.java.hw06.shell.ShellStatus;
import hr.fer.zemris.java.hw06.shell.commands.Helper.DummyEnvironment;

//nisam stigao testirati ostale naredbe na ovaj nacin
//ali nije bilo ni potrebno
class CatShellCommandTest {
	private static String[] failArgs = {
			"src/test/resources/cat/file1.txt UTF-8 too many args",
			"", //too few args
			"src/test/resources/cat/failDir",
			"src/test/resources/cat/nonexistant.txt"
	};
	
	private static String[] successArgs = {
			"src/test/resources/cat/file1.txt UTF-8",
			"\"src/test/resources/cat/file1.txt\" wkf",
			"src/test/resources/cat/file2.txt"
	};
	
	@Test
	void testFailure() {
		DummyEnvironment dm = new Helper.DummyEnvironment(true);
		Environment env = (Environment) dm;
		CatShellCommand cmd = new CatShellCommand();
		assertEquals(ShellStatus.CONTINUE, cmd.executeCommand(env, failArgs[0]));
		assertEquals(ShellStatus.CONTINUE, cmd.executeCommand(env, failArgs[1]));
		assertEquals(ShellStatus.CONTINUE, cmd.executeCommand(env, failArgs[2]));
		assertEquals(ShellStatus.CONTINUE, cmd.executeCommand(env, failArgs[3]));
		
		List<String> result = dm.getOutput();
		assertEquals(result.size(), 4);
		assertTrue(result.get(0).startsWith("Too many arguments"));
		assertTrue(result.get(1).startsWith("Too few arguments"));
		assertTrue(result.get(2).startsWith("Given path is not a path to a file"));
		assertTrue(result.get(3).startsWith("Given path is not a path to a file"));
	}
	
	@Test
	void testSuccess() {
		DummyEnvironment dm = new Helper.DummyEnvironment(true);
		Environment env = (Environment) dm;
		CatShellCommand cmd = new CatShellCommand();
		assertEquals(ShellStatus.CONTINUE, cmd.executeCommand(env, successArgs[0]));
		assertEquals(ShellStatus.CONTINUE, cmd.executeCommand(env, successArgs[1]));
		assertEquals(ShellStatus.CONTINUE, cmd.executeCommand(env, successArgs[2]));
		
		List<String> result = dm.getOutput();
		assertEquals(result.size(), 4+7+3);
		
		assertTrue(result.get(0).startsWith("file1"));
		assertTrue(result.get(1).startsWith("line1"));
		assertTrue(result.get(2).startsWith("line2"));
		assertTrue(result.get(3).startsWith("\n"));
		assertTrue(result.get(4).startsWith("Invalid charset"));
		assertTrue(result.get(5).startsWith("Do you wish"));
		assertTrue(result.get(6).startsWith(">"));
		assertTrue(result.get(7).startsWith("file1"));
		assertTrue(result.get(8).startsWith("line1"));
		assertTrue(result.get(9).startsWith("line2"));
		assertTrue(result.get(10).startsWith("\n"));
		assertTrue(result.get(11).startsWith("file2"));
		assertTrue(result.get(12).startsWith("is this working"));
		assertTrue(result.get(13).startsWith("\n"));
	}
}
