package hr.fer.zemris.java.hw06.crypto;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;

import static hr.fer.zemris.java.hw06.crypto.Util.*;

class UtilTest {
	private static byte[] arr1 = {1, 10, 20, 32, 40, 48, 15};
	private static byte[] arr2 = {15, 29, 31, 43, 100};
	@Test
	void testB2H() {
		assertEquals(bytetohex(null), "");
		assertEquals(bytetohex(new byte[] {}), "");
		
		assertEquals(bytetohex(arr1), "010a142028300f");
		assertEquals(bytetohex(arr2), "0f1d1f2b64");
	}

	@Test
	void testH2B() {
		assertEquals(hextobyte(null).length, 0);
		iterate("", new byte[] {});
		
		assertThrows(IllegalArgumentException.class, ()->hextobyte("a"));
		assertThrows(IllegalArgumentException.class, ()->hextobyte("abc"));
		assertThrows(IllegalArgumentException.class, ()->hextobyte("123")); //odd sized
		
		String val1="010a142028300f";
		String val2="0f1d1f2b64";
		iterate(val1, arr1);
		iterate(val2, arr2);
	}

	private void iterate(String val, byte[] arr) {
		byte[] res = hextobyte(val);
		assertEquals(res.length, val.length()/2);
		assertEquals(res.length, arr.length);
		for(int i = 0; i<arr.length;i++) {
			assertEquals(res[i], arr[i]);
		}
	}
}
