package hr.fer.zemris.java.raytracer;

import static hr.fer.zemris.java.raytracer.RayCasterUtil.tracer;

import java.util.concurrent.atomic.AtomicBoolean;

import hr.fer.zemris.java.raytracer.model.IRayTracerProducer;
import hr.fer.zemris.java.raytracer.model.IRayTracerResultObserver;
import hr.fer.zemris.java.raytracer.model.Point3D;
import hr.fer.zemris.java.raytracer.model.Ray;
import hr.fer.zemris.java.raytracer.model.Scene;
import hr.fer.zemris.java.raytracer.viewer.RayTracerViewer;

/**
 * RayCaster is a class demonstrating our implementation
 * of raycasting, with a single thread doing all the calculations.
 * 
 * @author Miroslav Bićanić
 */
public class RayCaster {
	
	/**
	 * The entry point for the program.
	 * 
	 * @param args command-line arguments: not used
	 */
	public static void main(String[] args) {
		RayTracerViewer.show(getIRayTracerProducer(),
				new Point3D(10,0,0),
				new Point3D(0,0,0),
				new Point3D(0,0,10),
				20, 20);
	}

	/**
	 * Returns an implementation of the {@link IRayTracerProducer}
	 * interface.
	 * 
	 * @return an IRayTracerProducer
	 */
	private static IRayTracerProducer getIRayTracerProducer() {
		return new IRayTracerProducer() {
			/**
			 * Produces the RGB components of each pixel on the image,
			 * iterating through every pixel and casting a ray through
			 * it.
			 * 
			 * Once the RGB components of all pixels have been calculated,
			 * the values are sent to the given observer to update the GUI.
			 */
			@Override
			public void produce(Point3D eye, Point3D view, Point3D viewUp, double horizontal, double vertical, int width,
					int height, long requestNo, IRayTracerResultObserver observer, AtomicBoolean cancel) {
				
				System.out.println("Zapocinjem izracune...");
				short[] red = new short[width*height];
				short[] green = new short[width*height];
				short[] blue = new short[width*height];
				
				Point3D og = view.sub(eye).normalize();
				Point3D vuv = viewUp.normalize();
				Point3D yAxis = vuv.sub(og.scalarMultiply(og.scalarProduct(vuv))).modifyNormalize();
				Point3D xAxis = og.vectorProduct(yAxis).modifyNormalize();
				Point3D screenCorner = view.
						sub(xAxis.scalarMultiply(horizontal/2)).
						add(yAxis.scalarMultiply(vertical/2));
				Scene scene = RayTracerViewer.createPredefinedScene();
				
				short[] rgb = new short[3];
				int offset = 0;
				double constX = horizontal/(width-1);
				double constY = vertical/(height-1);
				
				for(int y=0; y<height; y++) {
					for(int x=0; x<width; x++) {
						Point3D screenPoint = screenCorner
								.add(xAxis.scalarMultiply(x*constX))
								.sub(yAxis.scalarMultiply(y*constY));
						
						Ray ray = Ray.fromPoints(eye, screenPoint);
						tracer(scene, ray, rgb);

						red[offset] = rgb[0] > 255 ? 255 : rgb[0];
						green[offset] = rgb[1] > 255 ? 255 : rgb[1];
						blue[offset] = rgb[2] > 255 ? 255 : rgb[2];
						offset++;
					}
				}
				System.out.println("Izracuni gotovi...");
				observer.acceptResult(red, green, blue, requestNo);
				System.out.println("Dojava gotova...");
			}
		};
	}
}

