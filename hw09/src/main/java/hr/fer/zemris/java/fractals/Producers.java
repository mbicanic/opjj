package hr.fer.zemris.java.fractals;

import static hr.fer.zemris.java.fractals.NewtonUtil.*;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicBoolean;

import hr.fer.zemris.java.fractals.viewer.IFractalProducer;
import hr.fer.zemris.java.fractals.viewer.IFractalResultObserver;
import hr.fer.zemris.math.ComplexPolynomial;
import hr.fer.zemris.math.ComplexRootedPolynomial;

/**
 * Producers is a class containing implementations of the
 * {@linkplain IFractalProducer} interface, and factory methods
 * to get them.
 * 
 * @author Miroslav Bićanić
 */
public class Producers {
	
	/**
	 * Returns an IFractalProducer that performs the Newton-Raphson
	 * iteration with parallelization, using multiple threads.
	 * 
	 * @param cp the polynomial used for the iteration
	 * @return a parallel IFractalProducer
	 */
	public static IFractalProducer getParallelProducer(ComplexRootedPolynomial cp) {
		return new ParallelProducer(cp);
	}
	/**
	 * Returns an IFractalProducer that performs the Newton-Raphson
	 * iteration linearly, using a single thread.
	 * 
	 * @param cp the polynomial used for the iteration
	 * @return a linear IFractalProducer
	 */
	public static IFractalProducer getLinearProducer(ComplexRootedPolynomial cp) {
		return new LinearProducer(cp);
	}
	
	/**
	 * Implementation of the {@linkplain Runnable} interface to be
	 * given to threads, iterating over a segment of the image and
	 * storing the indices needed for drawing into the given array.
	 * 
	 * @author Miroslav Bićanić
	 */
	private static class Calculate implements Runnable {
		/* All the class variables are obvious and explained oin other
		 * places. It would be more redundant to comment them now than not.
		 * Also, they are private.
		 */
		private double reMin;
		private double reMax;
		private double imMin;
		private double imMax;
		private int width;
		private int height;
		private int yStart;
		private int yStop;
		private int m;
		private ComplexRootedPolynomial polynomial;
		private ComplexPolynomial derived;
		private AtomicBoolean cancel;
		/**
		 * The array of indices of this Calculate object, representing
		 * a segment of the full image.
		 */
		private short[] data;
		
		/**
		 * A constructor for a Calculate object.
		 * 
	 * @param reMin the smallest value on the real axis
	 * @param reMax the largest value on the real axis
	 * @param imMin the smallest value on the imaginary axis
	 * @param imMax the largest value on the imaginary axis
	 * @param width the width of the image on the screen
	 * @param height the height of the image on the screen
	 * @param yStart the y coordinate from which to start the iteration
	 * @param yStop the first y coordinate that will not be iterated over
	 * @param m the maximal number of iterations to do before assuming divergence
	 * @param polynomial the polynomial used for the Newton-Raphson iteration
	 * @param derived the derived polynomial used for the Newton-Raphson iteration
	 * @param cancel used to cancel calculation if a new drawing request was 
	 * 			already made and this calculation becomes irrelevant
	 */
		public Calculate(double reMin, double reMax, double imMin, double imMax, 
				int width, int height, int yStart, int yStop, int m, 
				ComplexRootedPolynomial polynomial, ComplexPolynomial derived,
				AtomicBoolean cancel) {
			this.reMin = reMin;
			this.reMax = reMax;
			this.imMin = imMin;
			this.imMax = imMax;
			this.width = width;
			this.height = height;
			this.yStart = yStart;
			this.yStop = yStop;
			this.m = m;
			this.polynomial = polynomial;
			this.derived = derived;
			this.data = new short[width*(yStop-yStart)];
			this.cancel = cancel;
		}

		@Override
		public void run() {
			calculate(reMin, reMax, imMin, imMax, width, height,
					yStart, yStop, m, data, polynomial, derived,
					cancel);
		}
	}
	
	/**
	 * ParallelProducer is an implementation of an {@link IFractalProducer}
	 * producing data for fractal generation using parallelization to speed
	 * up the process of calculation.
	 * 
	 * All worker threads made by this ParallelProducer are daemonic, and
	 * are terminated together with the program.
	 * 
	 * @author Miroslav Bićanić
	 */
	private static class ParallelProducer implements IFractalProducer {
		/** The polynomial to use for the iteration */
		private ComplexRootedPolynomial polynomial;
		/** The derived polynomial to use for the iteration */
		private ComplexPolynomial derived;
		/** A thread pool containing ready threads */
		private ExecutorService pool;
		
		/**
		 * A constructor for a ParallelProducer
		 * @param cp The polynomial to use for the iteration
		 */
		public ParallelProducer(ComplexRootedPolynomial cp) {
			this.polynomial = cp;
			this.derived = cp.toComplexPolynom().derive();
			this.pool = Executors.newFixedThreadPool(
					Runtime.getRuntime().availableProcessors(),
					new DaemonicThreadFactory()
					);
		}

		/**
		 * Produces the convergence indices for every pixel's corresponding
		 * complex number using the Newton-Raphson iteration, with the 
		 * process being parallelized.
		 * 
		 * The resulting array of indices is given to the observer, which
		 * forwards it to the GUI component.
		 */
		@Override
		public void produce(double reMin, double reMax, double imMin, double imMax, 
				int width, int height,
				long requestNo, IFractalResultObserver observer, AtomicBoolean cancel) {
			System.out.println("Starting calculation...");
			int m = polynomial.toComplexPolynom().order() + 1; 
			
			Calculate[] calculators = new Calculate[8*Runtime.getRuntime().availableProcessors()];
			List<Future<?>> results = new ArrayList<>();
			int segmentLength = height/(8*Runtime.getRuntime().availableProcessors());
			int segmentStart = 0;
			for(int i = 0; i < calculators.length; i++) {
				if(i==calculators.length-1) {
					segmentLength = height - i*segmentLength;
				}
				calculators[i]= new Calculate(
						reMin, reMax, imMin, imMax,
						width, height, segmentStart, segmentStart+segmentLength,
						16*16*16,
						polynomial, derived, 
						cancel);
				results.add(pool.submit(calculators[i]));
				segmentStart+=segmentLength;
			}
			
			for(Future<?> f : results) {
				while(true) {
					try {
						f.get();
						break;
					} catch (InterruptedException | ExecutionException ex) {
					}
				}
			}
			
			short[] data = new short[width*height];
			int offset = 0;
			for(Calculate c : calculators) {
				for(int i = 0; i < c.data.length; i++) {
					data[offset++] = c.data[i];
				}
			}
			observer.acceptResult(data, (short)(m), requestNo);
		}
	}
	
	/**
	 * LinearProducer is an implementation of an {@link IFractalProducer}
	 * producing data for fractal generation.
	 * 
	 * @author Miroslav Bićanić
	 */
	private static class LinearProducer implements IFractalProducer{
		/** The polynomial to use for the iteration */
		private ComplexRootedPolynomial polynomial;
		/** The derived polynomial to use for the iteration */
		private ComplexPolynomial derived;
		
		/**
		 * A constructor for a ParallelProducer
		 * @param cp The polynomial to use for the iteration
		 */
		public LinearProducer(ComplexRootedPolynomial cp) {
			this.polynomial = cp;
			this.derived = cp.toComplexPolynom().derive();
		}

		/**
		 * Produces the convergence indices for every pixel's corresponding
		 * complex number using the Newton-Raphson iteration.
		 * 
		 * The resulting array of indices is given to the observer, which
		 * forwards it to the GUI component.
		 */
		@Override
		public void produce(double reMin, double reMax, double imMin, double imMax, int width, int height, long requestNo,
				IFractalResultObserver observer, AtomicBoolean cancel) {
			System.out.println("Starting calculation...");
			int m = polynomial.toComplexPolynom().order() + 1;
			short[] data = new short[width*height];
			calculate(reMin, reMax, imMin, imMax, width, height, 0, height-1, 16*16*16, data, 
					this.polynomial, this.derived, cancel);
			observer.acceptResult(data, (short)(m), requestNo);
		}
	};
}
