package hr.fer.zemris.java.fractals;

import java.util.concurrent.atomic.AtomicBoolean;

import hr.fer.zemris.math.Complex;
import hr.fer.zemris.math.ComplexPolynomial;
import hr.fer.zemris.math.ComplexRootedPolynomial;

/**
 * NewtonUtil is a class containing utility methods and constants
 * used for the Newton-Raphson iteration-based fractal generation.
 * 
 * @author Miroslav Bićanić
 */
public class NewtonUtil {
	/**
	 * The maximal distance between two consecutive complex numbers
	 * in the iteration for us to consider the sequence as converging
	 */
	private static final double CONVERGENCE_THRESHOLD = 0.001;
	/**
	 * The maximal distance between two complex numbers for us to
	 * consider them close enough to be equal
	 */
	private static final double ROOT_THRESHOLD = 0.002;
	
	/**
	 * Maps a screen coordinate given as the (x,y) pair from the range
	 * width x height onto a complex plane from the range
	 * [rMax-rMin] x [iMax-iMin].
	 * 
	 * @param x the x coordinate of the pixel
	 * @param y the y coordinate of the pixel
	 * @param width the width of the image on screen
	 * @param height the height of the image on screen
	 * @param rMin the smallest value on the real axis
	 * @param rMax the largest value on the real axis
	 * @param iMin the smallest value on the imaginary axis
	 * @param iMax the largest value on the imainary axis
	 * @return a complex number corresponding to the (x,y) screen coordinates
	 */
	static Complex mapToComplex(int x, int y, int width, int height, double rMin, double rMax,
			double iMin, double iMax) {
		double fractionX = x / (double)(width-1);
		double fractionY = (height-1-y)/(double)(height-1);
		return new Complex(fractionX*(rMax-rMin)+rMin, fractionY*(iMax-iMin)+iMin);
	}
	
	/**
	 * Iterates over all the pixels in the range [yMax-yMin] x width,
	 * mapping them to the complex plane and performing the Newton-Raphson
	 * iteration for each such complex number.
	 * 
	 * The Newton-Raphson iteration consists of the following:
	 * 	For any complex number Zn and Zm, and a complex polynomial
	 * 	P(z):
	 * 		(1) Zm = Zn
	 * 		(2) Zn = Zn - P(Zn) / P'(Zn)
	 * 		(3) module = |Zm-Zn|
	 * 
	 * This process is repeated as long as the module (3) is larger
	 * than the convergence threshold, or the maximal number of 
	 * iterations has been reached.
	 * 
	 * If Zn doesn't converge to any of the roots, a 0 is placed into
	 * its place in the given {@code data} array. Otherwise, the index
	 * of the root to which it converges is placed in the array, where
	 * the indices are one-based (the first root has index 1).
	 * 
	 * The method enables stopping the calculation if the GUI finds it
	 * unnecessary. If multithreading is used to parallelize the 
	 * calculation, the {@code yMin, yMax} parameters determine the
	 * segment over which a thread could iterate over.
	 * 
	 * @param reMin the smallest value on the real axis
	 * @param reMax the largest value on the real axis
	 * @param imMin the smallest value on the imaginary axis
	 * @param imMax the largest value on the imaginary axis
	 * @param width the width of the image on the screen
	 * @param height the height of the image on the screen
	 * @param yMin the y coordinate from which to start the iteration
	 * @param yMax the first y coordinate that will not be iterated over
	 * @param m the maximal number of iterations to do before assuming divergence
	 * @param data an array storing the indices of roots to which a complex number
	 * 					converged, or 0 if it diverged
	 * @param polynomial the polynomial used for the Newton-Raphson iteration
	 * @param derived the derived polynomial used for the Newton-Raphson iteration
	 * @param cancel used to cancel calculation if a new drawing request was 
	 * 			already made and this calculation becomes irrelevant
	 */
	static void calculate(double reMin, double reMax, double imMin, double imMax, 
			int width, int height, int yMin, int yMax, int m, short[] data,
			ComplexRootedPolynomial polynomial, ComplexPolynomial derived,
			AtomicBoolean cancel) {
		
		int offset = 0;
		for(int y = yMin; y < yMax; y++) {
			if(cancel.get()) {
				return;
			}
			for(int x = 0; x < width; x++) {
				Complex c = mapToComplex(x,y,width,height,reMin,reMax,imMin,imMax);
				Complex zn = c;
				int iter = 0;
				double module;
				do {
					Complex num = polynomial.apply(zn);
					Complex den = derived.apply(zn);
					Complex znold = zn;
					Complex fraction = num.divide(den);
					zn = zn.sub(fraction);
					module = znold.sub(zn).module();
					iter++;
				} while (module > CONVERGENCE_THRESHOLD && iter<m);
				int index = polynomial.indexOfClosestRootFor(zn, ROOT_THRESHOLD);
				data[offset++] = (short)(index+1);
			}
		}
	}
}
