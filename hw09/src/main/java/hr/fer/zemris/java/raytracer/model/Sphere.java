package hr.fer.zemris.java.raytracer.model;

import hr.fer.zemris.java.raytracer.model.GraphicalObject;
import hr.fer.zemris.java.raytracer.model.Point3D;
import hr.fer.zemris.java.raytracer.model.Ray;
import hr.fer.zemris.java.raytracer.model.RayIntersection;

/**
 * Sphere is a {@linkplain GraphicalObject} representing
 * a sphere to be placed in a {@linkplain Scene}.
 * 
 * The sphere is defined geometrically with its center 
 * point, and its radius.
 * 
 * Aesthetically, the sphere is defined with coefficients
 * for diffusing light and for reflecting light, for each
 * of the RGB components individually.
 * Additionally, it must have a shineness factor.
 * 
 * @author Miroslav Bićanić
 */
public class Sphere extends GraphicalObject {
	
	/** Index of the red color component in diffusion and reflection arrays */
	private static final int R = 0;
	/** Index of the green color component in diffusion and reflection arrays */
	private static final int G = 1;
	/** Index of the blue color component in diffusion and reflection arrays */
	private static final int B = 2;

	/** The center of the sphere */
	private Point3D center;
	/** The radius of the sphere */
	private double radius;
	/** An array storing the diffusion coefficients of RGB components */
	private double[] kDiffuse = new double[3];
	/** An array storing the reflection coefficients of RGB components */
	private double[] kReflect = new double[3];
	/** The shineness coefficient of the sphere */
	private double krn;
	
	/**
	 * A constructor for a Sphere.
	 * 
	 * @param center the center point of the sphere
	 * @param radius the radius of the sphere
	 * @param kdr the diffusion coefficient of the red component
	 * @param kdg the diffusion coefficient of green component
	 * @param kdb the diffusion coefficient of blue component
	 * @param krr the reflection coefficient of red component
	 * @param krg the reflection coefficient of green component
	 * @param krb the reflection coefficient of blue component
	 * @param krn the shineness coefficient of the sphere
	 */
	public Sphere(Point3D center, double radius, double kdr, double kdg, 
			double kdb, double krr, double krg, double krb, double krn) {
		this.center = center;
		this.radius = radius;
		this.kDiffuse[R] = kdr;
		this.kDiffuse[G] = kdg;
		this.kDiffuse[B] = kdb;
		this.kReflect[R] = krr;
		this.kReflect[G] = krg;
		this.kReflect[B] = krb;
		this.krn = krn;
	}
	/**
	 * {@inheritDoc}
	 * 
	 */
	@Override
	public RayIntersection findClosestRayIntersection(Ray ray) {
		/*
		 * Setup of variables for easier writing:
		 * x = x0 + a*t --> xRes = x0 + coefA*tRes
		 * y = y0 + a*t --> yRes = y0 + coefB*tRes
		 * z = z0 + c*t --> zRes = z0 + coefC*tRes
		 * 
		 * Used equation for sphere:
		 * (x-p)^2 + (y-q)^2 + (z-w)^2 = r^2
		 * where p,q,w are coordinates of center point of sphere.
		 */
		double coefA = ray.direction.x;
		double x0 = ray.start.x;
		double coefB = ray.direction.y;
		double y0 = ray.start.y;
		double coefC = ray.direction.z;
		double z0 = ray.start.z;
		double p = center.x;
		double q = center.y;
		double w = center.z;
		double r = radius;
	
		/*
		 * plugging x = x0+at, y=y0+bt, z=z0+ct into circle
		 * equation and solving for t gives following equation:
		 *	(a^2+b^2+c^2)*t^2 									--> quadA*t^2
		 * + 2(ax0 - ap + by0 - bq + cz0 - cw)*t 				--> quadB*t
		 * + x0^2+y0^2+z0^2+p^2+q^2+w^2-r^2 - 2(px0+qy0+wz0) 	--> quadC
		 *   = 0
		 * --> at^2 + bt + c = 0
		 * D = b^2 - 4ac
		 */
		double quadA = coefA*coefA + coefB*coefB + coefC*coefC;
		double quadB = 2*(x0*coefA - coefA*p + y0*coefB - coefB*q + z0*coefC - coefC*w);
		double quadC1 = x0*x0+y0*y0+z0*z0+p*p+q*q+w*w - r*r;
		double quadC2 = 2*(x0*p + y0*q + z0*w);
		double quadC = quadC1-quadC2;
		double quadD = quadB*quadB - 4*quadA*quadC;
		
		if(quadD<0) { //solutions must be real
			return null;
		}
		double t1 = (-quadB-Math.sqrt(quadD))/(2*quadA);
		double t2 = (-quadB+Math.sqrt(quadD))/(2*quadA);
		double tRes;
		
		
		
		if(t1<0 && t2<0) {			//the solution must be positive, since the object must be
			return null;			//in front of the person looking
		} else if (t1>0 && t2>0) {
			tRes = t1 < t2 ? t1 : t2;
		} else {
			tRes = t1 < t2 ? t2 : t1;
		}
		double xRes = x0 + tRes*coefA;
		double yRes = y0 + tRes*coefB;
		double zRes = z0 + tRes*coefC;
		Point3D inter = new Point3D(xRes, yRes, zRes);
		double distance = inter.sub(ray.start).norm();
		
		/*
		 * Additional check to see the point was well calculated, by
		 * plugging its coordinates back into the circle equation.
		 */
		double calculatedRadius = Math.pow(xRes-center.x, 2) 
				+ Math.pow(yRes - center.y, 2)
				+ Math.pow(zRes - center.z, 2);
		if(Math.abs(Math.sqrt(calculatedRadius)-radius) > 1e-5) {
			return null;
		}		
		
		return new Intersection(inter, distance, true);
	}

	/**
	 * <p>Instances of this class represent intersections of a ray and some object.</p>
	 * <p>The point where ray and object intersected is given by {@link #point}.</p>
	 * <p>Intersection points can be inner and outer. Consider intersections of a ray
	 * that starts in point (10,0,0) and has directional vector (-1,0,0) and a
	 * sphere with center (0,0,0) and radius 1. Our ray will have two intersections:
	 * in point (1,0,0) ray will enter into the sphere, so this intersection is outer;
	 * it represents a point in which we pass from exterior into the sphere itself.
	 * The other intersection point is (-1,0,0) and that is inner intersection; in that point
	 * our ray passes from sphere interior into external space.</p>
	 * 
	 * @author Miroslav Bićanić
	 */
	private class Intersection extends RayIntersection {

		/**
		 * Constructor for intersection.
		 * 
		 * @param point point of intersection between ray and object
		 * @param distance distance between start of ray and intersection
		 * @param outer specifies if intersection is outer
		 */
		protected Intersection(Point3D point, double distance, boolean outer) {
			super(point, distance, outer);
		}
		
		/**
		 * {@inheritDoc}
		 */
		@Override
		public Point3D getNormal() {
			double x = this.getPoint().x - center.x;
			double y = this.getPoint().y - center.y;
			double z = this.getPoint().z - center.z;
			return new Point3D(x,y,z);
		}
		/**
		 * {@inheritDoc}
		 */
		@Override
		public double getKdr() {
			return kDiffuse[R];
		}
		/**
		 * {@inheritDoc}
		 */
		@Override
		public double getKdg() {
			return kDiffuse[G];
		}
		/**
		 * {@inheritDoc}
		 */
		@Override
		public double getKdb() {
			return kDiffuse[B];
		}
		/**
		 * {@inheritDoc}
		 */
		@Override
		public double getKrr() {
			return kReflect[R];
		}
		/**
		 * {@inheritDoc}
		 */
		@Override
		public double getKrg() {
			return kReflect[G];
		}
		/**
		 * {@inheritDoc}
		 */
		@Override
		public double getKrb() {
			return kReflect[B];
		}
		/**
		 * {@inheritDoc}
		 */
		@Override
		public double getKrn() {
			return krn;
		}
	}
}
