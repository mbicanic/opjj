package hr.fer.zemris.java.raytracer;

import java.util.List;

import hr.fer.zemris.java.raytracer.model.GraphicalObject;
import hr.fer.zemris.java.raytracer.model.LightSource;
import hr.fer.zemris.java.raytracer.model.Point3D;
import hr.fer.zemris.java.raytracer.model.Ray;
import hr.fer.zemris.java.raytracer.model.RayIntersection;
import hr.fer.zemris.java.raytracer.model.Scene;

/**
 * RayCasterUtil is a class providing several utility methods
 * used in the process of ray casting and coloring.
 * 
 * @author Miroslav Bićanić
 */
public class RayCasterUtil {
	
	/**
	 * For the given {@code scene} with its accessible objects, and the
	 * given {@code ray}, calculates the correct color components of that
	 * point of the image.
	 * 
	 * If the given ray doesn't intersect any objects, then it will be 
	 * colored black.
	 * If it intersects an object at some point T, but no light source
	 * reaches that point T, it is colored only with the ambient color.
	 * Otherwise, the color is computed as a linear combination of all
	 * contributing components from all light sources that reach it.
	 * 
	 * @param scene The scene containing objects and light sources
	 * @param ray The ray, cast from the point of perspective towards a point 
	 * 				in the scene
	 * @param rgb The array in which to store the values of RGB components
	 */
	static void tracer(Scene scene, Ray ray, short[] rgb) {
		rgb[0] = 0;
		rgb[1] = 0;
		rgb[2] = 0;
		RayIntersection ri = getClosestIntersection(scene.getObjects(), ray);
		if(ri==null) {
			return;
		}
		determineRgbForIntersection(ri, scene, ray.direction, rgb);
	}
	
	/**
	 * Determines the RGB component for a given intersection point in
	 * the scene by casting rays from each light source, and adding 
	 * their respective contributions to the components, but only if
	 * the ray of light intersects that exact same point in space.
	 * 
	 * @param ri The intersection point of the perspective ray and an object
	 * 				in the scene
	 * @param scene The scene containing objects and light sources
	 * @param eye The vector from the point of view towards the intersection
	 * @param rgb The array in which to store the RGB components
	 */
	static void determineRgbForIntersection(RayIntersection ri, Scene scene, Point3D eye, short[] rgb) {
		rgb[0] = 15;
		rgb[1] = 15;
		rgb[2] = 15;
		for(LightSource ls : scene.getLights()) {
			Point3D light = ls.getPoint().sub(ri.getPoint());
			Point3D normal = ri.getNormal().modifyNormalize();
			
			Ray r = Ray.fromPoints(ls.getPoint(), ri.getPoint());
			RayIntersection rOther = getClosestIntersection(scene.getObjects(), r); //PROVJERA OVOG
			
			if(rOther==null || Math.abs(rOther.getDistance()-light.norm()) > 1e-6) {
				continue;
			}
			
			double difCos = light.normalize().scalarProduct(normal);
			difCos = difCos < 0 ? 0 : difCos;
			double diffR = ls.getR()*ri.getKdr()*difCos;
			double diffG = ls.getG()*ri.getKdg()*difCos;
			double diffB = ls.getB()*ri.getKdb()*difCos;
			
			double coef = 2 * light.scalarProduct(normal);
			Point3D reflected = normal.scalarMultiply(coef).sub(light).modifyNormalize();
			double refCos = reflected.scalarProduct(eye.normalize().negate());
			refCos = refCos < 0 ? 0 : refCos;
			double refR = ls.getR()*ri.getKrr()*Math.pow(refCos, ri.getKrn());
			double refG = ls.getG()*ri.getKrg()*Math.pow(refCos, ri.getKrn());
			double refB = ls.getB()*ri.getKrb()*Math.pow(refCos, ri.getKrn());
			
			rgb[0]+= 1*(diffR+refR);
			rgb[1]+= 1*(diffG+refG);
			rgb[2]+= 1*(diffB+refB);
		}
	}
	
	/**
	 * Finds and returns the intersection point of the given {@code ray}
	 * and the first {@linkplain GraphicalObject} it intersects.
	 * 
	 * @param objects The list of GraphicalObject objects in the scene
	 * @param ray The ray for which to determine the first intersection
	 * @return a RayIntersection representing the point of intersection
	 */
	static RayIntersection getClosestIntersection(List<GraphicalObject> objects, Ray ray) {
		if(objects.size()==0) {
			return null;
		}
		RayIntersection chosenRi = null;
		for(GraphicalObject go : objects) {
			RayIntersection ri = go.findClosestRayIntersection(ray);
			if(chosenRi==null) {
				chosenRi = ri;
			} else {
				if(ri!=null && ri.getDistance() < chosenRi.getDistance()) {
					chosenRi = ri;
				}
			}
		}
		return chosenRi;
	}
}
