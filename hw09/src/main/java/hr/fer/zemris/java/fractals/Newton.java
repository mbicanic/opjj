package hr.fer.zemris.java.fractals;

import static hr.fer.zemris.java.fractals.Producers.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import hr.fer.zemris.java.fractals.viewer.FractalViewer;
import hr.fer.zemris.math.Complex;
import hr.fer.zemris.math.ComplexRootedPolynomial;

/**
 * This program enables the user to enter an arbitrary number
 * of complex numbers representing the roots of a polynomial.
 * 
 * Using the polynomial and the Newton-Raphson iteration we
 * can see if for any given complex number the polynomial
 * value converges to one of the roots, or if it diverges.
 * 
 * Based on its convergence (the index of the root it converged
 * to) or divergence, a fractal image is generated and displayed
 * on screen.
 * 
 * @author Miroslav Bićanić
 */
public class Newton {
	
	/**
	 * The entry point of the program
	 * @param args command-line arguments: not used
	 */
	public static void main(String[] args) {
		System.out.println("Welcome to Newton-Raphson iteration-based fractal viewer.");
		System.out.println("Please enter at least two roots, one root per line.");
		System.out.println("Enter 'done' when done.");
		FractalViewer.show(getParallelProducer(getCPR()));
		//FractalViewer.show(getLinearProducer(getCPR()));
	}
	
	/**
	 * Reads complex numbers from the standard input until the
	 * user writes "done". 
	 * From the roots it builds a {@linkplain ComplexRootedPolynomial}
	 * which is returned.
	 * 
	 * @return A ComplexRootedPolynomial representing the polynomial with
	 * 				the given roots
	 */
	private static ComplexRootedPolynomial getCPR() {
		Complex[] complexRoots;
		try(Scanner sc = new Scanner(System.in)){
			String root;
			List<String> roots = new ArrayList<>();
			while(true) {
				System.out.printf("Root %d> ", roots.size()+1);
				root = sc.nextLine();
				if(root.equalsIgnoreCase("done")) {
					break;
				}
				roots.add(root);
			}
			complexRoots = new Complex[roots.size()];
			int i = 0;
			for(String s : roots) {
				complexRoots[i++] = Complex.parse(s);
			}
		}
		return new ComplexRootedPolynomial(Complex.ONE, complexRoots);
	}
}
