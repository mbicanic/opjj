package hr.fer.zemris.java.fractals;

import java.util.concurrent.ThreadFactory;

/**
 * DaemonicThreadFactory is an implementation of the 
 * {@link ThreadFactory} interface, creating daemonic
 * threads.
 * 
 * @author Miroslav Bićanić
 */
public class DaemonicThreadFactory implements ThreadFactory {
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public Thread newThread(Runnable r) {
		Thread t = new Thread(r);
		t.setDaemon(true);
		return t;
	}
}
