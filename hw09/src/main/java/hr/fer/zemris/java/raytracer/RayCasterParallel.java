package hr.fer.zemris.java.raytracer;

import static hr.fer.zemris.java.raytracer.RayCasterUtil.tracer;

import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.RecursiveAction;
import java.util.concurrent.atomic.AtomicBoolean;

import hr.fer.zemris.java.raytracer.model.GraphicalObject;
import hr.fer.zemris.java.raytracer.model.IRayTracerAnimator;
import hr.fer.zemris.java.raytracer.model.IRayTracerProducer;
import hr.fer.zemris.java.raytracer.model.IRayTracerResultObserver;
import hr.fer.zemris.java.raytracer.model.LightSource;
import hr.fer.zemris.java.raytracer.model.Point3D;
import hr.fer.zemris.java.raytracer.model.Ray;
import hr.fer.zemris.java.raytracer.model.Scene;
import hr.fer.zemris.java.raytracer.viewer.RayTracerViewer;

/**
 * RayCasterParallel is a class demonstrating our implementation
 * of raycasting, parallelizing the calculation of pixel colors
 * using a {@linkplain ForkJoinPool}.
 * 
 * @author Miroslav Bićanić
 */
public class RayCasterParallel {
	
	/**
	 * The entry point of the program
	 * @param args command-line arguments: not used
	 */
	public static void main(String[] args) {
		RayTracerViewer.show(getIRayTracerProducer(),
				getIRayTracerAnimator(), 30,30);
	}
	
	/**
	 * Worker is an implementation of the {@link RecursiveAction}
	 * interface, to be used by the ForkJoinPool for parallelization
	 * of the process for producing RGB components.
	 * 
	 * @author Miroslav Bićanić
	 */
	private static class Worker extends RecursiveAction {
		private static final long serialVersionUID = 1L;
		
		/**
		 * Maximal height of an image segment for the Worker to
		 * produce components for it directly.
		 */
		private static final int MAX_HEIGHT = 30;
		
		/* See constructor for explanation of arguments */
		private Scene scene;
		private Point3D xAxis;
		private Point3D yAxis;
		private Point3D eye;
		private Point3D screenCorner;
		private int yStart;
		private int yStop;
		private int width;
		private double constX;
		private double constY;
		
		/** The array of values of the red color component */
		private short[] red;
		/** The array of values of the green color component */
		private short[] green;
		/** The array of values of the blue color component */
		private short[] blue;
		/** Used to cancel production if the value is true */
		private AtomicBoolean cancel;

		/**
		 * A constructor for a Worker.
		 * 
		 * Parameters {@code constX, constY} are constants used to correctly
		 * move the vector from the top left corner, given by {@code screenCorner},
		 * to an arbitrary position in the projection plane.
		 * 
		 * @param scene The scene in which {@link LightSource} and {@link GraphicalObject} objects
		 * 					are placed
		 * @param xAxis The unit vector determining the direction of the x-axis
		 * @param yAxis The unit vector determining the direction of the y-axis
		 * @param eye The eye-point in the scene (the point of perspective)
		 * @param screenCorner A vector from the eye to the top left screen corner
		 * @param yStart The y pixel coordinate of the first row of pixels for 
		 * 			which to produce components
		 * @param yStop The y pixel coordinate of the first row not included 
		 * 			in component production
		 * @param width The width of the image
		 * @param constX Constant for moving vectors along the x-axis
		 * @param constY Constant for moving vectors along the y-axis
		 * @param cancel used to cancel production if a new drawing request was 
		 * 			already made and this production becomes irrelevant
		 */
		public Worker(Scene scene, Point3D xAxis, Point3D yAxis, Point3D eye, Point3D screenCorner,
				int yStart, int yStop, int width, double constX, double constY, AtomicBoolean cancel) {
			this.scene = scene;
			this.xAxis = xAxis;
			this.yAxis = yAxis;
			this.eye = eye;
			this.screenCorner = screenCorner;
			this.yStart = yStart;
			this.yStop = yStop;
			this.width = width;
			this.constX = constX;
			this.constY = constY;
			this.red = new short[(yStop-yStart)*width];
			this.green = new short[(yStop-yStart)*width];
			this.blue = new short[(yStop-yStart)*width];
			this.cancel = cancel;
		}
		
		/**
		 * Produces the RGB components of each pixel from the segment
		 * specified by this Worker.
		 */
		protected void computeDirect() {
			int offset = 0;
			for(int y=yStart; y<yStop; y++) {
				if(cancel.get()) {
					return;
				}
				for(int x=0; x<width; x++) {
					short[] rgb = new short[3];
					Point3D screenPoint = screenCorner
							.add(xAxis.scalarMultiply(x*constX))
							.sub(yAxis.scalarMultiply(y*constY));
					
					Ray ray = Ray.fromPoints(eye, screenPoint);
					tracer(scene, ray, rgb);

					red[offset] = rgb[0] > 255 ? 255 : rgb[0];
					green[offset] = rgb[1] > 255 ? 255 : rgb[1];
					blue[offset] = rgb[2] > 255 ? 255 : rgb[2];
					offset++;
				}
			}
		}
		
		/**
		 * {@inheritDoc}
		 * 
		 * If the height of the image segment for which this Worker
		 * produces color components is larger than MAX_HEIGHT, then 
		 * this Worker creates two more workers, giving half of the 
		 * segment to one, and half of the segment to the other, 
		 * collecting their data afterwards.
		 */
		@Override
		protected void compute() {
			if(yStop-yStart <= MAX_HEIGHT) {
				computeDirect();
				return;
			}
			int yBetween = (yStart+yStop)/2;
			
			Worker w1 = new Worker(scene, xAxis, yAxis, eye, screenCorner,
					yStart, yBetween, width, constX, constY, cancel);
			Worker w2 = new Worker(scene, xAxis, yAxis, eye, screenCorner,
					yBetween, yStop, width, constX, constY, cancel);
			invokeAll(w1,w2);
			
			int offset = 0;
			for(int i = 0; i < w1.red.length; i++) {
				this.red[offset] = w1.red[i];
				this.blue[offset] = w1.blue[i];
				this.green[offset] = w1.green[i];
				offset++;
			}
			for(int i = 0; i < w2.red.length; i++) {
				this.red[offset] = w2.red[i];
				this.blue[offset] = w2.blue[i];
				this.green[offset] = w2.green[i];
				offset++;
			}
		}
	}
	
	/**
	 * Returns an implementation of the {@link IRayTracerProducer}
	 * interface.
	 * 
	 * @return an IRayTracerProducer
	 */
	private static IRayTracerProducer getIRayTracerProducer() {
		return new IRayTracerProducer() {
			
			/**
			 * Produces the RGB components of each pixel on the image,
			 * iterating through every pixel and casting a ray through
			 * it.
			 * 
			 * The values are produced for multiple segments of the image
			 * at once using parallelization.
			 * 
			 * Once the RGB components of all pixels have been calculated,
			 * the values are sent to the given observer to update the GUI.
			 */
			@Override
			public void produce(Point3D eye, Point3D view, Point3D viewUp, double horizontal, double vertical, int width,
					int height, long requestNo, IRayTracerResultObserver observer, AtomicBoolean cancel) {
				
				System.out.println("Zapocinjem izracune...");
				
				Point3D og = view.sub(eye).normalize();
				Point3D vuv = viewUp.normalize();
				Point3D yAxis = vuv.sub(og.scalarMultiply(og.scalarProduct(vuv))).modifyNormalize();
				Point3D xAxis = og.vectorProduct(yAxis).modifyNormalize();
				Point3D screenCorner = view.
						sub(xAxis.scalarMultiply(horizontal/2)).
						add(yAxis.scalarMultiply(vertical/2));
				Scene scene = RayTracerViewer.createPredefinedScene2();

				double constX = horizontal/(width-1);
				double constY = vertical/(height-1);
				
				ForkJoinPool pool = new ForkJoinPool();
				Worker w = new Worker(scene, xAxis, yAxis, eye, screenCorner, 0, height, width, constX, constY, cancel);
				pool.invoke(w);
				
				System.out.println("Izracuni gotovi...");
				observer.acceptResult(w.red, w.green, w.blue, requestNo);
				System.out.println("Dojava gotova...");	
			}
		};
	}
	
	/**
	 * Factory method returning an implementation of the
	 * {@linkplain IRayTracerAnimator} interface.
	 * 
	 * @return an IRayTracerAnimator
	 */
	private static IRayTracerAnimator getIRayTracerAnimator() {
		return new IRayTracerAnimator() {
			long time;
			
			@Override
			public void update(long deltaTime) {
				time+=deltaTime;
			}
			
			@Override
			public Point3D getViewUp() {
				return new Point3D(0,0,10);
			}
			
			@Override
			public Point3D getView() {
				return new Point3D(-2,0,-0.5);
			}
			
			@Override
			public long getTargetTimeFrameDuration() {
				return 150;
			}
			
			@Override
			public Point3D getEye() {
				double t = (double)time / 10000 * 2 * Math.PI;
				double t2 = (double)time / 5000 * 2 * Math.PI;
				double x = 50*Math.cos(t);
				double y = 50*Math.sin(t);
				double z = 30*Math.sin(t2);
				return new Point3D(x,y,z);
			}
		};
	}
}

