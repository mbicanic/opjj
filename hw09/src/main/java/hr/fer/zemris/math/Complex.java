package hr.fer.zemris.math;

import static java.lang.Math.PI;
import static java.lang.Math.atan2;
import static java.lang.Math.cos;
import static java.lang.Math.pow;
import static java.lang.Math.sin;
import static java.lang.Math.sqrt;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Complex models a complex number in Cartesian coordinates, 
 * providing the user with all the basic operations for working
 * with complex numbers.
 * 
 * Every ComplexNumber is immutable, all changes result in a
 * new ComplexNumber being created.
 * 
 * @author mirob
 *
 */
public class Complex {
	public static final Complex ZERO = new Complex(0,0);
	public static final Complex ONE = new Complex(1,0);
	public static final Complex ONE_NEG = new Complex(-1,0);
	public static final Complex IM = new Complex(0,1);
	public static final Complex IM_NEG = new Complex(0,-1);
	
	/**
	 * The maximal difference between two double values in order
	 * for us to consider them equal.
	 */
	public static final double TOLERANCE = 1e-6;
	
	/** The real part of the complex number. */
	private final double re;
	/** The imaginary part of the complex number. */
	private final double im;
	
	/**
	 * A constructor that takes both the real and imaginary
	 * parameters and creates a new ComplexNumber with the
	 * given parameters.
	 * 
	 * @param real The real part of the new ComplexNumber
	 * @param imaginary The imaginary part of the new ComplexNumber
	 */
	public Complex(double re, double im) {
		this.re= re;
		this.im = im;
	}
	
	/**
	 * Calculates and returns the module of the complex number.
	 * 
	 * @return the module of this complex number.
	 */
	public double module() {
		return sqrt(re*re + im*im);
	}
	
	/**
	 * Multiplies this Complex with the given Complex {@code other}.
	 * Does not modify any of the participating Complex objects.
	 * 
	 * It uses the formula:
	 * (a+bi)*(c+di) = (ac-bd) + (ad+bc)i
	 * 
	 * @param other The complex number with which to multiply
	 * @return a new ComplexNumber that is the result of multiplication.
	 * @throws NullPointerException if the handed Complex is null
	 */
	public Complex multiply(Complex other) {
		Objects.requireNonNull(other);
		double newReal = this.re * other.re - this.im * other.im;
		double newImaginary = this.re*other.im + this.im * other.re;
		return new Complex(newReal, newImaginary);
	}

	/**
	 * Divides this Complex by the given Complex {@code other}.
	 * Does not modify any of the participating Complex objects.
	 * 
	 * It uses the formula:
	 * z/w = (z * w~) / Re(w)
	 * for z, w, w~ complex numbers, w~ being the complex conjugate of w.
	 * 
	 * @param other The complex number with which to divide this complex number
	 * @return The ratio of this and other complex number
	 * @throws NullPointerException if the handed Complex is null
	 */
	public Complex divide(Complex other) {
		Objects.requireNonNull(other);
		
		double denominator = other.multiply(other.conjugate()).getReal();
		
		Complex interResult = this.multiply(other.conjugate());
		Complex result = new Complex(interResult.re/denominator, 
				interResult.im/denominator);
		return result;
	}
	/**
	 * Adds this Complex to the given Comples {@code other}.
	 * Does not modify any of the participating Complex objects.
	 *
	 * Using: (a+bi)+(c+di) = (a+c)+(b+d)i
	 * 
	 * @param other The complex number to add to this one
	 * @return a new Complex that is the result of addition.
	 * @throws NullPointerException if the handed Complex is null
	 */
	public Complex add(Complex other) {
		Objects.requireNonNull(other);
		return new Complex(this.re + other.re,
				this.im + other.im);
	}
	
	/**
	 * Subtracts given Complex {@code other} from this Complex.
	 * Does not modify any of the participating Complex objects.
	 * 
	 * Using: (a+bi)-(c+di) = (a-c)+(b-d)i
	 *
	 * @param other The complex number to subtract from this one
	 * @return a new Complex that is the result of subtraction.
	 * @throws NullPointerException if the handed Complex is null
	 */
	public Complex sub(Complex other) {
		Objects.requireNonNull(other);
		return new Complex(this.re-other.re,
				this.im - other.im);
	}
	
	/**
	 * Creates a new Complex with real and imaginary values
	 * opposite than those of this Complex.
	 * @return a Complex that is this Complex negated
	 */
	public Complex negate() {
		return new Complex(-this.re, -this.im);
	}

	/**
	 * Returns a complex conjugate of this Complex using the formula:
	 * (a + bi)~ = (a - bi).
	 * 
	 * @return a new Complex being the complex conjugate of this Complex
	 */
	public Complex conjugate() {
		return new Complex(this.re, -1 * this.im);
	}

	/**
	 * This method returns the real part of this ComplexNumber.
	 * @return the real part of the complex number
	 */
	public double getReal() {
		return this.re;
	}
	/**
	 * This method returns the imaginary part of this ComplexNumber.
	 * @return the imaginary part of this complex number.
	 */
	public double getImaginary() {
		return this.im;
	}
	
	/**
	 * This method calculates and returns a new Complex equal to this
	 * Complex raised to the nth power.
	 * 
	 * Using: mag(z)^n * (cos(ang(z)*n) + i*sin(ang(z)*n)
	 * For: 	z 	:= complex number 
	 * 		mag(z) 	:= magnitude
	 * 		ang(z) 	:= angle
	 * 
	 * @param n The power to which this complex number is raised
	 * @return A new complex number equal to this to the nth power
	 * @throws IllegalArgumentException if the power is negative,
	 * 			or both power and this complex number are zero.
	 */
	public Complex power(int n) {
		if(n<0) {
			throw new IllegalArgumentException("Cannot calculate negative powers for complex numbers.");
		}
		if(n==0 && this.re==0 && this.im == 0) {
			throw new IllegalArgumentException("Cannot calculate 0^0.");
		}
		double magnitude = pow(this.module(),n);
		double angle = this.getAngle();
		return Complex.fromMagnitudeAndAngle(magnitude, n*angle);
	}
	
	/**
	 * This method calculates and returns all the nth complex roots
	 * of this Complex in a Complex array.
	 * 
	 * Using: sqrt(mag(z)) * e^(j*(ang(z)+2k*pi)/n)
	 * For: 	k 	:= 0...n-1
	 * 			z	:= complex number
	 * 		ang(z)	:= angle
	 * 		mag(z) 	:= magnitude
	 * 
	 * @param n The root which should be calculated
	 * @return An array of complex numbers containing all {@code n}
	 * 			complex roots of this Complex
	 * @throws IllegalArgumentException if given {@code n} is negative or 0
	 */
	public List<Complex> root(int n) {
		List<Complex> roots = new ArrayList<Complex>();
		if(n<=0) {
			throw new IllegalArgumentException("Cannot calculate non-positive roots for complex numbers.");
		}
		if(this.re==0 && this.im==0) {
			roots.add(ZERO);
			return roots;
		}
		double magnitude = pow(this.module(), 1.0/n);
		double angle = this.getAngle();
		
		for(int k = 0; k < n; k++) {
			double newAngle = (angle + 2*k*PI) / n;
			roots.add(fromMagnitudeAndAngle(magnitude, newAngle));
		}
		return roots;
	}
	
	/**
	 * Returns a full record of this Complex with both real
	 * and imaginary parts written, even if zero.
	 * 
	 * Format: [real][+|-]i[imaginary]
	 */
	@Override
	public String toString() {
		String sign = this.im>=0 ? "+" : "-";
		StringBuilder sb = new StringBuilder(20);
		sb.append("(").append(this.re).append(sign).append("i").append(Math.abs(this.im)).append(")");
		return sb.toString();
	}
	
	/**
	 * Returns a string representation of this Complex without
	 * redundant information.
	 * 
	 * Format: [real][+|-]i[imaginary].
	 * @return the complex number written as a string
	 */
	public String conciseToString() {
		StringBuilder sb = new StringBuilder(20);
		if(this.re==0 && this.im==0) {
			sb.append("0");
			return sb.toString();
		}
		if(this.re!=0) {
			sb.append(this.re);
		}
		if(this.im!=0) {
			if(this.re!=0 && this.im>0) {
				sb.append("+");
			}
			if(Math.abs(this.im)==1) {
				sb.append(this.im<0 ? "-" : "");
				sb.append("i");
				return sb.toString();
			}
			sb.append(this.im);
			sb.append("i");
		}
		return sb.toString();
	}
	
	/**
	 * This method returns the angle of this ComplexNumber in
	 * polar coordinates, normalised to values from [0,2pi>.

	 * @return the angle of this complex number from the range [0,2pi>
	 */
	public double getAngle() {
		return normaliseAngleTo2Pi(atan2(im, re));
	}
	
	/**
	 * This method takes any angle and returns the same angle,
	 * normalised to the range of [0,2pi> radians.
	 * 
	 * @param angle The angle to be normalised
	 * @return The normalised angle from the [0,2pi> range
	 */
	private static double normaliseAngleTo2Pi(double angle) {
		while(angle >= 2*Math.PI) {
			angle-= 2*Math.PI;
		}
		while(angle < 0) {
			angle+= 2*Math.PI;
		}
		if(Math.abs(angle - 2*Math.PI) < TOLERANCE) {
			angle = 0;
		}
		return angle;
	}
	
	/**
	 * A factory method that creates a new ComplexNumber
	 * only from the real part, setting the imaginary to 0.
	 * 
	 * @param real The real part of the new ComplexNumber
	 * @return The new ComplexNumber with the given real part.
	 */
	public static Complex fromReal(double real) {
		return new Complex(real, 0);
	}
	/**
	 * A factory method that creates a new ComplexNumber
	 * only from the imaginary part, setting the real to 0.
	 * 
	 * @param imaginary The imaginary part of the new ComplexNumber
	 * @return The new ComplexNumber with the given imaginary part.
	 */
	public static Complex fromImaginary(double imaginary) {
		return new Complex(0, imaginary);
	}
	/**
	 * A factory method that computes Cartesian coordinates from
	 * given polar coordinates and creates a new ComplexNumber
	 * from them.
	 * 
	 * @param magnitude The magnitude of the complex number
	 * @param angle	The angle of the complex number
	 * @return	The new ComplexNumber created from the given parameters
	 */
	public static Complex fromMagnitudeAndAngle(double magnitude, double angle) {
		return new Complex(magnitude*cos(angle), magnitude*sin(angle));
	}
	
	/**
	 * Parses the given string as a complex number.
	 * 
	 * The parser accepts all formats which are in compliance with the
	 * following rules:<br>
	 * 1) The parser accepts a redundant '+' sign in front of a number,
	 * 		interpreting the following number as positive, but only if
	 * 		it is the first number in sequence (see point 2).
	 * 2) The parser DOES NOT accept sequences (length > 1) of symbols:
	 * 		'++', '--', '+.' are not allowed. "2+-i" is thus not correct.
	 * 3) If there is an imaginary part, the imaginary unit 'i' HAS TO be
	 * 		on the first position of the imaginary part:
	 * 		'2+4i', '-2i', '+11i' are not allowed.
	 * 4) If there are both parameters in the string, they HAVE TO be
	 * 		separated with a single '+' or '-'.
	 * 
	 * Accepted formats are ([number] = any decimal or whole number):
	 *  [number], +[number], -[number] 		-> new ComplexNumber(+-number, 0)
	 *  i, +i, -i							-> new ComplexNumber(0, +-1)
	 *  [number]i, +[number]i, -[number]i	-> new ComplexNumber(0, +-number)
	 *  [number]+i							-> new ComplexNumber(+-number, +-1)
	 * +[number]+i
	 * -[number]+i
	 *  [number]-i
	 * +[number]-i
	 * -[number]-i
	 *  [number1]+i[number2]				-> new ComplexNumber(+-number1, +-number2)
	 * +[number1]+i[number2]	
	 * -[number1]+i[number2]	
	 *  [number1]-i[number2]	
	 * +[number1]-i[number2]	
	 * -[number1]-i[number2]				-> {@link #ComplexNumber(double, double)}
	 * 
	 * @param givenString The string to parse into a complex number.
	 * @return A Complex built from the data in the string
	 * @throws NullPointerException if the handed string is null
	 * @throws IllegalArgumentException if the string is empty
	 * @throws NumberFormatException 
	 * 			-if {@link Double#parseDouble()} can't parse either
	 * 				the real or imaginary part.
	 * 			-if both real and imaginary part are present, but not
	 * 				separated by '+' or '-'
	 * 			-if the imaginary part does not begin with an 'i'
	 */
	public static Complex parse(String givenString) {
		Objects.requireNonNull(givenString);
		String s = givenString.trim();
		if(s.length()==0) {
			throw new IllegalArgumentException("Cannot parse empty string.");
		}
		
		int signum = 1;
		if(s.charAt(0)=='+' || s.charAt(0)=='-') {
			if(s.charAt(0)=='-') {
				signum = -1;
			}
			s = s.substring(1);
		}
		   
		if(s.charAt(0)=='i') {
			if(s.length()>1) {
				s = s.substring(1);
				return new Complex(0, signum*Double.parseDouble(s));
			}
			return new Complex(0, signum);
		}
		
		String arg1 = parseFirstPotentialDouble(s);
		double arg;
		try {
			arg = Double.parseDouble(arg1);
		} catch (NumberFormatException ex) {
			throw new NumberFormatException("String '"+arg1+"' can not be parsed as a double.");
		}
		
		if(arg1.length()==s.length()) {
			return new Complex(signum*arg, 0);	
			
		} else if(arg1.length()==s.length()-1 && s.charAt(s.length()-1)=='i') {
			return new Complex(0, signum*arg);
		}
		
		double real = arg*signum;
		s = s.substring(arg1.length()).trim();
		
		if(s.charAt(0)=='+') {
			signum = 1;
		} else if (s.charAt(0)=='-') {
			signum = -1;
		} else {
			throw new NumberFormatException("Real and imaginary part must be separated by '+' or '-'.");
		}
		s = s.substring(1).trim();
		
		if(s.charAt(0)!='i') {
			throw new NumberFormatException("The imaginary part must begin with 'i'.");
		}
		s = s.substring(1);
		if(s.length()==0) {
			return new Complex(real, signum);
		} else {
			return new Complex(real, signum*Double.parseDouble(s));
		}
	}

	/**
	 * This function takes a string and returns the substring from
	 * position 0 to the position of the first character that is 
	 * not a digit [0-9], allowing at most one dot (.) in the substring.
	 * 
	 * This method does not guarantee the number is parsable as double - 
	 * it can return an empty string. The final check is done by
	 * {@link Double#parseDouble(String)}.
	 * 
	 * @param s The received string which should start with a double number
	 * @return	The substring containing a potential double number
	 */
	private static String parseFirstPotentialDouble(String s) {
		int dotCounter=0;
		int indexOfNonDigit = s.length();
		String val;
		for(int i = 0; i < s.length(); i++) {
			char sign = s.charAt(i);
			if(!Character.isDigit(sign) && sign!='.') {
				indexOfNonDigit=i;
				break;
			}
			if(sign=='.') {
				dotCounter++;
			}
			if(dotCounter>1) {
				indexOfNonDigit=0;
				break;
			}
		}
		val = s.substring(0,indexOfNonDigit);
		return val;
	}
}
