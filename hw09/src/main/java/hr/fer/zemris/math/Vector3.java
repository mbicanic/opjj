package hr.fer.zemris.math;

/**
 * Vector3 models both vectors and points in a 3D Cartesian
 * coordinate system (XYZ).
 * 
 * All Vector3 objects are immutable and every supported
 * vector operation returns a new Vector3 object as a 
 * result of the operation.
 * 
 * @author Miroslav Bićanić
 */
public class Vector3 {
	/** The x coordinate of the vector */
	private double x;
	/** The y coordinate of the vector */
	private double y;
	/** The z coordinate of the vector */
	private double z;
	
	/**
	 * A constructor for a Vector3
	 * @param x the x coordinate of the vector
	 * @param y the y coordinate of the vector
	 * @param z the z coordinate of the vector
	 */
	public Vector3(double x, double y, double z) {
		this.x = x;
		this.y = y;
		this.z = z;
	}
	
	/**
	 * Calculates the length of the vector using:
	 * {@code |v| = sqrt(x^2 + y^2 + z^2)}
	 * 
	 * @return The length of this vector
	 */
	public double norm() {
		return Math.sqrt(x*x+y*y+z*z);
	}
	
	/**
	 * Retuns a normalized Vector3 (having length = 1) pointing in 
	 * the same direction as this Vector3.
	 * 
	 * @return normalized Vector3
	 */
	public Vector3 normalized() {
		double norm = norm();
		return new Vector3(this.x/norm, this.y/norm, this.z/norm);
	}
	
	/**
	 * Adds the given Vector3 {@code other} to this Vector3, returning
	 * a new Vector3 as a result.
	 * 
	 * @param other the vector to add to this vector
	 * @return the sum of {@code this} and {@code other}.
	 */
	public Vector3 add(Vector3 other) {
		return new Vector3(this.x+other.x, this.y+other.y, this.z+other.z);
	}
	
	/**
	 * Subtracts the given Vector3 {@code other} from this Vector3, 
	 * returning a new Vector3 as a result.
	 * 
	 * @param other the vector to subtract from this vector
	 * @return the difference of {@code this} and {@code other}.
	 */
	public Vector3 sub(Vector3 other) {
		return new Vector3(this.x-other.x, this.y-other.y, this.y-other.z);
	}
	
	/**
	 * Calculates and returns the dot-product (scalar product) of
	 * this vector and the given vector {@code other}.
	 * 
	 * @param other the vector with which to perform the dot-product operation
	 * @return the dot-product of vectors {@code this} and {@code other}
	 */
	public double dot(Vector3 other) {
		return this.x*other.x + this.y*other.y + this.z*other.z;
	}
	
	/**
	 * Computes and returns the cross-product (vector product) of
	 * this vector and the given vector {@code other}, returning
	 * a new Vector3 as the result.
	 * 
	 * @param other the vector with which to perform the cross-product operation
	 * @return the cross-product of vectors {@code this} and {@code other}
	 */
	public Vector3 cross(Vector3 other) {
		double newX = this.y*other.z - this.z*other.y;
		double newY = this.z*other.x - this.x*other.z;
		double newZ = this.x*other.y - this.y*other.x;
		return new Vector3(newX, newY, newZ);
	}
	
	/**
	 * Scales this vector for the given {@code factor}, effectively
	 * multiplying all components with the given factor, and 
	 * returning a new Vector3 as the result.
	 * 
	 * @param factor the factor with which to scale this vector
	 * @return the scaled vector
	 */
	public Vector3 scale(double factor) {
		return new Vector3(this.x*factor, this.y*factor, this.z*factor);
	}
	
	/**
	 * Returns the value of the cosine of the angle between this
	 * vector and the given vector {@code other}, using:
	 * {@code cos(a) = this*other / (|this|*|other|)}
	 * 
	 * @param other the vector closing an angle with this vector
	 * @return the cosine value of the angle between the two vectors
	 */
	public double cosAngle(Vector3 other) {
		return this.dot(other) / (this.norm() * other.norm());
	}

	/**
	 * @return the x component of this vector
	 */
	public double getX() {
		return x;
	}
	/**
	 * @return the y component of this vector
	 */
	public double getY() {
		return y;
	}
	/**
	 * @return the z component of this vector
	 */
	public double getZ() {
		return z;
	}
	
	/**
	 * Returns the x, y and z components of this vector
	 * in an array.
	 * 
	 * @return an array containing the x, y and z components
	 * 			of this vector
	 */
	public double[] toArray() {
		return new double[] {this.x, this.y, this.z};
	}
	
	/**
	 * Format: (x, y, z)
	 */
	public String toString() {
		return String.format("(%f, %f, %f)", this.x, this.y, this.z);
	}
	
	/**
	 * Demonstration program for Vector3, showing some of
	 * its operations.
	 * 
	 * @param args not used
	 */
	public static void main(String[] args) {
		/*
		 * Left this here because it will probably be involved
		 * in reviewing, and no point for a separate class.
		 */
		Vector3 i = new Vector3(1,0,0);
		Vector3 j = new Vector3(0,1,0);
		Vector3 k = i.cross(j);
		Vector3 l = k.add(j).scale(5);
		Vector3 m = l.normalized();
		System.out.println(i);
		System.out.println(j);
		System.out.println(k);
		System.out.println(l);
		System.out.println(l.norm());
		System.out.println(m);
		System.out.println(l.dot(j));
		System.out.println(i.add(new Vector3(0,1,0)).cosAngle(l));
	}
}
