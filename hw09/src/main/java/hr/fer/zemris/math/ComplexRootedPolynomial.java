package hr.fer.zemris.math;

/**
 * ComplexRootedPolynomial represents a polynomial equation over
 * the complex number domain, where all roots of the polynomial
 * are known prior to construction.
 * 
 * It represents polynomials in the form:
 *		 {@code z0(z-z1)(z-z2)...(z-zn) }
 * where z0 is a constant, and complex numbers z1 to zn represent
 * the roots of the polynomial. Plugging them into the polynomial
 * equation gives a result of 0.
 * 
 * @author Miroslav Bićanić
 */
public class ComplexRootedPolynomial {
	/** The roots of this ComplexRootedPolynomial */
	private Complex[] roots;
	/** The constant multiplying the whole polynomial */
	private Complex constant;
	
	/**
	 * A constructor for a ComplexRootedPolynomial
	 * @param constant the constant multiplying the polynomial
	 * @param roots the roots of the polynomial
	 */
	public ComplexRootedPolynomial(Complex constant, Complex...roots) {
		this.roots=roots;
		this.constant=constant;
	}
	
	/**
	 * Calculates the value of the polynomial for a given complex
	 * number {@code z}.
	 * 
	 * @param z the complex number for which to calculate the value of 
	 * 				the polynomial
	 * @return the complex number being the value of the polynomial at
	 * 				the given complex number {@code z}
	 */
	public Complex apply(Complex z) {
		Complex result = constant;
		for(Complex root : roots) {
			result = result.multiply(z.sub(root));
		}
		return result;
	}
	
	/**
	 * Converts this ComplexRootedPolynomial from the rooted form
	 * into a {@link ComplexPolynomial}.
	 * 
	 * @return A ComplexPolynomial equal to this ComplexRootedPolynomial
	 */
	public ComplexPolynomial toComplexPolynom() {
		Complex[] factors = new Complex[roots.length+1];
		for(int i = 0; i<factors.length; i++) {
			factors[i] = Complex.ZERO;
		}
		factors[0] = roots[0].negate();
		factors[1] = Complex.ONE;
		
		/*
		 * Starting from the first bracket (z-z1), the values next to powers 
		 * are:
		 * 	0: -z1
		 * 	1: 1
		 * The algorithm iteratively multiplies the values with each succeeding
		 * bracket (z-zn), doing so in two parts: first with the constant of
		 * the next bracket, then with the variable. The two parts are then added
		 * and store the regular polynomial form of the brackets processed so far.
		 */
		for(int i = 1; i < roots.length; i++) {
			Complex[] mulWithVariable = new Complex[factors.length+1];
			mulWithVariable[0] = Complex.ZERO;
			Complex[] mulWithConstant = new Complex[factors.length];
			for(int j = 0; j < factors.length; j++) {
				mulWithConstant[j] = factors[j].multiply(roots[i].negate());
				mulWithVariable[j+1] = factors[j];
				factors[j] = mulWithConstant[j].add(mulWithVariable[j]);
			}
		}
		for(int i = 0; i<factors.length; i++) {
			factors[i] = factors[i].multiply(constant);
		}
		return new ComplexPolynomial(factors);
	}
	
	/**
	 * Returns a string representation of this polynomial in the form
	 * (constant)*(z-(roots[0]))(z-(roots[1]))...(z-(roots[n]))
	 */
	public String toString() {
		StringBuilder sb = new StringBuilder(50);
		for(int i = 0; i < roots.length; i++) {
			sb.append("(z-").append(roots[i].toString()).append(")");
			sb.append(i==roots.length-1 ? "" : "*");
		}
		return String.format("%s * %s", constant.toString(), sb.toString());
	}
	
	/**
	 * Returns the index of the polynomial root whose distance
	 * from the given complex number {@code z} is less than the
	 * given {@code threshold}. 
	 * 
	 * If no root is close enough, it returns -1.
	 * 
	 * @param z the complex number with which to compare the roots
	 * @param threshold the maximal tolerable distance between a root
	 * 			and {@code z} to consider them close enough
	 * @return the index of the closest root if it exists; -1 otherwise
	 */
	public int indexOfClosestRootFor(Complex z, double threshold) {
		int closest = -1;
		double distance = threshold;
		for(int i=0; i<roots.length; i++) {
			if(roots[i].sub(z).module()<distance) {
				distance = roots[i].sub(z).module();
				closest = i;
			}
		}
		return closest;
	}
}
