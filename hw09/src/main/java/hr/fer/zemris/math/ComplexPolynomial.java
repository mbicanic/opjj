package hr.fer.zemris.math;

/**
 * ComplexPolynomial represents a polynomial equation over
 * the complex number domain with unknown roots.
 * 
 * The polynomial is in the form:
 * {@code Zn*Z^n + Zn-1 * Z^(n-1) +...+ Z1*Z + Z0}
 * 
 * @author Miroslav Bićanić
 */
public class ComplexPolynomial {
	/** The factors Z0 to Zn of this polynomial */
	private Complex[] factors;
	
	/**
	 * A constructor for a ComplexPolynomial
	 * @param factors the constant factors next to powers of Z, where
	 * 			the first given factor is Z0, the next one is Z1 and
	 * 			so on.
	 */
	public ComplexPolynomial(Complex...factors) {
		this.factors = factors;
	}
	
	/**
	 * Returns the order of this polynomial, e.g. the
	 * highest power of the variable Z
	 * 
	 * @return the order of this polynomial
	 */
	public short order() {
		return (short)(factors.length-1);
	}
	
	public static void main(String[] args) {
		ComplexPolynomial cp1 = new ComplexPolynomial(
				Complex.ONE,
				Complex.ZERO,
				Complex.ONE);
		ComplexPolynomial cp2 = new ComplexPolynomial(
				Complex.ONE_NEG,
				Complex.ZERO,
				Complex.ZERO,
				Complex.ONE);
		ComplexPolynomial cp3 = cp1.multiply(cp2);
		System.out.println(cp3);
	}
	
	/**
	 * Multiplies this polynomial with the given ComplexPolynomial
	 * {@code other}, and returns a new ComplexPolynomial as the 
	 * result.
	 * 
	 * @param other the polynomial with which to multiply this polynomial
	 * @return a polynomial representing the product of the two polynomials
	 */
	public ComplexPolynomial multiply(ComplexPolynomial other) {
		Complex[] f = new Complex[this.order()+other.order()+1];
		for(int i = 0; i < f.length; i++) {
			f[i] = Complex.ZERO;
		}
		for(int i = 0; i < this.factors.length; i++) {
			for(int j = 0; j < other.factors.length; j++) {
				f[i+j] = f[i+j].add(this.factors[i].multiply(other.factors[j]));
			}
		}
		return new ComplexPolynomial(f);
	}
	
	/**
	 * Calculates the value of the polynomial for a given complex
	 * number {@code z}.
	 * 
	 * @param z the complex number for which to calculate the value of 
	 * 				the polynomial
	 * @return the complex number being the value of the polynomial at
	 * 				the given complex number {@code z}
	 */
	public Complex apply(Complex z) {
		Complex result = Complex.ZERO;
		for(int i = 0; i < factors.length; i++) {
			result = result.add(z.power(i).multiply(factors[i]));
		}
		return result;
	}
	
	/**
	 * Calculates the first derivation of this ComplexPolynomial
	 * and returns it as a new ComplexPolynomial.
	 * 
	 * @return the first derivation of this polynomial
	 */
	public ComplexPolynomial derive() {
		Complex[] newFactors = new Complex[factors.length-1];
		for(int i = 0; i < newFactors.length; i++) {
			Complex f = factors[i+1].multiply(new Complex(i+1, 0));
			newFactors[i] = f;
		}
		return new ComplexPolynomial(newFactors);
	}
	
	/**
	 * Format: (zn)*z^n + (zn-1)*z^n-1 + ... + (z1)*z + (z0)
	 */
	public String toString() {
		StringBuilder sb = new StringBuilder(100);
		for(int i = factors.length-1; i>=0; i--) {
			sb.append(factors[i]);
			if(i!=0) {
				sb.append("*z^").append(i);
				sb.append(" + ");
			}
		}
		return sb.toString();
	}
	
}
