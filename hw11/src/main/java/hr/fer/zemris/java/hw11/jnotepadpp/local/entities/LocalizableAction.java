package hr.fer.zemris.java.hw11.jnotepadpp.local.entities;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.KeyStroke;

import hr.fer.zemris.java.hw11.jnotepadpp.IconRetriever;
import hr.fer.zemris.java.hw11.jnotepadpp.local.FormLocalizationProvider;
import hr.fer.zemris.java.hw11.jnotepadpp.local.ILocalizationProvider;

/**
 * LocalizableAction is a localized specification of {@link AbstractAction},
 * changing its parameters dynamically as the localization changes.
 * 
 * @author Miroslav Bićanić
 */
public abstract class LocalizableAction extends AbstractAction {
	private static final long serialVersionUID = 1L;
	/** An IconRetriever used for fetching icons to be associated with actions */
	private static final IconRetriever IR = IconRetriever.getInstance();
	
	/**
	 * A constructor for a LocalizableAction with an icon, an accelerator key combination
	 * and a mnemonic.
	 * 
	 * @param key the key with which the action name is associated
	 * @param lp a {@link ILocalizationProvider} used for translation
	 * @param iconKey a key for fetching the correct icon for the action
	 * @param keyStroke a {@link KeyStroke} representing the {@link Action#ACCELERATOR_KEY}
	 * @param mnemonic a number representing one of the keyboard keys used as the mnemonic
	 */
	public LocalizableAction(String key, ILocalizationProvider lp, String iconKey, KeyStroke keyStroke, int mnemonic) {
		putValue(NAME, lp.getString(key));
		putValue(SHORT_DESCRIPTION, lp.getString(key+"_d"));
		putValue(Action.ACCELERATOR_KEY, keyStroke);
		putValue(Action.MNEMONIC_KEY, mnemonic);
		putValue(Action.SMALL_ICON, IR.fetch(iconKey));
		lp.addLocalizationListener(()->{
			putValue(NAME, lp.getString(key));
			putValue(SHORT_DESCRIPTION, lp.getString(key+"_d"));
		});
	}

	/**
	 * A constructor for a LocalizableAction
	 * @param key the key with which the action name is associated
	 * @param lp a {@link ILocalizationProvider} used for translation
	 */
	public LocalizableAction(String key, FormLocalizationProvider lp) {
		putValue(NAME, lp.getString(key));
		putValue(SHORT_DESCRIPTION, lp.getString(key+"_d"));
		lp.addLocalizationListener(()->{
			putValue(NAME, lp.getString(key));
			putValue(SHORT_DESCRIPTION, lp.getString(key+"_d"));
		});
	}
}
