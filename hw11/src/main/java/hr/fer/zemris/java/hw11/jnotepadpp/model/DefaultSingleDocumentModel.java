package hr.fer.zemris.java.hw11.jnotepadpp.model;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.swing.JTextArea;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import hr.fer.zemris.java.hw11.jnotepadpp.JNotepadPP;

/**
 * Implementation of a {@link SingleDocumentModel} for the 
 * {@link JNotepadPP}
 * 
 * @author Miroslav Bićanić
 */
public class DefaultSingleDocumentModel implements SingleDocumentModel {

	/** The JTextArea of this model */
	private JTextArea jta;
	/** The path to the file associated with this model */
	private Path filePath;
	/** A flag indicating if the model has been modified */
	private boolean modified;
	
	/** A list of listeners registered to this model */
	private List<SingleDocumentListener> listeners;
	
	/**
	 * A constructor building a {@code DefaultSingleDocumentModel} from
	 * the given {@code filePath} (can be null) and initializing its text
	 * to the given {@code text}.
	 * 
	 * @param filePath path to file associated with the document; can be null
	 * @param text the text with which to initialize the document
	 */
	public DefaultSingleDocumentModel(Path filePath, String text) {
		jta = new JTextArea(text);
		jta.getDocument().addDocumentListener(new DocumentListener() {
			@Override
			public void removeUpdate(DocumentEvent e) {
				if(modified==true) {
					return;
				}
				modified=true;
				signalStatus();
			}
			@Override
			public void insertUpdate(DocumentEvent e) {
				if(modified==true) {
					return;
				}
				modified=true;
				signalStatus();
			}
			@Override
			public void changedUpdate(DocumentEvent e) {
				if(modified==true) {
					return;
				}
				modified=true;
				signalStatus();
			}
		});
		this.filePath = filePath==null ? filePath : filePath.toAbsolutePath();
		this.listeners = new ArrayList<SingleDocumentListener>();
		this.modified = false;
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public JTextArea getTextComponent() {
		return jta;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Path getFilePath() {
		return filePath;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setFilePath(Path path) {
		this.filePath = path;
		for(SingleDocumentListener l : listeners) {
			l.documentFilePathUpdated(this);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isModified() {
		return modified;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setModified(boolean modified) {
		this.modified = modified;
		signalStatus();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void addSingleDocumentListener(SingleDocumentListener l) {
		listeners.add(l);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void removeSingleDocumentListener(SingleDocumentListener l) {
		listeners.add(l);
	}
	
	/**
	 * Signals all listeners of this model that the modification
	 * flag has changed.
	 */
	protected void signalStatus() {
		for(SingleDocumentListener l : listeners) {
			l.documentModifyStatusUpdated(this);
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * Two {@link DefaultSingleDocumentModel} objects are equal if their
	 * {@link Path} objects are equal, unless both have their {@link Path}
	 * set to null, in which case the {@link JTextArea} components must
	 * be equal.
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof DefaultSingleDocumentModel))
			return false;
		DefaultSingleDocumentModel other = (DefaultSingleDocumentModel) obj;
		if(filePath==null && other.filePath==null) {
			return jta==other.jta; //differentiating unnamed files
		}
		return Objects.equals(filePath, other.filePath);
	}

	@Override
	public int hashCode() {
		return Objects.hash(filePath);
	}
}
