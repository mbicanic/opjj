package hr.fer.zemris.java.hw11.jnotepadpp.model;

import java.nio.file.Path;

import javax.swing.JTextArea;

import hr.fer.zemris.java.hw11.jnotepadpp.JNotepadPP;

/**
 * SingleDocumentModel is an interface for a model of one
 * document in {@link JNotepadPP}.
 * 
 * The model is a subject in the observer design pattern,
 * capable of notifying listeners on change.
 * 
 * @author Miroslav Bićanić
 */
public interface SingleDocumentModel {
	
	/**
	 * Returns the {@link JTextArea} used by the model for storing
	 * and displaying text.
	 * 
	 * @return {@link JTextArea} used by the model
	 */
	JTextArea getTextComponent();
	
	/**
	 * Returns the {@link Path} associated with the model.
	 *
	 * @return {@link Path} associated with the model
	 */
	Path getFilePath();
	
	/**
	 * Sets the {@link Path} of the model to the given {@code path}
	 * 
	 * @param path a {@link Path} representing the desired path for the model
	 */
	void setFilePath(Path path);
	
	/**
	 * Tells if the model is modified.
	 * 
	 * @return true if the model is modified; false otherwise
	 */
	boolean isModified();
	
	/**
	 * Sets the model's modification flag to the given
	 * {@code modified}.
	 * 
	 * @param modified the value to set for the model's modification flag
	 */
	void setModified(boolean modified);
	
	/**
	 * Registers a {@link SingleDocumentListener} to the model 
	 * @param l the {@link SingleDocumentListener} to register to the model
	 */
	void addSingleDocumentListener(SingleDocumentListener l);
	
	/**
	 * Removes a {@link SingleDocumentListener} from the model 
	 * @param l the {@link SingleDocumentListener} to remove from the model
	 */
	void removeSingleDocumentListener(SingleDocumentListener l);
}
