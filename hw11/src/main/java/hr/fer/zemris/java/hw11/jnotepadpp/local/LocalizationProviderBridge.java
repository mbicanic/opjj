package hr.fer.zemris.java.hw11.jnotepadpp.local;

/**
 * LocalizationProviderBridge is a full implementation of the
 * {@link ILocalizationProvider} interface, wrapping another
 * {@link ILocalizationProvider}, thus acting as a bridge between
 * the applications utilizing localization and the actual provider
 * itself.
 * 
 * The bridge can be connected and disconnected. When connected,
 * changes in localization reach the applications interested.
 * When disconnected, the localization change doesn't cross the
 * bridge.
 * 
 * The bridge caches a language of the wrapped provider, so if
 * the localization changes while the bridge is disconnected,
 * upon connecting the bridge will be aware of the change and
 * notify all interested listeners.
 * 
 * @author Miroslav Bićanić
 */
public class LocalizationProviderBridge extends AbstractLocalizationProvider {

	
	/** The wrapped provider */
	private ILocalizationProvider wrapped;
	
	/** The listener to register into the wrapped provider */
	private ILocalizationListener listener = () -> {
		cachedLanguage = wrapped.getCurrentLanguage();
		fire();
	};
	
	/** 
	 * Flag marking if the bridge is connected to the wrapped 
	 * provider or not
	 */
	private boolean connected;
	/** The language cached by the bridge */
	private String cachedLanguage;
	
	/**
	 * Constructor for a LocalizationProviderBridge
	 * @param p the {@link ILocalizationProvider} to wrap
	 */
	public LocalizationProviderBridge(ILocalizationProvider p) {
		this.wrapped = p;
		connected = false;
		cachedLanguage = wrapped.getCurrentLanguage();
	}
	
	/**
	 * Connects the bridge between the wrapped {@link ILocalizationProvider}
	 * and the applications utilizing localization.
	 */
	public void connect() {
		if(connected) return;
		wrapped.addLocalizationListener(listener);
		connected=true;
		
		if(!wrapped.getCurrentLanguage().equals(cachedLanguage)) {
			cachedLanguage=wrapped.getCurrentLanguage();
			fire();
		}
	}
	
	/**
	 * Disconnects the bridge between the wrapped {@link ILocalizationProvider}
	 * and the applications utilizing localization.
	 */
	public void disconnect() {
		if(!connected) return;
		wrapped.removeLocalizationListener(listener);
		connected=false;
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getString(String key) {
		return wrapped.getString(key);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getCurrentLanguage() {
		return cachedLanguage;
	}
}
