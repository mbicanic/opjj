package hr.fer.zemris.java.hw11.jnotepadpp.local;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JFrame;

/**
 * Specification of a {@link LocalizationProviderBridge} specialized
 * for working with {@link JFrame} objects, establishing a connection
 * with the wrapped provider once the window is opened, and disconnecting
 * once the window is closed.
 * 
 * @author Miroslav Bićanić
 */
public class FormLocalizationProvider extends LocalizationProviderBridge {

	/**
	 * Constructor for a FormLocalizationProvider
	 * @param p the {@link ILocalizationProvider} to wrap
	 * @param frame the {@link JFrame} utilizing localization
	 */
	public FormLocalizationProvider(ILocalizationProvider p, JFrame frame) {
		super(p);
		frame.addWindowListener(new WindowAdapter() {
			@Override
			public void windowOpened(WindowEvent e) {
				connect();
			}
			@Override
			public void windowClosed(WindowEvent e) {
				disconnect();
			}
		});
	}
}
