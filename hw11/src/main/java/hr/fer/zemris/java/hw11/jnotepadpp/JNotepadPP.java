package hr.fer.zemris.java.hw11.jnotepadpp;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GraphicsEnvironment;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.FlavorEvent;
import java.awt.datatransfer.FlavorListener;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.nio.file.Path;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ActionMap;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JToolBar;
import javax.swing.KeyStroke;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.border.BevelBorder;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;
import javax.swing.text.BadLocationException;
import javax.swing.text.Caret;
import javax.swing.text.DefaultEditorKit;

import hr.fer.zemris.java.hw11.jnotepadpp.local.LocalizationProvider;
import hr.fer.zemris.java.hw11.jnotepadpp.local.entities.LJLabel;
import hr.fer.zemris.java.hw11.jnotepadpp.local.entities.LJMenu;
import hr.fer.zemris.java.hw11.jnotepadpp.local.entities.LocalizableAction;
import hr.fer.zemris.java.hw11.jnotepadpp.model.DefaultMultipleDocumentModel;
import hr.fer.zemris.java.hw11.jnotepadpp.model.MultipleDocumentListener;
import hr.fer.zemris.java.hw11.jnotepadpp.model.MultipleDocumentModel;
import hr.fer.zemris.java.hw11.jnotepadpp.model.SingleDocumentModel;
import hr.fer.zemris.java.hw11.jnotepadpp.util.TimeLabel;

/**
 * JNotepadPP is a simple text editor program capable of having
 * multiple documents open in multiple tabs.
 * 
 * JNotepadPP uses a separate model, {@link MultipleDocumentModel},
 * to achieve its functionality.
 * 
 * The program supports standard text editing operations of
 * copy, cut and paste, utilizing the System's native clipboard.
 * 
 * The program can output statistics in the form of a number of
 * characters, number of non-blank characters and a number of lines
 * in the currently opened document.<br>
 * 
 * Several tools which operate on lines of text are included: <ul>
 * <li> Toggle case </li>
 * <li> To uppercase </li>
 * <li> To lowercase </li>
 * <li> Sort lines ascending </li>
 * <li> Sort lines descending </li>
 * <li> Delete duplicate lines </li> </ul>
 * For the operators to be enabled, a document segment must be 
 * selected.<br>
 * 
 * The program is fully localized and the supported languages 
 * are English, German, Croatian and Polish. 
 * 
 * @author Miroslav Bićanić
 */
public class JNotepadPP extends AbstractNotepadPP {
	private static final long serialVersionUID = 1L;
	
	/** Dropdown list of fonts supported on the system */
	private JComboBox<String> fontList;
	/** Dropdown list of font sizes */
	private JComboBox<Integer> sizeList;
	
	/** 
	 * The localized label displaying the length of the document
	 * (in characters) in the status bar. 
	 */
	private LJLabel lengthLabel;
	/** 
	 * The localized label displaying the caret location's line
	 * number in the status bar.
	 */
	private LJLabel lineLabel;
	/** 
	 * The localized label displaying the column of the caret's
	 * location in the status bar
	 */
	private LJLabel columnLabel;
	/** 
	 * The localized label displaying the length of the selected
	 * text in the status bar
	 */
	private LJLabel selectionLabel;
	
	private Action createDocument;
	private Action saveDocument;
	private Action saveAsDocument;
	private Action loadDocument;
	private Action closeDocument;
	private Action exitApp;
	private Action copyText;
	private Action cutText;
	private Action pasteText;
	private Action statistics;
	private Action fontAction;
	
	private Action toUpper;
	private Action toLower;
	private Action toggle;
	
	private Action ascending;
	private Action descending;
	private Action unique;
	
	/**
	 * Constructor for a JNotepadPP
	 */
	public JNotepadPP() {
		super();
		setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
		setLocation(20,20);
		setTitle("JNotepad++");
		setUpFonts();
		
		Toolkit.getDefaultToolkit().getSystemClipboard().addFlavorListener(new FlavorListener() {
			@Override
			public void flavorsChanged(FlavorEvent e) {
				try {
					Thread.sleep(20);	//Java has a problem with accessing clipboard too soon!
					Clipboard c = Toolkit.getDefaultToolkit().getSystemClipboard();
					String clip = (String)c.getData(DataFlavor.stringFlavor);
					pasteText.setEnabled(clip!=null);
				} catch (UnsupportedFlavorException | IOException | InterruptedException ignored) {}
			}
		});
		
		initGUI();
		pack();
	}

	/**
	 * Initializes the values in the dropdown lists used for
	 * font selection.
	 * 
	 * The action of the dropdown lists are registered later.
	 */
	private void setUpFonts() {
		Font[] fonts = GraphicsEnvironment.getLocalGraphicsEnvironment().getAllFonts();
		String[] names = new String[fonts.length];
		for(int i=0;i<fonts.length;i++) {
			names[i] = fonts[i].getName();
		}
		Integer[] sizes = new Integer[] {12,13,14,15,16,17,18,19,20,22,24,26,28,30,36,40,48};
		this.fontList = new JComboBox<String>(names);
		this.sizeList = new JComboBox<Integer>(sizes);
	}

	/**
	 * Method for GUI initialization
	 */
	private void initGUI() {
		Container cp = getContentPane();
		cp.setLayout(new BorderLayout());
		
		DefaultMultipleDocumentModel dmdm = new DefaultMultipleDocumentModel();
		JPanel mainPanel = new JPanel();
		mainPanel.setLayout(new BorderLayout());
		mainPanel.add(dmdm, BorderLayout.CENTER);
		mainPanel.setPreferredSize(new Dimension(900,500));
		mainPanel.add(createStatusBar(), BorderLayout.PAGE_END);
		cp.add(mainPanel, BorderLayout.CENTER);
		
		mdl = dmdm;
		mdl.addMultipleDocumentListener(MDListener);
		
		createActions();
		createMenus();
		cp.add(createToolbar(), BorderLayout.PAGE_START);
	}
		
	/**
	 * Creates a status bar with information labels on it.
	 * @return a status bar JPanel containing the information labels
	 */
	private JPanel createStatusBar() {
		JPanel statusPanel = new JPanel();
		statusPanel.setBorder(new BevelBorder(BevelBorder.LOWERED));
		statusPanel.setLayout(new GridLayout(0,4));
		
		lengthLabel = new LJLabel("length", flp, 0);
		lengthLabel.setHorizontalAlignment(SwingConstants.LEFT);
		statusPanel.add(lengthLabel);
		
		JPanel positionPanel = new JPanel();
		positionPanel.setLayout(new FlowLayout());
		lineLabel = new LJLabel("line", flp, 1);
		columnLabel = new LJLabel("column", flp, 1);
		selectionLabel = new LJLabel("select", flp, 0);
		positionPanel.add(lineLabel);
		positionPanel.add(columnLabel);
		positionPanel.add(selectionLabel);
		statusPanel.add(positionPanel);
		
		statusPanel.add(new JLabel());
		
		JLabel timeLabel = new TimeLabel();
		statusPanel.add(timeLabel);
		return statusPanel;
	}

	/**
	 * Creates a toolbar providing faster access to the most
	 * commonly used actions.
	 * @return the created JToolBar
	 */
	private JToolBar createToolbar() {
		JToolBar jtb = new JToolBar();
		JButton newFile = new JButton(createDocument);
		JButton openFile = new JButton(loadDocument);
		JButton closeFile = new JButton(closeDocument);
		JButton saveFile = new JButton(saveDocument);
		JButton saveAsFile = new JButton(saveAsDocument);
		JButton closeApp= new JButton(exitApp);
		JButton copy= new JButton(copyText);
		JButton cut = new JButton(cutText);
		JButton paste = new JButton(pasteText);
		JButton stats = new JButton(statistics);
		
		newFile.setHideActionText(true);
		openFile.setHideActionText(true);
		closeFile.setHideActionText(true);
		saveFile.setHideActionText(true);
		saveAsFile.setHideActionText(true);
		copy.setHideActionText(true);
		cut.setHideActionText(true);
		paste.setHideActionText(true);
		stats.setHideActionText(true);
		
		fontList.setAction(fontAction);
		sizeList.setAction(fontAction);
		fontList.setMaximumSize(new Dimension(200, 20));
		sizeList.setMaximumSize(new Dimension(50, 20));
		
		jtb.add(newFile);
		jtb.add(openFile);
		jtb.add(saveFile);
		jtb.add(saveAsFile);
		jtb.addSeparator();
		jtb.add(copy);
		jtb.add(cut);
		jtb.add(paste);
		jtb.addSeparator();
		jtb.add(stats);
		jtb.addSeparator();
		jtb.add(fontList);
		jtb.add(sizeList);
		jtb.addSeparator();
		jtb.add(closeFile);
		jtb.add(closeApp);
		return jtb;
	}

	/**
	 * Initializes all the Actions supported by JNotepad++ by implementing
	 * their actionPerformed method.
	 */
	private void createActions() {
		createDocument = new LocalizableAction("newfile", flp, IconRetriever.NEWFILE, KeyStroke.getKeyStroke("control N"), KeyEvent.VK_N) {
			private static final long serialVersionUID = 1L;
			@Override
			public void actionPerformed(ActionEvent e) {
				mdl.createNewDocument();
			} 
			
		};
		saveDocument = new LocalizableAction("savefile", flp, IconRetriever.SAVEFILE, KeyStroke.getKeyStroke("control S"), KeyEvent.VK_S) {
			private static final long serialVersionUID = 1L;
			@Override
			public void actionPerformed(ActionEvent e) {
				Path p = mdl.getCurrentDocument().getFilePath();
				saveDocument(p, mdl.getCurrentDocument());
			}
		};
		saveAsDocument = new LocalizableAction("saveasfile", flp, IconRetriever.SAVEAS, KeyStroke.getKeyStroke("control P"), KeyEvent.VK_A) {
			private static final long serialVersionUID = 1L;
			@Override
			public void actionPerformed(ActionEvent e) {
				saveDocument(null, mdl.getCurrentDocument());
			}
		};
		loadDocument = new LocalizableAction("openfile", flp, IconRetriever.OPENFILE, KeyStroke.getKeyStroke("control O"), KeyEvent.VK_O) {
			private static final long serialVersionUID=1L;
			@Override
			public void actionPerformed(ActionEvent e) {
				loadDocument();
			}
		};
		closeDocument = new LocalizableAction("closetab", flp, IconRetriever.CLOSETAB, KeyStroke.getKeyStroke("control W"), KeyEvent.VK_L) {
			private static final long serialVersionUID = 1L;
			@Override
			public void actionPerformed(ActionEvent e) {
				if(checkOneUnsavedFile(mdl.getCurrentDocument())) {
					mdl.closeDocument(mdl.getCurrentDocument());
				}
			}
		};
		exitApp = new LocalizableAction("closeapp", flp, IconRetriever.CLOSEAPP, KeyStroke.getKeyStroke("control Q"), KeyEvent.VK_X) {
			private static final long serialVersionUID = 1L;
			@Override
			public void actionPerformed(ActionEvent e) {
				checkForUnsavedFiles();
			}
		};
		copyText = new LocalizableAction("copy", flp, IconRetriever.COPY, KeyStroke.getKeyStroke("control C"), KeyEvent.VK_C) {
			private static final long serialVersionUID = 1L;
			@Override
			public void actionPerformed(ActionEvent e) {
				putToClipboard(false);
			}
		};
		cutText = new LocalizableAction("cut", flp, IconRetriever.CUT, KeyStroke.getKeyStroke("control X"), KeyEvent.VK_U) {
			private static final long serialVersionUID = 1L;
			@Override
			public void actionPerformed(ActionEvent e) {
				putToClipboard(true);
			}
		};
		pasteText = new LocalizableAction("paste", flp, IconRetriever.PASTE, KeyStroke.getKeyStroke("control V"), KeyEvent.VK_P) {
			private static final long serialVersionUID = 1L;
			@Override
			public void actionPerformed(ActionEvent e) {
				putFromClipboard();
				pasteText.setEnabled(true);
			}
		};
		statistics = new LocalizableAction("stats", flp, IconRetriever.STATS, KeyStroke.getKeyStroke("control T"), KeyEvent.VK_T) {
			private static final long serialVersionUID = 1L;
			@Override
			public void actionPerformed(ActionEvent e) {
				calculateStatistics();
			}
		};

		fontAction = new LocalizableAction("font", flp) {
				private static final long serialVersionUID = 1L;
				@Override
				public void actionPerformed(ActionEvent e) {
					for(SingleDocumentModel sdm : mdl) {
						JTextArea jta = sdm.getTextComponent();
						Font toSet = new Font((String)fontList.getSelectedItem(), Font.PLAIN, (Integer)sizeList.getSelectedItem());
						jta.setFont(toSet);
					}
				}
			};

		copyText.setEnabled(false);
		cutText.setEnabled(false);
		pasteText.setEnabled(false);
		statistics.setEnabled(false);
		fontAction.setEnabled(false);
		
		toggle = new LocalizableAction("toggle", flp, IconRetriever.TOGGLE, KeyStroke.getKeyStroke("control G"), KeyEvent.VK_G) {
			private static final long serialVersionUID = 1L;
			@Override
			public void actionPerformed(ActionEvent e) {
				toggleCase();
			}
		};
		toUpper = new LocalizableAction("up", flp, IconRetriever.UPPER, KeyStroke.getKeyStroke("control U"), KeyEvent.VK_U) {
			private static final long serialVersionUID = 1L;
			@Override
			public void actionPerformed(ActionEvent e) {
				toCase(true);
			}
		};
		toLower = new LocalizableAction("low", flp, IconRetriever.LOWER, KeyStroke.getKeyStroke("control L"), KeyEvent.VK_L) {
			private static final long serialVersionUID = 1L;
			@Override
			public void actionPerformed(ActionEvent e) {
				toCase(false);
			}
		};
		ascending = new LocalizableAction("asc", flp, IconRetriever.ASCENDING, KeyStroke.getKeyStroke("control Z"), KeyEvent.VK_A) {
			private static final long serialVersionUID = 1L;
			@Override
			public void actionPerformed(ActionEvent e) {
				lineTools(true, true);
			}
		};
		descending = new LocalizableAction("desc", flp, IconRetriever.DESCENDING, KeyStroke.getKeyStroke("control Y"), KeyEvent.VK_D) {
			private static final long serialVersionUID = 1L;
			@Override
			public void actionPerformed(ActionEvent e) {
				lineTools(true, false);
			}
		};
		unique = new LocalizableAction("uniq", flp, IconRetriever.UNIQUE, KeyStroke.getKeyStroke("control E"), KeyEvent.VK_Q) {
			private static final long serialVersionUID = 1L;
			@Override
			public void actionPerformed(ActionEvent e) {
				lineTools(false, false);
			}
		};
		toggle.setEnabled(false);
		toUpper.setEnabled(false);
		toLower.setEnabled(false);
		unique.setEnabled(false);
		descending.setEnabled(false);
		ascending.setEnabled(false);
	};

	/**
	 * Creates the menu bar containing various menus, setting it
	 * to the main JFrame.
	 */
	private void createMenus() {
		JMenuBar menuBar = new JMenuBar();
		createFileSubmenu(menuBar);
		createEditSubmenu(menuBar);
		createLanguageSubmenu(menuBar);
		createToolsSubmenu(menuBar);
		setJMenuBar(menuBar);
	}
	
	/**
	 * Creates the localized 'File' menu, containing menu items for
	 * the supported file manipulating actions, and adds it to the
	 * given menuBar.
	 * 
	 * @param menuBar the menu bar to which to add the 'File' menu
	 */
	private void createFileSubmenu(JMenuBar menuBar) {
		JMenu file = new LJMenu("file", flp);
		menuBar.add(file);
		
		JMenuItem newFile = new JMenuItem(createDocument);
		file.add(newFile);
		JMenuItem loadFile = new JMenuItem(loadDocument);
		file.add(loadFile);
		file.addSeparator();
		
		JMenuItem saveFile = new JMenuItem(saveDocument);
		file.add(saveFile);
		JMenuItem saveAs = new JMenuItem(saveAsDocument);
		file.add(saveAs);
		file.addSeparator();
		
		JMenuItem closeTab = new JMenuItem(closeDocument);
		file.add(closeTab);
		JMenuItem exit = new JMenuItem(exitApp);
		file.add(exit);
	}
	/**
	 * Creates the localized 'Edit' menu, containing menu items for
	 * the supported text editing, and adds it to the given menu bar.
	 * 
	 * @param menuBar the menu bar to which to add the 'Edit' menu
	 */
	private void createEditSubmenu(JMenuBar menuBar) {
		JMenu edit = new LJMenu("edit", flp);
		menuBar.add(edit);
		
		JMenuItem copy = new JMenuItem(copyText);
		edit.add(copy);
		JMenuItem cut = new JMenuItem(cutText);
		edit.add(cut);
		JMenuItem paste = new JMenuItem(pasteText);
		edit.add(paste);
		edit.addSeparator();
		
		JMenuItem stat = new JMenuItem(statistics);
		edit.add(stat);
	}
	
	/**
	 * Creates the localized 'Languages' menu, containing menu items for
	 * changing the language of the program.
	 * 
	 * @param menuBar the menu bar to which to add the 'Languages' menu
	 */
	private void createLanguageSubmenu(JMenuBar menuBar) {
		LJMenu languages = new LJMenu("lang", flp);
		menuBar.add(languages);
		
		JMenuItem german = new JMenuItem(createLanguageAction("de", "Deutsch"));
		JMenuItem english = new JMenuItem(createLanguageAction("en", "English"));
		JMenuItem croatian = new JMenuItem(createLanguageAction("hr", "Hrvatski"));
		JMenuItem polish = new JMenuItem(createLanguageAction("pl", "Polski"));
		languages.add(english);
		languages.add(croatian);
		languages.add(polish);
		languages.add(german);
	}
	
	/**
	 * Factory method creating an action to be assigned to all menu items 
	 * which represent supported languages.
	 * The action updates the application's language to the language 
	 * given as the argument to this method.
	 * 
	 * @param languageKey the key of the language
	 * @param languageName the full name of the language, as written in that language
	 * @return an Action for changing the language
	 */
	private Action createLanguageAction(String languageKey, String languageName) {
		Action a = new AbstractAction() {
			private static final long serialVersionUID = 1L;
			@Override
			public void actionPerformed(ActionEvent e) {
				LocalizationProvider.getInstance().setLanguage(languageKey);
			}
		};
		a.putValue(Action.NAME, languageName);
		a.putValue(Action.SMALL_ICON, IconRetriever.getInstance().fetch("lang/"+languageKey+".png"));
		return a;
	}
	
	/**
	 * Creates the localized 'Tools' menu, containing menu items for
	 * text tools, and adds it to the given menuBar.
	 * 
	 * @param menuBar the menu bar to which to add the 'Tools' menu
	 */
	private void createToolsSubmenu(JMenuBar menuBar) {
		LJMenu tools = new LJMenu("tools", flp);
		menuBar.add(tools);
		
		LJMenu subCase = new LJMenu("case", flp);
		tools.add(subCase);
		
		JMenuItem upper = new JMenuItem(toUpper);
		JMenuItem lower = new JMenuItem(toLower);
		JMenuItem toggle = new JMenuItem(this.toggle);
		subCase.add(upper);
		subCase.add(lower);
		subCase.add(toggle);
		
		LJMenu subSort = new LJMenu("sort", flp);
		tools.add(subSort);
		
		JMenuItem ascending = new JMenuItem(this.ascending);
		JMenuItem descending = new JMenuItem(this.descending);
		subSort.add(ascending);
		subSort.add(descending);
		
		JMenuItem unique = new JMenuItem(this.unique);
		tools.add(unique);
	}

	/**
	 * An implementation of a MultipleDocumentListener, registered to the
	 * underlying MultipleDocumentModel so that the GUI is informed of 
	 * changes in the model.
	 */
	private MultipleDocumentListener MDListener = new MultipleDocumentListener() {
		@Override
		public void documentRemoved(SingleDocumentModel model) {
			if(mdl.getNumberOfDocuments()==0) {
				closeDocument.setEnabled(false);
				pasteText.setEnabled(false);
				saveDocument.setEnabled(false);
				saveAsDocument.setEnabled(false);
				statistics.setEnabled(false);
				fontAction.setEnabled(false);
				lengthLabel.setVisible(false);
				lineLabel.setVisible(false);
				columnLabel.setVisible(false);
				selectionLabel.setVisible(false);
			}
		}
		
		@Override
		public void documentAdded(SingleDocumentModel model) {
			closeDocument.setEnabled(true);
			statistics.setEnabled(true);
			fontAction.setEnabled(true);
			
			lengthLabel.setVisible(true);
			lineLabel.setVisible(true);
			columnLabel.setVisible(true);
			selectionLabel.setVisible(true);
			JTextArea jta = model.getTextComponent();
			if(mdl.getNumberOfDocuments()==1) {	//set default font if first document
				try {
					jta.setFont(new Font("Consolas", Font.PLAIN, 14)); 
				} catch (Exception ignored) {}
				String name = jta.getFont().getName();
				int size = jta.getFont().getSize();
				fontList.setSelectedItem(name);
				sizeList.setSelectedItem(size);
			} else {	//otherwise set the currently selected font
				String name  = (String)fontList.getSelectedItem();
				int size = (int)sizeList.getSelectedItem();
				jta.setFont(new Font(name, Font.PLAIN, size));
			}
			
			ActionMap amap = jta.getActionMap();
			amap.put(DefaultEditorKit.copyAction, copyText);
			amap.put(DefaultEditorKit.cutAction, cutText);
			amap.put(DefaultEditorKit.pasteAction, pasteText);
		}
		
		@Override
		public void currentDocumentChanged(SingleDocumentModel previousModel, SingleDocumentModel currentModel) {
			if(previousModel==null && currentModel==null) {
				throw new IllegalArgumentException("Both models cannot be null!");
			}
			if(previousModel!=null) {
				previousModel.getTextComponent().addCaretListener(caretListener);
			}
			if(currentModel!=null) {
				currentModel.getTextComponent().addCaretListener(caretListener);
				caretListener.caretUpdate(null);	//manually forcing an update
				statistics.setEnabled(true);
			}
			if(currentModel==null) {
				setTitle("JNotepad++");
				return;
			}
			String title;
			if (currentModel.getFilePath()==null) {
				title = flp.getString("nonam");
			} else {
				title = currentModel.getFilePath().toString();
			}
			setTitle(title + " - JNotepad++");

			if(currentModel.isModified()) {
				saveDocument.setEnabled(true);
				saveAsDocument.setEnabled(true);
			} else {
				saveDocument.setEnabled(false);
				saveAsDocument.setEnabled(false);
			}
		}
	};
	
	/**
	 * An implementation of a CaretListener registered to every active
	 * document's caret, used to enable/disable actions and show correct
	 * information in the status bar.
	 */
	private CaretListener caretListener = new CaretListener() {
		@Override
		public void caretUpdate(CaretEvent e) {
			JTextArea jta = mdl.getCurrentDocument().getTextComponent();
			Caret c = jta.getCaret();
			
			int offset = jta.getCaretPosition();
			int lineNo=1;
			int colNo=0;
			try {
				lineNo = jta.getLineOfOffset(offset);
				colNo = offset - jta.getLineStartOffset(lineNo);
				lineNo++;
			} catch (BadLocationException ignored) {}
			int length = Math.abs(c.getDot() - c.getMark());
			
			lineLabel.updateValue(lineNo);
			columnLabel.updateValue(colNo+1);
			selectionLabel.updateValue(length);
			lengthLabel.updateValue(jta.getText().length());
			
			boolean enableTools = length!=0;
			copyText.setEnabled(enableTools);
			cutText.setEnabled(enableTools);
			toUpper.setEnabled(enableTools);
			toLower.setEnabled(enableTools);
			toggle.setEnabled(enableTools);
			ascending.setEnabled(enableTools);
			descending.setEnabled(enableTools);
			unique.setEnabled(enableTools);
		}
	};
	
	/**
	 * Entry point for the application.
	 * @param args command-line arguments - not used
	 */
	public static void main(String[] args) {
		SwingUtilities.invokeLater(()->{
			new JNotepadPP().setVisible(true);
		});
	}
}
