package hr.fer.zemris.java.hw11.jnotepadpp.local.entities;

import javax.swing.JLabel;

import hr.fer.zemris.java.hw11.jnotepadpp.JNotepadPP;
import hr.fer.zemris.java.hw11.jnotepadpp.local.ILocalizationProvider;

/**
 * LJLabel is a localized specification of a {@link JLabel},
 * used in {@link JNotepadPP} for displaying information about
 * the opened document.
 * 
 * The label contains a localized name of the parameter it
 * is displaying and a numerical value next to it.
 * 
 * @author Miroslav Bićanić
 *
 */
public class LJLabel extends JLabel {
	private static final long serialVersionUID = 1L;
	
	/** The {@link ILocalizationProvider} used for localizing the label */
	private ILocalizationProvider lp;
	/** The key with which the localized text value is associated */
	private String key;
	/** The numerical value to be displayed by this label */
	private int cachedValue;
	
	/**
	 * Constructor for a LJLabel
	 * @param key the key with which the localized name is associated
	 * @param lp the {@link ILocalizationProvider} used for translating
	 * @param value the initial value for this label
	 */
	public LJLabel(String key, ILocalizationProvider lp, int value) {
		this.lp = lp;
		this.key = key;
		this.cachedValue = value;
		setVisible(false);
		setText(lp.getString(key)+value);
		lp.addLocalizationListener(()->{
			setText(lp.getString(key)+cachedValue);
		});
	}

	/**
	 * Updates the numerical value to be displayed by this label
	 * @param newVal the new value
	 */
	public void updateValue(int newVal) {
		this.cachedValue = newVal;
		setText(lp.getString(key)+newVal);
	}
}
