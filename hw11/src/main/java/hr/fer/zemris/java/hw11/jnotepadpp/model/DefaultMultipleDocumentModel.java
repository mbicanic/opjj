package hr.fer.zemris.java.hw11.jnotepadpp.model;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;

import javax.swing.ImageIcon;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;

import hr.fer.zemris.java.hw11.jnotepadpp.IconRetriever;
import hr.fer.zemris.java.hw11.jnotepadpp.JNotepadPP;
import hr.fer.zemris.java.hw11.jnotepadpp.local.LocalizationProvider;

/**
 * Specification of a {@link JTabbedPane} and an implementation of 
 * a {@link MultipleDocumentModel}, providing the functionality of
 * multi-tabbing documents in {@link JNotepadPP}.
 * 
 * @author Miroslav Bićanić
 */
public class DefaultMultipleDocumentModel extends JTabbedPane implements MultipleDocumentModel {
	private static final long serialVersionUID = 1L;
	
	/** 
	 * The {@link ImageIcon} representing the icon of a tab containing
	 * an unmodified document.
	 */
	private static final ImageIcon SAVED = 
			IconRetriever.getInstance().fetch(IconRetriever.SAVED);
	/** 
	 * The {@link ImageIcon} representing the icon of a tab containing
	 * an unmodified document.
	 */
	private static final ImageIcon MODIFIED = 
			IconRetriever.getInstance().fetch(IconRetriever.MODIFIED);

	/**
	 * Internal list of {@link SingleDocumentModel} objects.
	 */
	private List<SingleDocumentModel> documents;
	/**
	 * The currently selected {@link SingleDocumentModel} 
	 */
	private SingleDocumentModel current;

	/**
	 * Internal list of JScrollPane wrappers wrapping each document's
	 * JTextArea. The JScrollPanes are added as actual tabs of 
	 * JTabbedPane.
	 * 
	 * Cached for easier access to JTabbedPane's components.
	 */
	private List<JScrollPane> tabs;
	
	/**
	 * List of registered listeners to this MultipleDocumentModel
	 */
	private List<MultipleDocumentListener> listeners;
	
	/**
	 * Constructor of a {@link DefaultMultipleDocumentModel}
	 */
	public DefaultMultipleDocumentModel() {
		this.documents = new ArrayList<SingleDocumentModel>();
		this.current = null;
		this.listeners = new ArrayList<MultipleDocumentListener>();
		this.tabs = new ArrayList<>();
		
		//listener added to JTabbedPane, setting the current document
		//and informing all MultipleDocumentModel listeners of the change.
		this.addChangeListener(e->{
			SingleDocumentModel previous = current;
			current = getSelectedComponent()==null ? 
					null : documents.get(tabs.indexOf(getSelectedComponent()));
			for(MultipleDocumentListener mdl : listeners) {
				mdl.currentDocumentChanged(previous, current);
			}
		});
	}
	
	/**
	 * Returns an iterator over all {@link SingleDocumentModel} objects
	 * stored in this MultipleDocumentModel.
	 */
	@Override
	public Iterator<SingleDocumentModel> iterator() {
		return documents.iterator();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public SingleDocumentModel createNewDocument() {
		SingleDocumentModel doc = new DefaultSingleDocumentModel(null, "");
		JScrollPane jsp = new JScrollPane(doc.getTextComponent());
		
		documents.add(doc);
		tabs.add(jsp);
		doc = registerSDL(doc, jsp);

		//LocProvider is okay - he is not registered as a listner
		//there are no problems here as described with i18n and Swing
		addTab(LocalizationProvider.getInstance().getString("nonam"), jsp);	//sets current, informs currentDocumentChange
		setSelectedComponent(jsp);
		setIconAt(indexOfComponent(jsp), SAVED);
		setToolTipTextAt(indexOfComponent(jsp), "(unnamed)");
		for(MultipleDocumentListener mdl : listeners) {
			mdl.documentAdded(doc);
		}
		return current;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * If the there is no model open yet, the current model is set to null,
	 * and null can be returned.
	 */
	@Override
	public SingleDocumentModel getCurrentDocument() {
		return current;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @throws NullPointerException if given {@code path} is null
	 * @throws IllegalArgumentException if file from given path is not readable</br> 
	 * 									-if error occurs while reading file
	 */
	@Override
	public SingleDocumentModel loadDocument(Path path) {
		Objects.requireNonNull(path);
		if(!Files.isReadable(path)) {
			throw new IllegalArgumentException("e_unread");
		}
		String content;
		try {
			byte[] arr = Files.readAllBytes(path);	//readString with UTF8 didn't display diacritics correctly for me
			content = new String(arr, StandardCharsets.UTF_8);
		} catch (IOException e) {
			throw new IllegalArgumentException("e_ioex_r");
		}
		
		SingleDocumentModel doc = new DefaultSingleDocumentModel(path, content);
		SingleDocumentModel previous = current;
		if(documents.contains(doc)) {
			int index = documents.indexOf(doc);
			SingleDocumentModel original = documents.get(index);
			JScrollPane pane = tabs.get(index);
			current = original; 				//current must be set when file already open
			setSelectedComponent(pane); 		//because setSelectedComponent does not notify
			for(MultipleDocumentListener mdl : listeners) {
				mdl.currentDocumentChanged(previous, current);
			}
			return current;
		}
		JScrollPane jsp = new JScrollPane(doc.getTextComponent());
		documents.add(doc);
		tabs.add(jsp);
		
		doc = registerSDL(doc, jsp);
		
		addTab(path.getFileName().toString(), jsp); //sets current, informs currentDocumentChanged
		setSelectedComponent(jsp);
		setIconAt(indexOfComponent(jsp), SAVED);
		for(MultipleDocumentListener mdl : listeners) {
			mdl.documentAdded(doc);
		}
		return current;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @throws NullPointerException if given {@code model} is null</br>
	 * 					-if both {@code newPath} and the model's path are null
	 * @throws IllegalArgumentException if attempted to save as a file that is 
	 * 					already open in the {@link MultipleDocumentModel} </br>
	 * 					-if error occurs while writing to file
	 */
	@Override
	public void saveDocument(SingleDocumentModel model, Path newPath) {
		if(newPath==null) {
			Path p = model.getFilePath(); 
			writeContentToFile(p, model);
		} else {
			for(SingleDocumentModel sdm : documents) {
				if(newPath.equals(sdm.getFilePath())&& sdm!=model) {
					throw new IllegalArgumentException("e_saveopen");
				}
			}
			writeContentToFile(newPath, model);
			model.setFilePath(newPath); //notifies the listeners, see #registerSDL method
		}
	}
	
	/**
	 * Writes the content of the {@code model} to the path {@code p}.
	 * 
	 * @param p the Path to which to write contents
	 * @param model the SingleDocumentModel whose contents to write
	 * @throws IllegalArgumentException if IOException occurs while writing
	 */
	private void writeContentToFile(Path p, SingleDocumentModel model) {
		try {
			byte[] arr = model.getTextComponent().getText().getBytes(StandardCharsets.UTF_8);
			Files.write(p, arr); 			//again, writeString with UTF8 didn't work
			model.setModified(false);
		} catch (IOException e) {
			throw new IllegalArgumentException("e_ioex_w");
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * </br> If the model does not exist, the method does nothing. 
	 * </br> If there are remaining documents after closing and removing
	 * 		 this one, the selected document becomes the first document
	 * 	 	 in the list.
	 */
	@Override
	public void closeDocument(SingleDocumentModel model) {
		if(!this.documents.contains(model)) {
			return;
		}
		SingleDocumentModel previous = current;
		int index = documents.indexOf(model);
		this.tabs.remove(index);
		this.documents.remove(index);
		remove(index);	
		
		if(documents.size()==0) {
			current=null;
			fireRemoved(previous, current);
			return;
		}

		JScrollPane pane = tabs.get(0);
		setSelectedComponent(pane);
		current = documents.get(0);
		fireRemoved(previous, current);
	}
	
	/**
	 * Notifies all listeners of this MultipleDocumentModel that
	 * a document has been closed and removed from the model.
	 * 
	 * @param removed the rmeoved {@link SingleDocumentModel}
	 * @param future the new current {@link SingleDocumentModel}
	 */
	private void fireRemoved(SingleDocumentModel removed, SingleDocumentModel future) {
		for(MultipleDocumentListener mdl : listeners) {
			mdl.currentDocumentChanged(removed, future);
			mdl.documentRemoved(removed);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void addMultipleDocumentListener(MultipleDocumentListener l) {
		listeners.add(l);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void removeMultipleDocumentListener(MultipleDocumentListener l) {
		listeners.remove(l);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getNumberOfDocuments() {
		return documents.size();
	}

	/**
	 * {@inheritDoc}
	 * @throws IndexOutOfBoundsException if given index is out of range
	 */
	@Override
	public SingleDocumentModel getDocument(int index) {
		if(index<0 || index>=documents.size()) {
			throw new IndexOutOfBoundsException();
		}
		return documents.get(index);
	}
	
	/**
	 * Registers a {@link SingleDocumentListener} to a {@link SingleDocumentModel}
	 * that is being added to this MultipleDocumentModel.
	 * 
	 * @param doc the document to which to register a listener
	 * @param jsp the JScrollPane in which the document's JTextArea is wrapped
	 * @return the document with a registered listener
	 */
	private SingleDocumentModel registerSDL(SingleDocumentModel doc, JScrollPane jsp) {
		doc.addSingleDocumentListener(new SingleDocumentListener() {
			@Override
			public void documentModifyStatusUpdated(SingleDocumentModel model) {
				int index = indexOfComponent(jsp);
				Path p = model.getFilePath();
				
				setIconAt(index, model.isModified() ? MODIFIED : SAVED);
				String title = p==null ? 
						LocalizationProvider.getInstance().getString("nonam") : getTitleAt(index);
				setTitleAt(index, title+(model.isModified() ? "*" : ""));
				String toolTip = p==null ? getTitleAt(index) : p.toString();
				setToolTipTextAt(index, toolTip);
				for(MultipleDocumentListener mdl : listeners) {
					mdl.currentDocumentChanged(null, model);
					//informing current doc changed but focus remained on it
				}
			}
			
			@Override
			public void documentFilePathUpdated(SingleDocumentModel model) {
				setTitleAt(indexOfComponent(jsp), model.getFilePath().getFileName().toString());
				setToolTipTextAt(indexOfComponent(jsp), model.getFilePath().toString());
				for(MultipleDocumentListener mdl : listeners) {
					mdl.currentDocumentChanged(null, model);
					//informing current doc changed but focus remained on it
				}
			}
		});
		return doc;
	}
}
