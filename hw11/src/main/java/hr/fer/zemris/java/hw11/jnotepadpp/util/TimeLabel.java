package hr.fer.zemris.java.hw11.jnotepadpp.util;

import java.sql.Date;
import java.text.SimpleDateFormat;

import javax.swing.JLabel;
import javax.swing.SwingConstants;

/**
 * Specification of a {@link JLabel} displays the current date
 * and time in the format {@code yyyy/MM/dd HH:mm:ss}.
 * 
 * Time is updated using a daemonic thread which performs the update
 * every 500ms.
 * 
 * @author Miroslav Bićanić
 */
public class TimeLabel extends JLabel {
	private static final long serialVersionUID=1L;
	
	/** The text to display on the label. */
	private volatile String time;
	/** The format in which to display the date and time */
	private SimpleDateFormat dtf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
	
	/**
	 * Constructor for a TimeLabel
	 */
	public TimeLabel() {
		updateTime();
		this.setHorizontalAlignment(SwingConstants.RIGHT);
		Thread t = new Thread(()->{
			while(true) {
				try {
					Thread.sleep(500);
				} catch (Exception ex) {}
				updateTime();
			}
		});
		t.setDaemon(true);
		t.start();
	}
	
	/**
	 * Formats the current time and sets it as the label's text.
	 */
	private void updateTime() {
		Date d = new Date(System.currentTimeMillis());
		time = dtf.format(d);
		this.setText(time);
	}
}
