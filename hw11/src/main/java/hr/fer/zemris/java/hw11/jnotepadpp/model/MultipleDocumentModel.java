package hr.fer.zemris.java.hw11.jnotepadpp.model;

import java.nio.file.Path;

import hr.fer.zemris.java.hw11.jnotepadpp.JNotepadPP;

/**
 * MultipleDocumentModel is an interface for a model containing
 * and managing multiple {@link SingleDocumentModel} objects in
 * {@link JNotepadPP}.
 * 
 * The model must track which of the documents is the current 
 * document, and have all documents indexed.
 * 
 * The model is a subject in the observer design pattern,
 * capable of notifying listeners on change of its parameters.
 * 
 * @author Miroslav Bićanić
 */
public interface MultipleDocumentModel extends Iterable<SingleDocumentModel> {
	
	/**
	 * Creates a new {@link SingleDocumentModel} and adds it to the
	 * internal storage.
	 * 
	 * @return the created {@link SingleDocumentModel}
	 */
	SingleDocumentModel createNewDocument();
	
	/**
	 * Returns the current document model
	 * @return the current document model
	 */
	SingleDocumentModel getCurrentDocument();

	/**
	 * Returns the number of document models contained in this
	 * MultipleDocumentModel.
	 * 
	 * @return the number of documents
	 */
	int getNumberOfDocuments();

	/***
	 * Returns the document model stored at the given {@code index} 
	 * in the internal list.
	 * 
	 * @param index the index from which to retrieve the model
	 * @return a {@link SingleDocumentModel} from the given index
	 */
	SingleDocumentModel getDocument(int index);
	
	/**
	 * Loads a document into the model from the given {@code path}.
	 * @param path the {@link Path} from which to load a document
	 * @return the loaded document model
	 */
	SingleDocumentModel loadDocument(Path path);
	
	/**
	 * Saves the given document {@code model} to a file specified by the
	 * given {@code newPath}. If {@code newPath} is null, the document is 
	 * saved to the path specified by the model's 
	 * {@link SingleDocumentModel#getFilePath()}
	 * 
	 * @param model the model to save
	 * @param newPath the path to which to save the model; can be null
	 */
	void saveDocument(SingleDocumentModel model, Path newPath);
	
	/**
	 * Closes the document and removes the {@code model} from this
	 * model's internal storage.
	 * 
	 * @param model the model of the document to close
	 */
	void closeDocument(SingleDocumentModel model);
	
	/**
	 * Registers a {@link MultipleDocumentListener} to the model 
	 * @param l the {@link MultipleDocumentListener} to register to the model
	 */
	void addMultipleDocumentListener(MultipleDocumentListener l);
	
	/**
	 * Removes a {@link MultipleDocumentListener} from the model 
	 * @param l the {@link MultipleDocumentListener} to remove from the model
	 */
	void removeMultipleDocumentListener(MultipleDocumentListener l);
}