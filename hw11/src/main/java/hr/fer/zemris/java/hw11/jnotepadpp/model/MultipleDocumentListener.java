package hr.fer.zemris.java.hw11.jnotepadpp.model;

/**
 * Interface for a listener of a {@link MultipleDocumentModel}.
 *
 * @author Miroslav Bićanić
 */
public interface MultipleDocumentListener {
	
	/**
	 * Invoked whenever the the current document changes from
	 * {@code previousModel} to {@code currentModel}.
	 * 
	 * @param previousModel the {@link SingleDocumentModel} that used to be the current model
	 * @param currentModel the {@link SingleDocumentModel} that is the new current model
	 */
	void currentDocumentChanged(SingleDocumentModel previousModel, SingleDocumentModel currentModel);
	
	/**
	 * Invoked whenever a document was added to the 
	 * {@link MultipleDocumentModel}
	 * 
	 * @param model the {@link SingleDocumentModel} that was added
	 */
	void documentAdded(SingleDocumentModel model);
	
	/**
	 * Invoked whenever a document is removed to the 
	 * {@link MultipleDocumentModel}
	 * 
	 * @param model the {@link SingleDocumentModel} that was removed
	 */
	void documentRemoved(SingleDocumentModel model);
}
