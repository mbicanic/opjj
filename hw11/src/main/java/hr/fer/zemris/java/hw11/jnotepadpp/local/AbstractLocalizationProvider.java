package hr.fer.zemris.java.hw11.jnotepadpp.local;

import java.util.ArrayList;
import java.util.List;

/**
 * An abstract implementation of the {@link ILocalizationProvider} interface,
 * defining common methods to all providers.
 * 
 * @author Miroslav Bićanić
 */
public abstract class AbstractLocalizationProvider implements ILocalizationProvider {
	
	/** The list of {@link ILocalizationListener} objects registered to this provider */
	protected List<ILocalizationListener> listeners = new ArrayList<ILocalizationListener>();
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public void addLocalizationListener(ILocalizationListener l) {
		listeners.add(l);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void removeLocalizationListener(ILocalizationListener l) {
		listeners.remove(l);
	}
	
	/**
	 * Notifies all listeners of a change in localization
	 */
	protected void fire() {
		for(ILocalizationListener l : listeners) {
			l.localizationChanged();
		}
	}

}
