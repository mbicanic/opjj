package hr.fer.zemris.java.hw11.jnotepadpp.local.entities;

import javax.swing.JMenu;

import hr.fer.zemris.java.hw11.jnotepadpp.JNotepadPP;
import hr.fer.zemris.java.hw11.jnotepadpp.local.ILocalizationProvider;

/**
 * LJMenu is a localized specification of a {@link JMenu},
 * used for the menus in {@link JNotepadPP}.
 * 
 * @author Miroslav Bićanić
 */
public class LJMenu extends JMenu {
	private static final long serialVersionUID = 1L;
	
	/**
	 * Constructor for a LJMenu
	 * @param key the key with which the localized name is associated
	 * @param lp the {@link ILocalizationProvider} used for translation
	 */
	public LJMenu(String key, ILocalizationProvider lp) {
		setText(lp.getString(key));
		lp.addLocalizationListener(()->{
			setText(lp.getString(key));
		});
	}
}
