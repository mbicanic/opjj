package hr.fer.zemris.java.hw11.jnotepadpp.model;

/**
 * Interface for a listener of a {@link SingleDocumentModel}.
 *
 * @author Miroslav Bićanić
 */
public interface SingleDocumentListener {
	
	/**
	 * Invoked whenever the model's modification indicator changes
	 * @param model the model whose indicator changed
	 */
	void documentModifyStatusUpdated(SingleDocumentModel model);
	
	/**
	 * Invoked whenever the path associated with the model changes
	 * @param model the model whose associated path changed
	 */
	void documentFilePathUpdated(SingleDocumentModel model);
}
