package hr.fer.zemris.java.hw11.jnotepadpp;

import java.io.IOException;
import java.io.InputStream;

import javax.swing.ImageIcon;

/**
 * IconRetriever is a utility class providing access to {@link ImageIcon}
 * objects located on the disk.
 * 
 * It defines several String constants corresponding to actual 
 * {@link ImageIcon} objects to fetch.
 * 
 * The class is designed using the Singleton design pattern, meaning
 * only one instance of IconRetriever can exist at any given time.
 * 
 * @author Miroslav Bićanić
 *
 */
public class IconRetriever {
	/** The constant associated with the icon used for an unmodified file */
	public static final String SAVED = "saved.png";
	/** The constant associated with the icon used for a modified file */
	public static final String MODIFIED = "modified.png";
	
	/** The constant associated with the icon used for creating new files */
	public static final String NEWFILE = "newFile.png";
	/** The constant associated with the icon used for opening files */
	public static final String OPENFILE = "openFile.png";
	/** The constant associated with the icon used for closing files */
	public static final String CLOSETAB = "closeTab.png";
	/** The constant associated with the icon used for closing the application */
	public static final String CLOSEAPP = "closeApp.png";
	/** The constant associated with the icon used for saving files */
	public static final String SAVEFILE = "save.png";
	/** The constant associated with the icon used for the save-as action */
	public static final String SAVEAS = "saveAs.png";

	/** The constant associated with the icon used for the copy action */
	public static final String COPY = "copy.png";
	/** The constant associated with the icon used for the cut action */
	public static final String CUT = "cut.png";
	/** The constant associated with the icon used for the paste action */
	public static final String PASTE = "paste.png";
	/** The constant associated with the icon used for the statistics action */
	public static final String STATS = "stats.png";

	/** The constant associated with the icon used for errors */
	public static final String ERROR = "error.png";
	/** The constant associated with the icon used for information messages */
	public static final String INFO = "info.png";
	/** The constant associated with the icon used for question dialogs */
	public static final String QUESTION = "question.png";

	/** The constant associated with the icon used for the to-lowercase tool */
	public static final String LOWER = "toLower.png";
	/** The constant associated with the icon used for the to-uppercase tool */
	public static final String UPPER = "toUpper.png";
	/** The constant associated with the icon used for the toggle-case tool */
	public static final String TOGGLE = "toggle.png";
	/** The constant associated with the icon used for the sort-ascending tool */
	public static final String ASCENDING = "ascending.png";
	/** The constant associated with the icon used for the sort-descending tool */
	public static final String DESCENDING = "descending.png";
	/** The constant associated with the icon used for the delete-duplicates tool */
	public static final String UNIQUE = "unique.png";
	
	/** The single existing instance of an IconRetriever */
	private static final IconRetriever instance = new IconRetriever();
	
	/**
	 * Private constructor
	 */
	private IconRetriever() { }
	
	/**
	 * Getter method for an instance of an IconRetriever
	 * @return an instance of an IconRetriever 
	 */
	public static IconRetriever getInstance() {
		return instance;
	}
	
	/**
	 * Returns an {@link ImageIcon} associated with the given 
	 * {@code key}.
	 * 
	 * @param id the key with which the desired icon is associated
	 * @return the {@link ImageIcon} associated with th ekey
	 */
	public ImageIcon fetch(String id) {
		InputStream is = this.getClass().getResourceAsStream("icons/"+id);
		if(is==null) {
			throw new IllegalArgumentException();
		}
		try {
			byte[] bytes = is.readAllBytes();
			is.close();
			return new ImageIcon(bytes);
		} catch (IOException ex) {
			throw new IllegalArgumentException();
		}
	}
}
