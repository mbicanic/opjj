package hr.fer.zemris.java.hw11.jnotepadpp.local;

import java.util.Locale;
import java.util.ResourceBundle;

/**
 * LocalizationProvider is a full implementation of the {@link ILocalizationProvider}
 * interface.
 * 
 * It is designed using the Singleton design pattern - a LocalizationProvider
 * cannot be instantiated.
 * 
 * By default, the localization language is English.
 * 
 * @author Miroslav Bićanić
 */
public class LocalizationProvider extends AbstractLocalizationProvider {
	/** The currently used language */
	private String language;
	/** The ResourceBundle used for translation */
	private ResourceBundle bundle;
	/** The single existing instance of a LocalizationProvider */
	private static final LocalizationProvider lp = new LocalizationProvider();
	
	/**
	 * A constructor for a LocalizationProvider
	 */
	private LocalizationProvider() {
		language = "en";
		bundle = ResourceBundle.getBundle("hr.fer.zemris.java.hw11.jnotepadpp.i18n.translate", Locale.forLanguageTag(language));
	}
	
	/**
	 * Returns the {@link LocalizationProvider}
	 * @return the instance of a {@link LocalizationProvider}
	 */
	public static LocalizationProvider getInstance() {
		return lp;
	}
	
	/**
	 * Sets the language to the given {@code language}
	 * @param language the language code of the language to set
	 */
	public void setLanguage(String language) {
		bundle = ResourceBundle.getBundle(
				"hr.fer.zemris.java.hw11.jnotepadpp.i18n.translate", 
				Locale.forLanguageTag(language));
		this.language = language;
		fire();
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getString(String key) {
		return bundle.getString(key);
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getCurrentLanguage() {
		return language;
	}
}
