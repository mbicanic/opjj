package hr.fer.zemris.java.hw11.jnotepadpp.local;

/**
 * An interface for listeners that listen for changes
 * in localization.
 * 
 * @author Miroslav Bićanić
 */
public interface ILocalizationListener {
	
	/**
	 * Invoked whenever the localization changes.s
	 */
	void localizationChanged();
}
