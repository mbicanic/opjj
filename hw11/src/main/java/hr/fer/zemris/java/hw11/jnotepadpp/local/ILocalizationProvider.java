package hr.fer.zemris.java.hw11.jnotepadpp.local;

/**
 * Interface for objects that provide translations for localizable
 * actions and objects, as well as information about the localization.
 * 
 * The provider is a subject in the observer design pattern,
 * capable of registering and notifying listeners whenever
 * the localization changes.
 * 
 * @author Miroslav Bićanić
 */
public interface ILocalizationProvider {
	
	/**
	 * Returns the localized string associated with the given {@code key}
	 * @param key the key to which a translation is associated
	 * @return the correctly localized value associated with the given key
	 */
	String getString(String key);
	/**
	 * Returns the code of the currently used language.
	 * @return the language code
	 */
	String getCurrentLanguage();
	/**
	 * Registers a {@link ILocalizationListener} to the provider
	 * @param l the {@link ILocalizationListener} to register to the provider
	 */
	void addLocalizationListener(ILocalizationListener l);
	/**
	 * Removes a {@link ILocalizationListener} from the provider
	 * @param l the {@link ILocalizationListener} to remove from the provider
	 */
	void removeLocalizationListener(ILocalizationListener l);
}	
