package hr.fer.zemris.java.hw11.jnotepadpp.util;

import java.text.Collator;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;

import hr.fer.zemris.java.hw11.jnotepadpp.JNotepadPP;
import hr.fer.zemris.java.hw11.jnotepadpp.local.ILocalizationProvider;

/**
 * Class providing with static methods used by {@link JNotepadPP}
 * for working with text.
 * 
 * @author Miroslav Bićanić
 */
public class TextUtilities {

	/**
	 * Removes all duplicate lines from the given list of lines,
	 * keeping the first occurrence and joins the remaining
	 * lines into a String.
	 * 
	 * @param lines the list of lines
	 * @return the remaining lines joined into a string
	 */
	public static String removeDuplicates(List<String> lines) {
		List<String> unique = new ArrayList<String>();
		lines.forEach(line -> {
			if(!unique.contains(line)) {
				unique.add(line);
			}
		});
		String result = String.join("", unique);
		return result.substring(0, result.length()-1);
	}

	/**
	 * Sorts the lines according to the rules of the current language
	 * and joins the sorted lines.
	 * 
	 * @param lines the lines to sort
	 * @param ascending sort ascending if true; descending if false
	 * @param flp an {@link ILocalizationProvider} for getting the current language
	 * @return the sorted lines joined into a string
	 */
	public static String sortLines(List<String> lines, boolean ascending, ILocalizationProvider flp) {
		Locale locale = new Locale(flp.getCurrentLanguage());
		Collator col = Collator.getInstance(locale);
		Comparator<Object> comp = ascending ? col : col.reversed();
		lines.sort(comp);
		String result = String.join("", lines);
		return result.substring(0, result.length()-1);
	}

	/**
	 * Turns all characters from the given array into either uppercase
	 * or lowercase, returning a string made of them.
	 * 
	 * @param chars the array of characters
	 * @param upper turns characters to uppercase if true; turns to lower otherwise
	 * @return a string comprised of the characters
	 */
	public static String switchCase(char[] chars, boolean upper) {
		for(int i=0; i<chars.length; i++) {
			chars[i] = upper ? 
					Character.toUpperCase(chars[i]) : Character.toLowerCase(chars[i]);
		}
		return new String(chars);
	}
	
	/**
	 * Swaps the case of every character in the given array,
	 * returning a string made of the array.
	 * 
	 * @param chars the array of characters
	 * @return a string made of characters with inverted case
	 */
	public static String toggleCase(char[] chars) {
		for(int i=0; i<chars.length; i++) {
			chars[i] = Character.isUpperCase(chars[i]) ? 
					Character.toLowerCase(chars[i]) : Character.toUpperCase(chars[i]);
		}
		return new String(chars);
	}
}
