package hr.fer.zemris.java.hw11.jnotepadpp;

import static hr.fer.zemris.java.hw11.jnotepadpp.util.TextUtilities.removeDuplicates;
import static hr.fer.zemris.java.hw11.jnotepadpp.util.TextUtilities.sortLines;
import static hr.fer.zemris.java.hw11.jnotepadpp.util.TextUtilities.switchCase;

import java.awt.Component;
import java.awt.Toolkit;
import java.awt.Desktop.Action;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.event.ActionEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTextArea;
import javax.swing.text.BadLocationException;
import javax.swing.text.Caret;
import javax.swing.text.Document;
import javax.swing.text.Element;
import javax.swing.text.JTextComponent;

import hr.fer.zemris.java.hw11.jnotepadpp.local.FormLocalizationProvider;
import hr.fer.zemris.java.hw11.jnotepadpp.local.LocalizationProvider;
import hr.fer.zemris.java.hw11.jnotepadpp.model.MultipleDocumentModel;
import hr.fer.zemris.java.hw11.jnotepadpp.model.SingleDocumentModel;
import hr.fer.zemris.java.hw11.jnotepadpp.util.TextUtilities;

/**
 * AbstractNotepadPP is a specification of {@link JFrame} towards
 * achieving the functionality of {@link JNotepadPP}.
 * 
 * It stores methods used by the actions of {@link JNotepadPP},
 * as well as some utility methods. 
 * 
 * @author Miroslav Bićanić
 */
public abstract class AbstractNotepadPP extends JFrame {
	private static final long serialVersionUID = 1L;
	
	/**
	 * The underlying {@link MultipleDocumentModel} 
	 */
	protected MultipleDocumentModel mdl;
	/**
	 * The object used for localization
	 */
	protected FormLocalizationProvider flp;
	
	/**
	 * Constructor for an AbstractNotepadPP
	 */
	protected AbstractNotepadPP() {
		
		flp = new FormLocalizationProvider(LocalizationProvider.getInstance(), this);
		flp.addLocalizationListener(()->{
			if(mdl.getCurrentDocument()!=null && mdl.getCurrentDocument().getFilePath()==null) {
				setTitle(flp.getString("nonam"));
			}
			for(int i=0, n = mdl.getNumberOfDocuments(); i<n; i++) {
				SingleDocumentModel doc = mdl.getDocument(i);
				doc.setModified(doc.isModified()); //forces tab name update, for translation
			}
		});
		
		this.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				checkForUnsavedFiles();
			}
		});
	}
	
	/**
	 * Method for saving documents to disk, called whenever a {@link Component}
	 * with a registered saveDocument {@link Action} fires an {@link ActionEvent}.
	 * 
	 * @param p the path to which to save the document; can be null
	 * @param model the {@link SingleDocumentModel} to save
	 */
	protected void saveDocument(Path p, SingleDocumentModel model) {
		if(p==null) {
			while(true) {
				p = chooseFile("m_notsave", "savefile", false);
				if(p==null) {
					return;
				}
				if(Files.exists(p)) {
					int ans = popConfirmDialog(flp.getString("q_over"));
					if(ans==0) {
						break;
					} else if(ans==1) {
						continue;
					} else {
						return;
					}
				} 
				break;
			}
		}
		try {
			mdl.saveDocument(model, p);
		} catch (IllegalArgumentException ex) {
			popErrorMsg(flp.getString(ex.getMessage()));
			return;
		}
		popMessage(flp.getString("m_saved"));
	}

	/**
	 * Method for loading documents from the disk, called whenever a
	 * {@link Component} with a registered loadDocument {@link Action}
	 * fires an {@link ActionEvent}.
	 */
	protected void loadDocument() {
		Path toLoad = chooseFile("m_notload", "openfile", true);
		if(toLoad==null) {
			return;
		}
		try {
			mdl.loadDocument(toLoad);
		} catch (IllegalArgumentException ex) {
			popErrorMsg(flp.getString(ex.getMessage()));
		}
	}

	/**
	 * Attempts to close all files, asking the user for permission
	 * to close each file that has been modified and hasn't been
	 * saved, closing the window itself in the end.
	 */
	protected void checkForUnsavedFiles() {
		for(SingleDocumentModel sdm : mdl) {
			if(!checkOneUnsavedFile(sdm)) {
				return;
			}
		}
		int len = mdl.getNumberOfDocuments();
		for(int i = len-1; i >=0 ; i--) {
			mdl.closeDocument(mdl.getDocument(i));
		}
		dispose();
	}
	
	/**
	 * Checks if the given {@link SingleDocumentModel} has been modified,
	 * offering the user to save it if it has.
	 * 
	 * @param sdm a {@link SingleDocumentModel} to check
	 * @return true if the file wasn't modified, if the user saved it
	 * 			or if the user doesn't want to save it; false otherwise
	 */
	protected boolean checkOneUnsavedFile(SingleDocumentModel sdm) {
		if(sdm.isModified()) {
			String filename = sdm.getFilePath()==null ? flp.getString("nonam") : sdm.getFilePath().getFileName().toString();
			int answer = popConfirmDialog(flp.getString("q_save")+" "+filename+"?");
			if(answer==1) { 		// 'no'
				return true;		//user doesn't want to save and doesn't care
			} else if(answer==0) { 
				saveDocument(sdm.getFilePath(), sdm);	//'yes'
				return true;
			} else { 			// 'cancel'
				return false;	//user gave up on closing because file isn't saved
			}
		}
		return true; //file wasn't modified
	}
	
	/**
	 * Calculates statistical data for the currently opened document:
	 * 	-length 	- length of file (in characters)
	 * 	-line 		- the number of the line in which the caret is located
	 * 	-column 	- the number of the column in which the caret is located
	 * 	-selection 	- the length of the selected text segment
	 */
	protected void calculateStatistics() {
		JTextArea jta = mdl.getCurrentDocument().getTextComponent();
		char[] text = jta.getText().toCharArray();
		int chars=text.length;
		int lines=0;
		try {
			lines = jta.getLineOfOffset(chars) + 1; //0-based
		} catch (BadLocationException ignored) {}
		
		int nonBlanks=chars;
		for(char c : text) {
			if(c=='\n' || c=='\t' || c==' ') {
				nonBlanks--;
			}
		}
		popMessage(String.format("%s %d %s, %d %s %s %d %s.", 
				flp.getString("m_stat"), chars, flp.getString("chars"),
				nonBlanks, flp.getString("nempty"),
				flp.getString("and"), lines, flp.getString("lines")));
	}
	
	/**
	 * Stores the currently selected text into the system's clipboard.
	 * 
	 * @param cut if true, the selected text is removed from the document
	 * 			after storing to clipboard
	 */
	protected void putToClipboard(boolean cut) {
		JTextArea jta = mdl.getCurrentDocument().getTextComponent();
		Caret c = jta.getCaret();
		int start = Math.min(c.getDot(), c.getMark());
		int len = Math.abs(c.getDot() - c.getMark());
		if(len<1) return;
		
		String content = getSelection(jta, start, len, cut);
		Clipboard cbd = Toolkit.getDefaultToolkit().getSystemClipboard();
		StringSelection ssel = new StringSelection(content);
		cbd.setContents(ssel, null);
	}
	
	/**
	 * Pastes the system's clipboard's content into the document at
	 * the current position of the caret.
	 */
	protected void putFromClipboard() {
		JTextArea jta = mdl.getCurrentDocument().getTextComponent();
		Caret c = jta.getCaret();
		int start = Math.min(c.getDot(), c.getMark());
		int len = Math.abs(c.getMark()-c.getDot());
		
		Clipboard cbd = Toolkit.getDefaultToolkit().getSystemClipboard();
		try {
			String stored = (String)cbd.getData(DataFlavor.stringFlavor);
			setSelection(stored, jta, start, len);
		} catch (UnsupportedFlavorException ignored) {} 
		catch (IOException e) {
			popErrorMsg(flp.getString("e_cbd"));
		}
	}

	/**
	 * Turns all uppercase letters from the currently selected text
	 * into lowercase, and vice versa.
	 */
	protected void toggleCase() {
		JTextArea jta = mdl.getCurrentDocument().getTextComponent();
		Caret c = jta.getCaret();
		int start = Math.min(c.getDot(), c.getMark());
		int len = Math.abs(c.getDot() - c.getMark());
		if(len<1) return;
	
		String selection = getSelection(jta, start, len, true);
		setSelection(TextUtilities.toggleCase(selection.toCharArray()), jta, start, 0);
	}

	/**
	 * Turns all letters from the currently selected text to either
	 * uppercase or lowercase, depending on the given {@code upper}
	 * @param upper turns to uppercase if true; turns to lowercase otherwise
	 */
	protected void toCase(boolean upper) {
		JTextArea jta = mdl.getCurrentDocument().getTextComponent();
		Caret c = jta.getCaret();
		int start = Math.min(c.getDot(), c.getMark());
		int len = Math.abs(c.getDot() - c.getMark());
		if(len<1) return;
		setSelection(switchCase(getSelection(jta,start,len,true).toCharArray(), upper), jta, start, 0);
	}

	/**
	 * Places the {@code replacement} string into the given {@code jta}
	 * at the {@code start} position.
	 * 
	 * If the {@code len} parameter is larger than zero, then the 
	 * {@code replacement} is placed instead of the first {@code len}
	 * characters, starting form position {@code start}.
	 * 
	 * @param replacement the string to place into the {@link JTextArea}
	 * @param jta the {@link JTextArea} to which to place the string
	 * @param start the position at which to place the string
	 * @param len the amount of characters that will be replaced by the given string
	 */
	private void setSelection(String replacement, JTextComponent jta, int start, int len) {
		Document doc = jta.getDocument();
		try {
			if(len>0) {
				doc.remove(start, len);;
			}
			doc.insertString(start, replacement, null);
		} catch (BadLocationException ignored) {}
	}

	/**
	 * Extracts a string from the {@code jta} starting at position {@code start},
	 * and being {@code len} characters long.
	 * If the {@code cut} parameter is true, it also removes the extracted string
	 * from the {@link JTextArea}.
	 * 
	 * @param jta the {@link JTextArea} to extract the string from
	 * @param start the position from which to extract
	 * @param len the length of the string to extract
	 * @param cut if true the text is removed after copying; if false it is left
	 * @return the selected string
	 */
	private String getSelection(JTextComponent jta, int start, int len, boolean cut) {
		Document doc = jta.getDocument();
		try {
			String result = doc.getText(start, len);
			if(cut) {
				doc.remove(start, len);
			}
			return result;
		} catch (BadLocationException ignored) {
			return null; //method always called with Document's Caret's parameters -> this line never executes
		}
	}
	
	/**
	 * This method encompasses all three tools which operate over lines:
	 * 	- sorting in ascending order
	 * 	- sorting in descending order
	 * 	- deleting duplicate lines.
	 * 
	 * If {@code sort} is true, the method sorts the selected lines and
	 * places them back in the document. If {@code ascending} is true,
	 * then it sorts in ascending order. Otherwise, it sorts in descending.
	 * 
	 * If {@code sort} is false, then {@code ascending} is ignored, and the
	 * method removes duplicate lines from the currently selected lines.
	 * 
	 * @param sort if true performs sorting; otherwise removes duplicates
	 * @param ascending if {@code sort} is true, used to determine order of sorting
	 */
	protected void lineTools(boolean sort, boolean ascending) {
		JTextComponent jtc = mdl.getCurrentDocument().getTextComponent();
		int start = Math.min(jtc.getCaret().getDot(), jtc.getCaret().getMark());
		int end = Math.max(jtc.getCaret().getDot(), jtc.getCaret().getMark());
		
		Document doc = jtc.getDocument();
		Element root = doc.getDefaultRootElement();
		
		int rowFirstLine = root.getElementIndex(start);
		int rowLastLine = root.getElementIndex(end);
		int firstStartOffset = root.getElement(rowFirstLine).getStartOffset();
		int lastEndOffset = root.getElement(rowLastLine).getEndOffset();
		
		List<String> lines = new ArrayList<>();
		for(int r = rowFirstLine; r <= rowLastLine; r++) {
			int lineStart = root.getElement(r).getStartOffset();
			int lineEnd = root.getElement(r).getEndOffset();
			try {
				lines.add(doc.getText(lineStart, lineEnd-lineStart));	
			} catch(BadLocationException ignored) {}
		}
		String result = sort ? sortLines(lines, ascending, flp) : removeDuplicates(lines);
		try {
			doc.remove(firstStartOffset, lastEndOffset-firstStartOffset-1);
			doc.insertString(firstStartOffset, result, null);
		} catch (BadLocationException ignored) {}
	}

	/**
	 * Displays an error message linked to this AbstractNotepadPP,
	 * with a localized title and buttons.
	 * 
	 * The given {@code message} must already be localized.
	 * 
	 * @param message the message to display
	 */
	private void popErrorMsg(String message){
		String[] comps = new String[] {flp.getString("ok")};
		JOptionPane.showOptionDialog(this, 
				message, 
				flp.getString("error"),
				JOptionPane.OK_OPTION, 
				JOptionPane.ERROR_MESSAGE, 
				IconRetriever.getInstance().fetch(IconRetriever.ERROR), 
				comps, 
				comps[0]);
	}
	
	/**
	 * Displays a confirm dialogue linked to this AbstractNotepadPP,
	 * with a localized title and buttons.
	 * 
	 * The given {@code question} must already be localized.
	 * 
	 * @param question the question to display
	 * @return 0 if the answer was yes, 1 if the answer was no, 2 if the answer was cancel
	 */
	private int popConfirmDialog(String question) {
		String[] comps = new String[] {flp.getString("yes"), flp.getString("no"), flp.getString("cancel")};
		int res = JOptionPane.showOptionDialog(this,
				question, 
				flp.getString("confirm"), 
				JOptionPane.YES_NO_CANCEL_OPTION, 
				JOptionPane.QUESTION_MESSAGE, 
				IconRetriever.getInstance().fetch(IconRetriever.QUESTION), 
				comps, 
				comps[0]);
		return res;
	}
	
	/**
	 * Displays an information message linked to this AbstractNotepadPP,
	 * with a localized title and buttons.
	 * 
	 * @param msg the message to display 
	 */
	private void popMessage(String msg) {
		String[] comps = new String[] {flp.getString("ok")};
		JOptionPane.showOptionDialog(this,
				msg, 
				flp.getString("info"), 
				JOptionPane.OK_OPTION, 
				JOptionPane.INFORMATION_MESSAGE, 
				IconRetriever.getInstance().fetch(IconRetriever.INFO), 
				comps, 
				comps[0]);
	}

	/**
	 * Opens a {@link JFileChooser} dialogue for choosing files, and returns
	 * the selected file.
	 * 
	 * If no file was selected, the method returns null.
	 * 
	 * @param key the localization key for the message to be displayed if no file was chosen
	 * @param title the title of the file chooser dialogue (save / open)
	 * @param open if true, method opens a dialogue for opening files; 
	 * 			otherwise a dialogue for saving files is opened
	 * @return the selected Path
	 */
	private Path chooseFile(String key, String title, boolean open) {
		JFileChooser jfc = new JFileChooser();
		jfc.setDialogTitle(flp.getString(title));
		int res = open ? jfc.showOpenDialog(this) : jfc.showSaveDialog(this);
		
		if(res!=JFileChooser.APPROVE_OPTION) {
			popMessage(flp.getString(key));
			return null;
		}
		return jfc.getSelectedFile().toPath();
	}
}
