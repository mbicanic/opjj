package hr.fer.zemris.java.custom.collections.demo;
import hr.fer.zemris.java.custom.collections.SimpleHashtable;

import java.util.Iterator;
public class SimpleHashtableDemo {
	public static void main(String[] args) {
		
		SimpleHashtable<String, Integer> examMarks = 
				new SimpleHashtable<>(2);
		examMarks.put("Ivana", 2);
		examMarks.put("Ante", 2);
		examMarks.put("Jasna", 2);
		examMarks.put("Kristina", 5);
		examMarks.put("Ivana", 5);
		
		for(SimpleHashtable.TableEntry<String, Integer> t : examMarks) {
			System.out.printf("%s => %d%n", t.getKey(), t.getValue());
		}
		
		for(SimpleHashtable.TableEntry<String, Integer> t : examMarks) {
			for(SimpleHashtable.TableEntry<String, Integer> e : examMarks) {
				System.out.printf(
						"(%s => %d) - (%s => %d)%n",
						t.getKey(), t.getValue(),
						e.getKey(), e.getValue()
					);
			}
		}
		System.out.println("########################################################");

		/*
		Iterator<SimpleHashtable.TableEntry<String, Integer>> iter = examMarks.iterator();
		while(iter.hasNext()) {
			SimpleHashtable.TableEntry<String, Integer> pair = iter.next();
			if(pair.getKey().equals("Ivana")) {
				iter.remove();
			}
		} 
		
		Iterator<SimpleHashtable.TableEntry<String, Integer>> iter = examMarks.iterator();
		while(iter.hasNext()) {
			SimpleHashtable.TableEntry<String, Integer> pair = iter.next();
			if(pair.getKey().equals("Ivana")) {
				iter.remove();
				try {
					iter.remove();
				} catch (IllegalStateException ex) {
					System.out.println("gotcha");
					break;
				}
			}
		}
		
		
		
		Iterator<SimpleHashtable.TableEntry<String, Integer>> iter = examMarks.iterator();
		while(iter.hasNext()) {
			SimpleHashtable.TableEntry<String, Integer> pair = iter.next();
			if(pair.getKey().equals("Ivana")) {
				examMarks.remove("Ivana");
			}
		}
		*/
		
		
		Iterator<SimpleHashtable.TableEntry<String, Integer>> iter = examMarks.iterator();
		while(iter.hasNext()) {
			SimpleHashtable.TableEntry<String, Integer> pair = iter.next();
			System.out.printf("%s => %d%n", pair.getKey(), pair.getValue());
			iter.remove();
		}
		System.out.printf("Veličina: %d.%n", examMarks.size());
	
	}
}
