package hr.fer.zemris.java.custom.collections;

/**
 * A parametrised interface of an object processor.
 * 
 * A processor takes an object and does something with
 * it. All classes implementing this interface must
 * define a process(T value) method defining what is the
 * action over an object.
 * 
 * @author Miroslav Bićanić
 *
 */
@FunctionalInterface
public interface Processor<T> {
	
	/**
	 * This method takes a <code>value</code> and does
	 * something with it.
	 * 
	 * All classes extending Processor must provide an 
	 * implementation of this method.
	 * 
	 * @param value The value with which something has to be done.
	 */
	public void process(T value);
}
