package hr.fer.zemris.java.custom.collections.demo;


import hr.fer.zemris.java.custom.collections.ArrayIndexedCollection;
import hr.fer.zemris.java.custom.collections.ElementsGetter;

public class DemoGenerics {
	public static void main(String[] args) {
		ArrayIndexedCollection<String> col
		= new ArrayIndexedCollection<>();
		ArrayIndexedCollection<Integer> col2
		= new ArrayIndexedCollection<>();
	
		col.add("pero");
		col.add("jure");
		col2.add(2);
		col2.add(10);
		
		System.out.println(col.contains(2));
		System.out.println(col2.contains("2"));
		col.remove(Integer.valueOf(2));
		col.remove("2");
		col.remove("pero");
		col.add("pero");
		col.add("tana");
		ElementsGetter<String> iterator = col.createElementsGetter();
		while(iterator.hasNextElement()) {
			System.out.println(iterator.getNextElement());
		}
		
	}
}
