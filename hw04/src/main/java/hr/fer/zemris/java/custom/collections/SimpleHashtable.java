package hr.fer.zemris.java.custom.collections;

import java.util.ConcurrentModificationException;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.Objects;

/**
 * This class models a simple hashtable that stores
 * data in key-value pairs.
 * 
 * Overflow in slots is solved by linking elements from
 * the same slot in a list.
 * 
 * The contract of this hashtable is:
 * -Keys are NOT allowed to be null
 * -Values CAN be null.
 * -Adding an entry with a key already existing in 
 * 		the table overwrites the previous value.
 * 
 * @author Miroslav Bićanić
 *
 * @param <K> The type of keys maintained by this map
 * @param <V> The type of values contained in this map
 */
public class SimpleHashtable<K,V> implements Iterable<SimpleHashtable.TableEntry<K,V>>{
	/**
	 * The default number of slots in the table.
	 */
	private static final int defaultCapacity = 16;
	
	/**
	 * Once the ratio of filled slots to all slots reaches
	 * this constant, the table reallocates itself to twice
	 * the size.
	 */
	private static final double THRESHOLD = 0.75;
	/**
	 * An array of entries that represents the table for this
	 * hashtable. 
	 * 
	 * The length of this array is the number of slots in 
	 * the table.
	 */
	private TableEntry<K,V>[] table;
	/**
	 * The number of entries in this hashtable.
	 */
	private int size; 
	
	/**
	 * A counter of structural modifications to the hashtable (adding
	 * a pair, reallocation...).
	 */
	private long modificationCount;
	
	/**
	 * A default constructor for a hashtable, creating it
	 * with the default capacity.
	 */
	public SimpleHashtable() {
		this(defaultCapacity);
	}
	/**
	 * A constructor for a hashtable taking a predefined value
	 * for its initial capacity.
	 * 
	 * @param initialCapacity The desired initial capacity of the hashtable
	 * @throws IllegalArgumentException if the given capacity is smaller than 1
	 */
	@SuppressWarnings("unchecked")
	public SimpleHashtable(int initialCapacity) {
		if(initialCapacity < 1) {
			throw new IllegalArgumentException("Initial capacity can't be smaller than 1!");
		}
		initialCapacity = closestPowerOf2(initialCapacity);
		table = (TableEntry<K,V>[])new TableEntry[initialCapacity];
		size = 0;
		modificationCount = 0;
	}
	
	/**
	 * A helper class modelling a single entry in the hashtable.
	 * 
	 * It represents a key-value pair, and has a pointer to the
	 * next entry in the same slot, in case of overflow.
	 * 
	 * @author Miroslav Bićanić
	 *
	 * @param <K> The type of the key of the entry
	 * @param <V> The type of the value of the entry
	 */
	public static class TableEntry<K,V> { //K and V here have NOTHING to do with K and V from hashmap
		/**
		 * The key under which the value is stored
		 */
		private K key;
		/**
		 * The value stored under the key of this entry
		 */
		private V value;
		/**
		 * The next TableEntry in the same slot.
		 */
		private TableEntry<K,V> next;
		
		/**
		 * A constructor for a TableEntry taking a key and a value.
		 * 
		 * @param key The key of the newly constructed TableEntry
		 * @param value The value stored under that key
		 */
		public TableEntry(K key, V value) {
			this.key = key;
			this.value = value;
			this.next = null;
		}
		/**
		 * @return The value under this key
		 */
		public V getValue() {
			return value;
		}
		/**
		 * @param value The new value to be stored under this entry's key
		 */
		public void setValue(V value) {
			this.value = value;
		}
		/**
		 * @return The key of this TableEntry
		 */
		public K getKey() {
			return key;
		}
		/**
		 * Returns this entry as a string in the
		 * KEY=VALUE format.
		 */
		@Override
		public String toString() {
			return key.toString()+"="+value.toString();
		}
	}
	
	/**
	 * This method says if the hashtable is empty.
	 * @return true if the table is empty, false otherwise
	 */
	public boolean isEmpty() {
		return size==0;
	}
	/**
	 * This method returns the current number of entries stored
	 * in the hashtable.
	 * 
	 * @return The number of pairs in the hashtable
	 */
	public int size() {
		return size;
	}
	
	/**
	 * This method creates a new entry based on the given key 
	 * and value, and puts it into the hashtable.
	 * 
	 * If an entry with the same key already exists, it 
	 * overwrites its value to the value given as the argument.
	 * 
	 * @param key The key of the new entry
	 * @param value The value of the new entry
	 * @throws NullPointerException if the given key is null
	 */
	public void put(K key, V value) {
		Objects.requireNonNull(key);
		
		if(size>=THRESHOLD*table.length) {
			grow();
		}
		
		TableEntry<K,V> entry = new TableEntry<>(key, value);
		
		if(emptySlot(key)) {
			table[slotID(key)] = entry;
			size++;
			modificationCount++;
			return;
		}
		
		TableEntry<K,V> head = table[slotID(key)];
		if(head.key.equals(key)) {
			head.value=value;
			return;
		}
		
		TableEntry<K,V> precedingEntry = stopAtPreviousEntry(key);
		if(precedingEntry.next!=null) {
			precedingEntry.next.value = value;
			return;
		}
		
		precedingEntry.next = entry;
		size++;
		modificationCount++;
		return;
	}
	
	/**
	 * This method reallocates the underlying array that stores
	 * all the slots to an array twice the size, returning
	 * all existing entries to their (likely new) slots.
	 */
	@SuppressWarnings("unchecked")
	private void grow() {
		Object[] entries = new Object[size];
		int index = 0;
		for(TableEntry<K,V> t : table) {
			while(t!=null) {
				entries[index] = t;
				index++;
				t = t.next;
			}
		}
		modificationCount++;
		table = (TableEntry<K,V>[])new TableEntry[table.length*2];
		size=0;
		for(Object o : entries) {
			TableEntry<K,V> t = (TableEntry<K,V>)o;
			put(t.key, t.value);
		}
	}
	
	/**
	 * This method returns the value stored under the given key
	 * in this hashtable.
	 * 
	 * If an entry with the given key doesn't exist, it returns
	 * null.
	 * Note: Since this hashtable allows null values, a null 
	 * 		result can be ambiguous - it can mean a value
	 * 		doesn't exist OR that a null value is stored under
	 * 		the given key.
	 * @param key The key under which to search in the table
	 * @return The value under the given key; null if entry with such key doesn't exist
	 */
	public V get(Object key) {
		if(key==null) {
			return null;
		}
		TableEntry<K,V> node = stopAtEntry(key);
		return node==null ? null : node.value;
	}
	
	/**
	 * This method removes an entry with the given key.
	 * 
	 * If such an entry doesn't exist, or a null key
	 * is given, it does nothing.
	 * 
	 * @param key The key the entry of which must be removed
	 */
	public void remove(Object key) {
		if(key==null) {
			return;
		}
		if(table[slotID(key)]==null) {
			return;
		}
		trySlotRemove(key);
	}
	
	/**
	 * This method searches for and removes a key from a slot.
	 * It assumes that presence of elements in the slot has
	 * already been checked.
	 * @param key
	 */
	private void trySlotRemove(Object key) {
		
		if(table[slotID(key)].key.equals(key)) {
			table[slotID(key)] = table[slotID(key)].next;
			size--;
			modificationCount++;
			return;
		}
		TableEntry<K,V> precedingEntry = stopAtPreviousEntry(key);
		if(precedingEntry.next==null) {
			return;
		}
		
		TableEntry<K,V> temp = precedingEntry.next.next;
		precedingEntry.next = temp;
		size--;
		modificationCount++;
	}
	
	/**
	 * This method tells if an entry with the given key exists
	 * in the hashtable.
	 * 
	 * @param key The key to look for in the table
	 * @return true if an entry with the key exists, false otherwise
	 */
	public boolean containsKey(Object key) {
		if(key==null) {
			return false;
		}
		return stopAtEntry(key)==null ? false : true;
	}
	
	/**
	 * This method tells if an entry with the given value exists
	 * in the hashtable.
	 * 
	 * @param value The value to look for in the table
	 * @return true if an entry with the key exists, false otherwise
	 */
	public boolean containsValue(Object value) {
		for(TableEntry<K,V> t : table) {
			while(t!=null && !areEqual(t.value, value)) {
				t = t.next;
			}
			if(t!=null && areEqual(t.value, value)) {
				return true;
			}
		}
		return false;
	}
	
	/**
	 * This method returns a string representation of this
	 * hashtable in the format:
	 * "[key1=value1, key2=value2, ... keyN=valueN]"
	 */
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder(size*5);
		sb.append("[");
		for(int i = 0; i < table.length; i++) {
			TableEntry<K,V> t = table[i];
			if(t==null) {
				continue;
			}
			sb.append(t.toString());
			while(t.next!=null) {
				sb.append(", ");
				sb.append(t.next.toString());
				t = t.next;
			}
			if(i==table.length-1) {
				sb.append("]");
			} else {
				sb.append(", ");
			}
		}
		return sb.toString();
	}
	
	/**
	 * This method removes all existing entries from this table,
	 * not affecting the number of slots in the process.
	 */
	public void clear() {
		for(int i = 0; i < table.length; i++) {
			table[i]=null;
		}
		size = 0;
		modificationCount++;
	}
	
	/**
	 * This method goes through a slot and returns the entry
	 * whose next entry is stored under the given key.
	 * 
	 * If an entry with the given key doesn't exist, it returns
	 * the last entry in the slot.
	 * 
	 * Used in methods that make structural changes and need
	 * to link entries properly. (SimpleHashmap::remove, 
	 * SimpleHashmap::IteratorImpl::remove).
	 * 
	 * @param key The key to look for in a slot
	 * @return The entry previous to the one looked for, or the last entry in the slot
	 */
	private TableEntry<K,V> stopAtPreviousEntry(Object key) {
		TableEntry<K,V> root = table[slotID(key)];
		while(root.next!=null && !root.next.key.equals(key)) {
			root = root.next;
		}
		return root;
	}
	
	/**
	 * This method goes through a slot and returns the entry
	 * that has the given key.
	 * 
	 * If such an entry doesn't exist, it returns null.
	 * 
	 * Used in methods that only need to reach a certain element,
	 * whether to compare it or to fetch it.
	 * 
	 * @param key The key to look for in the hashtable
	 * @return The entry with the given key, null if nonexistant
	 */
	private TableEntry<K,V> stopAtEntry(Object key) {
		int slotID = slotID(key);
		TableEntry<K,V> root = table[slotID];
		if(root == null) {
			return null;
		}
		if(root.key.equals(key)) {
			return root;
		}
		while(!root.key.equals(key)) {
			root = root.next;
			if(root==null) {
				return null;
			}
		}
		return root;
	}
	/**
	 * This method calculates the closest power of 2 larger
	 * than the initial capacity desired by the user.
	 * 
	 * @param initialCapacity The initial capacity of the table the user specified
	 * @return The first larger number that is a power of 2
	 */
	private int closestPowerOf2(int initialCapacity) {
		int number = 1;
		while(initialCapacity>number) {
			number*=2;
		}
		return number;
	}
	
	/**
	 * This method calculates the index of the underlying array
	 * where a new entry under the given key must be placed.
	 * @param key
	 * @return
	 */
	private int slotID(Object key) { 
		return Math.abs(key.hashCode()) % table.length;
	}
	
	/**
	 * This method is a shorthand for checking if the slot
	 * where this key would go is empty or not.
	 * 
	 * @param key The key that determines the slot
	 * @return true if the slot is empty, false otherwise
	 */
	private boolean emptySlot(Object key) {
		return table[slotID(key)]==null;
	}
	
	/**
	 * A shorthand method for checking if two values are equal.
	 * 
	 * Separated into own method because of handling null values,
	 * which would look unclear as conditions in if and while
	 * expressions.
	 * 
	 * @param val1 The first value to compare (the value from an entry)
	 * @param val2 The second value to compare (the value user gave)
	 * @return true if the values are equal, false otherwise
	 */
	private boolean areEqual(Object val1, Object val2) {
		if(val1==null && val2==null) {
			return true;
		}
		if(val1==null || val2==null) {
			return false;
		}
		return val1.equals(val2);
	}
	
	/**
	 * This method returns an iterator over this hashtable.
	 */
	@Override
	public Iterator<TableEntry<K, V>> iterator() {
		return new IteratorImpl();
	}
	
	/**
	 * This class is an implementation of an iterator over a hashtable.
	 * 
	 * The iterator returns entries from the table in an order: it goes
	 * slot by slot, and in each slot, in the order of how the entries
	 * were added.
	 * 
	 * Altering the hashtable while there is an active iterator will make
	 * the iterator throw a ConcurrentModificationException on a call of 
	 * any of its methods.
	 * Elements can only be removed, and only through the iterator, in order
	 * to avoid this.
	 * 
	 * Attempting to remove an element twice, or to remove an element before 
	 * any element was returned results in an IllegalStateException.
	 * 
	 * Calling the {@code next} method when there are no more elements results
	 * in a NoSuchElementException being thrown.
	 * 
	 * @author Miroslav Bićanić
	 */
	private class IteratorImpl implements Iterator<SimpleHashtable.TableEntry<K, V>> {
		
		/**
		 * The index of the slot this iterator is currently in.
		 */
		private int slotIndex;
		/**
		 * The current entry the iterator is supposed to return,
		 * or from which it continues searching for the next entry.
		 */
		private TableEntry<K,V> current;
		/**
		 * The last entry of the hashtable this iterator has 
		 * returned. Used to support removal of elements.
		 */
		private TableEntry<K,V> lastReturned;
		/**
		 * The number of modifications the hashtable that created
		 * this iterator had at the moment of creating it.
		 */
		private long savedModCount;
		
		/**
		 * A default constructor for a hashtable iterator.
		 */
		private IteratorImpl() {
			this.slotIndex = 0;
			this.current = table[slotIndex];
			while(current==null && ++slotIndex < table.length) {
				current = table[slotIndex];
			}
			this.savedModCount = modificationCount;
		}
		
		/**
		 * This method tells if this iterator still has entries
		 * to return from the hashtable.
		 * 
		 * @throws ConcurrentModificationException if the hashtable has been altered
		 */
		@Override
		public boolean hasNext() {
			if(savedModCount!=modificationCount) {
				throw new ConcurrentModificationException();
			}
			return current!=null;
		}
		
		/**
		 * This method returns the next entry from this hashtable
		 * in the specified order.
		 * 
		 * @throws NoSuchElementException if there are no more elements
		 * @throws ConcurrentModificationException if the hashtable has been altered
		 */
		@Override
		public TableEntry<K, V> next() {
			if(savedModCount!=modificationCount) {
				throw new ConcurrentModificationException();
			}
			if(!hasNext()) {
				throw new NoSuchElementException();
			}
			lastReturned = current;
			current = current.next;
			while(current==null && ++slotIndex<table.length) {
				current = table[slotIndex];
			}
			return lastReturned;
		}
		
		/**
		 * This method removes the entry from the hashtable.
		 * It is the only legal way to remove an element from the
		 * hashtable while there is an iterator opened over it.
		 * 
		 * @throws ConcurrentModificationException if the hashtable has been altered
		 * @throws IllegalStateException 
		 * 				-if the method is called before any element was returned
		 * 				-if the method is called twice for the same element
		 */
		@Override
		public void remove() {
			if(savedModCount!=modificationCount) {
				throw new ConcurrentModificationException();
			}
			if(lastReturned==null) {
				throw new IllegalStateException("Cannot remove before returning any element!");
			}
			int tableSizeBefore = size;
			trySlotRemove(lastReturned.key);
			
			if(size==tableSizeBefore) {
				throw new IllegalStateException("Cannot remove an entry with the same key twice!");
			}
			savedModCount++;
		}
		
	}
}
