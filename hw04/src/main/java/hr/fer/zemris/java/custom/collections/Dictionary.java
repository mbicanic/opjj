package hr.fer.zemris.java.custom.collections;

import java.util.Objects;

/**
 * This class models a dictionary (map, associative array)
 * that stores elements by their key in key -> value format.
 * 
 * Values are allowed to be null values, but keys are not.
 * 
 * @author Miroslav Bićanić
 */
public class Dictionary<K,V> {
	/**
	 * An ArrayIndexedCollection used to internally store entries.
	 */
	private ArrayIndexedCollection<Entry<K,V>> dict;
	
	/**
	 * This class models one entry of the dictionary,
	 * having a key and a value.
	 * 
	 * @author Miroslav Bićanić
	 */
	private static class Entry<X,Y> {
		/**
		 * A key by which we access elements
		 */
		X key;
		/**
		 * The value this entry stores under its key
		 */
		Y value;
		/**
		 * A constructor for an Entry taking a key and a value.
		 * @param key The key to access this entry
		 * @param value The value stored in this entry
		 */
		public Entry(X key, Y value) {
			this.key = key;
			this.value = value;
		}
		
		@Override
		public int hashCode() {
			return Objects.hash(key, value);
		}
		@SuppressWarnings("rawtypes")
		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (!(obj instanceof Entry))
				return false;
			Entry other = (Entry) obj;
			return Objects.equals(key, other.key) && Objects.equals(value, other.value);
		}
	}
	
	/**
	 * A default constructor for a dictionary
	 */
	public Dictionary() {
		this.dict = new ArrayIndexedCollection<Entry<K,V>>();
	}
	
	/**
	 * This method says if the dictionary is empty.
	 * @return true if there are no entries, false otherwise
	 */
	public boolean isEmpty() {
		return dict.isEmpty();
	}
	
	/**
	 * This method returns the number of entries in the dictionary.
	 * @return The number of entries
	 */
	public int size() {
		return dict.size();
	}
	
	/**
	 * This method deletes all entries from the dictionary.
	 */
	public void clear() {
		dict.clear();
	}
	
	/**
	 * This method adds the given value to the dictionary under 
	 * the certain key.
	 * 
	 * If an entry with the same key was present in the dictionary,
	 * its value is rewritten. Otherwise, a new entry is added.
	 * 
	 * @param key The key to access the handed value
	 * @param value The value to be stored under the given key
	 */
	public void put(K key, V value) {
		Objects.requireNonNull(key);
		ElementsGetter<Entry<K,V>> iter = dict.createElementsGetter();
		while(iter.hasNextElement()) {
			Entry<K,V> e = (Entry<K,V>)iter.getNextElement();
			if(e.key.equals(key)) {
				e.value = value;
				return;
			}
		}
		dict.add(new Entry<>(key, value));
	}
	
	/**
	 * This method returns the value stored under the given key
	 * in this dictionary.
	 * 
	 * @param key The key to search for in the dictionary
	 * @return The value stored under that key, null if doesn't exist
	 */
	public V get(Object key) {
		ElementsGetter<Entry<K,V>> iter = dict.createElementsGetter();
		while(iter.hasNextElement()) {
			Entry<K,V> e = (Entry<K,V>)iter.getNextElement();
			if(e.key.equals(key)) {
				return e.value;
			}
		}
		return null;
	}

	@Override
	public int hashCode() {
		return Objects.hash(dict);
	}

	/**
	 * This method says if this dictionary is equal to a given Object.
	 * 
	 * Two dictionaries are equal if for the same keys they have the 
	 * same values, while having an equal number of elements.
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Dictionary))
			return false;
		
		Dictionary<?,?> other = (Dictionary<?,?>) obj;
		if(size()!=other.size()) {
			return false;
		}
		if(size()==0) {
			return true;
		}
		
		boolean equal = true;		
		ElementsGetter<Entry<K,V>> it = dict.createElementsGetter();
		while(it.hasNextElement()) {
			Entry<K,V> thisEntry = it.getNextElement();
			if(!other.get(thisEntry.key).equals(thisEntry.value)) {
				equal = false;
				break;
			}
		}
		return equal;
	}
	
}

