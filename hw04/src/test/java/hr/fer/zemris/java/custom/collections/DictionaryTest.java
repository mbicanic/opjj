package hr.fer.zemris.java.custom.collections;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class DictionaryTest {

	private Dictionary<String,Integer> giveMe(){
		Dictionary<String, Integer> d = new Dictionary<>();
		d.put("marko", 1);
		d.put("pero", 2);
		d.put("ivana", 5);
		d.put("petra", 4);
		return d;
	}
	
	@Test
	void testConstructorDictionary() {
		Dictionary<String, Integer> d = new Dictionary<>();
		assertEquals(0, d.size());
		assertTrue(d.isEmpty());
	}
	
	@Test
	void testPutNullKey() {
		Dictionary<String, Integer> d = new Dictionary<>();
		assertThrows(NullPointerException.class, () -> d.put(null, 16));
	}
	@Test
	void testPutNullValue() {
		Dictionary<String, Integer> d = new Dictionary<>();
		d.put("marko", null);
		assertEquals(null, d.get("marko"));
	}
	@Test
	void testGetNonExistant() {
		Dictionary<String, Integer> d = new Dictionary<>();
		d.put("marko", 1);
		assertEquals(null,  d.get("pero"));
	}
	@Test
	void testPutMultiple() {
		Dictionary<String, Integer> d = giveMe();
		
		assertEquals(1, d.get("marko"));
		assertEquals(2, d.get("pero"));
		assertEquals(5, d.get("ivana"));
		assertEquals(4, d.get("petra"));
		
		assertEquals(null, d.get("stjepan"));
	}
	@Test
	void testOverwrite() {
		Dictionary<String, Integer> d = giveMe();
		d.put("marko", 5);
		assertEquals(5, d.get("marko"));
	}
	@Test
	void testEquality() {
		Dictionary<String, Integer> d = giveMe();
		Dictionary<String, Integer> test = d;
		assertEquals(d, test);
		test = null;
		assertNotEquals(d, test);
		assertNotEquals(d, "marko");
		
		test = new Dictionary<String,Integer>();
		assertEquals(new Dictionary<String,Integer>(), test);
		assertNotEquals(d,test);

		test.put("pero", 2);
		test.put("ivana", 5);
		test.put("marko", 1);
		assertNotEquals(d, test);
		test.put("petra", 4);
		assertEquals(d,test);
		test.put("stjepan", 3);
		assertNotEquals(d,test);
	}

}
