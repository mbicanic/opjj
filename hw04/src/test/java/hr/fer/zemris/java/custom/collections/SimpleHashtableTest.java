package hr.fer.zemris.java.custom.collections;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ConcurrentModificationException;
import java.util.Iterator;
import java.util.NoSuchElementException;

import org.junit.jupiter.api.Test;

class SimpleHashtableTest {

	private SimpleHashtable<String,Integer> giveMe(){
		SimpleHashtable<String,Integer> s = new SimpleHashtable<>(3);
		s.put("Ivana", 2);
		s.put("Ante", 5);
		s.put("Jasna", 4);
		s.put("Kristina", 3);
		s.put("Jurica", 1);
		s.put("Toni", 4);
		return s;
	}
	
	@Test
	void testDefaultConstructor() {
		SimpleHashtable<String, Integer> s = new SimpleHashtable<>();
		assertEquals(0, s.size());
		assertTrue(s.isEmpty());
		assertFalse(s.containsValue(null));
	}
	@Test
	void testCapacityConstructor() {
		assertThrows(IllegalArgumentException.class, ()-> new SimpleHashtable<String,Integer>(0));
		SimpleHashtable<String, Integer> s = new SimpleHashtable<>(3);
		assertEquals(0, s.size());
		assertTrue(s.isEmpty());
		assertFalse(s.containsValue(null));
	}
	@Test
	void testReallocationDoesntCrash() {
		SimpleHashtable<String, Integer> s = new SimpleHashtable<>(1); //capacity = 1
		assertEquals(0, s.size());
		assertTrue(s.isEmpty());
		s.put("Pero", 1);		//size(0) < 0.75 * capacity(1), no reallocation
		s.put("Jura", 3);		//size(1) > 0.75 * capacity(1), reallocation to 2
		s.put("Mara", 5);		//size(2) > 0.75 * capacity(2), reallocation to 4
		s.put("Klara", 2);		//size(3) = 0.75 * capacity(4), reallocation to 8
		s.put("Antonela", 4);		//size(4) < 0.75 * capacity(8), no reallocation
		assertEquals(5, s.size());
		assertFalse(s.isEmpty());
	}
	@Test
	void testPutThrows() {
		SimpleHashtable<String,Integer> s = new SimpleHashtable<String, Integer>();
		assertThrows(NullPointerException.class, ()-> s.put(null, 20));
	}
	@Test
	void testGetByKey() {
		SimpleHashtable<String, Integer> s = giveMe();
		assertEquals(6, s.size());
		assertEquals(2, s.get("Ivana"));
		assertEquals(5, s.get("Ante"));
		assertEquals(1, s.get("Jurica"));
		assertEquals(null, s.get("Trpimir"));
		assertEquals(null, s.get(null));
	}
	@Test
	void testPutOverwrite() {
		SimpleHashtable<String, Integer> s = giveMe();
		assertEquals(6, s.size());
		s.put("Ivana", 5);
		s.put("Jasna", 2);
		s.put("Kristina", 1);
		s.put("Toni", 5);
		
		assertEquals(5, s.get("Ivana"));
		assertEquals(2, s.get("Jasna"));
		assertEquals(1, s.get("Kristina"));
		assertEquals(5, s.get("Toni"));
		
		assertEquals(6, s.size());
		
		s.put("Zvonko", 5);
		assertEquals(7, s.size());
	}
	@Test
	void testContainsKey() {
		SimpleHashtable<String,Integer> s = giveMe();
		assertTrue(s.containsKey("Ivana"));
		assertTrue(s.containsKey("Ante"));
		assertTrue(s.containsKey("Kristina"));
		assertFalse(s.containsKey(null));
		assertFalse(s.containsKey("Zvonko"));
	}
	@Test
	void testContainsValue() {
		SimpleHashtable<String,Integer> s = giveMe();
		
		assertTrue(s.containsValue(2));
		assertEquals(2, s.get("Ivana"));
		s.put("Ivana", 5);
		assertFalse(s.containsValue(2));
		
		assertTrue(s.containsValue(5));
		assertTrue(s.containsValue(4));
		assertTrue(s.containsValue(3));
		assertTrue(s.containsValue(1));
		
		assertFalse(s.containsValue(10));
		assertFalse(s.containsValue(null));
		
		s.put("NullGuy", null);
		assertTrue(s.containsValue(null));
	}
	@Test
	void testRemoveByKey() {
		SimpleHashtable<String,Integer> s = giveMe();
		int size = s.size();
		assertEquals(6, size);
		
		s.remove("Jurica");
		assertFalse(s.containsKey("Jurica"));
		assertFalse(s.containsValue(1));
		assertEquals(--size, s.size());
		
		s.remove("Ivana");
		assertFalse(s.containsKey("Ivana"));
		assertFalse(s.containsValue(2));
		assertEquals(--size, s.size());
		
		s.remove("Ivana");
		assertEquals(size, s.size());
		s.remove(null);
		assertEquals(size, s.size());
		
		s.remove("Ante");
		s.remove("Jasna");
		s.remove("Kristina");
		s.remove("Toni");
		assertTrue(s.isEmpty());
		assertEquals(0, s.size());
	}
	@Test
	void testClearTable() {
		SimpleHashtable<String,Integer> s = giveMe();
		s.clear();
		assertEquals(0, s.size());
		assertTrue(s.isEmpty());
	}
	
	@Test
	void testIterator() {
		SimpleHashtable<String,Integer> s = giveMe();
		Iterator<SimpleHashtable.TableEntry<String, Integer>> iter = s.iterator();
		int iterations = 0;
		while(iter.hasNext()) {
			SimpleHashtable.TableEntry<String, Integer> e = iter.next();
			iterations++;
			/*
			 * The order is deterministic as all new entries are added to the
			 * table in the same order as in the instructions. All new values
			 * added (Jurica, Toni) didn't provoke the reallocation of the
			 * table, thus maintaining the order Ivana-Jasna-Kristina in the
			 * same slot as in the original table.
			 */
			if(e.getKey().equals("Ivana")) {
				e = iter.next();
				iterations++;
				assertEquals(e.getKey(), "Jasna");
				e = iter.next();
				iterations++;
				assertEquals(e.getKey(), "Kristina");
			}
		}
		assertEquals(6, iterations);
	}
	
	@Test
	void testIteratorThrowsWhenEndIsReached() {
		SimpleHashtable<String,Integer> s = new SimpleHashtable<>();
		Iterator<SimpleHashtable.TableEntry<String, Integer>> iter = s.iterator();
		assertFalse(iter.hasNext());
		assertThrows(NoSuchElementException.class, ()->iter.next());
		
		s = giveMe();
		Iterator<SimpleHashtable.TableEntry<String,Integer>> iter2 = s.iterator();
		iter2.next();
		iter2.next();
		iter2.next();
		iter2.next();
		iter2.next();
		iter2.next();
		assertThrows(NoSuchElementException.class, ()-> iter.next());
	}
	@Test
	void testIteratorThrowsForConcurrentRemove() {
		SimpleHashtable<String,Integer> s = giveMe();
		Iterator<SimpleHashtable.TableEntry<String, Integer>> i = s.iterator();
		/*
		 * I manually tested to know the first two elements are Jurica and
		 * Ante. Then I try to remove an element the iterator already removed,
		 * one it didn't yet remove.
		 */
		i.next();
		i.next();
		s.remove("Jurica");
		assertThrows(ConcurrentModificationException.class, ()->i.hasNext());
		
		s = giveMe();
		Iterator<SimpleHashtable.TableEntry<String, Integer>> i2 = s.iterator();
		i2.next();
		s.remove("Ante");
		assertThrows(ConcurrentModificationException.class, ()->i2.next());
		
		s = giveMe();
		Iterator<SimpleHashtable.TableEntry<String, Integer>> i3 = s.iterator();
		i3.next();
		i3.next();
		s.remove("Toni");
		assertThrows(ConcurrentModificationException.class, ()->i3.next());
	}
	@Test
	void testIteratorThrowsForConcurrentAdd() {
		SimpleHashtable<String,Integer> s = giveMe();
		Iterator<SimpleHashtable.TableEntry<String, Integer>> i = s.iterator();
		i.next();
		s.put("Zoran", 12);
		assertThrows(ConcurrentModificationException.class, ()->i.next());
	}
	@Test
	void testIteratorIgnoresOverwrite() {
		SimpleHashtable<String,Integer> s = giveMe();
		s.remove("Ante");
		Iterator<SimpleHashtable.TableEntry<String, Integer>> i = s.iterator();
		i.next(); //Jurica
		i.next(); //Toni
		s.put("Ivana", 5);
		assertEquals(5, i.next().getValue());
		assertEquals("Jasna", i.next().getKey());
	}
	@Test
	void testIteratorThrowsForClear() {
		SimpleHashtable<String,Integer> s = giveMe();
		Iterator<SimpleHashtable.TableEntry<String, Integer>> i = s.iterator();
		i.next();
		s.clear();
		assertThrows(ConcurrentModificationException.class, ()->i.next());
	}
	@Test
	void testIteratorRemovesCorrectly() {
		SimpleHashtable<String,Integer> s = giveMe();
		Iterator<SimpleHashtable.TableEntry<String, Integer>> i = s.iterator();
		i.next(); //Jurica
		i.next(); //Ante
		i.remove();
		assertFalse(s.containsKey("Ante"));
		assertNull(s.get("Ante"));
		assertEquals("Toni", i.next().getKey());
		i.remove();
		assertEquals("Ivana", i.next().getKey());
	}
	@Test
	void testIteratorRemoveWhenOpenedThrows() {
		SimpleHashtable<String, Integer> s = giveMe();
		Iterator<SimpleHashtable.TableEntry<String, Integer>> i = s.iterator();
		assertThrows(IllegalStateException.class, ()->i.remove());
	}
	
	@Test
	void testIteratorDoubleRemoveThrows() {
		SimpleHashtable<String, Integer> s = giveMe();
		Iterator<SimpleHashtable.TableEntry<String, Integer>> i = s.iterator();
		i.next();
		i.next();
		i.next();
		i.remove();
		assertThrows(IllegalStateException.class, ()->i.remove());
	}
}
