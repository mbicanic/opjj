package hr.fer.zemris.math;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class Vector2DTest {

	@Test
	void testConstructor() {
		Vector2D v = new Vector2D(0, 0);
		assertEquals(0, v.getX());
		assertEquals(0, v.getY());
	}
	@Test
	void testEquality() {
		Vector2D v1 = new Vector2D(15.12345678, 12.12345678);
		Vector2D v2 = new Vector2D(15.12345678, 12.12345669);
		assertEquals(v1, v2);
		Vector2D v3 = new Vector2D(15.12345678, 12.12345665);
		assertNotEquals(v1, v3);
	}
	
	@Test
	void testTranslateThrows() {
		Vector2D vSource = new Vector2D(5,8);
		assertThrows(NullPointerException.class,()-> vSource.translate(null));
		assertThrows(NullPointerException.class,()-> vSource.translated(null));
	}
	@Test
	void testTranslation() {
		Vector2D vSource = new Vector2D(5,8);
		Vector2D offset = new Vector2D(5, -4);
		Vector2D translated = vSource.translated(offset);
		assertEquals(new Vector2D(10, 4), translated);
		vSource.translate(offset);
		assertEquals(translated, vSource);
		offset.translate(new Vector2D(-15,0));
		vSource.translate(offset);
		assertEquals(new Vector2D(0,0), vSource);
	}
	
	@Test
	void testRotation() {
		Vector2D vSource = new Vector2D(1,1);
		double angle = Math.PI;
		Vector2D rotated = vSource.rotated(angle);
		assertEquals(new Vector2D(-1,-1), rotated);
		vSource.rotate(angle);
		assertEquals(rotated, vSource);
		vSource.rotate(-angle);
		assertEquals(new Vector2D(1,1), vSource);
		vSource = new Vector2D(1, 0);
		angle /= 3;
		vSource.rotated(angle);
		assertEquals(new Vector2D(1./2, Math.sqrt(3)/2), vSource.rotated(angle));
		assertEquals(new Vector2D(-1./2, Math.sqrt(3)/2), vSource.rotated(angle).rotated(angle));
	}
	
	@Test
	void testScaling() {
		Vector2D vSource = new Vector2D(10,10);
		double scaler = 11.271;
		Vector2D scaled = vSource.scaled(scaler);
		assertEquals(new Vector2D(112.71, 112.71), scaled);
		vSource.scale(scaler);
		assertEquals(scaled, vSource);
		vSource.rotate(Math.PI);
		scaler = 1./112.71;
		vSource.scale(scaler);
		assertEquals(new Vector2D(-1,-1), vSource);
		assertEquals(new Vector2D(0,0), vSource.scaled(0));
		assertThrows(IllegalArgumentException.class, ()-> vSource.scale(-5));
	}

}
