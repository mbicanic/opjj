package hr.fer.zemris.java.custom.scripting.parser;

import static org.junit.jupiter.api.Assertions.*;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

import org.junit.jupiter.api.Test;

import hr.fer.zemris.java.custom.scripting.nodes.DocumentNode;
import hr.fer.zemris.java.custom.scripting.nodes.EchoNode;
import hr.fer.zemris.java.custom.scripting.nodes.ForLoopNode;
import hr.fer.zemris.java.custom.scripting.nodes.TextNode;

class SmartScriptParserTest {

	@Test
	void testMainExample() {
		String doc = loader("doc1.txt");
		SmartScriptParser p = new SmartScriptParser(doc);
		DocumentNode dn = p.getDocumentNode();
		assertEquals(4, dn.numberOfChildren());
		assertEquals(3, dn.getChild(1).numberOfChildren());
		assertEquals(5, dn.getChild(3).numberOfChildren());
		assertTrue(dn.getChild(0) instanceof TextNode);
		assertTrue(dn.getChild(1) instanceof ForLoopNode);
		assertTrue(dn.getChild(2) instanceof TextNode);
		assertTrue(dn.getChild(3) instanceof ForLoopNode);
		
		ForLoopNode fn1 = (ForLoopNode)dn.getChild(1);
		ForLoopNode fn2 = (ForLoopNode)dn.getChild(3);
		assertTrue(fn1.getChild(0) instanceof TextNode);
		assertTrue(fn1.getChild(1) instanceof EchoNode);
		assertTrue(fn1.getChild(2) instanceof TextNode);
		
		assertTrue(fn2.getChild(0) instanceof TextNode);
		assertTrue(fn2.getChild(1) instanceof EchoNode);
		assertTrue(fn2.getChild(2) instanceof TextNode);
		assertTrue(fn2.getChild(3) instanceof EchoNode);
		assertTrue(fn2.getChild(4) instanceof TextNode);
	}
	
	@Test
	void testLeavingClosableTagUnclosed() {
		String doc = loader("error1.txt");
		assertThrows(SmartScriptParserException.class, ()-> new SmartScriptParser(doc));
	}
	@Test
	void testClosingUnclosableTag() {
		String doc = loader("error2.txt");
		assertThrows(SmartScriptParserException.class, ()-> new SmartScriptParser(doc));
	}
	@Test
	void testUnsupportedTagName() {
		String doc = loader("error3.txt");
		assertThrows(SmartScriptParserException.class, ()-> new SmartScriptParser(doc));
	}
	@Test
	void testIllegalArgumentsForTag() {
		String doc1 = loader("error4a.txt");	//completely illegal type
		assertThrows(SmartScriptParserException.class, ()-> new SmartScriptParser(doc1));
		String doc2 = loader("error4b.txt");	//variable not given at first place of for tag
		assertThrows(SmartScriptParserException.class, ()-> new SmartScriptParser(doc2));
		String doc3 = loader("error4c.txt");	//operator inside for tag
		assertThrows(SmartScriptParserException.class, ()-> new SmartScriptParser(doc3));
		String doc4 = loader("error4d.txt");	//function inside for tag
		assertThrows(SmartScriptParserException.class, ()-> new SmartScriptParser(doc4));
	}
	@Test
	void testTooManyArgumentsForTag() {
		String doc = loader("error5.txt");
		assertThrows(SmartScriptParserException.class, ()-> new SmartScriptParser(doc));
	}
	@Test
	void testEOFInForTag() {
		String doc = loader("error7.txt");
		assertThrows(SmartScriptParserException.class, ()-> new SmartScriptParser(doc));
	}

	@Test
	void testIllegalArgumentEchoTag() {
		String doc = loader("error7.5.txt");
		assertThrows(SmartScriptParserException.class, ()-> new SmartScriptParser(doc));
	}
	@Test
	void testEOFInEchoTag() {
		String doc = loader("error8.txt");
		assertThrows(SmartScriptParserException.class, ()-> new SmartScriptParser(doc));		
	}
	@Test
	void testEOFInEndTag() {
		String doc = loader("error9.txt");
		assertThrows(SmartScriptParserException.class, ()-> new SmartScriptParser(doc));
	}
	@Test
	void testEndTagWithArguments() {
		String doc = loader("error10.txt");
		assertThrows(SmartScriptParserException.class, ()-> new SmartScriptParser(doc));
	}
	
	private String loader(String filename) {
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		try(InputStream is = this.getClass().getClassLoader().getResourceAsStream(filename)) {
			byte[] buffer = new byte[1024];
			while(true) {
				int read = is.read(buffer);
				if(read<1) {
					break;
				}
				bos.write(buffer, 0, read);
			}
			return new String(bos.toByteArray(), StandardCharsets.UTF_8);
		} catch (IOException ex) {
			return null;
		}
	}

}
