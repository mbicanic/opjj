package hr.fer.zemris.java.custom.scripting.elems;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Objects;

import org.junit.jupiter.api.Test;

class ElementTest {
	
	@Test
	void blankElementConstructor(){
		Element blank = new Element();
		String space = new String(" ");
		assertEquals("", blank.asText());
		assertEquals(space, blank.toString());
	}
	@Test
	void blankElementEquals() {
		Element blank = new Element();
		assertEquals(Objects.hash(new String()), blank.hashCode());
		assertEquals(blank, new Element());
	}
	
	@Test
	void doubleElementConstructor() {
		ElementConstantDouble ecd = new ElementConstantDouble(2.19);
		assertEquals("2.19", ecd.asText());
		assertEquals("2.19 ", ecd.toString());
	}
	@Test
	void intElementConstructor() {
		ElementConstantInteger ecd = new ElementConstantInteger(-3);
		assertEquals("-3", ecd.asText());
		assertEquals("-3 ", ecd.toString());
	}
	@Test
	void varElementConstructor() {
		ElementVariable ecd = new ElementVariable("var1");
		assertEquals("var1", ecd.asText());
		assertEquals("var1 ", ecd.toString());
	}
	@Test
	void stringElementConstructor() {
		ElementString ecd = new ElementString("a string");
		assertEquals("a string", ecd.asText());
		assertEquals("\"a string\" ", ecd.toString());
	}
	@Test
	void operatorElementConstructor() {
		ElementOperator ecd = new ElementOperator("+");
		assertEquals("+", ecd.asText());
		assertEquals("+ ", ecd.toString());
	}
	@Test
	void functionElementConstructor() {
		ElementFunction ecd = new ElementFunction("parse");
		assertEquals("parse", ecd.asText());
		assertEquals("@parse ", ecd.toString());
	}
	
	/*
	 * Element won't reject faulty construction (variable with name
	 * "0219__", operator with value "yes"...) - but only if the
	 * parser sees the entry is correct, an element is being created
	 * in the first place.
	 * Because of that, here I will only test if equality between them
	 * works properly.
	 */
	
	@Test
	void testDoubleEquals() {
		ElementConstantDouble[] nums = getDoubles();
		assertEquals(new ElementConstantDouble(12.0), nums[0]);
		assertNotEquals(get120String(), nums[0]);
		assertNotEquals(getIntegers()[0], nums[0]);
		assertNotEquals(nums[0], nums[1]);
		assertEquals(new ElementConstantDouble(Math.PI), nums[3]);
		assertEquals(new ElementConstantDouble(13.29), nums[1]);
	}
	@Test
	void testIntegerEquals() {
		ElementConstantInteger[] nums = getIntegers();
		assertEquals(new ElementConstantInteger(-5), nums[1]);
		assertNotEquals(get12String(), nums[0]);
		assertNotEquals(getDoubles()[0], nums[0]);
		assertNotEquals(nums[0], nums[1]);
		assertEquals(new ElementConstantInteger(0), nums[2]);
	}
	@Test
	void testFunctionEquals() {
		ElementFunction s = getSine();
		ElementFunction c = getCos();
		ElementString s2 = getSineString();
		assertNotEquals(s, c);
		assertNotEquals(s, s2);
		assertEquals(new ElementFunction("sin"), s);
	}
	@Test
	void testOperatorEquals() {
		ElementOperator plus = getPlus();
		ElementOperator pow = getPower();
		assertNotEquals(plus, pow);
		assertEquals(new ElementOperator("+"), plus);
	}
	@Test
	void testVariableEquals() {
		ElementVariable sinVar = sinGetVar();
		ElementString sinStr = getSineString();
		ElementVariable cnt = cntGetVar();
		assertNotEquals((Element)cnt, (Element)sinVar);
		assertNotEquals((Element)sinStr, (Element)sinVar);
		assertEquals(new ElementVariable("cnt"), cnt);
	}
	@Test
	void testStringEquals() {
		assertEquals(new ElementString("sin"), getSineString());
		assertNotEquals(getSineString(), getSine());
		assertNotEquals(sinGetVar(), getSine());
		assertEquals("es\\cap\"e me!", getEscape().asText());
		assertEquals("\"es\\\\cap\\\"e me!\" ", getEscape().toString());
	}
	
	
	
	//##################################################################//
	public static ElementConstantDouble[] getDoubles() {
		return new ElementConstantDouble[] {
				new ElementConstantDouble(12.0),
				new ElementConstantDouble(13.29),
				new ElementConstantDouble(-4.18),
				new ElementConstantDouble(Math.PI)
		};
	}
	public static ElementConstantInteger[] getIntegers() {
		return new ElementConstantInteger[] {
				new ElementConstantInteger(12),
				new ElementConstantInteger(-5),
				new ElementConstantInteger(0),
		};
	}
	public static ElementFunction getSine() {
		return new ElementFunction("sin");
	}
	public static ElementFunction getCos() {
		return new ElementFunction("cos");
	}
	public static ElementVariable sinGetVar() {
		return new ElementVariable("sin");
	}
	public static ElementVariable cntGetVar() {
		return new ElementVariable("cnt");
	}
	public static ElementOperator getPlus() {
		return new ElementOperator("+");
	}
	public static ElementOperator getPower() {
		return new ElementOperator("^");
	}
	public static ElementString getSineString() {
		return new ElementString("sin");
	}
	public static ElementString get120String() {
		return new ElementString("12.0");
	}
	public static ElementString get12String() {
		return new ElementString("12");
	}
	public static ElementString getEscape() {
		return new ElementString("es\\cap\"e me!");
	}
	

}
