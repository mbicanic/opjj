package hr.fer.zemris.java.custom.scripting.nodes;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;

import hr.fer.zemris.java.custom.scripting.elems.*;

class NodeTest {

	@Test
	void blankNodeConstructor() {
		Node blank = new Node();
		assertEquals(new Node(), blank);
		assertNotEquals(new DocumentNode(), blank);
		assertEquals(0, blank.numberOfChildren());
	}
	@Test
	void addChildToNodeTest() {
		Node blank = new Node();
		blank.addChildNode(new Node());
		assertEquals(1, blank.numberOfChildren());
		assertEquals(new Node(), blank.getChild(0));
		assertNotEquals(new Node(), blank);
	}
	@Test
	void numberOfChildrenTest() {
		Node blank = new Node();
		blank.addChildNode(new DocumentNode());
		blank.getChild(0).addChildNode(new EchoNode());
		blank.getChild(0).addChildNode(new EchoNode());
		blank.addChildNode(new Node());
		blank.addChildNode(new TextNode("Dobar dan!"));
		assertEquals(3, blank.numberOfChildren());
		assertEquals(2, blank.getChild(0).numberOfChildren());
	}
	
	@Test
	void testTextNode() {
		TextNode a = new TextNode("Some cool text\ngoing across.");
		TextNode b = new TextNode("It {$ should be \\ escaped.");
		assertEquals(a, b); //only the number of child nodes counts - the structure!
		assertEquals("Some cool text\ngoing across.", a.toString());
		assertEquals("It \\{$ should be \\\\ escaped.", b.toString());
	}
	
	@Test
	void testForLoopNode() {
		assertThrows(NullPointerException.class, ()-> new ForLoopNode(null, null, null, null));
		ForLoopNode a = new ForLoopNode
				(new ElementVariable("var"),
				new ElementConstantDouble(12.4),
				new ElementString("a"),
				new ElementConstantInteger(5));
		assertNotEquals(a,new EchoNode());
		assertEquals("{$ FOR var 12.4 \"a\" 5 $}", a.toString());
	}	
	@Test
	void testEchoNode() {
		EchoNode a = new EchoNode
				(new ElementConstantDouble(12.4),
				new ElementVariable("var"),
				new ElementString("a\"\\a"),
				new ElementConstantInteger(5));
		assertEquals(a,new EchoNode());
		assertEquals("{$= 12.4 var \"a\\\"\\\\a\" 5 $}", a.toString());
	}
}
