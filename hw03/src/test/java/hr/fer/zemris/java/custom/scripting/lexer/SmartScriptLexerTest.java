package hr.fer.zemris.java.custom.scripting.lexer;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;


class SmartScriptLexerTest {
	public static SmartScriptLexer lex;
	@Test
	void testConstructorThrows() {
		assertThrows(NullPointerException.class, ()-> new SmartScriptLexer(null));
	}
	@Test
	void testEmptyString() {
		lex = new SmartScriptLexer("");
		assertEquals(SmartTokenType.EOF, lex.nextToken().getType());
		assertEquals(null, lex.getToken().getValue());
		assertThrows(SmartLexerException.class, ()-> lex.nextToken());
	}
	@Test
	void testGetAfterNext() {
		lex = new SmartScriptLexer("pero");
		assertEquals("pero", lex.nextToken().getValue());
		assertEquals("pero", lex.getToken().getValue());
	}
	@Test
	void testWhitespace() {
		lex = new SmartScriptLexer("  ");
		assertEquals(SmartTokenType.TEXT, lex.nextToken().getType());
		assertEquals("  ", lex.getToken().getValue());
		assertEquals(SmartTokenType.EOF, lex.nextToken().getType());
	}
	
	@Test
	void TEXTIndifferentToSymbols() {
		lex = new SmartScriptLexer("\\{$ @function + \\\\ - ;$}");
		assertEquals(new Token(SmartTokenType.TEXT, "{$ @function + \\ - ;$}"), lex.nextToken());
		assertEquals(SmartTokenType.EOF, lex.nextToken().getType());
	}
	@Test
	void testIrregularTextEscapes() {
		SmartScriptLexer works1 = new SmartScriptLexer("Regular, but \\{ useless!");
		SmartScriptLexer works2 = new SmartScriptLexer("Also regular \\{$ 12.");
		SmartScriptLexer works3 = new SmartScriptLexer("\\\\regular text");
		SmartScriptLexer works4 = new SmartScriptLexer("regular text \\\\");
		SmartScriptLexer throws1 = new SmartScriptLexer("irregular \\ text");
		SmartScriptLexer throws2 = new SmartScriptLexer("irregular \\n text");
		SmartScriptLexer throws3 = new SmartScriptLexer("irregular \\\" text");
		SmartScriptLexer throws4 = new SmartScriptLexer("irregular text\\");
		SmartScriptLexer throws5 = new SmartScriptLexer("irregular \\ text");
		assertEquals("Regular, but { useless!", works1.nextToken().getValue());
		assertEquals("Also regular {$ 12.", works2.nextToken().getValue());
		assertEquals("\\regular text", works3.nextToken().getValue());
		assertEquals("regular text \\", works4.nextToken().getValue());
		assertThrows(SmartLexerException.class, ()->throws1.nextToken().getValue());
		assertThrows(SmartLexerException.class,()-> throws2.nextToken().getValue());
		assertThrows(SmartLexerException.class,()-> throws3.nextToken().getValue());
		assertThrows(SmartLexerException.class,()-> throws4.nextToken().getValue());
		assertThrows(SmartLexerException.class,()-> throws5.nextToken().getValue());
	}
	
	@Test
	void testTagRecognition() {
		lex = new SmartScriptLexer(" {$name$}{$=$}");
		Token[] expected = new Token[] {
				new Token(SmartTokenType.TEXT, " "),
				new Token(SmartTokenType.SYMBOL, "{$"),
				new Token(SmartTokenType.KWD, "name"),
				new Token(SmartTokenType.SYMBOL, "$}"),
				new Token(SmartTokenType.SYMBOL, "{$"),
				new Token(SmartTokenType.KWD, "="),
				new Token(SmartTokenType.SYMBOL, "$}"),
		};
		iterateAndCheck(expected, lex);
	}
	@Test
	void testTagGivesKWD() {
		String doc1 = "{$ petnaest 12 kilo$}";
		String doc2 = "{$pet_na3st FOR i 1 10 12$}";
		String doc3 = "{$P_monkey_12 =$}";
		lex = new SmartScriptLexer(doc1);
		lex.nextToken();
		lex.setState(SmartLexerState.TAG);
		assertEquals(new Token(SmartTokenType.KWD, "petnaest"), lex.nextToken());
		lex = new SmartScriptLexer(doc2);
		lex.nextToken();
		lex.setState(SmartLexerState.TAG);
		assertEquals(new Token(SmartTokenType.KWD, "pet_na3st"), lex.nextToken());
		lex = new SmartScriptLexer(doc3);
		lex.nextToken();
		lex.setState(SmartLexerState.TAG);
		assertEquals(new Token(SmartTokenType.KWD, "P_monkey_12"), lex.nextToken());
		assertThrows(SmartLexerException.class, ()-> lex.nextToken());
	}
	@Test
	void namelessTagThrows() {
		String s1 = "{$ $}";
		String s2 = "{$$}";
		lex = new SmartScriptLexer(s1);
		lex.nextToken();
		lex.setState(SmartLexerState.TAG);
		assertThrows(SmartLexerException.class, ()->lex.nextToken());
		lex = new SmartScriptLexer(s2);
		lex.nextToken();
		lex.setState(SmartLexerState.TAG);
		assertThrows(SmartLexerException.class, ()->lex.nextToken());
	}
	@Test
	void invalidTagNameThrows() {
		String s1 = "{$12$}";
		String s2 = "{$+25$}";
		String s3 = "{$_zas$}";
		lex = new SmartScriptLexer(s1);
		lex.nextToken();
		lex.setState(SmartLexerState.TAG);
		assertThrows(SmartLexerException.class,()->lex.nextToken());
		lex = new SmartScriptLexer(s2);
		lex.nextToken();
		lex.setState(SmartLexerState.TAG);
		assertThrows(SmartLexerException.class,()->lex.nextToken());
		lex = new SmartScriptLexer(s3);
		lex.nextToken();
		lex.setState(SmartLexerState.TAG);
		assertThrows(SmartLexerException.class,()->lex.nextToken());
	}
	@Test
	void generatesAllFromTagTest() {
		lex = new SmartScriptLexer("{$ tag \"with\" @functions and 12 -4.12 + $}  ");
		lex.nextToken();
		lex.setState(SmartLexerState.TAG);
		assertEquals(new Token(SmartTokenType.KWD, "tag"), lex.nextToken());
		assertEquals(new Token(SmartTokenType.STRING, "with"), lex.nextToken());
		assertEquals(new Token(SmartTokenType.FUNCTION, "functions"), lex.nextToken());
		assertEquals(new Token(SmartTokenType.VAR, "and"), lex.nextToken());
		assertEquals(new Token(SmartTokenType.INT, 12), lex.nextToken());
		assertEquals(new Token(SmartTokenType.DOUBLE, -4.12), lex.nextToken());
		assertEquals(new Token(SmartTokenType.OPERATOR, "+"), lex.nextToken());
		assertEquals(new Token(SmartTokenType.SYMBOL, "$}"), lex.nextToken());
		assertEquals(SmartTokenType.EOF, lex.nextToken().getType());
	}
	@Test
	void illegalTagsCheck() {
		String one = "{$=@_f}";
		String two = "{$=@2f}";
		String three = "{$=\"rij\\ec\"$}";
		String four = "{$=\"krivi es\\{cape\"$}";
		String five = "{$=\"krivi es\\{cape\"$}";
		String six = "{$= \"krivi st";
		lex = new SmartScriptLexer(one);
		lex.nextToken();
		lex.setState(SmartLexerState.TAG);
		lex.nextToken();
		assertThrows(SmartLexerException.class, ()-> lex.nextToken());
		lex = new SmartScriptLexer(two);
		lex.nextToken();
		lex.setState(SmartLexerState.TAG);
		lex.nextToken();
		assertThrows(SmartLexerException.class,()-> lex.nextToken());
		lex = new SmartScriptLexer(three);
		lex.nextToken();
		lex.setState(SmartLexerState.TAG);
		lex.nextToken();
		assertThrows(SmartLexerException.class,()-> lex.nextToken());
		lex = new SmartScriptLexer(four);
		lex.nextToken();
		lex.setState(SmartLexerState.TAG);
		lex.nextToken();
		assertThrows(SmartLexerException.class,()-> lex.nextToken());
		lex = new SmartScriptLexer(five);
		lex.nextToken();
		lex.setState(SmartLexerState.TAG);
		lex.nextToken();
		assertThrows(SmartLexerException.class,()-> lex.nextToken());
		lex = new SmartScriptLexer(six);
		lex.nextToken();
		lex.setState(SmartLexerState.TAG);
		lex.nextToken();
		assertThrows(SmartLexerException.class,()-> lex.nextToken());
	}
	@Test
	void numberParsingTest() {
		lex = new SmartScriptLexer("{$= 12.345a1 3482396239847238734713847207836192 $}");
		lex.nextToken();
		lex.setState(SmartLexerState.TAG);
		lex.nextToken();
		assertEquals(new Token(SmartTokenType.DOUBLE, 12.345), lex.nextToken());
		assertEquals(new Token(SmartTokenType.VAR, "a1"), lex.nextToken());
		assertThrows(SmartLexerException.class,()-> lex.nextToken());
	}
	
	public static void iterateAndCheck(Token[] expected, SmartScriptLexer lex) {
		for(Token t : expected) {
			Token test = lex.nextToken();
			assertEquals(t, test);
			if(test.getValue().equals("{$")) {
				lex.setState(SmartLexerState.TAG);
			} else if(test.getValue().equals("$}")) {
				lex.setState(SmartLexerState.TEXT);
			}
		}
		assertEquals(SmartTokenType.EOF, lex.nextToken().getType());
	}
}
