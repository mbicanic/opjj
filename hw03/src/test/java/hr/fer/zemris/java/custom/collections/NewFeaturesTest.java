package hr.fer.zemris.java.custom.collections;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ConcurrentModificationException;
import java.util.NoSuchElementException;

import org.junit.jupiter.api.Test;

class NewFeaturesTest {

	@Test
	void testIteratorArray() {
		ArrayIndexedCollection col = new ArrayIndexedCollection();
		col.add(10);
		col.add(15);
		col.insert(5, 0);
		ElementsGetter iter = col.createElementsGetter();
		assertEquals(5, iter.getNextElement());
		assertEquals(10, iter.getNextElement());
		assertEquals(15, iter.getNextElement());
		assertThrows(NoSuchElementException.class, ()->iter.getNextElement());
		
		ElementsGetter iter2 = col.createElementsGetter();
		assertEquals(5, iter2.getNextElement());
		col.add(20);
		assertThrows(ConcurrentModificationException.class, ()-> iter2.getNextElement());
	}
	
	@Test
	void testIteratorLinked() {
		LinkedListIndexedCollection col = new LinkedListIndexedCollection();
		col.add(10);
		col.add(15);
		col.insert(5, 0);
		ElementsGetter iter = col.createElementsGetter();
		assertEquals(5, iter.getNextElement());
		assertEquals(10, iter.getNextElement());
		assertEquals(15, iter.getNextElement());
		assertThrows(NoSuchElementException.class, ()->iter.getNextElement());
		
		ElementsGetter iter2 = col.createElementsGetter();
		assertEquals(5, iter2.getNextElement());
		col.add(20);
		assertThrows(ConcurrentModificationException.class, ()-> iter2.getNextElement());
	}
	@Test
	void testForEachBoth() {
		StringBuilder sb = new StringBuilder(15);
		ArrayIndexedCollection col = new ArrayIndexedCollection();
		col.add(2);
		col.add("marko");
		col.add(12.5);
		LinkedListIndexedCollection col2 = new LinkedListIndexedCollection(col);
		
		col.forEach((o)->sb.append(o));
		assertEquals("2marko12.5", sb.toString());
		StringBuilder sb2 = new StringBuilder(15);
		col2.forEach((o)->sb2.append(o));
		assertEquals("2marko12.5", sb2.toString());
	}
	
	@Test
	void testEquality() {
		ArrayIndexedCollection col = new ArrayIndexedCollection();
		assertNotEquals(col, null);
		assertNotEquals("key", col);
		
		assertEquals(new ArrayIndexedCollection(), col);
		assertEquals(new LinkedListIndexedCollection(), col);
		
		col.add("val1");
		col.add("foo");
		ArrayIndexedCollection col2 = new ArrayIndexedCollection();
		col2.add("val1");
		col2.add("bar");
		assertNotEquals(col, col2);
		col2.remove("bar");
		col2.add("foo");
		assertEquals(col, col2);
		
		LinkedListIndexedCollection lcol = new LinkedListIndexedCollection(col);
		assertEquals(lcol, col);
	}
}
