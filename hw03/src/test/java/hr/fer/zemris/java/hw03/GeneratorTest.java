package hr.fer.zemris.java.hw03;

import static org.junit.jupiter.api.Assertions.*;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

import org.junit.jupiter.api.Test;

import hr.fer.zemris.java.custom.scripting.elems.ElementConstantDouble;
import hr.fer.zemris.java.custom.scripting.elems.ElementConstantInteger;
import hr.fer.zemris.java.custom.scripting.elems.ElementFunction;
import hr.fer.zemris.java.custom.scripting.elems.ElementString;
import hr.fer.zemris.java.custom.scripting.elems.ElementVariable;
import hr.fer.zemris.java.custom.scripting.nodes.DocumentNode;
import hr.fer.zemris.java.custom.scripting.nodes.EchoNode;
import hr.fer.zemris.java.custom.scripting.nodes.ForLoopNode;
import hr.fer.zemris.java.custom.scripting.nodes.TextNode;
import hr.fer.zemris.java.custom.scripting.parser.SmartScriptParser;

class GeneratorTest {
	/*
	 * A generator doesn't throw exceptions. The very fact parser
	 * generated a tree means the tree is correct and a file can
	 * be re-generated. 
	 */
	@Test
	void testTreeGeneration() {
		String expected = loader("genTest1.txt");
		
		DocumentNode d = new DocumentNode();
		TextNode tn1 = new TextNode("int i;\r\nint j;\r\n");
		ForLoopNode fn1 = new ForLoopNode(
				new ElementVariable("var"),
				new ElementConstantInteger(1),
				new ElementConstantInteger(10),
				new ElementConstantInteger(2));
			TextNode forTn1 = new TextNode("\r\n\tanother text\r\n");
			fn1.addChildNode(forTn1);
		TextNode tn2 = new TextNode("\r\n");
		EchoNode en1 = new EchoNode(
				new ElementVariable("echo1"),
				new ElementString("ec\"ho2"),
				new ElementConstantInteger(3260),
				new ElementConstantDouble(14.2),
				new ElementFunction("sin"));
		StringBuilder sb = new StringBuilder(100);
		
		d.addChildNode(tn1);
		d.addChildNode(fn1);
		d.addChildNode(tn2);
		d.addChildNode(en1);
		
		Generator.loop(d, sb);
		assertEquals(expected, sb.toString());
		
		SmartScriptParser p = new SmartScriptParser(expected);
		assertEquals(p.getDocumentNode(), d);
	}

	private String loader(String filename) {
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		try(InputStream is = this.getClass().getClassLoader().getResourceAsStream(filename)) {
			byte[] buffer = new byte[1024];
			while(true) {
				int read = is.read(buffer);
				if(read<1) {
					break;
				}
				bos.write(buffer, 0, read);
			}
			return new String(bos.toByteArray(), StandardCharsets.UTF_8);
		} catch (IOException ex) {
			return null;
		}
	}
}
