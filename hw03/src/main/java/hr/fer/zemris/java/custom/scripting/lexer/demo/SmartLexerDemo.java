package hr.fer.zemris.java.custom.scripting.lexer.demo;

import hr.fer.zemris.java.custom.scripting.lexer.SmartLexerState;
import hr.fer.zemris.java.custom.scripting.lexer.SmartScriptLexer;

/**
 * This is a small demonstration example for the SmartScriptLexer.
 * 
 * The first commented lines of code, are my
 * examples to see the working of the lexer.
 * After that follows the example from the 
 * instructions.
 * 
 * @author Miroslav Bićanić
 */
public class SmartLexerDemo {
	public static void main(String[] args) {
		//for basic testing
		
		/*
		String docNull = null;
		String docEmpty = "";
		String doc1 = "Today is a very nice day!";
		String doc2 = "Today \\\\is a very nice day!";
		String doc3 = "Today is a very nice day!\\";
		
		String doc4 = "Today is a very {$= i o 12 14.5 \"0.\\\\0\\\"0\" * @sin $}\\{$ day!\"";
		SmartScriptLexer lex2 = new SmartScriptLexer(doc4);
		System.out.println(lex2.nextToken());
		System.out.println(lex2.nextToken());
		lex2.setState(SmartLexerState.TAG);
		System.out.println(lex2.nextToken());
		System.out.println(lex2.nextToken());
		System.out.println(lex2.nextToken());
		System.out.println(lex2.nextToken());
		System.out.println(lex2.nextToken());
		System.out.println(lex2.nextToken());
		System.out.println(lex2.nextToken());
		System.out.println(lex2.nextToken());
		System.out.println(lex2.nextToken());
		lex2.setState(SmartLexerState.TEXT);
		System.out.println(lex2.nextToken());
		
		
		System.out.println("---------------------------------");
		*/
		//example from instructions
		String test = "This is sample text.\r\n"
				+ "{$ FOR i 1 10 1 $}  "
				+ "\r\n  This is {$= i $}-th time this message is generated.\r\n"
				+ "{$END$}\r\n"
				+ "{$FOR i 0 10 2 $}  \r\n"
				+ "sin({$=i$}^2) = {$= i i * @sin  \"0.000\" @decfmt $}\r\n"
				+ "{$END$}";
		
		SmartScriptLexer lex = new SmartScriptLexer(test);
		System.out.println(lex.nextToken());
		System.out.println(lex.nextToken());
		lex.setState(SmartLexerState.TAG);
		System.out.println(lex.nextToken());
		System.out.println(lex.nextToken());
		System.out.println(lex.nextToken());
		System.out.println(lex.nextToken());
		System.out.println(lex.nextToken());
		System.out.println(lex.nextToken());
		lex.setState(SmartLexerState.TEXT);
		System.out.println(lex.nextToken());
		System.out.println(lex.nextToken());
		lex.setState(SmartLexerState.TAG);
		System.out.println(lex.nextToken());
		System.out.println(lex.nextToken());
		System.out.println(lex.nextToken());
		lex.setState(SmartLexerState.TEXT);
		System.out.println(lex.nextToken());
		System.out.println(lex.nextToken());
		lex.setState(SmartLexerState.TAG);
		System.out.println(lex.nextToken());
		System.out.println(lex.nextToken());
		lex.setState(SmartLexerState.TEXT);
		System.out.println(lex.nextToken());
		System.out.println(lex.nextToken());
		lex.setState(SmartLexerState.TAG);
		System.out.println(lex.nextToken());
		System.out.println(lex.nextToken());
		System.out.println(lex.nextToken());
		System.out.println(lex.nextToken());
		System.out.println(lex.nextToken());
		System.out.println(lex.nextToken());
		lex.setState(SmartLexerState.TEXT);
		System.out.println(lex.nextToken());
		System.out.println(lex.nextToken());
		lex.setState(SmartLexerState.TAG);
		System.out.println(lex.nextToken());
		System.out.println(lex.nextToken());
		System.out.println(lex.nextToken());
		lex.setState(SmartLexerState.TEXT);
		System.out.println(lex.nextToken());
		System.out.println(lex.nextToken());
		lex.setState(SmartLexerState.TAG);
		System.out.println(lex.nextToken());
		System.out.println(lex.nextToken());
		System.out.println(lex.nextToken());
		System.out.println(lex.nextToken());
		System.out.println(lex.nextToken());
		System.out.println(lex.nextToken());
		System.out.println(lex.nextToken());
		System.out.println(lex.nextToken());
		lex.setState(SmartLexerState.TEXT);
		System.out.println(lex.nextToken());
		System.out.println(lex.nextToken());
		lex.setState(SmartLexerState.TAG);
		System.out.println(lex.nextToken());
		System.out.println(lex.nextToken());
	}
}
