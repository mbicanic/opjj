package hr.fer.zemris.java.hw03.prob1;

/**
 * An enumeration of all the possible types of Tokens
 * our simple lexer can generate.
 * 
 * @author Miroslav Bićanić
 */
public enum TokenType {
	/**
	 * Special token type that is generated from both
	 * LexerStates if the End Of File has been reached.
	 */
	EOF,
	/**
	 * This token represents a string of characters.
	 * Here, the definition is general and identical
	 * to the definition of a String in Java.
	 * 
	 * In the Lexer, the definition varies depending on
	 * the state the Lexer is in:
	 * In BASIC state, a WORD can be comprised ONLY of
	 * letters.
	 * In EXTENDED state - a WORD really is any string
	 * of characters except whitespaces.
	 * 
	 */
	WORD,
	/**
	 * This token represents a number that can be shown
	 * in Long format.
	 */
	NUMBER,
	/** 
	 * This token represents everything else that wasn't
	 * interpreted as a WORD, NUMBER or EOF.
	 */
	SYMBOL;
}
