package hr.fer.zemris.java.custom.scripting.elems;

/**
 * ElementOperator is an element of an expression that
 * represents an operator. Every operator has a symbol
 * that represents it.
 * 
 * @author Miroslav Bićanić
 */
public class ElementOperator extends Element {
	/**
	 * The symbol of this operator.
	 */
	private String symbol;
	
	/**
	 * A constructor for an ElementOperator.
	 * 
	 * @param symbol The symbol of this operator.
	 */
	public ElementOperator(String symbol) {
		this.symbol = symbol;
	}
	
	/**
	 * {@inheritDoc}
	 * 
	 * @return The string representation of this element's operator.
	 */
	@Override
	public String asText() {
		return symbol;
	}
	/**
	 * @return The string representation of this element's operator
	 */
	public String getSymbol() {
		return asText();
	}
	
	
}
