package hr.fer.zemris.java.custom.collections.demo;

import hr.fer.zemris.java.custom.collections.Tester;

/**
 * An implementation of the Tester interface that says
 * whether a given object is an even number.
 * 
 * Made as a demonstration of features interfaces provide.
 * 
 * @author Miroslav Bićanić
 */
public class EvenIntegerTester implements Tester {

	/**
	 * This method returns true if the given object
	 * is an even Integer, false if it is odd or not
	 * an Integer at all.
	 */
	@Override
	public boolean test(Object obj) {
		if(!(obj instanceof Integer)) {
			return false;
		}
		return ((Integer)obj)%2==0;
	}

}