package hr.fer.zemris.java.custom.scripting.nodes;

import java.util.Objects;

import hr.fer.zemris.java.custom.collections.ArrayIndexedCollection;

/**
 * Node is a base class for representing nodes in
 * a document.
 * 
 * @author Miroslav Bićanić
 */
public class Node {
	/**
	 * This collection represents all the nodes this node
	 * contains.
	 */
	private ArrayIndexedCollection children;
	
	/**
	 * This method adds a new child node to this node.
	 * 
	 * Note: a collection is allocated only when there
	 * is a need to add a child!
	 * @param child The child node of this node
	 */
	public void addChildNode(Node child) {
		if(children==null) {
			children = new ArrayIndexedCollection();
		}
		children.add(child);
	}
	/**
	 * This method returns the number of direct children
	 * this node has.
	 * @return The number of direct children of this node
	 */
	public int numberOfChildren() {
		return children==null ? 0 : children.size();
	}
	/**
	 * This method returns the index-th node from this
	 * node's collection of children nodes.
	 * 
	 * @param index The index of the node to return
	 * @return The node at the index-th position in the collection
	 * @throws IllegalArgumentException if the index is out of range.
	 */
	public Node getChild(int index) {
		if(index < 0 || index >= children.size()) {
			throw new IllegalArgumentException("Index is out of interval [0,size>.");
		}
		return (Node)children.get(index);
	}
	
	/**
	 * Overridden hashCode method in compliance with
	 * equals method.
	 */
	@Override
	public int hashCode() {
		return Objects.hash(children);
	}
	/**
	 * This method returns true if a handed object is equal
	 * to this node.
	 * Two nodes are equal if all of their children are
	 * equal. Since children are nodes, this method is
	 * called recursively for the whole document if called
	 * by a DocumentNode.
	 * This only checks the structure of the document, not
	 * the actual content of the specific nodes.
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!obj.getClass().equals(this.getClass()))
			return false;
		Node other = (Node) obj;
		if(this.children==null && other.children==null) {
			return true;
		}
		if(this.children==null || other.children==null) {
			return false;
		}
		return children.equals(other.children);
	}
	
	
}
