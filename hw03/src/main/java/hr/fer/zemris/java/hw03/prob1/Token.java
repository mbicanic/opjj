package hr.fer.zemris.java.hw03.prob1;

import java.util.Objects;

/**
 * This class represents a token from a text.
 * 
 * A token can be classified by its type and given
 * a value to hold, which can then be used by a 
 * parser to check if the original text was OK.
 * 
 * @see TokenType
 * @author Miroslav Bićanić
 */
public class Token {
	
	/**
	 * The value that this Token stores.
	 */
	private Object value;
	
	/**
	 * The type of this token.
	 */
	private TokenType type;
	
	/**
	 * A constructor that builds a token based on its
	 * type and value.
	 * 
	 * @param type The type of the token
	 * @param value The value of the token
	 */
	public Token(TokenType type, Object value) {
		this.type = Objects.requireNonNull(type);
		this.value = (type==TokenType.EOF ? value : Objects.requireNonNull(value));
	}
	
	/**
	 * Getter method for this token's value.
	 * @return The value of this token
	 */
	public Object getValue() {
		return value;
	}
	/**
	 * Getter method for this token's type.
	 * @return The type of this token.
	 */
	public TokenType getType() {
		return type;
	}
}
