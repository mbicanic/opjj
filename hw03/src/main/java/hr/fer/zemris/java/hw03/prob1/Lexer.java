package hr.fer.zemris.java.hw03.prob1;

import java.util.Objects;

/**
 * This is a simple lexer that takes a text as a String
 * and transforms it into tokens.
 * 
 * It is a "lazy" lexer, meaning it produces tokens on
 * demand, it doesn't tokenize the whole text at once.
 * 
 * It has two modes of operating: a BASIC mode in which
 * a text is parsed into a series of tokens of various
 * types, and an EXTENDED mode which parses a sequence
 * only as text tokens - never a number.
 * 
 * @see LexerState
 * @see TokenType
 * @author Miroslav Bićanić
 *
 */
public class Lexer {
	
	/**
	 * An array of characters that stores all the characters
	 * from the original text.
	 */
	private char[] data;
	
	/**
	 * The last token that was sent to the user.
	 */
	private Token token;
	
	/**
	 * The index of the first character this lexer did not
	 * read yet.
	 */
	private int currentIndex;
	
	/**
	 * The state in which the lexer is currently.
	 */
	private LexerState state;
	
	/**
	 * A Lexer constructor. The lexer is initially constructed
	 * in BASIC mode.
	 * 
	 * @param text The text which needs to be tokenized.
	 * @throws NullPointerException if text is a null reference.
	 */
	public Lexer(String text) {
		Objects.requireNonNull(text);
		data = text.toCharArray();
		currentIndex = 0;
		state = LexerState.BASIC;
	}
	
	/**
	 * This method returns the last retrieved token. It does
	 * not initiate new token generation.
	 * @return
	 */
	public Token getToken() {
		return this.token;
	}
	
	/**
	 * Setter for this Lexer's state, changing its mode
	 * of operating.
	 * 
	 * @param state The new state of the Lexer (BASIC or EXTENDED)
	 */
	public void setState(LexerState state) {
		Objects.requireNonNull(state);
		this.state = state;
	}

	/**
	 * This method extracts the next token from the given text.
	 * After the Lexer returns a token of the EOF type, it is
	 * not permitted to call this method again.
	 * 
	 * @return The next token in the original text
	 * @throws LexerException if it is called again after returning EOF Token.
	 */
	public Token nextToken() {
		if(this.token!=null && this.token.getType()==TokenType.EOF) {
			throw new LexerException("EOF already returned!");
		}
		
		skipBlanks();

		if(currentIndex>=data.length) {
			Token result = new Token(TokenType.EOF, null);
			this.token = result;
			return result;
		}
		
		/*
		 * One string builder is created because no matter what is
		 * the token, it has to be built character by character,
		 * (and then potentially parsed into a number), making
		 * this a more efficient way to build it.
		 */
		char curChar = data[currentIndex];
		StringBuilder sb = new StringBuilder(16);
		
		/*
		 * This if-block completely covers all the functionality
		 * in EXTENDED mode.
		 */
		if(state.equals(LexerState.EXTENDED) && curChar!='#') {
			String word = getExtendedWord(sb);
			this.token = new Token(TokenType.WORD, word);
			return this.token;
		}
		
		/*
		 * The remaining code in this method takes care of
		 * the functionality in BASIC mode.
		 */
		if(Character.isLetter(curChar)) {
			String word = getWord(sb);
			this.token = new Token(TokenType.WORD, word);
			return this.token;
		}
		if(curChar=='\\') {
			String sign = tryEscape();
			sb.append(sign);
			String word = getWord(sb);
			this.token = new Token(TokenType.WORD, word);
			return this.token;
		}
		if(Character.isDigit(curChar)) {
			Long number = getNumber(sb);
			this.token = new Token(TokenType.NUMBER, number);
			return this.token;
		} 
		
		 // The program from the instructions DOES NOT support negative
		 // numbers.
		 // he following if-block is an added support for that.
		 // Uncommenting them makes the tests UNSUCCESSFUL:
		 // testCombinedInput gives -24 and expects '-' and '24'.
		
		/*
		if(curChar=='-') {
			if(currentIndex+1 < data.length && 
					Character.isDigit(data[currentIndex+1])) {
				sb.append("-");
				currentIndex++;
				Long number = getNumber(sb);
				this.token = new Token(TokenType.NUMBER, number);
				return this.token;
			}
		} */
		
		this.token = new Token(TokenType.SYMBOL, (Character)curChar);
		currentIndex++;
		return this.token;
		
	}
	
	/**
	 * This method extracts the next word from the text, without
	 * caring if the word contains numbers or symbols - the whole
	 * thing is parsed as one String.
	 * 
	 * It stops extracting a word once it reaches the symbol
	 * for opening/ending extended mode ('#'), or a whitespace
	 * is reached. It is used only in EXTENDED mode.
	 * 
	 * @param sb An instance of a StringBuilder used to build a word
	 * @return A string representation of the next word in the text
	 */
	private String getExtendedWord(StringBuilder sb) {
		while(currentIndex < data.length 
				&& !Character.isWhitespace(data[currentIndex]) 
				&& data[currentIndex]!='#') {
			sb.append(data[currentIndex]);
			currentIndex++;
		}
		return sb.toString();
	}
	
	/**
	 * This method extracts the next word from the text, caring
	 * that it is comprised only of letters. By default it 
	 * does not allow symbols or numbers.
	 * 
	 * However, escaping is allowed, meaning that apart from letters
	 * it WILL accept a backslash and a number, if they were preceded
	 * by another backslash. (\\ or \[digit]).
	 * 
	 * Extraction is stopped once the first non-letter character
	 * is encountered. It is used only in BASIC mode.
	 * 
	 * @param sb An instance of a StringBuilder that builds the word
	 * @return A string representation of a word
	 */
	private String getWord(StringBuilder sb) {
		while(currentIndex < data.length && 
				(Character.isLetter(data[currentIndex]) || data[currentIndex]=='\\')) {
			char curChar = data[currentIndex];
			if(curChar=='\\') {
				String word = tryEscape();
				sb.append(word);
				continue;
			}
			sb.append(data[currentIndex]);
			currentIndex++;
		}
		return sb.toString();
	}
	
	/**
	 * This method is called once the first backslash (\) has been found,
	 * to check whether the character following it is acceptable, as there
	 * are only two escape sequences for this lexer: \\ and \[digit].
	 * 
	 * If the escape was correctly done, it returns only the second 
	 * character from the sequence as a String.
	 * 
	 * E.g.: "I am \2\1 years old, which is 42\\2" will be transformed
	 * into "I am 21 years old, which is 42\2".
	 * 
	 * @return The string representation of the second character of the sequence
	 * @throws LexerException if the escape sequence is invalid.
	 */
	private String tryEscape() {
		char curChar = data[currentIndex];
		if(currentIndex == data.length-1) {
			throw new LexerException("Invalid escape sequence - one \\ at end of line.");
		}
		currentIndex++;
		curChar = data[currentIndex];
		if(!Character.isDigit(curChar) && curChar!='\\') {
			throw new LexerException("Invalid escape sequence - \\ not followed by digit nor another \\.");
		}
		String val = Character.toString(curChar);
		currentIndex++;
		return val;
	}

	/**
	 * This method extracts the next number from the text.
	 * 
	 * It stops extracting when it reaches the first non-digit
	 * character.
	 * 
	 * The number in the text must not be larger than
	 * {@link Long.MAX_VALUE}, or an exception will be thrown
	 * 
	 * @param sb An instance of a StringBuilder used to build a word
	 * @return A string representation of the next word in the text
	 * @throws LexerException if the number is too large to be displayed in Long format.
	 */
	private Long getNumber(StringBuilder sb) {
		while(currentIndex < data.length &&
				Character.isDigit(data[currentIndex])) {
			sb.append(data[currentIndex]);
			currentIndex++;
		}
		try {
			Long val = Long.parseLong(sb.toString());
			return val;
		} catch (NumberFormatException ex) {
			throw new LexerException("Number cannot be parsed as long!");
		}
	}
	
	/**
	 * A simple helper method that iterates over the internal
	 * array of characters, skipping all whitespace characters
	 * and updating the current index accordingly.
	 */
	private void skipBlanks() {
		while(currentIndex<data.length && Character.isWhitespace(data[currentIndex])) {
			currentIndex++;
		}
	}
}
