package hr.fer.zemris.java.custom.scripting.lexer;

import java.util.Objects;

/**
 * SmartScriptLexer is a class that takes a String
 * and tokenizes it on-demand, token by token.
 * 
 * Once EOF token has been generated, asking for the
 * next token will result in an exception.
 * 
 * The lexer works in two states - TEXT and TAG,
 * initially being set up in TEXT mode.

 * In TEXT state, the lexer can only generate a TEXT token,
 * an EOF token if end of file is reached, or a single
 * SYMBOL token - the tag opener '{$'.
 * 
 * In TAG state, the lexer generates all types of tokens
 * except the TEXT token:
 * Placeholders preceded by '@' are generated as
 * FUNCTION tokens, 
 * Strings surrounded with double quotation marks (" ") 
 * are generated as STRING tokens, 
 * Numbers are generated as either INT or DOUBLE tokens, 
 * operators as one of the OPERATOR tokens. 
 * Pure placeholders are generated as VAR tokens. 
 * When a tag closer is reached ('$}') (and only then), a
 * SYMBOL token is generated.
 * EOF token can also be generated if end of file is reached.
 * 
 * The lexer allows different escape sequences depending on
 * the state the lexer is in:
 * 1) In TEXT state, the lexer accepts '\\' as an 
 * 		escape sequence for '\', and '\{' as a sequence
 * 		for '{'.
 * 	Note: A '{' has to be escaped only when it is followed by a '$'.
 * 		Otherwise it is treated as a normal part of text.
 * 2) In TAG state, it is legal to use escape sequences
 * 		only in strings. The accepted sequences are: 
 * 		( \\ ) for a ( \ ) and ( \" ) for a single ( " ).
 * 		Apart from that, the regular escapes for whitespace
 * 		( \n, \r, \t ) are also supported.
 * 
 * @see SmartLexerState
 * @see TokenType
 * @author Miroslav Bićanić
 *
 */
public class SmartScriptLexer {
	
	/**
	 * The initial capacity for a string builder for every next
	 * token.
	 */
	private static final int SB_CAPACITY = 50;
	/**
	 * A class constant defining the string representation of the
	 * tag opener
	 */
	private static final String OPENER = "{$";
	/**
	 * A class constant defining the string representation of the
	 * tag closer
	 */
	private static final String CLOSER = "$}";
	
	/**
	 * An array of characters that represents the whole 
	 * given string that has to be tokenized.
	 */
	private char[] data;
	/**
	 * The last token this lexer returned.
	 */
	private Token token;
	/**
	 * The index of the first not-yet-visited character
	 * in the data array.
	 */
	private int current;
	/**
	 * The state this lexer is currently in.
	 */
	private SmartLexerState state;
	
	/**
	 * A constructor for a SmartScriptLexer.
	 * 
	 * @param text The text to be tokenized.
	 * @throws NullPointerException if handed text is null
	 */
	public SmartScriptLexer(String text) {
		Objects.requireNonNull(text);
		data = text.toCharArray();
		current = 0;
		state = SmartLexerState.TEXT;
	}

	/**
	 * @return The last token this lexer generated.
	 */
	public Token getToken() {
		return this.token;
	}
	/**
	 * @param state The new desired state of this lexer.
	 */
	public void setState(SmartLexerState state) {
		this.state = Objects.requireNonNull(state);
	}
	
	/**
	 * This method extracts and generates the next token from
	 * the original text. Calling this method after EOF token
	 * has once been generated results in an exception.
	 * 
	 * @return The next token in the text
	 * @throws SmartLexerException 
	 * 			-if EOF was already returned and it is called again
	 * 			-if a tag name isn't provided or is illegal
	 * 			-if a function name isn't provided or is illegal
	 */
	public Token nextToken() {
		/*
		 * First two if blocks identical to the simple lexer from
		 * problem 2.
		 */
		if(this.token!=null && this.token.getType()==SmartTokenType.EOF) {
			throw new SmartLexerException("EOF already returned!");
		}
		
		if(current>=data.length) {
			this.token = new Token(SmartTokenType.EOF, null);
			return this.token;
		}
		
		/*
		 * A string builder is used because all tokens are first produced
		 * as strings which are later parsed into integers or doubles, if
		 * necessary. Usage of a StringBuilder greatly improves the
		 * performance of concatenating strings.
		 */
		StringBuilder sb = new StringBuilder(SB_CAPACITY);
		
		/*
		 * This if block completely covers the behaviour of the program
		 * in the TEXT state.
		 */
		if(state.equals(SmartLexerState.TEXT)) {
			if(isTagOpener()) {
				current+=2;
				this.token = new Token(SmartTokenType.SYMBOL, OPENER);
				return this.token;
			}
			String text = getText(sb);
			this.token = new Token(SmartTokenType.TEXT, text);
			return this.token;
		}
		//IF THIS CODE IS EXECUTING, THE LEXER IS IN TAG STATE.
		
		skipBlanks();
		if(current>=data.length) {
			return new Token(SmartTokenType.EOF, null);
		}
		return parseTag(sb);
		
		
		
		/*
		current++;
		this.token = new Token(SmartTokenType.SYMBOL, Character.toString(curChar));
		return this.token;
		*/
	}

	/**
	 * This method returns the next token in tag mode, covering all
	 * the possibilities in a tag:
	 * -It can be the tag's identifier, if the tag was just opened
	 * -The tag can be closed ('$}')
	 * -It can be a variable -> starts with a letter
	 * -It can be a number -> starts with a digit OR '-'
	 * -It can be a function ('@*****')
	 * -It can be a string, meaning it's enclosed in quotation marks
	 * -It can be an operator.
	 * 
	 * If it is none of the above mentioned types, then the character is illegal
	 * and an exception is thrown.
	 * 
	 * @param sb A copy of a stringbuilder to build a string from all the characters
	 * @return The next token that is in a tag
	 * @throws SmartLexerException if an illegal character is found
	 */
	private Token parseTag(StringBuilder sb) {
		/*
		 * If the last token was a tag opener, that means what
		 * follows is (should be) a tag name (of type KWD).
		 */
		if(this.token.getValue().equals("{$")) {
			if(data[current]!='=' && !Character.isLetter(data[current])) {
				throw new SmartLexerException("Invalid tag name!");
			}
			if(data[current]=='='){
				current++;
				this.token = new Token(SmartTokenType.KWD, "=");
				return this.token;
			}
			String tagName = getPlaceholder(sb);
			this.token = new Token(SmartTokenType.KWD, tagName);
			return this.token;
		}
		
		if(isTagCloser()) {
			current+=2;
			this.token = new Token(SmartTokenType.SYMBOL, CLOSER);
			return this.token;
		}
		
		if(Character.isLetter(data[current])) {
			String varName = getPlaceholder(sb);
			this.token = new Token(SmartTokenType.VAR, varName);
			return this.token;
		}

		if(Character.isDigit(data[current]) || data[current]=='-') {
			return numberProcessor(sb);
		}
		
		if(data[current]=='@') {
			current++;
			String funcName = getPlaceholder(sb);
			if(funcName.length()==0) {
				throw new SmartLexerException("Given function name is invalid!");
			}
			this.token = new Token(SmartTokenType.FUNCTION, funcName);
			return this.token;
		}
	
		if(data[current]=='\"' ) {
			current++; //skipping first quote
			String word = getString(sb);
			current++; //skipping the second quote
			this.token = new Token(SmartTokenType.STRING, word);
			return this.token;
		}
		
		if(data[current]=='+' || data[current]=='*' || data[current]=='/' || data[current]=='^') {
			this.token = new Token(SmartTokenType.OPERATOR, Character.toString(data[current]));
			current++;
			return this.token;
		}
		
		throw new SmartLexerException(data[current]);
	}
	
	/**
	 * This method is used in TEXT state to extract the maximal
	 * sequence of characters until the first tag opener is reached.
	 * 
	 * It supports the escape sequences of the TEXT state.
	 * 
	 * @see SmartScriptLexer for documentation about escaping.
	 * @param sb A copy of a StringBuilder to build a string from all the characters
	 * @return A string that represents the block of text.
	 */
	private String getText(StringBuilder sb) {
		while(current < data.length) {
			if(isTagOpener()) {
				return sb.toString();
			}
			if(data[current]=='\\') {
				String word = textEscape();
				sb.append(word);
				continue;
			}
			sb.append(data[current]);
			current++;
		}
		return sb.toString();
	}
	
	/**
	 * This method is used to test if an attempted escape sequence is valid, 
	 * and if it is, returns a string with only the escaped character.
	 *
	 * This method covers the escapes of TEXT state.
	 *
	 * @return A string representation of the second character of the sequence
	 * @throws SmartLexerException if the escape sequence is invalid.
	 */
	private String textEscape() {
		if(current == data.length-1) {
			throw new SmartLexerException("Invalid escape sequence - one \\ at end of line.");
		}
		current++;
		char curChar = data[current];
		if(curChar!='\\' && curChar!='{') {
			throw new SmartLexerException("Invalid escape sequence in text: \\"+curChar);
		}
		current++;
		return Character.toString(curChar);
	}
	/**
	 * This method retrieves characters as long as the string 
	 * built from them qualifies as a placeholder.
	 * 
	 * A placeholder takes place for a name of something (e.g.
	 * function, variable) - its name must start with a letter,
	 * and then have an arbitrary number of digits, underscores
	 * and letters following it.
	 * 
	 * @param sb A copy of a StringBuilder to build a string of all the characters
	 * @return String value of the placeholder
	 */
	private String getPlaceholder(StringBuilder sb) {
		char cur = data[current];
		if(!Character.isLetter(cur)) {
			return new String();
		}
		while(current < data.length &&
				placeholderCondition(data[current])) {
			sb.append(data[current]);
			current++;
		}
		return sb.toString();
	}

	/**
	 * This method (procedure) processes both integer and double
	 * constants and returns them as Tokens of adequate type.
	 * 
	 * Apart from numbers, it can also generate one OPERATOR
	 * token '-', if the '-' isn't immediately followed by a
	 * digit.
	 * 
	 * @param sb A copy of a StringBuilder to build a string of all the characters
	 * @return one of the following tokens: DOUBLE, INT, OPERATOR ('-')
	 * @throws SmartLexerException
	 * 		-if the integer number given is too big to be parsed as integer
	 * 		-if the double number given cannot be parsed as a double
	 */
	private Token numberProcessor(StringBuilder sb) {
		char curChar = data[current];
		if(curChar=='-' && current+1<data.length) {
			if(!Character.isDigit(data[current+1])) {
				current++;
				this.token = new Token(SmartTokenType.OPERATOR, Character.toString(curChar));
				return this.token;
			}
			sb.append("-");
			current++;
		}
		String firstNumeric = getDigitString(sb);
		curChar = data[current];
		
		if(curChar=='.' && current+1<data.length) {
			current++;
			if(Character.isDigit(data[current])) {
				sb = new StringBuilder(16);
				String secondNumeric = getDigitString(sb);
				String number = firstNumeric + "." + secondNumeric;
				try {	
					this.token = new Token(SmartTokenType.DOUBLE, Double.parseDouble(number));
					return this.token;
				} catch (IllegalArgumentException ex) {
					throw new SmartLexerException("Number has to be interpretable as a double!");
				}
			} 
		}
		try {
			this.token = new Token(SmartTokenType.INT, Integer.parseInt(firstNumeric));
			return this.token;
		} catch (IllegalArgumentException ex) {
			throw new SmartLexerException("Number cannot be larger than the capacity of integer!");
		}
	}
	
	/**
	 * This method returns the largest possible string
	 * of digits from the current character onward.
	 * 
	 * @param sb A copy of a StringBuilder to build a string of all the characters
	 * @return A string of digits
	 */
	private String getDigitString(StringBuilder sb) {
		while(current < data.length && Character.isDigit(data[current])) {
			sb.append(data[current]);
			current++;
		}
		return sb.toString();
	}

	/**
	 * This method returns the string value that was enclosed in
	 * double quotes in the original expression.
	 * 
	 * It supports the escape sequences of the TAG state.
	 * 
	 * @see SmartScriptLexer for escape sequences documentation
	 * @param sb A copy of a StringBuilder to build a string of all the characters
	 * @return A string value that was a part of an expression
	 * @throw SmartLexerException if a string is opened but end of file
	 * 		is reached before it is closed.
	 */
	private String getString(StringBuilder sb) {
		while(current<data.length && data[current]!='\"') {
			if(data[current]=='\\') {
				String word = tagEscape();
				sb.append(word);
				continue;
			}
			sb.append(data[current]);
			current++;
		}
		if(current==data.length && data[current-1]!='\"') {
			throw new SmartLexerException("Opened string wasn't closed!");
		}
		return sb.toString();
	}
	
	/**
	 * This method is used to test if an attempted escape sequence is valid, 
	 * and if it is, returns a string with only the escaped character or
	 * a whitespace character if \n, \r or \t was entered.
	 *
	 * This method is used only in TAG state.
	 *
	 * @return A string representation of the second character of the sequence
	 * @throws SmartLexerException if the escape sequence is invalid.
	 */
	private String tagEscape() {
		if(current == data.length-1) {
			throw new SmartLexerException("Invalid escape sequence - one \\ at end of line.");
		}
		current++;
		char curChar = data[current];
		String val = "";
		if(curChar=='\\') {
			val = "\\";
		} else if (curChar=='\"') {
			val="\"";
		} else if (curChar=='n' || curChar=='r' || curChar=='t') {
			val = Character.toString('\n');
		} else if (curChar=='r') {
			val = Character.toString('\r');
		} else if (curChar=='t') {
			val = Character.toString('\t');
		}
		else {
			throw new SmartLexerException("Invalid escape sequence in a tag: \\"+curChar);
		}
		current++;
		return val;
	}
	

	/**
	 * This method checks if the character satisfies the criteria
	 * to be a placeholder.
	 * 
	 * @param c The character to be checked
	 * @return true if it is a letter, underscore or digit; false otherwise
	 */
	private boolean placeholderCondition(char c) {
		return Character.isLetter(c) || c=='_' || Character.isDigit(c);
	}
	
	/**
	 * This method checks if the current and next character in sequence
	 * together form a tag opener.
	 * @return true if there is a tag opener, false otherwise
	 */
	private boolean isTagOpener() {
		return current+1<data.length && data[current]=='{' && data[current+1]=='$';
	}
	/**
	 * This method checks if the current and next character in sequence
	 * together form a tag closer.
	 * @return true if there is a tag closer, false otherwise
	 */
	private boolean isTagCloser() {
		return current+1<data.length && data[current]=='$' && data[current+1]=='}';
	}
	
	/**
	 * This simple method skips a whole sequence of whitespace 
	 * characters of the input strings.
	 */
	private void skipBlanks() {
		while(current<data.length && Character.isWhitespace(data[current])) {
			current++;
		}
	}
}
