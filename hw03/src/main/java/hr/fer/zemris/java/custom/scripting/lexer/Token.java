package hr.fer.zemris.java.custom.scripting.lexer;

import java.util.Objects;

/**
 * This class represents a token from a text.
 * 
 * A token can be classified by its type and given
 * a value to hold, which can then be used by a 
 * parser to check if the original text was OK.
 * 
 * @see SmartTokenType
 * @author Miroslav Bićanić
 */
public class Token {
	
	/**
	 * The value that this Token stores.
	 */
	private Object value;
	
	/**
	 * The type of this token.
	 */
	private SmartTokenType type;
	
	/**
	 * A constructor that builds a token based on its
	 * type and value.
	 * 
	 * @param type The type of the token
	 * @param value The value of the token
	 * @throws NullPointerException if the handed type is null, or if the value is null while the type is not EOF.
	 */
	public Token(SmartTokenType type, Object value) {
		this.type = Objects.requireNonNull(type);
		this.value = type==SmartTokenType.EOF ? value : Objects.requireNonNull(value);
	}
	
	/**
	 * @return The value of this token
	 */
	public Object getValue() {
		return value;
	}
	/**
	 * @return The type of this token.
	 */
	public SmartTokenType getType() {
		return type;
	}

	/**
	 * Two tokens are equal if they are of the same type
	 * and have the same value.
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Token))
			return false;
		Token other = (Token) obj;
		return type == other.type && Objects.equals(value, other.value);
	}
	/**
	 * Overridden in accordance with equals.
	 */
	@Override
	public int hashCode() {
		return Objects.hash(type, value);
	}

	/**
	 * This method returns a string representation of this token 
	 * in the format (TYPE, VALUE).
	 */
	@Override
	public String toString() {
		return "("+type+", "+value+")";
	}
}
