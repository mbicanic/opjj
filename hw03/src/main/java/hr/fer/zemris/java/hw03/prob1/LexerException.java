package hr.fer.zemris.java.hw03.prob1;

/**
 * This exception is thrown whenever a Lexer encounters
 * an error in the file.
 * 
 * @author Miroslav Bićanić
 */
public class LexerException extends RuntimeException {
	private static final long serialVersionUID = 1L;
	
	/**
	 * A default constructor for a LexerException
	 */
	public LexerException() {
		super();
	}
	/**
	 * A constructor for a LexerException that takes a message
	 * describing the error that caused it.
	 * @param message A description of the error
	 */
	public LexerException(String message) {
		super(message);
	}
}
