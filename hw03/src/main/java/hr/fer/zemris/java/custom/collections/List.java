package hr.fer.zemris.java.custom.collections;

/**
 * A List is a interface that is a subset of a Collection.
 * 
 * It represents an ordered indexed list, and defines some
   additional methods all Lists must implement, but does not
 * specify whether elements can be duplicated.
 * 
 * @author Miroslav Bićanić
 */
public interface List extends Collection {
	/**
	 * This method returns an element from the index-th position
	 * from the List.
	 * 
	 * @param index The index of the element in the List
	 * @return The element at position index in the list
	 */
	Object get(int index);
	/**
	 * This method inserts an element on the index-th position
	 * in the list.
	 * 
	 * It does not overwrite the element that was previously
	 * on that position.
	 * 
	 * @param value The value to insert
	 * @param position The position on which to insert the value
	 */
	void insert(Object value, int position);
	/**
	 * This method returns the index of an object with the given
	 * value in the List if it exists.
	 * 
	 * The index does not have to be the index of the first
	 * appearance of the element.
	 * 
	 * @param value The looked for object
	 * @return The position of the object in the List, -1 if it doesn't exist
	 */
	int indexOf(Object value);
	/**
	 * This method removes an element from a certain index,
	 * shifting all later elements for one spot to the left.
	 * 
	 * @param index The index of the element to be removed
	 */
	void remove(int index);
	
	
}
