package hr.fer.zemris.java.custom.scripting.elems;

/**
 * ElementConstantDouble is an element of an expression
 * that can be interpreted as a double constant.
 * 
 * @author Miroslav Bićanić
 */
public class ElementConstantDouble extends Element {
	/**
	 * The double value of this ElementConstantDouble.
	 */
	private double value;
	
	/**
	 * A constructor for ElementConstantDouble.
	 * 
	 * @param val The value assigned to this ElementConstantDouble.
	 */
	public ElementConstantDouble(double val) {
		this.value = val;
	} 
	
	/**
	 * {@inheritDoc}
	 * 
	 * @return The string representation of this Element's double value.
	 */
	@Override
	public String asText() {
		return Double.toString(value);
	}

	/**
	 * @return The double value of this Element
	 */
	public double getValue() {
		return value;
	}
	
	
	
}
