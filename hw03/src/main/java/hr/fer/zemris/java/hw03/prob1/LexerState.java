package hr.fer.zemris.java.hw03.prob1;

/**
 * An enumeration of the states the Lexer can be in.
 * 
 * There are only two states, BASIC and EXTENDED.
 * 
 * @author Miroslav Bićanić
 *
 */
public enum LexerState {
	/**
	 * In this state, the Lexer tokenizes the text into
	 * 3 different types of tokens: WORD, NUMBER or
	 * SYMBOL. 
	 * 
	 * The lexer is cautious here, stopping the
	 * extraction of a WORD as soon as the first digit
	 * or symbol is encountered.
	 * 
	 * Note: using escape sequences there is a way to
	 * include a digit or a backslash in a WORD.
	 */
	BASIC,
	/**
	 * In this state, the Lexer tokenizes the text only
	 * into WORD Tokens which CAN include numbers and
	 * digits. 
	 * 
	 * The extraction of a WORD is stopped as soon as
	 * a whitespace or a hash symbol ('#') are
	 * encountered.
	 * 
	 * Escape sequences are NOT supported in this state.
	 */
	EXTENDED;
}
