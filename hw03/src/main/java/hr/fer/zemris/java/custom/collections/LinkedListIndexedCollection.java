package hr.fer.zemris.java.custom.collections;

import java.util.Arrays;
import java.util.ConcurrentModificationException;
import java.util.NoSuchElementException;
import java.util.Objects;

/**
 * An indexed collection of Objects based on a linked list of nodes.
 * 
 * This collection allows duplicate elements, 
 * but does not allow null values.
 * 
 * @author Miroslav Bićanić
 *
 */
public class LinkedListIndexedCollection implements List {
	
	/**
	 * The number of elements currently in the array.
	 */
	private int size;
	
	/**
	 * The first node in the linked list.
	 */
	private ListNode first;
	
	/**
	 * The last node in the linked list.
	 */
	private ListNode last;
	
	/**
	 * A data structure representing each node in the linked list.
	 * 
	 * @author Miroslav Bićanić
	 */
	private static class ListNode {
		/**
		 * The previous node in the list. 
		 */
		private ListNode previous;
		/**
		 * The next node in the list.
		 */
		private ListNode next;
		/**
		 * The value of this node.
		 */
		private Object value;
		
		/**
		 * The constructor for a ListNode.
		 * 
		 * @param previous The previous node
		 * @param next The next node
		 * @param value The value of this node
		 */
		private ListNode(ListNode previous, ListNode next, Object value) {
			this.previous = previous;
			this.next = next;
			this.value = value;
		}
	}
	
	private long modificationCount;
	
	/**
	 * The default LinkedListIndexedCollection constructor.
	 * 
	 * It is a default constructor with no body, because all
	 * values are initialised automatically.
	 * 
	 * Creates a new empty LinkedListIndexedCollection.
	 */
	public LinkedListIndexedCollection() {	
		this.modificationCount = 0;
	}
	/**
	 * A constructor that takes a Collection and makes a
	 * LinkedListIndexedCollection copy of it.
	 * 
	 * @param other The collection to copy
	 */
	public LinkedListIndexedCollection(Collection other) {
		Objects.requireNonNull(other);
		this.addAll(other);
		this.modificationCount = 0;
	}
	
	/**
	 * Returns the size of this LinkedListIndexedCollection.
	 * 
	 * @return The number of non-null elements in the collection
	 */
	@Override
	public int size() {
		return this.size;
	}
	
	/**
	 * This method says whether a <code>value</code> is in this 
	 * LinkedListIndexedCollection, where the equality is determined 
	 * by the equals method.
	 * 
	 * This operation is done in O(n) complexity (O(n/2) average).
	 * 
	 * @param value The value to look for in the collection.
	 */
	@Override
	public boolean contains(Object value) {
		if(value==null) {
			return false;
		}
		ListNode goalNode = this.seekRetrieve(value);
		return goalNode==null ? false : true;
	}

	/**
	 * This method returns the index of the first occurrence of a
	 * <code>value</code> in this LinkedListIndexedCollection as determined 
	 * by the equals method.
	 * 
	 * This operation is done in O(n) complexity (O(n/2) average).
	 * 
	 * @param value The value to look for in the collection
	 * @return -1 if <code>value</code> doesn't occur in the collection; the index of the first occurrence otherwise
	 */
	public int indexOf(Object value) {
		if(value==null) {
			return -1;
		}
		ListNode temp = first;
		int index = 0;
		while(temp!=null) {
			if(temp.value.equals(value)) {
				return index;
			}
			temp = temp.next;
			index++;
		}
		return -1;
	}

	/**
	 * This method adds a <code>value</code> into this LinkedListIndexedCollection.
	 * 
	 * The operation is done in O(1) complexity.
	 * 
	 * @throws NullPointerException if the given <code>value</code> is null
	 */
	@Override
	public void add(Object value) {
		Objects.requireNonNull(value);
		
		if(first==null) {
			first = new ListNode(null, null, value);
			last = first;
			this.size++;
			this.modificationCount++;
			return;
		}
		ListNode toAdd = new ListNode(this.last, null, value);
		this.last.next = toAdd;
		this.last = toAdd;
		this.size++;
		this.modificationCount++;
	}

	/**
	 * This method inserts the element into this LinkedListIndexedCollection
	 * at a given position.
	 * * 
	 * The index of all elements starting from the given index is increased
	 * by one after this operation - it does not overwrite.
	 * 
	 * This operation is done in O(n/2+1) complexity.
	 * 
	 * @param value The element to be inserted
	 * @param index The position at which the value will be inserted
	 * @throws NullPointerException if the value is null
	 * @throws IndexOutOfBoundsException if the given index is out of range [0,{@link #size}]
	 */
	public void insert(Object value, int index) {
		Objects.requireNonNull(value);
		
		ListNode newNode = new ListNode(null,null,value);
		if(index==0) {
			newNode.next = first;
			first.previous = newNode;
			first = newNode;
			this.modificationCount++;
			this.size++;
			return;
		} else if (index==this.size) { //index can be this.size
			newNode.previous = last;
			last.next = newNode;
			last = newNode;
			this.modificationCount++;
			this.size++;
			return;
		} 
		ListNode nodeAtPosition = seekIndexed(index); //throws IndexOutOfBoundsException!
		newNode.next = nodeAtPosition;
		newNode.previous = nodeAtPosition.previous;
		nodeAtPosition.previous = newNode;
		newNode.previous.next = newNode;
		
		this.size++;
		this.modificationCount++;
		return;
	}

	/**
	 * This method returns the element of this LinkedListIndexedCollection 
	 * from a given position.
	 * 
	 * This operation is done in O(n/2+1) complexity.
	 * 
	 * @param index The index of the element to retrieve
	 * @return The element at the given index
	 * @throws IndexOutOfBoundsException if the given index is out of range [0,{@link #size}-1]
	 */
	public Object get(int index) {
		return seekIndexed(index).value;
	}
	
	/**
	 * Removes one (first) occurrence of <code>value</code> from this 
	 * LinkedListIndexedCollection, where the equality is determined by the 
	 * equals method.
	 * 
	 * This operation is done in O(n) complexity (O(n/2) average).
	 * 
	 * @param value The value to remove from the collection.
	 */
	@Override
	public boolean remove(Object value) {
		if(value==null) {
			return false;
		}
		ListNode goalNode = this.seekRetrieve(value);
		if(goalNode==null) {
			return false;
		}
		if(goalNode==first) {
			ListNode temp = first.next;
			temp.previous=null;
			first = temp;
			this.size--;
			this.modificationCount++;
			return true;
		}
		if(goalNode==last) {
			ListNode temp = last.previous;
			temp.next = null;
			last = temp;
			this.size--;
			this.modificationCount++;
			return true;
		}
		
		goalNode.previous.next = goalNode.next;
		goalNode.next.previous = goalNode.previous;
		goalNode=null;
		this.size--;
		this.modificationCount++;
		return true;
	}

	/**
	 * Removes the element from a given index from this LinkedListIndexedCollection.
	 * 
	 * This operation is done in O(n/2+1) complexity.
	 * @param index The index of the element to remove from the collection
	 * @throws IndexOutOfBoundsException if the given index is out of range [0,{@link #size}-1]
	 */
	public void remove(int index) {
		if(index==0) {
			first = first.next;
			first.previous = null;
			this.size--;
			this.modificationCount++;
			return;
		}
		if(index==this.size-1) {
			last = last.previous;
			last.next = null;
			this.size--;
			this.modificationCount++;
			return;
		}
		ListNode goalNode = seekIndexed(index);
		
		goalNode.previous.next = goalNode.next;
		goalNode.next.previous = goalNode.previous;
		goalNode = null;
		this.size--;
		this.modificationCount++;
	}
	
	/**
	 * Deletes all elements from this LinkedListIndexedCollection.
	 * 
	 * This operation is done in O(1) complexity.
	 */
	@Override
	public void clear() {
		first = null;
		last = null;
		this.size=0;
		this.modificationCount++;
	}
	
	/**
	 * Gives the content of this LinkedListIndexedCollection as an array.
	 * 
	 * The resulting array has same elements on same positions
	 * as the original LinkedListIndexedCollection.
	 * 
	 * @return An array with all the elements of this LinkedListIndexedCollection
	 */
	@Override
	public Object[] toArray() {
		Object[] result = new Object[this.size];
		int i = 0;
		ListNode temp = first;
		while(temp!=null) {
			result[i] = temp.value;
			i++;
			temp = temp.next;
		}
		return result;
	}

	/**
	 * Seeks for the node with the given value and returns it if it exists.
	 * 
	 * The method is probing the LinkedListIndexedCollection from the left
	 * side, finding the first occurrence of <code>value</code>.
	 * 
	 * @param value The value that is looked for in the collection
	 * @return The reference to the node with the given value if it exists, null otherwise
	 */
	private ListNode seekRetrieve(Object value) {
		ListNode iter = this.first;
		while(iter!=null) {
			if(iter.value.equals(value)) {
				return iter;
			}
			iter = iter.next;
		}
		return null;
	}
	
	/**
	 * Reaches the node at position <code>index</code> and
	 * returns it.
	 * 
	 * This method probes the LinkedListIndexedCollection from
	 * either side, depending on what will give a faster result,
	 * never exceeding O(n/2 + 1) complexity.
	 * 
	 * @param index The index of the node that needs to be reached
	 * @return a reference to the node at the given position
	 * @throws IndexOutOfBoundsException if the index is out of range
	 */
	private ListNode seekIndexed(int index) {
		checkIndexInRange(index, 0, this.size-1);
		int i;
		ListNode iterator;
		if(index>this.size/2) {
			i = this.size-1;
			iterator = last;
			while(i>index) {
				iterator = iterator.previous;
				i--;
			}
			return iterator;
		} else {
			i = 0;
			iterator = first;
			while(i<index) {
				iterator = iterator.next;
				i++;
			}
			return iterator;
		}
	}
	
	/**
	 * Checks if a given integer is within the <b>closed</b> given range.
	 * 
	 * @param index The index to be checked
	 * @param lowerLimit The lower range limit (inclusive)
	 * @param upperLimit The upper range limit (inclusive)
	 */
	private void checkIndexInRange(int index, int lowerLimit, int upperLimit) {
		if(index<lowerLimit) {
			throw new IndexOutOfBoundsException("Expected positive index. Got: '"+index+"'.");
		}
		if(index>upperLimit) {
			throw new IndexOutOfBoundsException("Index '"+index+"' invalid for collection of size: "+upperLimit);
		}
	}
	
	/**
	 * This method returns a {@link #ElementsGetter} for
	 * iteration over this collection.
	 */
	@Override
	public ElementsGetter createElementsGetter() {
		return new LinkedIterator(this);
	}
	
	/**
	 * An implementation of an ElementsGetter designed
	 * for LinkedListIndexedCollections.
	 * 
	 * This ElementsGetter keeps track of the modification
	 * count, so no changes to the collection are allowed
	 * while an active ElementsGetter exists.
	 * 
	 * @author Miroslav Bićanić
	 *
	 */
	private static class LinkedIterator implements ElementsGetter {
		/**
		 * The first node that holds a value not sent to the user
		 * yet.
		 */
		private ListNode currentNode;
		/**
		 * The reference to an LinkedListIndexedCollection this 
		 * LinkedIterator iterates over.
		 */
		private LinkedListIndexedCollection myCollection;
		/**
		 * The number of modifications this collection already had
		 * at the moment of creating this LinkedIterator.
		 */
		private long savedModificationCount;

		/**
		 * Constructor for a LinkedIterator that takes a 
		 * LinkedListIndexedCollection col and iterates over it on demand.
		 * @param col The collection to iterate over
		 */
		public LinkedIterator(LinkedListIndexedCollection col) {
			this.myCollection = col;
			this.currentNode = myCollection.first;
			this.savedModificationCount = col.modificationCount;
		}
		
		/**
		 * {@inheritDoc}
		 * 
		 * Operation is successful only if the collection hasn't been
		 * modified since the creation of ArrayIterator.
		 * 
		 * @throws ConcurrentModificationException
		 */
		@Override
		public boolean hasNextElement() {
			return currentNode!=null;
		}
		/**
		 * {@inheritDoc}
		 * 
		 * Returning an element is successful only if the collection
		 * hasn't been modified since the creation of ArrayIterator.
		 * 
		 * @throws ConcurrentModificationException
		 */
		@Override
		public Object getNextElement() {
			if(savedModificationCount!=myCollection.modificationCount) {
				throw new ConcurrentModificationException();
			}
			if(currentNode==null) {
				throw new NoSuchElementException();
			}
			Object value = currentNode.value;
			currentNode = currentNode.next;
			return value;
		}
	}

	/**
	 * Two Lists are considered equal when they are of the
	 * same size, and store equal elements on identical
	 * positions.
	 * If both Lists have size 0, they are equal.
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof List))
			return false;
		List other = (List) obj;
		if(size!=other.size()) {
			return false;
		}
		if(size==0) {
			return true;
		}
		boolean bool = true;
		for(int i = 0; i < size; i++) {
			if (!this.get(i).equals(other.get(i))) {
				bool = false;
			}
		}
		return bool;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Arrays.deepHashCode(toArray());
		result = prime * result + Objects.hash(size);
		return result;
	}
}
