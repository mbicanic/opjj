package hr.fer.zemris.java.custom.collections;

/**
 * An interface of an object tester.
 * 
 * A Tester is an object that takes another object
 * and determines whether it satisfies that Tester's
 * criteria.
 * 
 * @author Miroslav Bićanić
 */
@FunctionalInterface
public interface Tester {
	
	/**
	 * The prototype of the test method. This method
	 * is supposed to return true if the object
	 * satisfies the criteria.
	 * 
	 * @param obj The object to be checked
	 * @return	true if it satisfies the criteria, false otherwise
	 */
	boolean test(Object obj);
}
