package hr.fer.zemris.java.hw03;

import hr.fer.zemris.java.custom.scripting.nodes.EchoNode;
import hr.fer.zemris.java.custom.scripting.nodes.ForLoopNode;
import hr.fer.zemris.java.custom.scripting.nodes.Node;
import hr.fer.zemris.java.custom.scripting.nodes.TextNode;

/**
 * Generator is a class of static methods for reconstructing
 * a text string from a given document tree.
 * 		
 * @author Miroslav Bićanić
 */
public class Generator {
	/*
	  * Note: I built this class additionally for two reasons:
	  * 1) To put some pressure off of the SmartScriptTester - 
	  * 		all methods are static and could as well be there.
	  * 2) To have a senseful place to store the escape sequence 
	  * 		restoring methods, since those methods are imported
	  * 		in Node and it doesn't make sense to import something
	  * 		from a "demo" program
	  * 3) To achieve greater modularity and generic code - 
	  * 		these methods will print out any tree built from
	  * 		Nodes (only a support in the if-block has to be added)
	 */
	
	/**
	 * This method loops over all the children of the current node,
	 * building a string from each of their content.
	 * 
	 * @param parent The parent whose children nodes we are iterating over
	 * @param res A string builder that builds the whole document
	 */
	public static void loop(Node parent, StringBuilder res) {
		for(int i=0, n=parent.numberOfChildren(); i<n; i++) {
			Node child = parent.getChild(i);
			if(child instanceof TextNode) {
				res.append((TextNode)child);
			} else if(child instanceof ForLoopNode) {
				res.append(child.toString());
				loop(child, res);
				res.append("{$ END $}");
			} else if(child instanceof EchoNode) {
				res.append((EchoNode)child);
			}
		}
	}

	/**
	 * This method replaces all characters that should be escaped
	 * from the text of a TextNode with an escape sequence used
	 * to escape them ( \ -> \\, {$ -> \{$ ) to make the generated
	 * text reparsable.
	 * 
	 * @param s A string to reverse escape
	 * @return A string with all escape sequences in place
	 */
	public static String replaceTextEscape(String s) {
		for(int i=0; i<s.length(); i++) {
			if(s.charAt(i)=='\\' || (s.charAt(i)=='{' && s.charAt(i+1)=='$')) {
				s = s.substring(0,i) + "\\" + s.substring(i);
				i+=1;
			}
		}
		return s;
	}
	
	/**
	 * This method replaces all characters that should be escaped
	 * from the strings within a tag with an escape sequence used
	 * to escape them ( \ -> \\, " -> \" ) to make the generated
	 * text reparsable.
	 * 
	 * @param s A string to reverse escape
	 * @return A string with all escape sequences in place
	 */
	public static String replaceTagEscape(String s) {
		for(int i=0; i<s.length(); i++) {
			if(s.charAt(i)=='\\' || s.charAt(i)=='\"') {
				s = s.substring(0,i) + "\\" + s.substring(i);
				i+=1;
			}
		}
		return s;
	}
	
}
