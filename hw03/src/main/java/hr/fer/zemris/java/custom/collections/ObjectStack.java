package hr.fer.zemris.java.custom.collections;

/**
 * A data structure that represents a stack of Objects.
 * 
 * The stack allows duplicate elements, but does not allow
 * storing null values.
 * 
 * @author Miroslav Bićanić
 *
 */
public class ObjectStack {

	/**
	 * The underlying ArrayIndexCollection used to realize
	 * the functionality of a stack.
	 */
	private ArrayIndexedCollection stack;
	
	/**
	 * A default constructor that creates an empty stack.
	 */
	public ObjectStack() {
		this.stack = new ArrayIndexedCollection();
	}
	
	/**
	 * This function says whether this ObjectStack is empty.
	 * 
	 * @return true if stack is empty, false otherwise
	 */
	public boolean isEmpty() {
		return stack.isEmpty();
	}
	
	/**
	 * Returns the size (number of non-null elements in the stack)
	 * of this ObjectStack.
	 * 
	 * @return the size of the stack
	 */
	public int size() {
		return stack.size();
	}
	
	/**
	 * Pushes a value to the top of the stack.
	 * 
	 * This operation is achieved in O(1) complexity.
	 * 
	 * @param value The value to be pushed to the stack
	 * @throws NullPointerException if the value is null
	 */
	public void push(Object value) {
		stack.add(value);
	}
	/**
	 * Retrieves a value from the top of the stack and then removes it
	 * from the stack.
	 * 
	 * This operation is achieved in O(1) complexity.
	 * 
	 * @return The element that was on the top of the stack
	 * @throws EmptyStackException if the stack is empty.
	 */
	public Object pop() {
		if(stack.size()==0) {
			throw new EmptyStackException("Cannot pop from an empty stack.");
		}
		Object result = stack.get(stack.size()-1);
		stack.remove(stack.size()-1);
		return result;
	}
	
	/**
	 * Retrieves a value from the top of the stack without removing it.
	 * 
	 * This operation is achieved in O(1) complexity.
	 *  
	 * @return The element that is on the top of the stack
	 * @throws EmptyStackException if the stack is empty.
	 */
	public Object peek() {
		if(stack.size()==0) {
			throw new EmptyStackException("Cannot peek from an empty stack.");
		}
		return stack.get(stack.size()-1);
	}
	
	/**
	 * This method clears the stack of all elements.
	 */
	public void clear() {
		stack.clear();
	}
}
