package hr.fer.zemris.java.hw03;
import hr.fer.zemris.java.custom.scripting.nodes.*;
import hr.fer.zemris.java.custom.scripting.parser.*;
import java.nio.file.Files;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Paths;

/**
 * This class is used to demonstrate how a SmartScriptParser
 * uses a SmartScriptLexer to build a document tree, and that
 * it is possible to reconstruct the original file from it.
 * A document is first parsed, then the reconstruction is 
 * printed, and then given to the parser. After that, it is 
 * printed again (to visually compare the two generated documents).
 * 
 * In the end, a true or false statement is printed, depending
 * on the equality of the two generated document nodes.
 * 
 * @see Node
 * @author Miroslav Bićanić
 */
public class SmartScriptTester {
	/*
	 * In the examples folder you can find all the files present
	 * also in the src/test/resources folder.
	 * The files called error* are supposed to throw an exception.
	 * The SmartScriptParserException is caught and the exact
	 * description of what caused the error is printed.
	 * The remaining files work properly. 
	 */
	
	public static void main(String[] args) throws IOException {
		
		if(args.length!=1) {
			System.out.println("One argument required at start-up: path to source file.");
			return;
		}
		
		String docBody;
		try {
			docBody = new String(Files.readAllBytes(Paths.get(args[0])), StandardCharsets.UTF_8);
		} catch (IOException ex) {
			System.out.println("Unable to open file!");
			return;
		}
		
		//PART ONE: PARSING THE ORIGINAL FILE AND PRINTING ITS RECONSTRUCTION
		SmartScriptParser parser = null;
		try {  
			parser = new SmartScriptParser(docBody);
		} catch(SmartScriptParserException e) {  
			System.out.println(e.getMessage());
			System.out.println("Unable to parse document!");  
			System.exit(-1);
		} catch(Exception e) {  
			e.printStackTrace();
			System.out.println("If this line ever executes, you have failed this class!");  
			System.exit(-1);
		}
		DocumentNode document = parser.getDocumentNode();
		
		String originalDocumentBody = createOriginalDocumentBody(document);
		System.out.println(originalDocumentBody);
		
		//PART TWO: PARSING THE RECONSTRUCTION AND PRINTING IT, SHOWING THEY'RE EQUAL
		System.out.println("----------------------------------");
		parser = new SmartScriptParser(originalDocumentBody);
		DocumentNode document2 = parser.getDocumentNode();
		System.out.println(createOriginalDocumentBody(document));
		System.out.println();
		System.out.println(document.equals(document2));
	}
	
	/**
	 * This method returns the reconstructed content of a parsed file
	 * from a given document node containing the document tree of the
	 * file.
	 * 
	 * @param doc The document node of the document tree
	 * @return The reconstructed contents of the originally parsed file.
	 */
	public static String createOriginalDocumentBody(DocumentNode doc) {
		StringBuilder res = new StringBuilder(500);
		Generator.loop(doc, res);
		return res.toString();
	}
}
