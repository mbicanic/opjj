package hr.fer.zemris.java.custom.scripting.elems;

import static hr.fer.zemris.java.hw03.Generator.*;
/**
 * ElementString is an element of an expression that
 * represents a string.
 *
 * @author Miroslav Bićanić
 */
public class ElementString extends Element {
	
	/**
	 * The String value of this ElementString.
	 */
	private String value;
	
	/**
	 * A constructor for an ElementString.
	 * 
	 * @param val The value to be assigned to this ElementString
	 */
	public ElementString(String val) {
		this.value = val;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @return This ElementString's string value.
	 */
	@Override
	public String asText() {
		return value;
	}
	/**
	 * {@inheritDoc}
	 * 
	 * It also adds escapes to all characters that need
	 * to be escaped in the string (" -> \", \ -> \\),
	 * and encloses the string in double quotes (" ").
	 */
	@Override
	public String toString() {
		String t = replaceTagEscape(value);
		return "\""+t+"\" ";
	}
	
	/**
	 * @return This ElementString's string value
	 */
	public String getValue() {
		return asText();
	}
}
