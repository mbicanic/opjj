package hr.fer.zemris.java.custom.collections;

import java.util.Objects;

/**
 * An abstract collection.
 * 
 * It dictates the contract and the user interface all 
 * collections have to have.
 * 
 * @author Miroslav Bićanić
 *
 */
public interface Collection {
	
	/**
	 * This method says whether a collection is empty.
	 * 
	 * All classes implementing this interface must either override
	 * this method, or provide an implementation of <code>size
	 * </code> for this method to work.
	 * 
	 * @see #size()
	 * @return true if collection is empty, false otherwise
	 */
	default boolean isEmpty() {
		return this.size()==0;
	}
	
	/**
	 * This method gives the current number of elements in the list.
	 */
	int size();
	
	/**
	 * Adds a <code>value</code> to the collection.
	 *
	 * @param value The value to add to the collection
	 */
	void add(Object value);
	
	/**
	 * This method says whether a <code>value</code> is in the collection.
	 *
	 * @param value The value to look for in the collection.
	 */
	boolean contains(Object value);
	
	/**
	 * Removes one occurrence of <code>value</code> from the collection.
	 *
	 * @param value The value to remove from the collection
	 */
	boolean remove(Object value);
	
	/**
	 * Gives the content of the collection as an array.
	 */
	Object[] toArray();
	
	/**
	 * Does the processor's action on every element of the collection.
	 * 
	 * @param processor The processor that does the action on every element.
	 */
	default void forEach(Processor processor) {
		Objects.requireNonNull(processor);
		ElementsGetter getter = this.createElementsGetter();
		while(getter.hasNextElement()) {
			processor.process(getter.getNextElement());
		}
	}
	
	/**
	 * Adds all elements of another Collection to this collection.
	 * 
	 * All classes extending Collection only have to override the methods
	 * <code>add</code> and <code>forEach</code> for this method to work.
	 * @param other The collection whose elements will be added to this collection.
	 */
	default void addAll(Collection other) {
		Objects.requireNonNull(other);
		other.forEach(o->this.add(o));
	}
	/**
	 * A variant of the previous method that adds all elements from 
	 * Collection col to this collection, if the element satisfied
	 * a condition.
	 * 
	 * @param col The collection to add from
	 * @param tester A tester object that tests every element on a certain criteria
	 */
	default void addAllSatisfying(Collection col, Tester tester) {
		ElementsGetter getter = col.createElementsGetter();
		while(getter.hasNextElement()) {
			Object element = getter.getNextElement();
			if(tester.test(element)) {
				this.add(element);
			}
		}
	}
	
	/**
	 * Deletes all elements from the collection.
	 */
	void clear();
	
	/**
	 *  Getter method for an ElementsGetter that iterates
	 *  over this Collection.
	 */
	ElementsGetter createElementsGetter();
	
	
}
