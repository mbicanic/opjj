package hr.fer.zemris.lsystems.demo;

import hr.fer.zemris.lsystems.gui.LSystemViewer;
import hr.fer.zemris.lsystems.impl.LSystemBuilderImpl;

/**
 * Second demonstrational program configuring an LSystem
 * from an input file selected at runtime.
 * 
 * @author Miroslav Bićanić
 */
public class Glavni3 {

	public static void main(String[] args) {
		LSystemViewer.showLSystem(LSystemBuilderImpl::new);
	}
}
