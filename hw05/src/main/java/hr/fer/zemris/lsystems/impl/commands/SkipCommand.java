package hr.fer.zemris.lsystems.impl.commands;

import hr.fer.zemris.lsystems.Painter;
import hr.fer.zemris.lsystems.impl.Command;
import hr.fer.zemris.lsystems.impl.Context;

/**
 * SkipCommand moves the turtle on the screen using the
 * parameters from the current {@link TurtleState}, without
 * leaving a trace on the screen.
 * 
 * @author Miroslav Bićanić
 */
public class SkipCommand implements Command {
	/**
	 * The length for which to move the turtle on the screen
	 */
	private double step;
	/**
	 * A constructor for a SkipCommand
	 * @param step The length for which this SkipCommand will move the turtle
	 */
	public SkipCommand(double step) {
		this.step = step;
	}
	/**
	 * {@inheritDoc}
	 */
	@Override
	public void execute(Context ctx, Painter painter) {
		ctx.getCurrentState().getPosition().translate( 
				ctx.getCurrentState().getDirection().scaled(
						step*ctx.getCurrentState().getStep()
					)
			);
	}

}
