package hr.fer.zemris.lsystems.impl.commands;

import hr.fer.zemris.lsystems.Painter;
import hr.fer.zemris.lsystems.impl.Command;
import hr.fer.zemris.lsystems.impl.Context;

/**
 * RotateCommand rotates the direction of the current 
 * {@link TurtleState}, making the turtle now move in
 * the rotated direction.
 * 
 * The angle must be expressed in radians.
 * 
 * @author Miroslav Bićanić
 */
public class RotateCommand implements Command{
	/**
	 * The angle for which to rotate the turtle
	 */
	private double angle;
	/**
	 * A constructor for a RotateCommand
	 * @param angle The angle for which this RotateCommand will rotate the current direction
	 */
	public RotateCommand(double angle) {
		this.angle = angle;
	}
	/**
	 * {@inheritDoc}
	 */
	@Override
	public void execute(Context ctx, Painter painter) {
		ctx.getCurrentState().getDirection().rotate(angle);
	}
}
