package hr.fer.zemris.lsystems.impl.commands;

import hr.fer.zemris.lsystems.Painter;
import hr.fer.zemris.lsystems.impl.Command;
import hr.fer.zemris.lsystems.impl.Context;

/**
 * PopCommand pops one {@link TurtleState} from the top
 * of the {@link Context}, returning the turtle to its
 * previous state.
 * 
 * @author Miroslav Bićanić
 */
public class PopCommand implements Command{
	/**
	 * {@inheritDoc}
	 */
	@Override
	public void execute(Context ctx, Painter painter) {
		ctx.popState();
	}

}
