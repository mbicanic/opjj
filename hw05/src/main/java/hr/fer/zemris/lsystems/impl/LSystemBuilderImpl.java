package hr.fer.zemris.lsystems.impl;

import java.awt.Color;
import java.util.Arrays;
import java.util.Objects;

import hr.fer.zemris.java.custom.collections.Dictionary;
import hr.fer.zemris.lsystems.LSystem;
import hr.fer.zemris.lsystems.LSystemBuilder;
import hr.fer.zemris.lsystems.Painter;
import hr.fer.zemris.lsystems.impl.commands.*;
import hr.fer.zemris.math.Vector2D;

/**
 * This class is an implementation of an {@link LSystemBuilder}
 * that builds a Lindenmayer System from parameters in two 
 * different ways.
 * 
 * One way is to define it by calling adequate methods, with
 * each method setting one parameter. The second way is to define
 * the whole LSystem through a textual definition.
 * 
 * A Lindermayer System uses productions, similar to those in
 * context insensitive grammars, to generate fractals.
 * It requires an axiom - the initial sequence of characters,
 * a set of productions that define how to produce sequences 
 * from an axiom and a number of productions to apply to the axiom. 
 * The resulting sequence is used to draw fractals such as a
 * Koch Curve.
 * 
 * An example of productions:
 * Axiom: 					X+Y
 * Productions: 			X => X-Y-X, Y => Y+X+Y
 * Level: 					1
 * Resulting sequence:	 	X-Y-X+Y+X+Y
 * 
 * All the symbols of the alphabet (in above example: X, Y, +, -) can
 * be set to trigger a certain command for the turtle.
 * 
 * @author Miroslav Bićanić
 */
public class LSystemBuilderImpl implements LSystemBuilder {
	
	/**
	 * A dictionary holding the values of hexadecimal digits larger than 9,
	 * for easier conversion.
	 */
	private static final Dictionary<Character,Integer> hexDigits = new Dictionary<>();
	static {
		hexDigits.put('A', 10);
		hexDigits.put('B', 11);
		hexDigits.put('C', 12);
		hexDigits.put('D', 13);
		hexDigits.put('E', 14);
		hexDigits.put('F', 15);
	}
	
	/**
	 * A dictionary holding all the registered actions for the turtle.
	 * The key is the letter the user has to write to execute the command.
	 */
	private Dictionary<String, Command> actions = new Dictionary<String, Command>();
	/**
	 * A dictionary holding all the registered productions for the turtle.
	 */ 
	private Dictionary<String, String> productions = new Dictionary<String, String>();
	/**
	 * This defines the initial length of one unit of movement of the turtle.
	 * By default, it is set to 10% of window width/height (depending on orientation).
	 */ 
	private double unitLength = 0.1;
	/**
	 * A scaler used to scale the unit length in every next level of production so
	 * the fractal can fit onto a screen and maintain a permanent size.
	 * By default, it is set to 1 (meaning no resizing is done).
	 */ 
	private double unitLengthDegreeScaler = 1;
	/**
	 * The point from which the turtle starts drawing.
	 * By default, it is the lower left corner of the screen.
	 */
	private Vector2D origin = new Vector2D(0,0);
	/**
	 * The initial angle in which the turtle looks, expressed in degrees.
	 * The angles are interpreted mathematically: an angle 0 means the turtle 
	 * is "looking" to the right, and an angle 90 means the turtle is "looking" up.
	 * By default, the turtle is looking to the right.
	 */
	private double angle = 0;
	/**
	 * An axiom from which all productions begin.
	 * By default, it is an empty string.
	 */
	private String axiom = "";
	
	/**
	 * An implementation of an L-System which generates a character
	 * sequence from the axiom and a given number of productions, 
	 * and then draws the figure using the resulting sequence.
	 * 
	 * @author Miroslav Bićanić
	 */
	private class LSystemImpl implements LSystem {
		 
		/** 
		 * This method initializes a TurtleState and a Context in which
		 * the figure will be drawn, after which it executes commands
		 * in accordance to the character sequence for the given level.
		 * 
		 * @param level The amount of productions to be applied to the axiom
		 * @param painter A Painter object used to draw the figure on the screen
		 * @throws NullPointerException if given painter is null
		 */
		@Override
		public void draw(int level, Painter painter) {
			Objects.requireNonNull(painter);
			Context ctx = new Context();
			double radAngle = angle * (2*Math.PI)/360;
			TurtleState initial = new TurtleState(
					origin.copy(),
					new Vector2D(Math.cos(radAngle), Math.sin(radAngle)),
					Color.BLACK,
					unitLength * Math.pow(unitLengthDegreeScaler,level));
		
			ctx.pushState(initial);
			
			String sequence = generate(level);
			for(int i=0, n = sequence.length(); i < n; i++) {
				String c = sequence.substring(i,i+1);
				if(actions.get(c)!=null) {
					actions.get(c).execute(ctx, painter);
				}
			}
		}

		/** 
		 * This method generates a String from the axiom applying the
		 * productions repeatedly on the previous generation a given 
		 * number of times. The first production is applied to the axiom.
		 * 
		 * @param level The number of productions to apply
		 */
		@Override
		public String generate(int level) {
			String prevLevel = axiom;
			for(int lvl = 0; lvl < level; lvl++) {
				StringBuilder interResult = new StringBuilder(50);
				for(int i = 0; i < prevLevel.length(); i++) {
					String c = Character.toString(prevLevel.charAt(i));
					if(productions.get(c)!=null) {
						interResult.append(productions.get(c));
					} else {
						interResult.append(c);
					}
				}
				prevLevel = interResult.toString();
			}
			return prevLevel;
		}
	}
	
	/** 
	 * This method creates an LSystem based on the parameters defined
	 * within this class.
	 */
	@Override
	public LSystem build() {
		return new LSystemImpl();
	}

	/**
	 * This method parses the given command and adds it to this 
	 * LSystemBuilder's dictionary of actions under the given symbol.
	 * 
	 * @param symbol The symbol under which to store the command
	 * @param command The string representation of the command
	 * @throws NullPointerException if given command is null
	 */
	@Override
	public LSystemBuilder registerCommand(char symbol, String command) {
		Objects.requireNonNull(command);
		
		command = command.trim();
		String[] tokens = command.split("\\s+");
		tokens = toLowercase(tokens);
				
		parseCommandTokens(Character.toString(symbol), tokens);
		return this;
	}
	

	/** 
	 * This method registers the given production into this LSystemBuilder's
	 * dictionary of productions under the given symbol as the key.
	 * 
	 * @param symbol The symbol under which to store the production
	 * @param production The resulting string after a production to
	 * 					the symbol has been applied.
	 * @throws NullPointerException if given production is null
	 */
	@Override
	public LSystemBuilder registerProduction(char symbol, String production) {
		Objects.requireNonNull(production);
		productions.put(Character.toString(symbol), production);
		return this;
	}

	/** 
	 * This method sets the initial angle of the turtle in the LSystem.
	 * The angle is given in degrees.
	 */
	@Override
	public LSystemBuilder setAngle(double angle) {
		this.angle = angle;
		return this;
	}

	/** 
	 * Setter for the axiom of the LSystem.
	 * @param axiom The axiom to set to this LSystem
	 * @throws NullPointerException if given axiom is null
	 */
	@Override
	public LSystemBuilder setAxiom(String axiom) {
		Objects.requireNonNull(axiom);
		this.axiom = axiom;
		return this;
	}

	/**
	 * Setter for the origin point for the turtle in the LSystem.
	 */
	@Override
	public LSystemBuilder setOrigin(double x, double y) {
		this.origin = new Vector2D(x,y);
		return this;
	}

	/**
	 * Setter for the initial length of one unit step of
	 * the turtle in the LSystem.
	 */
	@Override
	public LSystemBuilder setUnitLength(double step) {
		unitLength = step;
		return this;
	}

	/** 
	 * Setter for the unit length scaler to the given value.
	 * The unit length of the turtle gets multiplied by this scaler
	 * once for every new level of productions.
	 */
	@Override
	public LSystemBuilder setUnitLengthDegreeScaler(double scaler) {
		unitLengthDegreeScaler = scaler;
		return this;
	}

	/**
	 * This method configures a whole LSystem from a textual definition 
	 * of its parameters.
	 * 
	 * Each string in the given array of strings must be either empty,
	 * or declare exactly one parameter and assign it a value.
	 * Empty strings are skipped.
	 * 
	 * General structure of a line is:
	 * "[LSystem parameter] [args...]"
	 * Where LSystem parameter stands for the name of any of the parameters 
	 * defining an LSystem (origin, angle, axiom...), and args are arguments
	 * separated by space, in accordance with the parameter from the line.
	 * Parameters and the arguments between generally all must be separated 
	 * by one or more spaces.
	 * 
	 * An example of a text that would result in a successful 
	 * configuration follows:
	 * 		"origin		0.05 0.4"					//X and Y coordinate of origin
	 * 		"angle		0"							//initial angle
	 * 		"unitLength 0.5"						//initial unit length
	 * 		"unitLengthDegreeScaler	1.0 / 3.0"		//initial unit length scaler
	 * 		"axiom		F"							//a string representing the initial axiom
	 * 		"production F F+F--F+F"					//character that is being replaced, string with which it is replaced
	 * 		"command F draw 1"						// [SEE BELOW]
	 * 		""										// skipped
	 * 		"command + rotate 60"
	 * 		"command - rotate -60"
	 * 
	 * When defining a command or a production, the first argument is 
	 * a trigger character. In defining productions, it is followed by the
	 * result of the production applied to the trigger character. 
	 * In defining commands it is followed by the command name, and then 
	 * additional arguments depending on the command name. 
	 * The general syntax for this is:
	 * "command [trigger] [command name] [args...]"
	 * 
	 * Command name can be only one of the following:
	 * 		draw s 			//s = percentage of one unit length 
	 * 		rotate s 		//s = angle to rotate
	 * 		skip s			//s = percentage of one unit length
	 * 		scale s			//s = a scaler with which to scale the unit length
	 * 		push
	 * 		pop
	 * 		color rrggbb 	//rrggbb = a hexadecimal representation of a color
	 * 
	 * Note: The parameter for unitLengthDegreeScaler can be given as one double number,
	 * 		or as a fraction of two double numbers.
	 * 
	 * @param lines An array of strings, each string configuring one parameter
	 * @see Command
	 * @see TurtleState
	 * @throws NullPointerException if the given array of lines is null
	 */
	@Override
	public LSystemBuilder configureFromText(String[] lines) {
		Objects.requireNonNull(lines);
		for(String line : lines) {
			if(line.length()==0) {
				continue;
			}
			line = line.trim();
			parseLine(line);
		}
		return this;
	}

	/**
	 * This method parses a line according to the rules described in the
	 * {@code configureFromText} method.
	 * @param line The line to parse
	 * @throws LSystemException
	 */
	private void parseLine(String line) {
		String parameterName = getParameterName(line);
		line = line.substring(parameterName.length()).trim();	//-> the line without the [LSystem parameter] field
		String[] tokens = line.split("\\s+");		
		
		switch(parameterName) {
		
		case("origin"):
			parseOrigin(tokens);
			break;
		case("angle"):
			try {
				angle = Double.parseDouble(line);
			} catch (NumberFormatException ex) {
				throw new LSystemException("Invalid angle parameter given: "+line);
			}
			break;
		case("unitlength"):
			try {
				unitLength = Double.parseDouble(line);
			} catch (NumberFormatException ex) {
				throw new LSystemException("Invalid unit length parameter given: "+line);
			}
			break;
		case("unitlengthdegreescaler"):
			parseUnitScaler(line);
			break;
		case("axiom"):
			axiom = line;
			break;
		case("production"):
			parseProduction(tokens);
			break;
		case("command"):
			if(tokens.length<2 || tokens[0].length()!=1) {
				throw new LSystemException("Invalid format of arguments in command definition.");
			}
			parseCommandTokens(tokens[0], Arrays.copyOfRange(tokens, 1, tokens.length));
			break;
		}
	}

	/**
	 * This method extracts the name of the command as the
	 * first sequence of non-whitespace characters in a line.
	 * 
	 * @param line The line from which to extract
	 * @return The name of the command
	 */
	private String getParameterName(String line) {
		int i;
		for(i = 0; !Character.isWhitespace(line.charAt(i)); i++) {
		}
		return line.substring(0, i).toLowerCase();
	}

	/**
	 * This method extracts the X and Y coordinate parameters from the
	 * original line split on whitespaces.
	 * @param tokens The contents of the original line split on whitespaces
	 */
	private void parseOrigin(String[] tokens) {
		if(tokens.length!=2) {
			throw new LSystemException("Invalid number of arguments in origin definition. Expected 2.");
		}
		try {
			origin = new Vector2D(
					Double.parseDouble(tokens[0]),
					Double.parseDouble(tokens[1])
				);
		} catch (NumberFormatException ex) {
			throw new LSystemException("Invalid argument type in origin definition. Expected double.");
		}
	}
	
	/**
	 * This method extracts the unit scaler from the original line.
	 * The scaler can be written in two ways:
	 * "unitLengthDegreeScaler [double]"
	 * OR
	 * "unitLengthDegreeScaler [double] / [double]"
	 * 
	 * Note: Spaces between two double numbers and a frontslash
	 * 		are not required here. 
	 * 
	 * @param line The line from which to extract the unit length scaler
	 */
	private void parseUnitScaler(String line) {
		if(line.contains("/")) {
			String[] tokens = line.split("\\s*/\\s*");
			if(tokens.length!=2) {
				throw new LSystemException("Invalid argument format with unit scaler: "+line);
			}
			try {
				unitLengthDegreeScaler = Double.parseDouble(tokens[0]) / Double.parseDouble(tokens[1]);
			} catch (NumberFormatException ex) {
				throw new LSystemException("Invalid format of numbers in unit scaler: "+line);
			}
		} else {
			line = line.trim();
			try {
				unitLengthDegreeScaler = Double.parseDouble(line);
			} catch (NumberFormatException ex) {
				throw new LSystemException("Invalid number format in unit scaler: "+line);
			}
		}
	}
	
	/**
	 * This method registers a new command into the actions dictionary under
	 * the given sign, based on the arguments given in the tokens array.
	 * 
	 * @param sign The trigger character for the command to be executed
	 * @param tokens The arguments for the registered command 
	 * 				(command name + an additional argument with some commands).
	 */
	private void parseCommandTokens(String sign, String[] tokens) {
		if(tokens.length==2) {
			switch(tokens[0]) {
			case("draw"):
				try {
					double step = Double.parseDouble(tokens[1]);
					actions.put(sign, new DrawCommand(step));
				} catch (NumberFormatException ex) {
					throw new LSystemException("Invalid argument for draw command: "+tokens[1]);
				}
				break;
			case("skip"):
				try {
					double step = Double.parseDouble(tokens[1]);
					actions.put(sign, new SkipCommand(step));
				} catch (NumberFormatException ex) {
					throw new LSystemException("Invalid argument for skip command: "+tokens[1]);
				}
				break;
			case("scale"):
				try {
					double factor = Double.parseDouble(tokens[1]);
					actions.put(sign, new ScaleCommand(factor));
				} catch (NumberFormatException ex) {
					throw new LSystemException("Invalid argument for scale command: "+tokens[1]);
				}
				break;
			case("rotate"):
				try {
					double angle = Double.parseDouble(tokens[1]);
					actions.put(sign, new RotateCommand((angle  * 2 *Math.PI)/360.));
				} catch (NumberFormatException ex) {
					throw new LSystemException("Invalid argument for rotate command: "+tokens[1]);
				}
				break;
			case("color"):
				String rgb = tokens[1];
				actions.put(sign, new ColorCommand(parseColor(rgb)));
				break;
			default:
				throw new LSystemException("Unknown command: "+tokens[0]);
			}
		} else if (tokens.length==1) {
			switch(tokens[0]) {
			case("push"):
				actions.put(sign, new PushCommand());
				break;
			case("pop"):
				actions.put(sign, new PopCommand());
				break;
			default:
				throw new LSystemException("Unknown command: "+tokens[0]);
			}
		} else {
			throw new LSystemException("Unknown command format!");
		}
	}
	
	/**
	 * This method registers a new production into this LSystemBuilder's
	 * production dictionary from a given array of strings.
	 * 
	 * @param tokens An array of strings, meant to contain the trigger character
	 * 					and the production for it.
	 */
	private void parseProduction(String[] tokens) {
		if(tokens.length!=2 || tokens[0].length()!=1) {
			throw new LSystemException("Invalid arguments for a production definition!");
		}
		registerProduction(tokens[0].charAt(0), tokens[1]);
	}
	
	
	/**
	 * This method takes an array of strings and converts them
	 * all to lowercase strings.
	 * 
	 * @param tokens An array of strings to convert
	 * @return The same array, with all strings in it converted to lowercase
	 */
	private String[] toLowercase(String[] tokens) {
		for(int i = 0; i < tokens.length; i++) {
			tokens[i] = tokens[i].toLowerCase();
		}
		return tokens;
	}

	/**
	 * This method parses a String containing the hexadecimal representation
	 * of a color in the RRGGBB format.
	 * 
	 * @param rgb A string in the RRGGBB format, with each character being a hexadecimal digit
	 * @return A color specified by the input string
	 */
	private Color parseColor(String rgb) {
		if(rgb.length()!=6) {
			throw new LSystemException("Invalid format of string for color representation: "+rgb);
		}
		int red = stringToHex(rgb.substring(0,2));
		int green = stringToHex(rgb.substring(2,4));
		int blue = stringToHex(rgb.substring(4,6));
		return new Color(red, green, blue);
	}

	/**
	 * This method converts a whole hexadecimal number written as a string into 
	 * a decimal integer.
	 *  
	 * @param hex The number to convert
	 * @return The integer value of the given hex number
	 */
	private int stringToHex(String hex) {
		int sum=0;
		int power = 1;
		for(int i = hex.length()-1; i >=0; i--) {
			if(Character.isDigit(hex.charAt(i))) {
				sum+=power*Integer.parseInt(Character.toString(hex.charAt(i)));
			}
			else {
				Integer decimalValue = hexDigits.get(hex.charAt(i));
				if(decimalValue==null) {
					throw new LSystemException("Invalid hexadecimal digit given: "+hex.charAt(i));
				}
				sum+=power*decimalValue;
			}
			power*=16;
		}
		return sum;
	}
}
