package hr.fer.zemris.lsystems.impl;

/**
 * This exception is thrown whenever an LSystemBuilder or
 * an LSystem itself encounter an error.
 * 
 * @author Miroslav Bićanić
 */
public class LSystemException extends RuntimeException {
	private static final long serialVersionUID = 1L;
	
	/**
	 * Constructor for an LSystemException taking a message parameter
	 * containing the description of the error causing it.
	 * @param message The description of the error
	 */
	public LSystemException(String message) {
		super(message);
	}

}
