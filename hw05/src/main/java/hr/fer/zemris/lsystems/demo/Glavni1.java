package hr.fer.zemris.lsystems.demo;
import hr.fer.zemris.lsystems.impl.*;
import hr.fer.zemris.lsystems.*;
import hr.fer.zemris.lsystems.gui.*;

/**
 * First demonstrational program configuring an LSystem
 * by calling appropriate methods and drawing a Koch Curve.
 * 
 * @author Miroslav Bićanić
 */
public class Glavni1 {
	public static void main(String[] args) {
		LSystemViewer.showLSystem(createKochCurve(LSystemBuilderImpl::new));
	}
	
	private static LSystem createKochCurve(LSystemBuilderProvider lsbp) {
		return lsbp.createLSystemBuilder()
				.registerCommand('F', "draw 1")
				.registerCommand('+', "rotate 60")
				.registerCommand('-', "rotate -60")
				.setOrigin(0.05, 0.4)
				.setAngle(0)
				.setUnitLength(0.9)
				.setUnitLengthDegreeScaler(1.0/3.0)
				.registerProduction('F', "F+F--F+F")
				.setAxiom("F")
				.build();
	}
}
