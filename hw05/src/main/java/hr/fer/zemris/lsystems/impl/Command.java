package hr.fer.zemris.lsystems.impl;
import hr.fer.zemris.lsystems.Painter;

/**
 * Represents an action in the process of drawing fractals through
 * an LSystem.
 * 
 * This is a functional interface whose functional method is
 * {@link #execute(Context, Painter)}.
 *
 * @author Miroslav Bićanić
 */
@FunctionalInterface
public interface Command {
	/**
	 * Executes the command in the given context.
	 * @param ctx The context in which to execute the command
	 * @param painter A painter used to draw on the screen
	 */
	void execute(Context ctx, Painter painter);
}
