package hr.fer.zemris.lsystems.impl;

import hr.fer.zemris.java.custom.collections.ObjectStack;

/**
 * This class represents the context in which a turtle draws.
 * It contains a stack of {@link TurtleState} objects to
 * enable the turtle to draw multiple segments "at once".
 * 
 * @author Miroslav Bićanić
 */
public class Context {
	
	/**
	 * A stack of TurtleState objects.
	 */
	private ObjectStack<TurtleState> stack;
	
	/**
	 * A default constructor for a Context, initializing the stack.
	 */
	public Context() {
		stack = new ObjectStack<TurtleState>();
	}
	
	/**
	 * This method retrieves the TurtleState from the top of the stack,
	 * without removing it from the top.
	 * 
	 * A TurtleState on top of the stack is considered "current state".
	 * 
	 * @return The current TurtleState
	 */
	public TurtleState getCurrentState() {
		return stack.peek();
	}
	/**
	 * This method pushes a TurtleState to the top of the stack.
	 * 
	 * @param state A TurtleState to push on the stack
	 */
	public void pushState(TurtleState state) {
		stack.push(state);
	}
	/**
	 * This method removes one TurtleState from the top of the stack.
	 */
	public void popState() {
		stack.pop();
	}
	
}
