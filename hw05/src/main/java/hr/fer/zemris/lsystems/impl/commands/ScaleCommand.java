package hr.fer.zemris.lsystems.impl.commands;

import hr.fer.zemris.lsystems.Painter;
import hr.fer.zemris.lsystems.impl.Command;
import hr.fer.zemris.lsystems.impl.Context;

/**
 * ScaleCommand scales the length of the unit step of the current
 * {@link TurtleState} by a given factor.
 * 
 * @author Miroslav Bićanić
 */
public class ScaleCommand implements Command {
	/**
	 * The factor with which to scale the step of the current state
	 */
	private double factor;
	/**
	 * A constructor for a ScaleCommand
	 * @param factor The factor with which this ScaleCommand will scale the current unit step
	 */
	public ScaleCommand(double factor) {
		this.factor = factor;
	}
	/**
	 * {@inheritDoc}
	 */
	@Override
	public void execute(Context ctx, Painter painter) {
		ctx.getCurrentState().setStep(
				ctx.getCurrentState().getStep()*factor
			);
	}

}
