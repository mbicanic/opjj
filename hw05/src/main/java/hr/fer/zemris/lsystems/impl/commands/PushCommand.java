package hr.fer.zemris.lsystems.impl.commands;

import hr.fer.zemris.lsystems.Painter;
import hr.fer.zemris.lsystems.impl.Command;
import hr.fer.zemris.lsystems.impl.Context;

/**
 * PushCommand pushes a copy of the current {@link TurtleState} to the top
 * of the {@link Context}, switching the current state to the copied state,
 * allowing the turtle to later come back to its previous state.
 * 
 * @author Miroslav Bićanić
 */
public class PushCommand implements Command{
	/**
	 * {@inheritDoc}
	 */
	@Override
	public void execute(Context ctx, Painter painter) {
		ctx.pushState(ctx.getCurrentState().copy());
	}

}
