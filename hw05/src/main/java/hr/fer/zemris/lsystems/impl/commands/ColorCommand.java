package hr.fer.zemris.lsystems.impl.commands;

import java.awt.Color;

import hr.fer.zemris.lsystems.Painter;
import hr.fer.zemris.lsystems.impl.Command;
import hr.fer.zemris.lsystems.impl.Context;

/**
 * ColorCommand changes the drawing color of the current
 * TurtleState.
 * 
 * @author Miroslav Bićanić
 */
public class ColorCommand implements Command {
	/**
	 * The color which this ColorCommand sets
	 */
	private Color color;
	/**
	 * A constructor for a ColorCommand
	 * @param color The color that this ColorCommand will set
	 */
	public ColorCommand(Color color){
		this.color = color;
	}
	/**
	 * {@inheritDoc}
	 */
	@Override
	public void execute(Context ctx, Painter painter) {
		ctx.getCurrentState().setColor(color);
	}

}
