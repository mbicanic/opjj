package hr.fer.zemris.lsystems.impl;

import java.awt.Color;
import java.util.Objects;

import hr.fer.zemris.math.Vector2D;

/**
 * This class represents a state of the turtle that
 * draws on the screen.
 * A TurtleState is defined by the turtle's position,
 * direction, color it draws in and the length of its
 * step (the length of the line drawn when commanded
 * to draw).
 * 
 * @author Miroslav Bićanić
 *
 */
public class TurtleState {
	/**
	 * The current position of the turtle on the screen.
	 */
	private Vector2D position;
	/**
	 * The direction in which the turtle is currently looking.
	 * The backing vector has a length of 1.
	 */
	private Vector2D direction;
	/**
	 * The color in which the turtle draws.
	 */
	private Color color;
	/**
	 * The length of one unit step a turtle makes.
	 */
	private double step;
	
	
	
	public TurtleState(Vector2D position, Vector2D direction, Color color, double step) {
		this.position = Objects.requireNonNull(position);
		this.direction = Objects.requireNonNull(direction);
		this.color = Objects.requireNonNull(color);
		this.step = Objects.requireNonNull(step);
	}
	/**
	 * @return the current position of the turtle
	 */
	public Vector2D getPosition() {
		return position;
	}
	/**
	 * @param position The new position of the turtle
	 */
	public void setPosition(Vector2D position) {
		this.position = position;
	}

	/**
	 * @return the current direction of the turtle
	 */
	public Vector2D getDirection() {
		return direction;
	}
	/**
	 * @param direction The new direction of the turtle
	 */
	public void setDirection(Vector2D direction) {
		this.direction = direction;
	}

	/**
	 * @return the color in which the turtle currently draws
	 */
	public Color getColor() {
		return color;
	}
	/**
	 * @param color The new color for the turtle to draw in
	 */
	public void setColor(Color color) {
		this.color = color;
	}

	/**
	 * @return The current unit step of the turtle
	 */
	public double getStep() {
		return step;
	}
	/**
	 * @param step The new unit step for the turtle
	 */
	public void setStep(double step) {
		this.step = step;
	}

	/**
	 * This method returns a new TurtleState with identical parameters
	 * as this one, but in such a way that changes to the copy do not
	 * affect the original.
	 * 
	 * Only a completely defined TurtleState can be copied. If copy() is
	 * called from a non-complete TurtleState, null is returned.
	 * 
	 * @return A copy of this TurtleState
	 */
	public TurtleState copy() {
		return new TurtleState(
				position.copy(),
				direction.copy(),
				new Color(color.getRed(), color.getGreen(), color.getBlue()),
				step);
	}
}
