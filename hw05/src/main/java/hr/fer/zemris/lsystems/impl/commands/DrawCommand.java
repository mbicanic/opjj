package hr.fer.zemris.lsystems.impl.commands;

import hr.fer.zemris.lsystems.Painter;
import hr.fer.zemris.lsystems.impl.Command;
import hr.fer.zemris.lsystems.impl.Context;
import hr.fer.zemris.lsystems.impl.TurtleState;

/**
 * DrawCommand draws a line on the screen using the parameters
 * (color, direction, position) from the current {@link TurtleState}
 * 
 * @author Miroslav Bićanić
 */
public class DrawCommand implements Command {
	/**
	 * The length of the line to be drawn on the screen
	 */
	private double step;
	/**
	 * A constructor for a DrawCommand
	 * @param step The length of the line this DrawCommand will draw
	 */
	public DrawCommand(double step) {
		this.step = step;
	}
	/**
	 * {@inheritDoc}
	 */
	@Override
	public void execute(Context ctx, Painter painter) {
		double x0 = ctx.getCurrentState().getPosition().getX();
		double y0 = ctx.getCurrentState().getPosition().getY();
		
		ctx.getCurrentState().getPosition().translate( 
				ctx.getCurrentState().getDirection().scaled(
						step*ctx.getCurrentState().getStep()
					)
			);
		double x1 = ctx.getCurrentState().getPosition().getX();
		double y1 = ctx.getCurrentState().getPosition().getY();
		painter.drawLine(x0, y0, x1, y1, ctx.getCurrentState().getColor(), 1f);
	}

}
