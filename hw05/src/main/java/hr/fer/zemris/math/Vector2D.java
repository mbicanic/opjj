package hr.fer.zemris.math;

import java.util.Objects;

/**
 * This class models a two-dimensional vector
 * in Cartesian coordinates.
 * 
 * @author Miroslav Bićanić
 */
public class Vector2D {
	/**
	 * The maximal difference between two double values for us to consider them equal.
	 */
	private static final double DECIMAL_TOLERANCE = 1e-7;
	/**
	 * This vector's X axis coordinate.
	 */
	private double x;
	/**
	 * This vector's Y axis coordinate.
	 */
	private double y;
	
	/**
	 * A constructor for a vector taking both parameters for it.
	 * @param x The x coordinate of the vector
	 * @param y The y coordinate of the vector
	 */
	public Vector2D(double x, double y) {
		this.x = x;
		this.y = y;
	}

	/**
	 * @return This vector's X coordinate
	 */
	public double getX() {
		return x;
	}
	/**
	 * @return This vector's Y coordinate
	 */
	public double getY() {
		return y;
	}
	
	/**
	 * This method translates this vector's coordinates
	 * for the amount specified by the given vector's
	 * coordinates.
	 * 
	 * @param offset A vector dictating for how much to translate this one
	 */
	public void translate(Vector2D offset) {
		Objects.requireNonNull(offset);
		x+=offset.x;
		y+=offset.y;
	}
	/**
	 * This method returns a new vector with the coordinates
	 * equal to the coordinates of this one translated by
	 * the coordinates of offset vector.
	 * 
	 * @param offset A vector dictating for how much to translate this one
	 * @return A new vector with the translated coordinates
	 */
	public Vector2D translated(Vector2D offset) {
		Objects.requireNonNull(offset);
		return new Vector2D(x+offset.x, y+offset.y);
	}
	
	/**
	 * This method rotates this vector for the given angle.
	 * 
	 * @param angle The angle for which to rotate this vector.
	 */
	public void rotate(double angle) {
		double newX = x*Math.cos(angle) - y*Math.sin(angle);
		double newY = x*Math.sin(angle) + y*Math.cos(angle);
		x = newX;
		y = newY;
	}
	/**
	 * This method returns a new vector with coordinates
	 * equal to this vector rotated for the given angle.
	 * 
	 * @param angle An angle for which to rotate 
	 * @return A new vector with the rotated coordinates
	 */
	public Vector2D rotated(double angle) {
		double newX = x*Math.cos(angle) - y*Math.sin(angle);
		double newY = x*Math.sin(angle) + y*Math.cos(angle);
		return new Vector2D(newX, newY);
	}
	
	/**
	 * This method scales this vector's coordinates
	 * using a scaler given as a parameter.
	 * 
	 * @param scaler The number with which to scale this vector
	 */
	public void scale(double scaler) {
		if(scaler<0) {
			throw new IllegalArgumentException("Can't scale negatively!");
		}
		x*=scaler;
		y*=scaler;
	}
	/**
	 * This method returns a new vector that is equal
	 * to this vector scaled by the given scaler.
	 * 
	 * @param scaler The number with which to scale this vector
	 * @return A new vector with the scaled coordinates
	 */
	public Vector2D scaled(double scaler) {
		if(scaler<0) {
			throw new IllegalArgumentException("Can't scale negatively!");
		}
		return new Vector2D(x*scaler, y*scaler);
	}
	
	/**
	 * Returns a new vector that is a copy of this one.
	 * 
	 * @return A new vector equal to this one
	 */
	public Vector2D copy() {
		return new Vector2D(x, y);
	}

	/**
	 * This method says if two vectors are equal.
	 * Two vectors are equal if their whole number value and first
	 * 7 decimals are equal.
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Vector2D))
			return false;
		Vector2D other = (Vector2D) obj;
		return Math.abs(x-other.x)<DECIMAL_TOLERANCE && Math.abs(y-other.y)<DECIMAL_TOLERANCE;
	}
	
}
