package hr.fer.zemris.java.hw05.db;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import hr.fer.zemris.java.hw05.db.lex.QueryException;

/**
 * This class is a shell through which queries are sent to the
 * {@link StudentDatabase}, and shows the output of the database
 * for a given query to the standard output.
 * 
 * Only one command is supported in this shell, and that is the
 * query command - all user input must begin with the word "query",
 * followed by conditional expressions.
 * 
 * Queries that consist of ONLY ONE conditional expression, whose
 * field name is the JMBAG attribute and whose operator is the
 * '=' operator are called direct queries, and they are executed
 * in O(1) complexity.
 * 
 * All other queries use filtering by all provided conditional
 * expressions.
 * 
 * When starting, a filename can be given through a command line,
 * and the database is loaded from there. Otherwise, the default
 * database is used.
 * 
 * @author Miroslav Bićanić
 */
public class StudentDB {
	/**
	 * The name of the default database for the program.
	 */
	private static String filename = "./database.txt";
	/**
	 * This constant is the textual representation of the
	 * query command.
	 */
	private static final String QUERY_COMMAND = "query";
	
	/**
	 * The entry point of the program.
	 * 
	 * @param args One possible argument: the name of the database source file
	 */
	public static void main(String[] args) {
		if(args.length==1) {
			filename = args[0];
		}
		
		StudentDatabase sdb = getSDB();
		if(sdb==null) {
			System.out.println("Terminating...");
			return;
		}
		
		Scanner sc = new Scanner(System.in);
		
		execute(sdb, sc);
		
		sc.close();
		System.out.println("Goodbye!");
	}

	/**
	 * This is the method controlling the execution of the program,
	 * accepting queries one by one and displaying their results
	 * on standard output.
	 * 
	 * @param sdb A StudentDatabase with which to work
	 * @param sc A Scanner object to read from the standard input
	 */
	private static void execute(StudentDatabase sdb, Scanner sc) {
		while(true) {
			System.out.printf("> ");
			String request = sc.nextLine().trim();
			
			if(request.equalsIgnoreCase("exit")) {
				break;
			}
			if(!isValidCommand(request)) {
				System.err.println("Unsupported command format!\n");
				continue;
			}
			request = request.substring(5).trim();	
			
			List<StudentRecord> records;
			try {
				records = fetchRequestedRecords(sdb, request);
			} catch (QueryException ex) {
				System.err.println(ex.getMessage());
				System.out.println();
				continue;
			}
			List<String> outputRecords = RecordFormatter.format(records);
			
			if(outputRecords==null) {
				System.out.printf("Records selected: 0.%n%n");
				continue;
			} 
			outputRecords.forEach(System.out::println);
			System.out.printf("Records selected: %d.%n%n", outputRecords.size()-2);
		}
	}
	
	/**
	 * This method checks if the user-entered request is interpretable by 
	 * the database.
	 * A correct request starts with the word "query" followed by at least
	 * one whitespace character.
	 * 
	 * @param request The user-entered query
	 * @return true if it is a valid request, false otherwise
	 */
	private static boolean isValidCommand(String request) {
		if(!request.toLowerCase().startsWith(QUERY_COMMAND)) {
			return false;
		}
		if(request.length()<QUERY_COMMAND.length()) { 
			return false;
		}
		return true;
	}
	
	/**
	 * This method tries to read all the lines from the source file,
	 * and then creates a StudentDatabase based on them.
	 *
	 * @return A StudentDatabase built from the text file
	 */
	private static StudentDatabase getSDB() {
		List<String> lines;
		try{
			lines = Files.readAllLines(    
					Paths.get(filename),
					StandardCharsets.UTF_8);
		} catch(IOException e) {
			System.err.println("An exception occured while trying to open the file.");
			return null;
		}
		try {
			return new StudentDatabase(lines);
		} catch (QueryException ex) {
			System.err.println(ex.getMessage());
			return null;
		}
	}

	/**
	 * This method forwards the query to a parser, and then retrieves the records
	 * evaluated as acceptable by the provided conditions from the database,
	 * doing so as a direct query if possible.
	 * 
	 * @param sdb A StudentDatabase from which the records are fetched
	 * @param request The user's query without the 'query ' command
	 * @return A list of StudentRecord objects that passed all the provided conditions
	 */
	private static List<StudentRecord> fetchRequestedRecords(StudentDatabase sdb, String request) {
		QueryParser parser = new QueryParser(request);
		List<StudentRecord> recs = new ArrayList<>();
		
		if(parser.isDirectQuery()) {
			StudentRecord r = sdb.forJMBAG(parser.getQueriedJMBAG());
			if(r!=null) {
				recs.add(r);
			}
			System.out.println("Using index for record retrieval.");
			return recs;
		}
		
		for(StudentRecord r : sdb.filter(new QueryFilter(parser.getQuery()))) {
			recs.add(r);
		}
		return recs;
		
	}
	
}
