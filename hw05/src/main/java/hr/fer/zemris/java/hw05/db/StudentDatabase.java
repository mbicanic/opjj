package hr.fer.zemris.java.hw05.db;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import hr.fer.zemris.java.hw05.db.lex.QueryException;

/**
 * A StudentDatabase models a simple database of students.
 * <br><br>
 * The database takes a list of strings where each string represents
 * one StudentRecord and creates {@link StudentRecord} objects, adding
 * them to the internal list of StudentRecords.
 * <br><br>
 * The ID attribute of a StudentRecord is the JMBAG, thus, the database
 * does not allow multiple records with the same JMBAG attribute.
 * <br><br>
 * While creating instances of the StudentRecord class, the database
 * ensures that the format of the textual entry is correct, that the
 * grade is a number between 1 and 5, and that a record with the same
 * JMBAG doesn't exist.
 * <br><br>
 * The textual records in the file must be written in the following format:
 * [JMBAG] [LAST NAME(S)] [FIRST NAME(S)] [GRADE]
 * with each attribute separated by a tab.
 * Note: if there are multiple first or last names, they should be separated
 * 			with a space instead of a tab.
 * <br><br>
 * The database provides two ways to access its data:<br>
 * 	1) Directly with a known JMBAG:
 * 		One record with the given JMBAG is fetched instantly,
 * 		if it exists.<br>
 * 	2) Filtering all records through the {@link #filter(IFilter)} method,
 * 		using an IFilter to evaluate each record.
 * <br><br>
 * Please note that swapping the positions of the [JMBAG] and [GRADE] 
 * field in the input text might work, if the JMBAG was an integer
 * between 1 and 5 - but leads to inconsistencies in the database.
 * Similarly, the order of [FIRST NAME(S)], [LAST NAME(S)] and
 * [JMBAG] can be swapped at will, because the integrity constraints
 * of those fields are identical, but this also leads to inconsistencies.
 * 		
 * @author Miroslav Bićanić
 */
public class StudentDatabase {
	/**
	 * This is used as a delimiter of attribute values when reading
	 * the textual representation of a record.
	 */
	private static final String TAB = "\t";
	
	/**
	 * A list of all StudentRecords stored in the database
	 */
	private List<StudentRecord> records;
	/**
	 * A map used for direct access to a record with a known JMBAG.
	 * When records are added, their index in the records list is
	 * stored into this map under the unique JMBAG of that record.
	 */
	private Map<String, Integer> quickAccess;
	
	/**
	 * A constructor for a StudentDatabase.
	 * @param strRecords A list of lines, each of them representing a student record
	 * @throws NullPointerException if the given list is null
	 */
	public StudentDatabase(List<String> strRecords) {
		Objects.requireNonNull(strRecords);
		records = new ArrayList<>();
		quickAccess = new HashMap<>();
		setUp(strRecords);
	}

	/**
	 * This method parses each textual record and adds a new StudentRecord
	 * into the records list.
	 * @param strRecords A list of lines, each of them representing a student records
	 * @throws QueryException if the format of the textual record is incorrect
	 */
	private void setUp(List<String> strRecords) {
		int i = 0;
		for(String rec : strRecords) {
			String[] param = rec.split(TAB);
			if(param.length!=4) {
				throw new QueryException("Incorrect record format in text file: "+rec);
			}
			String jmbag = checkJmbag(param[0].trim());
			int grade = getCheckGrade(param[3].trim());
			String name = param[2].trim();
			String surname = param[1].trim();
			
			records.add(new StudentRecord(name, surname, jmbag, grade));
			quickAccess.put(jmbag, i);
			i++;
		}
	}

	/**
	 * This method tries to parse the grade from a string, and checks
	 * if the grade is within the inclusive [1,5] range.
	 * 
	 * @param strGrade A string representation of a grade
	 * @return The integer value of the given string
	 * @throws QueryException 
	 * 			-if the string is not parsable as an integer
	 * 			-if the grade is out of the [1,5] range
	 */
	private int getCheckGrade(String strGrade) {
		int grade;
		try{
			grade = Integer.parseInt(strGrade);
		} catch (NumberFormatException ex) {
			throw new QueryException("Invalid argument given as a grade: "+strGrade);
		}
		if(grade<1 || grade>5) {
			throw new QueryException("Illegal grade given in a record: "+grade);
		}
		return grade;
	}

	/**
	 * This method checks if a record with a given JMBAG already
	 * exists in the database, throwing an exception if so.
	 * 
	 * @param jmbag The JMBAG whose existance to check in the database
	 * @return jmbag the given JMBAG if it is not present in the database yet
	 * @throws QueryException if a record with the given JMBAG already exists
	 */
	private String checkJmbag(String jmbag) {
		if(quickAccess.containsKey(jmbag)) {
			throw new QueryException("Record with JMBAG "+jmbag+" already exists!");
		}
		return jmbag;
	}

	/**
	 * This method is used for direct record retrieval when
	 * the JMBAG is known. 
	 * 
	 * The requested record is retrieved in constant time.
	 * 
	 * @param jmbag The JMBAG by which to retrieve the record
	 * @return The record with the given jmbag if it exists
	 * @throws QueryException if called with a non-existant JMBAG
	 */
	public StudentRecord forJMBAG(String jmbag) {
		Integer index = quickAccess.get(jmbag);
		if(index==null) {
			throw new QueryException("Record with given JMBAG doesn't exist: "+jmbag);
		}
		return records.get(index);
	}
	
	/**
	 * This method returns a list of records created from all
	 * the records of this database that the given IFilter
	 * evaluated as acceptable.
	 * 
	 * @param filter A copy of an IFilter that evaluates records
	 * @return A list of StudentRecords that were evaluated as acceptable
	 */
	public List<StudentRecord> filter(IFilter filter){
		List<StudentRecord> selection = new ArrayList<>();
		for(StudentRecord sr : records) {
			if(filter.accepts(sr)) {
				selection.add(sr);
			}
		}
		return selection;
	}
	
}
