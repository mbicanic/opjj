package hr.fer.zemris.java.hw05.db;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * RecordFormatter is a class that provides methods that
 * produce an output formatted as an automatically sized
 * table of records.
 * 
 * @author Miroslav Bićanić
 */
public class RecordFormatter {
	
	/**
	 * This class holds the dimensions of each attribute cell in
	 * the table, adjusted for the longest attribute value
	 * with a space added on each side of it.
	 * 
	 * @author Miroslav Bićanić
	 */
	private static class CellSettings {
		/**
		 * The size of the cell in which the JMBAG attribute is written.
		 * By default it is set to 2, representing only the two spaces
		 * surrounding the jmbag.
		 */
		private int jmbagWidth = 2;
		/**
		 * The size of the cell in which the grade attribute is written:
		 * 1 character for the grade and 2 spaces around it.
		 */
		private final int gradeWidth = 1+2;
		/**
		 * The width of the cell in which the name attribute is written.
		 * By default it is set to 2, representing only the two spaces
		 * surrounding the name.
		 */
		private int nameWidth = 2;
		/**
		 * The width of the cell in which the name attribute is written.
		 * By default it is set to 2, representing only the two spaces
		 * surrounding the last name.
		 */
		private int surnameWidth = 2;
		/**
		 * This method calculates the total length of a formatted
		 * table record expressed in a number of characters.
		 * 
		 * @return The number of characters required for the longest record.
		 */
		public int sum() {
			return jmbagWidth + gradeWidth + nameWidth + surnameWidth + 5; //there are 5 walls in total
		}
	}
	
	/**
	 * This method takes a list of StudentRecord objects and formats
	 * their data into a table format.
	 * Please note that the returned list of strings contains two 
	 * additional elements that ARE NOT records: the upper table
	 * border and the lower table border.
	 * 
	 * @param records The records to format for table output
	 * @return A list of strings representing formatted StudentRecords
	 * @throws NullPointerException if given records are null
	 */
	public static List<String> format(List<StudentRecord> records) {
		Objects.requireNonNull(records);
		List<String> result = new ArrayList<>();
		if(records.size()==0) {
			return null;
		}
		CellSettings settings = getWidth(records);
		
		result.add(createBorder(settings));
		for(StudentRecord sr : records) {
			String formatted = getRecordFormat(sr, settings);
			result.add(formatted);
		}
		result.add(createBorder(settings));
		return result;
	}
	
	/**
	 * This method takes one StudentRecord and returns its String
	 * representation for table formatting, using a CellSettings
	 * object for specific cell dimensions.
	 * 
	 * @param sr A StudentRecord to transform
	 * @param settings An object containing information about the dimensions of cells in the table
	 * @return A string representation of this student record, ready for table output
	 */
	private static String getRecordFormat(StudentRecord sr, CellSettings settings) {
		StringBuilder sb = new StringBuilder(settings.sum());
		sb.append("| ")
		.append(sr.getJmbag());
		int remaining = settings.jmbagWidth - sr.getJmbag().length() -1;
		sb.append(generateSignSequence(' ', remaining))
		.append("| ")
		.append(sr.getLastName());
		
		//adding spaces for surnames and names shorter than the longest surname or name
		remaining = settings.surnameWidth - sr.getLastName().length() - 1; 
		sb.append(generateSignSequence(' ', remaining))
		.append("| ")
		.append(sr.getFirstName());
		
		remaining = settings.nameWidth - sr.getFirstName().length() - 1;
		sb.append(generateSignSequence(' ', remaining))
		.append("| ")
		.append(sr.getGrade())
		.append(" |");
		return sb.toString();
	}

	/**
	 * This method returns a string that is made by repeating
	 * the given character a given number of times.
	 * 
	 * @param c The character to repeat
	 * @param length The number of times to repeat it
	 * @return A string comprised of a given number of given characters
	 */
	private static String generateSignSequence(char c, int length) {
		StringBuilder sb = new StringBuilder(length);
		for(int i = 0; i < length; i++) {
			sb.append(c);
		}
		return sb.toString();
	}

	/**
	 * This method creates the horizontal borders of the table
	 * according to the cell dimensions specified in the 
	 * given CellSettings object.
	 * 
	 * @param settings An object specifying cell dimensions
	 * @return A string representation of a table border
	 */
	private static String createBorder(CellSettings settings) {
		StringBuilder sb = new StringBuilder(settings.sum());
		sb.append("+")
		.append(generateSignSequence('=', settings.jmbagWidth))
		.append("+")
		.append(generateSignSequence('=', settings.surnameWidth))
		.append("+")
		.append(generateSignSequence('=', settings.nameWidth))
		.append("+")
		.append(generateSignSequence('=', settings.gradeWidth))
		.append("+");
		
		return sb.toString();
	}
	
	/**
	 * This method creates a CellSettings object containing information
	 * about cell dimensions, based on a given list of records.
	 * 
	 * @param records The StudentRecords based on which the dimensions are calculated
	 * @return A CellSettings object containing cell dimensions.
	 */
	private static CellSettings getWidth(List<StudentRecord> records) {
		CellSettings settings = new CellSettings();
		for(StudentRecord r : records) {
			if(r.getJmbag().length()+2>settings.jmbagWidth) {
				settings.jmbagWidth = r.getJmbag().length()+2;
			}
			if(r.getFirstName().length()+2>settings.nameWidth) {
				settings.nameWidth = r.getFirstName().length() + 2;
			}
			if(r.getLastName().length()+2 >settings.surnameWidth) {
				settings.surnameWidth = r.getLastName().length() + 2;
			}
		}
		return settings;
	}
}
