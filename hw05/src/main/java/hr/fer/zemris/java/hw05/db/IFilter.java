package hr.fer.zemris.java.hw05.db;

/**
 * Represents an operation that takes a {@link StudentRecord}
 * returning true if it is accepted, and false otherwise.
 * 
 * This is a functional interface whose functional method is
 * {@link #accepts(StudentRecord)}.
 *
 * @author Miroslav Bićanić
 */
@FunctionalInterface
public interface IFilter {
	/**
	 * Evaluates this filter on the given StudentRecord.
	 * 
	 * @param record A StudentRecord to evaluate
	 * @return true if the evaluation is true; false otherwise
	 */
	boolean accepts(StudentRecord record);
}
