package hr.fer.zemris.java.hw05.db;

import java.util.Objects;

/**
 * This class models one conditional expression that can be
 * set up to describe all supported conditions.
 * 
 * In text, every expression is made of a field name, an
 * operator, and a string literal to which all records'
 * values under the given field name are compared.
 * 
 * A ConditionalExpression object contains an
 * {@link IFieldValueGetter}, an {@link IComparisonOperator}, 
 * and a literal String.
 * 
 * @see StudentDatabase for information on field names
 * @author Miroslav Bićanić
 *
 */
public class ConditionalExpression {
	/**
	 * An object that can retrieve the value of a certain attribute
	 * of a {@link StudentRecord} that is used for comparing with
	 * the literal value of the expression.
	 */
	private IFieldValueGetter fieldGetter;
	/**
	 * The literal value to which the attribute value is compared.
	 */
	private String value;
	/**
	 * An operator used to compare two string values.
	 */
	private IComparisonOperator operator;
	
	/**
	 * A constructor for a ConditionalExpression.
	 * @param fieldGetter Object that retrieves a certain attribute's value from a given StudentRecord
	 * @param value The literal string value to which the attribute's value is compared
	 * @param operator An operator used for comparing two string values
	 * @throws NullPointerException if any of the given parameters is null
	 */
	public ConditionalExpression(IFieldValueGetter fieldGetter, String val, IComparisonOperator operator) {
		this.fieldGetter = Objects.requireNonNull(fieldGetter);
		this.value = Objects.requireNonNull(val);
		this.operator = Objects.requireNonNull(operator);
	}

	/**
	 * @return this ConditionalExpression's IFieldValueGetter
	 */
	public IFieldValueGetter getFieldGetter() {
		return fieldGetter;
	}
	/**
	 * @return this ConditionalExpression's string literal
	 */
	public String getStringLiteral() {
		return value;
	}
	/**
	 * @return this ConditionalExpression's IComparisonOperator
	 */
	public IComparisonOperator getOperator() {
		return operator;
	}
	
	
}
