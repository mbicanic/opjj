package hr.fer.zemris.java.custom.collections;


/**
 * An interface for an object that has to be able to
 * iterate over a collection, returning all its elements.
 * 
 * @author Miroslav Bićanić
 */
public interface ElementsGetter<T> {
	/**
	 * This method says whether this iterator reached the
	 * end of the collection or not.
	 * @return true if the end is not reached, false otherwise
	 */
	boolean hasNextElement();
	
	/**
	 * This method gets the next element from the collection.
	 * @return The next element in the collection
	 */
	T getNextElement();
	/**
	 * This method performs an action on every element this
	 * ElementsGetter did not visit yet.
	 * @param p A processor object that does an action on elements
	 */
	default void processRemaining(Processor<T> p) {
		while(hasNextElement()) {
			p.process(getNextElement());
		}
	}
}
