package hr.fer.zemris.java.hw05.db.lex;

import java.util.Objects;

/**
 * This class models tokens with which the QueryLexer 
 * tokenizes the input query.
 * 
 * Each token has a type, and a value that it holds.
 * 
 * @see QueryTokenType
 * @author Miroslav Bićanić
 */
public class QueryToken {
	/**
	 * The type of the QueryToken
	 */
	private QueryTokenType type;
	/**
	 * The value of the QueryToken
	 */
	private String value;
	
	/**
	 * A constructor for a QueryToken setting up both the type and the value.
	 * 
	 * @param type The type of the QueryToken
	 * @param value The value this QueryToken will hold
	 */
	public QueryToken(QueryTokenType type, String value) {
		this.type = Objects.requireNonNull(type);
		this.value = Objects.requireNonNull(value);
	}

	/**
	 * @return The type of this QueryToken
	 */
	public QueryTokenType getType() {
		return type;
	}

	/**
	 * @return The value of this query token
	 */
	public String getValue() {
		return value;
	}

	/**
	 * Returns a string representation of the QueryToken in the format:
	 * "[TYPE => VALUE]"
	 */
	@Override
	public String toString() {
		return "[" + type + " => " + value + "]";
	}

	
}
