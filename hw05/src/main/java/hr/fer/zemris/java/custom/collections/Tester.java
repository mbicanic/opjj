package hr.fer.zemris.java.custom.collections;

/**
 * A parametrized interface of an object tester.
 * 
 * A Tester is an object that takes another object of
 * parametrised type and determines whether it satisfies 
 * that Tester's criteria. All classes implementing this
 * interface must define a process(T obj) method defining
 * the criteria.
 * 
 * @author Miroslav Bićanić
 */
@FunctionalInterface
public interface Tester<T> {
	
	/**
	 * This method returns true if the object satisfies 
	 * a criteria.
	 * 
	 * @param obj The object to be checked
	 * @return	true if it satisfies the criteria, false otherwise
	 */
	boolean test(T obj);
}
