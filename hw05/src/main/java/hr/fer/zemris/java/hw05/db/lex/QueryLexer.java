package hr.fer.zemris.java.hw05.db.lex;

import java.util.Objects;

/**
 * QueryLexer is a query tokenizer, taking in the query and
 * generating tokens of one of the {@link QueryTokenType} 
 * types, doing so on demand.
 * 
 * Once an EOQ token has been generated, calling the QueryLexer's
 * {@code nextToken} method causes an exception.
 * 
 * This lexer generates tokens of the following types:
 * 		FIELD 		-	Generated for strings of only letters that are not 
 * 						surrounded with anything.
 * 		OPERATOR	-	Generated for ALL sequences of characters that are
 * 						composed of the legal operator characters ('<', '>',
 * 						'!', '='. Note that a string such as ">>!=<!!" will
 * 						be accepted as an operator.
 * 						This token is also generated if the last generated token
 * 						was of FIELD type and the current token is equal to "LIKE".
 * 		STRING		-	Generated for any sequence of characters surrounded with
 * 						double quotation marks ( " ).
 * 		LINK		-	Generated when the last generated token was of STRING
 * 						type and the current token is equal to "and", ignoring
 * 						case.
 * 		EOQ			-	Generated when the end of file has been reached.
 * 
 * Tokens are extracted greedily, taking a maximal number of characters into
 * a token until the first illegal character for the decided token type is
 * reached. This means tokens that could be interpreted differently when 
 * written together must be separated by at least one space.
 * A space is necessary:
 * 		- between a FIELD and a LIKE operator
 * 		- between a LINK and a FIELD token
 * 
 * Other operators and strings can be written without any spaces surrounding
 * them.
 * 
 * @author Miroslav Bićanić
 */
public class QueryLexer {
	/**
	 * The keyword representing a logical AND operator
	 */
	private static final String LINK_AND = "and";
	/**
	 * The correct way to use the LIKE operator in an expressions
	 */
	private static final String OP_LIKE = "LIKE";
	/**
	 * String literals must be enclosed in this character
	 */
	private static final char DOUBLE_QUOTE = '"';
	
	/**
	 * This is a constant defining the initial StringBuilder capacity
	 */
	private static final int SB_CAPACITY = 20;
	
	/**
	 * An array of characters of which the original query is made up
	 */
	private char[] data;
	/**
	 * The index of the first unprocessed character in the array
	 */
	private int current;
	/**
	 * The last token this QueryLexer returned
	 */
	private QueryToken token;
	
	/**
	 * A constructor for a QueryLexer taking the original textual query.
	 * @param text The original query conditions
	 * @throws NullPointerException if the given query is null
	 */
	public QueryLexer(String text) {
		Objects.requireNonNull(text);
		text = text.trim();
		data = text.toCharArray();
		current = 0;
	}
	
	/**
	 * @return The last generated token by this QueryLexer
	 */
	public QueryToken getToken() {
		return token;
	}
	
	/**
	 * Extracts the next token from the original query.
	 * 
	 * @return The next token generated from the query.
	 * @throws QueryException if this method is called after EOQ has been reached
	 */
	public QueryToken nextToken() {
		if(token!=null && token.getType()==QueryTokenType.EOQ) {
			throw new QueryException("Illegal to ask for next token after EOQ has been reached!");
		}
		skipBlanks();
		if(current>=data.length) {
			token = new QueryToken(QueryTokenType.EOQ, "#");
			return token;
		}
		
		StringBuilder sb = new StringBuilder(SB_CAPACITY);
		
		if(Character.isLetter(data[current])) {
			token = getField(sb);
			return token;
		}
		if(data[current]==DOUBLE_QUOTE) {;
			token = new QueryToken(QueryTokenType.STRING, getString(sb));
			return token;
		}
		if(isOperator(data[current])) {
			token = new QueryToken(QueryTokenType.OPERATOR, getOperator(sb));
			return token;
		}
		throw new QueryException("Unknown character encountered while tokenizing: "+data[current]);
	}

	/**
	 * This method extracts a sequence of characters that are letters
	 * and checks if the sequence isn't a LIKE operator or a logical
	 * AND (generating their respective tokens if it is).
	 * 
	 * Note: The method does not accept field names with underscores or
	 * 		digits, which may be necessary.
	 * 		It also generates two tokens types outside of its description.
	 * 
	 * @param sb A StringBuilder to extract the characters into
	 * @return A token of the FIELD, OPERATOR or LINK type
	 */
	private QueryToken getField(StringBuilder sb) {
		while(current<data.length && Character.isLetter(data[current])) {
			sb.append(data[current++]);
		}
		String val = sb.toString();
		if(val.equals(OP_LIKE) && token!=null && token.getType()==QueryTokenType.FIELD) {
			token = new QueryToken(QueryTokenType.OPERATOR, val);
			return token;
		}
		if(val.equalsIgnoreCase(LINK_AND) && token!=null && token.getType()==QueryTokenType.STRING) {
			token = new QueryToken(QueryTokenType.LINK, val);
			return token;
		}
		token = new QueryToken(QueryTokenType.FIELD, val);
		return token;
	}

	/**
	 * This method generates a sequence of all characters that can
	 * appear in operators (excluding the LIKE operator).
	 * It does not check if the operator itself is semantically
	 * correct - "!!<><==>" will be accepted by the lexer.
	 * 
	 * @param sb A StringBuilder to extract the characters into
	 * @return A string representation of the operator
	 */
	private String getOperator(StringBuilder sb) {
		while(current<data.length && isOperator(data[current])) {
			sb.append(data[current++]);
		}
		return sb.toString();
	}
	/**
	 * A shorthand method for checking if a character is a valid
	 * character for an operator ('<', '>', '!' or '=').
	 * 
	 * @param c The character to check
	 * @return true if it is a valid character, false otherwise
	 */
	private boolean isOperator(char c) {
		return data[current]=='<' || data[current]=='>' ||
				data[current]=='=' || data[current]=='!';
	}
	
	/**
	 * This method returns the content of a string literal from
	 * a query.
	 * A string literal has to be surrounded with double
	 * quotation marks.
	 * 
	 * @param sb A StringBuilder to extract the characters into
	 * @return The content of the string literal
	 */
	private String getString(StringBuilder sb) {
		current++; //skip quotation mark
		while(current<data.length && data[current]!=DOUBLE_QUOTE) {
			sb.append(data[current++]);
		}
		if(current>=data.length) {
			throw new QueryException("Unexpected EOQ reached in string!");
		}
		current++;	//skip quotation mark
		return sb.toString();
	}
	
	/**
	 * This method skips whitespace characters in the array
	 * of characters.
	 */
	private void skipBlanks() {
		while(current<data.length && Character.isWhitespace(data[current])) {
			current++;
		}
	}
}
