package hr.fer.zemris.java.custom.collections;

import java.util.Arrays;
import java.util.ConcurrentModificationException;
import java.util.NoSuchElementException;
import java.util.Objects;

/**
 * An indexed collection of Objects based on an array.
 * 
 * This collection allows duplicate elements, 
 * but does not allow null values.
 * 
 * @author Miroslav Bićanić
 *
 */
public class ArrayIndexedCollection<T> implements List<T> {
	/**
	 * The default capacity of the underlying Object array.
	 */
	private static final int DEFAULT_CAPACITY = 16;
	
	/**
	 * The number of elements currently in the array.
	 */
	private int size;
	
	/**
	 * The underlying Object array.
	 * 
	 * This array is used to store the elements of the collection.
	 * Its capacity is increased exponentially, doubling in size
	 * every time it gets full.
	 */
	private T[] elements;
	
	/**
	 * The number of structural modifications done to the list:
	 * adding, getting, removing...
	 */
	private long modificationCount;
	
	/**
	 * The default constructor.
	 * 
	 * Creates a new empty collection with the initial capacity of the
	 * underlying array set to the {@link #DEFAULT_CAPACITY}.
	 * 
	 * @see #ArrayIndexedCollection(Collection, int)
	 */
	public ArrayIndexedCollection() {
		this(DEFAULT_CAPACITY);
	}
	
	/**
	 * A constructor that creates an ArrayIndexedCollection 
	 * with a specified initial capacity.
	 * 
	 * @param initialCapacity The initial capacity of the created collection's underlying array.
	 * @see #ArrayIndexedCollection(Collection, int)
	 */
	@SuppressWarnings("unchecked")
	public ArrayIndexedCollection(int initialCapacity) {
		this.size = 0;
		this.elements = (T[])new Object[initialCapacity];
		this.modificationCount = 0;
	}
	/**
	 * A constructor that takes another collection and creates
	 * an ArrayIndexedCollection copy of it.
	 * 
	 * The capacity it sends further is the {@link #DEFAULT_CAPACITY},
	 * but it is not guaranteed it will be the used initial capacity.
	 * If the given collection's size is larger than the default 
	 * capacity, it is used as the initial capacity.
	 * 
	 * @param other The collection to copy.
	 * @see #ArrayIndexedCollection(Collection, int)
	 */
	public ArrayIndexedCollection(Collection<? extends T> other) {
		this(other, DEFAULT_CAPACITY);
	}
	/**
	 * A constructor that takes another collection and an initial capacity.
	 * 
	 * This constructor creates an ArrayIndexedCollection copy out of 
	 * another collection, using the larger number between the specified 
	 * capacity and the other collection's size as its underlying array's 
	 * initial capacity.
	 * 
	 * @param other The collection which this constructor will copy.
	 * @param initialCapacity The desired initial capacity for the array.
	 * @throws NullPointerException if the given collection is null
	 * @throws IllegalArgumentException if the given initial capacity is smaller than 1
	 */
	@SuppressWarnings("unchecked")
	public ArrayIndexedCollection(Collection<? extends T> other, int initialCapacity) {
		Objects.requireNonNull(other);
		if(initialCapacity < 1) {
			throw new IllegalArgumentException("Expected: capacity bigger than 1. Got: '"+initialCapacity+"'.");
		}
		this.elements = (T[])new Object[initialCapacity>other.size() ? initialCapacity : other.size()];
		this.addAll(other);
		this.modificationCount = 0;
	}
	
	/**
	 * Returns the size of this ArrayIndexedCollection.
	 * 
	 * @return The number of non-null elements in the collection
	 */
	@Override
	public int size() {
		return this.size;
	}

	/**
	 * This method says whether a <code>value</code> is in this 
	 * ArrayIndexedCollection, where the equality is determined 
	 * by the equals method.
	 * 
	 * This operation is done in O(n) complexity (O(n/2) average).
	 * 
	 * @param value The value to look for in the collection.
	 */
	@Override
	public boolean contains(Object value) {
		return indexOf(value)>=0;
	}
	
	/**
	 * This method returns the index of the first occurrence of a
	 * <code>value</code> in this ArrayIndexedCollection as determined 
	 * by the equals method.
	 * 
	 * This operation is done in O(n) complexity (O(n/2) average).
	 * 
	 * @param value The value to look for
	 * @return -1 if <code>value</code> doesn't occur in the collection; its first occurrence's index otherwise
	 */
	public int indexOf(Object value) {
		if(value==null) {
			return -1;
		}
		for(int i = 0; i < this.size; i++) {
			if(this.elements[i].equals(value)) {
				return i;
			}
		}
		return -1;
	}
	
	/**
	 * This method adds a <code>value</code> into this ArrayIndexedCollection.
	 * 
	 * The operation is done in O(1) complexity, except when the underlying
	 * array has to be reallocated, in which case it is done in O(n) 
	 * complexity.
	 * 
	 * @throws NullPointerException if the given <code>value</code> is null
	 */
	@Override
	public void add(T value) {
		Objects.requireNonNull(value);
		if(size==this.elements.length) {
			this.reallocate();
		}
		this.elements[size] = value;
		size++;
		modificationCount++;
	}
	
	/**
	 * This method inserts the element into this ArrayIndexedCollection
	 * at a given position.
	 * 
	 * The index of all elements starting from the given index is increased
	 * by one after this operation - it does not overwrite.
	 * 
	 * This operation is done in O(n) complexity (O(n/2) average), unless
	 * the underlying array has to be reallocated, in which case it is 
	 * done in O(n + n) complexity (O(n + n/2) average).
	 * 
	 * @param value The element to be inserted
	 * @param index The position at which the value will be inserted
	 * @throws NullPointerException if the value is null
	 * @throws IndexOutOfBoundsException if the given index is out of range [0,{@link #size}]
	 */
	public void insert(T value, int index) {
		Objects.requireNonNull(value);
		checkIndexInRange(index, 0, this.size);
		
		if(this.size == this.elements.length) {
			this.reallocate();
		}
		
		if(index == this.size) {
			this.elements[index] = value;
			this.size++;
			return;
		}
		for(int i = this.size; i > index; i--) {
			this.elements[i] = this.elements[i-1];
		}
		this.elements[index] = value;
		this.modificationCount++;
		this.size++; 
	}

	/**
	 * This method returns the element of this ArrayIndexedCollection 
	 * from a given position.
	 * 
	 * This operation is done in O(1) complexity.
	 * 
	 * @param index The index of the element to retrieve
	 * @return The element at the given index
	 * @throws IndexOutOfBoundsException if the given index is out of range [0,{@link #size}-1]
	 */
	public T get(int index) {
		checkIndexInRange(index, 0, this.size-1);
		return elements[index];
	}

	/**
	 * Removes one (first) occurrence of <code>value</code> from this 
	 * ArrayIndexedCollection, where the equality is determined by the 
	 * equals method.
	 * 
	 * This operation is done in O(n) complexity (O(n) average).
	 * 
	 * @param value The value to remove from the collection.
	 */
	@Override
	public boolean remove(Object value) {
		if(value==null) {
			return false;
		}
		int index = this.indexOf(value);
		if(index == -1) {
			return false;
		}
		this.remove(index);
		return true;
	}

	/**
	 * Removes the element from a given index from this ArrayIndexedCollection.
	 * 
	 * This operation is done in O(n) complexity (O(n/2) average).
	 * 
	 * @param index The index of the element to remove from the collection
	 * @throws IndexOutOfBoundsException if the given index is out of range [0,{@link #size}-1]
	 */
	public void remove(int index) {
		checkIndexInRange(index, 0, this.size-1);
		int i;
		this.elements[index] = null;
		for(i = index; i < this.size-1; i++) {
			this.elements[i] = this.elements[i+1];
		}
		this.size--;
		this.modificationCount++;
	}
	
	/**
	 * Deletes all elements from this ArrayIndexedCollection.
	 * 
	 * The capacity of the underlying array remains unchanged.
	 * This operation is done in O(n) complexity (O(n) average).
	 */
	@Override
	public void clear() {
		for(int i = 0; i < this.size; i++) {
			this.elements[i]=null;
		}
		this.size=0;
		this.modificationCount++;
	}
	
	/**
	 * Gives the content of this ArrayIndexedCollection as an array of the
	 * same size.
	 * 
	 * The resulting array has the same elements on the same positions
	 * as the original ArrayIndexedCollection.
	 * 
	 * @return An array with all the elements of this ArrayIndexedCollection
	 */
	@Override
	public Object[] toArray() {
		return Arrays.copyOf(this.elements, this.size);
	}
	
	/**
	 * This method allocates a new array with twice the capacity as
	 * the this.elements array, keeping all elements on their positions.
	 */
	private void reallocate() {
		this.elements = Arrays.copyOf(this.elements, this.elements.length*2);
	}

	/**
	 * Checks if a given integer is within the <b>closed</b> given range.
	 * 
	 * @param index The index to be checked
	 * @param lowerLimit The lower range limit (inclusive)
	 * @param upperLimit The upper range limit (inclusive)
	 */
	private void checkIndexInRange(int index, int lowerLimit, int upperLimit) {
		if(index<lowerLimit) {
			throw new IndexOutOfBoundsException("Expected positive index. Got: '"+index+"'.");
		}
		if(index>upperLimit) {
			throw new IndexOutOfBoundsException("Index '"+index+"' invalid for collection of size: "+upperLimit);
		}
	}
	
	/**
	 * This method returns a {@link #ElementsGetter} for
	 * iteration over this collection.
	 */
	@Override
	public ElementsGetter<T> createElementsGetter() {
		return new ArrayIterator();
	}

	/**
	 * An implementation of an ElementsGetter designed
	 * for ArrayIndexedCollections.
	 * 
	 * This ElementsGetter keeps track of the modification
	 * count, so no changes to the collection are allowed
	 * while an active ElementsGetter exists.
	 * 
	 * @author Miroslav Bićanić
	 *
	 */
	private class ArrayIterator implements ElementsGetter<T> { 

		/**
		 * The index of the first element that was not yet
		 * given to user.
		 */
		private int currentIndex;
		
		/**
		 * The number of modifications this collection had
		 * at the moment of creating this ArrayIterator.
		 */
		private long savedModificationCount;
		
		/**
		 * Constructor for an ArrayIterator that takes an
		 * ArrayIndexedCollection col and iterates over it on demand.
		 * @param col The collection to iterate over
		 */
		private ArrayIterator() {
			this.currentIndex = 0;
			this.savedModificationCount = modificationCount;
		}
		
		/**
		 * {@inheritDoc}
		 * 
		 * Operation is successful only if the collection hasn't been
		 * modified since the creation of ArrayIterator.
		 * 
		 * @throws ConcurrentModificationException
		 */
		@Override
		public boolean hasNextElement() {
			if(savedModificationCount!=modificationCount) {
				throw new ConcurrentModificationException();
			}
			return currentIndex < size;
		}
		/**
		 * {@inheritDoc}
		 * 
		 * Returning an element is successful only if the collection
		 * hasn't been modified since the creation of ArrayIterator.
		 * 
		 * @throws ConcurrentModificationException
		 */
		@Override
		public T getNextElement() {
			if(savedModificationCount!=modificationCount) {
				throw new ConcurrentModificationException();
			}
			if(!hasNextElement()) {
				throw new NoSuchElementException();
			}
			currentIndex++;
			return get(currentIndex-1);
		}
	}

	/**
	 * Two Lists are considered equal when they are of the
	 * same size, and store equal elements on identical
	 * positions.
	 * If both Lists have size 0, they are equal.
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!(obj instanceof List))
			return false;
		List<?> other = (List<?>) obj;
		if(this.size!=other.size()) {
			return false;
		}
		if(this.size==0) {
			return true;
		}
		boolean bool = true;
		for(int i = 0, n = this.size(); i < n; i++) {
			if (!get(i).equals(other.get(i))) {
				bool = false;
				break;
			}
		}
		return bool;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Arrays.deepHashCode(elements);
		result = prime * result + Objects.hash(size);
		return result;
	}
	
	
}
