package hr.fer.zemris.java.hw05.db;

/**
 * Represents an operation that accepts two string values, 
 * and compares them, returning true if 
 * {@code value1 [operation] value2 } is true.
 * 
 * This is a functional interface whose functional method is
 * {@link #satisfied(String, String)}.
 *
 * @author Miroslav Bićanić
 */
@FunctionalInterface
public interface IComparisonOperator {
	/**
	 * Compares two string values using this operator.
	 * @param value1 the first string value
	 * @param value2 the second string value
	 */
	boolean satisfied(String value1, String value2);
}
