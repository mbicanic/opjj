package hr.fer.zemris.java.hw05.db;

/**
 * Represents an object that takes a {@link StudentRecord} and
 * returns the value of a certain attribute of that StudentRecord.
 * 
 * This is a functional interface whose functional method is
 * {@link #get(StudentRecord)}
 * 
 * @author Miroslav Bićanić
 */
@FunctionalInterface
public interface IFieldValueGetter {
	/**
	 * Returns the value of the attribute that this getter gets.
	 * 
	 * @param record A StudentRecord from which to get the value
	 * @return The value of the attribute of the given StudentRecord
	 */
	String get(StudentRecord record);
}
