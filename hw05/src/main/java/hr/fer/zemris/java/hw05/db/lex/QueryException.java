package hr.fer.zemris.java.hw05.db.lex;

/**
 * This exception is thrown whenever an error of any kind is
 * encountered during the work of {@link StudentDB}.
 * 
 * @author Miroslav Bićanić
 */
public class QueryException extends RuntimeException {
	private static final long serialVersionUID = 1L;
	
	/**
	 * A constructor for a QueryException taking a message
	 * describing the error behind it.
	 * 
	 * @param message Description of the error that caused the exception
	 */
	public QueryException(String message) {
		super(message);
	}

}
