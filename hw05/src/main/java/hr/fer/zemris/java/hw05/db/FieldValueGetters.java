package hr.fer.zemris.java.hw05.db;

import java.util.Objects;

/**
 * This class provides an implementation of the  {@link IFieldValueGetter} 
 * functional interface for each attribute supported by this database.
 * 
 * All implementations throw a NullPointerException if a null
 * StudentRecord is given.
 * 
 * @author Miroslav Bićanić
 */
public class FieldValueGetters {
	/**
	 * A getter that returns the value of the first name attribute of the
	 * given student record.
	 */
	public static final IFieldValueGetter FIRST_NAME = (sr) -> {
		return Objects.requireNonNull(sr).getFirstName();
		};
	/**
	 * A getter that returns the value of the last name attribute of the
	 * given student record.
	 */
	public static final IFieldValueGetter LAST_NAME = (sr) -> {
		return Objects.requireNonNull(sr).getLastName();
		};
	/**
	 * A getter that returns the value of the jmbag attribute of the
	 * given student record.
	 */
	public static final IFieldValueGetter JMBAG = (sr) -> {
		return Objects.requireNonNull(sr).getJmbag();
		};
}
