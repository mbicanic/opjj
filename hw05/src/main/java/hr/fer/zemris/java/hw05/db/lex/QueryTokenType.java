package hr.fer.zemris.java.hw05.db.lex;

import hr.fer.zemris.java.hw05.db.ComparisonOperators;

/**
 * QueryTokenType is an enumeration of all types of tokens
 * the QueryLexer can generate.
 * 
 * @author Miroslav Bićanić
 */
public enum QueryTokenType {
	/**
	 * This token is generated whenever a field name is encountered.
	 * A field name is a string of characters, not surrounded by
	 * quotation marks (firstName, lastName...).
	 * It is used as the first argument in expressions.
	 */
	FIELD,
	/**
	 * This token is generated when the QueryLexer encounters an
	 * operator. Supported operators are: <, <=, >, >=, =, !=
	 * with their usual meanings, and a LIKE operator.
	 * @see ComparisonOperators
	 */
	OPERATOR,
	/**
	 * A string token represents the second argument of an expression.
	 * It is generated for all sequences of characters surrounded with
	 * quotation marks.
	 */
	STRING,
	/**
	 * This token represents a link between two unit expressions.
	 * Only supported value that triggers a generation of a link
	 * token is "and", linking two expressions together with a
	 * logical "and" operator.
	 */
	LINK,
	/**
	 * This is a special token type, generated when an end of query 
	 * is reached.
	 */
	EOQ
}
