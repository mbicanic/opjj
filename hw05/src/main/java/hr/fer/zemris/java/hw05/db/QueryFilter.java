package hr.fer.zemris.java.hw05.db;

import java.util.List;
import java.util.Objects;

/**
 * This is an implementation of an {@link IFilter}, taking a list
 * of elementary conditional expressions on construction and 
 * running the received StudentRecord through all the conditions.
 * <br><br>
 * The only way two conditional expressions can be separated is by
 * a logical "AND", meaning all ConditionalExpressions must return
 * true for the record to be evaluated as true and accepted.
 * 
 * @author Miroslav Bićanić
 */
public class QueryFilter implements IFilter {
	/**
	 * A list of ConditionalExpressions every object has to satisfy in order
	 * to satisfy this filter.
	 */
	private List<ConditionalExpression> conditions;
	
	/**
	 * A constructor for a QueryFilter.
	 * @param conditions The list of conditions
	 * @throws NullPointerException if the given list is null
	 */
	public QueryFilter(List<ConditionalExpression> conditions) {
		this.conditions = Objects.requireNonNull(conditions);
	}
	/**
	 * {@inheritDoc}
	 * @return true if the record satisfied all ConditionalExpressions
	 */
	@Override
	public boolean accepts(StudentRecord record) {
		for(ConditionalExpression ex : conditions) {
			
			String value = ex.getStringLiteral();
			IComparisonOperator op = ex.getOperator();
			IFieldValueGetter fvg = ex.getFieldGetter();
			
			if(!op.satisfied(fvg.get(record), value)) {
				return false;
			}
		}
		return true;
	}
	
}
