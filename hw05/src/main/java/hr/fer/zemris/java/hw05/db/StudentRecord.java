package hr.fer.zemris.java.hw05.db;

import java.util.Objects;

/**
 * This class models one StudentRecord in the {@link StudentDatabase}.
 * 
 * A StudentRecord has four different attributes:
 * 		firstName	-	the first name of the student (multiple first names supported)
 * 		lastName	-	the last name of the student (multiple last names supported)
 * 		jmbag		-	the ID number of the student, it is also the key attribute,
 * 						meaning two things:
 * 							1) Two students are considered equal if their jmbag 
 * 								attributes are equal.
 * 							2) A student with its jmbag must be unique and there can
 * 								be no two students with a same jmbag attribute.
 * 		grade		-	the grade of the student
 * 
 * @author Miroslav Bićanić
 */
public class StudentRecord {
	/**
	 * The first name(s) of the student.
	 */
	private String firstName;
	/**
	 * The last name(s) of the student.
	 */
	private String lastName;
	/**
	 * The jmbag of the student.
	 */
	private String jmbag;
	/**
	 * The grade of the student.
	 */
	private int grade;
	
	/**
	 * A constructor for a StudentRecord.
	 * 
	 * @param firstName The first name(s) of the student
	 * @param lastName The last name(s) of the student
	 * @param jmbag	The jmbag (ID) of this student
	 * @param grade	The grade of this student
	 */
	public StudentRecord(String firstName, String lastName, String jmbag, int grade) {
		this.firstName = Objects.requireNonNull(firstName);
		this.lastName = Objects.requireNonNull(lastName);
		this.jmbag = Objects.requireNonNull(jmbag);
		this.grade = grade;
	}
	/**
	 * @return This StudentRecord's first name
	 */
	public String getFirstName() {
		return firstName;
	}
	/**
	 * @return This StudentRecord's last name
	 */
	public String getLastName() {
		return lastName;
	}
	/**
	 * @return This StudentRecord's jmbag
	 */
	public String getJmbag() {
		return jmbag;
	}
	/**
	 * @return This StudentRecord's grade
	 */
	public int getGrade() {
		return grade;
	}
	
	/**
	 * Two StudentRecords are considered equal when their
	 * jmbag attributes are equal.
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof StudentRecord))
			return false;
		StudentRecord other = (StudentRecord) obj;
		return Objects.equals(jmbag, other.jmbag);
	}
	
	@Override
	public int hashCode() {
		return Objects.hash(jmbag);
	}
}
