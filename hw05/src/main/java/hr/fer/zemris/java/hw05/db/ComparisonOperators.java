package hr.fer.zemris.java.hw05.db;

import java.util.Objects;

/**
 * This class provides several often used implementations of the 
 * {@link IComparisonOperator} functional interface.
 * 
 * For all operations, strings are compared lexicographically.
 * Thus, "X" is considered smaller than "XA" and "Z", but greater
 * than "AX" and "A". Naturally, it is equal only to "X" and not
 * equal to everything else.
 * 
 * For the LIKE operation, the parts of the given pattern
 * around the wildcard character are compared lexicographically,
 * while the wildcard takes the place of as many arbitrary characters.
 * 
 * All implementations of an IComparisonOperator throw a null pointer
 * exception if any of the given values is null.
 * 
 * @author Miroslav Bićanić
 */
public class ComparisonOperators {
	/**
	 * An operator that takes two strings and tests if the first
	 * one is lexicographically smaller than the second string,
	 * returning true if yes, and false otherwise.
	 */
	public static final IComparisonOperator LESS = (v1,v2) -> {
		Objects.requireNonNull(v1,v2);
		return v1.compareTo(v2) < 0;
	};
	/**
	 * An operator that takes two strings and tests if the first
	 * one is lexicographically smaller than or equal to the second string,
	 * returning true if yes, and false otherwise.
	 */
	public static final IComparisonOperator LESS_OR_EQUALS = (v1,v2) -> {
		Objects.requireNonNull(v1,v2);
		return v1.compareTo(v2) <= 0;
	};
	/**
	 * An operator that takes two strings and tests if the first
	 * one is lexicographically greater than the second string,
	 * returning true if yes, and false otherwise.
	 */
	public static final IComparisonOperator GREATER = (v1,v2) -> {
		Objects.requireNonNull(v1,v2);
		return v1.compareTo(v2) > 0;
	};
	/**
	 * An operator that takes two strings and tests if the first
	 * one is lexicographically smaller than or equal to the second string,
	 * returning true if yes, and false otherwise.
	 */
	public static final IComparisonOperator GREATER_OR_EQUALS = (v1,v2) -> {
		Objects.requireNonNull(v1,v2);
		return v1.compareTo(v2) >= 0;
	};
	/**
	 * An operator that takes two strings and tests if the first
	 * one is equal to the second string.
	 */
	public static final IComparisonOperator EQUALS = (v1,v2) -> {
		Objects.requireNonNull(v1,v2);
		return v1.compareTo(v2) == 0;
	};
	/**
	 * An operator that takes two strings and tests if the first
	 * one is not equal to the second string.
	 */
	public static final IComparisonOperator NOT_EQUALS = (v1,v2) -> {
		Objects.requireNonNull(v1,v2);
		return v1.compareTo(v2) != 0;
	};
	/**
	 * This operator compares the first string with a pattern given in 
	 * the second string, returning true if they match.
	 * 
	 * The pattern string can contain at most one wildcard character
	 * ('*'), which can take place for any amount of characters from
	 * the original string, including zero characters.
	 * 
	 * Wildcard character can be located anywhere in the pattern string.
	 * If no wildcard character is present, the effect is the same as
	 * if the {@link #EQUALS} operator was called.
	 * 
	 * All characters around the wildcard must exactly match with the
	 * original string, when we would remove its substring represented
	 * by a wildcard character.
	 * 
	 * Examples: 
	 * 		"Technology" LIKE "Te*ogy" 	 --> true, '*' stands for 'chnol'
	 * 		"AAA" LIKE "AA*AA" 			 --> false
	 * 		"Bićanić" LIKE "*ić" 		 --> true, '*' stands for 'Bićan'
	 * 		"Comparison" LIKE "C*" 		 --> true, '*' stands for 'omparison'
	 * 		"Equal" LIKE "Equal" 		 --> true
	 * 		"True" LIKE "Tr*ue"			 --> true, '*' stands for an empty string
	 * 		"Everything" LIKE "Everywh*" --> false
	 */
	public static final IComparisonOperator LIKE = (v1,v2) -> {
		Objects.requireNonNull(v1,v2);
		int indexOfWild = v2.indexOf('*');
		if(indexOfWild==-1) {
			return EQUALS.satisfied(v1, v2);
		}
		if(indexOfWild == 0) {
			String end = v2.substring(1);
			return v1.endsWith(end) && v1.length() >= v2.length()-1;
		}
		if(indexOfWild == v2.length()-1) {
			String start = v2.substring(0, v2.length()-1);
			return v1.startsWith(start) && v1.length()>=v2.length()-1;
		}
		String start = v2.substring(0,indexOfWild);
		String end = v2.substring(indexOfWild+1);
		
		if(!v1.startsWith(start) || !v1.endsWith(end)) {
			return false;
		}
		if(v1.length() >= v2.length()-1) {
			return true;
		}
		return false;
	};
	
}
