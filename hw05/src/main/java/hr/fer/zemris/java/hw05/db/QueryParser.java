 package hr.fer.zemris.java.hw05.db;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import hr.fer.zemris.java.hw05.db.lex.QueryException;
import hr.fer.zemris.java.hw05.db.lex.QueryLexer;
import hr.fer.zemris.java.hw05.db.lex.QueryToken;
import hr.fer.zemris.java.hw05.db.lex.QueryTokenType;

/**
 * QueryParser parses the query given as a string into a list
 * of {@link ConditionalExpression} objects.
 * 
 * The given query must consist of one or more expressions,
 * separated by an "AND" string, representing the logical AND:
 * [EXPRESSION] AND [EXPRESSION] AND [EXPRESSION]..
 * 
 * Each expression must consist of a legal field name, marking
 * an attribute of a {@link StudentRecord}, one of the supported
 * operators and a string literal with which to compare the 
 * attribute. The structure of an expression is as follows:
 * "[FIELD] [OPERATOR] [LITERAL]"
 * 
 * Field names, operators and literals are case sensitive 
 * (LIKE has to be capitalized), while the logical AND can
 * be written with any case.
 * 
 * If the query isn't written according to these rules, the parser
 * throws a QueryException.
 * 
 * The parser can tell from the list of conditions if filtration
 * of records can be executed as a direct query, or it has to be
 * done through a {@link QueryFilter}.
 * 
 * @see StudentDB
 * @see QueryLexer
 * @author Miroslav Bićanić
 *
 */
public class QueryParser {
	/**
	 * The correct way to address the "first name" attribute in a query.
	 */
	private static final String ATTR_FIRST = "firstName";
	/**
	 * The correct way to address the "last name" attribute in a query.
	 */
	private static final String ATTR_LAST = "lastName";
	/**
	 * The correct way to address the "JMBAG" attribute in a query.
	 */
	private static final String ATTR_JMBAG = "jmbag";
	/**
	 * The correct way to type "LIKE" operator in a query.
	 */
	private static final String OP_LIKE = "LIKE";
	/**
	 * The character representing a wildcard when using the LIKE operator
	 */
	private static final char WILDCARD = '*';
	
	/**
	 * A QueryLexer used by the parser to retrieve tokens from the 
	 * textual query.
	 */
	private QueryLexer lexer;
	/**
	 * A list of ConditionalExpression objects containing all parsed
	 * expressions from the query.
	 */
	private List<ConditionalExpression> conditions; 
	
	/**
	 * A constructor for a QueryParser.
	 * @param query The textual query to parse
	 * @throws NullPointerException if the given query is null
	 */
	public QueryParser(String query) {
		Objects.requireNonNull(query);
		this.lexer = new QueryLexer(query);
		this.conditions = new ArrayList<>();
		parse();
	}

	/**
	 * Tells if the parsed query was a direct query.
	 * @return true if the query was direct; false otherwise
	 */
	public boolean isDirectQuery() {
		if(conditions.size()!=1) {
			return false;
		}
		ConditionalExpression ce = conditions.get(0);
		return(ce.getFieldGetter().equals(FieldValueGetters.JMBAG) && ce.getOperator().equals(ComparisonOperators.EQUALS));
	}
	/**
	 * Gives the literal JMBAG value of the direct query, if the
	 * parsed query was a direct query. Throws an exception otherwise.
	 * 
	 * @return The JMBAG literal value of the parsed query
	 * @throws IllegalStateException if the parsed query wasn't a direct query
	 */
	public String getQueriedJMBAG() {
		if(!isDirectQuery()) {
			throw new IllegalStateException();
		}
		return conditions.get(0).getStringLiteral();
	}

	/**
	 * Returns a list of ConditionalExpression objects created
	 * by parsing the textual query.
	 * 
	 * @return A list of ConditionalExpressions
	 */
	public List<ConditionalExpression> getQuery() {
		return conditions;
	}
	
	
	/**
	 * Main method for parsing the query, parsing one 
	 * expression in one iteration.
	 * 
	 * @throws QueryException
	 * 			-if the query is empty
	 * 			-if the order of arguments (FIELD-OPERATOR-LITERAL) is invalid
	 * 			-if end of query is reached after a logical AND
	 */
	private void parse() {
		QueryToken t = lexer.nextToken();
		if(t.getType()==QueryTokenType.EOQ) {
			throw new QueryException("Query cannot be empty!");
		}
		while(t!=null && t.getType()!=QueryTokenType.EOQ) {
			if(t.getType()!=QueryTokenType.FIELD) {	
				throw new QueryException("First argument in a query expression must be a field name. Got: "+t.getType());
			}
			IFieldValueGetter getter = fieldGetterSetUp(t);
			
			t = lexer.nextToken();
			if(t.getType()!=QueryTokenType.OPERATOR) {
				checkEOQ(t, "field name.");
				throw new QueryException("After a field name, an operator must follow. Got: "+t.getType());
			}
			IComparisonOperator operator = operatorSetUp(t);
			
			t = lexer.nextToken();
			if(t.getType()!=QueryTokenType.STRING) {
				checkEOQ(t, "operator.");
				throw new QueryException("The second argument in a query expression must be a string literal. Got: "+t.getType());
			}
			String literal =  literalSetUp(t, operator);
			
			conditions.add(new ConditionalExpression(getter, literal, operator));
			
			t = lexer.nextToken();
			if(t.getType()==QueryTokenType.LINK) {
				t = lexer.nextToken();
				if(t.getType()==QueryTokenType.EOQ) {
					throw new QueryException("Unexpected end of query reached after logical AND.");
				}
			} else if(t.getType()!=QueryTokenType.EOQ) {
				throw new QueryException("Unexpected token after string literal: "+t.getValue()+". Expected end of query or logical AND.");
			}
		}
	}

	/**
	 * Helper method that throws an exception carrying an appropriate message
	 * when an EOQ is reached when a concrete value was expected.
	 * @param t
	 * @param lastParsed
	 */
	private void checkEOQ(QueryToken t, String lastParsed) {
		if(t.getType()==QueryTokenType.EOQ) {
			throw new QueryException("Unexpected end of query reached after "+lastParsed);
		}
	}

	/**
	 * This method creates an IFieldValueGetter for a single conditional
	 * expression, based on the value in the received token.
	 * 
	 * @param t The token representing a field name
	 * @return An IFieldValueGetter that retrieves the value of the field given in the token
	 * @throws QueryException if the value contained in the token is invalid
	 */
	private IFieldValueGetter fieldGetterSetUp(QueryToken t) {
		switch(t.getValue()) {
		case(ATTR_FIRST):
			return FieldValueGetters.FIRST_NAME;
		case(ATTR_LAST):
			return FieldValueGetters.LAST_NAME;
		case(ATTR_JMBAG):
			return FieldValueGetters.JMBAG;
		default:
			throw new QueryException("Illegal field name in query: '"+t.getValue()+"'");
		}
	}

	/**
	 * This method creates an IComparisonOperator for a single conditional
	 * expression, based on the value in the received token.
	 * @param t The token representing an operator
	 * @return An IComparisonOperator that can perform the specified operation on two objects
	 * @throws QueryException if an unknown/illegal operator is used
	 */
	private IComparisonOperator operatorSetUp(QueryToken t) {
		switch(t.getValue()) {
		case("="):
			return ComparisonOperators.EQUALS;
		case("!="):
			return ComparisonOperators.NOT_EQUALS;
		case(">"):
			return ComparisonOperators.GREATER;
		case(">="):
			return ComparisonOperators.GREATER_OR_EQUALS;
		case("<"):
			return ComparisonOperators.LESS;
		case("<="):
			return ComparisonOperators.LESS_OR_EQUALS;
		case(OP_LIKE):
			return ComparisonOperators.LIKE;
		default:
			throw new QueryException("Illegal operator used in query!");
		}
	}
	
	/**
	 * This method sets up the literal String value of an expression, 
	 * checking it's validity if the used operator is a LIKE operator.
	 * (Only one wildcard permitted in such a string).
	 * 
	 * @param t The token representing a string literal
	 * @param operator The operator of the expression currently parsed
	 * @return The String literal value, if it is valid
	 * @throws QueryException if the used operator is a LIKE operator, and
	 * 			more than one wildcard is present in the string literal.
	 */
	private String literalSetUp(QueryToken t, IComparisonOperator operator) {
		String literal = t.getValue();
		if(operator.equals(ComparisonOperators.LIKE)) {
			if(literal.indexOf(WILDCARD)!=literal.lastIndexOf(WILDCARD)) {
				throw new QueryException("Only one occurrence of a wildcard can be present in a string literal!");
			}
		}
		return literal;
	}
}	
