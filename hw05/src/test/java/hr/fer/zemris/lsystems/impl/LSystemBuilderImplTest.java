package hr.fer.zemris.lsystems.impl;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import hr.fer.zemris.lsystems.LSystem;
import hr.fer.zemris.lsystems.LSystemBuilder;

class LSystemBuilderImplTest {

	static LSystemBuilder lsb;
	
	@BeforeAll
	static void setUp() {
		lsb = new LSystemBuilderImpl();
		lsb.registerCommand('F', "draw 1")
		.registerCommand('+', "rotate 60")
		.registerCommand('-', "rotate -60")
		.setOrigin(0.05, 0.4)
		.setAngle(0)
		.setUnitLength(0.9)
		.setUnitLengthDegreeScaler(1.0/3.0)
		.registerProduction('F', "F+F--F+F")
		.setAxiom("F")
		.build();
	}
	@Test
	void testGeneration() {
		LSystem ls = lsb.build();
		assertEquals("F", ls.generate(0));
		assertEquals("F+F--F+F", ls.generate(1));
		assertEquals("F+F--F+F+F+F--F+F--F+F--F+F+F+F--F+F", ls.generate(2));
	}
	@Test
	void testDifferentGeneration() {
		lsb.registerProduction('G', "G---G");
		lsb.setAxiom("F+G");
		LSystem ls = lsb.build();
		assertEquals("F+G", ls.generate(0));
		assertEquals("F+F--F+F+G---G", ls.generate(1));
		assertEquals("F+F--F+F+F+F--F+F--F+F--F+F+F+F--F+F+G---G---G---G", ls.generate(2));
	}

}
