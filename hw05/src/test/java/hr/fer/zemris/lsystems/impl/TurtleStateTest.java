package hr.fer.zemris.lsystems.impl;

import static org.junit.jupiter.api.Assertions.*;

import java.awt.Color;

import org.junit.jupiter.api.Test;

import hr.fer.zemris.math.Vector2D;

class TurtleStateTest {

	private TurtleState getOneState() {
		return new TurtleState(
				new Vector2D(10,10),
				new Vector2D(1./2, Math.sqrt(3)/2),
				Color.RED,
				0.1);
	}
	@Test
	void testConstructorThrows() {
		assertThrows(NullPointerException.class, ()->new TurtleState(null, null, null, 0));
	}
	@Test
	void testAlteringCopyOriginalUnaffected() {
		TurtleState tst = getOneState();
		Vector2D originalDirection = tst.getDirection();
		Vector2D originalPosition = tst.getPosition();
		Color originalColor = tst.getColor();
		
		TurtleState copy = tst.copy();
		
		copy.setPosition(new Vector2D(2,8));
		copy.setDirection(new Vector2D(1,1));
		copy.setColor(Color.GREEN);
		
		assertNotEquals(copy.getPosition(), tst.getPosition());
		assertNotEquals(copy.getDirection(), tst.getDirection());
		assertNotEquals(copy.getColor(), tst.getColor());
		
		assertEquals(originalDirection, tst.getDirection());
		assertEquals(originalPosition, tst.getPosition());
		assertEquals(originalColor, tst.getColor());
		
	}

}
