package hr.fer.zemris.java.hw05.db;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class StudentRecordTest {

	@Test
	void testConstrutorThrows() {
		assertThrows(NullPointerException.class, ()->new StudentRecord(null, "", "", 1));
		assertThrows(NullPointerException.class, ()->new StudentRecord("",null,"",1));
		assertThrows(NullPointerException.class, ()->new StudentRecord("","",null,1));
	}
	@Test
	void testEquality() {
		StudentRecord s1 = new StudentRecord("X", "Y", "000", 1);
		StudentRecord s2 = new StudentRecord("A", "B", "000", 5);
		StudentRecord s3 = new StudentRecord("X", "Y", "001", 1);
		assertEquals(s1,s2);
		assertNotEquals(s1,s3);
	}

}
