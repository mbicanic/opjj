package hr.fer.zemris.java.hw05.db;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;

class QueryFilterTest {
	private static List<ConditionalExpression> getConditions() {
		List<ConditionalExpression> result = new ArrayList<>();
		ConditionalExpression c1 = new ConditionalExpression(FieldValueGetters.LAST_NAME, "*ić", ComparisonOperators.LIKE);
		ConditionalExpression c2 = new ConditionalExpression(FieldValueGetters.JMBAG, "0005", ComparisonOperators.GREATER_OR_EQUALS);
		ConditionalExpression c3 = new ConditionalExpression(FieldValueGetters.FIRST_NAME, "Marko", ComparisonOperators.NOT_EQUALS);
		result.add(c1);
		result.add(c2);
		result.add(c3);
		return result;
	}
	private static StudentRecord[] getRecords() {
		StudentRecord[] records = new StudentRecord[] {
				new StudentRecord("Filip", "Pranić", "0005", 1),
				new StudentRecord("Josip", "Tupić", "0006", 1),
				new StudentRecord("Zoran", "Linić", "0007", 1),
				
				new StudentRecord("Igor", "Frljić", "0004", 1),
				new StudentRecord("Marko", "Pranjić", "0008", 1),
				new StudentRecord("Perun", "Bezena", "0009", 1),
				
				new StudentRecord("Marko", "Novoselec", "0010", 1),
				new StudentRecord("Marko", "Zinić", "0003", 1),
				new StudentRecord("Juraj", "Tesla", "0002", 1),
				
				new StudentRecord("Marko", "Lopatica", "0001", 1),
		};
		
		return records;
	}
	@Test
	void testQueryFilterThrows() {
		assertThrows(NullPointerException.class, ()-> new QueryFilter(null));
	}
	@Test
	void testQueryFilter() {
		QueryFilter f = new QueryFilter(getConditions());
		boolean[] expected = new boolean[] {true, true, true, false, false, false, false, false, false, false};
		StudentRecord[] recs = getRecords();
		for(int i=0; i<recs.length; i++) {
			assertTrue(expected[i]==f.accepts(recs[i]));
		}
	}

}
