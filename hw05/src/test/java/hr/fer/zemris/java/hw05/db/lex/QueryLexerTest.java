package hr.fer.zemris.java.hw05.db.lex;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;
import hr.fer.zemris.java.hw05.db.lex.QueryTokenType;
class QueryLexerTest {

	@Test
	void testConstructorThrowsForNull() {
		assertThrows(NullPointerException.class, ()->new QueryLexer(null));
	}
	@Test
	void testEmptyString() {
		QueryLexer l = new QueryLexer("");
		assertEquals(QueryTokenType.EOQ, l.nextToken().getType());
		l = new QueryLexer("   ");
		assertEquals(QueryTokenType.EOQ, l.nextToken().getType());
	}
	@Test
	void testThrowsForNextTokenAfterEOQ() {
		QueryLexer l = new QueryLexer("");
		l.nextToken();
		assertThrows(QueryException.class, ()->l.nextToken());
	}
	@Test
	void testCorrectTokenization() {
		QueryLexer l = new QueryLexer("a\"liter*al$#\"field LIKE \"aa\"");
		assertEquals(l.nextToken().getType(), QueryTokenType.FIELD);
		assertEquals(l.getToken().getValue(), "a");
		assertEquals(l.nextToken().getType(), QueryTokenType.STRING);
		assertEquals(l.getToken().getValue(), "liter*al$#");
		assertEquals(l.nextToken().getType(), QueryTokenType.FIELD);
		assertEquals(l.getToken().getValue(), "field");
		assertEquals(l.nextToken().getType(), QueryTokenType.OPERATOR);
		assertEquals(l.getToken().getValue(), "LIKE");
		assertEquals(l.nextToken().getType(), QueryTokenType.STRING);
		assertEquals(l.getToken().getValue(), "aa");
	}
	
	@Test
	void testAcceptsOperatorStrings() {
		QueryLexer l = new QueryLexer("field <<>>!==");
		assertEquals(l.nextToken().getType(), QueryTokenType.FIELD);
		assertEquals(l.getToken().getValue(), "field");
		assertEquals(l.nextToken().getType(), QueryTokenType.OPERATOR);
		assertEquals(l.getToken().getValue(), "<<>>!==");
	}
	@Test
	void testCorrectLIKETokenization() {
		QueryLexer l = new QueryLexer("LIKE LIKE \"LIKE\"");
		assertEquals(l.nextToken().getType(), QueryTokenType.FIELD);
		assertEquals(l.nextToken().getType(), QueryTokenType.OPERATOR);
		assertEquals(l.nextToken().getType(), QueryTokenType.STRING);
	}
	@Test
	void testCorrectANDTokenization() {
		QueryLexer l = new QueryLexer("AND LIKE \"AND\" AND");
		assertEquals(l.nextToken().getType(), QueryTokenType.FIELD);
		assertEquals(l.nextToken().getType(), QueryTokenType.OPERATOR);
		assertEquals(l.nextToken().getType(), QueryTokenType.STRING);
		assertEquals(l.nextToken().getType(), QueryTokenType.LINK);
	}	
}
