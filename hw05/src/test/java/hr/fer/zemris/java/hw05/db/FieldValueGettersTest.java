package hr.fer.zemris.java.hw05.db;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class FieldValueGettersTest {

	private void iterate(StudentRecord[] list, String[] expected, IFieldValueGetter fname) {
		for(int i = 0; i < list.length; i++) {
			assertEquals(expected[i], fname.get(list[i]));
		}
	}
	private StudentRecord[] getArray() {
		return new StudentRecord[] {
				new StudentRecord("Mirko", "Dermin", "0000000090", 5),
				new StudentRecord("Pero", "Falekar", "0000000091", 5),
				new StudentRecord("Faruk", "Militan", "0000000092", 5),
				new StudentRecord("Jozo", "Salopek", "0000000093", 5)
			};
	}
	
	@Test
	void testFirstNameGetter() {
		IFieldValueGetter fname = FieldValueGetters.FIRST_NAME;
		StudentRecord[] list = getArray();
		String[] expected = new String[] {"Mirko", "Pero", "Faruk", "Jozo"};
		iterate(list, expected, fname);
		assertThrows(NullPointerException.class, ()->fname.get(null));
	}
	@Test
	void testLastNameGetter() {
		IFieldValueGetter flastname = FieldValueGetters.LAST_NAME;
		StudentRecord[] list = getArray();
		String[] expected = new String[] {"Dermin", "Falekar", "Militan", "Salopek"};
		iterate(list, expected, flastname);
		assertThrows(NullPointerException.class, ()->flastname.get(null));
	}
	@Test
	void testJMBAGGetter() {
		IFieldValueGetter fjmbag = FieldValueGetters.JMBAG;
		StudentRecord[] list = getArray();
		String[] expected = new String[] {"0000000090", "0000000091", "0000000092", "0000000093"};
		iterate(list, expected, fjmbag);
		assertThrows(NullPointerException.class, ()->fjmbag.get(null));
	}

}
