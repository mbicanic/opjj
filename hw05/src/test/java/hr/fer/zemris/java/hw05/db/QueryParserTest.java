package hr.fer.zemris.java.hw05.db;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import hr.fer.zemris.java.hw05.db.lex.QueryException;

/*
 * Not all incorrect scenarios are covered in these tests, the tests
 * for the QueryLexer ensure additional limitations and specific
 * errors.
 */
class QueryParserTest {
	/*
	 * This method is written to avoid writing the whole try-catch block
	 * within individual test methods. It ensures that an exception will
	 * be thrown, and by checking the message ensures that it is thrown 
	 * from the correct place in code, and that the message is appropriate.
	 */
	@SuppressWarnings("unused")
	private void assertExpectedException(String arg, String message) {
		try {
			QueryParser p = new QueryParser(arg);
		} catch (QueryException ex) {
			assertTrue(ex.getMessage().startsWith(message));
		}
	}
	
	@Test
	void testConstructorThrowsForNull() {
		assertThrows(NullPointerException.class, ()-> new QueryParser(null));
	}
	@Test
	void testThrowsForEmpty() {
		String message = "Query cannot be empty!";
		assertExpectedException("", message);
		assertExpectedException("      ", message);
	}
	@Test
	void testThrowsForFirstNotField() {
		String message = "First argument in a query";
		assertExpectedException("   \" string\"  ", message);
		assertExpectedException("!field", message);
		assertExpectedException(">\"a\"", message);
		assertExpectedException("  <><>!!==<< firstName", message);
	}

	@Test
	void testThrowsForIllegalFieldName() {
		String message = "Illegal field name in query";
		assertExpectedException("first name LIKE \"M*\"", message); //space between first and name, 'n' not capitalized
		assertExpectedException("JMBAG = \"0000000000\"", message); 
		assertExpectedException("LastName LIKE \"M*\"", message); 
	}
	@Test
	void testThrowsForSecondNotOperator() {
		String message = "After a field name, an operator must follow";
		assertExpectedException(" firstName lastName ", message);
		assertExpectedException(" firstName LIKe ", message);
		assertExpectedException(" firstName LIKEE ", message);
		assertExpectedException(" jmbag \"20202\"", message);
		assertExpectedException(" lastName jmbag", message);
		assertExpectedException("lastName", "Unexpected end of query reached after field name"); 
	}

	@Test
	void testThrowsForIllegalOperator() {
		String message = "Illegal operator used in query";
		assertExpectedException("firstName <> \"M*\"", message); 
		assertExpectedException("jmbag == \"0000000000\"", message); 
		assertExpectedException("lastName >==!!< \"M*\"", message); 
	}
	@Test
	void testThrowsForInvalidLiteral() {
		String message = "The second argument in a query";
		assertExpectedException(" firstName=lastName", message);
		assertExpectedException("jmbag LIKE <=", message);
		assertExpectedException("lastName >= LIKE", message);
		assertExpectedException("lastName LIKE ", "Unexpected end of query reached after operator");
	}

	@Test
	void testThrowsForMultipleWildcards() {
		String message = "Only one occurrence of a wildcard can be present";
		assertExpectedException("firstName LIKE \"*M*\"", message); //space between first and name, 'n' not capitalized
		assertExpectedException("jmbag LIKE \"Ma**ri\"", message); 
		assertExpectedException("lastName LIKE \"***\"", message); 
	}
	@Test
	void testDirectQuery() {
		QueryParser p = new QueryParser("jmbag=\"10293\"");
		assertTrue(p.isDirectQuery());
		assertEquals("10293", p.getQueriedJMBAG());
		assertEquals(1, p.getQuery().size()); //checking if the list is updated for direct queries too
		
		QueryParser p2 = new QueryParser("jmbag>\"10293\"");
		assertFalse(p2.isDirectQuery());
		assertThrows(IllegalStateException.class, ()->p2.getQueriedJMBAG());
		assertEquals(1, p.getQuery().size());
		
		QueryParser p3 = new QueryParser("lastName = \"Marić\"");
		assertFalse(p3.isDirectQuery());
		assertThrows(IllegalStateException.class, ()->p2.getQueriedJMBAG());
		assertEquals(1, p.getQuery().size());
	}
	@Test
	void testNormalQuery() {
		QueryParser p = new QueryParser("jmbag>\"10293\"");
		assertEquals(FieldValueGetters.JMBAG, p.getQuery().get(0).getFieldGetter());
		assertEquals(ComparisonOperators.GREATER, p.getQuery().get(0).getOperator());
		assertEquals("10293", p.getQuery().get(0).getStringLiteral());
		p = new QueryParser("lastName LIKE \"Ma*ć\"");
		assertEquals(FieldValueGetters.LAST_NAME, p.getQuery().get(0).getFieldGetter());
		assertEquals(ComparisonOperators.LIKE, p.getQuery().get(0).getOperator());
		assertEquals("Ma*ć", p.getQuery().get(0).getStringLiteral());
	}
	@Test
	void testThrowsForUnknownAfterLiteral() {
		assertExpectedException("jmbag>\"1000\" field", "Unexpected token after string literal: field");
		assertExpectedException("jmbag>\"1000\" \"another\"", "Unexpected token after string literal: another");
		assertExpectedException("jmbag>\"1000\" <=", "Unexpected token after string literal: <=");
	}
	@Test
	void testThrowsForEOQAfterAnd() {
		assertExpectedException("firstName=\"Dewie\" AND ", "Unexpected end of query reached after logical");
	}
	@Test
	void testMultipleQueries() {
		QueryParser p = new QueryParser("jmbag=\"000\" aNd lastName LIKE \"M*\" AnD firstName <= \"ttt\"");
		assertFalse(p.isDirectQuery());
		assertEquals(FieldValueGetters.JMBAG, p.getQuery().get(0).getFieldGetter());
		assertEquals(FieldValueGetters.LAST_NAME, p.getQuery().get(1).getFieldGetter());
		assertEquals(FieldValueGetters.FIRST_NAME, p.getQuery().get(2).getFieldGetter());
		assertEquals(ComparisonOperators.EQUALS, p.getQuery().get(0).getOperator());
		assertEquals(ComparisonOperators.LIKE, p.getQuery().get(1).getOperator());
		assertEquals(ComparisonOperators.LESS_OR_EQUALS, p.getQuery().get(2).getOperator());
		assertEquals("000", p.getQuery().get(0).getStringLiteral());
		assertEquals("M*", p.getQuery().get(1).getStringLiteral());
		assertEquals("ttt", p.getQuery().get(2).getStringLiteral());
		assertEquals(3, p.getQuery().size());
	}
	@Test
	void test() {
		//test constructor
		//test all exceptions
		//test correct cases
		//test other methods
	}

}
