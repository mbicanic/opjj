package hr.fer.zemris.java.hw05.db;

import static org.junit.jupiter.api.Assertions.*;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;

import hr.fer.zemris.java.hw05.db.lex.QueryException;
@SuppressWarnings("unused") //to ignore StudentDatabase objects constructed in try-catch blocks
class StudentDatabaseTest {
	
	/*
	 * This method is written to avoid writing the whole try-catch block
	 * within individual test methods. It ensures that an exception will
	 * be thrown, and by checking the message ensures that it is thrown 
	 * from the correct place in code, and that the message is appropriate.
	 */
	@SuppressWarnings("unused")
	private void assertExpectedException(List<String> lines, String message) {
		try {
			StudentDatabase p = new StudentDatabase(lines);
		} catch (QueryException ex) {
			assertTrue(ex.getMessage().startsWith(message));
		}
	}
	
	private void iterate(StudentRecord[] expected, String[] jmbags, StudentDatabase sdb) {
		for(int i = 0; i < expected.length; i++) {
			assertEquals(sdb.forJMBAG(jmbags[i]), expected[i]);
		}
	}
	private StudentDatabase getSDB() {
		List<String> lines; 
		try{
			lines = Files.readAllLines(    
					Paths.get("./database.txt"),
					StandardCharsets.UTF_8);
		} catch(IOException e) {
			throw new RuntimeException();
		}
		return new StudentDatabase(lines);
	}
	
	@Test
	void testForJMBAG() {
		StudentDatabase sdb = getSDB();
		StudentRecord[] expected = new StudentRecord[] {
				new StudentRecord("Marin", "Akšamović", "0000000001", 2),
				new StudentRecord("a", "b", "0000000001", 2),
				new StudentRecord("Jelena", "Martinec", "0000000039", 4),
				new StudentRecord("Andro", "Skočir", "0000000051", 4),
				new StudentRecord("Željko", "Žabčić", "0000000063", 4)
		};
		String[] jmbags = new String[] {
				"0000000001",
				"0000000001",
				"0000000039",
				"0000000051",
				"0000000063"
		};
		iterate(expected, jmbags, sdb);
		assertThrows(QueryException.class, ()->sdb.forJMBAG("999999999"));
	}
	@Test
	void testFilterTrue() {
		StudentDatabase sdb = getSDB();
		IFilter positive = (sr) -> true;
		assertEquals(63, sdb.filter(positive).size());
		assertEquals(new StudentRecord("a", "b", "0000000063", 4), sdb.filter(positive).get(62));
		assertEquals(new StudentRecord("a", "b", "0000000001", 4), sdb.filter(positive).get(0));
	}
	@Test
	void testFilterFalse() {
		StudentDatabase sdb = getSDB();
		IFilter negative = (sr)->false;
		assertEquals(0, sdb.filter(negative).size());
	}
	
	@Test
	void testConstructorThrows() {
		assertThrows(NullPointerException.class, ()-> {new StudentDatabase(null);});
	}
	@Test
	void testSDBEmptyForEmptyList() {
		StudentDatabase sdb = new StudentDatabase(new ArrayList<String>());
		assertEquals(new ArrayList<StudentRecord>(), sdb.filter((sr)->true));
		assertEquals(new ArrayList<StudentRecord>(), sdb.filter((sr)->false));
	}
	@Test
	void testSDBAcceptsIrregularOrder() {
		ArrayList<String> lines = new ArrayList<>();
		lines.add("  Marić  \t  Filip  \t5  \t  0000000002  ");
		StudentDatabase sdb = new StudentDatabase(lines);
		StudentRecord sr = sdb.forJMBAG("Marić");
		assertEquals("5", sr.getFirstName());
		assertEquals(2, sr.getGrade());
		assertEquals("Filip", sr.getLastName());
	}
	@Test
	void testSDBMultipleNames() {
		ArrayList<String> lines = new ArrayList<>();
		lines.add("0000000001\tMarić Erić Bilić\t Petar-Kristijan Ante  \t  2 ");
		StudentDatabase sdb = new StudentDatabase(lines);
		StudentRecord sr = sdb.forJMBAG("0000000001");
		assertEquals("Petar-Kristijan Ante", sr.getFirstName());
		assertEquals(2, sr.getGrade());
		assertEquals("Marić Erić Bilić", sr.getLastName());
	}
	@Test
	void testSDBThrowsForTooManyParams() {
		ArrayList<String> lines = new ArrayList<>();
		lines.add("0000000005\tDoe\tJohn\tExtra\t5");
		assertExpectedException(lines, "Incorrect record format in text file");
	}
	@Test
	void testSDBThrowsForExistingJMBAG() {
		ArrayList<String> lines = new ArrayList<>();
		lines.add("0000000005\tDoe\tJohn\t5");
		lines.add("0000000005\tPackard\tHubert\t2");
		assertExpectedException(lines, "Record with JMBAG 0000000005 already exists!");
	}
	@Test
	void testSDBThrowsForUnparsableGrade() {
		ArrayList<String> lines = new ArrayList<>();
		lines.add("0000000005\tDoe\tJohn\t 5");
		lines.add("0000000006\tPackard\tHubert\t grade");
		assertExpectedException(lines, "Invalid argument given as a grade: grade");
	}
	@Test
	void testSDBThrowsForGradeOutOfRange() {
		ArrayList<String> lines = new ArrayList<>();
		lines.add("0000000005\tDoe\tJohn\t 5");
		lines.add("0000000006\tPackard\tHubert\t 7");
		assertExpectedException(lines, "Illegal grade given in a record: 7");
	}
	
	@Test
	void testSimpleFilterOne() {
		List<ConditionalExpression> conditions = new ArrayList<>();
		conditions.add(new ConditionalExpression(
				FieldValueGetters.LAST_NAME,
				"S",
				ComparisonOperators.GREATER
				));
		QueryFilter f = new QueryFilter(conditions);
		List<StudentRecord> result = getSDB().filter(f);
		//15 records with last name really bigger than "S", + 2 records starting with Č or Ć
		assertEquals(17, result.size()); 
		assertTrue(result.contains(new StudentRecord("a","b","0000000007", 0)));
		assertTrue(result.contains(new StudentRecord("a","b","0000000049", 0)));
	}
	@Test
	void testSimpleFilterTwo() {
		List<ConditionalExpression> conditions = new ArrayList<>();
		conditions.add(new ConditionalExpression(
				FieldValueGetters.JMBAG,
				"*9",
				ComparisonOperators.LIKE));
		QueryFilter f = new QueryFilter(conditions);
		List<StudentRecord> result = getSDB().filter(f);
		assertEquals(6, result.size()); 
		assertTrue(result.contains(new StudentRecord("a","b","0000000009", 0)));
		assertTrue(result.contains(new StudentRecord("a","b","0000000049", 0)));
	}
	@Test
	void testComplexFilter() {
		List<ConditionalExpression> conditions = new ArrayList<>();
		conditions.add(new ConditionalExpression(
				FieldValueGetters.JMBAG,
				"*9",
				ComparisonOperators.LIKE));
		conditions.add(new ConditionalExpression(
				FieldValueGetters.LAST_NAME,
				"S",
				ComparisonOperators.GREATER));
		conditions.add(new ConditionalExpression(
				FieldValueGetters.FIRST_NAME,
				"Branimir",
				ComparisonOperators.LESS_OR_EQUALS));
		QueryFilter f = new QueryFilter(conditions);
		List<StudentRecord> result = getSDB().filter(f);
		assertEquals(1, result.size()); 
		assertTrue(result.contains(new StudentRecord("a","b","0000000049", 0)));
	}
}
