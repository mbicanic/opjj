package hr.fer.zemris.java.hw05.db;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class ConditionalExpressionTest {

	private StudentRecord[] getTemplate() {
		return new StudentRecord[] {
				new StudentRecord("Marko", "Perun", "1", 5),
				new StudentRecord("Tvrtko", "Savić", "2", 4),
				new StudentRecord("Kristijan", "Ljutić", "3", 3)
		};
	}
	
	@Test
	void testThrowsForAnyNull() {
		assertThrows(NullPointerException.class, ()-> { 
			new ConditionalExpression(
					null, 
					"a", 
					ComparisonOperators.EQUALS
					);
			});
		assertThrows(NullPointerException.class, ()-> { 
			new ConditionalExpression(
					FieldValueGetters.FIRST_NAME, 
					null, 
					ComparisonOperators.EQUALS
					);
			});
		assertThrows(NullPointerException.class, ()-> { 
			new ConditionalExpression(
					FieldValueGetters.FIRST_NAME, 
					"a", 
					null
					);
			});
	}
	@Test
	void testConditionalExpressionLikeFirstName() {
		StudentRecord[] recs = getTemplate(); 
		
		ConditionalExpression ce1 = new ConditionalExpression(
				FieldValueGetters.FIRST_NAME,
				"*ko",
				ComparisonOperators.LIKE
				);
		assertTrue(ce1.getOperator().satisfied(ce1.getFieldGetter().get(recs[0]), ce1.getStringLiteral()));
		assertTrue(ce1.getOperator().satisfied(ce1.getFieldGetter().get(recs[1]), ce1.getStringLiteral()));
		assertFalse(ce1.getOperator().satisfied(ce1.getFieldGetter().get(recs[2]), ce1.getStringLiteral()));
	}
	@Test
	void testConditionalExpressionLikeLastName() {
		StudentRecord[] recs = getTemplate(); 
		
		ConditionalExpression ce1 = new ConditionalExpression(
				FieldValueGetters.LAST_NAME,
				"*ić",
				ComparisonOperators.LIKE
				);
		assertFalse(ce1.getOperator().satisfied(ce1.getFieldGetter().get(recs[0]), ce1.getStringLiteral()));
		assertTrue(ce1.getOperator().satisfied(ce1.getFieldGetter().get(recs[1]), ce1.getStringLiteral()));
		assertTrue(ce1.getOperator().satisfied(ce1.getFieldGetter().get(recs[2]), ce1.getStringLiteral()));
	}
	@Test
	void testConditionalExpressionJmbagNotEqual() {
		StudentRecord[] recs = getTemplate(); 
		
		ConditionalExpression ce1 = new ConditionalExpression(
				FieldValueGetters.JMBAG,
				"2",
				ComparisonOperators.NOT_EQUALS
				);
		assertTrue(ce1.getOperator().satisfied(ce1.getFieldGetter().get(recs[0]), ce1.getStringLiteral()));
		assertFalse(ce1.getOperator().satisfied(ce1.getFieldGetter().get(recs[1]), ce1.getStringLiteral()));
		assertTrue(ce1.getOperator().satisfied(ce1.getFieldGetter().get(recs[2]), ce1.getStringLiteral()));
	}

}
