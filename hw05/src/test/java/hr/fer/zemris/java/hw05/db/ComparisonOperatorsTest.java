package hr.fer.zemris.java.hw05.db;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class ComparisonOperatorsTest {

	@Test
	void testEquals() {
		assertTrue(ComparisonOperators.EQUALS.satisfied("pero", "pero"));
		assertTrue(ComparisonOperators.EQUALS.satisfied("0036506059", "0036506059"));
		assertFalse(ComparisonOperators.EQUALS.satisfied("pero", "per"));
		assertFalse(ComparisonOperators.EQUALS.satisfied("0036506059", "0036506058"));
		assertThrows(NullPointerException.class, ()->ComparisonOperators.EQUALS.satisfied(null, "0036506058"));
		assertThrows(NullPointerException.class, ()->ComparisonOperators.EQUALS.satisfied("0036506058", null));
	}
	@Test
	void testNotEquals() {
		assertFalse(ComparisonOperators.NOT_EQUALS.satisfied("pero", "pero"));
		assertFalse(ComparisonOperators.NOT_EQUALS.satisfied("0036506059", "0036506059"));
		assertTrue(ComparisonOperators.NOT_EQUALS.satisfied("pero", "per"));
		assertTrue(ComparisonOperators.NOT_EQUALS.satisfied("0036506059", "0036506058"));
		assertThrows(NullPointerException.class, ()->ComparisonOperators.NOT_EQUALS.satisfied(null, "0036506058"));
		assertThrows(NullPointerException.class, ()->ComparisonOperators.NOT_EQUALS.satisfied("0036506058", null));
	}
	@Test
	void testGreater() {
		assertTrue(ComparisonOperators.GREATER.satisfied("pero", "per"));
		assertTrue(ComparisonOperators.GREATER.satisfied("0036506059", "0036506058"));
		assertFalse(ComparisonOperators.GREATER.satisfied("value one", "value one"));
		assertFalse(ComparisonOperators.GREATER.satisfied("X", "XAAA"));
		assertFalse(ComparisonOperators.GREATER.satisfied("0036506059", "0036506060"));
		assertThrows(NullPointerException.class, ()->ComparisonOperators.GREATER.satisfied(null, "0036506058"));
		assertThrows(NullPointerException.class, ()->ComparisonOperators.GREATER.satisfied("0036506058", null));
	}
	@Test
	void testGreaterEquals() {
		assertTrue(ComparisonOperators.GREATER_OR_EQUALS.satisfied("pero", "per"));
		assertTrue(ComparisonOperators.GREATER_OR_EQUALS.satisfied("0036506059", "0036506058"));
		assertTrue(ComparisonOperators.GREATER_OR_EQUALS.satisfied("value one", "value one"));
		assertFalse(ComparisonOperators.GREATER_OR_EQUALS.satisfied("X", "XAAA"));
		assertFalse(ComparisonOperators.GREATER_OR_EQUALS.satisfied("0036506059", "0036506060"));
		assertThrows(NullPointerException.class, ()->ComparisonOperators.GREATER_OR_EQUALS.satisfied(null, "0036506058"));
		assertThrows(NullPointerException.class, ()->ComparisonOperators.GREATER_OR_EQUALS.satisfied("0036506058", null));
	}
	@Test
	void testLesser() {
		assertFalse(ComparisonOperators.LESS.satisfied("pero", "per"));
		assertFalse(ComparisonOperators.LESS.satisfied("0036506059", "0036506058"));
		assertFalse(ComparisonOperators.LESS.satisfied("value one", "value one"));
		assertTrue(ComparisonOperators.LESS.satisfied("X", "XAAA"));
		assertTrue(ComparisonOperators.LESS.satisfied("0036506059", "0036506060"));
		assertThrows(NullPointerException.class, ()->ComparisonOperators.LESS.satisfied(null, "0036506058"));
		assertThrows(NullPointerException.class, ()->ComparisonOperators.LESS.satisfied("0036506058", null));
	}
	@Test
	void testLesserEquals() {
		assertFalse(ComparisonOperators.LESS_OR_EQUALS.satisfied("pero", "per"));
		assertFalse(ComparisonOperators.LESS_OR_EQUALS.satisfied("0036506059", "0036506058"));
		assertTrue(ComparisonOperators.LESS_OR_EQUALS.satisfied("value one", "value one"));
		assertTrue(ComparisonOperators.LESS_OR_EQUALS.satisfied("X", "XAAA"));
		assertTrue(ComparisonOperators.LESS_OR_EQUALS.satisfied("0036506059", "0036506060"));
		assertThrows(NullPointerException.class, ()->ComparisonOperators.LESS_OR_EQUALS.satisfied(null, "0036506058"));
		assertThrows(NullPointerException.class, ()->ComparisonOperators.LESS_OR_EQUALS.satisfied("0036506058", null));
	}
	@Test
	void testLike() {
		assertTrue(ComparisonOperators.LIKE.satisfied("AAAA", "AA*AA"));
		assertTrue(ComparisonOperators.LIKE.satisfied("Zagreb", "Z*b"));
		assertTrue(ComparisonOperators.LIKE.satisfied("Syringe", "*nge"));
		assertTrue(ComparisonOperators.LIKE.satisfied("Syringe", "*Syringe"));
		assertTrue(ComparisonOperators.LIKE.satisfied("Technics", "Technic*"));
		assertTrue(ComparisonOperators.LIKE.satisfied("Technics", "Technics*"));
		assertFalse(ComparisonOperators.LIKE.satisfied("Zagreb", "Aba*"));
		assertFalse(ComparisonOperators.LIKE.satisfied("AAA", "AA*AA"));
		assertTrue(ComparisonOperators.LIKE.satisfied("Whatever text you may want!", "*"));
		assertThrows(NullPointerException.class, ()->ComparisonOperators.LIKE.satisfied(null, "0036506058"));
		assertThrows(NullPointerException.class, ()->ComparisonOperators.LIKE.satisfied("0036506058", null));
	}

}
