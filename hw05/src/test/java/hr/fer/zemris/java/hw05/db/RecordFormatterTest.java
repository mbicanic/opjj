package hr.fer.zemris.java.hw05.db;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;

class RecordFormatterTest {

	public List<StudentRecord> getList() {
		List<StudentRecord> l = new ArrayList<>();
		l.add(new StudentRecord("Marko", "Perić", "00000", 1));
		l.add(new StudentRecord("Tin", "Jurić", "00001", 2));
		l.add(new StudentRecord("Mara", "Purić", "00002", 3));
		l.add(new StudentRecord("Sara", "Belić", "00003", 4));
		return l;
	}
	@Test
	void testThrowsForNull() {
		assertThrows(NullPointerException.class, ()->RecordFormatter.format(null));
	}
	@Test
	void testNullForEmpty() {
		assertNull(RecordFormatter.format(new ArrayList<StudentRecord>()));
	}
	@Test
	void testCorrectOneRecord() {
		List<StudentRecord> l = new ArrayList<>();
		l.add(new StudentRecord("Marko", "Perić", "0000000001", 2));
		List<String> t = RecordFormatter.format(l);
		assertEquals(t.size(), 3);
		assertEquals("+============+=======+=======+===+", t.get(0));
		assertEquals("| 0000000001 | Perić | Marko | 2 |", t.get(1));
		assertEquals("+============+=======+=======+===+", t.get(2));
	}
	@Test
	void testCorrectMultipleRecords() {
		List<String> t = RecordFormatter.format(getList());
		assertEquals(6, t.size());
		assertEquals("+=======+=======+=======+===+", t.get(0));
		assertEquals("| 00001 | Jurić | Tin   | 2 |", t.get(2));
		assertEquals("+=======+=======+=======+===+", t.get(5));
	}
	@Test
	void testResizing() {
		List<StudentRecord> l = getList();
		l.add(new StudentRecord("Tomislav", "Novoselec", "00000000", 2));
		List<String> t = RecordFormatter.format(l);
		assertEquals(7, t.size());
		assertEquals("+==========+===========+==========+===+", t.get(0));
		assertEquals("| 00001    | Jurić     | Tin      | 2 |", t.get(2));
		assertEquals("| 00000000 | Novoselec | Tomislav | 2 |", t.get(5));
		assertEquals("+==========+===========+==========+===+", t.get(6));
	}
}
