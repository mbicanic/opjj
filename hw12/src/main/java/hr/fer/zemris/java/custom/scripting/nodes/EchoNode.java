package hr.fer.zemris.java.custom.scripting.nodes;

import hr.fer.zemris.java.custom.scripting.elems.*;

/**
 * EchoNode is a node that represents the whole echo
 * tag ( {$= v1 v2 ... vn $} ).
 * 
 * It contains all numbers, operators, variables, strings
 * and functions contained in the original tag from the text.
 * 
 * @author Miroslav Bićanić
 */
public class EchoNode extends Node {
	/**
	 * An array of elements that comprise this EchoNode.
	 */
	private Element[] elements;
	/**
	 * A constructor taking a variable number of elements
	 * and adding them to this EchoNode's elements array.
	 * @param elems
	 */
	public EchoNode(Element...elems) {
		this.elements = elems;
	}
	
	/**
	 * @return An array of all elements that comprise this EchoNode
	 */
	public Element[] getElements() {
		return this.elements;
	}
	
	@Override
	public String toString() {
		StringBuilder s = new StringBuilder(10 + 2*elements.length);
		s.append("{$= ");
		for(int i = 0; i < elements.length; i++) {
			s.append(elements[i].toString());
		}
		s.append("$}");
		return s.toString();
	}
	
	/**
	 * Gives this EchoNode as the argument to the given {@link INodeVisitor}
	 * {@code v}.
	 * 
	 * @param v the {@link INodeVisitor} that visited this node
	 */
	public void accept(INodeVisitor v) {
		v.visitEchoNode(this);
	}
	
}
