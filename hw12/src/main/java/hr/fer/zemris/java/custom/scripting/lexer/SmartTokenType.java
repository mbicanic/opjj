package hr.fer.zemris.java.custom.scripting.lexer;

/**
 * An enumeration of all possible types of tokens
 * that a SmartScriptLexer can generate.
 * 
 * @see SmartLexerState
 * @author Miroslav Bićanić
 */
public enum SmartTokenType {
	
	/**
	 * This type represents a string in the context of an
	 * expression. This token type can be generated only
	 * in TAG state (i.e. as part of an expression).
	 */
	STRING,
	/**
	 * This type represents a keyword in an expression.
	 * A keyword in this context means tag name.
	 * It can be generated only in TAG state, and only
	 * when the previous token was a tag opener.
	 * Legal tag names are "=" or anything legal for a
	 * variable name.
	 */
	KWD,
	/**
	 * This type represents a variable inside an expression.
	 * This type can be generated only in TAG state.
	 * A legal variable name starts with a character, and
	 * then has 0 or more characters, digits or underscores.
	 */
	VAR,
	/**
	 * This type represents an operator inside an expression.
	 * This type can be generated only in TAG state.
	 * Valid operators are: +, -, *, /, ^.
	 */
	OPERATOR,
	/**
	 * This type represents a function inside an expression.
	 * This type can be generated only in TAG state.
	 * A legal function name starts with a character, and
	 * then has 0 or more characters, digits or underscores.
	 */
	FUNCTION,
	/**
	 * This type represents a double constant inside an expression.
	 * This type can be generated only in TAG state.
	 * Only numbers in [digit].[digit] format are allowed - 
	 * scientific notation is NOT supported.
	 */
	DOUBLE,
	/**
	 * This type represents an integer constant inside an expression.
	 * This type can be generated only in TAG state.
	 * The range of numbers is limited by the Integer format.
	 */
	INT,
	/**
	 * This is another type that represents a string, but outside the
	 * context of an expression. This token type can be generated only 
	 * from TEXT state.
	 */
	TEXT,
	/**
	 * This is a special type of a token that can be generated from
	 * both states. 
	 * In TEXT state, the only SYMBOL that can be generated is the
	 * tag opener ('{$').
	 * In TAG state, the only SYMBOL that can be generated is the
	 * tag close ('$}').
	 */
	SYMBOL,
	/**
	 * This is a special type of a token that can be generated from
	 * both states. It is generated once the end of file has been
	 * reached and there are no more tokens to be generated.
	 */
	EOF;
}
