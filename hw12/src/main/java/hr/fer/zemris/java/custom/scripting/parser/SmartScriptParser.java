package hr.fer.zemris.java.custom.scripting.parser;

import java.util.ArrayList;
import java.util.Stack;

import hr.fer.zemris.java.custom.scripting.elems.Element;
import hr.fer.zemris.java.custom.scripting.elems.ElementConstantDouble;
import hr.fer.zemris.java.custom.scripting.elems.ElementConstantInteger;
import hr.fer.zemris.java.custom.scripting.elems.ElementFunction;
import hr.fer.zemris.java.custom.scripting.elems.ElementOperator;
import hr.fer.zemris.java.custom.scripting.elems.ElementString;
import hr.fer.zemris.java.custom.scripting.elems.ElementVariable;
import hr.fer.zemris.java.custom.scripting.lexer.SmartLexerException;
import hr.fer.zemris.java.custom.scripting.lexer.SmartLexerState;
import hr.fer.zemris.java.custom.scripting.lexer.SmartScriptLexer;
import hr.fer.zemris.java.custom.scripting.lexer.SmartTokenType;
import hr.fer.zemris.java.custom.scripting.lexer.Token;
import hr.fer.zemris.java.custom.scripting.nodes.DocumentNode;
import hr.fer.zemris.java.custom.scripting.nodes.EchoNode;
import hr.fer.zemris.java.custom.scripting.nodes.ForLoopNode;
import hr.fer.zemris.java.custom.scripting.nodes.Node;
import hr.fer.zemris.java.custom.scripting.nodes.TextNode;

/**
 * SmartScriptParser parses a document, received as a
 * series of tokens from SmartScriptLexer, into nodes
 * that represent blocks of the original text file.
 * 
 * The documents it reads can consist only of the following:
 * 1) Tags - segment of a text file surrounded by '{$' and
 * 		'$}'. Only the following tags are supported: 
 * 			a) ECHO TAG: {$ = [var|str|func|int|double|op]... $}
 * 			b) FOR TAG: {$ FOR [var] [var|str|number] [var|str|number] ([var|str|number]) $}
 * 			c) END TAG: {$ END $}
 * 	a) Echo tags must start with one KWD token "=" and then
 * 		have an arbitrary number of VAR, STRING, INT, DOUBLE,
 * 		FUNCTION or OPERATOR tokens.
 * 	b) For tags must start with one KWD token "for" and then
 * 		one VAR token, two VAR, DOUBLE, INT or STRING tokens
 * 		and an optional third VAR, DOUBLE, INT or STRING token.
 * 2) Text - all remaining text outside of or in between any
 * 			two neighbor tags.
 * 
 * For tags are closable tags - after N for tags there has 
 * to be exactly N end tags to close them.
 * 
 * This parser generates a document tree using {@link Node}
 * and its derivatives. After parsing the document, the 
 * parser can return the {@link DocumentNode}, with which
 * it is possible to reconstruct the document, or use it
 * for further analysis.
 * 
 * @author Miroslav Bićanić
 */
public class SmartScriptParser {
	
	private static final String OPENER = "{$";
	private static final String CLOSER = "$}";
	
	private static final String KWDFor = "for";
	private static final String KWDEcho = "=";
	private static final String ECHO_STR = "echo";
	private static final String KWDEnd = "end";
	
	/**
	 * The document node of the document whose text
	 * is received in the constructor.
	 */
	private DocumentNode document;
	/**
	 * A stack used to manipulate nodes. Initially document
	 * is pushed on stack. Segments (nodes) of the file are added
	 * to the node from top of the stack as its children.
	 * If one of the added nodes is a closable node
	 * (for tag), the segments from it until the first
	 * end tag are added as that closable node's children instead.
	 * This allows us to have closable tags within closable
	 * tags and keep the order of the document.
	 */
	private Stack<Node> stack;
	/**
	 * An instance of a SmartScriptLexer used to generate tokens
	 * for parsing.
	 */
	private SmartScriptLexer lexer;
	
	/**
	 * A constructor for a SmartScriptParser taking a body
	 * of a document as a String and immediately parsing it.
	 * @param docBody The body of a document as a string.
	 */
	public SmartScriptParser(String docBody) {
		try {
			this.lexer = new SmartScriptLexer(docBody);			
		} catch (NullPointerException ex) {
			throw new SmartScriptParserException("PARSER ERROR: Null reference handed.");
		}
		document = new DocumentNode();
		stack = new Stack<>();
		stack.push(document);
		parse();
	}
	
	/**
	 * Parses tokens received from a SmartScriptLexer, checking
	 * if the document is structurally acceptable and generating
	 * its structural tree if it is.
	 * 
	 * Updates the document node so that from it the whole file
	 * can be reconstructed (and reparsable), or used for further
	 * analysis.
	 * @throws SmartScriptParserException 
	 * 		-if the first token in iteration is neither
	 * 			a text nor a tag opener, 
	 * 		-if unexpected EOF is reached, 
	 * 		-if a closable tag isn't closed
	 */
	private void parse() {
		Token t = nextTokenSecure(null);
		while(t.getType()!=SmartTokenType.EOF) {
			Node current = (Node)stack.peek();
			/*
			 * The type of the next token can be only text, or a tag
			 * opener, because once a tag opener is encountered, the
			 * whole tag is parsed and the tag is closed, after which
			 * the next token must be text value.
			 */
			if(t.getType()==SmartTokenType.TEXT) {
				TextNode child = new TextNode((String)t.getValue());
				current.addChildNode(child);
				t = nextTokenSecure(null);
				continue;
			}
			if(!isTagOpener(t)) {
				throw new SmartScriptParserException("PARSER ERROR: Invalid format of input file. File must contain of text and tags.");
			}
			
			//IF THIS CODE IS EXECUTING A TAG OPENER WAS READ AND WHAT FOLLOWS IS TAG CONTENT
			
			lexer.setState(SmartLexerState.TAG);
			
			Token tagID = nextTokenSecure(null);	//this token is of type KWD, it is the tag name.
			if(tagID.getType()==SmartTokenType.EOF) {
				throw new SmartScriptParserException("PARSER ERROR: Unexpected EOF after tag opener. Expected tag name.");
			}
			String name = tagID.getValue().toString().toLowerCase();
			parseTag(name, current);

			t = nextTokenSecure(null);
			continue;
		}
		/*
		 * Once parsing is finished, only DocumentNode is supposed to
		 * remain on stack.
		 */
		if(stack.peek() instanceof ForLoopNode) {
			int i = 0;
			while(true) {
				if(stack.pop() instanceof ForLoopNode){
					i++;
				} else {
					break;
				}
			}
			throw new SmartScriptParserException("PARSER ERROR: "+i+" for tag"+(i>1?"s":"")+" remained unclosed!");
		}
		if(stack.size()!=1) {
			throw new SmartScriptParserException("PARSER ERROR: "+stack.size()+" nodes remained. Expected 1.");
		}
	}
	
	/**
	 * This method parses a tag by checking the name of the tag
	 * and delegating it to appropriate methods, and returning
	 * the lexer back to TEXT state.
	 * 
	 * @param tagName The name of the opened tag
	 * @param current The current node on the stack
	 * @throws SmartScriptParserException
	 * 		-If unsupported tag name is used
	 */
	private void parseTag(String tagName, Node current) {
		if(tagName.toLowerCase().equals(KWDFor)) {
			ForLoopNode result = parseForTag();
			current.addChildNode(result);
			stack.push(result);
		} else if (tagName.equals(KWDEcho)) {
			EchoNode result = parseEchoTag();
			current.addChildNode(result);
		} else if (tagName.toLowerCase().equals(KWDEnd)) {
			parseEndTag(current);
			stack.pop();
		} else {
			throw new SmartScriptParserException("PARSER ERROR: Unsupported tag name: "+tagName);
		}
		lexer.setState(SmartLexerState.TEXT);
	}
	
	/**
	 * This method checks if the end tag is applicable at this point 
	 * (the current top of stack must be a closable node), and parses
	 * the tag. 
	 * 
	 * @param current The node currently on top of stack
	 * @throws SmartScriptParserException 
	 * 		-if an attempt at closing an unclosable
	 * 			tag is made
	 * 		-if an EOF is reached
	 * 		-if the tag wasn't immediately closed
	 */
	private void parseEndTag(Node current) {
		if(!(current instanceof ForLoopNode)) {
			throw new SmartScriptParserException("PARSER ERROR: Cannot close a non-closable tag.");
		}
		Token t = nextTokenSecure(KWDEnd); 
		if(t.getType()==SmartTokenType.EOF) {
			throw new SmartScriptParserException("PARSER ERROR: Unexpected EOF reached in end tag.");
		}
		if(!isTagCloser(t)){
			throw new SmartScriptParserException("PARSER ERROR: End tag must be closed without arguments.");
		}
		return;
	}

	/**
	 * This method parses all the elements of the expression within
	 * the echo tag and creates an EchoNode for the document structure.
	 * 
	 * @return An EchoNode set up by the elements of the echo tag
	 * @throws SmartScriptParserException
	 * 		-if an invalid token is received
	 * 		-if an unexpected EOF is reached
	 */
	private EchoNode parseEchoTag() {
		Token t = nextTokenSecure(ECHO_STR);
		ArrayList<Element> col = new ArrayList<>();
		while(t.getType()!=SmartTokenType.EOF && !isTagCloser(t)) {
			Element el ;
			if(t.getType()==SmartTokenType.DOUBLE) {
				el = new ElementConstantDouble((double)t.getValue());
			} else if (t.getType()==SmartTokenType.INT) {
				el = new ElementConstantInteger((int)t.getValue());
			} else if (t.getType()==SmartTokenType.FUNCTION) {
				el = new ElementFunction((String)t.getValue());
			} else if (t.getType()==SmartTokenType.OPERATOR) {
				el = new ElementOperator((String)t.getValue());
			} else if (t.getType()==SmartTokenType.VAR) {
				el = new ElementVariable((String)t.getValue());
			} else if (t.getType()==SmartTokenType.STRING) {
				el = new ElementString((String)t.getValue());
			} else {
				throw new SmartScriptParserException("PARSER ERROR: Invalid token received in echo tag: "+t.getValue());
			}
			col.add(el);
			t = nextTokenSecure(ECHO_STR);
		}
		if(t.getType()==SmartTokenType.EOF) {
			throw new SmartScriptParserException("PARSER ERROR: Unexpected end of file in echo tag declaration.");
		}
		Element[] elems = new Element[col.size()];
		Object[] arrayObjects = col.toArray();
		for(int i = 0; i < arrayObjects.length; i++) {
			elems[i] = (Element)arrayObjects[i];
		}
		return new EchoNode(elems);
	}

	/**
	 * This method parses the whole for tag and creates
	 * a ForLoopNode of it for the document structure. 
	 * 
	 * @return ForLoopNode built by the elements of the for tag
	 * @throws SmartScriptParserException 
	 * 		-if too many arguments are given in a for tag
	 */
	private ForLoopNode parseForTag() {
		ElementVariable var = extractVariable(KWDFor);
		Element start = getForParameter(true);
		Element end = getForParameter(true);
		Element step = getForParameter(false);
		if(step!=null) {
			Token temp = nextTokenSecure(KWDFor);
			if(!isTagCloser(temp)) {
				throw new SmartScriptParserException("PARSER ERROR: Too many arguments given in a for-tag!");
			}
		}
		return new ForLoopNode(var, start, end, step);
	}

	/**
	 * This method extracts the next token and expects it
	 * to be of VAR type, building an ElementVariable from it.
	 * 
	 * @return An ElementVariable built on the token
	 * @throws SmartScriptParserException 
	 * 		-if EOF is reached
	 * 		-if received token is not a variable
	 */
	private ElementVariable extractVariable(String source) {
		Token t = nextTokenSecure(source);
		if(t.getType()==SmartTokenType.EOF) {
			throw new SmartScriptParserException("PARSER ERROR: Unexpected end of file in a for-tag declaration.");
		}
		if(t.getType()!=SmartTokenType.VAR) {
			throw new SmartScriptParserException("PARSER ERROR: First argument in for-tag must be a variable.");
		}
		return new ElementVariable((String)t.getValue());
	}
	
	/**
	 * This method extracts the next token, allowing it to be any 
	 * of the tokens legal in a for tag, and creates a corresponding 
	 * element.
	 * The mandatory parameter dictates if retreiving an element is
	 * mandatory (first two arguments of for tag) or not (third
	 * argument).
	 * It returns null if an element isn't found and isn't mandatory.
	 * 
	 * @return One of the Element derivatives that can be found in expressions
	 * @throws SmartScriptParserException
	 * 		-if EOF is reached
	 * 		-if finding is mandatory but tag closer was found
	 * 			(too few arguments in for tag)
	 * 		-if an illegal argument for a for tag is found
	 */
	private Element getForParameter(boolean mandatory) {
		Token t = nextTokenSecure(KWDFor);
		if(t.getType()==SmartTokenType.EOF) {
			throw new SmartScriptParserException("PARSER ERROR: Unexpected end of file in for-tag declaration.");
		}
		if(t.getType()==SmartTokenType.INT ) {
			return new ElementConstantInteger((int)t.getValue());
		} else if (t.getType()==SmartTokenType.DOUBLE) {
			return new ElementConstantDouble((double)t.getValue());
		} else if (t.getType()==SmartTokenType.STRING) {
			return new ElementString((String)t.getValue());
		} else if (t.getType()==SmartTokenType.VAR) {
			return new ElementVariable((String)t.getValue());
		} else if (isTagCloser(t)) {
			if(mandatory) {
				throw new SmartScriptParserException("PARSER ERROR: For tag must contain at least 2 arguments.");
			} else {
				return null;
			}
		}
		throw new SmartScriptParserException("PARSER ERROR: Invalid argument type in for tag: "+t.getValue());  
	}
	
	/**
	 * This method extracts the next token from the lexer
	 * securely.
	 * 
	 * The code parameter (if a value is given) is used to give a more 
	 * specific description of a message - the exact tag in which it
	 * happened, and the character that caused it.
	 * 
	 * @return Any type of token the lexer can generate.
	 * @throws SmartScriptParserException
	 * 		-if the SmartScriptLexer throws any exceptions 
	 * 		(the file has ortographical errors)
	 */
	private Token nextTokenSecure(String code) {
		try {
			Token t = lexer.nextToken();
			return t;
		} catch (SmartLexerException ex) {
			if(code!=null && ex.getIllegalCharacter()!=null) {
				throw new SmartScriptParserException("PARSER:LEXER ERROR: Illegal argument in "+code+" tag: "+ex.getIllegalCharacter());
			}
			throw new SmartScriptParserException("PARSER:LEXER ERROR: "+ex.getMessage());
		}
	}
	
	/**
	 * Shorthand methods for testing if the token is a tag closer.
	 * @param t A token to check
	 * @return true if the token is a tag closer, false otherwise
	 */
	private boolean isTagCloser(Token t) {
		return t.getType()==SmartTokenType.SYMBOL && t.getValue().equals(CLOSER);
	}
	/**
	 * Shorthand methods for testing if the token is a tag opener.
	 * @param t A token to check
	 * @return true if the token is a tag opener, false otherwise
	 */
	private boolean isTagOpener(Token t) {
		return t.getType()==SmartTokenType.SYMBOL && t.getValue().equals(OPENER);
	}
	
	/**
	 * @return A document node with a built document tree in it
	 */
	public DocumentNode getDocumentNode() {
		return this.document;
	}
}
