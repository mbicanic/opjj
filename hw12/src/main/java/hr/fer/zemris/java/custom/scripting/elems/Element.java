package hr.fer.zemris.java.custom.scripting.elems;

import java.util.Objects;

/**
 * A base class for all elements in an expression.
 * 
 * Expressions are comprised of various different 
 * elements: operators, numbers, variables, functions
 * and strings.
 * 
 * @author Miroslav Bićanić
 */
public class Element {
	
	/**
	 * This method returns this Element's String 
	 * representation. 
	 * 
	 * @return An empty string.
	 */
	public String asText() {
		return new String();
	}
	/**
	 * This method is used to return the string representation
	 * of this Element followed by a space bar.
	 * It is used for generating a document from a document tree.
	 */
	@Override
	public String toString() {
		return asText()+" ";
	}
	
	/**
	 * Two Elements are equal when they are instances
	 * of the same class (derivatives of Element) and
	 * their text representations are equal.
	 */
	@Override
	public boolean equals(Object obj) {
		if(this==obj) {
			return true;
		}
		if(!(obj.getClass().equals(this.getClass()))) {
			return false;
		}
		Element other = (Element)obj;
		return this.asText().equals(other.asText());
	}

	@Override
	public int hashCode() {
		return Objects.hash(this.asText());
	}
	
	/**
	 * This method replaces all characters that should be escaped
	 * from the strings within a tag with an escape sequence used
	 * to escape them ( \ -> \\, " -> \" ) to make the generated
	 * text reparsable.
	 * 
	 * @param s A string to reverse escape
	 * @return A string with all escape sequences in place
	 */
	protected static String replaceTagEscape(String s) {
		for(int i=0; i<s.length(); i++) {
			if(s.charAt(i)=='\\' || s.charAt(i)=='\"') {
				s = s.substring(0,i) + "\\" + s.substring(i);
				i+=1;
			}
		}
		return s;
	}
}
