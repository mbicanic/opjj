package hr.fer.zemris.java.custom.scripting.lexer;

/**
 * An enumeration of states the SmartScriptLexer can be in.
 * 
 * @see SmartTokenType
 * @author Miroslav Bićanić
 */
public enum SmartLexerState {
	/**
	 * In this state the lexer interprets everything
	 * it finds until the first tag opener ('{$') as
	 * a single token of the type {@link TokenType}
	 * .TEXT. It can also return EOF type.
	 */
	TEXT,
	/**
	 * In this state, the lexer tokenizes a tag,
	 * returning tokens of all other types. 
	 */
	TAG;
}
