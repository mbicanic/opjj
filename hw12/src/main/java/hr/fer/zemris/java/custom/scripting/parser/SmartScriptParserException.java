package hr.fer.zemris.java.custom.scripting.parser;

/**
 * This exception is thrown whenever a parser encounters
 * an error of any kind.
 * 
 * @author Miroslav Bićanić
 */
public class SmartScriptParserException extends RuntimeException {
	private static final long serialVersionUID = 1L;
	
	/**
	 * A constructor for this exception.
	 */
	public SmartScriptParserException() {
		super();
	}
	/**
	 * A constructor that takes a message describing the error.
	 * @param msg A description of the error that happened
	 */
	public SmartScriptParserException(String msg) {
		super(msg);
	}
	
}
