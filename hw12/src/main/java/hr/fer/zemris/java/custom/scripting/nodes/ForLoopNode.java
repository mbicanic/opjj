package hr.fer.zemris.java.custom.scripting.nodes;

import java.util.Objects;

import hr.fer.zemris.java.custom.scripting.elems.*;

/**
 * ForLoopNode is a node that represents the whole block of
 * a for-tag ( {$ FOR v1 v2 v3 [v4] $} ).
 * 
 * It stores the parameters of the for-tag itself, and
 * all the nodes encompassed within this for-tag's block.
 * (until the first {$ END $} tag).
 * 
 * @author Miroslav Bićanić
 */
public class ForLoopNode extends Node {
	/**
	 * The variable parameter of the for-tag.
	 */
	private ElementVariable variable;
	/**
	 * The start expression of the for-tag.
	 */
	private Element startExpression;
	/**
	 * The end expression of the for-tag.
	 */
	private Element endExpression;
	/**
	 * The step expression of the for-tag.
	 * This element is not mandatory!
	 */
	private Element stepExpression;
	
	/**
	 * A constructor for a ForLoopNode, taking three mandatory parameters
	 * (a variable, a start expression and an end expression), with an
	 * optional fourth one (a step expression).
	 * 
	 * @param variable The variable for this ForLoopNode
	 * @param startExpression The start expression of this ForLoopNode
	 * @param endExpression The end expression of this ForLoopNode
	 * @param stepExpression The step expression of this ForLoopNode - can be null
	 */
	public ForLoopNode(ElementVariable variable, Element startExpression, Element endExpression,
			Element stepExpression) {
		super();
		Objects.requireNonNull(variable);
		Objects.requireNonNull(startExpression);
		Objects.requireNonNull(endExpression);
		
		this.variable = variable;
		this.startExpression = startExpression;
		this.endExpression = endExpression;
		
		this.stepExpression = stepExpression;
	}
	/**
	 * @return This node's variable
	 */
	public ElementVariable getVariable() {
		return variable;
	}
	/**
	 * @return This node's start expression
	 */
	public Element getStartExpression() {
		return startExpression;
	}
	/**
	 * @return This node's end expression.
	 */
	public Element getEndExpression() {
		return endExpression;
	}
	/**
	 * @return This node's step expression.
	 */
	public Element getStepExpression() {
		return stepExpression;
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder(20);
		sb.append("{$ FOR ");
		sb.append(variable);
		sb.append(startExpression);
		sb.append(endExpression);
		sb.append((stepExpression==null ? "" : stepExpression));
		sb.append("$}");
		return sb.toString();
	}
	
	/**
	 * Gives this ForLoopNode as the argument to the given {@link INodeVisitor}
	 * {@code v}.
	 * 
	 * @param v the {@link INodeVisitor} that visited this node
	 */
	public void accept(INodeVisitor v) {
		v.visitForLoopNode(this);
	}
}
