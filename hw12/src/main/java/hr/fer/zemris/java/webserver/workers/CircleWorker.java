package hr.fer.zemris.java.webserver.workers;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

import javax.imageio.ImageIO;

import hr.fer.zemris.java.webserver.IWebWorker;
import hr.fer.zemris.java.webserver.RequestContext;

/**
 * Implementation of a {@link IWebWorker} interface that paints
 * a green circle onto a white background and sends it through
 * the {@link RequestContext} as a PNG image.
 * 
 * @author Miroslav Bićanić
 */
public class CircleWorker implements IWebWorker {

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void processRequest(RequestContext context) throws Exception {
		BufferedImage bim = new BufferedImage(200, 200, BufferedImage.TYPE_3BYTE_BGR);
		Graphics2D g2 = bim.createGraphics();
		g2.setColor(Color.WHITE);
		g2.fillRect(0, 0, 200, 200);
		g2.setColor(Color.GREEN);
		g2.fillOval(0, 0, 200, 200);
		g2.dispose();
		
		context.setMimeType("image/png");
		
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		try {
			ImageIO.write(bim, "png", bos);
			context.write(bos.toByteArray());
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		
	}

}
