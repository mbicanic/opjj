package hr.fer.zemris.java.custom.scripting.nodes;
/**
 * DocumentNode is a node that represents the whole
 * document. It does not have any new features
 * compared to the base Node class.
 * 
 * @author Miroslav Bićanić
 */
public class DocumentNode extends Node {

	@Override
	public String toString() {
		return "";
	}
	
	/**
	 * Gives this DocumentNode as the argument to the given {@link INodeVisitor}
	 * {@code v}.
	 * 
	 * @param v the {@link INodeVisitor} that visited this node
	 */
	public void accept(INodeVisitor v) {
		v.visitDocumentNode(this);
	}
	
}
