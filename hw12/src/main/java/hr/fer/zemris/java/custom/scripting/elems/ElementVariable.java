package hr.fer.zemris.java.custom.scripting.elems;

import hr.fer.zemris.java.custom.scripting.lexer.SmartTokenType;

/**
 * ElementVariable is an element of an expression that
 * represents a variable.
 * 
 * @see SmartTokenType
 * @author Miroslav Bićanić
 */
public class ElementVariable extends Element {
	/**
	 * The name of this ElementVariable.
	 */
	private String name;
	
	/**
	 * A constructor for an ElementVariable.
	 * @param name The name to be assigned to this ElementVariable.
	 */
	public ElementVariable(String name) {
		this.name = name;
	}
	
	/**
	 * {@inheritDoc}
	 * 
	 * @return The name of this ElementVariable.
	 */
	@Override
	public String asText() {
		return name;
	}
	
	/**
	 * @return The name of this ElementVariable.
	 */
	public String getName() {
		return asText();
	}
	
}
