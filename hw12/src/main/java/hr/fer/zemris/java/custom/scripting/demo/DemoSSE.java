package hr.fer.zemris.java.custom.scripting.demo;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import hr.fer.zemris.java.custom.scripting.exec.SmartScriptEngine;
import hr.fer.zemris.java.custom.scripting.parser.SmartScriptParser;
import hr.fer.zemris.java.webserver.RequestContext;
import hr.fer.zemris.java.webserver.RequestContext.RCCookie;

/**
 * DemoSSE is a simple demonstration program for our implementation
 * of a {@link SmartScriptEngine}.
 * 
 * @author Miroslav Bićanić
 */
public class DemoSSE {
	
	private static final String ONE = "osnovni.smscr";
	private static final String TWO = "zbrajanje.smscr";
	private static final String THREE = "brojPoziva.smscr";
	private static final String FOUR = "fibonacci.smscr";
	private static final String FIVE = "fibonaccih.smscr";
	
	/**
	 * Entry point to the program
	 * @param args
	 * @throws IOException
	 */
	public static void main(String[] args) throws IOException {
		execute(ONE);
	}

	/**
	 * Exectues one of the five defined SmartScript scripts
	 * depending on the given {@code file} argument.
	 * 
	 * @param file the script to execute
	 * @throws IOException
	 */
	private static void execute(String file) throws IOException {
		String docBody = Files.readString(Paths.get("webroot/scripts/"+file), StandardCharsets.UTF_8);
		Map<String, String> parameters = new HashMap<>();
		Map<String, String> persParam = new HashMap<>();
		List<RCCookie> cookies = new ArrayList<>();
		OutputStream os = System.out;
		RequestContext rc = null;
		switch(file) {
		case TWO:
			parameters.put("a", "4");
			parameters.put("b", "2");
			break;
		case THREE:
			persParam.put("brojPoziva", "3");
			break;
		case FIVE:
			os = Files.newOutputStream(Paths.get("fibo.html"));
			break;
		}
		rc = new RequestContext(os, parameters, persParam, cookies);
		new SmartScriptEngine(
				new SmartScriptParser(docBody).getDocumentNode(),
				rc
			).execute();
		if(file.equals(THREE)) {
			System.out.println("Vrijednost u mapi: "+rc.getPersistentParameter("brojPoziva"));
		}
		os.close();
	}
}
