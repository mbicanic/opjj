package hr.fer.zemris.java.custom.scripting.exec;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.Stack;
import java.util.function.BiConsumer;
import java.util.function.BiFunction;
import java.util.function.Consumer;

import hr.fer.zemris.java.custom.scripting.elems.Element;
import hr.fer.zemris.java.custom.scripting.elems.ElementConstantDouble;
import hr.fer.zemris.java.custom.scripting.elems.ElementConstantInteger;
import hr.fer.zemris.java.custom.scripting.elems.ElementFunction;
import hr.fer.zemris.java.custom.scripting.elems.ElementOperator;
import hr.fer.zemris.java.custom.scripting.elems.ElementString;
import hr.fer.zemris.java.custom.scripting.elems.ElementVariable;
import hr.fer.zemris.java.custom.scripting.nodes.DocumentNode;
import hr.fer.zemris.java.custom.scripting.nodes.EchoNode;
import hr.fer.zemris.java.custom.scripting.nodes.ForLoopNode;
import hr.fer.zemris.java.custom.scripting.nodes.INodeVisitor;
import hr.fer.zemris.java.custom.scripting.nodes.TextNode;
import hr.fer.zemris.java.custom.scripting.parser.SmartScriptParser;
import hr.fer.zemris.java.webserver.RequestContext;

/**
 * SmartScriptEngine is an engine used to execute scripts written
 * in SmartScript.
 * 
 * The engine needs a {@link DocumentNode} representing the structure
 * of the script to execute, meaning a script must first be parsed by
 * a {@link SmartScriptParser} to obtain a {@link DocumentNode}.
 * A {@link RequestContext} is also required so that the execution of 
 * the script is outputted to it.
 * 
 * The engine executes the script following these rules: 
 * <ol>
 * <li> The content of {@link TextNode} objects is forwarded to the
 * 		{@link RequestContext} without changes </li>
 * <li> {@link EchoNode} objects contain various {@link Element} objects
 * 		which determine how to execute that particular EchoNode. The 
 * 		contents can be variables, constants (string, int, double) and
 * 		functions. A list of functions as well as how they work is given
 * 		below. </li>
 * <li> All nodes within a {@link ForLoopNode} are executed exactly as 
 * 		many times as it is specified in the {@link ForLoopNode}. </li>
 * </ol>
 * 
 * {@link EchoNode} elements can also be simple operators (+, -, *, /), 
 * which are executed in prefix notation, just like functions, meaning
 * a stack is used to store values, perform operations and execute 
 * functions.
 * </br>
 * Supported functions are: <ul>
 * <li> paramGet(name, defValue) - puts the value of the parameter with the given name
 * 			from the {@link RequestContext} on the stack. If the parameter does not exist,
 * 			the default value is used 														</li>
 * <li> pparamGet(name, defValue) - same as paramGet, but for persistent parameters 		</li>
 * <li> tparamGet(name, defValue) - same as paramGet, but for temporary parameters 			</li>
 * <li> pparamSet(name, value) - stores a value into the persistent parameters of the
 * 			{@link RequestContext}. 														</li>
 * <li> tparamSet(name, value) - same as pparamSet, but for temporary parameters	 		</li>
 * <li> pparamDel(name) - removes association for name from persistent parameters of
 * 			the {@link RequestContext} 														</li>
 * <li> tparamDel(name) - same as pparamDel, but for temporary parameters 					</li>
 * <li> sin(x) - calculates the sine of given argument 										</li>
 * <li> decfmt(x, f) - formats decimal number x using the given format f, which must be
 * 			compatible with {@link DecimalFormat}. x can be a {@link String}, {@link Double}
 * 			or {@link Integer} representation of a number. 									</li>
 * <li> dup() - duplicates the last value on the stack 										</li>
 * <li> swap() - swaps positions of last two elements on the stack 							</li>
 * <li> setMimeType(x) - sets the mime type of the {@link RequestContex} to the given mime 
 * 			type		 																	</li>
 * </ul>
 * 
 * All objects that remain on stack after execution of an {@link EchoNode} are outputted
 * to the {@link RequestContext}. For example: <code> {$= 1.57 @sin 1 + $} </code> will
 * calculate {@code sin(1.57)} and then perform {@code sin(1.57) + 1} which will leave
 * number 2 on the stack, and that will be outputted to the {@link RequestContext}. 
 * 
 * @author Miroslav Bićanić
 */
public class SmartScriptEngine {
			
	/** Map of biconsumers used to execute functions related to {@link RequestContext} */
	private static final Map<String, BiConsumer<RequestContext, Stack<Object>>> BICONSUMERS;
	
	/** Map of consumers used to execute all remaining functions */
	private static final Map<String, Consumer<Stack<Object>>> CONSUMERS;
	
	/** Map of functions representing simple mathematical operators */
	private static final Map<String, BiFunction<ValueWrapper, Object, Object>> OPS;
	
	/**
	 * Sets up all supported operators, functions operating with the {@link RequestContext}
	 * and functions operating only with the internal stack.
	 */
	static {
		OPS = new HashMap<>();
		BICONSUMERS = new HashMap<>();
		CONSUMERS = new HashMap<>();
		
		OPS.put("+", (v1,v2)->{
			v1.add(v2);
			return v1.getValue();
		});
		OPS.put("-", (v1,v2)->{
			v1.sub(v2);
			return v1.getValue();
		});
		OPS.put("*", (v1,v2)->{
			v1.mul(v2);
			return v1.getValue();
		});
		OPS.put("/", (v1,v2)->{
			v1.div(v2);
			return v1.getValue();
		});
		
		BICONSUMERS.put("paramGet", (rc,s)->{
			Object def = s.pop();
			Object param = rc.getParameter(s.pop().toString());
			s.push(param==null ? def : param);
		});
		BICONSUMERS.put("pparamGet", (rc,s)->{
			Object def = s.pop();
			Object param = rc.getPersistentParameter(s.pop().toString());
			s.push(param==null ? def : param);
		});
		BICONSUMERS.put("tparamGet", (rc,s)->{
			Object def = s.pop();
			Object param = rc.getTemporaryParameter(s.pop().toString());
			s.push(param==null ? def : param);
		});
		BICONSUMERS.put("pparamSet", (rc, s)->{
			rc.setPersistentParameter(s.pop().toString(), s.pop().toString());
		});
		BICONSUMERS.put("tparamSet", (rc, s)->{
			rc.setTemporaryParameter(s.pop().toString(), s.pop().toString());
		});
		BICONSUMERS.put("pparamDel", (rc, s)-> rc.removePersistentParameter(s.pop().toString()));
		BICONSUMERS.put("tparamDel", (rc, s)-> rc.removeTemporaryParameter(s.pop().toString()));
		BICONSUMERS.put("setMimeType", (rc, s)-> {
			Object mime = s.pop();
			if(!(mime instanceof String)) {
				throw new IllegalArgumentException();
			}
			rc.setMimeType(mime.toString());
		});
		
		CONSUMERS.put("sin", (s)->{
			Object xObj = s.pop();
			Double xSin = null;
			try {
				xSin = Double.parseDouble(xObj.toString());
			} catch (NumberFormatException ex) {
				throw new IllegalArgumentException();
			}
			s.push(Math.sin(xSin*Math.PI/180));
		});
		CONSUMERS.put("decfmt", (s)->{
			Object fmt = s.pop();
			Object val = s.pop();
			s.push(decFormat(val, fmt));
		});
		CONSUMERS.put("dup", (s)-> s.push(s.peek()));
		CONSUMERS.put("swap", (s)->{
			Object a1 = s.pop();
			Object a2 = s.pop();
			s.push(a1);
			s.push(a2);
		});
	}
	
	/**
	 * Interprets the given {@code value} as a double number and formats
	 * it through the {@link DecimalFormat} class, using the given 
	 * {@code format}.
	 * 
	 * @param value the double value to format
	 * @param format the format in which to format the value
	 * @return the formatted value
	 */
	private static String decFormat(Object value, Object format) {
		if(!(format instanceof String)) {
			throw new IllegalArgumentException();
		}
		Double dValue = Double.parseDouble(value.toString());
		DecimalFormat df = new DecimalFormat(format.toString());
		return df.format(dValue);
	}
	
	/**
	 * The root {@link DocumentNode} of the script to be executed
	 * by the engine.
	 */
	private DocumentNode documentNode;
	/**
	 * The {@link RequestContext} used for communication, fetching and
	 * manipulation of parameters.
	 */
	private RequestContext requestContext;
	/**
	 * The {@link ObjectMultistack} used for storing variables and their
	 * values.
	 */
	private ObjectMultistack multistack = new ObjectMultistack();
	
	/**
	 * A constructor for a SmartScriptEngine
	 * @param documentNode the root node of the parsed document
	 * @param requestContext the {@link RequestContext} for communication
	 */
	public SmartScriptEngine(DocumentNode documentNode, RequestContext requestContext) {
		this.documentNode = documentNode;
		this.requestContext = requestContext;
	}
	
	/**
	 * Executes the script contained in this engine's
	 * {@link DocumentNode}
	 */
	public void execute() {
		documentNode.accept(executor);
	}
	
	/**
	 * An implementation of the {@link INodeVisitor} interface
	 * used for executing the script previously parsed into 
	 * nodes.
	 */
	private INodeVisitor executor = new INodeVisitor() {

		/**
		 * {@inheritDoc}
		 */
		@Override
		public void visitTextNode(TextNode node) {
			try {
				requestContext.write(node.getText());
			} catch (IOException e) {
				throw new RuntimeException(e.getMessage());
			}				
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public void visitForLoopNode(ForLoopNode node) {
			ElementVariable var = node.getVariable();
			int start = Integer.parseInt(node.getStartExpression().asText());
			int end = Integer.parseInt(node.getEndExpression().asText());
			int step = Integer.parseInt(node.getStepExpression().asText());
			
			ValueWrapper vw = new ValueWrapper(start);
			multistack.push(var.getName(), vw);
			while(!endLoop(var, end)) {
				for(int i=0, n=node.numberOfChildren(); i<n; i++) {
					distribute(node.getChild(i));
				}
				vw.add(step);
			}
			multistack.pop(var.getName());
		}

		/**
		 * Checks if the for loop iterator is larger than the
		 * end parameter of the for loop.
		 * 
		 * @param var the {@link ElementVariable} that is the iterator
		 * @param end the largest value the iterator can reach
		 * @return true if the loop should be ended; false otherwise
		 */
		private boolean endLoop(ElementVariable var, int end) {
			ValueWrapper vw = multistack.peek(var.getName());
			int i = (Integer)vw.getValue();
			return i>end;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public void visitEchoNode(EchoNode node) {
			Stack<Object> tempStack = new Stack<Object>();
			Element[] elems = node.getElements();
			for(Element e : elems) {
				if(e instanceof ElementConstantInteger) {
					tempStack.push(((ElementConstantInteger)e).getValue());
				} else if(e instanceof ElementConstantDouble) {
					tempStack.push(((ElementConstantDouble)e).getValue());
				} else if(e instanceof ElementString) {
					tempStack.push(((ElementString)e).getValue());
				} else if(e instanceof ElementVariable) {
					ElementVariable ev = (ElementVariable)e;
					ValueWrapper vw = multistack.peek(ev.getName());
					tempStack.push(vw.getValue());
					
				} else if(e instanceof ElementOperator) {
					ElementOperator eo = (ElementOperator)e;
					Object b = tempStack.pop();
					ValueWrapper a = new ValueWrapper(tempStack.pop()); //VW knows how to perform operations with all supported objects
					tempStack.push(OPS.get(eo.getSymbol()).apply(a, b));
					
				} else if(e instanceof ElementFunction) {
					ElementFunction ef = (ElementFunction)e;
					handleFunctions(tempStack, ef.asText());
				}
			}
			outputRemaining(tempStack);
		}

		/**
		 * Executes the method given by the {@code name} parameter using
		 * the values found on the given {@code tempStack}.
		 * 
		 * @param tempStack the stack containing values for the operation
		 * @param name the name of the operation to perform
		 */
		private void handleFunctions(Stack<Object> tempStack, String name) {
			if(BICONSUMERS.keySet().contains(name)) {
				BICONSUMERS.get(name).accept(requestContext, tempStack);
			} else if(CONSUMERS.keySet().contains(name)) {
				CONSUMERS.get(name).accept(tempStack);
			} else {
				throw new UnsupportedOperationException("SmartScript does not support function: "+name);
			}
		}
		
		/**
		 * Outputs the elements that remained on stack into the engine's
		 * {@link RequestContext}.
		 * 
		 * @param tempStack the stack containing remaining elements
		 */
		private void outputRemaining(Stack<Object> tempStack) {
			try {
				String[] elems = new String[tempStack.size()];
				for(int i=tempStack.size()-1; i>=0; i--) {
					elems[i] = tempStack.pop().toString();
				}
				requestContext.write(String.join("", elems));
			} catch(IOException e) {
				throw new RuntimeException(e.getMessage());
			}
		}
		
		/**
		 * {@inheritDoc}
		 */
		@Override
		public void visitDocumentNode(DocumentNode node) {
			for(int i=0, n=node.numberOfChildren(); i<n; i++) {
				distribute(node.getChild(i));
			}
			try {
				requestContext.write("\r\n");
			} catch (IOException e) {
				throw new RuntimeException(e.getMessage());
			}
		}
	};
}
