package hr.fer.zemris.java.webserver;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

/**
 * RequestContext models a context through which requests
 * are processed and responses are sent using the HTTP 
 * protocol.
 * 
 * It automatically generates a header for the HTTP response
 * using the parameters stored in it.
 * 
 * Cookies are supported, as well as storing the Session ID.
 * 
 * By default, it generates a header with the following content:
 * <pre>
 * HTTP/1.1 200 OK
 * Content-Type: text/html; charset=UTF-8
 * 
 * </pre>
 * 
 * @author Miroslav Bićanić
 *
 */
public class RequestContext {
	
	/**
	 * The stream to which to write received data
	 */
	private OutputStream outputStream;
	/**
	 * The charset to use and to write into the header
	 */
	private Charset charset;

	/** The encoding to use while writing to the {@link OutputStream} */
	private String encoding = "UTF-8";
	/** The status code of the server's response */
	private int statusCode = 200;
	/** The status text of the server's response */
	private String statusText = "OK";
	/** The mime type of the server's response */
	private String mimeType = "text/html";
	/** The length of the server's response's content */
	private Long contentLength = null;
	/** The session ID in which this RequestContext is generated */
	private String sessionID;
	
	/** The dispatcher for this RequestContext */
	private IDispatcher dispatcher;
	
	/** Parameters received from the HTTP request */
	private Map<String, String> parameters;
	/** Temporary parameters used for running scripts */
	private Map<String, String> temporaryParameters;
	/** Persistent parameters used for storing session data */
	private Map<String, String> persistentParameters;
	/** List of cookies to be generated in the header */
	private List<RCCookie> outputCookies;
	
	/** Flag telling if the header has been generated or not */
	private boolean headerGenerated = false;
	
	/**
	 * RCCookie is a class modelling a HTTP cookie, storing all
	 * the cookie's parameters.
	 * 
	 * @author Miroslav Bićanić
	 */
	public static class RCCookie {
		/** The name of the cookie */
		private String name;
		/** The value of the cookie */
		private String value;
		/** The domain at which the cookie has been generated */
		private String domain;
		/** The path this cookie will access */
		private String path;
		/** The maximum age of the user */
		private Integer maxAge;
		
		/**
		 * Constructor for an RCCookie
		 * @param name the name of the cookie 
		 * @param value the value of the cookie
		 * @param maxAge the maximum age of the user
		 * @param domain the domain at which the cookie has been generated
		 * @param path the path this cookie will access
		 */
		public RCCookie(String name, String value, Integer maxAge, String domain, String path) {
			this.name = name;
			this.value = value;
			this.domain = domain;
			this.path = path;
			this.maxAge = maxAge;
		}
	}

	/**
	 * Constructor for a RequestContext
	 * @param outputStream the {@link OutputStream} to which this RequestContext
	 * 						will write
	 * @param parameters the parameters sent through HTTP
	 * @param persistentParameters parameters related to the session in which the
	 * 						RequestContext is made
	 * @param outputCookies list of cookies related to the session in which the 
	 * 						RequestContext is made
	 */
	public RequestContext (
			OutputStream outputStream,
			Map<String, String> parameters,
			Map<String, String> persistentParameters,
			List<RCCookie> outputCookies) {
		this(outputStream, parameters, persistentParameters, outputCookies, null, null, null);
	}
	
	/**
	 * Constructor for a RequestContext, additionally taking a map of
	 * temporary parameters for running scripts, an {@link IDispatcher} 
	 * for dispatching the request and a session ID.
	 * 
	 * @param outputStream the {@link OutputStream} to which this RequestContext
	 * 						will write
	 * @param parameters the parameters sent through HTTP
	 * @param persistentParameters parameters related to the session in which the
	 * 						RequestContext is made
	 * @param outputCookies list of cookies related to the session in which the 
	 * 						RequestContext is made
	 * @param temporaryParameters parameters used for running scripts
	 * @param dispatcher a dispatcher to dispatch the request to other processors
	 * @parma sid the ID of the session in which the RequestContext has been generated
	 */
	public RequestContext (
			OutputStream outputStream,
			Map<String, String> parameters,
			Map<String, String> persistentParameters,
			List<RCCookie> outputCookies,
			Map<String, String> temporaryParameters,
			IDispatcher dispatcher,
			String sid) {
		
		this.outputStream = Objects.requireNonNull(outputStream);
		this.parameters = parameters==null ? new HashMap<>() : parameters;
		this.persistentParameters = persistentParameters==null ? new HashMap<>() : persistentParameters;
		this.temporaryParameters = temporaryParameters==null ? new HashMap<>() : temporaryParameters;
		this.outputCookies = outputCookies==null ? new ArrayList<>() : outputCookies;
		this.dispatcher = dispatcher;
		this.sessionID = sid;
	}
	
	/**
	 * @param encoding the encoding to be used by this RequestContext
	 * @throws RuntimeException if the header has already been generated
	 */
	public void setEncoding(String encoding) {
		checkHeaderGenerated();
		this.encoding = encoding;
	}
	/**
	 * @param statusCode the status code to be used by this RequestContext
	 * @throws RuntimeException if the header has already been generated
	 */
	public void setStatusCode(int statusCode) {
		checkHeaderGenerated();
		this.statusCode = statusCode;
	}
	/**
	 * @param statusText the status text to be used by this RequestContext
	 * @throws RuntimeException if the header has already been generated
	 */
	public void setStatusText(String statusText) {
		checkHeaderGenerated();
		this.statusText = statusText;
	}
	/**
	 * @param mimeType the mime type to be used by this RequestContext
	 * @throws RuntimeException if the header has already been generated
	 */
	public void setMimeType(String mimeType) {
		checkHeaderGenerated();
		this.mimeType = mimeType;
	}
	/**
	 * @param contentLength the length of the content to be sent by this RequestContext
	 * @throws RuntimeException if the header has already been generated
	 */
	public void setContentLength(Long contentLength) {
		checkHeaderGenerated();
		this.contentLength = contentLength;
	}
	/**
	 * Checks if the header has already been generated.
	 * @throws RuntimeException if the header has already been generated
	 */
	private void checkHeaderGenerated() {
		if(headerGenerated) {
			throw new RuntimeException("Cannot set properties after header has been generated.");
		}
	}
	/**
	 * @param name the name of the parameter to get
	 * @return the parameter associated with the given {@code name}
	 */
	public String getParameter(String name) {
		return parameters.get(name);
	}
	/**
	 * @return a set of names of all parameters registered in the RequestContext
	 */
	public Set<String> getParameterNames() {
		return Collections.unmodifiableSet(parameters.keySet());
	}
	/**
	 * @param name the name of the persistent parameter to get
	 * @return the persistent parameter associated with the given {@code name}
	 */
	public String getPersistentParameter(String name) {
		return persistentParameters.get(name);
	}
	/**
	 * @return a set of names of all persistent parameters registered in the RequestContext
	 */
	public Set<String> getPersistentParameterNames() {
		return Collections.unmodifiableSet(persistentParameters.keySet());
	}
	/**
	 * @param name the name of the persistent parameter to set
	 * @param value the value to assign to the persistent parameter
	 */
	public void setPersistentParameter(String name, String value) {
		persistentParameters.put(name, value);
	}
	/**
	 * @param name the name of the persistent parameter to removes
	 */
	public void removePersistentParameter(String name) {
		persistentParameters.remove(name);
	}
	/**
	 * @param name the name of the temporary parameter to get
	 * @return the temporary parameter associated with the given {@code name}
	 */
	public String getTemporaryParameter(String name) {
		return temporaryParameters.get(name);
	}
	/**
	 * @return a set of names of all temporary parameters registered in the RequestContext
	 */
	public Set<String> getTemporaryParameterNames() {
		return Collections.unmodifiableSet(temporaryParameters.keySet());
	}
	/**
	 * @param name the name of the temporary parameter to set
	 * @param value the value to assign to the temporary parameter
	 */
	public void setTemporaryParameter(String name, String value) {
		temporaryParameters.put(name, value);
	}
	/**
	 * @param name the name of the temporary parameter to removes
	 */
	public void removeTemporaryParameter(String name) {
		temporaryParameters.remove(name);
	}
	/**
	 * @return the ID of the session in which this RequestContext was generated
	 */
	public String getSessionID() {
		return sessionID;
	}
	/**
	 * @return the {@link IDispatcher} of this RequestContext
	 */
	public IDispatcher getDispatcher() {
		return dispatcher;
	}

	/**
	 * Writes the whole given {@code data} array into the {@link OutputStream} 
	 * of this RequestContext.
	 * 
	 * The method ensures that the header is written before any other
	 * data.
	 * 
	 * @param data the array of bytes to write into the output stream
	 * @return this RequestContext
	 * @throws IOException if an IOException occurs during writing
	 */
	public RequestContext write(byte[] data) throws IOException {
		return write(data, 0, data.length);
	}
	/**
	 * Writes a {@code len} character long segment of the given {@code data} 
	 * array from position {@code offset} (inclusive) into the {@link OutputStream} 
	 * of this RequestContext.
	 * 
	 * The method ensures that the header is written before any other
	 * data.
	 * 
	 * @param data the array of bytes to write into the output stream
	 * @param offset the position in the array from which to start writing
	 * @param len the length of the segment to write
	 * @return this RequestContext
	 * @throws IOException if an IOException occurs during writing
	 */
	public RequestContext write(byte[] data, int offset, int len) throws IOException {
		if(!headerGenerated) {
			writeHeader();
		}
		outputStream.write(data, offset, len);
		return this;
	}
	
	/**
	 * Writes the given {@code text} String into the {@link OutputStream} 
	 * of this RequestContext, using the charset defined in this RequestContext
	 * 
	 * The method ensures that the header is written before any other
	 * data.
	 * 
	 * @param data the array of bytes to write into the output stream
	 * @return this RequestContext
	 * @throws IOException if an IOException occurs during writing
	 */
	public RequestContext write(String text) throws IOException {
		if(!headerGenerated) {
			writeHeader(); //to ensure charset is set first time
		}
		return write(text.getBytes(this.charset));
	}
	
	/**
	 * Writes the header of the HTTP response to the {@link OutputStream}.
	 * 
	 * @throws IOException if writing was unsuccessful
	 */
	private void writeHeader() throws IOException {
		charset = Charset.forName(encoding);
		StringBuilder header = new StringBuilder(100);
		header.append("HTTP/1.1 "+statusCode+" "+statusText+"\r\n");
		header.append("Content-Type: ").append(mimeType);
		if(mimeType.startsWith("text/")) {
			header.append("; charset=").append(encoding);
		}
		header.append("\r\n");
		if(contentLength!=null) {
			header.append("Content-Length: ").append(contentLength).append("\r\n");
		}
		if(!outputCookies.isEmpty()) {
			for(RCCookie rc : outputCookies) {
				header.append(generateLine(rc));
			}
		}
		header.append("\r\n");
		headerGenerated=true;
		write(header.toString());
	}
	
	/**
	 * Creates a line representing the received {@code rcc} 
	 * {@link RCCookie}.
	 * 
	 * @param rcc the {@link RCCookie} to transform into a line of text
	 * @return the string representing the given {@link RCCookie}
	 */
	private String generateLine(RCCookie rcc) {
		StringBuilder inter = new StringBuilder(30);
		inter.append("Set-Cookie: ");
		inter.append(rcc.name).append("=\"").append(rcc.value).append("\"");
		
		Object[] params = new Object[] {rcc.domain, rcc.path, rcc.maxAge};
		String[] leftSide = new String[] {"Domain=", "Path=", "Max-Age="};
		for(int i=0; i<3; i++) {
			if(params[i]!=null) {
				inter.append("; ").append(leftSide[i]).append(params[i]);
			}
		}
		inter.append("; HttpOnly\r\n");
		return inter.toString();
	}
	
	/**
	 * @param cookie the {@link RCCookie} to add to this RequestContext
	 */
	public void addRCCookie(RCCookie cookie) {
		this.outputCookies.add(cookie);
	}
}
