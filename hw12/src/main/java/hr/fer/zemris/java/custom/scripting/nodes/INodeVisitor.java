package hr.fer.zemris.java.custom.scripting.nodes;

/**
 * Interface for implementing the visitor design pattern
 * over {@link Node} objects.
 * 
 * @author Miroslav Bićanić
 */
public interface INodeVisitor {
	/**
	 * Action to perform when a {@link TextNode} is visited
	 * @param node the visited {@link TextNode}
	 */
	void visitTextNode(TextNode node);
	/**
	 * Action to perform when a {@link ForLoopNode} is visited
	 * @param node the visited {@link ForLoopNode}
	 */
	void visitForLoopNode(ForLoopNode node);
	/**
	 * Action to perform when a {@link EchoNode} is visited
	 * @param node the visited {@link EchoNode}
	 */
	void visitEchoNode(EchoNode node);
	/**
	 * Action to perform when a {@link DocumentNode} is visited
	 * @param node the visited {@link DocumentNode}
	 */
	void visitDocumentNode(DocumentNode node);
	
	/**
	 * Calls the correct visitor operation depending on the type
	 * of {@link Node} given.
	 * 
	 * @param child the node to distribute to one of the visitor methods
	 */
	default void distribute(Node child) {
		if(child instanceof ForLoopNode) {
			this.visitForLoopNode((ForLoopNode)child);
		} else if(child instanceof EchoNode) {
			this.visitEchoNode((EchoNode)child);
		} else if(child instanceof TextNode) {
			this.visitTextNode((TextNode)child);
		} else if(child instanceof DocumentNode) {
			this.visitDocumentNode((DocumentNode)child);
		}
	}
}
