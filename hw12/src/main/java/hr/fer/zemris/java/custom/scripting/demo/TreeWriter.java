package hr.fer.zemris.java.custom.scripting.demo;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

import hr.fer.zemris.java.custom.scripting.nodes.DocumentNode;
import hr.fer.zemris.java.custom.scripting.nodes.EchoNode;
import hr.fer.zemris.java.custom.scripting.nodes.ForLoopNode;
import hr.fer.zemris.java.custom.scripting.nodes.INodeVisitor;
import hr.fer.zemris.java.custom.scripting.nodes.TextNode;
import hr.fer.zemris.java.custom.scripting.parser.SmartScriptParser;

/**
 * A demonstration program of our implementation of an 
 * {@link INodeVisitor} used to reconstruct a document
 * body from a parsed document.
 * 
 * @author Miroslav Bićanić
 */
public class TreeWriter {
	
	/**
	 * Entry point of the program
	 * @param args one command line argument: path to document to be parsed
	 */
	public static void main(String[] args) {
		if(args.length!=1) {
			System.err.println("Expected 1 argument.");
			return;
		}
		String docBody;
		try {
			docBody = new String(Files.readAllBytes(Paths.get(args[0])), StandardCharsets.UTF_8);
		} catch (IOException e) {
			System.err.println("File could not be read");
			return;
		}
		SmartScriptParser p = new SmartScriptParser(docBody);
		WriterVisitor visitor = new WriterVisitor();
		p.getDocumentNode().accept(visitor);
	}
	
	/**
	 * Implementation of an {@link INodeVisitor} visiting all nodes
	 * in a document tree and reconstructing the document body.
	 * 
	 * @author Miroslav Bićanić
	 */
	private static class WriterVisitor implements INodeVisitor {
		@Override
		public void visitTextNode(TextNode node) {
			System.out.printf(node.toString());
		}

		@Override
		public void visitForLoopNode(ForLoopNode node) {
			System.out.printf(node.toString());
			int n = node.numberOfChildren();
			for(int i=0; i<n; i++) {
				distribute(node.getChild(i));
			}
			System.out.printf("{$ END $}");
		}

		@Override
		public void visitEchoNode(EchoNode node) {
			System.out.printf(node.toString());
		}

		@Override
		public void visitDocumentNode(DocumentNode node) {
			for(int i=0, n=node.numberOfChildren(); i<n; i++) {
				distribute(node.getChild(i));
			}	
		}
	}
}
