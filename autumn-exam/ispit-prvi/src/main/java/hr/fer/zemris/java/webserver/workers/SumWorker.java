package hr.fer.zemris.java.webserver.workers;

import hr.fer.zemris.java.webserver.IWebWorker;
import hr.fer.zemris.java.webserver.RequestContext;

/**
 * SumWorker is an implementation of an {@link IWebWorker} interface,
 * taking two Integer parameters and adding them. </br>
 * 
 * The result, the parameters and additionally an image name parameter
 * are stored into the temporary parameters of the {@link RequestContext}
 * before the processing is dispatched to a script generating a HTML with
 * a table showing the parameters and their sum, as well as the photo
 * whose name is stored in the temporary parameters.
 * 
 * @author Miroslav Bićanić
 */
public class SumWorker implements IWebWorker {

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void processRequest(RequestContext context) throws Exception {
		String aStr = context.getParameter("a");
		String bStr = context.getParameter("b");
		Integer a = getInteger(aStr, 1);
		Integer b = getInteger(bStr, 2);
		Integer c = a+b;
		context.setTemporaryParameter("zbroj", c.toString());
		context.setTemporaryParameter("varA", a.toString());
		context.setTemporaryParameter("varB", b.toString());
		context.setTemporaryParameter("imgName", c%2==0 ? "even.png" : "odd.png");
		context.getDispatcher().dispatchRequest("/private/pages/calc.smscr");
	}

	/**
	 * Tries to parse the given {@code str} into an {@link Integer}.
	 * In case the given string is null, or cannot be parsed into 
	 * an {@link Integer}, the default value {@code i} is used.
	 * 
	 * @param str the string to parse into an {@link Integer}
	 * @param i the default value to use
	 * @return the parsed {@link Integer} or the default value
	 */
	private Integer getInteger(String str, int i) {
		try {
			return str==null ? i : Integer.parseInt(str);
		} catch (NumberFormatException ex) {
			return i;
		}
	}
}
