package hr.fer.zemris.java.webserver;

import java.io.IOException;

/**
 * Interface for objects that dispatch HTTP requests for further
 * processing.
 * 
 * @author Miroslav Bićanić
 */
public interface IDispatcher {
	
	/**
	 * Dispatches processing of a request
	 * @param urlPath the url path determining the request
	 * @throws IOException if an exception during dispatching occurs
	 */
	void dispatchRequest(String urlPath) throws IOException;
}
