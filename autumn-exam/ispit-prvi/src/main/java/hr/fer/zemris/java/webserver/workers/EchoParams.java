package hr.fer.zemris.java.webserver.workers;

import hr.fer.zemris.java.webserver.IWebWorker;
import hr.fer.zemris.java.webserver.RequestContext;

/**
 * Implementation of a {@link IWebWorker} interface that outputs
 * a HTML table containing all parameters sent through the HTTP
 * request in the {@code | name | value |} format to the
 * {@link RequestContext} as a HTML document.
 * 
 * @author Miroslav Bićanić
 */
public class EchoParams implements IWebWorker {

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void processRequest(RequestContext context) throws Exception {
		StringBuilder sb = new StringBuilder(300);
		sb.append("<html><head><title>Table of parameters</title></head><body>");
		sb.append("<table border=1>");
		for(String name : context.getParameterNames()) {
			sb.append("<tr><td>"+name+"</td><td>");
			sb.append(context.getParameter(name)+"</td></tr>");
		}
		sb.append("</table></body></html>");
		context.setMimeType("text/html");
		context.setStatusCode(200);
		context.write(sb.toString());
	}
}
