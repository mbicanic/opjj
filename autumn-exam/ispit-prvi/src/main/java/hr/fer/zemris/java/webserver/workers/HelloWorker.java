package hr.fer.zemris.java.webserver.workers;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import hr.fer.zemris.java.webserver.IWebWorker;
import hr.fer.zemris.java.webserver.RequestContext;

/**
 * HelloWorker is an implementation of an {@link IWebWorker} that
 * outputs a simple HTML document containing a message that depends
 * on whether a "name" argument has been sent through the HTTP request
 * or not.
 * </br>
 * If the name has been sent, it outputs the number of letters in 
 * the name.
 * 
 * @author Miroslav Bićanić
 */
public class HelloWorker implements IWebWorker {

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void processRequest(RequestContext context) throws Exception {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date now = new Date();
		
		context.setMimeType("text/html");
		
		String name = context.getParameter("name");
		try {
			context.write("<html><body>");
			context.write("<h1>Hello!!!</h1>");
			context.write("<p>Now is: "+sdf.format(now)+"</p>");
			if(name==null) {
				context.write("<p>You did not send me your name!</p>");
			} else {
				context.write("<p>Your name has "+name.trim().length()+" letters.</p>");
			}
			context.write("</body></html>");
		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}

}
