package hr.fer.zemris.java.custom.scripting.exec;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * ObjectMultistack is a stack-like structure, providing
 * the user with multiple virtual stacks, accessible by
 * the key of the stack.
 * 
 * Neither the key nor the value to be placed on stack are
 * allowed to be null.
 * 
 * @author Miroslav Bićanić
 */
public class ObjectMultistack {
	
	/** The internal map mapping stack keys to their actual implementations */
	private Map<String, MultistackEntry> multistack;
	
	/**
	 * A default constructor for an ObjectMultistack
	 */
	public ObjectMultistack() {
		this.multistack = new HashMap<String, MultistackEntry>();
	}
	
	/**
	 * MultistackEntry is a model of one virtual named stack
	 * in the ObjectMultistack.
	 * 
	 * It is implemented as a linked list, where each stack is
	 * represented by the node that is the top of the stack,
	 * and each stack element is linked to the stack element 
	 * below it.
	 * 
	 * @author Miroslav Bićanić
	 */
	private static class MultistackEntry {
		/** The value contained in this node of the virtual stack */
		private ValueWrapper value;
		/** The pointer to the next element, going from the top of stack to the bottom */
		private MultistackEntry next;
		
		/**
		 * A constructor for a MultistackEntry
		 * @param val The initial value for the MultistackEntry
		 */
		private MultistackEntry(ValueWrapper val) {
			this.value = val;
		}
	}
	
	/**
	 * Places the given {@code valueWrapper} on top of the stack whose
	 * key is the given {@code key}. <br><br>
	 * 
	 * If a stack with the given {@code key} does not exist, it is
	 * created and the given {@code valueWrapper} is pushed as the 
	 * first element.
	 * 
	 * @param key The key of the stack on top of which to push the given value
	 * @param valueWrapper The value to place on top of the stack
	 * @throws NullPointerException if given key or value wrapper are null
	 */
	public void push(String key, ValueWrapper valueWrapper) {
		Objects.requireNonNull(key, "Given key must not be null!");
		Objects.requireNonNull(valueWrapper, "Given ValueWrapper must not be null!");
		MultistackEntry newEntry = new MultistackEntry(valueWrapper);
		MultistackEntry head = multistack.get(key);
		
		newEntry.next = head;
		multistack.put(key, newEntry);
	}
	
	/**
	 * Returns the element from the top of the stack with the
	 * given {@code key}, removing that element from the stack.
	 * 
	 * @param key The key of the stack from which to pop
	 * @return The element from the top of the stack
	 * @throws EmptyMultistackException if the stack with the given key is empty
	 * @throws NullPointerException if the given stack key is null
	 */
	public ValueWrapper pop(String key) {
		MultistackEntry mse = getStackTop(key);
		multistack.put(key, mse.next);
		return mse.value;
	}
	
	/**
	 * Returns the element from the top of the stack with the
	 * given {@code key}, but not removing it from the stack.
	 * 
	 * @param key The key of the stack from which to peek
	 * @return The element from the top of the stack
	 * @throws EmptyMultistackException if the stack with the given key is empty
	 * @throws NullPointerException if the given stack key is null
	 */
	public ValueWrapper peek(String key) {
		return getStackTop(key).value;
	}
	
	/**
	 * Returns the MultistackEntry representing the top of one 
	 * virtual stack under the given {@code key}.
	 * 
	 * @param key The key of the stack to retrieve
	 * @return The stack under the given key
	 * @throws EmptyMultistackException if the stack with the given key
	 * 			does not exist
	 * @throws NullPointerException if the given stack key is null
	 */
	private MultistackEntry getStackTop(String key) {
		Objects.requireNonNull(key, "Stack key cannot be null!");
		MultistackEntry mse = multistack.get(key);
		if(mse==null) {
			throw new EmptyMultistackException("The stack with the key '"+key+"' is empty!");
		}
		return mse;
	}
	
	/**
	 * Tells whether the stack under the given key is empty.
	 * @param key The key of the stack to check
	 * @return true if the stack is empty, false otherwise
	 * @throws NullPointerException if given stack key is null
	 */
	public boolean isEmpty(String key) {
		Objects.requireNonNull(key, "Stack key cannot be null!");
		return multistack.get(key) == null;
	}
}
