package hr.fer.zemris.java.custom.scripting.elems;

import hr.fer.zemris.java.custom.scripting.lexer.SmartTokenType;

/**
 * ElementFunction is an element of an expression that
 * represents a function. 
 * 
 * @see SmartTokenType
 * @author Miroslav Bićanić
 */
public class ElementFunction extends Element {
	/**
	 * The name of this function (e.g. sin, cos, decfmt..)
	 */
	private String name;
	
	/**
	 * A constructor for ElementFunction.
	 * 
	 * @param name The name of the function assigned to this ElementFunction.
	 */
	public ElementFunction(String name) {
		this.name = name;
	}
	
	/**
	 * {@inheritDoc}
	 * 
	 * @return The name of the function.
	 */
	@Override
	public String asText() {
		return name;
	}
	/**
	 * {@inheritDoc}
	 * 
	 * It also adds a '@' symbol in front of the name of 
	 * the function, to mark it as a function.
	 */
	@Override
	public String toString() {
		return "@"+asText()+" ";
	}
	/**
	 * @return The name of the function
	 */
	public String getName() {
		return asText();
	}
	
}
