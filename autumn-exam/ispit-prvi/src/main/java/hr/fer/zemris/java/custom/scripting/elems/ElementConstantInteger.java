package hr.fer.zemris.java.custom.scripting.elems;

/**
 * ElementConstantInteger is an element of an expression
 * that can be interpreted as a integer constant.
 * 
 * @author Miroslav Bićanić
 */
public class ElementConstantInteger extends Element {
	
	/**
	 * The integer value of this ElementConstantInteger.
	 */
	private int value;
	
	/**
	 * A constructor for ElementConstantInteger.
	 * 
	 * @param value The value assigned to this ElementConstantInteger.
	 */
	public ElementConstantInteger(int value) {
		this.value = value;
	}
	
	/**
	 * {@inheritDoc}
	 * 
	 * @return The string representation of this Element's integer value.
	 */
	@Override
	public String asText() {
		return Integer.toString(value);
	}
	/**
	 * @return The integer value of this element.
	 */
	public int getValue() {
		return value;
	}
	
}
