	package hr.fer.zemris.java.webserver;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PushbackInputStream;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.function.BiConsumer;

import hr.fer.zemris.java.custom.scripting.exec.SmartScriptEngine;
import hr.fer.zemris.java.custom.scripting.parser.SmartScriptParser;
import hr.fer.zemris.java.webserver.RequestContext.RCCookie;
import hr.fer.zemris.java.webserver.workers.BgColorWorker;
import hr.fer.zemris.java.webserver.workers.CircleWorker;
import hr.fer.zemris.java.webserver.workers.EchoParams;
import hr.fer.zemris.java.webserver.workers.HelloWorker;
import hr.fer.zemris.java.webserver.workers.Home;
import hr.fer.zemris.java.webserver.workers.SumWorker;

/**
 * SmartHttpServer is a simple web-server utilizing the HTTP 1.1
 * protocol.
 * </br>
 * The server is set up using a {@code properties} configuration file
 * given to the server at construction.
 * The properties that must be included are: {@code server.address, 
 * server.domainName, server.port, server.workerThreads, server.mimeConfig,
 * session.timeout} and {@code server.workers}. </br>
 * The {@code server.mimeConfig} and {@code server.workers} must point
 * to separate property files for defining mime types and supported
 * workers.
 * </br>
 * The server maintains a single thread taking requests, distributing
 * the requests to a thread pool containing as many threads as defined
 * in the properties file.
 * </br>
 * Several actions of the server are mapped to the in the {@code worker.properties}
 * file for a faster access with the following syntax:
 * {@code http://domainName:port/<URL>}, where the URL parameter can be:<ul>
 * 	<li>{@code cw} - starts a {@link CircleWorker} </li>
 * 	<li>{@code hello} - starts a {@link HelloWorker} </li>
 * 	<li>{@code calc} - starts a {@link SumWorker} </li>
 * 	<li>{@code index2.html} - starts a {@link Home} worker </li>
 * 	<li>{@code setbgcolor} - starts a {@link BgColorWorker} </li> </ul>
 * 
 * Alternatively, workers can be called by using {@code /ext/<WORKER>} as the
 * URL parameter. The WORKER parameter is the name of the worker class.
 * One worker - {@link EchoParams} - is only runnable in such a way.</br>
 * {@link EchoParams}, {@link SumWorker}, {@link BgColorWorker} and {@link HelloWorker}
 * can all take various parameters.
 * </br>
 * Finally, using {@code /script/<SCRIPT>} as the URL, with SCRIPT being
 * one of the available scripts on the server, will execute the script and
 * display it. Currently supported scripts are: <ul>
 * 	<li> osnovni.smscr </li>
 * 	<li> zbrajanje.smscr </li>
 * 	<li> brojPoziva.smscr </li>
 * 	<li> fibonacci.smscr </li>
 * 	<li> fibonaccih.smscr </li> </ul>
 * 
 * @author Miroslav Bićanić
 */
public class SmartHttpServer {
	/** The address of the server */
	private String address;
	/** The domain name of the server */
	private String domainName;
	/** The port of the server */
	private int port;
	/** The number of worker threads that will process requests */
	private int workerThreads;
	/** Time until a new session expires, expressed in seconds */
	private int sessionTimeout;
	
	/** All sessions registered in this server, accessed by the session ID */
	private Map<String, SessionMapEntry> sessions = new HashMap<>();
	/** A Random generator for generating pseudo-random session IDs */
	private Random sessionRandom = new Random();
	/** A map of {@link IWebWorker} objects, accessed by a specific url path */
	private Map<String, IWebWorker> workersMap = new HashMap<>();
	/** 
	 * A map containing all mime types supported by the server, accessed
	 * by their extension.
	 */
	private Map<String, String> mimeTypes = new HashMap<>();
	/** The main thread running the server */
	private ServerThread serverThread;
	/** A pool of threads for processing requests */
	private ExecutorService threadPool;
	/** The root folder of all documents on the server */
	private Path documentRoot;
	
	/**
	 * Constructor for a SmartHttpServer
	 * 
	 * @param configFile the path to a configuration file to set up this server
	 */
	public SmartHttpServer(String configFile) {
		PropertyFetch pf  = new PropertyFetch(Paths.get(configFile));
		this.address = pf.fetch("server.address");
		this.domainName = pf.fetch("server.domainName");
		this.port = Integer.parseInt(pf.fetch("server.port"));
		this.workerThreads = Integer.parseInt(pf.fetch("server.workerThreads"));
		this.documentRoot = Paths.get(pf.fetch("server.documentRoot"));
		this.sessionTimeout = Integer.parseInt(pf.fetch("session.timeout"));
		
		PropertyFetch mimePF = new PropertyFetch(Paths.get(pf.fetch("server.mimeConfig")));
		this.mimeTypes.putAll(mimePF.allEntries());
		
		Map<String,String> workerLines = new PropertyFetch(Paths.get(pf.fetch("server.workers"))).allEntries();
		for(Map.Entry<String, String> e : workerLines.entrySet()) {
			IWebWorker iww = extractWebWorker(e.getValue());
			workersMap.put(e.getKey(), iww);
		}
		startSessionCleaner();
	}

	/**
	 * Using the given {@code fqcn} fully qualified class name, creates
	 * that class, assuming it to be an implementation of the {@link IWebWorker}
	 * interface.
	 * 
	 * @param fqcn the fully qualified class name of the class to create
	 * @return an instance of the class
	 * @throws RuntimeException if the class could not have been instantiated
	 */
	private IWebWorker extractWebWorker(String fqcn) {
		try {
			Class<?> reference = this.getClass().getClassLoader().loadClass(fqcn);
			Object temp = reference.getDeclaredConstructor().newInstance();
			return (IWebWorker) temp;
		} catch (Exception ex) {
			throw new RuntimeException(ex.getMessage());
		}
	}
	
	/**
	 * Starts a daemonic thread which removes all expired sessions
	 * from the sessions map, doing so periodically every 5 minutes.
	 */
	private void startSessionCleaner() {
		Thread t = new Thread(()-> {
			while(true) {
				try {
					Thread.sleep(300*1000);
				} catch (InterruptedException ignored) {}
				Iterator<Map.Entry<String, SessionMapEntry>> it = sessions.entrySet().iterator();
				while(it.hasNext()) {
					SessionMapEntry sme = it.next().getValue();
					if(sme.validUntil < System.currentTimeMillis()) {
						it.remove();
					}
				}
			}
		});
		t.setDaemon(true);
		t.start();
	}

	/**
	 * Creates a thread pool with a fixed number of threads,
	 * and starts the {@link ServerThread}.
	 */
	protected synchronized void start() {
		threadPool = Executors.newFixedThreadPool(workerThreads);
		if(serverThread==null) {
			serverThread = new ServerThread();
		}
		serverThread.start();
	}
	
	/**
	 * Stops the {@link ServerThread} from running and shuts down
	 * the {@link ExecutorService} thread pool.
	 */
	protected synchronized void stop() {
		serverThread.stop=true;
		threadPool.shutdown();
	}
	
	/**
	 * ServerThread is an implementation of the main thread
	 * running the server and submitting requests to the thread
	 * pool.
	 * 
	 * @author Miroslav Bićanić
	 */
	protected class ServerThread extends Thread {
		/**
		 * A flag to signal the server thread that it should stop
		 * accepting requests.
		 */
		private volatile boolean stop = false;

		@Override
		public void run() {
			try {
				@SuppressWarnings("resource")
				ServerSocket socket = new ServerSocket();
				socket.bind(new InetSocketAddress(
						(InetAddress)null, port));
				while(!stop) {
					Socket client = socket.accept();
					ClientWorker cw = new ClientWorker(client);
					threadPool.submit(cw).get();
				}
			} catch (IOException | InterruptedException | ExecutionException e) {
				e.printStackTrace();
			}	
		}
	}
	
	/**
	 * SessionMapEntry is all the data that defines a session,
	 * including its session ID, host name and time until it's
	 * valid.
	 * 
	 * @author Miroslav Bićanić
	 */
	private static class SessionMapEntry {
		String sid;
		String host;
		long validUntil;
		Map<String, String> map;
		
		/**
		 * Constructor for a SessionMapEntry
		 * @param sid the session ID
		 * @param host the host at which the session exists
		 * @param validUntil the time in milliseconds when the session will expire
		 * @param map a map of other parameters for the session
		 */
		public SessionMapEntry(String sid, String host, long validUntil, ConcurrentHashMap<String, String> map) {
			this.sid = sid;
			this.host = host;
			this.validUntil = validUntil;
			this.map = map;
		}
	}

	/**
	 * ClientWorker is a model of a worker capable of processing
	 * a request received from a {@link Socket}.
	 * 
	 * @author Miroslav Bićanić
	 */
	private class ClientWorker implements Runnable, IDispatcher {
		/** The socket through which the worker received a request */
		private Socket csocket;
		/** The input stream from the socket */
		private PushbackInputStream istream;
		/** The output stream of the soket */
		private OutputStream ostream;
		/** The version of the HTTP protocol used */
		private String version;
		/** The method used by the HTTP protocol */
		private String method;
		/** The host at which the processing is done */
		private String host;
		/** 
		 * The {@link RequestContext} used to represent and execute
		 * the request.
		 */
		private RequestContext context = null;
		
		/** A map of parameters received through the HTTP request */
		private Map<String,String> params = new HashMap<>();
		/** A map of temporary parameters used for execution of scripts */
		private Map<String,String> tempParams = new HashMap<>();
		/** A map of permanent parameters used for storing session parameters */
		private Map<String,String> permParams = new HashMap<>();
		/** A list of {@link RCCookie} cookies to be generated by the server */
		private List<RCCookie> outputCookies = new ArrayList<>();
		
		/**
		 * An implementation of a {@link BiConsumer} that extracts an {@link IWebWorker}
 		 * from the given path and calls its {@link IWebWorker#processRequest(RequestContext)}
		 * method using the {@code rc} {@link RequestContext}
		 */
		private final BiConsumer<String, RequestContext> EXTRACT = (path, rc) ->{
			try {
				extractWebWorker(path).processRequest(rc);
			} catch (Exception e) {
				e.printStackTrace();
			}
		};
		/**
		 * An implementation of a {@link BiConsumer} that retrieves an {@link IWebWorker}
		 * from the {@link SmartHttpServer#workersMap} and calls its 
		 * {@link IWebWorker#processRequest(RequestContext)} method using the received 
		 * {@code rc} {@link RequestContext}
		 */
		private final BiConsumer<String, RequestContext> FETCH = (url, rc) -> {
			try {
				workersMap.get(url).processRequest(rc);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		};
		
		/**
		 * Constructor for a ClientWorker
		 * @param client the {@link Socket} representing the connection to the client
		 */
		public ClientWorker(Socket client){
			this.csocket = client;
		}

		@Override
		public void run() {
			try {
				istream = new PushbackInputStream(csocket.getInputStream());
				ostream = csocket.getOutputStream();
				//retrieve and check header structure
				List<String> headers = getHeaders();
				if(headers==null || headers.size()<1) {
					sendError(ostream, 400, "Bad request");
					return;
				}
				//check first line of header and extract requested path
				String requestedPath = extractParams(headers.get(0));
				if(requestedPath==null) {
					return;
				}
				String hostLine=null;
				for(String h : headers) {
					if(h.startsWith("Host:")) {
						hostLine = h.substring(5).trim();
						break;
					}
				}
				//using domainName as host if no host was provided, otherwise
				//using the provided host, with eventual port address discarded
				this.host = hostLine==null ? domainName : hostLine.split(":")[0];
				
				String sid = checkSession(headers);
				String path = parseParameters(requestedPath);
				
				if(context==null) {
					context = new RequestContext(ostream, params, 
							permParams, outputCookies, tempParams, this, sid);
				}
				internalDispatchRequest(path, true);
			} catch (IOException e) {
				e.printStackTrace();
			} 
			finally {
				closeSocket();
			}
		}

		/**
		 * Closes the connection with the {@link Socket} that was used
		 * for communiation with the client.
		 */
		private void closeSocket() {
			try {
				csocket.close();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
		/**
		 * Returns a list of header lines.
		 * @return a list of lines comprising the header
		 * @throws IOException if reading from the {@link InputStream} fails
		 */
		private List<String> getHeaders() throws IOException {
			byte[] request = readRequest(istream);
			if(request==null) {
				return null;
			}
			String requestStr = new String(request, StandardCharsets.US_ASCII);
			return extractHeaders(requestStr);
		}
		
		/**
		 * Extracts the parameters given in the {@code firstLine} of 
		 * the header.
		 * 
		 * @param firstLine the first line of the header
		 * @return the url path parameter in the line
		 * @throws IOException if the parameters are incorrect, and sending
		 * 			an error to the {@link OutputStream} fails
		 */
		private String extractParams(String firstLine) throws IOException {
			String[] split = firstLine.split(" ");
			if(split.length!=3) {
				sendError(ostream, 400, "Bad request");
				return null;
			}
			this.method = split[0].toUpperCase();
			if(!method.equals("GET")) {
				sendError(ostream, 405, "Method not allowed");
				return null;
			}
			this.version = split[2].toUpperCase();
			if(!version.equals("HTTP/1.1") && !version.equals("HTTP/1.0")) {
				sendError(ostream, 505, "HTTP version not supported.");
				return null;
			}
			return split[1]; //requested path
			
		}
		
		/**
		 * Finds cookie entries in the list of header lines, and tries
		 * to find a {@code sid} cookie marking the session ID among them.
		 * <br>
		 * If a non-expired session with the same sID exists, its persistent
		 * parameters are used by the {@link RequestContext}'s persistent
		 * parameters map.<br>
		 * Otherwise, a new session is created with its unique session ID.
		 * 
		 * @param headers the list of header lines
		 * @return the found or generated session ID
		 */
		private String checkSession(List<String> headers) {
			synchronized(this.getClass()) {
				String sidCandidate = null;
				for(String h : headers) {
					if(!h.startsWith("Cookie:")) {
						continue;
					}
					String cookies[] = h.substring(7).trim().split(";");
					for(String c : cookies) {
						if(c.split("\\s*=\\s*")[0].equals("sid")) {
							sidCandidate = c.split("\\s*=\\s*")[1];
							sidCandidate = sidCandidate.replaceAll("\"", "");
						}
					}
				}
				if(sidCandidate==null) {
					return createSession();
				}
				SessionMapEntry sme = sessions.get(sidCandidate);
				if(sme==null || !sme.host.equals(this.host)) {
					return createSession();
				}
				if(sme.validUntil < System.currentTimeMillis()) {
					sessions.remove(sidCandidate);
					return createSession();
				}
				sme.validUntil = System.currentTimeMillis() + sessionTimeout*1000;
				permParams = sme.map;
				return sme.sid;
			}
		}
		
		/**
		 * Creates a new {@link SessionMapEntry} and adds it to the sessions 
		 * map under a unique session ID, and adds a cookie representing itself
		 * to the list of {@link RCCookie} cookies.
		 * 
		 * @return the generated session ID
		 */
		private String createSession(){
			String sid = generateSid();
			SessionMapEntry sme = new SessionMapEntry(sid, 
					this.host, 
					System.currentTimeMillis()+sessionTimeout*1000,
					new ConcurrentHashMap<String,String>());
			sessions.put(sid, sme);
			outputCookies.add(new RCCookie("sid", sid, null, host, "/"));
			
			permParams = sme.map;
			return sid;
		}
		
		/**
		 * Generates a pseudo-random session ID made of 20 random
		 * uppercase characters from the English alphabet.
		 * 
		 * @return the generated session ID
		 */
		private String generateSid() {
			char[] word = new char[20];
			for(int i=0; i<20; i++) {
				int val = Math.abs(sessionRandom.nextInt());
				word[i] = (char)(65 + val%26);
			}
			return new String(word);
		}
		
		/**
		 * Reads all parameters given through the HTTP GET method
		 * and adds them to the parameters map, returning the path
		 * parameter.
		 * 
		 * @param paramString the url path containing the path and the arguments
		 * @return the path parameter of the request
		 */
		private String parseParameters(String urlPath) {
			String[] splitInit = urlPath.split("\\?");
			if(splitInit.length==1 || splitInit[1].isEmpty()) {
				return splitInit[0]; //no parameters at all (e.g. localhost:5721/calc)
			}
			String[] splitOne = splitInit[1].split("&");
			for(String s : splitOne) {
				String[] keyValue = s.split("=");
				if(keyValue.length==1 || keyValue[1].isEmpty()) {
					continue;	//empty parameter (e.g. localhost:5721/calc?a=&b=)
				}
				params.put(keyValue[0], keyValue[1]);
			}
			return splitInit[0];
		}

		private void internalDispatchRequest(String path, boolean directCall) throws IOException {
			if(directCall && path.startsWith("/private/")) {
				//user cannot access webroot/private/... 
				sendError(ostream, 404, "File not found.");
			}
			if(path.startsWith("/ext/")) {
				//instantiation and starting workers using their literal name
				path = "hr.fer.zemris.java.webserver.workers."+path.substring(5);
				EXTRACT.accept(path, context);
				return;
			}
			if(workersMap.containsKey(path)) {
				//starting workers with a mapped property
				FETCH.accept(path, context);
				return;
			}
			//otherwise, we have a regular file or script to show, no workers involved
			Path requested = documentRoot.resolve(path.substring(1));
			if(requested.equals(documentRoot)) {
				//redirect to index2.html if empty path given
				FETCH.accept("/index2.html", context);	
				return;
			}
			if(!Files.exists(requested) || !Files.isReadable(requested)) {
				//if file doesn't exist, that means it either doesn't exist anywhere,
				//or exists but not resolved to documentRoot
				sendError(ostream, 404, "File not found");
				return;
			}
			
			String[] parts = requested.getFileName().toString().split("\\.");
			String mimeType = "application/octet-stream";	//default
			if(parts.length==2) {
				mimeType = mimeTypes.get(parts[1]);	//extension = mime type
				if(parts[1].equals("smscr")) {
					//executing scripts
					String docBody = Files.readString(requested, StandardCharsets.UTF_8);
					SmartScriptParser parser = new SmartScriptParser(docBody);
					new SmartScriptEngine(parser.getDocumentNode(), context).execute();
					return;
				}
			}
			
			context.setMimeType(mimeType);
			//writing the contents of the requested file into the request context
			try(InputStream fis = new BufferedInputStream(Files.newInputStream(requested))) {
				byte[] buf = new byte[1024];
				while(true) {
					int r = fis.read(buf);
					if(r<1) break;
					context.write(buf, 0, r);
				}
			}
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public void dispatchRequest(String urlPath) throws IOException {
			internalDispatchRequest(urlPath, false);
		}
	}
	
	/**
	 * Simulates a finite state machine that correctly reads exactly
	 * the bytes corresponding to the header of the request.
	 * 
	 * @param is the {@link InputStream} from which data is read
	 * @return an array of bytes corresponding to the header of the request
	 * @throws IOException if reading from {@link InputStream} fails
	 */
	private static byte[] readRequest(InputStream is) 
			throws IOException {

		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		int state = 0;
	l:	while(true) {
			int b = is.read();
			if(b==-1) return null;
			if(b!=13) {
				bos.write(b);
			}
			switch(state) {
			case 0: 
				if(b==13) { state=1; } else if(b==10) state=4;
				break;
			case 1: 
				if(b==10) { state=2; } else state=0;
				break;
			case 2: 
				if(b==13) { state=3; } else state=0;
				break;
			case 3: 
				if(b==10) { break l; } else state=0;
				break;
			case 4: 
				if(b==10) { break l; } else state=0;
				break;
			}
		}
		return bos.toByteArray();
	}
	
	/**
	 * Creates a list of lines comprising the header of the request.
	 * @param requestHeader the header of the request
	 * @return the list of lines making the header
	 */
	private static List<String> extractHeaders(String requestHeader) {
		List<String> headers = new ArrayList<String>();
		String currentLine = null;
		for(String s : requestHeader.split("\n")) {
			if(s.isEmpty()) break;
			char c = s.charAt(0);
			if(c==9 || c==32) {
				currentLine += s;
			} else {
				if(currentLine != null) {
					headers.add(currentLine);
				}
				currentLine = s;
			}
		}
		if(!currentLine.isEmpty()) {
			headers.add(currentLine);
		}
		return headers;
	}
	
	/**
	 * Sends a HTTP header representing an error with the given
	 * {@code statusCode} and {@code statusText}.
	 * 
	 * @param cos the {@link OutputStream} to which to write the error
	 * @param statusCode the status code of the error
	 * @param statusText the status text of the error
	 * @throws IOException if an exception occurs during writing
	 */
	private static void sendError(OutputStream cos, 
			int statusCode, String statusText) throws IOException {
	
			cos.write(
				("HTTP/1.1 "+statusCode+" "+statusText+"\r\n"+
				"Server: simple java server\r\n"+
				"Content-Type: text/plain;charset=UTF-8\r\n"+
				"Content-Length: 0\r\n"+
				"Connection: close\r\n"+
				"\r\n").getBytes(StandardCharsets.US_ASCII)
			);
			cos.flush();
		}

	/**
	 * Entry point of the program
	 * @param args one command-line argument representing the path to the 
	 * 			configuration file
	 */
	public static void main(String[] args) {
		if(args.length!=1) {
			System.err.println("Expected path to config file");
			return;
		}
		SmartHttpServer shs = new SmartHttpServer(args[0]);
		shs.start();
	}
}
