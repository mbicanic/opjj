package hr.fer.zemris.java.webserver.workers;

import java.io.IOException;

import hr.fer.zemris.java.webserver.IWebWorker;
import hr.fer.zemris.java.webserver.RequestContext;

/**
 * BgColorWorker is an implementation of the {@link IWebWorker} interface
 * that retrieves the {@code bgcolor} parameter from the HTTP request
 * parameters and, if the color is valid, sets it as a persistent parameter
 * for that session.
 * 
 * @author Miroslav Bićanić
 */
public class BgColorWorker implements IWebWorker {

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void processRequest(RequestContext context) throws Exception {
		String color = context.getParameter("bgcolor");
		if(color==null || !validHex(color.toCharArray())) {
			generateHTML(context, "Boja nije odabrana");
		} else {
			context.setPersistentParameter("background", color);
			generateHTML(context, "Boja je odabrana");
		}
	}

	/**
	 * Checks if the given color is correctly written as a hexadecimal
	 * number containing the RGB components.
	 * 
	 * @param charArray the array of characters comprising the color parameter
	 * @return true if it is valid; false otherwise
	 */
	private boolean validHex(char[] charArray) {
		if(charArray.length!=6) {
			return false;
		}
		for(char c : charArray) {
			if((c>='0' && c<='9') || (c>='A' && c<='F')) {
				continue;
			}
			return false;
		}
		return true;
	}

	/**
	 * Generates a simple HTML document informing the user of the
	 * status of the color changing operation.
	 * 
	 * @param context the {@link RequestContext} to which to write the HTML
	 * @param msg the status message of the operation
	 */
	private void generateHTML(RequestContext context, String msg) {
		context.setMimeType("text/html");
		try {
			context.write("<html><body><a href=\"/index2.html\"> Home </a>");
			context.write("<p> "+msg+" </p></body></html>");
		} catch (IOException e) {}
	}
}
