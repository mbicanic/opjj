package hr.fer.zemris.java.custom.scripting.lexer;

/**
 * SmartLexerException is an exception that is thrown if any
 * error occurs during tokenization of an input text file.
 * 
 * @author Miroslav Bićanić
 */
public class SmartLexerException extends RuntimeException {
	private static final long serialVersionUID = 1L;
	
	private String illegalCharacter = null;
	/**
	 * A default constructor for a SmartLexerException.
	 */
	public SmartLexerException() {
		super();
	}
	/**
	 * A constructor for a SmartLexerException taking an
	 * illegal character that was found, so that a precise
	 * error can be produced.
	 * @param illegal An illegal character found in text
	 */
	public SmartLexerException(char illegal) {
		this.illegalCharacter = Character.toString(illegal);
	}
	/**
	 * A constructor for a SmartLexerException that takes a message
	 * and forwards it to the parent constructor.
	 * @param msg The message describing the exception
	 */
	public SmartLexerException(String msg) {
		super(msg);
	}
	/**
	 * @return The character that caused the exception, if it exists.
	 */
	public String getIllegalCharacter() {
		return illegalCharacter;
	}
	
}
