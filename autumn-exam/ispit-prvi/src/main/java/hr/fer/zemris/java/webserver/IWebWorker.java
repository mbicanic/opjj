package hr.fer.zemris.java.webserver;

/**
 * Interface for objects that can process a HTTP request
 * given a {@link RequestContext} object.
 * 
 * @author Miroslav Bićanić
 */
public interface IWebWorker {
	/**
	 * Invoked whenever a request must be processed
	 * @param context the context in which to process the request
	 * @throws Exception if an exception occurs during processing a request
	 */
	void processRequest(RequestContext context) throws Exception;
}
