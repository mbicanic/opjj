package hr.fer.zemris.java.webserver.workers;

import hr.fer.zemris.java.webserver.IWebWorker;
import hr.fer.zemris.java.webserver.RequestContext;

/**
 * Home is an implementation of the {@link IWebWorker} interface which
 * sets the background to the default gray (7F7F7F) color if no color
 * is registered in the persistent parameters of the {@link RequestContext}.
 * If a color is registered, then that color is used.
 * </br>
 * Once the parameters are set, Home dispatches request processing to 
 * a script which generates a HTML document with the background color
 * as specified.
 * 
 * @author Miroslav Bićanić
 */
public class Home implements IWebWorker {
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public void processRequest(RequestContext context) throws Exception {
		String param = context.getPersistentParameter("background");
		context.setPersistentParameter("background", 
				param==null ? "7F7F7F" : param);
		context.getDispatcher().dispatchRequest("/private/pages/home.smscr");
	}
}
