package hr.fer.zemris.java.custom.scripting.exec;

import java.util.function.BinaryOperator;

/**
 * ValueWrapper is a container class capable of storing
 * one object of any kind.
 * <br><br>
 * In case the objects are interpretable as numbers, 
 * it provides support for basic arithmetic operations 
 * (addition, subtraction, multiplication and division) 
 * as well as comparing the values.
 * <br><br>
 * An Object is interpretable as a number if it is null 
 * or an instance of: <br>
 * 	1) The Integer class (in case the object is null, it
 * 		is interpreted as {@code Integer.valueOf(0)} )<br>
 * 	2) The Double class <br>
 * 	3) The String class, but only if the string value
 * 		contains a number parsable as an Integer or a
 * 		Double number. Acceptable strings are:<br>
 * 		{@code "12345", "123.45", "123.4E5", "12.3e45"},
 * 		with only the first example being parsed as an Integer.
 * 
 * @author Miroslav Bićanić
 */
public class ValueWrapper {
	/*
	 * BinaryOperator implementations of basic arithmetic operations
	 * with Integer numbers.
	 * Created once to avoid creating trash for the garbage collector.
	 */
	private static final BinaryOperator<Integer> INT_ADD = (i1,i2)->i1+i2;
	private static final BinaryOperator<Integer> INT_SUB = (i1,i2)->i1-i2;
	private static final BinaryOperator<Integer> INT_MUL = (i1,i2)->i1*i2;
	private static final BinaryOperator<Integer> INT_DIV = (i1,i2)->i1/i2;
	/*
	 * BinaryOperator implementations of basic arithmetic operations
	 * with Double numbers.
	 * Created once to avoid creating trash for the garbage collector.
	 */
	private static final BinaryOperator<Double> DBL_ADD = (d1,d2)->d1+d2;
	private static final BinaryOperator<Double> DBL_SUB = (d1,d2)->d1-d2;
	private static final BinaryOperator<Double> DBL_MUL = (d1,d2)->d1*d2;
	private static final BinaryOperator<Double> DBL_DIV = (d1,d2)->d1/d2;
	
	/** The Object value stored by this ValueWrapper */
	private Object value;

	/**
	 * A default constructor for a ValueWrapper, setting the
	 * stored value to null.
	 */
	public ValueWrapper() {
		this(null);
	}
	/**
	 * A constructor for a ValueWrapper setting the stored
	 * value to the given initialization value.
	 * @param initial The initial value to store
	 */
	public ValueWrapper(Object initial) {
		this.value = initial;
	}
	/**
	 * @return This ValueWrapper's value
	 */
	public Object getValue() {
		return value;
	}
	/**
	 * @param value The new value to be stored by this ValueWrapper
	 */
	public void setValue(Object value) {
		this.value = value;
	}
	
	/**
	 * Adds this ValueWrapper's value to the given value and stores
	 * the result as its new value.
	 * 
	 * This operation is possible only with objects interpretable
	 * as numbers that support the operation.
	 * 
	 * @param incValue the value to add to this ValueWrapper's value
	 * @throws RuntimeException
	 			-if either of the two values does not support the operation
	 			-if either of the two values was a string, but its content
	 				was not interpretable as a number
	 */
	public void add(Object incValue) {
		value = doOperation(adaptValues(value, incValue), INT_ADD, DBL_ADD);
	}
	
	/**
	 * Subtracts the given value from this ValueWrapper's value
	 * and stores the result as its new value.
	 * 
	 * This operation is possible only with objects interpretable
	 * as numbers that support the operation.
	 * 
	 * @param subValue the value to subtract from this ValueWrapper's value
	 * @throws RuntimeException
	 			-if either of the two values does not support the operation
	 			-if either of the two values was a string, but its content
	 				was not interpretable as a number
	 */
	public void sub(Object subValue) {
		value = doOperation(adaptValues(value, subValue), INT_SUB, DBL_SUB);
	}
	
	/**
	 * Multiplies this ValueWrapper's value with the given value 
	 * and stores the result as its new value.
	 * 
	 * This operation is possible only with objects interpretable
	 * as numbers that support the operation.
	 * 
	 * @param mulValue the value with which to multiply this ValueWrapper's value
	 * @throws RuntimeException
	 			-if either of the two values does not support the operation
	 			-if either of the two values was a string, but its content
	 				was not interpretable as a number
	 */
	public void mul(Object mulValue) {
		value = doOperation(adaptValues(value, mulValue), INT_MUL, DBL_MUL);
	}
	
	/**
	 * Divides this ValueWrapper's value by the given value and
	 * stores the result as its new value.
	 * 
	 * This operation is possible only with objects interpretable
	 * as numbers that support the operation.
	 * 
	 * @param mulValue the value with which to multiply this ValueWrapper's value
	 * @throws RuntimeException
	 			-if either of the two values does not support the operation
	 			-if either of the two values was a string, but its content
	 				was not interpretable as a number
	 			-if both values were Integers and the given value was equal
	 				to {@code Integer.valueOf(0)}
	 */
	public void div(Object divValue) {
		value = doOperation(adaptValues(value, divValue), INT_DIV, DBL_DIV);
	}
	
	/**
	 * Compares this ValueWrapper's value with the given value,
	 * returning 1 if the wrapper's value is larger than the
	 * given value, -1 if it is smaller and 0 if the two values
	 * are equal.
	 * 
	 * This operation is possible only with objects interpretable
	 * as numbers that can be compared.
	 * 
	 * @param withValue The value to which to compare this ValueWrapper's value
	 * @return 1 if wrapper's value is larger than, -1 if it is smaller than and
	 * 			0 if it is equal to the given value
	 */
	public int numCompare(Object withValue) {
		double dif = doOperation(adaptValues(value, withValue), INT_SUB, DBL_SUB).doubleValue();
		if(dif==0) {
			return 0;
		}
		return dif > 0 ? 1 : -1;
	}

	/**
	 * Performs an arithmetic operation using the appropriate of the
	 * two given operators, depending on the type of arguments.
	 * 
	 * Only if both given arguments are Integer objects, the {@code intOperator}
	 * is used. Otherwise, the {@code doubleOperator} is used.
	 * 
	 * @param arguments An array of Number objects representing the arguments
	 * 						for the operation
	 * @param intOperator A BinaryOperator operating with Integer objects, to be
	 * 						used if both given Numbers are Integers
	 * @param doubleOperator A BinaryOperator operating with Double objects, to be
	 * 						used if either of the given Numbers is Double.
	 * @return The result of the operation - either an Integer or a Double object
	 * @throws RuntimeException if an attempt of integer division by zero occurs
	 */
	private Number doOperation(Number[] arguments, BinaryOperator<Integer> intOperator, BinaryOperator<Double> doubleOperator) {
		if(arguments[0] instanceof Integer && arguments[1] instanceof Integer) {
			if(intOperator==INT_DIV && arguments[1].intValue()==0) {
				throw new RuntimeException("Cannot divide by zero!");
			}
			return intOperator.apply(arguments[0].intValue(), arguments[1].intValue());
		}
		return doubleOperator.apply(arguments[0].doubleValue(), arguments[1].doubleValue());
	}

	/**
	 * Adapts the arguments of an arithmetic operation to make
	 * them compatible for basic arithmetic operations.
	 * <br><br>
	 * Adapting is done according to the following rules:
	 * 	1) If any of the arguments is null, it is treated 
	 * 		as {@code Integer.valueOf(0)}<br>
	 * 	2) If any of the arguments is a String, an attempt
	 * 		is made at parsing it into a Number, which can
	 * 		result in an exception.<br>
	 * 	3) Otherwise, it must be either a Double or an Integer,
	 * 		in which case it must be castable into a Number.
	 * <br><br>
	 * Once adapted, the arguments are returned as an array of
	 * Number objects.
	 * 
	 * @param first The first argument for the arithmetic operation
	 * @param second The second argument for the arithmetic operation
	 * @return An array of Number objects representing the arguments
	 * @throws ClassCastException if either of the values is not suitable
	 * 								for performing arithmetic operations
	 */
	private Number[] adaptValues(Object first, Object second) {
		Number[] array = new Number[2];
		array[0] = (first==null ? Integer.valueOf(0) : null);
		array[1] = (second==null ? Integer.valueOf(0) : null);
		
		array[0] = (first instanceof String ? parse((String)first) : array[0]);
		array[1] = (second instanceof String ? parse((String)second) : array[1]);
		
		try {
			array[0] = (array[0]==null ? (Number)first : array[0]);
			array[1] = (array[1]==null ? (Number)second : array[1]);
			return array;
		} catch (ClassCastException ex) {
			throw new RuntimeException("Cannot perform arithmetic operation on given argument.");
		}
	}
	
	/**
	 * Parses a String as either a Double number or an Integer number.
	 * 
	 * @param s The string to parse
	 * @return A Number being either a Double or an Integer
	 */
	private Number parse(String s) {
		try {
			if(s.contains(".") || s.contains("E") || s.contains("e")) {
				return Double.parseDouble(s);
			}
			return Integer.parseInt(s);
		} catch (NumberFormatException ex) {
			throw new RuntimeException("Cannot parse '"+s+"' to any number format.");
		}
	}

}