package hr.fer.zemris.java.custom.scripting.nodes;

/**
 * TextNode is a node that represents a textual
 * segment of the document.
 * 
 * For a segment to be considered textual, it must not be
 * a tag (enclosed in {$ $}). 
 * The text is stored with all escape sequences already 
 * escaped.
 * 
 * @author Miroslav Bićanić
 */
public class TextNode extends Node {
	/**
	 * The text contained in this TextNode.
	 */
	private String text;
	/**
	 * A constructor for a TextNode, setting up its text.
	 */
	public TextNode(String text) {
		this.text = text;
	}
	/**
	 * @return This TextNode's text.
	 */
	public String getText() {
		return this.text;
	}
	
	@Override
	public String toString() {
		String t = replaceTextEscape(text);
		return t;
	}
	
	/**
	 * Gives this TextNode as the argument to the given {@link INodeVisitor}
	 * {@code v}.
	 * 
	 * @param v the {@link INodeVisitor} that visited this node
	 */
	public void accept(INodeVisitor v) {
		v.visitTextNode(this);
	}
}
