package ispit;

import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.util.Iterator;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

public class Datumi extends JFrame {
	private static final long serialVersionUID = 1L;

	private final AbstractAction open = new AbstractAction() {
		private static final long serialVersionUID = 1L;

		@Override
		public void actionPerformed(ActionEvent e) {
			JFileChooser jfc = new JFileChooser();
			jfc.setDialogTitle("Open file");
			int res = jfc.showOpenDialog(Datumi.this);
			if(res!=JFileChooser.APPROVE_OPTION) {
				return;
			}
			Path p = jfc.getSelectedFile().toPath();
			List<String> lines;
			try {
				lines = Files.readAllLines(p);
			} catch (IOException ex) {
				ex.printStackTrace();
				return;
			}
			Iterator<String> it = lines.iterator();
			while(it.hasNext()) {
				String s = it.next();
				if(s.isBlank() || s.startsWith("#")) {
					it.remove();;
				}
			}
			model.repopulate(lines);
		}
	};
					
	private JList<String> datelist;
	private Model model;
	private JLabel datelabel;
	
	public Datumi() {
		setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		setSize(new Dimension(500,500));
		setLocation(20, 20);
		
		initGUI();
	}

	private void initGUI() {
		Container cp = getContentPane();
		this.model = new Model();
		this.datelist = new JList<String>(model);
		this.datelabel = new JLabel();
		
		this.open.putValue(AbstractAction.NAME, "Open");
		
		datelist.addListSelectionListener(new ListSelectionListener() {
			
			@Override
			public void valueChanged(ListSelectionEvent e) {
				Object o = datelist.getSelectedValue();
				if(o==null) {
					datelabel.setText("");
					return;
				}
				String s = o.toString();
				StringBuilder sb = new StringBuilder(50);
				LocalDate ldt = LocalDate.parse(s);
				DayOfWeek dow = ldt.getDayOfWeek();
				sb.append("Na datum ").append(s);
				switch(dow) {
				case WEDNESDAY:
					sb.append(" bila je srijeda.");
					break;
				case SATURDAY:
					sb.append(" bila je subota.");
					break;
				case SUNDAY:
					sb.append(" bila je nedjelja.");
					break;
				case MONDAY:
					sb.append(" bio je ponedjeljak.");
					break;
				case TUESDAY:
					sb.append(" bio je utorak.");
					break;
				case THURSDAY:
					sb.append(" bio je četvrtak.");
					break;
				case FRIDAY:
					sb.append(" bio je petak.");
					break;
				}
				datelabel.setText(sb.toString());
			}
		});

		cp.setLayout(new GridLayout(1,2));
		cp.add(datelist);
		cp.add(datelabel);
		createMenuBar();
	}

	private void createMenuBar() {
		JMenuBar mb = new JMenuBar();
		JMenu file = new JMenu("File");
		mb.add(file);
		
		JMenuItem open = new JMenuItem(this.open);
		file.add(open);
		
		setJMenuBar(mb);
	}
	
	public static void main(String[] args) {
		SwingUtilities.invokeLater(()->{
			new Datumi().setVisible(true);
		});
	}
}
