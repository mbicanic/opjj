package ispit;

import java.util.ArrayList;
import java.util.List;

import javax.swing.AbstractListModel;

public class Model extends AbstractListModel<String>{
	private static final long serialVersionUID = 1L;
	
	private List<String> dates = new ArrayList<String>();
	
	public void repopulate(List<String> dts) {
		this.dates.clear();
		this.dates.addAll(dts);
		fireIntervalAdded(this, 0, dates.size()-1);
	}
	
	@Override
	public int getSize() {
		return dates.size();
	}

	@Override
	public String getElementAt(int index) {
		return dates.get(index);
	}

}
