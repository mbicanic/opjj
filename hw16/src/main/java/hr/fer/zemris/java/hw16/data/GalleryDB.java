package hr.fer.zemris.java.hw16.data;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

/**
 * GalleryDB is a class modelling a database containing records
 * of {@link Image} objects. The class provides with several
 * methods for retrieving Image objects.
 * 
 * GalleryDB is modelled using the Singleton design pattern - 
 * only one instance of GalleryDB exists at any given point.
 * 
 * @author Miroslav Bićanić
 */
public class GalleryDB {
	/** The instance of the GalleryDB */
	private static GalleryDB instance;
	/** The list of {@link Image} objects contained by the database */
	private List<Image> images;
	
	/**
	 * A private constructor building the GalleryDB from a
	 * configuration file.
	 * 
	 * @param p the {@link Path} pointing to the configuration file
	 */
	private GalleryDB(Path p) {
		configureGDB(p);
	}
	
	/**
	 * Retrieves the {@link Image} whose {@code path} is equal to
	 * the given {@code filename}.
	 * 
	 * @param filename the name of the {@link Image} to look for
	 * @return the {@link Image} with the given filename; null if doesn't exist
	 */
	public Image getByPath(String filename) {
		for(Image i : images) {
			if(i.getPath().equals(filename)) {
				return i;
			}
		}
		return null;
	}
	
	/**
	 * Returns all {@link Image} records which are tagged by the
	 * given {@code tag}.
	 * 
	 * @param tag the tag to look for in {@link Image} objects
	 * @return a {@link List} of {@link Image} objects associated with the given tag
	 */
	public List<Image> getByTag(String tag){
		List<Image> list = new ArrayList<Image>();
		for(Image i : images) {
			for(String imgTag : i.getTags()) {
				if(tag.equals(imgTag)) {
					list.add(i);
				}
			}
		}
		return list;
	}
	
	/**
	 * Returns a set of all tags to which at least one image is tagged.
	 * @return a {@link Set} of all tags
	 */
	public Set<String> getTagSet(){
		Set<String> tags = new HashSet<String>();
		for(Image i : images) {
			for(String imgTag : i.getTags()) {
				tags.add(imgTag);
			}
		}
		return tags;
	}
	
	/**
	 * Initializes the database contents using the data from the
	 * given configuration file {@link Path}.
	 * @param p the {@link Path} from which to configure the database
	 */
	private void configureGDB(Path p) {
		try {
			List<String> lines = Files.readAllLines(p);
			Iterator<String> i = lines.iterator();
			this.images = new ArrayList<Image>();
			while(i.hasNext()) {
				String src = i.next();
				String desc = i.next();
				String tagLine = i.next();
				String[] tags = tagLine.split(",");
				for(int iter=0; iter<tags.length; iter++) {
					tags[iter] = tags[iter].toLowerCase().trim();
				}
				images.add(new Image(desc, tags, src, "thumbnails/"+src));
			}
		} catch(IOException e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * Builds the GalleryDB instance using the given {@link Path}
	 * pointing to a configuration file.
	 * @param p the {@link Path} to a configuration file
	 */
	public static void build(Path p) {
		if(instance==null) {
			instance = new GalleryDB(p);
		}
	}
	
	/**
	 * @return the instance of the GalleryDB
	 */
	public static GalleryDB get() {
		return instance;
	}
}
