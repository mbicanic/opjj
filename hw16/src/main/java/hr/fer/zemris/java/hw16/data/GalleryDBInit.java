package hr.fer.zemris.java.hw16.data;

import java.nio.file.Path;
import java.nio.file.Paths;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

/**
 * A {@link WebListener} implementing the {@link ServletContextListener}
 * interface, used to build the {@link GalleryDB} instance on startup of
 * the web application.
 * 
 * @author Miroslav Bićanić
 */
@WebListener
public class GalleryDBInit implements ServletContextListener {

	@Override
	public void contextInitialized(ServletContextEvent sce) {
		String path = sce.getServletContext().getRealPath("/WEB-INF/opisnik.txt");
		Path p = Paths.get(path);
		GalleryDB.build(p);
	}

	@Override
	public void contextDestroyed(ServletContextEvent sce) {}

}
