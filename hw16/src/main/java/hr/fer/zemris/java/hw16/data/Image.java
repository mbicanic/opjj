package hr.fer.zemris.java.hw16.data;

/**
 * This class represents an Image record in the database.
 * 
 * @author Miroslav Bićanić
 */
public class Image {
	private String desc;
	private String[] tags;
	private String path;
	private String thumbPath;
	
	/**
	 * A constructor for an Image
	 * @param desc the description of the image
	 * @param tags the tags associated with the image
	 * @param path the path to the image file
	 * @param thumbPath the path to the image thumbnail file
	 */
	public Image(String desc, String[] tags, String path, String thumbPath) {
		super();
		this.desc = desc;
		this.tags = tags;
		this.path = path;
		this.thumbPath = thumbPath;
	}

	public String getDesc() {
		return desc;
	}

	public String[] getTags() {
		return tags;
	}

	public String getPath() {
		return path;
	}

	public String getThumbPath() {
		return thumbPath;
	}
}
