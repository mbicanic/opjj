package hr.fer.zemris.java.hw16.servlets;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import javax.imageio.ImageIO;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * LoadImage is a specification of a {@link HttpServlet} which
 * is in charge of processing requests for images.
 * 
 * Depending on the parameters with which it is called, it retrieves 
 * either the thumbnail (in which the parameters of the request must 
 * be {@code form=thumb&src=[relative path to file]}) or the source
 * image (parameters: {@code form=full&src=[relative path to file]}).
 * 
 * @author Miroslav Bićanić
 */
public class LoadImage extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String form = req.getParameter("form");
		if(form==null) {
			return;
		}
		String source = req.getParameter("src");
		if(source.equals(null)) {
			return;
		}
		String extension = source.substring(source.lastIndexOf(".")+1);
		resp.setContentType("image/"+extension);
		BufferedImage outImage = null;
		if(form.equals("thumb")) {
			String filename = source.replace("thumbnails/", "");
			String thumbPath = req.getServletContext().getRealPath("/WEB-INF/"+source);
			Path thumbnail = Paths.get(thumbPath);
			if(!Files.exists(thumbnail)) {
				File dir = new File(req.getServletContext().getRealPath("/WEB-INF/thumbnails"));
				if(!Files.exists(dir.toPath())) {
					dir.mkdirs();
				}
				cacheImage(req.getServletContext().getRealPath("/WEB-INF/"+filename), thumbnail, extension);
			}
			outImage = ImageIO.read(thumbnail.toFile());
		} else if(form.equals("full")) {
			String fullPath = req.getServletContext().getRealPath("/WEB-INF/"+source);
			outImage = ImageIO.read(Paths.get(fullPath).toFile());
		} else {
			return;
		}
		ImageIO.write(outImage, extension, resp.getOutputStream());
	}

	/**
	 * Creates a 150x150 pixel thumbnail of an image and stores it
	 * into a file.
	 * @param path the path to the full image source file
	 * @param thumb the {@link Path} to the thumbnail destination file
	 * @param extension the extension of the image to create
	 * @throws IOException if reading or writing fails
	 */
	private void cacheImage(String path, Path thumb, String extension) throws IOException {
		BufferedImage thumbBim = new BufferedImage(150, 150, BufferedImage.TYPE_3BYTE_BGR);
		Path source = Paths.get(path);
		System.out.println("####################################################");
		System.out.println(path);
		System.out.println(source.toAbsolutePath().toString());
		System.out.println("####################################################");
		//this prints out to the tomcat server in terminal.
		//for some reason, I was receiving errors at the next line.
		//once I added this, the program worked correctly.
		//I have no idea why is that so. Try if you get the same
		//problem when you delete this. (Note: problem exists only
		//when app is deployed directly to tomcat, not running 
		//through Eclipse)
		
		BufferedImage fullBim = ImageIO.read(source.toFile());
		
		Graphics2D g2 = thumbBim.createGraphics();
		g2.drawImage(fullBim, 0, 0, 150, 150, null);
		g2.dispose();
		
		ImageIO.write(thumbBim, //extension.equals("jpg") ? "jpeg" : 
			extension, thumb.toFile());
	}
}
