package hr.fer.zemris.java.hw16.rest;

import java.util.List;
import java.util.Set;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.json.JSONArray;
import org.json.JSONObject;

import hr.fer.zemris.java.hw16.data.GalleryDB;
import hr.fer.zemris.java.hw16.data.Image;

/**
 * ImageJSON is a class modelling the server's interface
 * for REST requests. It formats responses as JSON arrays
 * or objects.
 * 
 * @author Miroslav Bićanić
 */
@Path("/gallery")
public class ImageJSON {
	
	/**
	 * Returns a Response with a {@link JSONArray} containing all
	 * tags present in the web application.
	 * 
	 * @return the {@link Response}
	 */
	@GET
	@Produces("application/json")
	public Response getTagsList() {
		JSONArray tags = new JSONArray();
		GalleryDB gdb = GalleryDB.get();
		Set<String> tagSet = gdb.getTagSet();
		for(String s : tagSet) {
			tags.put(s);
		}
		
		return Response.status(Status.OK).entity(tags.toString()).build();
	}
	
	/**
	 * Returns a {@link Response} with a {@link JSONArray} containing
	 * a {@link JSONObject} for each {@link Image} present in the 
	 * database that is associated with the given tag.
	 * 
	 * @param tag the tag by which filter photos
	 * @return the {@link Response}
	 */
	@Path("/tag/{id}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getImages(@PathParam("id") String tag) {
		List<Image> list = GalleryDB.get().getByTag(tag);
		
		JSONArray array = new JSONArray();
		for(Image i : list) {
			JSONObject imgObj = new JSONObject();
			imgObj.put("realPath", i.getPath());
			imgObj.put("thumbPath", i.getThumbPath());
			array.put(imgObj);
		}
		
		return Response.status(Status.OK).entity(array.toString()).build();
	}
	
	/**
	 * Returns a {@link Response} with a {@link JSONObject} containing
	 * data about the image with the given name, which corresponds to 
	 * the file name.<br>
	 * The data contains the image description and tags with which the
	 * image is associated.
	 * 
	 * @param name the name of the image
	 * @return the {@link Response}
	 */
	@Path("/img/{name}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getSingleImage(@PathParam("name") String name) {
		Image image = GalleryDB.get().getByPath(name);
		
		JSONObject obj = new JSONObject();
		obj.put("desc", image.getDesc());
		obj.put("path", image.getPath());
		JSONArray tags = new JSONArray();
		for(String tag : image.getTags()) {
			tags.put(tag);
		}
		obj.put("tags", tags);
		
		return Response.status(Status.OK).entity(obj.toString()).build();
	}
}
