package hr.fer.zemris.java.custom.collections;

/**
 * Custom exception thrown when an application tries to pop
 * or peek from an empty stack.
 * 
 * @author mirob
 *
 */
public class EmptyStackException extends RuntimeException {
	private static final long serialVersionUID = 1L;
	
	/**
	 * Constructs a {@code: EmptyStackException} with no detail message.
	 */
	public EmptyStackException() {
		super();
	}
	/**
	 * Constructs a {@code: EmptyStackException} with a detail message.
	 * 
	 * @param message The message to store with the exception
	 */
	public EmptyStackException(String message) {
		super(message);
	}
	/**
	 * Constructs a {@code: EmptyStackException} over an existing {@code: Throwable}.
	 * 
	 * @param cause The Throwable object that caused this exception.
	 */
	public EmptyStackException(Throwable cause) {
		super(cause);
	}
	/**
	 * Constructs a {@code: EmptyStackException} over an existing {@code: Throwable}
	 * with a detail message.
	 * 
	 * @param cause The Throwable object that caused this exception.
	 */
	public EmptyStackException(String message, Throwable cause) {
		super(message, cause);
	}
}
