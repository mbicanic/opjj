package hr.fer.zemris.java.custom.collections;

import java.util.Objects;

/**
 * An abstract collection.
 * 
 * It dictates the contract and the user interface all 
 * collections have to have.
 * 
 * @author mirob
 *
 */
public class Collection {
	
	/**
	 * A protected default constructor.
	 */
	protected Collection() {};
	
	/**
	 * This method says whether a collection is empty.
	 * 
	 * All classes extending Collection must either override
	 * this method, or provide an implementation of <code>size
	 * </code> for this method to work.
	 * 
	 * @see #size()
	 * @return true if collection is empty, false otherwise
	 */
	public boolean isEmpty() {
		return this.size()==0;
	}
	
	/**
	 * This method gives the current number of elements in the list.
	 * 
	 * All classes extending Collection must override this method
	 * for it to work.
	 * @return The number of elements in the collection
	 */
	public int size() {
		return 0;
	}
	
	/**
	 * Adds a <code>value</code> to the collection.
	 * 
	 * All classes extending Collection must override this method
	 * for it to work.
	 * @param value The value to add to the collection
	 */
	public void add(Object value) {}
	
	/**
	 * This method says whether a <code>value</code> is in the collection.
	 * 
	 * All classes extending Collection must override this method
	 * for it to work.
	 * @param value The value to look for in the collection.
	 */
	public boolean contains(Object value) {
		return false;
	}
	
	/**
	 * Removes one occurrence of <code>value</code> from the collection.
	 * 
	 * All classes extending Collection must override this method
	 * for it to work.
	 * @param value The value to remove from the collection
	 */
	public boolean remove(Object value) {
		return false;
	}
	
	/**
	 * Gives the content of the collection as an array.
	 * 
	 * All classes extending Collection must override this method
	 * for it to work.
	 */
	public Object[] toArray() {
		throw new UnsupportedOperationException();
	}
	
	/**
	 * Does the processor's action on every element of the collection.
	 * 
	 * All classes extending Collection must override this method
	 * for it to work.
	 * @param processor The processor that does the action on every element.
	 */
	public void forEach(Processor processor) {}
	
	/**
	 * Adds all elements of another Collection to this collection.
	 * 
	 * All classes extending Collection only have to override the methods
	 * <code>add</code> and <code>forEach</code> for this method to work.
	 * @param other The collection whose elements will be added to this collection.
	 */
	public void addAll(Collection other) {
		Objects.requireNonNull(other);
		class Adder extends Processor {
			@Override
			public void process(Object value) {
				Collection.this.add(value);
			}
		}
		Adder adder = new Adder();
		other.forEach(adder);
	}
	
	/**
	 * Deletes all elements from the collection.
	 * 
	 * All classes extending Collection have to override this method
	 * for it to work.
	 */
	public void clear() {}
	
	/**
	 * Checks if a given integer is within the <b>closed</b> given range.
	 * 
	 * @param index The index to be checked
	 * @param lowerLimit The lower range limit (inclusive)
	 * @param upperLimit The upper range limit (inclusive)
	 */
	public static void checkValidInteger(int index, int lowerLimit, int upperLimit) {
		if(index<lowerLimit) {
			throw new IndexOutOfBoundsException("Expected positive index. Got: '"+index+"'.");
		}
		if(index>upperLimit) {
			throw new IndexOutOfBoundsException("Index '"+index+"' invalid for collection of size: "+upperLimit);
		}
	}
}
