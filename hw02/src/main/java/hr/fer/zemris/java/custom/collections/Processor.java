package hr.fer.zemris.java.custom.collections;

/**
 * An abstract processor.
 * 
 * A template class used to define what all
 * processors have to be able to do.
 * 
 * @author mirob
 *
 */
public class Processor {
	
	/**
	 * This method takes a <code>value</code> and does
	 * something with it.
	 * 
	 * All classes extending Processor must provide an 
	 * implementation of this method.
	 * 
	 * @param value The value with which something has to be done.
	 */
	public void process(Object value) {}
}
