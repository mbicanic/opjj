package hr.fer.zemris.java.hw02;

import static java.lang.Math.*;
import java.util.Objects;

/**
 * A class that models a complex number in Cartesian coordinates, 
 * providing the user with all the basic operations for working
 * with complex numbers.
 * 
 * Every ComplexNumber is immutable, all changes result in a
 * new ComplexNumber being created.
 * 
 * This class specifies a maximal acceptable difference of two
 * double numbers for them to still be considered equal. This
 * in turn is used to compare complex numbers through their
 * real and imaginary parts. 
 * The currently specified allowed difference is 1 * 10^-6.
 * This can lead to incorrect results where high precision is
 * needed!
 * 
 * @author mirob
 *
 */
public class ComplexNumber {
	/**
	 * The maximal difference between two double values in order
	 * for us to consider them equal. For example, with it set to 
	 * 1e-6 we will consider 10.0000009 and 10.0000000 equal, 
	 * however 10.0000011 and 10.0000000 are not!
	 */
	public static final double acceptableOffset = 1e-6;
	
	/**
	 * The real part of the complex number.
	 */
	private final double real;
	/**
	 * The imaginary part of the complex number.
	 */
	private final double imaginary;
	
	/**
	 * A constructor that takes both the real and imaginary
	 * parameters and creates a new ComplexNumber with the
	 * given parameters.
	 * 
	 * @param real The real part of the new ComplexNumber
	 * @param imaginary The imaginary part of the new ComplexNumber
	 */
	public ComplexNumber(double real, double imaginary) {
		this.real = real;
		this.imaginary = imaginary;
	}
	
	/**
	 * A factory method that creates a new ComplexNumber
	 * only from the real part, setting the imaginary to 0.
	 * 
	 * @param real The real part of the new ComplexNumber
	 * @return The new ComplexNumber with the given real part.
	 */
	public static ComplexNumber fromReal(double real) {
		return new ComplexNumber(real, 0);
	}
	/**
	 * A factory method that creates a new ComplexNumber
	 * only from the imaginary part, setting the real to 0.
	 * 
	 * @param imaginary The imaginary part of the new ComplexNumber
	 * @return The new ComplexNumber with the given imaginary part.
	 */
	public static ComplexNumber fromImaginary(double imaginary) {
		return new ComplexNumber(0, imaginary);
	}
	/**
	 * A factory method that computes Cartesian coordinates from
	 * given polar coordinates and creates a new ComplexNumber
	 * from them.
	 * 
	 * @param magnitude The magnitude of the complex number
	 * @param angle	The angle of the complex number
	 * @return	The new ComplexNumber created from the given parameters
	 */
	public static ComplexNumber fromMagnitudeAndAngle(double magnitude, double angle) {
		return new ComplexNumber(magnitude*cos(angle), magnitude*sin(angle));
	}
	
	/**
	 * Parses the given string as a complex number.
	 * 
	 * The parser accepts all formats which are in compliance with the
	 * following rules:<br>
	 * 1) The parser accepts a redundant '+' sign in front of a number,
	 * 		interpreting the following number as positive.
	 * 2) The parser DOES NOT accept sequences (length > 1) of symbols:
	 * 		'++', '--', '+.' are not allowed. 
	 * 		The exception is if the second symbol is 'i' ('+i', '-i' is OK).
	 * 3) If there is an imaginary part, the imaginary unit 'i' HAS TO be
	 * 		on the last position in the string:
	 * 		'2+i4', '-i2', '+i11' are not allowed.
	 * 4) If there are both parameters in the string, they HAVE TO be
	 * 		separated with a single '+' or '-'.
	 * 
	 * Accepted formats are ([number] = any decimal or whole number):
	 *  [number], +[number], -[number] 		-> new ComplexNumber(+-number, 0)
	 *  i, +i, -i							-> new ComplexNumber(0, +-1)
	 *  [number]i, +[number]i, -[number]i	-> new ComplexNumber(0, +-number)
	 *  [number]+i							-> new ComplexNumber(+-number, +-1)
	 * +[number]+i
	 * -[number]+i
	 *  [number]-i
	 * +[number]-i
	 * -[number]-i
	 *  [number1]+[number2]i				-> new ComplexNumber(+-number1, +-number2)
	 * +[number1]+[number2]i
	 * -[number1]+[number2]i
	 *  [number1]-[number2]i
	 * +[number1]-[number2]i
	 * -[number1]-[number2]i				-> {@link #ComplexNumber(double, double)}
	 * 
	 * @param str The string to parse into a complex number.
	 * @return A ComplexNumber built from the data in the string
	 * @throws NullPointerException if the handed string is null
	 * @throws IllegalArgumentException if the string is empty
	 * @throws NumberFormatException: 
	 * 		-if the sequence contains illegal characters
	 * 		-if 'i' exists but isn't the last symbol
	 * 		-if there are two numbers but aren't separated with a single '+' or '-'
	 * 		-if Double.parseDouble() can't parse the number.
	 */
	public static ComplexNumber parse(String givenString) {
		/*
		 * First we check if the givenString is even acceptable
		 * and remove whitespace.
		 */
		Objects.requireNonNull(givenString);
		if(givenString.length()==0) {
			throw new IllegalArgumentException("Cannot parse empty string.");
		}
		givenString = givenString.trim();
		String s = givenString;
		
		/*
		 * Then we check for the sign of the following number.
		 * The first character can be a digit, +, - or i. If
		 * it was + or -, we remove it, remembering the sign.
		 * The sign here doesn't have to appear, it can be
		 * only implied.
		 */
		char symbol = s.charAt(0);
		int signum = parseSignum(s, false);
		if(symbol=='+' || symbol=='-') {
			s = s.substring(1);
			symbol = s.charAt(0);
		}
		
		/*
		 * We must make sure that the character isn't only 'i' now, 
		 * and if it is, that it is the only character in the string.
		 */
		if(symbol=='i') {
			if(s.length()>1) {
				throw new NumberFormatException("Invalid format: If the imaginary unit is present, 'i' must be the last character.");
			}
			return ComplexNumber.fromImaginary(signum);
		}
		
		/*
		 * Since it's not 'i', it has to be a number (whether
		 * the real part, or the imaginary part).
		 */
		String arg1 = parseFirstPotentialDouble(s);
		double arg;
		try {
			arg = Double.parseDouble(arg1);
		} catch (NumberFormatException ex) {
			throw new NumberFormatException("String '"+arg1+"' can not be parsed as a double.");
		}
		
		/*
		 * If we parsed the whole string as a double, that means the 
		 * complex number has only the real component. If only one 
		 * character remained and it is 'i', it has only the imaginary
		 * component.
		 */
		if(arg1.length()==s.length()) {
			return ComplexNumber.fromReal(signum*arg);	
			
		} else if(arg1.length()==s.length()-1 && s.charAt(s.length()-1)=='i') {
			return ComplexNumber.fromImaginary(signum * arg);
		}
		
		/*
		 * Otherwise, the string has to contain two arguments. The one
		 * we just parsed is the real part, the next is the imaginary 
		 * part.
		 * The following sign HAS to be '+' or '-', which parseSignum
		 * checks.
		 * We can immediately try and eliminate some possibilities
		 * of wrong input.
		 */
		double real = arg*signum;
		
		if(s.charAt(s.length()-1)!='i') {
			throw new NumberFormatException("Invalid format: if there is a second argument, it has to be imaginary and end with 'i'.");
		}
		
		s = s.substring(arg1.length());
		/*
		 * The procedure for the second argument is almost the same as earlier.
		 */
		
		int imSignum = parseSignum(s, true);
		s = s.substring(1);
		
		//after a mandatory signum, the next character has to be a digit or i
		symbol = s.charAt(0);
		if(!Character.isDigit(symbol) && !(symbol=='i' && s.length()==1)) {
			if(symbol=='+' || symbol=='-') {
				throw new NumberFormatException("Invalid format: there can only be one + or - sign between two numbers.");
			}
			if(symbol=='i') {
				throw new NumberFormatException("Invalid format: if there is a second argument, it has to have only one 'i' at the end."); 
				}
			throw new NumberFormatException("Encountered invalid character: '"+symbol+"'. Expected: a digit, 'i'.");
		}
		
		if(symbol=='i') {
			return new ComplexNumber(real, imSignum);	
		}
		
		s = s.substring(0,s.length()-1); //removing 'i' from the end
		double imag;
		try {
			imag = Double.parseDouble(s) * imSignum;
		} catch (NumberFormatException ex) {
			throw new NumberFormatException("String '"+s+"' can not be parsed as a double.");
		}
		return new ComplexNumber(real, imag);
	}

	/**
	 * Parses the sign of a given string in two modes, depending
	 * on if the sign is mandatory or not.
	 * 
	 * If mandatorySignum is true, then the first character of the
	 * string must be either a '+' or a '-'. Otherwise, the first
	 * character can be a digit, '+', '-' or 'i'. The character
	 * must never be something other than a digit, '+', '-' or 'i'.
	 * 
	 * It assumes the string isn't a null, nor empty (it is a private 
	 * function called only in controlled circumstances).
	 * 
	 * @param s The string to check the sign of
	 * @param mandatorySignum Parameter for deciding what are the acceptable characters
	 * @return 1 if encountered '+', 'i' or a digit, -1 if encountered '-'.
	 * @throws NumberFormatException if encountered anything else.
	 */
	private static int parseSignum(String s, boolean mandatorySignum) {
		char c = s.charAt(0);
		if(c=='+' || c=='-') {
			if(c=='-') {
				return -1;
			}
			return 1;
		} else {
			if((Character.isDigit(c) || c=='i') && mandatorySignum) {
				throw new NumberFormatException("Encountered invalid character '"+c+"'. Expected: '+' or '-'.");
			} else if((Character.isDigit(c) || c =='i') && !mandatorySignum) {
				return 1;
			}
			throw new NumberFormatException("Encountered invalid character: '"+c+"'. Expected: " + (mandatorySignum ? "'+', '-'." : "a digit, '+', '-', 'i'."));
			
		}
	}
	
	/**
	 * This function takes a string and returns the substring from
	 * position 0 to the position of the first character that is 
	 * not a digit [0-9] (allowing one dot to appear, however).
	 * 
	 * This method does not guarantee the number is parsable as double - 
	 * it can return an empty string. The final check is done by
	 * Double::parseDouble.
	 * 
	 * @param s The received string which should start with a double number
	 * @return	The substring containing a potential double number
	 */
	private static String parseFirstPotentialDouble(String s) {
		int dotCounter=0;
		int indexOfNonDigit = s.length();
		String val;
		for(int i = 0; i < s.length(); i++) {
			char sign = s.charAt(i);
			if(!Character.isDigit(sign) && sign!='.') {
				indexOfNonDigit=i;
				break;
			}
			if(sign=='.') {
				dotCounter++;
			}
			if(dotCounter>1) {
				indexOfNonDigit=0;
				break;
			}
		}
		val = s.substring(0,indexOfNonDigit);
		return val;
	}

	/**
	 * This method returns the real part of this ComplexNumber.
	 * @return the real part of the complex number
	 */
	public double getReal() {
		return this.real;
	}
	/**
	 * This method returns the imaginary part of this ComplexNumber.
	 * @return the imaginary part of this complex number.
	 */
	public double getImaginary() {
		return this.imaginary;
	}
	/**
	 * This method returns the magnitude of this ComplexNumber
	 * in polar coordinates.
	 * @return the magnitude of this complex number.
	 */
	public double getMagnitude() {
		return sqrt(real*real + imaginary*imaginary);
	}
	/**
	 * This method returns the angle of this ComplexNumber in
	 * polar coordinates, normalised to values from [0,2pi>.

	 * @return the angle of this complex number from the range [0,2pi>
	 */
	public double getAngle() {
		return normaliseAngleTo2Pi(atan2(imaginary, real));
	}
	
	/**
	 * Adds a ComplexNumber <code>other</code> and this ComplexNumber.
	 *
	 * Using: (a+bi)+(c+di) = (a+c)+(b+d)i
	 * 
	 * @param other The complex number to add to this one
	 * @return a new ComplexNumber that is the result of addition.
	 * @throws NullPointerException if the handed ComplexNumber is null
	 */
	public ComplexNumber add(ComplexNumber other) {
		Objects.requireNonNull(other);
		return new ComplexNumber(this.getReal() + other.getReal(),
				this.getImaginary() + other.getImaginary());
	}
	
	/**
	 * Subtracts ComplexNumber <code>other</code> from this ComplexNumber.
	 * 
	 * Using: (a+bi)-(c+di) = (a-c)+(b-d)i
	 *
	 * @param other The complex number to subtract from this one
	 * @return a new ComplexNumber that is the result of subtraction.
	 * @throws NullPointerException if the handed ComplexNumber is null
	 */
	public ComplexNumber sub(ComplexNumber other) {
		Objects.requireNonNull(other);
		return this.add(new ComplexNumber(-1*other.real, -1*other.imaginary));
	}
	
	/**
	 * Multiplies this ComplexNumber with ComplexNumber <code>other</code>.
	 * 
	 * It uses the formula:
	 * (a+bi)*(c+di) = (ac-bd) + (ad+bc)i
	 * 
	 * @param other The complex number with which to multiply
	 * @return a new ComplexNumber that is the result of multiplication.
	 * @throws NullPointerException if the handed ComplexNumber is null
	 */
	public ComplexNumber mul(ComplexNumber other) {
		Objects.requireNonNull(other);
		double newReal = this.real * other.real - this.imaginary * other.imaginary;
		double newImaginary = this.real*other.imaginary + this.imaginary * other.real;
		return new ComplexNumber(newReal, newImaginary);
	}
	/**
	 * Divides this ComplexNumber with the ComplexNumber <code>other</code>.
	 * 
	 * It uses the formula:
	 * z/w = (z * w~) / Re(w)
	 * for z, w, w~ complex numbers, w~ being the complex conjugate of w.
	 * 
	 * @param other The complex number with which to divide this complex number
	 * @return The ratio of this and other complex number
	 * @throws NullPointerException if the handed ComplexNumber is null
	 */
	public ComplexNumber div(ComplexNumber other) {
		Objects.requireNonNull(other);
		
		double denominator = other.mul(other.conjugate()).getReal();
		
		ComplexNumber interResult = this.mul(other.conjugate());
		ComplexNumber result = new ComplexNumber(interResult.real/denominator, 
				interResult.imaginary/denominator);
		return result;
	}
	/**
	 * Returns a complex conjugate of this ComplexNumber using the formula:
	 * (a + bi)~ = (a - bi).
	 * @return a new ComplexNumber: the complex conjugate of this ComplexNumber
	 */
	public ComplexNumber conjugate() {
		return new ComplexNumber(this.real, -1 * this.imaginary);
	}
	
	/**
	 * This method calculates and returns a new ComplexNumber equal to this
	 * ComplexNumber raised to the nth power.
	 * 
	 * Using: mag(z)^n * (cos(ang(z)*n) + i*sin(ang(z)*n)
	 * For: z complex number, mag(z) magnitude and ang(z) angle.
	 * 
	 * @param n The power to which this complex number is raised
	 * @return A new complex number equal to this to the nth power
	 * @throws IllegalArgumentException if the power is negative,
	 * 			or both power and this complex number are zero.
	 */
	public ComplexNumber power(int n) {
		if(n<0) {
			throw new IllegalArgumentException("Cannot calculate negative powers for complex numbers.");
		}
		if(n==0 && doubleEquals(this.real, 0) && doubleEquals(this.imaginary, 0)) {
			throw new IllegalArgumentException("Cannot calculate 0^0.");
		}
		double magnitude = pow(this.getMagnitude(),n);
		double angle = this.getAngle();
		return ComplexNumber.fromMagnitudeAndAngle(magnitude, n*angle);
	}
	
	/**
	 * This method calculates and returns all the nth complex roots
	 * of this ComplexNumber in a ComplexNumber array.
	 * 
	 * Using: sqrt(mag(z)) * e^(j*(ang(z)+2k*pi)/n)
	 * For: k = 0..n-1, z complex number, ang(z) angle, mag(z) magnitude
	 * @param n
	 * @return
	 */
	public ComplexNumber[] root(int n) {
		if(n<=0) {
			throw new IllegalArgumentException("Cannot calculate non-positive roots for complex numbers.");
		}
		if(doubleEquals(this.real, 0) && doubleEquals(this.imaginary, 0)) {
			return new ComplexNumber[] {new ComplexNumber(0,0)};
		}
		
		ComplexNumber[] roots = new ComplexNumber[n];
		double magnitude = pow(this.getMagnitude(), 1.0/n);
		double angle = this.getAngle();
		
		for(int k = 0; k < n; k++) {
			double newAngle = (angle + 2*k*PI) / n;
			roots[k] = ComplexNumber.fromMagnitudeAndAngle(magnitude, newAngle);
		}
		return roots;
	}
	
	/**
	 * Returns the string representation of this ComplexNumber.
	 * 
	 * Format: [real][+|-][imaginary]i.
	 * @return the complex number written as a string
	 */
	public String toString() {
		StringBuilder sb = new StringBuilder(20);
		if(this.real==0 && this.imaginary==0) {
			sb.append("0");
			return sb.toString();
		}
		if(this.real!=0) {
			sb.append(this.real);
		}
		if(this.imaginary!=0) {
			sb.append((this.imaginary>0 && this.real!=0) ? "+" : "");
			if(Math.abs(this.imaginary)==1) {
				sb.append(this.imaginary<0 ? "-" : "");
				sb.append("i");
				return sb.toString();
			}
			sb.append(this.imaginary);
			sb.append("i");
		}
		return sb.toString();
	}

	/**
	 * Method for calculating the hash code of each ComplexNumber.
	 * 
	 * Overridden in accordance with equals.
	 */
	@Override
	public int hashCode() {
		return Objects.hash(imaginary, real);
	}

	/**
	 * Method that says whether two ComplexNumbers are equal.
	 * 
	 * Two complex numbers are considered equal when the absolute 
	 * difference between their real and their imaginary parts is
	 * smaller than {@link #acceptableOffset}
	 * 
	 * @return true if they are equal, false otherwise
	 * @see ComplexNumber#doubleEquals(double, double)
	 * @see ComplexNumber#acceptableOffset
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof ComplexNumber))
			return false;
		ComplexNumber other = (ComplexNumber) obj;
		
		return doubleEquals(this.real, other.real)
				&& doubleEquals(this.imaginary, other.imaginary);
	}
	
	/**
	 * This method says whether the absolute difference of two
	 * doubles is smaller than {@link #acceptableOffset}, thus
	 * considering them equal.
	 * 
	 * Value NaN is considered not equal to anything, even itself.
	 * Infinities are equal if both are of equal sign.
	 * 
	 * @param a First number
	 * @param b Second number
	 * @return true if they are considered equal, false otherwise
	 */
	public static boolean doubleEquals(double a, double b) {
		if(Double.isNaN(a) || Double.isNaN(b)) {
			return false;
		}
		if(Double.isInfinite(a) && Double.isInfinite(b)) {
			return a==b;
		}
		return Math.abs(a-b) <= acceptableOffset;
	}
	
	/**
	 * This method takes any angle and returns the same angle,
	 * normalised to the range of [0,2pi> radians.
	 * 
	 * @param angle The angle to be normalised
	 * @return The normalised angle from the [0,2pi> range
	 */
	public static double normaliseAngleTo2Pi(double angle) {
		while(angle >= 2*Math.PI) {
			angle-= 2*Math.PI;
		}
		while(angle < 0) {
			angle+= 2*Math.PI;
		}
		if(doubleEquals(angle, 2*Math.PI)) {
			angle = 0;
		}
		return angle;
	}
}
