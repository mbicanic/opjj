package hr.fer.zemris.java.custom.collections;

import java.util.Arrays;
import java.util.Objects;

/**
 * An indexed collection of Objects based on an array.
 * 
 * This collection allows duplicate elements, 
 * but does not allow null values.
 * 
 * @author mirob
 *
 */
public class ArrayIndexedCollection extends Collection {
	
	/**
	 * The default capacity of the underlying Object array.
	 */
	private static final int defaultCapacity = 16;
	
	/**
	 * The number of elements currently in the array.
	 */
	private int size;
	
	/**
	 * The underlying Object array.
	 * 
	 * This array is used to store the elements of the collection.
	 * Its capacity is increased exponentially, doubling in size
	 * every time it gets full.
	 */
	private Object[] elements;
	
	/**
	 * The default constructor.
	 * 
	 * Creates a new empty collection with the initial capacity of the
	 * underlying array set to the {@link #defaultCapacity}.
	 * 
	 * @see #ArrayIndexedCollection(Collection, int)
	 */
	public ArrayIndexedCollection() {
		this(new Collection(), defaultCapacity);
	}
	
	/**
	 * A constructor that creates an ArrayIndexedCollection 
	 * with a specified initial capacity.
	 * 
	 * @param initialCapacity The initial capacity of the created collection's underlying array.
	 * @see #ArrayIndexedCollection(Collection, int)
	 */
	public ArrayIndexedCollection(int initialCapacity) {
		this(new Collection(), initialCapacity);
	}
	/**
	 * A constructor that takes another collection and creates
	 * an ArrayIndexedCollection copy of it.
	 * 
	 * The capacity it sends further is the {@link #defaultCapacity},
	 * but it is not guaranteed it will be the used initial capacity.
	 * If the given collection's size is larger than the default 
	 * capacity, it is used as the initial capacity.
	 * 
	 * @param other The collection to copy.
	 * @see #ArrayIndexedCollection(Collection, int)
	 */
	public ArrayIndexedCollection(Collection other) {
		this(other, defaultCapacity);
	}
	/**
	 * A constructor that takes another collection and an initial capacity.
	 * 
	 * This constructor creates an ArrayIndexedCollection copy out of 
	 * another collection, using the larger number between the specified 
	 * capacity and the other collection's size as its underlying array's 
	 * initial capacity.
	 * 
	 * @param other The collection which this constructor will copy.
	 * @param initialCapacity The desired initial capacity for the array.
	 * @throws NullPointerException if the given collection is null
	 * @throws IllegalArgumentException if the given initial capacity is smaller than 1
	 */
	public ArrayIndexedCollection(Collection other, int initialCapacity) {
		Objects.requireNonNull(other);
		if(initialCapacity < 1) {
			throw new IllegalArgumentException("Expected: capacity bigger than 1. Got: '"+initialCapacity+"'.");
		}
		this.elements = new Object[initialCapacity>other.size() ? initialCapacity : other.size()];
		this.addAll(other);
	}
	
	//INFORMATION RETRIEVING METHODS
	/**
	 * Returns the size of this ArrayIndexedCollection.
	 * 
	 * @return The number of non-null elements in the collection
	 */
	@Override
	public int size() {
		return this.size;
	}

	/**
	 * This method says whether a <code>value</code> is in this 
	 * ArrayIndexedCollection, where the equality is determined 
	 * by the equals method.
	 * 
	 * This operation is done in O(n) complexity (O(n/2) average).
	 * 
	 * @param value The value to look for in the collection.
	 */
	@Override
	public boolean contains(Object value) {
		if(value==null) {
			return false;
		}
		for(int i = 0; i < this.size; i++) {
			if(this.elements[i].equals(value)) {
				return true;
			}
		}
		return false;
	}
	
	/**
	 * This method returns the index of the first occurrence of a
	 * <code>value</code> in this ArrayIndexedCollection as determined 
	 * by the equals method.
	 * 
	 * This operation is done in O(n) complexity (O(n/2) average).
	 * 
	 * @param value The value to look for
	 * @return -1 if <code>value</code> doesn't occur in the collection; its first occurrence's index otherwise
	 */
	public int indexOf(Object value) {
		if(value==null) {
			return -1;
		}
		for(int i = 0; i < this.size; i++) {
			if(this.elements[i].equals(value)) {
				return i;
			}
		}
		return -1;
	}
	
	// READ/WRITE METHODS
	/**
	 * This method adds a <code>value</code> into this ArrayIndexedCollection.
	 * 
	 * The operation is done in O(1) complexity, except when the underlying
	 * array has to be reallocated, in which case it is done in O(n) 
	 * complexity.
	 * 
	 * @throws NullPointerException if the given <code>value</code> is null
	 */
	@Override
	public void add(Object value) {
		Objects.requireNonNull(value);
		if(size==this.elements.length) {
			this.reallocate();
		}
		this.elements[size] = value;
		size++;
	}
	
	/**
	 * This method inserts the element into this ArrayIndexedCollection
	 * at a given position.
	 * 
	 * The index of all elements starting from the given index is increased
	 * by one after this operation - it does not overwrite.
	 * 
	 * This operation is done in O(n) complexity (O(n/2) average), unless
	 * the underlying array has to be reallocated, in which case it is 
	 * done in O(n + n) complexity (O(n + n/2) average).
	 * 
	 * @param value The element to be inserted
	 * @param index The position at which the value will be inserted
	 * @throws NullPointerException if the value is null
	 * @throws IndexOutOfBoundsException if the given index is out of range [0,{@link #size}]
	 */
	public void insert(Object value, int index) {
		Objects.requireNonNull(value);
		checkValidInteger(index, 0, this.size);
		
		if(this.size == this.elements.length) {
			this.reallocate();
		}
		
		if(index == this.size) {
			this.elements[index] = value;
			this.size++;
			return;
		}
		for(int i = this.size; i > index; i--) {
			this.elements[i] = this.elements[i-1];
		}
		this.elements[index] = value;
		this.size++; 
	}

	/**
	 * This method returns the element of this ArrayIndexedCollection 
	 * from a given position.
	 * 
	 * This operation is done in O(1) complexity.
	 * 
	 * @param index The index of the element to retrieve
	 * @return The element at the given index
	 * @throws IndexOutOfBoundsException if the given index is out of range [0,{@link #size}-1]
	 */
	public Object get(int index) {
		checkValidInteger(index, 0, this.size-1);
		return elements[index];
	}

	/**
	 * Removes one (first) occurrence of <code>value</code> from this 
	 * ArrayIndexedCollection, where the equality is determined by the 
	 * equals method.
	 * 
	 * This operation is done in O(n) complexity (O(n) average).
	 * 
	 * @param value The value to remove from the collection.
	 */
	@Override
	public boolean remove(Object value) {
		if(value==null) {
			return false;
		}
		int index = this.indexOf(value);
		if(index == -1) {
			return false;
		}
		this.remove(index);
		return true;
	}

	/**
	 * Removes the element from a given index from this ArrayIndexedCollection.
	 * 
	 * This operation is done in O(n) complexity (O(n/2) average).
	 * 
	 * @param index The index of the element to remove from the collection
	 * @throws IndexOutOfBoundsException if the given index is out of range [0,{@link #size}-1]
	 */
	public void remove(int index) {
		checkValidInteger(index, 0, this.size-1);
		int i;
		this.elements[index] = null;
		for(i = index; i < this.size-1; i++) {
			this.elements[i] = this.elements[i+1];
		}
		this.size--;
	}
	
	/**
	 * Deletes all elements from this ArrayIndexedCollection.
	 * 
	 * The capacity of the underlying array remains unchanged.
	 * This operation is done in O(n) complexity (O(n) average).
	 */
	@Override
	public void clear() {
		for(int i = 0; i < this.size; i++) {
			this.elements[i]=null;
		}
		this.size=0;
	}
	
	//UTILITY METHODS
	/**
	 * Gives the content of this ArrayIndexedCollection as an array of the
	 * same size.
	 * 
	 * The resulting array has the same elements on the same positions
	 * as the original ArrayIndexedCollection.
	 * 
	 * @return An array with all the elements of this ArrayIndexedCollection
	 */
	@Override
	public Object[] toArray() {
		return Arrays.copyOf(this.elements, this.size);
	}
	
	/**
	 * Does the processor's action on every element of this
	 * ArrayIndexedCollection.
	 * 
	 * @param processor The processor that does the action on every element.
	 * @throws NullPointerException if the given processor is null.
	 */
	@Override
	public void forEach(Processor processor) {
		Objects.requireNonNull(processor);
		for(int i = 0; i < this.size; i++) {
			processor.process(this.elements[i]);
		}
	}
	
	
	//PRIVATE HELPER METHODS
	/**
	 * This method allocates a new array with twice the capacity as
	 * the this.elements array, keeping all elements on their positions.
	 */
	private void reallocate() {
		this.elements = Arrays.copyOf(this.elements, this.elements.length*2);
	}
}
