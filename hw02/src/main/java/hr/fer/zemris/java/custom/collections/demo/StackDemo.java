package hr.fer.zemris.java.custom.collections.demo;
import hr.fer.zemris.java.custom.collections.ArrayIndexedCollection;
import hr.fer.zemris.java.custom.collections.EmptyStackException;
import hr.fer.zemris.java.custom.collections.ObjectStack;

/* TESTED INPUTS:		
 * "8 -2 / -1 *" = 4
 * '+ 8 2 5 / +' = Error - empty stack
 * '36 18 / 2 4 * 33 31 - 16 -8 + 9 7 % + + + +' = 22
 * '-4 8 + 5 7 - + +' = Error - only one elem on stack
 * "2 -4 8 + 4 - /" = Error - cannot divide by 0.
 * "2 -4 4 + % 2 2 +" = Error - modulo by 0
 * "19 21 23 - + slovo 5" = Error - invalid token 'slovo'
 * 
 */

/**
 * A class demonstrating the usage of a stack to perform arithmetic
 * operations written in post-fix notation.
 * 
 * The input is handed as ONE argument through the command line.
 * 
 * This program handles input as specified below:
 * 1) If a completely invalid token (real number, string or a symbol
 * 		that IS NOT an operator: +,-,*,/,%) is encountered, the whole
 * 		evaluation is terminated, and the error is shown to the user.
 * 2) If a whole number is encountered, it is pushed to the stack.
 * 3) If an operator is encountered, the program attempts to pop two
 *		values from the stack. 
 *		-If there aren't enough elements on the, 
 *		the stack, the evaluation is terminated and the error shown
 *		to the user.
 *		-If division or modulo operation by 0 is attempted, the 
 *		evaluation is terminated and the error shown to the user
 *		-Otherwise, the result of the operation is pushed back on 
 *		the stack. Repeat the process for all tokens of the input
 *		sequence.
 * 	
 * @author mirob
 *
 */
public class StackDemo {
	
	/**
	 * A static collection of strings that represent legal operators for 
	 * the functionality of this program.
	 */
	public static ArrayIndexedCollection operators = new ArrayIndexedCollection();
	static {
		operators.add("+");
		operators.add("-");
		operators.add("*");
		operators.add("/");
		operators.add("%");
	}
	
	/**
	 * Entry point of the program.
	 * 
	 * This method controls the behaviour of the program.
	 */
	public static void main(String...args) {
		/*
		 * Uncomment the print lines to see every step of the calculation!
		 */
		if(args.length!=1) {
			System.out.printf("%d arguments given. Expected: %d.%nTerminating...%n", args.length, 1);
			return;
		}
		String[] tokens = args[0].split("\\s+"); // '\\s+' = regular expression for "one or more whitespace elements"
		ObjectStack stack = new ObjectStack();
		
		String errorMessage = "";
		
		for(int i = 0; i < tokens.length; i++) {
			String token = tokens[i];
			//System.out.printf("%d->'%s': %n", i, token);
			
			try {
				Integer intToken = Integer.parseInt(token);
				//System.out.println("Pushing number "+token+" to stack.\n");
				stack.push(intToken);
				continue;
			} catch (NumberFormatException ex) {
				if(!operators.contains(token)) {
					errorMessage = "Encountered invalid token: '"+token+"'. Allowed only digits and operators.";
					stack.clear();
					break;
				}	
			}
			
			Integer arg2, arg1;
			arg2 = integerPop(stack);
			arg1 = integerPop(stack);
			if(arg1==null) {
				errorMessage = "For operation '"+token+"' expected 2 values on stack, found "+ (arg2==null ? "0." : "1.");
				break;
			}
			
			Integer result = performOperation(arg1, arg2, token);
			if(result==null) {
				errorMessage = String.format("Illegal operation: %s %s %s. Cannot %s by %2$s.", arg1, token, arg2, (token.equals("/") ? "divide" : "perform modulo")); 
				break;
			}
			stack.push(result);
			//System.out.printf("Pushing result:  %d %s %d = %d.%n%n", arg1, token, arg2, result);
			
		}
		if(stack.size()!=1) {
			System.out.println("Error: Sequence \""+args[0]+"\" was not calculated correctly.");
			System.out.println(errorMessage);
			System.out.println("Terminating.");
			return;
		}
		System.out.println("Expression evaluates to "+stack.pop()+".");
	}
	
	
	/**
	 * This is a helper method that attempts to pop an object from a stack,
	 * returning it as Integer or null if it fails.
	 * 
	 * @param s The stack to pop from
	 * @return	The Integer value of the popped Object
	 */
	public static Integer integerPop(ObjectStack s) {
		try {
			Object o = s.pop();
			return (Integer)o;
		} catch (EmptyStackException ex) {
			return null;
		}
	}
	
	/**
	 * This method performs the operation given through the token
	 * on two Integer arguments and returns its result.
	 * 
	 * In case of division or modulo by zero, it returns null.
	 * 
	 * @param arg1 The first operand
	 * @param arg2 The second operand
	 * @param token The string that has to be one of the operators: +, -, *, /, %
	 * @return Integer result of the operation, null if illegal operation
	 */
	private static Integer performOperation(Integer arg1, Integer arg2, String token) {
		if(token.matches("[/%]") && arg2.intValue()==0) {
			return null;
		}
		switch(token) {
		case "+":
			return Integer.valueOf(arg1.intValue() + arg2.intValue());
		case "-":
			return Integer.valueOf(arg1.intValue() - arg2.intValue());
		case "*":
			return Integer.valueOf(arg1.intValue() * arg2.intValue());
		case "/":
			return Integer.valueOf(arg1.intValue() / arg2.intValue());
		case "%":
			return Integer.valueOf(arg1.intValue() % arg2.intValue());
		}
		return null;
	}
}
