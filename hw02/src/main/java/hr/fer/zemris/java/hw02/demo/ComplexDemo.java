package hr.fer.zemris.java.hw02.demo;

import hr.fer.zemris.java.hw02.ComplexNumber;

public class ComplexDemo {
	public static void main(String[] args) {
		ComplexNumber c1 = new ComplexNumber(2,3);
		ComplexNumber c2 = ComplexNumber.parse("2.5-3i");
	    ComplexNumber cres = c1.add(ComplexNumber.fromMagnitudeAndAngle(2, 1.57)).div(c2).power(3).root(2)[1];
		System.out.println(cres);
		
		 //EXAMPLES FROM THE INSTRUCTIONS
		/*
		String[] workingExamples = new String[] {
				"351",
				"-317",
				"3.51",
				"-3.17",
				"351i",
				"-317i",
				"3.51i",
				"-3.17i",
				"i",
				"+i",
				"-i",
				"-2.71-3.15i",
				"31+24i",
				"-1-i",
				"+2.71",
				"+2.71+3.15i"
		};
		String[] notOkayExamples = new String[] {
				"i351",
				"-i317",
				"i3.51",
				"-i3.17",
				"-+2.71",
				"--2.71",
				"-2.71+-3.15i",
				"+2.71-+3.15i",
				"-+2.71",
				"+i3i"
		};
		for(String parseMe : workingExamples) {
			System.out.println(ComplexNumber.parse(parseMe));
		}
		for(String parseMe : notOkayExamples) {
			try {
				System.out.println(ComplexNumber.parse(parseMe));
			} catch (Exception e) {
				System.out.println("I can't "+parseMe);
			}
		}
		*/
	}
}
