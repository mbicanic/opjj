package hr.fer.zemris.java.custom.collections;

import java.util.Objects;

/**
 * An indexed collection of Objects based on a linked list of nodes.
 * 
 * This collection allows duplicate elements, 
 * but does not allow null values.
 * 
 * @author mirob
 *
 */
public class LinkedListIndexedCollection extends Collection {
	
	/**
	 * The number of elements currently in the array.
	 */
	private int size;
	
	/**
	 * The first node in the linked list.
	 */
	private ListNode first;
	
	/**
	 * The last node in the linked list.
	 */
	private ListNode last;
	
	/**
	 * A data structure representing each node in the linked list.
	 * 
	 * @author mirob
	 */
	private static class ListNode {
		/**
		 * The previous node in the list. 
		 */
		private ListNode previous;
		/**
		 * The next node in the list.
		 */
		private ListNode next;
		/**
		 * The value of this node.
		 */
		private Object value;
		
		/**
		 * The constructor for a ListNode.
		 * 
		 * @param previous The previous node
		 * @param next The next node
		 * @param value The value of this node
		 */
		private ListNode(ListNode previous, ListNode next, Object value) {
			this.previous = previous;
			this.next = next;
			this.value = value;
		}
	}
	
	/**
	 * The default LinkedListIndexedCollection constructor.
	 * 
	 * Creates a new empty LinkedListIndexedCollection.
	 */
	public LinkedListIndexedCollection() {
		this(new Collection());
	}
	/**
	 * A constructor that takes a Collection and makes a
	 * LinkedListIndexedCollection copy of it.
	 * 
	 * @param other The collection to copy
	 */
	public LinkedListIndexedCollection(Collection other) {
		Objects.requireNonNull(other);
		this.addAll(other);
		/*
		 * size, first and last are all initialised to 0,
		 * null and null respectively, automatically.
		 * if other isn't empty, size will change as part
		 * of addAll->forEach->Adder->process::add(value)
		 */
	}
	
	
	//INFORMATION RETRIEVING METHODS
	/**
	 * Returns the size of this LinkedListIndexedCollection.
	 * 
	 * @return The number of non-null elements in the collection
	 */
	@Override
	public int size() {
		return this.size;
	}
	
	/**
	 * This method says whether a <code>value</code> is in this 
	 * LinkedListIndexedCollection, where the equality is determined 
	 * by the equals method.
	 * 
	 * This operation is done in O(n) complexity (O(n/2) average).
	 * 
	 * @param value The value to look for in the collection.
	 */
	@Override
	public boolean contains(Object value) {
		if(value==null) {
			return false;
		}
		ListNode goalNode = this.seekRetrieve(value);
		return goalNode==null ? false : true;
	}

	/**
	 * This method returns the index of the first occurrence of a
	 * <code>value</code> in this LinkedListIndexedCollection as determined 
	 * by the equals method.
	 * 
	 * This operation is done in O(n) complexity (O(n/2) average).
	 * 
	 * @param value The value to look for in the collection
	 * @return -1 if <code>value</code> doesn't occur in the collection; the index of the first occurrence otherwise
	 */
	public int indexOf(Object value) {
		if(value==null) {
			return -1;
		}
		ListNode temp = first;
		int index = 0;
		while(temp!=null) {
			if(temp.value.equals(value)) {
				return index;
			}
			temp = temp.next;
			index++;
		}
		return -1;
	}

	// READ/WRITE METHODS
	/**
	 * This method adds a <code>value</code> into this LinkedListIndexedCollection.
	 * 
	 * The operation is done in O(1) complexity.
	 * 
	 * @throws NullPointerException if the given <code>value</code> is null
	 */
	@Override
	public void add(Object value) {
		Objects.requireNonNull(value);
		
		if(first==null) {
			first = new ListNode(null, null, value);
			last = first;
			this.size++;
			return;
		}
		ListNode toAdd = new ListNode(this.last, null, value);
		this.last.next = toAdd;
		this.last = toAdd;
		this.size++;
	}

	/**
	 * This method inserts the element into this LinkedListIndexedCollection
	 * at a given position.
	 * * 
	 * The index of all elements starting from the given index is increased
	 * by one after this operation - it does not overwrite.
	 * 
	 * This operation is done in O(n/2+1) complexity.
	 * 
	 * @param value The element to be inserted
	 * @param index The position at which the value will be inserted
	 * @throws NullPointerException if the value is null
	 * @throws IndexOutOfBoundsException if the given index is out of range [0,{@link #size}]
	 */
	public void insert(Object value, int index) {
		Objects.requireNonNull(value);
		
		ListNode newNode = new ListNode(null,null,value);
		if(index==0) {
			newNode.next = first;
			first.previous = newNode;
			first = newNode;
			this.size++;
			return;
		} else if (index==this.size) { //index can be this.size
			newNode.previous = last;
			last.next = newNode;
			last = newNode;
			this.size++;
			return;
		} 
		ListNode nodeAtPosition = seekIndexed(index); //throws IndexOutOfBoundsException!
		newNode.next = nodeAtPosition;
		newNode.previous = nodeAtPosition.previous;
		nodeAtPosition.previous = newNode;
		newNode.previous.next = newNode;
		
		this.size++;
		return;
	}

	/**
	 * This method returns the element of this LinkedListIndexedCollection 
	 * from a given position.
	 * 
	 * This operation is done in O(n/2+1) complexity.
	 * 
	 * @param index The index of the element to retrieve
	 * @return The element at the given index
	 * @throws IndexOutOfBoundsException if the given index is out of range [0,{@link #size}-1]
	 */
	public Object get(int index) {
		return seekIndexed(index).value;
	}
	
	/**
	 * Removes one (first) occurrence of <code>value</code> from this 
	 * LinkedListIndexedCollection, where the equality is determined by the 
	 * equals method.
	 * 
	 * This operation is done in O(n) complexity (O(n/2) average).
	 * 
	 * @param value The value to remove from the collection.
	 */
	@Override
	public boolean remove(Object value) {
		if(value==null) {
			return false;
		}
		ListNode goalNode = this.seekRetrieve(value);
		if(goalNode==null) {
			return false;
		}
		if(goalNode==first) {
			ListNode temp = first.next;
			temp.previous=null;
			first = temp;
			this.size--;
			return true;
		}
		if(goalNode==last) {
			ListNode temp = last.previous;
			temp.next = null;
			last = temp;
			this.size--;
			return true;
		}
		
		goalNode.previous.next = goalNode.next;
		goalNode.next.previous = goalNode.previous;
		goalNode=null;
		this.size--;
		return true;
	}

	/**
	 * Removes the element from a given index from this LinkedListIndexedCollection.
	 * 
	 * This operation is done in O(n/2+1) complexity.
	 * @param index The index of the element to remove from the collection
	 * @throws IndexOutOfBoundsException if the given index is out of range [0,{@link #size}-1]
	 */
	public void remove(int index) {
		if(index==0) {
			first = first.next;
			first.previous = null;
			this.size--;
			return;
		}
		if(index==this.size-1) {
			last = last.previous;
			last.next = null;
			this.size--;
			return;
		}
		ListNode goalNode = seekIndexed(index);
		
		goalNode.previous.next = goalNode.next;
		goalNode.next.previous = goalNode.previous;
		goalNode = null;
		this.size--;
	}
	
	/**
	 * Deletes all elements from this LinkedListIndexedCollection.
	 * 
	 * This operation is done in O(1) complexity.
	 */
	@Override
	public void clear() {
		first = null;
		last = null;
		this.size=0;
	}
	
	//UTILITY METHODS
	/**
	 * Gives the content of this LinkedListIndexedCollection as an array.
	 * 
	 * The resulting array has same elements on same positions
	 * as the original LinkedListIndexedCollection.
	 * 
	 * @return An array with all the elements of this LinkedListIndexedCollection
	 */
	@Override
	public Object[] toArray() {
		Object[] result = new Object[this.size];
		int i = 0;
		ListNode temp = first;
		while(temp!=null) {
			result[i] = temp.value;
			i++;
			temp = temp.next;
		}
		return result;
	}
	
	/**
	 * Does the processor's action on every element of this
	 * LinkedListIndexedCollection.
	 * 
	 * @param processor The processor that does the action on every element.
	 * @throws NullPointerException if the given processor is null.
	 */
	@Override
	public void forEach(Processor processor) {
		Objects.requireNonNull(processor);
		ListNode temp = first;
		while(temp!=null) {
			processor.process(temp.value);
			temp = temp.next;
		}
	}
	
	//PRIVATE HELPER METHODS
	/**
	 * Seeks for the node with the given value and returns it if it exists.
	 * 
	 * The method is probing the LinkedListIndexedCollection from the left
	 * side, finding the first occurrence of <code>value</code>.
	 * 
	 * @param value The value that is looked for in the collection
	 * @return The reference to the node with the given value if it exists, null otherwise
	 */
	private ListNode seekRetrieve(Object value) {
		ListNode iter = this.first;
		while(iter!=null) {
			if(iter.value.equals(value)) {
				return iter;
			}
			iter = iter.next;
		}
		return null;
	}
	
	/**
	 * Reaches the node at position <code>index</code> and
	 * returns it.
	 * 
	 * This method probes the LinkedListIndexedCollection from
	 * either side, depending on what will give a faster result,
	 * never exceeding O(n/2 + 1) complexity.
	 * 
	 * @param index The index of the node that needs to be reached
	 * @return a reference to the node at the given position
	 * @throws IndexOutOfBoundsException if the index is out of range
	 */
	private ListNode seekIndexed(int index) {
		checkValidInteger(index, 0, this.size-1);
		int i;
		ListNode iterator;
		if(index>this.size/2) {
			i = this.size-1;
			iterator = last;
			while(i>index) {
				iterator = iterator.previous;
				i--;
			}
			return iterator;
		} else {
			i = 0;
			iterator = first;
			while(i<index) {
				iterator = iterator.next;
				i++;
			}
			return iterator;
		}
	}
	
}
