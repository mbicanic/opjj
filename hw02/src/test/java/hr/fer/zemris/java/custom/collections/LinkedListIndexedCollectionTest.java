package hr.fer.zemris.java.custom.collections; 

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;
import hr.fer.zemris.java.custom.collections.Processor;

class LinkedListIndexedCollectionTest {

	@Test
	void defaultConstructor() {
		LinkedListIndexedCollection array = new LinkedListIndexedCollection();
		assertEquals(0, array.size());
		assertThrows(IndexOutOfBoundsException.class, ()->{
			array.get(0);
		});
	}
	
	@Test
	void collectionConstructorThrowsForNull() {
		assertThrows(NullPointerException.class,()->{
			LinkedListIndexedCollection array = new LinkedListIndexedCollection(null);
			array.size();
		});
	}
	
	@Test
	void collectionConstructorWorksForEmpty() {
		LinkedListIndexedCollection array = new LinkedListIndexedCollection();
		LinkedListIndexedCollection result = new LinkedListIndexedCollection(array);
		assertEquals(0, result.size());
		assertThrows(IndexOutOfBoundsException.class, ()->
		{
			array.get(0);
		});
	}
	
	/*
	 * Before testing the collection constructor for a full collection
	 * we must test if the method add works, and to do that we will use
	 * the size method, after which we can prove the contains method.
	 * The get method is proved in the process.
	 * Only then can we test the remaining constructor possibilities:
	 * collectionConstructor with full collection
	 * bothArgsConstructor with full collection and initCap smaller
	 * bothArgsConstructor with full collection and initCap bigger
	 */
	
	@Test
	void addThrowsForNull() {
		assertThrows(NullPointerException.class, ()-> {
			LinkedListIndexedCollection array = new LinkedListIndexedCollection();
			array.add(null);
		});
	}
	@Test
	void addChangesSize() {
		LinkedListIndexedCollection array = new LinkedListIndexedCollection();
		array.add(Integer.valueOf(5));
		assertEquals(1, array.size());
	}
	@Test
	void addAllowsGet() {
		LinkedListIndexedCollection array = new LinkedListIndexedCollection();
		array.add("papiga");
		assertEquals("papiga", array.get(0));
		assertThrows(IndexOutOfBoundsException.class, ()-> {
			array.get(1);
		});
	}
	@Test
	void addWorksWithGetAndSize() {
		LinkedListIndexedCollection array = new LinkedListIndexedCollection();
		array.add("pero");
		assertEquals(1, array.size());
		array.add(Integer.valueOf(10));
		assertEquals(2, array.size());
		array.add(Double.valueOf(Math.PI));
		assertEquals(3, array.size());
		
		assertEquals("pero", array.get(0));
		assertEquals(Integer.valueOf(10), array.get(1));
		assertEquals(Double.valueOf(Math.PI), array.get(2));
	}
	@Test
	void containsAfterAdd() {
		LinkedListIndexedCollection array = new LinkedListIndexedCollection();
		array.add("foo");
		array.add("bar");
		array.add("key");
		array.add("val");
		
		assertTrue(array.contains("foo"));
		assertTrue(array.contains("bar"));
		assertTrue(array.contains("key"));
		assertTrue(array.contains("val"));
		
		assertFalse(array.contains("yoo"));
		assertFalse(array.contains("VAL"));
	}
	@Test
	void containsNull() {
		LinkedListIndexedCollection array = new LinkedListIndexedCollection();
		array.add("foo");
		array.add("bar");
		array.add("key");
		assertFalse(array.contains(null));
	}
	
	/*
	 * Now that we know size, contains, add and get all work together, 
	 * we can test the three remaining constructors. 
	 */
	
	@Test
	void collectionConstructorWorksForFull() {
		LinkedListIndexedCollection source = new LinkedListIndexedCollection();
		source.add("foo");
		source.add("bar");
		source.add("key");
		LinkedListIndexedCollection dest = new LinkedListIndexedCollection(source);
		
		assertEquals(3, dest.size());
		assertEquals("foo", dest.get(0));
		assertEquals("key", dest.get(2));
		assertThrows(IndexOutOfBoundsException.class, ()->{
			dest.get(3);
		});
		assertTrue(dest.contains("bar"));
	}
	
	
	/*
	 * Finally, we can formally test all the remaining methods:
	 * addAll (tested through constructors)
	 * forEach (tested through constructors)
	 * get (partially tested, border cases remaining)
	 * isEmpty
	 * remove(Object)
	 * toArray
	 * clear
	 * insert(Object, int)
	 * indexOf(Object)
	 * remove(int)
	 */
	
	
	@Test
	void addAllToFullCollection() {
		LinkedListIndexedCollection src = new LinkedListIndexedCollection();
		LinkedListIndexedCollection dest = new LinkedListIndexedCollection();
		src.add("foo");
		src.add("bar");
		src.add("bee");
		dest.add("key");
		dest.add("yoo");
		dest.addAll(src);
		assertTrue(dest.contains("foo"));
		assertTrue(dest.contains("bar"));
		assertEquals("bee", dest.get(4));
		assertEquals(5, dest.size());
	}
	@Test
	void addAllFromNull() {
		LinkedListIndexedCollection dest = new LinkedListIndexedCollection();
		assertThrows(NullPointerException.class, ()-> {
			dest.addAll(null);
		});
	}
	
	@Test
	void forEachIteratesCorrectly() {
		class CountProcessor extends Processor {
			public int accessed = 0;
			@Override
			public void process(Object value) {
				accessed+=1;
			}
		}
		CountProcessor proc = new CountProcessor();
		
		LinkedListIndexedCollection source = new LinkedListIndexedCollection();
		source.add("foo");
		source.add("bar");
		source.add("key");
		
		source.forEach(proc);
		assertEquals(3, proc.accessed);
	}
	@Test
	void forEachIteratesInOrder() {
		class CheckProcessor extends Processor {
			public String[] words = {"foo", "bar", "key"};
			public int isCorrect = 0;
			public int index = 0;
			@Override
			public void process(Object value) {
				if(value.equals(words[index])) {
					isCorrect++;
				}
				index++;
			}
		}
		CheckProcessor cProc = new CheckProcessor();
		
		LinkedListIndexedCollection source = new LinkedListIndexedCollection();
		source.add("foo");
		source.add("bar");
		source.add("key");
		
		source.forEach(cProc);
		assertEquals(cProc.isCorrect, 3);
	}
	
	@Test
	void getThrowsUnderLimit() {
		LinkedListIndexedCollection source = new LinkedListIndexedCollection();
		source.add("foo");
		source.add("bar");
		source.add("key");
		assertThrows(IndexOutOfBoundsException.class, ()-> {
			source.get(-1);
		});
	}
	@Test
	void getThrowsForEmpty() {
		LinkedListIndexedCollection source = new LinkedListIndexedCollection();
		assertThrows(IndexOutOfBoundsException.class, () -> {
			source.get(0);
		});
	}
	
	@Test
	void testIsEmpty() {
		LinkedListIndexedCollection source = new LinkedListIndexedCollection();
		assertTrue(source.isEmpty());
		source.add("foo");
		source.add("bar");
		source.add("key");
		assertFalse(source.isEmpty());
	}
	
	@Test
	void removeExistingObject() {
		LinkedListIndexedCollection source = new LinkedListIndexedCollection();
		source.add("foo");
		source.add("bar");
		source.add("key");
		source.add("pen");
		assertTrue(source.remove("bar"));
		assertFalse(source.contains("bar"));
		assertEquals(3, source.size());
		assertThrows(IndexOutOfBoundsException.class, () -> {
			source.get(3);
		});
	}
	
	@Test
	void removeNonexistantObject() {
		LinkedListIndexedCollection source = new LinkedListIndexedCollection();
		source.add("foo");
		source.add("bar");
		source.add("key");
		assertFalse(source.remove("yoo"));
	}
	@Test
	void removeNull() {
		LinkedListIndexedCollection source = new LinkedListIndexedCollection();
		source.add("foo");
		source.add("bar");
		source.add("key");
		assertFalse(source.remove(null));
	}
	
	@Test
	void testToArray() {
		LinkedListIndexedCollection source = new LinkedListIndexedCollection();
		source.add("foo");
		source.add("bar");
		source.add("key");
		Object[] dest = source.toArray();
		assertEquals("foo", dest[0]);
		assertEquals("bar", dest[1]);
		assertEquals("key", dest[2]);
		assertEquals(3, dest.length);
	}
	
	@Test
	void testClear() {
		LinkedListIndexedCollection source = new LinkedListIndexedCollection();
		source.add("foo");
		source.add("bar");
		source.add("key");
		source.clear();
		assertEquals(0, source.size());
		assertThrows(IndexOutOfBoundsException.class, () -> {
			source.get(0);
		});
	}
	@Test
	void testClearOnEmpty() {
		LinkedListIndexedCollection source = new LinkedListIndexedCollection();
		source.clear();
		assertEquals(0, source.size());
		assertThrows(IndexOutOfBoundsException.class, () -> {
			source.get(0);
		});
	}
	
	@Test
	void insertNull() {
		LinkedListIndexedCollection source = new LinkedListIndexedCollection();
		source.add("foo");
		source.add("bar");
		source.add("key");
		assertThrows(NullPointerException.class, () -> {
			source.insert(null, 1);
		});
	}
	@Test
	void insertBelowRange() {
		LinkedListIndexedCollection source = new LinkedListIndexedCollection();
		source.add("foo");
		source.add("bar");
		source.add("key");
		assertThrows(IndexOutOfBoundsException.class, () -> {
			source.insert("val", -1);
		});
	}
	@Test
	void insertAboveRange() {
		LinkedListIndexedCollection source = new LinkedListIndexedCollection();
		source.add("foo");
		source.add("bar");
		source.add("key");
		assertThrows(IndexOutOfBoundsException.class, () -> {
			source.insert("val", source.size()+1);
		});
	}
	@Test
	void insertAtZero() {
		LinkedListIndexedCollection source = new LinkedListIndexedCollection();
		source.add("foo");
		source.add("bar");
		source.add("key");
		source.insert("val", 0);
		assertEquals("val", source.get(0));
		assertEquals("foo", source.get(1));
		assertEquals("key", source.get(3));
		assertEquals(4, source.size());
	}
	@Test
	void insertAtEnd() {
		LinkedListIndexedCollection source = new LinkedListIndexedCollection();
		source.add("foo");
		source.add("bar");
		source.add("key");
		source.insert("val", source.size());
		assertEquals("val", source.get(3));
		assertEquals("foo", source.get(0));
		assertEquals("key", source.get(2));
		assertEquals(4, source.size());
	}
	@Test
	void insertAtPreviousLast() {
		LinkedListIndexedCollection source = new LinkedListIndexedCollection();
		source.add("foo");
		source.add("bar");
		source.add("key");
		source.insert("val", 2);
		assertEquals("val", source.get(2));
		assertEquals("foo", source.get(0));
		assertEquals("key", source.get(3));
		assertEquals(4, source.size());
	}
	@Test
	void insertInMiddle() {
		LinkedListIndexedCollection source = new LinkedListIndexedCollection();
		source.add("foo");
		source.add("bar");
		source.add("key");
		source.add("yoo");
		source.insert("val", 2);
		assertEquals("val", source.get(2));
		assertEquals("foo", source.get(0));
		assertEquals("key", source.get(3));
		assertEquals("yoo", source.get(4));
		assertEquals(5, source.size());
	}
	
	@Test
	void indexOfNull() {
		LinkedListIndexedCollection array = new LinkedListIndexedCollection();
		array.add("foo");
		assertEquals(-1, array.indexOf(null));
	}
	@Test
	void indexOfExisting() {
		LinkedListIndexedCollection array = new LinkedListIndexedCollection();
		array.add("foo");
		array.add("bar");
		array.add("key");
		assertEquals(1, array.indexOf("bar"));
	}
	@Test
	void indexOfNonexistant() {
		LinkedListIndexedCollection array = new LinkedListIndexedCollection();
		array.add("foo");
		array.add("bar");
		array.add("key");
		assertEquals(-1, array.indexOf("val"));
	}
	
	@Test
	void removeIntBelowRange() {
		LinkedListIndexedCollection array = new LinkedListIndexedCollection();
		array.add("foo");
		array.add("bar");
		array.add("key");
		assertThrows(IndexOutOfBoundsException.class, () -> {
			array.remove(-1);
		});
	}
	@Test
	void removeIntAboveRange() {
		LinkedListIndexedCollection array = new LinkedListIndexedCollection();
		array.add("foo");
		array.add("bar");
		array.add("key");
		assertThrows(IndexOutOfBoundsException.class, () -> {
			array.remove(3);
		});
	}
	@Test
	void removeIntZero() {
		LinkedListIndexedCollection array = new LinkedListIndexedCollection();
		array.add("foo");
		array.add("bar");
		array.add("key");
		array.remove(0);
		assertEquals(2, array.size());
		assertEquals("bar", array.get(0));
		assertFalse(array.contains("foo"));
	}
}
