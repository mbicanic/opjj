 package hr.fer.zemris.java.hw02;

import static org.junit.jupiter.api.Assertions.*;

import java.text.NumberFormat;

import org.junit.jupiter.api.Test;
import static hr.fer.zemris.java.hw02.ComplexNumber.*;

@SuppressWarnings("unused")
class ComplexNumberTest {

	/*
	 * Constructor tests.
	 */
	@Test
	void constructorZeroes() {
		ComplexNumber cpn = new ComplexNumber(0,0);
		assertEquals(0, cpn.getReal());
		assertEquals(0, cpn.getImaginary());
	}
	@Test
	void constructorPositives() {
		ComplexNumber cpn1 = new ComplexNumber(1,0);
		assertEquals(1, cpn1.getReal());
		assertEquals(0, cpn1.getImaginary());
		
		ComplexNumber cpn2 = new ComplexNumber(0,1);
		assertEquals(0, cpn2.getReal());
		assertEquals(1, cpn2.getImaginary());
		
		ComplexNumber cpn3 = new ComplexNumber(1,1);
		assertEquals(1, cpn3.getReal());
		assertEquals(1, cpn3.getImaginary());
		
		ComplexNumber cpn4 = new ComplexNumber(2.51, 3.14);
		assertEquals(2.51, cpn4.getReal());
		assertEquals(3.14, cpn4.getImaginary());
		
		ComplexNumber cpn5 = new ComplexNumber(0, 3.14);
		assertEquals(0, cpn5.getReal());
		assertEquals(3.14, cpn5.getImaginary());
		
		ComplexNumber cpn6 = new ComplexNumber(2.51, 0);
		assertEquals(2.51, cpn6.getReal());
		assertEquals(0, cpn6.getImaginary());
	}
	
	@Test
	void constructorNegativesA() {
		ComplexNumber cpn1 = new ComplexNumber(-1,0);
		assertEquals(-1, cpn1.getReal());
		assertEquals(0, cpn1.getImaginary());

		ComplexNumber cpn2 = new ComplexNumber(0,-1);
		assertEquals(0, cpn2.getReal());
		assertEquals(-1, cpn2.getImaginary());

		ComplexNumber cpn3 = new ComplexNumber(-1,-1);
		assertEquals(-1, cpn3.getReal());
		assertEquals(-1, cpn3.getImaginary());
		
		ComplexNumber cpn4 = new ComplexNumber(-2.51, -3.14);
		assertEquals(-2.51, cpn4.getReal());
		assertEquals(-3.14, cpn4.getImaginary());

		ComplexNumber cpn5 = new ComplexNumber(0, -3.14);
		assertEquals(0, cpn5.getReal());
		assertEquals(-3.14, cpn5.getImaginary());

		ComplexNumber cpn6 = new ComplexNumber(-2.51, 0);
		assertEquals(-2.51, cpn6.getReal());
		assertEquals(0, cpn6.getImaginary());
	}
	
	/*
	 * Now that we are sure the constructors work, we should
	 * test the getters for both the Cartesian and polar 
	 * coordinates since they are used in every other test.
	 */
	@Test
	void testGetCartesianA() {
		ComplexNumber cpn1 = new ComplexNumber(0,0);
		assertTrue(doubleEquals(cpn1.getReal(), 0));
		assertTrue(doubleEquals(cpn1.getImaginary(), 0));
		
		ComplexNumber cpn2 = new ComplexNumber(1,0);
		assertTrue(doubleEquals(cpn2.getReal(), 1));
		assertTrue(doubleEquals(cpn2.getImaginary(), 0));

		ComplexNumber cpn3 = new ComplexNumber(0,1);
		assertTrue(doubleEquals(cpn3.getReal(), 0));
		assertTrue(doubleEquals(cpn3.getImaginary(), 1));

		ComplexNumber cpn4 = new ComplexNumber(-1,0);
		assertTrue(doubleEquals(cpn4.getReal(), -1));
		assertTrue(doubleEquals(cpn4.getImaginary(), 0));

		ComplexNumber cpn5 = new ComplexNumber(0,-1);
		assertTrue(doubleEquals(cpn5.getReal(), 0));
		assertTrue(doubleEquals(cpn5.getImaginary(), -1));

		ComplexNumber cpn6 = new ComplexNumber(1,1);
		assertTrue(doubleEquals(cpn6.getReal(), 1));
		assertTrue(doubleEquals(cpn6.getImaginary(), 1));
	}	
	@Test
	void testGetCartesianB() {
		ComplexNumber cpn1 = new ComplexNumber(-1,1);
		assertTrue(doubleEquals(cpn1.getReal(), -1));
		assertTrue(doubleEquals(cpn1.getImaginary(), 1));

		ComplexNumber cpn2 = new ComplexNumber(1,-1);
		assertTrue(doubleEquals(cpn2.getReal(), 1));
		assertTrue(doubleEquals(cpn2.getImaginary(), -1));

		ComplexNumber cpn3 = new ComplexNumber(-1,-1);
		assertTrue(doubleEquals(cpn3.getReal(), -1));
		assertTrue(doubleEquals(cpn3.getImaginary(), -1));

		ComplexNumber cpn4 = new ComplexNumber(1,-0.0000001);
		assertTrue(doubleEquals(cpn4.getReal(), 1));
		assertTrue(doubleEquals(cpn4.getImaginary(), 0));

		ComplexNumber cpn5 = new ComplexNumber(Math.sqrt(3)/2, 0.5);
		assertTrue(doubleEquals(cpn5.getReal(), Math.sqrt(3)/2));
		assertTrue(doubleEquals(cpn5.getImaginary(), 0.5));

		ComplexNumber cpn6 = new ComplexNumber(0.5,Math.sqrt(3)/2);
		assertTrue(doubleEquals(cpn6.getReal(), 0.5));
		assertTrue(doubleEquals(cpn6.getImaginary(), Math.sqrt(3)/2));
	}
	@Test
	void testGetPolarsA() {
		ComplexNumber cpn1 = new ComplexNumber(0,0);
		assertTrue(doubleEquals(cpn1.getMagnitude(), 0));
		assertTrue(doubleEquals(cpn1.getAngle(),0));

		ComplexNumber cpn2 = new ComplexNumber(1,0);
		assertTrue(doubleEquals(cpn2.getMagnitude(), 1));
		assertTrue(doubleEquals(cpn2.getAngle(), 0));

		ComplexNumber cpn3 = new ComplexNumber(0,1);
		assertTrue(doubleEquals(cpn3.getMagnitude(), 1));
		assertTrue(doubleEquals(cpn3.getAngle(), Math.PI/2));

		ComplexNumber cpn4 = new ComplexNumber(-1,0);
		assertTrue(doubleEquals(cpn4.getMagnitude(), 1));
		assertTrue(doubleEquals(cpn4.getAngle(), Math.PI));

		ComplexNumber cpn5 = new ComplexNumber(0,-1);
		assertTrue(doubleEquals(cpn5.getMagnitude(), 1));
		assertTrue(doubleEquals(cpn5.getAngle(), 3*Math.PI/2));

		ComplexNumber cpn6 = new ComplexNumber(1,1);
		assertTrue(doubleEquals(cpn6.getMagnitude(), Math.sqrt(2)));
		assertTrue(doubleEquals(cpn6.getAngle(), Math.PI/4));
	}
	@Test
	void testGetPolarsB() {
		ComplexNumber cpn1 = new ComplexNumber(-1,1);
		assertTrue(doubleEquals(cpn1.getMagnitude(), Math.sqrt(2)));
		assertTrue(doubleEquals(cpn1.getAngle(), (3*Math.PI)/4));

		ComplexNumber cpn2 = new ComplexNumber(1,-1);
		assertTrue(doubleEquals(cpn2.getMagnitude(), Math.sqrt(2)));
		assertTrue(doubleEquals(cpn2.getAngle(), 7*Math.PI/4));

		ComplexNumber cpn3 = new ComplexNumber(-1,-1);
		assertTrue(doubleEquals(cpn3.getMagnitude(), Math.sqrt(2)));
		assertTrue(doubleEquals(cpn3.getAngle(), 5*Math.PI/4));

		ComplexNumber cpn4 = new ComplexNumber(1,-0.0000001);
		assertTrue(doubleEquals(cpn4.getMagnitude(), 1));
		assertTrue(doubleEquals(cpn4.getAngle(), 0));

		ComplexNumber cpn5 = new ComplexNumber(Math.sqrt(3)/2, 0.5);
		assertTrue(doubleEquals(cpn5.getMagnitude(), 1));
		assertTrue(doubleEquals(cpn5.getAngle(), Math.PI/6));

		ComplexNumber cpn6 = new ComplexNumber(0.5,Math.sqrt(3)/2);
		assertTrue(doubleEquals(cpn6.getMagnitude(), 1));
		assertTrue(doubleEquals(cpn6.getAngle(), Math.PI/3));
	}
	
	/*
	 * Tests for factory methods.
	 */
	@Test
	void testFromReal() {
		ComplexNumber cpn = ComplexNumber.fromReal(2.5);
		assertEquals(2.5, cpn.getReal());
		assertEquals(0, cpn.getImaginary());
		cpn = ComplexNumber.fromReal(-11);
		assertEquals(-11, cpn.getReal());
	}
	@Test
	void testFromImaginary() {
		ComplexNumber cpn = ComplexNumber.fromImaginary(2.5);
		assertEquals(2.5, cpn.getImaginary());
		assertEquals(0, cpn.getReal());
		cpn = ComplexNumber.fromImaginary(-11);
		assertEquals(-11, cpn.getImaginary());
	}

	@Test
	void fromMagAngleTestsA() {
		ComplexNumber cpn1 = ComplexNumber.fromMagnitudeAndAngle(1, 0);
		assertEquals(1, cpn1.getMagnitude());
		assertEquals(0, cpn1.getAngle());
		assertTrue(doubleEquals(1, cpn1.getReal()));
		assertTrue(doubleEquals(0, cpn1.getImaginary()));
		
		ComplexNumber cpn2 = ComplexNumber.fromMagnitudeAndAngle(1, Math.PI/2);
		assertEquals(1, cpn2.getMagnitude());
		assertEquals(Math.PI/2, cpn2.getAngle());
		assertTrue(doubleEquals(0, cpn2.getReal()));
		assertTrue(doubleEquals(1, cpn2.getImaginary()));
		
		ComplexNumber cpn3 = ComplexNumber.fromMagnitudeAndAngle(1, Math.PI);
		assertEquals(1, cpn3.getMagnitude());
		assertEquals(Math.PI, cpn3.getAngle());
		assertTrue(doubleEquals(-1, cpn3.getReal()));
		assertTrue(doubleEquals(0, cpn3.getImaginary()));
		
		ComplexNumber cpn4 = ComplexNumber.fromMagnitudeAndAngle(1, 1.5*Math.PI);
		assertEquals(1, cpn4.getMagnitude());
		assertEquals(1.5*Math.PI, cpn4.getAngle());
		assertTrue(doubleEquals(0, cpn4.getReal()));
		assertTrue(doubleEquals(-1, cpn4.getImaginary()));
	}
	
	@Test
	void fromMagAngleTestsB() {
		ComplexNumber cpn1 = ComplexNumber.fromMagnitudeAndAngle(1, 2*Math.PI);
		assertEquals(1, cpn1.getMagnitude());
		assertTrue(doubleEquals(0, cpn1.getAngle()));
		assertTrue(doubleEquals(1, cpn1.getReal()));
		assertTrue(doubleEquals(0, cpn1.getImaginary()));

		ComplexNumber cpn2 = ComplexNumber.fromMagnitudeAndAngle(1, Math.PI/6);
		assertEquals(1, cpn2.getMagnitude());
		assertTrue(doubleEquals(Math.PI/6, cpn2.getAngle()));
		assertTrue(doubleEquals(Math.sqrt(3)/2, cpn2.getReal()));
		assertTrue(doubleEquals(0.5, cpn2.getImaginary()));
		
		ComplexNumber cpn3 = ComplexNumber.fromMagnitudeAndAngle(1, 4*Math.PI);
		assertEquals(1, cpn3.getMagnitude());
		assertTrue(doubleEquals(0, cpn3.getAngle()));
		assertTrue(doubleEquals(1, cpn3.getReal()));
		assertTrue(doubleEquals(0, cpn3.getImaginary()));
	}
	
	/*
	 * Testing the parse method.
	 */
	@Test
	void parseNull() {
		assertThrows(NullPointerException.class, () -> {
			ComplexNumber cpn = ComplexNumber.parse(null);
		});
	}
	@Test
	void parseEmpty() {
		assertThrows(IllegalArgumentException.class, () -> {
			ComplexNumber cpn = ComplexNumber.parse("");
		});
	}
	@Test
	void parseInvalids() {
		assertThrows(NumberFormatException.class, () -> {
			ComplexNumber cpn = ComplexNumber.parse("ia");
		});
		assertThrows(NumberFormatException.class, () -> {
			ComplexNumber cpn = ComplexNumber.parse("+9i2");
		});
		assertThrows(NumberFormatException.class, () -> {
			ComplexNumber cpn = ComplexNumber.parse("14+ai");
		});
		assertThrows(NumberFormatException.class, () -> {
			ComplexNumber cpn = ComplexNumber.parse("a82.1-i");
		});
		assertThrows(NumberFormatException.class, () -> {
			ComplexNumber cpn = ComplexNumber.parse("12+3iz");
		});
		assertThrows(NumberFormatException.class, () -> {
			ComplexNumber cpn = ComplexNumber.parse("11-2k");
		});
		assertThrows(NumberFormatException.class, () -> {
			ComplexNumber cpn = ComplexNumber.parse("11-i2i");
		});
		assertThrows(NumberFormatException.class, () -> {
			ComplexNumber cpn = ComplexNumber.parse("1..0-0i");
		});
		assertThrows(NumberFormatException.class, () -> {
			ComplexNumber cpn = ComplexNumber.parse("1.5.0-1.0.5i");
		});
	}
	
	@Test
	void parseOnlyI() {
		ComplexNumber cpn1 = ComplexNumber.parse("i");
		ComplexNumber cpn2 = ComplexNumber.parse("+i");
		ComplexNumber cpn3 = ComplexNumber.parse("1i");
		ComplexNumber cpn4 = ComplexNumber.parse("+1i");
		assertEquals(new ComplexNumber(0,1), cpn1);
		assertEquals(new ComplexNumber(0,1), cpn2);
		assertEquals(new ComplexNumber(0,1), cpn3);
		assertEquals(new ComplexNumber(0,1), cpn4);
	}
	@Test
	void parseOnlyMinusI() {
		ComplexNumber cpn1 = ComplexNumber.parse("-i");
		ComplexNumber cpn2 = ComplexNumber.parse("-1i");
		assertEquals(new ComplexNumber(0,-1), cpn1);
		assertEquals(new ComplexNumber(0,-1), cpn2);
	}
	@Test
	void parseOnlyPositiveNumber() {
		ComplexNumber cpn1 = ComplexNumber.parse("1");
		ComplexNumber cpn2 = ComplexNumber.parse("+1");
		ComplexNumber cpn3 = ComplexNumber.parse("6");
		ComplexNumber cpn4 = ComplexNumber.parse("+6");
		ComplexNumber cpn5 = ComplexNumber.parse("11.19");
		ComplexNumber cpn6 = ComplexNumber.parse("+11.19");
		assertEquals(new ComplexNumber(1,0), cpn1);
		assertEquals(new ComplexNumber(1,0), cpn2);
		assertEquals(new ComplexNumber(6,0), cpn3);
		assertEquals(new ComplexNumber(6,0), cpn4);
		assertEquals(new ComplexNumber(11.19,0), cpn5);
		assertEquals(new ComplexNumber(11.19,0), cpn6);
	}
	@Test
	void parseOnlyNegativeNumber() {
		ComplexNumber cpn1 = ComplexNumber.parse("-1");
		ComplexNumber cpn2 = ComplexNumber.parse("-6");
		ComplexNumber cpn3 = ComplexNumber.parse("-11.19");
		assertEquals(new ComplexNumber(-1,0), cpn1);
		assertEquals(new ComplexNumber(-6,0), cpn2);
		assertEquals(new ComplexNumber(-11.19,0), cpn3);
	}
	@Test
	void parsePositivePositive() {
		ComplexNumber cpn1 = ComplexNumber.parse("2+3i");
		ComplexNumber cpn2 = ComplexNumber.parse("+2+3i");
		ComplexNumber cpn3 = ComplexNumber.parse("17.54+8.12i");
		ComplexNumber cpn4 = ComplexNumber.parse("+17.54+8.12i");
		ComplexNumber cpn5 = ComplexNumber.parse("0+0i");
		assertEquals(new ComplexNumber(2,3), cpn1);
		assertEquals(new ComplexNumber(2,3), cpn2);
		assertEquals(new ComplexNumber(17.54, 8.12), cpn3);
		assertEquals(new ComplexNumber(17.54, 8.12), cpn4);
		assertEquals(new ComplexNumber(0,0), cpn5);
	}
	@Test
	void parsePositiveNegative() {
		ComplexNumber cpn1 = ComplexNumber.parse("2-3i");
		ComplexNumber cpn2 = ComplexNumber.parse("+2-3i");
		ComplexNumber cpn3 = ComplexNumber.parse("17.54-8.12i");
		ComplexNumber cpn4 = ComplexNumber.parse("+17.54-8.12i");
		assertEquals(new ComplexNumber(2,-3), cpn1);
		assertEquals(new ComplexNumber(2,-3), cpn2);
		assertEquals(new ComplexNumber(17.54, -8.12), cpn3);
		assertEquals(new ComplexNumber(17.54, -8.12), cpn4);
	}
	@Test
	void parseNegativeNegative() {
		ComplexNumber cpn1 = ComplexNumber.parse("-2-3i");
		ComplexNumber cpn2 = ComplexNumber.parse("-17.54-8.12i");
		ComplexNumber cpn3 = ComplexNumber.parse("  -17.54-8.12i  ");
		assertEquals(new ComplexNumber(-2,-3), cpn1);
		assertEquals(new ComplexNumber(-17.54, -8.12), cpn2);
		assertEquals(new ComplexNumber(-17.54, -8.12), cpn3);
	}
	@Test
	void parseNegativePositive() {
		ComplexNumber cpn1 = ComplexNumber.parse("-2+3i");
		ComplexNumber cpn2 = ComplexNumber.parse("-17.54+8.12i");
		assertEquals(new ComplexNumber(-2,3), cpn1);
		assertEquals(new ComplexNumber(-17.54, 8.12), cpn2);
	}
	
	@Test 
	void testNormalise() {
		assertTrue(doubleEquals(Math.PI/2, normaliseAngleTo2Pi(5*Math.PI/2)));
		assertTrue(doubleEquals(0, normaliseAngleTo2Pi(2*Math.PI)));
		assertTrue(doubleEquals(0, normaliseAngleTo2Pi(2*Math.PI + 1e-7)));
		assertTrue(doubleEquals(3*Math.PI/2, normaliseAngleTo2Pi(-Math.PI/2)));
		assertTrue(doubleEquals(0, normaliseAngleTo2Pi(-1e-7)));
		assertTrue(Math.PI == normaliseAngleTo2Pi(Math.PI));
		
		assertFalse(doubleEquals(0, normaliseAngleTo2Pi(-9e-5)));
		assertFalse(doubleEquals(0, normaliseAngleTo2Pi(2*Math.PI+11e-6)));
		
		assertTrue(doubleEquals(normaliseAngleTo2Pi(11e-6), normaliseAngleTo2Pi(2*Math.PI+11e-6)));
	}
	
	/*
	 * Getter methods are tested within all the factory and constructors methods.
	 * Writing tests for them would result in code duplication, as they would
	 * be identical to some of the tests already written.
	 * 
	 * Arithmetic methods tests.
	 */
	@Test
	void addNullThrows() {
		ComplexNumber cpn1 = new ComplexNumber(5,10);
		assertThrows(NullPointerException.class, () -> {
			cpn1.add(null);
		});
	}
	@Test
	void addZeroGivesSame() {
		ComplexNumber cpn1 = new ComplexNumber(5,6);
		ComplexNumber cpn2 = new ComplexNumber(0,0);
		ComplexNumber res = cpn1.add(cpn2);
		assertEquals(5, cpn1.getReal());
		assertEquals(6, cpn1.getImaginary());
		assertEquals(5, res.getReal());
		assertEquals(6, res.getImaginary());
	}
	@Test
	void addOneGivesNew() {
		ComplexNumber cpn1 = new ComplexNumber(5,5);
		ComplexNumber cpn2 = cpn1.add(new ComplexNumber(1, 0));
		assertFalse(cpn2==cpn1); //not pointing to same memory -> gives now
		assertNotEquals(cpn1, cpn2); 	//not equal
		assertEquals(6, cpn2.getReal());
		assertEquals(5, cpn1.getReal());
	}
	@Test
	void addAnotherWorks() {
		ComplexNumber cpn1 = new ComplexNumber(10.75, 20.5);
		ComplexNumber cpn2 = new ComplexNumber(5.25, 3.625);
		assertEquals(new ComplexNumber(16,24.125), cpn1.add(cpn2));
	}
	
	 // sub method is realized through adding a negative value of the handed
	 // number - border cases already tested in add
	@Test
	void subAnotherWorks() {
		ComplexNumber cpn1 = new ComplexNumber(10.78, 22.54);
		ComplexNumber cpn2 = new ComplexNumber(5.38, 7.29);
		assertEquals(new ComplexNumber(5.4,15.25),cpn1.sub(cpn2));
	}
	
	@Test
	void mulNullThrows() {
		ComplexNumber cpn1 = new ComplexNumber(5,10);
		assertThrows(NullPointerException.class, () -> {
			cpn1.mul(null);
		});
	}
	@Test
	void mulZeroNullifies() {
		ComplexNumber cpn1 = new ComplexNumber(5,10);
		ComplexNumber cpn2 = new ComplexNumber(0,0);
		assertEquals(new ComplexNumber(0, 0), cpn1.mul(cpn2));
	}
	@Test
	void indifferentToMulOne() {
		ComplexNumber cpn1 = new ComplexNumber(5,10);
		ComplexNumber cpn2 = new ComplexNumber(1,0);
		assertEquals(cpn1, cpn1.mul(cpn2));
	}
	@Test
	void mulImaginarySwaps() {
		ComplexNumber cpn1 = new ComplexNumber(5,10);
		ComplexNumber cpn2 = new ComplexNumber(0,1);
		assertEquals(new ComplexNumber(-10, 5), cpn1.mul(cpn2));
	}
	@Test
	void mulWorks() {
		ComplexNumber cpn1 = new ComplexNumber(2,2);
		ComplexNumber cpn2 = new ComplexNumber(-1.5, 14);
		ComplexNumber cpn3 = new ComplexNumber(-6, -3.14);
		ComplexNumber cpn4 = new ComplexNumber(3.14, -1.125);
		assertEquals(new ComplexNumber(-31,25), cpn1.mul(cpn2));
		assertEquals(new ComplexNumber(-12+6.28, -6.28-12), cpn1.mul(cpn3));
		assertEquals(new ComplexNumber(6.28+2.25, -2.25+6.28), cpn1.mul(cpn4));
		assertEquals(new ComplexNumber(9+43.96, 4.71-84), cpn2.mul(cpn3));
		assertEquals(new ComplexNumber(-4.71+15.75, 1.6875+31.4+12.56 ), cpn2.mul(cpn4));
		assertEquals(new ComplexNumber(-18.84-3.5325, 6.75-9.8596), cpn3.mul(cpn4));
	}
	
	@Test
	void divNullThrows() {
		ComplexNumber cpn = new ComplexNumber(5,10);
		assertThrows(NullPointerException.class, () -> {
			cpn.div(null);
		});
		
	}
	@Test
	void divZeroWorks() {
		ComplexNumber cpn1 = new ComplexNumber(5,10);
		ComplexNumber cpn2 = new ComplexNumber(0,0);
		assertEquals(Double.NaN, cpn1.div(cpn2).getReal());
		assertEquals(Double.NaN, cpn1.div(cpn2).getImaginary());
		assertNotEquals(new ComplexNumber(Double.NaN, Double.NaN), cpn1.div(cpn2));
		assertNotEquals(new ComplexNumber(Double.POSITIVE_INFINITY, 0),
				new ComplexNumber(Double.NEGATIVE_INFINITY, 0));
		assertNotEquals(new ComplexNumber(Double.POSITIVE_INFINITY, 0),
				new ComplexNumber(5, 0));
		
	}
	@Test
	void indifferentToDivOne() {
		ComplexNumber cpn1 = new ComplexNumber(5,10);
		ComplexNumber cpn2 = new ComplexNumber(1,0);
		assertEquals(cpn1, cpn1.div(cpn2));
	}
	@Test
	void divWorks() {
		ComplexNumber num1 = new ComplexNumber(-31,25);
		ComplexNumber num2 = new ComplexNumber(-12+6.28, -6.28-12);
		ComplexNumber num3 = new ComplexNumber(6.28+2.25, -2.25+6.28);
		ComplexNumber num4 = new ComplexNumber(9+43.96, 4.71-84);
		ComplexNumber num5 = new ComplexNumber(-4.71+15.75, 1.6875+31.4+12.56 );
		ComplexNumber num6 = new ComplexNumber(-18.84-3.5325, 6.75-9.8596);
		ComplexNumber den1 = new ComplexNumber(2,2);
		ComplexNumber den2 = new ComplexNumber(-1.5, 14);
		ComplexNumber den3 = new ComplexNumber(-6, -3.14);
		ComplexNumber den4 = new ComplexNumber(3.14, -1.125);
		
		assertEquals(den1, num1.div(den2));
		assertEquals(den2, num1.div(den1));
		assertEquals(den1, num2.div(den3));
		assertEquals(den3, num2.div(den1));
		assertEquals(den1, num3.div(den4));
		assertEquals(den4, num3.div(den1));
		
		assertEquals(den2, num4.div(den3));
		assertEquals(den3, num4.div(den2));
		assertEquals(den2, num5.div(den4));
		assertEquals(den4, num5.div(den2));
		
		assertEquals(den3, num6.div(den4));
		assertEquals(den4, num6.div(den3));
	}
	@Test
	void conjugateWorks() {
		ComplexNumber cpn1 = new ComplexNumber(0,0);
		ComplexNumber cpn2 = new ComplexNumber(0,1);
		ComplexNumber cpn3 = new ComplexNumber(1,0);
		ComplexNumber cpn4 = new ComplexNumber(-5, -5);
		assertEquals(cpn1, cpn1.conjugate());
		assertEquals(new ComplexNumber(0,-1), cpn2.conjugate());
		assertEquals(cpn3, cpn3.conjugate());
		assertEquals(new ComplexNumber(-5, 5), cpn4.conjugate());
	}
	
	@Test
	void powerThrows() {
		ComplexNumber cpn = new ComplexNumber(1,0);
		ComplexNumber cpn2 = new ComplexNumber(0,0);
		assertThrows(IllegalArgumentException.class, () -> {
			cpn.power(-1);
		});assertThrows(IllegalArgumentException.class, () -> {
			cpn2.power(0);
		});
	}
	@Test
	void toZeroPowerIsOne() {
		ComplexNumber cpn = new ComplexNumber(3,6);
		assertEquals(new ComplexNumber(1, 0), cpn.power(0));
		cpn = new ComplexNumber(0,1);
		assertEquals(new ComplexNumber(1, 0), cpn.power(0));
	}
	@Test
	void powerOfZeroIsZero() {
		ComplexNumber cpn = new ComplexNumber(0,0);
		assertEquals(cpn, cpn.power(5));
	}
	@Test
	void powerOfOneIsOne() {
		ComplexNumber cpn = new ComplexNumber(1,0);
		assertEquals(cpn, cpn.power(5));
		assertEquals(cpn, cpn.power(0));
	}
	@Test
	void toOnePowerIsSame() {
		ComplexNumber cpn = new ComplexNumber(3,6);
		assertEquals(cpn, cpn.power(1));
		cpn = new ComplexNumber(0,0);
		assertEquals(cpn, cpn.power(1));
	}
	@Test
	void powerTwoWorks() {
		ComplexNumber cpn = new ComplexNumber(1,1);
		assertEquals(
				ComplexNumber.fromMagnitudeAndAngle(2, Math.PI/2),
				cpn.power(2));
		assertEquals(cpn.mul(cpn), cpn.power(2));
	}
	@Test
	void powerFourWorks() {
		ComplexNumber cpn = new ComplexNumber(2,1);
		assertEquals(
				ComplexNumber.fromMagnitudeAndAngle(25, Math.atan2(1,2)*4),
				cpn.power(4));
	}
	
	@Test
	void rootThrows() {
		ComplexNumber cpn = new ComplexNumber(2,5);
		assertThrows(IllegalArgumentException.class, () -> {
			ComplexNumber[] roots = cpn.root(0);
		});
		assertThrows(IllegalArgumentException.class, () -> {
			ComplexNumber[] roots = cpn.root(-1);
		});
	}
	
	@Test
	void firstRootReturnsEqual() {
		ComplexNumber cpn = new ComplexNumber(2,5);
		assertEquals(cpn, cpn.root(1)[0]);
		assertEquals(1, cpn.root(1).length);
		cpn = new ComplexNumber(0,0);
		assertEquals(cpn, cpn.root(1)[0]);
		assertEquals(1, cpn.root(1).length);
		cpn = new ComplexNumber(0,1);
		assertEquals(cpn, cpn.root(1)[0]);
		assertEquals(1, cpn.root(1).length);
	}
	@Test
	void secondRoot() {
		ComplexNumber cpn = ComplexNumber.fromMagnitudeAndAngle(2, Math.PI/2);
		assertEquals(new ComplexNumber(1,1), cpn.root(2)[0]);
		assertEquals(new ComplexNumber(-1,-1), cpn.root(2)[1]);
	}
	@Test
	void fourthRoot() {
		ComplexNumber cpn = ComplexNumber.fromMagnitudeAndAngle(25, Math.atan2(1,2)*4);
		assertEquals(ComplexNumber.fromMagnitudeAndAngle(Math.sqrt(5), Math.atan2(1, 2)),
				cpn.root(4)[0]);
		assertEquals(ComplexNumber.fromMagnitudeAndAngle(Math.sqrt(5), Math.atan2(1, 2) + Math.PI/2),
				cpn.root(4)[1]);
		assertEquals(ComplexNumber.fromMagnitudeAndAngle(Math.sqrt(5), Math.atan2(1, 2) + Math.PI),
				cpn.root(4)[2]);
		assertEquals(ComplexNumber.fromMagnitudeAndAngle(Math.sqrt(5), Math.atan2(1, 2) + 3*Math.PI/2),
				cpn.root(4)[3]);
	}
	
	/*
	 * Equality methods.
	 */
	@Test
	void testEquals() {
		ComplexNumber cpn1 = new ComplexNumber(2,5);
		ComplexNumber cpn2 = new ComplexNumber(2+9e-7, 5+9e-7);
		ComplexNumber cpn3 = new ComplexNumber(2-11e-7, 5-11e-7);
		assertEquals(cpn1, cpn2);
		assertNotEquals(cpn1, cpn3);
		assertTrue(doubleEquals(cpn1.getReal(), cpn2.getReal()));
		assertTrue(doubleEquals(cpn1.getImaginary(), cpn2.getImaginary()));
		assertFalse(doubleEquals(cpn1.getReal(), cpn3.getReal()));
		assertFalse(doubleEquals(cpn1.getImaginary(), cpn3.getImaginary()));
	}
	@Test
	void testDoubleEquals() {
		double a = 3.1415;
		double b = 3.1415926;
		double c = 6.1234567;
		double d = 6.1234572;
		double e = 6.1234499;
		double tenPlusOne = 10.0000010;
		double tenPlusTwo = 10.0000022;
		double ten = 		10.0000000;
		assertTrue(doubleEquals(b, Math.PI));
		assertFalse(doubleEquals(a, Math.PI));
		assertTrue(doubleEquals(c, d));
		assertFalse(doubleEquals(c, e));
		assertTrue(doubleEquals(tenPlusOne, ten));
		assertFalse(doubleEquals(tenPlusTwo, tenPlusOne));
	}
	@Test
	void testToString() {
		String[] strs = new String[] {"i", "-i", "1.0", "-1.0", "2.0", "-2.0", "5.1i", "-5.1i", 
				"2.1+i", "2.1-i", "-2.1-i", "-2.1+i", "5.3+4.0i", "5.3-4.0i", "-5.3+4.0i", 
				"-5.3-4.0i"};
		ComplexNumber[] cpns = new ComplexNumber[] {
				new ComplexNumber(0,1),
				new ComplexNumber(0,-1),
				new ComplexNumber(1,0),
				new ComplexNumber(-1,0),
				new ComplexNumber(2, 0),
				new ComplexNumber(-2, 0),
				new ComplexNumber(0,5.1),
				new ComplexNumber(0,-5.1),
				new ComplexNumber(2.1,1),
				new ComplexNumber(2.1,-1),
				new ComplexNumber(-2.1,-1),
				new ComplexNumber(-2.1,1),
				new ComplexNumber(5.3, 4),
				new ComplexNumber(5.3, -4),
				new ComplexNumber(-5.3, 4),
				new ComplexNumber(-5.3, -4)};
		for(int i = 0; i < cpns.length; i++) {
			assertEquals(strs[i], cpns[i].toString());
		}
	}
}
