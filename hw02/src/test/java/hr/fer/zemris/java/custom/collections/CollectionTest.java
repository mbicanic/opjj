package hr.fer.zemris.java.custom.collections;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class CollectionTest {

	@Test
	void testAddAll() {
		Collection col = new Collection();
		Collection col1 = new ArrayIndexedCollection(col);
		assertEquals(0, col1.size());
		assertEquals(0, col.size());
		col1.add("foo");
		col1.add("bar");
		col1.add("key");
		col.addAll(col1);
		assertEquals(0, col.size());
	}
	
	@Test
	void testIsEmpty() {
		Collection col1 = new Collection();
		assertTrue(col1.isEmpty());
		col1.add("pero");
		assertTrue(col1.isEmpty());
	}
	
	@Test
	void testOtherMethods() {
		Collection col = new Collection();
		col.add("pero");
		col.add(2);
		col.add(Math.PI);
		assertFalse(col.contains(2));
		assertFalse(col.contains(Math.PI));
		assertFalse(col.remove("pero"));
		assertThrows(UnsupportedOperationException.class, ()->{
			col.toArray();
		});
		
		class LocalProc extends Processor {
			public int i;
			@Override
			public void process(Object o) {
				i++;
			}
		}
		LocalProc proc = new LocalProc();
		col.forEach(proc);
		assertEquals(0, proc.i);
	}
	
	@Test
	void testCheckValidIntegerBelow() {
		int lower = 0;
		int higher = 10;
		int desired = -1;
		assertThrows(IndexOutOfBoundsException.class, () -> {
			Collection.checkValidInteger(desired, lower, higher);
		});
	}
	@Test
	void testCheckValidIntegerAbove() {
		int lower = 0;
		int higher = 10;
		int desired = 11;
		assertThrows(IndexOutOfBoundsException.class, () -> {
			Collection.checkValidInteger(desired, lower, higher);
		});
	}
	@Test
	void testCheckValidIntegerExactLow() {
		int lower = 0;
		int higher = 10;
		int desired = 0;
		Collection.checkValidInteger(desired, lower, higher);
		//could throw an exception, if doesn't:
		int test = 0;
		assertEquals(0, test);
	}
	@Test
	void testCheckValidIntegerExactUp() {
		int lower = 0;
		int higher = 10;
		int desired = 10;
		Collection.checkValidInteger(desired, lower, higher);
		//could throw an exception, if doesn't:
		int test = 0;
		assertEquals(0, test);
	}
	@Test
	void testCheckValidIntegerWithin() {
		int lower = 0;
		int higher = 10;
		int desired = 5;
		Collection.checkValidInteger(desired, lower, higher);
		//could throw an exception, if doesn't:
		int test = 0;
		assertEquals(0, test);
	}
}
