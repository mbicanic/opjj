package hr.fer.zemris.java.custom.collections;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class ObjectStackTest {

	@Test
	void constructorState() {
		ObjectStack stack = new ObjectStack();
		assertEquals(0, stack.size());
	}
	
	@Test
	void pushingToStack() {
		ObjectStack stack = new ObjectStack();
		stack.push(5);
		assertEquals(1, stack.size());
		assertEquals(5, stack.pop());
	}
	@Test
	void pushingNull() {
		ObjectStack stack = new ObjectStack();
		assertThrows(NullPointerException.class, ()-> {
			stack.push(null);
		});
		assertEquals(0, stack.size());
	}
	
	@Test
	void poppingFromStack() {
		ObjectStack stack = new ObjectStack();
		stack.push(5);
		stack.push(7);
		assertEquals(2, stack.size());
		assertEquals(7, stack.pop());
		assertEquals(5, stack.pop());
	}
	@Test 
	void poppingFromEmpty() {
		ObjectStack stack = new ObjectStack();
		assertThrows(EmptyStackException.class, () -> {
			stack.pop();
		});
	}
	@Test
	void peekingAtStack() {
		ObjectStack stack = new ObjectStack();
		stack.push(5);
		stack.push(7);
		assertEquals(2, stack.size());
		assertEquals(7, stack.peek());
		assertEquals(7, stack.pop());
	}
	@Test 
	void peekingAtEmpty() {
		ObjectStack stack = new ObjectStack();
		assertThrows(EmptyStackException.class, () -> {
			stack.peek();
		});
	}
	
	@Test
	void clearStack() {
		ObjectStack stack = new ObjectStack();
		assertTrue(stack.isEmpty());
		stack.push(5);
		stack.push(7);
		stack.push("pero");
		assertFalse(stack.isEmpty());
		assertEquals(3, stack.size());
		stack.clear();
		assertTrue(stack.isEmpty());
		assertEquals(0, stack.size());
	}
}
