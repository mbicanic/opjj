package hr.fer.zemris.java.gui.prim;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class PrimListModelTest {

	private static PrimListModel plm;
	
	@BeforeEach
	void give() {
		plm = new PrimListModel();
	}
	
	@Test
	void testConstruction() {
		assertEquals(1, plm.getElementAt(0));
		assertEquals(1, plm.getSize());
	}
	
	@Test
	void checkGeneration() {
		int size = 1;
		int[] primes = {1,2,3,5,7,11};
		for(int i=0; i<6; i++) {
			assertEquals(size, plm.getSize());
			assertEquals(primes[i], plm.getElementAt(i));
			size++;
			plm.next();
		}
	}
	
	@Test
	void check100thPrime() {
		for(int i=0; i<100; i++) {
			plm.next();
		}
		assertEquals(101, plm.getSize());
		assertEquals(541, plm.getElementAt(100));
	}
	
}
