package hr.fer.zemris.java.gui.calc.entities;

import java.awt.Color;

import java.util.function.DoubleBinaryOperator;
import java.util.function.DoubleUnaryOperator;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

import hr.fer.zemris.java.gui.calc.model.CalculatorInputException;
import hr.fer.zemris.java.gui.calc.model.CalcModel;

/**
 * OperatorButton is a specification of a {@link JButton}, whose action 
 * is to set itself as the pending operation into the {@link CalcModel}.
 * If a pending operation already is set, that operation is applied to
 * the active operand and the value of the model, the result is stored
 * into the active operand of the model, and this operation is stored
 * as the pending operation of the model.
 * 
 * Since some binary operations, such as power|root, are invertable,
 * they receive two {@link DoubleUnaryOperator} objects at construction.
 * Whether the standard or inverted operation should be applied is 
 * determined at every press of the invertable binary operation button
 * based on the given {@link JCheckBox} given at construction.
 * 
 * @author Miroslav Bićanić
 */
public class OperatorButton extends JButton {
private static final long serialVersionUID = 1L;
	
	/**
	 * A constructor for an OperatorButton.
	 * 
	 * @param text the text to display on the button
	 * @param op the operation to perform when the button is not inverted
	 * @param cm the {@link CalcModel} from and to which to read and write data
	 * @param inverted the operation to perform when the button is inverted
	 * @param JCBInvert the {@link JCheckBox} determining which operation to use
	 */
	public OperatorButton(String text, DoubleBinaryOperator op, CalcModel cm,
			DoubleBinaryOperator inverted, JCheckBox JCBInvert) {
		super(text);
		setBackground(new Color(190,210,250));
		this.addActionListener(e->{
			try {
				DoubleBinaryOperator actual = JCBInvert.isSelected() ? inverted : op;
				if(!cm.isActiveOperandSet()) {
					cm.setActiveOperand(cm.getValue());
					cm.setPendingBinaryOperation(actual);
					cm.clear();
					return;
				} else if(cm.isActiveOperandSet()) {
					cm.setActiveOperand(
							cm.getPendingBinaryOperation().applyAsDouble(cm.getActiveOperand(), cm.getValue()));
					cm.setPendingBinaryOperation(actual);
					cm.clear();
					return;
				}
			} catch (UnsupportedOperationException | IllegalStateException | CalculatorInputException ex) {
				JFrame root = (JFrame) SwingUtilities.getRoot(this);
				JOptionPane.showMessageDialog(root, ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
			}
		});
	}
}
