package hr.fer.zemris.java.gui.prim;

import java.util.ArrayList;
import java.util.List;

import javax.swing.ListModel;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;

/**
 * PrimListModel is an implementation of the {@link ListModel} interface,
 * generating prime numbers on demand and remembering all the previously 
 * generated prime numbers.
 * 
 * On construction, the model contains only the number 1 as the first 
 * prime number.
 * 
 * @author Miroslav Bićanić
 */
public class PrimListModel implements ListModel<Integer> {
	/** A list of generated values. */
	private List<Integer> values;
	/** A list of listeners registered to this PrimListModel */
	private List<ListDataListener> listeners;
	
	/**
	 * A default constructor for a PrimListModel.
	 */
	public PrimListModel() {
		this.listeners = new ArrayList<>();
		this.values = new ArrayList<>();
		values.add(1);
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getSize() {
		return values.size();
	}
	/**
	 * {@inheritDoc}
	 */
	@Override
	public Integer getElementAt(int index) {
		return values.get(index);
	}
	/**
	 * {@inheritDoc}
	 */
	@Override
	public void addListDataListener(ListDataListener l) {
		listeners.add(l);
	}
	/**
	 * {@inheritDoc}
	 */
	@Override
	public void removeListDataListener(ListDataListener l) {
		listeners.remove(l);
	}
	
	/**
	 * Generates the next prime number and adds it to the list.
	 */
	public void next() {
		int pos = values.size();
		int val = values.get(values.size()-1);
		val++;
		while(!isPrime(val)) {
			val++;
		}
		values.add(val);
		
		ListDataEvent event = new ListDataEvent(this, ListDataEvent.INTERVAL_ADDED, pos, pos);
		for(ListDataListener l : listeners) {
			l.intervalAdded(event);
		}
	}
	
	/**
	 * Checks if a number is a prime number, using the simplest
	 * prime number detection algorithm.
	 * 
	 * @param number The number whose primality to check
	 * @return true if the given number is prime; false otherwise
	 */
	private boolean isPrime(int number) {
		if(number==2) {
			return true;
		}
		if(number%2==0) {
			return false;
		}
		for(int i=3; i<=Math.sqrt(number); i+=2) {
			if(number%i==0) {
				return false;
			}
		}
		return true;
	}
}