package hr.fer.zemris.java.gui.layouts;

/**
 * This exception must be thrown whenever an error occurs 
 * within the {@link CalcLayout}.
 * 
 * @author Miroslav Bićanić
 */
public class CalcLayoutException extends RuntimeException {
	private static final long serialVersionUID = 1L;
	
	/**
	 * Default constructor for a CalcLayoutException
	 */
	public CalcLayoutException() {
		super();
	}
	
	/**
	 * A constructor for a CalcLayoutException containing
	 * a description of the error that caused it.
	 * 
	 * @param message The description of the error causing the exception
	 */
	public CalcLayoutException(String message) {
		super(message);
	}

}
