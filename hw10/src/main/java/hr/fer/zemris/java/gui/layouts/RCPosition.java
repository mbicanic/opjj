package hr.fer.zemris.java.gui.layouts;

import java.util.Objects;

/**
 * RCPosition is a class representing a constraint for
 * the CalcLayout, determining where in the layout's
 * grid should an element be placed.
 * 
 * @author Miroslav Bićanić
 */
public class RCPosition {
	/** The row in which the component should be placed */
	private int row;
	/** The column in which the component should be placed */
	private int column;
	
	/**
	 * Constructor for an RCPosition
	 * @param row the row in which to place the component
	 * @param column the column in which to place the component
	 */
	public RCPosition(int row, int column) {
		this.row = row;
		this.column = column;
	}

	/**
	 * @return the row defined by this RCPosition
	 */
	public int getRow() {
		return row;
	}
	/**
	 * @return the column defined by this RCPosition
	 */
	public int getColumn() {
		return column;
	}

	/**
	 * Two RCPosition objects are equal if both their
	 * row and column are equal.
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof RCPosition))
			return false;
		RCPosition other = (RCPosition) obj;
		return column == other.column && row == other.row;
	}
	
	@Override
	public int hashCode() {
		return Objects.hash(column, row);
	}
	
	@Override
	public String toString() {
		return String.format("(%d,%d)",row,column);
	}
}
