package hr.fer.zemris.java.gui.charts;

/**
 * XYValue is a class modelling an (x,y) pair of integers,
 * to be added to a {@link BarChart}.
 * 
 * @author Miroslav Bićanić
 */
public class XYValue {
	/** The x value */
	private int x;
	/** The y value */
	private int y;
	
	/**
	 * Constructor for an XYValue
	 * @param x the x value
	 * @param y the y value
	 */
	public XYValue(int x, int y) {
		this.x = x;
		this.y = y;
	}
	/**
	 * @return the x value
	 */
	public int getX() {
		return x;
	}
	/**
	 * @return the y value
	 */
	public int getY() {
		return y;
	}
	
	/**
	 * {@inheritDoc}
	 * XYValue is represented in the format {@code (x,y)}
	 */
	@Override
	public String toString() {
		return String.format("(%d,%d)", x, y);
	}
}
