package hr.fer.zemris.java.gui.calc;

import java.awt.Color;
import java.awt.Container;
import java.util.Arrays;
import java.util.List;
import java.util.Stack;
import java.util.function.DoubleBinaryOperator;
import java.util.function.DoubleUnaryOperator;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;

import hr.fer.zemris.java.gui.calc.entities.NumberButton;
import hr.fer.zemris.java.gui.calc.entities.OperatorButton;
import hr.fer.zemris.java.gui.calc.entities.UnaryOpButton;
import hr.fer.zemris.java.gui.calc.model.CalcModel;
import hr.fer.zemris.java.gui.calc.model.CalcModelImpl;
import hr.fer.zemris.java.gui.calc.model.CalculatorInputException;
import hr.fer.zemris.java.gui.layouts.CalcLayout;
import hr.fer.zemris.java.gui.layouts.RCPosition;

/**
 * A simple calculator program.
 * 
 * The program relies on a separate model - {@link CalcModel},
 * which determines the behaviour of the calculator.
 * 
 * The calculator contains 10 buttons for the 10 digits of the
 * decimal number system, and several operations that can be 
 * performed on numbers. Alongside them there is a button for 
 * inserting a decimal point, as well as a button to swap the 
 * sign of the number.
 * 
 * Supported binary operations are: addition, subtraction,
 * multiplication, division, powers|n-th root.
 * 
 * Supported unary operations are: sin|arcsin, cos|arccos,
 * tan|arctan, ctg|arcctg, log|10^, ln|e^ and 1/x.
 * 
 * All operations separated with a vertical line (|) are invertable
 * operations. The corresponding button represents both functions,
 * depending if the 'inverted' checkbox is pressed.
 * 
 * The calculator also contains a stack, to which the currently
 * displayed value can be pushed, and from which a value can be
 * popped, replacing the currently displayed value.
 * 
 * Pressing the 'clr' button clears the currently displayed value,
 * while 'res' clears both the currenlty displayed value, and any
 * stored operands or operations.
 * 
 * The calculator does not support the order of operations. Instead,
 * the operations are performed in the order of writing.
 * 
 * @author Miroslav Bićanić
 */
public class Calculator extends JFrame {
	private static final long serialVersionUID = 1L;
	
	/** A list of buttons representing invertable operations */
	private List<JButton> invertable;
	/** The calculator's stack */
	private Stack<Double> stack;
	
	private static final DoubleUnaryOperator SIN = d->Math.sin(d);
	private static final DoubleUnaryOperator COS = d->Math.cos(d);
	private static final DoubleUnaryOperator TAN = d->Math.tan(d);
	private static final DoubleUnaryOperator CTG = d->1/Math.tan(d);
	private static final DoubleUnaryOperator ASIN = d->Math.asin(d);
	private static final DoubleUnaryOperator ACOS = d->Math.acos(d);
	private static final DoubleUnaryOperator ATAN = d->Math.atan(d);
	private static final DoubleUnaryOperator ACTG = d-> Math.PI/2 - Math.atan(d);
	
	private static final DoubleUnaryOperator LOG = d->Math.log10(d);
	private static final DoubleUnaryOperator TEN_POW = d->Math.pow(10, d);
	
	private static final DoubleUnaryOperator LN = d->Math.log(d);
	private static final DoubleUnaryOperator E_POW = d->Math.pow(Math.E, d);
	
	private static final DoubleUnaryOperator RECIPROCATE = d->1/d;
	
	private static final DoubleBinaryOperator ADD = (a,b)->a+b;
	private static final DoubleBinaryOperator SUB = (a,b)->a-b;
	private static final DoubleBinaryOperator MUL = (a,b)->a*b;
	private static final DoubleBinaryOperator DIV = (a,b)->a/b;
	private static final DoubleBinaryOperator POW = (a,b)->Math.pow(a, b);
	private static final DoubleBinaryOperator ROOT = (a,b)->Math.pow(a, 1/b);
	
	/** The {@link CalcModel} used by this Calculator */
	private CalcModel cm;
	
	/** 
	 * A constructor for a Calculator.
	 */
	public Calculator() {
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		setSize(500, 300);
		setTitle("JCalculator");
		stack = new Stack<>();
		this.cm = new CalcModelImpl();
		initGUI();
	}

	/**
	 * Method for GUI initialization.
	 */
	private void initGUI() {
		Container cp = getContentPane();
		cp.setLayout(new CalcLayout(5));
		
		JLabel display = new JLabel();
		display.setBackground(new Color(246,199,11));
		display.setOpaque(true);
		display.setFont(display.getFont().deriveFont(30f));
		display.setHorizontalAlignment(SwingConstants.RIGHT);
		cm.addCalcValueListener(m->{
			display.setText(cm.toString());
		});
		
		JCheckBox invert = new JCheckBox("inv");
		invert.addActionListener(e->{
			for(JButton b : invertable) {
				if(b instanceof UnaryOpButton) {
					UnaryOpButton uob = (UnaryOpButton)b;
					uob.invert();
				} else {
					b.setText(b.getText().equals("x^n") ? "x^(1/n)" : "x^n");
				}
			}
		});
		
		JButton equ = new JButton("=");
		equ.setBackground(new Color(190,210,250));
		equ.addActionListener(e->{
			if(cm.isActiveOperandSet()) {
				try {
					double v1 = cm.getActiveOperand();
					double v2 = cm.getValue();
					double res = cm.getPendingBinaryOperation().applyAsDouble(v1, v2);
					cm.clearAll();
					cm.setValue(res);
				} catch (IllegalStateException ex) {
					JOptionPane.showMessageDialog(this, ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
				} catch (NullPointerException ex) {
					JOptionPane.showMessageDialog(this, "Unexpected error occurred!", "Error", JOptionPane.ERROR_MESSAGE);
					//managed to provoke it once, couldn't reproduce it again, left it for precaution
				}
			} else {
				return; //pressing '=' when there is only one number leaves the number written
			}
		});
		
		setUpOperatorButtons(cp, invert);
		setUpMetaButtons(cp);
		setUpNumberButtons(cp);

		cp.add(display, new RCPosition(1, 1));
		cp.add(invert, new RCPosition(5,7));
		cp.add(equ, new RCPosition(1, 6));
	}
	
	/**
	 * Creates and adds all 10 digit buttons to the given
	 * {@link Container}. Apart from them, it also creates and
	 * adds the swap sign button and the decimal point button.
	 * 
	 * @param cp the {@link Container} to which to add the digits
	 */
	private void setUpNumberButtons(Container cp) {
		cp.add(new NumberButton(0, cm), new RCPosition(5, 3));
		cp.add(new NumberButton(1, cm), new RCPosition(4, 3));
		cp.add(new NumberButton(2, cm), new RCPosition(4, 4));
		cp.add(new NumberButton(3, cm), new RCPosition(4, 5));
		cp.add(new NumberButton(4, cm), new RCPosition(3, 3));
		cp.add(new NumberButton(5, cm), new RCPosition(3, 4));
		cp.add(new NumberButton(6, cm), new RCPosition(3, 5));
		cp.add(new NumberButton(7, cm), new RCPosition(2, 3));
		cp.add(new NumberButton(8, cm), new RCPosition(2, 4));
		cp.add(new NumberButton(9, cm), new RCPosition(2, 5));
		
		JButton swap = new JButton("+/-");
		swap.setBackground(new Color(190,210,250));
		swap.addActionListener(e->{
			try {
				cm.swapSign();
			} catch (CalculatorInputException ex) {
				JOptionPane.showMessageDialog(this, ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
			}
		});
		JButton dot = new JButton(".");
		dot.setBackground(new Color(190,210,250));
		dot.addActionListener(e->{
			try {
				cm.insertDecimalPoint();
			} catch (CalculatorInputException ex) {
				JOptionPane.showMessageDialog(this, ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
			}
		});
		
		cp.add(swap, new RCPosition(5, 4));
		cp.add(dot, new RCPosition(5, 5));
	}

	/**
	 * Creates and adds buttons that represent operations related
	 * to the calculator itself to the given {@link Container}:
	 * push, pop, clear, reset.
	 * 
	 * @param cp the {@link Container} to which to add the buttons
	 */
	private void setUpMetaButtons(Container cp) {
		JButton push = new JButton("push");
		push.setBackground(new Color(190,210,250));
		push.addActionListener(e->{
			stack.push(cm.getValue());
		});
		
		JButton pop = new JButton("pop");
		pop.setBackground(new Color(190,210,250));
		pop.addActionListener(e->{
			if(stack.isEmpty()) {
				JOptionPane.showMessageDialog(this, "The stack is empty!", "Error",
						JOptionPane.ERROR_MESSAGE);
			}
			cm.setValue(stack.pop());
		});
		
		JButton clear = new JButton("clr");
		clear.setBackground(new Color(190,210,250));
		clear.addActionListener(e->{
			cm.clear();
		});
		
		JButton reset = new JButton("res");
		reset.setBackground(new Color(190,210,250));
		reset.addActionListener(e->{
			cm.clearAll();
		});

		cp.add(push, new RCPosition(3, 7));
		cp.add(pop, new RCPosition(4, 7));
		cp.add(clear, new RCPosition(1,7));
		cp.add(reset, new RCPosition(2, 7));
	}

	/**
	 * Creates and adds all operation buttons to the given 
	 * {@link Container}, adding all invertable operation
	 * buttons into the internal list of such buttons.
	 * 
	 * @param cp the {@link Container} to which to add the buttons
	 * @param invert the {@link JCheckBox} forwarded to each button
	 */
	private void setUpOperatorButtons(Container cp, JCheckBox invert) {
		OperatorButton add = new OperatorButton("+", ADD, cm, ADD, invert);
		OperatorButton sub = new OperatorButton("-", SUB, cm, SUB, invert);
		OperatorButton mul = new OperatorButton("*", MUL, cm, MUL, invert);
		OperatorButton div = new OperatorButton("/", DIV, cm, DIV, invert);
		OperatorButton pow = new OperatorButton("x^n", POW, cm, ROOT, invert);
	
		UnaryOpButton sin = new UnaryOpButton("sin", "arcsin", SIN, 
				ASIN, cm);
		UnaryOpButton cos = new UnaryOpButton("cos", "arccos", COS, 
				ACOS, cm);
		UnaryOpButton tan = new UnaryOpButton("tan", "arctan", TAN, 
				ATAN, cm);
		UnaryOpButton ctg = new UnaryOpButton("ctg", "arcctg", CTG, 
				ACTG, cm);
		UnaryOpButton log = new UnaryOpButton("log", "10^x", LOG, 
				TEN_POW, cm);
		UnaryOpButton ln = new UnaryOpButton("ln", "e^x", LN, 
				E_POW, cm);
		UnaryOpButton rec = new UnaryOpButton("1/x", "asin", RECIPROCATE, 
				RECIPROCATE, cm);
		
		invertable = Arrays.asList(sin,cos,tan,ctg,log,ln,pow);
		
		cp.add(add, new RCPosition(5, 6));
		cp.add(sub, new RCPosition(4, 6));
		cp.add(mul, new RCPosition(3, 6));
		cp.add(div, new RCPosition(2, 6));
		cp.add(pow, new RCPosition(5, 1));
		
		cp.add(rec, new RCPosition(2, 1));
		cp.add(log, new RCPosition(3, 1));
		cp.add(ln, new RCPosition(4, 1));
		cp.add(sin, new RCPosition(2, 2));
		cp.add(cos, new RCPosition(3, 2));
		cp.add(tan, new RCPosition(4, 2));
		cp.add(ctg, new RCPosition(5, 2));
	}

	/**
	 * The entry point of the program.
	 * 
	 * @param args command-line arguments - not used
	 */
	public static void main(String[] args) {
		SwingUtilities.invokeLater(()->{
			new Calculator().setVisible(true);
		});
	}
}
