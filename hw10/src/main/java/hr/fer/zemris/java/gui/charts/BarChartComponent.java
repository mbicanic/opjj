package hr.fer.zemris.java.gui.charts;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.util.List;

import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

/**
 * BarChartComponent is a component that displays a bar chart,
 * given to it through the constructor as a {@link BarChart}
 * object.
 * 
 * The BarChartComponent is comprised of several elements:
 * <ul> 
 * <li> The y-axis, displaying values from {@link #chart},
 * 		going from {@link BarChart#getyMin()} to 
 * 		{@link BarChart#getyMax()}, with a step of the size
 * 		{@link BarChart#getyStep()}.
 * <li> The label (name) of the y-axis, displayed as a text along 
 * 		the outer edge of the y-axis
 * <li> The x-axis, displaying all the x components of the
 * 		{@link XYValue} objects from {@link BarChart#getValues()},
 * 		setting the y-values to 0 for potential missing
 * 		x-values.<br>
 * 		For example, if the values are {@code (1,3), (2,4), (4,1)},
 * 		the value for x=3 is missing, which will have the same effect
 * 		as if the values were {@code (1,3), (2,4), (3,0), (4,1)}.
 * 		The x values are centered with respect to the column representing
 * 		that x value.
 * <li> The label (name) of the x-axis, displayed as a text along
 * 		the outer edge of the x axis.
 * <li> The actual bar chart values, displayed as orange rectangles
 * 		of appropriate size, as determined by the {@link XYValue}
 * 		objects.
 * </ul>
 * 
 * The distance from the y-axis to its label, and from that label to
 * the left edge of the component is constant.
 * Likewise, the distance from the x-axis to its label, and from that
 * label to the bottom edge of the component is constant.
 * If the component is resized, it will stretch to fill the size.
 * 
 * In case the {@link BarChart#getyMax()} - {@link BarChart#getyMin()}
 * isn't a multiplicate of {@link BarChart#getyStep()}, then the maximum
 * y value is set to be the first {@code step} multiplicate larger than
 * the maximum value as given by the {@link BarChart}.
 * 
 * Neither y nor x values can be negative.
 * 
 * @author Miroslav Bićanić
 */
public class BarChartComponent extends JComponent {
	private static final long serialVersionUID = 1L;
	
	/**
	 * The {@link BarChart} whose data is to be drawn on this component.
	 */
	private BarChart chart;
	
	/**
	 * A constant used for padding. It is used as the distance 
	 * from the labels to the numeric values written next to 
	 * their respective axes.
	 */
	private static final int OFFSET_A = 20;
	/**
	 * A smaller constant used for all other paddings:
	 * 	- as the distance from the numeric values to their respective axes, 
	 *  - as the gap from the last value on the axis to the end of the axis. 
	 *  - for arrow dimensions
	 *  - as the length of the dashes drawn on the axes next to the values
	 *  - as the distance from the bottom and left window border to the label
	 *  	of the x and y axis, respectively 
	 */
	private static final int OFFSET_M = 10;
	
	/** The total width of the component */
	private int W;
	/** The total height of the component */
	private int H;
	/** 
	 * The height of the words written by the Graphics object received
	 * by this BarChartComponent.
	 */
	private int ASC;
	/**
	 * The number of pixels that should be allocated for writing digits
	 * next to the y-axis, determined by the largest number in the set
	 * of values.
	 */
	private int ROOM;
	/**
	 * For faster calculations, this represents the pixel-wise X coordinate of the
	 * (0,0) point of the coordinate system stretched by this component.
	 */
	private int LEFT;
	/**
	 * For faster calculations, this represents the pixel-wise distance of the 
	 * (0,0) point from the bottom of the screen, so the pixel-wise coordinate
	 * would be H-BOTTOM.
	 */
	private int BOTTOM;
	
	/** Number of Y-values to be drawn next to the Y-axis */
	private int NOY;
	/** The pixel length between two neighboring y-values on the chart. */
	private double STEPY;
	
	/**
	 * Number of X-values to be drawn next to the X-axis. Also the number of 
	 * values in general.
	 */
	private int NOX;
	/** The pixel width of a single entry's bar in the chart. */
	private double STEPX;
	/** The smalles x-value in the list of values */
	private int XMIN;
	/** The largest x-value in te list of values */
	private int XMAX;
	
	/**
	 * Constructor for a BarChartComponent
	 * @param b the {@link BarChart} based on which the component is drawn
	 */
	public BarChartComponent(BarChart b) {
		this.chart = b;
	}
	
	/**
	 * Paints the component on the frame in which it is installed.
	 * @param g The Graphics object used for drawing
	 */
	@Override
	protected void paintComponent(Graphics g) {
		Graphics2D g2 = (Graphics2D)g;
		FontMetrics fm = g2.getFontMetrics();
		this.W = getWidth();
		this.H = getHeight();
		this.ASC = fm.getAscent();
		this.ROOM = fm.stringWidth(Integer.valueOf(chart.getyMax()).toString());
		this.LEFT = OFFSET_M + ASC + OFFSET_A + ROOM + OFFSET_M;
		this.BOTTOM = OFFSET_M + ASC + OFFSET_A + ASC + OFFSET_M;
		this.NOY = (chart.getyMax() - chart.getyMin())/chart.getyStep();
		
		List<XYValue> vals = chart.getValues();
		if(vals.size()==0) {
			return;
		}
		this.XMIN = vals.get(0).getX();
		this.XMAX = vals.get(0).getX();
		for(XYValue v : vals) {
			XMIN = v.getX() < XMIN ? v.getX() : XMIN;
			XMAX = v.getX() > XMAX ? v.getX() : XMAX;
			if(v.getX() < 1) {
				JFrame root = (JFrame)SwingUtilities.getRoot(this);
				JOptionPane.showMessageDialog(root,"Negative x values cannot be displayed!", "Error", JOptionPane.ERROR_MESSAGE);
				return;
			}
		}
		this.NOX = XMAX-XMIN+1;
		
		writeYLabel(g2, fm);
		writeYValues(g2,fm);
		writeXLabel(g2, fm);
		writeXValues(g2, fm);
		drawHistogram(g2);
	}

	/**
	 * Paints the actual bar chart on top of the already built coordinate
	 * system.
	 * 
	 * @param g2 the {@link Graphics2D} object used for drawing
	 */
	private void drawHistogram(Graphics2D g2) {
		g2.setColor(Color.ORANGE);
		List<XYValue> vals = chart.getValues();
		for(XYValue v : vals) {
			int valX = v.getX();
			int valY = v.getY();
			int step = chart.getyStep();
			
			//the first and last 2 pixels are skipped for each bar
			//so there is a visible distinction between bars
			int topLeftX = (int)(Math.round(LEFT+(valX-1)*STEPX)) + 2;
			int topLeftY = (int)(Math.round(H-BOTTOM-STEPY*valY/step));
			int height = (int)(Math.round(STEPY*valY/step));
			int width = (int)STEPX-4;
			g2.fillRect(topLeftX, topLeftY, width, height);
		}
		g2.setColor(getForeground());
	}

	/**
	 * Draws the x-axis and all the values between the minimum and
	 * maximum x-values, using the given step.
	 * 
	 * The axis is decorated with black dashes, marking each value,
	 * and the actual value represented by the dash is horizontally
	 * centered between two dashes.
	 * 
	 * Additionally, it paints grey vertical lines extended from
	 * each dash, creating a grid.
	 * 
	 * @param g2 The {@link Graphics2D} object for drawing
	 * @param fm The {@link FontMetrics} used for getting string parameters
	 * @return true if the values were drawn without error, false otheri
	 */
	private void writeXValues(Graphics2D g2, FontMetrics fm) {
		//Painting the axis
		int x1 = LEFT - OFFSET_M/2;
		int x2 = W - OFFSET_M;
		int y = H-BOTTOM;
		g2.drawLine(x1, y, x2, y);
		
		//Drawing the dashes and the corresponding values, as well as gridlines
		STEPX = (W - LEFT - OFFSET_M - 5)/NOX;
		int yPos = H - BOTTOM + OFFSET_M + ASC;
		for(int x = XMIN, i=0; x <= XMAX; x++, i++) {
			//value
			int ww = fm.stringWidth(Integer.valueOf(x).toString());
			int xPos = (int)(Math.round(LEFT + i*STEPX + STEPX/2 - ww/2));
			Font old = g2.getFont();
			g2.setFont(new Font("default", Font.BOLD, old.getSize()));
			g2.drawString(Integer.valueOf(x).toString(), xPos, yPos);
			g2.setFont(old);
			
			int dashXPos = (int)(Math.round(LEFT + (i+1)*STEPX));
			int dashYPos1 = H - BOTTOM + OFFSET_M/2;	//dash bottom
			int dashYPos2 = H - BOTTOM - OFFSET_M/2;	//dash top
			int dashYPos3 = 2*OFFSET_M;					//gridline top
			//dash
			g2.drawLine(dashXPos, dashYPos1, dashXPos, dashYPos2);
			//gridline
			g2.setColor(Color.LIGHT_GRAY);
			g2.drawLine(dashXPos, dashYPos2, dashXPos, dashYPos3);
			g2.setColor(getForeground());
		}
		
		//drawing the arrow at the end of the x-axis
		int[] arrowYPoints = new int[] {
			H-BOTTOM-OFFSET_M/2,
			H-BOTTOM,
			H-BOTTOM+OFFSET_M/2
		};
		int[] arrowXPoints = new int[] { W-OFFSET_M, W, W-OFFSET_M };
		g2.fillPolygon(arrowXPoints, arrowYPoints, 3);
	}

	/**
	 * Writes the label of the x-axis below the axis, centered with
	 * respect to the axis width.
	 * 
	 * @param g2 The {@link Graphics2D} object for drawing
	 * @param fm The {@link FontMetrics} used for getting label parameters
	 */
	private void writeXLabel(Graphics2D g2, FontMetrics fm) {
		String label = chart.getxDescription();
		int ww = fm.stringWidth(label);
		int xPos = LEFT + (W-LEFT)/2 - ww/2;
		int yPos = H-OFFSET_M;
		g2.drawString(label, xPos, yPos);
	}

	/**
	 * Draws the y-axis and all the values between the minimum and
	 * maximum y-values, using the given step.
	 * 
	 * The axis is decorated with black dashes, marking each value,
	 * and the actual value represented by the dash is vertically
	 * centered with respect to the dash. 
	 * 
	 * Additionally, it paints grey horizontal lines extended from
	 * each dash, creating a grid.
	 * 
	 * @param g2 The {@link Graphics2D} object for drawing
	 * @param fm The {@link FontMetrics} used for getting string parameters
	 */
	private void writeYValues(Graphics2D g2, FontMetrics fm) {
		g2.drawLine(LEFT, H-BOTTOM+OFFSET_M/2, LEFT, OFFSET_M);	//the axis
		
		//Drawing the dashes and the corresponding values, as well as grid lines
		int yMin = chart.getyMin();
		int yMax = chart.getyMax();
		int step = chart.getyStep();
		STEPY = (double)(H-BOTTOM-2*OFFSET_M)/NOY;
		int desc = fm.getDescent();			// used to correctly represent word height and center the values next to dashes
		for(int y = yMin, i = 0; y<=yMax; y+=step, i++) {
			//value
			int ww = fm.stringWidth(Integer.valueOf(y).toString());
			int xPos = OFFSET_M + ASC + OFFSET_A + ROOM - ww;
			int yp = (int)Math.round(H - BOTTOM + (double)(ASC-desc)/2 - i*STEPY); //the pixel-wise position on which to draw
			Font old = g2.getFont();
			g2.setFont(new Font("default", Font.BOLD, old.getSize()));
			g2.drawString(Integer.valueOf(y).toString(), xPos, yp);
			g2.setFont(old);
			//dash
			xPos = xPos + ww + OFFSET_M/2;
			yp -= Math.round((double)(ASC-desc)/2);
			g2.drawLine(xPos, yp, xPos+OFFSET_M, yp);
			//gridline
			g2.setColor(Color.LIGHT_GRAY);
			g2.drawLine(xPos+OFFSET_M, yp, W-2*OFFSET_M, yp);
			g2.setColor(getForeground());
		}
		//drawing the arrow at the top of the axis
		int[] arrowYPoints = new int[] {OFFSET_M, 0, OFFSET_M};
		int[] arrowXPoints = new int[] {
			LEFT-OFFSET_M/2,
			LEFT,
			LEFT+OFFSET_M/2
		};
		g2.fillPolygon(arrowXPoints, arrowYPoints, 3);
	}

	/**
	 * Writes the label of the y-axis next to the axis, rotated for
	 * 90 degrees counter-clockwise, centered with respect to the 
	 * axis height.
	 * 
	 * @param g2 The {@link Graphics2D} object for drawing
	 * @param fm The {@link FontMetrics} used for getting label parameters
	 */
	private void writeYLabel(Graphics2D g2, FontMetrics fm) {
		AffineTransform old = g2.getTransform();
		AffineTransform at = new AffineTransform();
		at.rotate(-Math.PI/2);
		g2.transform(at);
		int ww = fm.stringWidth(chart.getyDescription());
		
		int yPos = OFFSET_M + ASC;
		int xPos = (int)Math.round((double)(-H+BOTTOM)/2 - (double)ww/2);
		g2.drawString(chart.getyDescription(), xPos, yPos);
		
		g2.setTransform(old);
	}
}
