package hr.fer.zemris.java.gui.charts;

import java.awt.BorderLayout;
import java.awt.Container;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;

/**
 * Demonstration program for our implementation of a
 * {@link BarChart} and a {@link BarChartComponent}.
 * 
 * The program can be run from the command line with
 * one argument: a path to an existing file containing
 * the data to configure a {@link BarChart} that will
 * be used by this program.
 * 
 * The configuration file must contain at least 6 lines,
 * in the following format:
 * <pre>
 * X-axis label					// String
 * Y-axis label					// String
 * x1,y1 x2,y2 x3,y3... xn,yn	// int,int int,int...
 * minimum y value				// int
 * maximum y value				// int
 * Y-axis unit length			// int
 * </pre>
 * 
 * The configuration file may contain more lines after the
 * first 6 configuration lines. Additional lines are ignored.
 * 
 * @author Miroslav Bićanić
 */
public class BarChartDemo extends JFrame {
	private static final long serialVersionUID = 1L;
	
	/** The BarChart whose data will be displayed in the window */
	private BarChart bc;
	
	/** 
	 * Constructor for a BarChartDemo
	 * 
	 * @param bc the BarChart that is the source of data
	 */
	public BarChartDemo(BarChart bc, String path) {
		this.bc = bc;
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		setSize(500,300);
		initGUI(path);
	}
	
	/**
	 * Method for GUI initialization
	 */
	private void initGUI(String path) {
		Container cp = getContentPane();
		BarChartComponent bcp = new BarChartComponent(bc);
		cp.setLayout(new BorderLayout());
		cp.add(bcp, BorderLayout.CENTER);
		
		JLabel lbl = new JLabel(path, SwingConstants.CENTER);
		cp.add(lbl, BorderLayout.PAGE_START);
	}

	/**
	 * Entry point for the program.
	 * 
	 * @param args command-line arguments: one path to an existing configuration file
	 */
	public static void main(String[] args) {
		if(args.length!=1) {
			System.out.println("Illegal number of arguments given. Expected one path to file.");
			System.out.println("Terminating...");
			return;
		}
		try {
			Path p = Paths.get(args[0]);
			List<String> lines = Files.readAllLines(p);
			BarChart bc = createBarChart(lines);
			SwingUtilities.invokeLater(()->{
				new BarChartDemo(bc, p.toAbsolutePath().toString()).setVisible(true);
			});
		} catch (IOException e) {
			System.out.println("Error reading given file. Terminating...");
		} catch (IllegalArgumentException e) {
			System.out.println(e.getMessage());
		}
	}

	/**
	 * Creates a {@link BarChart} object using the first 6 lines
	 * from the configuration file.
	 * 
	 * @param lines The configuration lines from the configuration file
	 * @return a {@link BarChart} configured using the given lines
	 */
	private static BarChart createBarChart(List<String> lines) {
		String xDesc = lines.get(0);
		String yDesc = lines.get(1);
		int yMin = Integer.parseInt(lines.get(3));
		int yMax = Integer.parseInt(lines.get(4));
		int step = Integer.parseInt(lines.get(5));
		String[] XYStrings = lines.get(2).split("\\s+");
		List<XYValue> values = new ArrayList<>();
		for(String s : XYStrings) {
			String[] split = s.split(",");
			values.add(new XYValue(
					Integer.parseInt(split[0]),
					Integer.parseInt(split[1])));
		}
		return new BarChart(
				values, xDesc, yDesc,
				yMin, yMax, step);
	}
}
