package hr.fer.zemris.java.gui.calc.entities;

import java.awt.Color;
import java.util.function.DoubleUnaryOperator;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

import hr.fer.zemris.java.gui.calc.model.CalcModel;
import hr.fer.zemris.java.gui.calc.model.CalculatorInputException;

/**
 * UnaryOpButton is a specification of a {@link JButton}, whose action 
 * is to apply a DoubleUnaryOperator to the number extracted from the 
 * given {@link CalcModel} and set the model's value to be the result
 * of the operation.
 * 
 * Almost all UnaryOpButtons can be invertable, which is why at construction
 * they receive two {@link DoubleUnaryOperator} objects, and two textual
 * values representing the text the button must display in each state.
 * 
 * To invert the button, the {@link #invert()} method must be called.
 * 
 * @author Miroslav Bićanić
 */
public class UnaryOpButton extends JButton {
	private static final long serialVersionUID = 1L;
	/** The text to be displayed when the button is not inverted */
	private String normal;
	/** The text to be displayed when the button is inverted */
	private String inverted;
	
	/** The standard, non-inverted operation */
	private DoubleUnaryOperator std;
	/** The inverted operation */
	private DoubleUnaryOperator inv;
	/** The currently active operation */
	private DoubleUnaryOperator active;
	
	/** 
	 * A constructor for a UnaryOpButton.
	 * 
	 * @param normal the text to be displayed when the button is not inverted
	 * @param opposite the text to be displayed when the button is inverted
	 * @param op the operation to perform when the button is not inverted
	 * @param inverted the operation to perform when the button is inverted
	 * @param cm the {@link CalcModel} from and to which the value is read and written
	 */
	public UnaryOpButton(String normal, String opposite, DoubleUnaryOperator op, DoubleUnaryOperator inverted, CalcModel cm) {
		super(normal);
		setBackground(new Color(190,210,250));
		this.normal = normal;
		this.inverted = opposite;
		this.std = op;
		this.inv = inverted;
		this.active = op;
		this.addActionListener(e->{
			try {
				double res = active.applyAsDouble(cm.getValue());
				cm.setValue(res);
			} catch (UnsupportedOperationException | IllegalStateException | CalculatorInputException ex) {
				JFrame root = (JFrame) SwingUtilities.getRoot(this);
				JOptionPane.showMessageDialog(root, ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
			}
		});
	}
	
	/**
	 * Inverts the text and the operation of the button.
	 */
	public void invert() {
		setText(getText().equals(normal) ? inverted : normal);
		this.active = active==std ? inv : std;
	}
}
