package hr.fer.zemris.java.gui.charts;

import java.util.List;

/**
 * BarChart is a class modelling a bar chart (histogram).
 * 
 * The histogram contains a list of {@link XYValues}, given
 * at construction, through which an y-value is mapped to
 * each x-value existing in the list.
 * 
 * Negative y values are illegal, as well as giving a maximal
 * y value in the constructor that is smaller or equal to the 
 * given minimal value.
 * Likewise, no y-value present in the list of values can be
 * smaller than the minimal value. A value can be larger than
 * the announced maximum value.
 * 
 * @author Miroslav Bićanić
 */
public class BarChart {
	/** The list of XYValue objects representing the values in the BarChart */
	private List<XYValue> values;
	/** The semantics represented by the x-value of each XYValue pair */
	private String xDescription;
	/** The semantics represented by the y-value of each XYValue pair */
	private String yDescription;
	/** The minimum value on the y axis */
	private int yMin;
	/** The maximum value on the y axis */
	private int yMax;
	/** The distance between two neighboring y-values */
	private int yStep;
	
	/**
	 * A constructor for a BarChart.
	 * 
	 * @param values the list of values to represent in this BarChart
	 * @param xAxis the description of the x axis
	 * @param yAxis the description of the y axis
	 * @param minY the minimum value on the y axis
	 * @param maxY the maximum value on the y axis
	 * @param step the distance between neighboring values on the y axis
	 */
	public BarChart(List<XYValue> values, String xAxis, String yAxis, int minY, int maxY, int step) {
		if(minY<0 || maxY<0 || step<0) {
			throw new IllegalArgumentException("None of the y-values in the histogram can be 0.");
		}
		if(maxY<=minY) {
			throw new IllegalArgumentException("The minimal value must be smaller than the maximal value.");
		}
		this.values = values;
		this.xDescription = xAxis;
		this.yDescription = yAxis;
		this.yMin = minY;
		this.yStep = step;
		if((yMax-yMin)%step!=0) {
			int y = yMin;
			while(y < yMax) {
				y+=step;
			}
			this.yMax = y;
		} else {
			this.yMax = maxY;
		}
		for(XYValue v : values) {
			if(v.getY() < minY) {
				throw new IllegalArgumentException(String.format("The entry %s has y-value smaller than minimal value.", v));
			}
		}
	}

	/**
	 * @return the list of {@link XYValue} objects contained by this BarChart
	 */
	public List<XYValue> getValues() {
		return values;
	}
	/**
	 * @return the description of the x axis
	 */
	public String getxDescription() {
		return xDescription;
	}
	/**
	 * @return the description of the y axis
	 */
	public String getyDescription() {
		return yDescription;
	}
	/**
	 * @return the minimum y value
	 */
	public int getyMin() {
		return yMin;
	}
	/**
	 * @return the maximum y value
	 */
	public int getyMax() {
		return yMax;
	}
	/**
	 * @return the distance between two neighboring y values
	 */
	public int getyStep() {
		return yStep;
	}
}
