package hr.fer.zemris.java.gui.layouts;


import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.LayoutManager2;
import java.awt.Point;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

/**
 * CalcLayout is a layout manager implementing the LayoutManager2
 * interface.
 * 
 * CalcLayout lays out grid of 7x5 {@link Component} objects,
 * where all components will be shaped as squares of equal sizes.
 * An exception is the component at position (1,1), which takes
 * places of all components from (1,1) to (1,5), inclusive, 
 * including the potential space between the components.
 * 
 * The constraints (positions on the grid) are given by an
 * {@link RCPosition} object. The legal positions are in the
 * format (r, c) where r takes a value between 1 and 5, and m
 * takes a value between 1 and 7, both inclusively.
 * Positions (1,2), (1,3), (1,4) and (1,5) are illegal, since 
 * they will be occupied by the element at position (1,1).
 * 
 * Alternatively, the constraint can be given as a String in
 * the format "r,c". The same boundaries apply to this, too.
 * 
 * Multiple components cannot be added with the same constraint.
 * 
 * @author Miroslav Bićanić
 */
public class CalcLayout implements LayoutManager2 {
	
	/**
	 * The {@linkplain RCPosition} representing the element at
	 * position (1,1)
	 */
	private static final RCPosition TOP_LEFT = new RCPosition(1,1);
	/** The number of columns in the layout grid */
	private static final int COLUMNS = 7;
	/** The number of rows in the layout grid */
	private static final int ROWS = 5;
	
	/** Returns the maximal {@link Dimension} of a given {@link Component} */
	private static final Function<Component, Dimension> MAX = (c) -> c.getMaximumSize();
	/** Returns the minimal {@link Dimension} of a given {@link Component} */
	private static final Function<Component, Dimension> MIN = (c) -> c.getMinimumSize();
	/** Returns the preferred {@link Dimension} of a given {@link Component} */
	private static final Function<Component, Dimension> PREF = (c) -> c.getPreferredSize();
	
	/** A Map mapping {@link Component} objects to their {@link RCPosition}*/
	private Map<RCPosition, Component> layout;
	/** The gap to leave between two {@link Component} objects in the layout */
	private int gap;
	
	/**
	 * A default constructor for a CalcLayout, setting the gap size to 0.
	 */
	public CalcLayout() {
		this(0);
	}
	
	/**
	 * A constructor for a CalcLayout.
	 * 
	 * @param gapLength The length of the gap to leave between components of the layout
	 */
	public CalcLayout(int gapLength) {
		this.gap = gapLength;
		this.layout = new HashMap<RCPosition, Component>();
	}
	
	/**
	 * This layout does not use a per-component string.
	 * @throws UnsupportedOperationException if this method is called
	 */
	@Override
	public void addLayoutComponent(String name, Component comp) {
		throw new UnsupportedOperationException();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * A {@linkplain Component} present in the layout has to point to the
	 * same object in memory as the given component {@code comp} for it
	 * to be removed.
	 */
	@Override
	public void removeLayoutComponent(Component comp) {
		RCPosition goal = null;
		for(Map.Entry<RCPosition, Component> e : layout.entrySet()) {		
			if(e.getValue()==comp) {
				goal = e.getKey();
				break;
			}
		}
		if(goal!=null) {
			layout.remove(goal);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Dimension preferredLayoutSize(Container parent) {
		return containerAggregate(PREF, parent);
	}
	/**
	 * {@inheritDoc}
	 */
	@Override
	public Dimension minimumLayoutSize(Container parent) {
		return containerAggregate(MIN, parent);
	}
	/**
	 * {@inheritDoc}
	 */
	@Override
	public Dimension maximumLayoutSize(Container target) {
		return containerAggregate(MAX, target);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void layoutContainer(Container parent) {
		int[] widths = new int[COLUMNS];
		int[] heights = new int[ROWS];
		Dimension d = parent.getSize();
		int unitW = (int)Math.round((double)(d.width - 6*gap)/COLUMNS);
		int diffW = d.width - COLUMNS*unitW - 6*gap;
		
		int unitH = (int)Math.round((double)(d.height - 4*gap)/ROWS);
		int diffH = d.height - ROWS*unitH - 4*gap;
		
		for(int i = 0; i<COLUMNS; i++) {
			widths[i] = unitW;
		}
		for(int i = 0; i<ROWS; i++) {
			heights[i] = unitH;
		}
		distribute(diffW, widths);
		distribute(diffH, heights);
		
		for(Map.Entry<RCPosition, Component> e : layout.entrySet()) {
			if(e.getKey().equals(TOP_LEFT)) {
				int w = widths[0]+widths[1]+widths[2]+widths[3]+widths[4];
				w += 4*gap;
				e.getValue().setBounds(0,0,w,heights[0]);
				continue;
			}
			int r = e.getKey().getRow();
			int c = e.getKey().getColumn();
			Point p = calculatePosition(r, c, widths, heights);
			e.getValue().setBounds(p.x, p.y, widths[c-1], heights[r-1]);
		}
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public void addLayoutComponent(Component comp, Object constraints) {
		if(constraints instanceof String) {
			String s = (String) constraints;
			try {
				int r = Integer.parseInt(s.split(",")[0]);
				int c = Integer.parseInt(s.split(",")[1]);
				constraints = new RCPosition(r, c);
			} catch (NumberFormatException ex) {
				throw new UnsupportedOperationException("String constraint '"+s+"' is not formatted correctly.");
			}
		}
		if(!(constraints instanceof RCPosition)) {
			throw new UnsupportedOperationException("A constraint for CalcLayout can only be a String or an RCPosition.");
		}
		RCPosition rcp = (RCPosition) constraints;
		checkIllegal(rcp);
		
		layout.put(rcp, comp);
	}


	/**
	 * {@inheritDoc}
	 * This layout should be aligned by the origin.
	 */
	@Override
	public float getLayoutAlignmentX(Container target) {
		return 0;
	}
	/**
	 * {@inheritDoc}
	 * This layout should be aligned by the origin.
	 */
	@Override
	public float getLayoutAlignmentY(Container target) {
		return 0;
	}
	
	/**
	 * This method does not cache data about the layout.
	 */
	@Override
	public void invalidateLayout(Container target) {
	}
	
	/**
	 * Checks if the given {@link RCPosition} is legal according to
	 * the rules described by CalcLayout.
	 * 
	 * @param rcp The RCPosition to check
	 * @throws CalcLayoutException 
	 * 				- if a Component is already registered at the same position
	 * 				- if the component's indices are illegal
	 */
	private void checkIllegal(RCPosition rcp) {
		if(layout.get(rcp)!=null) {
			throw new CalcLayoutException("A component already exists at position "+rcp);
		}
		int r = rcp.getRow();
		int c = rcp.getColumn();
		if(r<1 || r>ROWS) {
			throw new CalcLayoutException("The row index must be between 1 and 5, inclusively.");
		}
		if(c<1 || c>COLUMNS) {
			throw new CalcLayoutException("The column index must be between 1 and 7, inclusively.");
		}
		if(r==1 && c>1 && c<6) {
			throw new CalcLayoutException("A component cannot be placed on position "+rcp);
		}
	}
	
	/**
	 * Iterates through all components in the given {@code container} extracting their 
	 * minimum, maximum or preferred size, depending on the given {@code extractor},
	 * returning a {@link Dimension} with its height being the largest of the heights
	 * of all the components, and its width the largest of the widths of all the 
	 * components.
	 * 
	 * @param extractor A Function extracting a Dimension from a Component
	 * @param container The Container whose components are used for calculation
	 * @return A Dimension with largest width and height found
	 */
	private Dimension componentAggregate(Function<Component, Dimension> extractor, Container container) {
		Component[] comps = container.getComponents();
		Dimension d = new Dimension(0,0);
		boolean allNull = true;
		for(Component c : comps) {
			Dimension compDimension = extractor.apply(c);
			if(compDimension==null) {
				continue;
			}
			allNull=false;
			if(c==layout.get(new RCPosition(1, 1))) {
				compDimension.width -= 4*gap;
				double tmp = (double)compDimension.width/5.0;
				compDimension.width= (int)Math.round(tmp);
			}
			if(compDimension.height > d.height) {
				d.height = compDimension.height;
			}
			if(compDimension.width > d.width) {
				d.width = compDimension.width;
			}
		}
		if(allNull) {
			return null;
		}
		return d;
	}
	
	/**
	 * Calculates the minimum, maximum or preferred size for a given {@code container},
	 * based on the largest minimum, maximum or preferred size of its components.
	 * 
	 * @param extractor A Function extracting a Dimension from a Component
	 * @param container The Container whose components are used for calculation
	 * @return A minimum, maximum or preferred Dimension of the given component
	 */
	private Dimension containerAggregate(Function<Component, Dimension> extractor, Container container) {
		Dimension d = componentAggregate(extractor, container);
		if(d==null) {
			return null;
		}
		d.height*=5;
		d.width*=7;
		d.height += 4*gap;
		d.width += 6*gap;
		return d;
	}

	/**
	 * Calculates the position of the top left corner of a component in the layout
	 * for the given {@code row} and {@code column} indices, based on the values in
	 * the {@code widths} and {@code heights} array.
	 * 
	 * @param row The row index
	 * @param column The column index
	 * @param widths An array of widths of components, in order from column 1 to 7
	 * @param heights An array of heights of components, in order from row 1 to 5
	 * @return
	 */
	private Point calculatePosition(int row, int column, int widths[], int heights[]) {
		int xCoord=0;
		for(int i = 0; i < column-1; i++) {
			xCoord+=widths[i];
			xCoord+=gap;
		}
		int yCoord=0;
		for(int i = 0; i < row-1; i++) {
			yCoord+=heights[i];
			yCoord+=gap;
		}
		return new Point(xCoord, yCoord);
	}
	
	/**
	 * When a parent container cannot be divided into 7 squares by width
	 * or 5 squares by height (accounting also for the potential gaps 
	 * between them), this method distributes the difference between the
	 * closest divisible size uniformly to some of the components' sizes.
	 * 
	 * For example, if the the window width is 200px, the closest width
	 * with which this layout can be accomplished (if there are no gaps
	 * between components) is Math.round(200/7) * 7 = 203px, which makes
	 * all components 29px wide. The 3 extra pixels will be taken away
	 * from 3 components uniformly, causing the array:
	 * {@code [29, 29, 29, 29, 29, 29, 29]}
	 * to become
	 * {@code [29, 28, 29, 28, 29, 28, 29]}
	 * 
	 * @param diff The difference between the width of the window and
	 * 				the desired width by this layout (can be negative).
	 * @param dims An array of either 5 or 7 heights or widths of components
	 * 				to which to distribute the difference
	 * @throws IllegalArgumentException if the difference is larger than the
	 * 			number of rows/columns (the total deviation in size is at most
	 * 			one pixel per component).
	 */
	private static void distribute(int diff, int[] dims) {
		if(diff==0) {
			return;
		}
		int n = Math.abs(diff);
		if(n>dims.length) {
			throw new IllegalArgumentException();
		}
		int xCrement = n/diff;
		for(int i = 0; i<n; i++) {
			int index = (i+1) * dims.length / (n+1);
			dims[index] += xCrement;
		}
	}
}
