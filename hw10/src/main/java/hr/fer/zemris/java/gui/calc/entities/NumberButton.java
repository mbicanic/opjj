package hr.fer.zemris.java.gui.calc.entities;

import java.awt.Color;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

import hr.fer.zemris.java.gui.calc.model.CalcModel;
import hr.fer.zemris.java.gui.calc.model.CalculatorInputException;

/**
 * NumberButton is a specification of a {@link JButton}, whose
 * action is to insert the digit corresponding to the button 
 * into the {@link CalcModel}.
 * 
 * @author Miroslav Bićanić
 */
public class NumberButton extends JButton {
	private static final long serialVersionUID = 1L;
	
	/**
	 * Constructor for a NumberButton.
	 * 
	 * @param digit the digit to insert
	 * @param cm the {@link CalcModel} to which to insert the digit
	 */
	public NumberButton(int digit, CalcModel cm) {
		super(Integer.valueOf(digit).toString());
		setBackground(new Color(190,210,250));
		setFont(getFont().deriveFont(30f));
		this.addActionListener(e->{
			try {
				cm.insertDigit(digit);
			} catch (UnsupportedOperationException | IllegalStateException | CalculatorInputException ex) {
				JFrame root = (JFrame) SwingUtilities.getRoot(this);
				JOptionPane.showMessageDialog(root, ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
			}
		});
	}
}
