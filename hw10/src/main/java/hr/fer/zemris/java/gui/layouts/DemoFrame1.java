package hr.fer.zemris.java.gui.layouts;

import java.awt.Color;
import java.awt.Container;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;

/**
 * Demonstration program showing our implementation of
 * a CalcLayout.
 * 
 * @author Miroslav Bićanić
 */
public class DemoFrame1 extends JFrame {
	private static final long serialVersionUID = 1L;
	
	/**
	 * Default constructor for a DemoFrame1
	 */
	public DemoFrame1() {
		setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		setSize(500,500);
		initGUI();
		//pack() //comment line setSize(500,500); if using pack
	}
	
	/**
	 * Method for GUI initialization.
	 */
	private void initGUI() {
		Container cp = getContentPane();
		cp.setLayout(new CalcLayout(3));
		cp.add(l("tekst 1"), new RCPosition(1,1));
		cp.add(l("tekst 2"), new RCPosition(2,3));
		cp.add(l("tekst stvarno najdulji"), new RCPosition(2,7));
		cp.add(l("tekst kraći"), new RCPosition(4,2));
		cp.add(l("tekst srednji"), new RCPosition(4,5));
		cp.add(l("tekst"), new RCPosition(4,7));
	}
	
	/**
	 * JLabel factory method for yellow labels, parametrized by
	 * given text.
	 * 
	 * @param text The text for the label
	 * @return A yellow JLabel with the given {@code text}
	 */
	private JLabel l(String text) {
		JLabel l = new JLabel(text);
		l.setBackground(Color.YELLOW);
		l.setOpaque(true);
		return l;
	}
	
	/**
	 * Entry point for the program.
	 * 
	 * @param args command-line arguments - not used
	 */
	public static void main(String[] args) {
		SwingUtilities.invokeLater(()->{
			new DemoFrame1().setVisible(true);
			}
		);
	}
}
