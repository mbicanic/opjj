package hr.fer.zemris.java.gui.prim;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListModel;
import javax.swing.SwingUtilities;

/**
 * PrimDemo is a program demonstrating our implementation
 * of the {@link ListModel} interface on the example of two
 * lists which display prime numbers, generating new numbers
 * and adding them to the list on demand. 
 * 
 * Both lists use the same model in the background.
 * 
 * @author Miroslav Bićanić
 */
public class PrimDemo extends JFrame {
	private static final long serialVersionUID = 1L;

	/**
	 * A constructor for a PrimDemo JFrame
	 */
	public PrimDemo() {
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		setTitle("Prime Number Generator");
		setSize(600,300);
		initGUI();
	}
	
	/**
	 * Method for GUI initialization.
	 */
	private void initGUI() {
		PrimListModel plm = new PrimListModel();
		Container cp = getContentPane();
		cp.setLayout(new BorderLayout());
		
		//creating the lists, giving them the model in the constructor
		JPanel centerPanel = new JPanel();
		centerPanel.setLayout(new GridLayout(0,2));
		JList<Integer> display1 = new JList<>(plm);
		JList<Integer> display2 = new JList<>(plm);
		centerPanel.add(new JScrollPane(display1));
		centerPanel.add(new JScrollPane(display2));
		
		JButton generate = new JButton("Sljedeći");
		generate.addActionListener(e->{
			plm.next();
		});
		
		cp.add(centerPanel, BorderLayout.CENTER);
		cp.add(generate, BorderLayout.PAGE_END);
	}
	
	/**
	 * Entry point for the program.
	 * 
	 * @param args command-line arguments - not used
	 */
	public static void main(String[] args) {
		SwingUtilities.invokeLater(()->{
			new PrimDemo().setVisible(true);
		});
	}
}
