package hr.fer.zemris.java.gui.calc.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.function.DoubleBinaryOperator;

/*
 * NOTE: I'm not using {@inheritDoc} from the CalcModel documentation
 * because it's provided in Croatian, and references the homework 
 * assignment which isn't something the user would have access to.
 */

/**
 * CalcModelImpl is an implementation of the {@link CalcModel} 
 * interface, modelling a simple calculator.
 * 
 * @author Miroslav Bićanić
 */
public class CalcModelImpl implements CalcModel {

	/** Marks if the currently stored value is editable */
	private boolean editable;
	/** 
	 * Indicates the sign of the currently stored value, where
	 * 1 means it's positive and -1 that it's negative.
	 */
	private int sign;
	/** A string representation of the currently stored value */
	private String digits;
	/** The actual value as represented by the {@code digits} string */
	private double value;
	
	/** The current active operand, used for performing binary operations */
	private Double activeOperand = null;
	/** The current pending binary operation */
	private DoubleBinaryOperator pendingOperation = null;
	
	/** A list of listeners registered to this CalcModel */
	private List<CalcValueListener> listeners;
	
	/**
	 * A constructor for a CalcModel, setting the initial value to 0
	 * and its string representation to an empty string.
	 */
	public CalcModelImpl() {
		this.editable=true;
		this.sign=1;
		this.digits = "";
		this.value=0;
		this.listeners = new ArrayList<>();
	}
	
	/**
	 * Registers a listener that should be notified whenever the value
	 * stored in the calculator changes.
	 * 
	 * @param l the listener; must not be {@code null}
	 * @throws NullPointerException if {@code l} is {@code null}
	 */
	@Override
	public void addCalcValueListener(CalcValueListener l) {
		this.listeners.add(Objects.requireNonNull(l));
	}

	/**
	 * Removes a listener from the list of listeners of this
	 * CalcModel.
	 * 
	 * @param l listener; must not be {@code null}
	 * @throws NullPointerException if {@code l} is {@code null}
	 */
	@Override
	public void removeCalcValueListener(CalcValueListener l) {
		this.listeners.remove(Objects.requireNonNull(l));
	}

	/**
	 * Returns the text that should be displayed on the calculator
	 * display, representing the currently stored value.
	 * 
	 * @return text representing the currently stored value
	 */
	@Override
	public String toString() {
		String signum = sign==1 ? "" : "-";
		if(digits.equals("")) {
			return String.format("%s0", signum);
		} 
		return String.format("%s%s", signum, digits);
	}
	/**
	 * Returns the currently stored value in the calculator.
	 * @return the currently stored value
	 */
	@Override
	public double getValue() {
		return sign*value;
	}

	/**
	 * Inserts the given {@code value} into the current value, 
	 * replacing the previously stored value.
	 * The given value can be NaN or Infinity.
	 *  
	 * After setting the value, the model becomes uneditable,
	 * meaning the value can only be used for a further operation:
	 * 	-no digits can be appended to it
	 * 	-its sign cannot be swapped
	 * 	-a decimal point cannot be added
	 * 
	 * @param value The value to store into the calculator
	 */
	@Override
	public void setValue(double value) {
		this.value = Math.abs(value);
		this.sign = value>=0 ? 1 : -1;
		if(value==Double.POSITIVE_INFINITY || value==Double.NEGATIVE_INFINITY) {
			this.digits = "Infinity";
		} else if(value==Double.NaN) {
			this.digits = "NaN";
		} else {
			this.digits = Double.valueOf(Math.abs(value)).toString();
		}
		this.editable = false;
		informAll();
	}
	
	/**
	 * Returns information whether the calculator is editable,
	 * determining if the operations {@link #swapSign()},
	 * {@link #insertDecimalPoint()} and {@link #insertDigit(int)}
	 * can be called.
	 * 
	 * @return true if the model is editable; false otherwise
	 */
	@Override
	public boolean isEditable() {
		return editable;
	}
	
	/**
	 * Clears the currently stored value and sets the model
	 * to be editable.
	 */
	@Override
	public void clear() {
		clearImpl();
		informAll();
	}

	/**
	 * Does the same as {@link #clear()}, and additionally clears
	 * the active operand and the pending operation.
	 */
	@Override
	public void clearAll() {
		clearImpl();
		this.activeOperand = null;
		this.pendingOperation = null;
		informAll();
	}

	/**
	 * Swaps the sign of the currently stored value.
	 * 
	 * @throws CalculatorInputException if the calculator is not editable
	 */
	@Override
	public void swapSign() throws CalculatorInputException {
		if(editable) {
			sign = -sign;
			informAll();
		} else {
			throw new CalculatorInputException("The displayed number is not editable.");
		}
	}

	/**
	 * Inserts a decimal point on the end of the currently stored
	 * value.
	 * 
	 * @throws CalculatorInputException if the calculator is not editable
	 */
	@Override
	public void insertDecimalPoint() throws CalculatorInputException {
		if(!editable) {
			throw new CalculatorInputException("The displayed number is not editable.");
		}
		try {
			String val = digits+".";
			this.value = Double.parseDouble(val);
			this.digits = val;
			informAll();
		} catch (NumberFormatException ex) {
			String msg = digits.contains(".") ? 
				"Cannot enter multiple decimal points." : "Number cannot start with decimal point.";
			throw new CalculatorInputException(msg);
		}
	}

	/**
	 * Inserts a digit to the rightmost position of the currently stored
	 * value.
	 * If the value is 0, adding another zero is ignored.
	 * 
	 * @param digit the digit to add to the value
	 * @throws CalculatorInputException 
	 * 				-if adding a digit makes the number too large to parse
	 * 				-if the model is not editable
	 * @throws IllegalArgumentException if the given {@code digit} is negative or bigger than 9.
	 */
	@Override
	public void insertDigit(int digit) throws CalculatorInputException, IllegalArgumentException {
		if(!editable) {
			throw new CalculatorInputException("The displayed number is not editable.");	
		} 
		if(digit < 0 || digit > 9) {
			throw new IllegalArgumentException("Given number must be a digit [0-9].");
		}
		String temp = digits;
		if(temp.equals("0")) { //using the string as the value is 0 on startup
			if(digit!=0) {
				temp="";
			} else {
				return;
			}
		}
		temp+=digit;
		value = Double.parseDouble(temp);
		if(value==Double.NEGATIVE_INFINITY || value==Double.POSITIVE_INFINITY) {
			throw new CalculatorInputException("Number can no longer be parsed as finite double.");
		}
		digits = temp;
		informAll();
		return;
	}

	/**
	 * Checks if the active operand is set.
	 * 
	 * @return true if the operand is set; false otherwise
	 */
	@Override
	public boolean isActiveOperandSet() {
		return activeOperand!=null;
	}

	/**
	 * Returns the active operand.
	 * 
	 * @return The currently active operand
	 * @throws IllegalStateException if the active operand is not set
	 */
	@Override
	public double getActiveOperand() throws IllegalStateException {
		if(activeOperand==null) {
			throw new IllegalStateException("There is no active operand stored.");
		}
		return activeOperand;
	}

	/**
	 * Sets the active operand to the given value.
	 * If the active operand was already set, this method
	 * overwrites it.
	 * 
	 * @param activeOperand the value to store as the active operand
	 */
	@Override
	public void setActiveOperand(double activeOperand) {
		this.activeOperand = activeOperand;
	}

	/**
	 * Clears the currently active operand.
	 */
	@Override
	public void clearActiveOperand() {
		this.activeOperand = null;
	}

	/**
	 * Returns the currently stored pending operation, or null
	 * if no operation is stored.
	 */
	@Override
	public DoubleBinaryOperator getPendingBinaryOperation() {
		return pendingOperation;
	}

	/**
	 * Setter for the pending operation. If a pending operation already
	 * exists, this method overwrites it.
	 * 
	 * @param op the pending operation to set; can be null
	 */
	@Override
	public void setPendingBinaryOperation(DoubleBinaryOperator op) {
		if(this.digits.equals("")) {
			throw new IllegalStateException("Cannot set an operation with no stored value.");
		}
		this.pendingOperation = op;
		this.editable=true;
	}
	
	/**
	 * Informs all listeners registered to this CalcModel
	 * that the value has changed.
	 */
	private void informAll() {
		for(CalcValueListener cvl : listeners) {
			cvl.valueChanged(this);
		}
	}
	
	/**
	 * Clears the currently stored value, and sets the model to be
	 * editable.
	 */
	private void clearImpl() {
		this.digits="";
		this.sign=1;
		this.value=0;
		this.editable = true;
	}
}
