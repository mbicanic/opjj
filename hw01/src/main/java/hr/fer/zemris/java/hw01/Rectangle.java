package hr.fer.zemris.java.hw01;

import java.util.Scanner;

/**
 * This class calculates the area and circumference of a user-defined
 * rectangle. <br>
 * 
 * The user can define the parameters directly via command line, 
 * starting the program with the two arguments (width and height)
 * like in the following example which defines a rectangle with a
 * width of 10 and a height of 50: <br>
 * 
 * <pre> 
 * Rectangle 10 50
 * </pre>
 * 
 * <p>Alternatively, you can start it with NO arguments and enter
 * the parameters during execution.<br>
 * 
 * Starting the program with any other number of arguments will output
 * a warning message and terminate the program.</p>
 * 
 * @author mirob
 *
 */
public class Rectangle {

	/**
	 * This constant represents the number of dimensions the user
	 * has to enter. In this case it's 2 - width and height.
	 */
	private static final int dimensions = 2;
	
	/**
	 * String that is used to build a prompt for the user
	 * when the width is supposed to be entered.
	 */
	private static final String lblWidth = "širinu";
	
	/**
	 * String that is used to build a prompt for the user
	 * when the height is supposed to be entered.
	 */
	private static final String lblHeight = "visinu";
	
	/**
	 * The entry point of the program. 
	 * 
	 * This method contains the control of the program, directing
	 * it into 3 possible branches based on the number of arguments
	 * provided through the command line.
	 * 
	 * @param args	arguments from the command line
	 */
	public static void main(String ... args) {
		
		if(args.length!=0 && args.length!=dimensions){
			System.out.println("Neispravan broj ulaznih argumenata. Zatvaram...");
			return;
			
		} else if(args.length == dimensions) {
			double width, height;
			try {
				width = ensurePositiveDouble(args[0]);
				height = ensurePositiveDouble(args[1]);
				printCalculations(width, height);
			} catch (IllegalArgumentException ex) {
				System.out.println(ex.getMessage());
			}
			return;
		} 
		try (Scanner sc = new Scanner(System.in)) {
			double width = getDimension(lblWidth, sc);
			double height = getDimension(lblHeight, sc);
			printCalculations(width, height);
		}
	}		

	/**
	 * This method takes input from the user, ensuring that the input
	 * can be interpreted as a non-negative double number, since 
	 * dimensions of something can only be such.
	 * 
	 * @param label - A String label - the name of the dimension we want the user to enter. 
	 * @param sc	- A Scanner opened over <code> System.in </code>.
	 * @return		- The positive real value, when such value is entered.
	 */
	private static double getDimension(String label, Scanner sc) {
		while(true) {
			System.out.printf("Unesite %s > ", label);
			String strValue = sc.next();
			try {
				double dimension = ensurePositiveDouble(strValue);
				return dimension;
			} catch (IllegalArgumentException ex) {
				System.out.println(ex.getMessage());
				continue;
			}
		}
	}
	
	/**
	 * This method checks if the given string is parsable as a positive 
	 * double number.
	 * 
	 * @param string The string to validate
	 * @return The double value of the string
	 */
	private static double ensurePositiveDouble(String string) {
		double number;
		try {
			number = Double.parseDouble(string);
		} catch (NumberFormatException ex) {
			throw new NumberFormatException("'"+string+"' se ne može protumačiti kao broj.");
		}
		if(number<0) {
			throw new IllegalArgumentException("Unijeli ste negativnu vrijednost.");
		}
		return number;
	}

	/**
	 * This method calculates and prints the area and circumference
	 * of a rectangle using the already checked arguments.
	 * 
	 * @param width	-	the width of the rectangle
	 * @param height -	the height of the rectangle
	 */
	private static void printCalculations(double width, double height) {
		double circumference = 2*width + 2*height;
		double area = width * height;
		System.out.printf("Pravokutnik širine %.1f i visine %.1f ima opseg %.1f i površinu %.1f.%n",
				width, height, circumference, area);
	}
}
