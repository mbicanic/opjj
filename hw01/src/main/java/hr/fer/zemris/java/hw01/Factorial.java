package hr.fer.zemris.java.hw01;

import java.util.Scanner;

/**
 * A class that calculates the factorial of a number that the user 
 * enters. <br>
 * 
 * Despite being possible to calculate the factorial of any given
 * positive integer or zero, the program only accepts numbers from 
 * the closed range [3, 20]. <br>
 * 
 * Typing <code> kraj </code> terminates the program.
 * 
 * @author mirob
 *
 */
public class Factorial {
	
	/**
	 * This constant represents the highest number for which the
	 * program will work correctly.
	 */
	private static final int upperLimit = 20;
	
	/**
	 * This constant represents the lowest number for which the
	 * program will work correctly.
	 */
	private static final int lowerLimit = 3;
	
	/**
	 * This string constant is the query we want the user to
	 * see when we ask him to enter a number.
	 */
	private static final String numberQuery = "Unesite broj > ";
	
	/**
	 * The entry point of the program. 
	 * 
	 * This method contains the control of the program.
	 * 
	 * @param args	arguments from the command line - not used!
	 */
	public static void main(String ... args) {
		try (Scanner sc = new Scanner(System.in)) {
			while(true) {
				String userInput = getInput(sc, numberQuery);
				
				if(userInput.equals("kraj")) {
					break;
				}
				int nextInt = validateAndParse(userInput);
				if(nextInt<0) {
					continue;
				}
				System.out.printf("%d! = %d%n", nextInt, getFactorial(nextInt));
			}
		}
		System.out.println("Doviđenja.");
	}

	/**
	 * This method calculates the factorial of any integer from the closed
	 * range [0,20], throwing an exception if given an argument out of range.
	 * 
	 * @param n		the number for which we are calculating the factorial
	 * @return		the value of n! as a long integer
	 * @throws IllegalArgumentException	if the given parameter n is negative.
	 */
	public static long getFactorial(int n) throws IllegalArgumentException {
		if (n<0 || n>20) {
			throw new IllegalArgumentException("Uneseni broj mora biti u rasponu [0,20].");
		}
		if (n==0 || n==1) {
			return 1;
		}
		
		long result = 2L;
		for(int i = 3; i <= n; i++) {
			result *= i;
		}
		
		return result;
	}
	
	/**
	 * This method gets the next token from the given Scanner.
	 * 
	 * Meant to be used with a Scanner opened over System.in,
	 * so that the query message can be shown to the user.
	 * 
	 * @param sc The Scanner object opened over a stream
	 * @param query The message to output every time the method is called
	 * @return The user's input
	 */
	private static String getInput(Scanner sc, String query) {
		System.out.printf(query);
		return sc.next().trim().toLowerCase();		
	}
	
	/**
	 * This method validates and parses an input string into an
	 * integer.
	 * 
	 * If the input string isn't parsable as an integer, or is
	 * but isn't an integer from within the specified range,
	 * this method returns -1.
	 * 
	 * @param input The input string
	 * @return the integer value represented by the input string, -1 if invalid
	 * @throws NumberFormatException if the input string isn't parsable 
	 * as integer
	 */
	private static int validateAndParse(String input) {
		try {
			int n = Integer.parseInt(input);
			if(n<lowerLimit || n>upperLimit) {
				System.out.println("'"+n+"' nije broj u dozvoljenom rasponu.");
				return -1;
			}
			return n;
		} catch (NumberFormatException ex) {
			System.out.println("'"+input+"' nije cijeli broj.");
			return -1;
		}
	}
}
