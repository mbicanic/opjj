package hr.fer.zemris.java.hw01;

import java.util.Scanner;

/**
 * This program adds distinct integers the user enters into a binary tree,
 * and then prints it to standard output.
 * <br>
 * This binary tree allows adding only distinct elements - there are no
 * duplicates. The user enters the elements until he types in <code> 
 * kraj </code>, which terminates the program.
 * 
 * @author mirob
 *
 */
public class UniqueNumbers {
	
	/**
	 * This string constant is the query we want the user to
	 * see when we ask him to enter a number.
	 */
	private static final String numberQuery = "Unesite broj > ";
	
	/**
	 * The second part of the error message that's concatenated to
	 * the value that the user entered, in case the number can't be
	 * interpreted as a whole number.
	 */
	private static final String errorNaN = " nije cijeli broj.";
	
	/**
	 * The second part of the error message that's concatenated to
	 * the value that the user entered, in case the tree already
	 * contains the entered number.
	 */
	private static final String errorContains = " već postoji. Preskačem.";
	
	/**
	 * TreeNode is a simple data structure representing each node
	 * in the binary tree.
	 * Every node has an integer value, and two child nodes.
	 * <br>
	 * Keep in mind that creating a new instance of TreeNode with
	 * the <code> new </code> operator, you set the <code> value
	 * </code> to 0. An empty tree is instead represented with a 
	 * null reference to a TreeNode.
	 * 
	 * @author mirob
	 *
	 */
	public static class TreeNode {
		int value;
		TreeNode left = null;
		TreeNode right = null;
	}

	/**
	 * The entry point of the program. 
	 * 
	 * This method contains the control of the program.
	 * 
	 * @param args	arguments from the command line - not used!
	 */
	public static void main(String ... args) {
		
		TreeNode root = null;
		
		try (Scanner sc = new Scanner(System.in)) {
			
			while(true) {		
				String userInput = getInput(sc, numberQuery);
				if(userInput.equals("kraj")) {
					break;
				}
				
				try {
					int value = Integer.parseInt(userInput);
					if(containsValue(root, value)) {
						errorMessage(userInput, errorContains);
						continue;
					}
					root = addNode(root, value);
					System.out.println("Dodano.");
				} catch (NumberFormatException ex) {
					errorMessage(userInput, errorNaN);
					continue;
				}  
			}
		}
		
		System.out.printf("Ispis od najmanjeg: ");
		printTreeAscending(root);
		System.out.println();
		
		System.out.printf("Ispis od najvećeg: ");
		printTreeDescending(root);
		System.out.println();
	}
	
	/**
	 * This method adds an element into the the binary tree. If the tree is
	 * empty (null pointer handed to function), it creates the root node.
	 * <br>
	 * Adding a value already in the tree will not result in any changes.
	 * <br>
	 * Adding a value and handing a <code> new TreeNode() </code> results
	 * in the handed value being either in the left or the right branch
	 * of the root of the tree, since a new TreeNode has a value of zero,
	 * and becomes the root.
	 * 
	 * @param root		the root of the tree
	 * @param value		the value to be added into the tree
	 * @return			the same root of the tree
	 */
	public static TreeNode addNode (TreeNode root, int value) {
		if (root==null) {
			root = new TreeNode();
			root.value = value;
			return root;
		}
		if(value < root.value) {
			root.left = addNode(root.left, value);
		} else if (value > root.value) {
			root.right = addNode(root.right, value);
		}
		return root;
	}
	
	//KOREKCIJA: SKRACEN JAVADOC
	/**
	 * This method counts the number of non-null nodes 
	 * in the tree.
	 * 
	 * @param root	the root of the tree
	 * @return		the size of the tree
	 */
	public static int treeSize(TreeNode root) {
		if(root==null) {
			return 0;
		}
		return 1 + treeSize(root.left) + treeSize(root.right);
	}
	
	/**
	 * This method says whether a certain value is or is not in a tree,
	 * recursively calling itself for every child of every node until
	 * the element is found, or the whole tree has been searched.
	 * 
	 * @param root		the root of the tree we are searching
	 * @param value		the value we are searching for
	 * @return			true (tree contains) or false (tree doesn't contain the value) 
	 */
	public static boolean containsValue(TreeNode root, int value) {
		if(root == null) {
			return false;
		}
		if(value == root.value) {
			return true;
		} else if (value > root.value) {
			return containsValue(root.right, value);
		} else if (value < root.value) {
			return containsValue(root.left, value);
		}
		return false;
	}

	/**
	 * This method prints the values of each node of the tree in 
	 * descending order.
	 * @param root The root of the tree to print
	 */
	private static void printTreeDescending(TreeNode root) {
		if(root==null) {
			return;
		}
		printTreeDescending(root.right);
		System.out.printf("%d ", root.value);
		printTreeDescending(root.left);	
	}

	/**
	 * This method prints the values of each node of the tree in 
	 * ascending order.
	 * @param root The root of the tree to print
	 */
	private static void printTreeAscending(TreeNode root) {
		if(root==null) {
			return;
		}
		printTreeAscending(root.left);
		System.out.printf("%d ", root.value);
		printTreeAscending(root.right);
	}
	
	/**
	 * This method gets the next token from the given Scanner.
	 * 
	 * Meant to be used with a Scanner opened over System.in,
	 * so that the query message can be shown to the user.
	 * 
	 * @param sc The Scanner object opened over a stream
	 * @param query The message to output every time the method is called
	 * @return The user's input
	 */
	private static String getInput(Scanner sc, String query) {
		System.out.printf(query);
		return sc.next().toLowerCase();		
	}
	
	/**
	 * This method prints one error message made by concatenating 
	 * the user's input and a message explaining what went wrong,
	 * or by just outputting an error message.
	 * 
	 * @param userInput	-	the value that the user tried to enter
	 * @param errorMsg	-	the explanation of the error
	 */
	private static void errorMessage(String userInput, String errorMsg) {
		if(errorMsg==null) {
			return;
		}
		if(userInput==null || userInput.length()==0) {
			System.out.printf("%s%n", errorMsg);
			return;
		}
		System.out.printf("%s%s%n", 
						(userInput.length() > 0) ? ("'"+userInput+"' ") : "", 
						errorMsg);
	}
}
