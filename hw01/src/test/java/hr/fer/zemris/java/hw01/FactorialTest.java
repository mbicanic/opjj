package hr.fer.zemris.java.hw01;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;

class FactorialTest {
	
	
	@Test
	void negativeNumber() {
		assertThrows(IllegalArgumentException.class,
				() -> {
					Factorial.getFactorial(-1);
				});
	}
	
	@Test
	void zero() {
		long result = Factorial.getFactorial(0);
		assertEquals(1, result);
	}
	
	@Test
	void one() {
		long result = Factorial.getFactorial(1);
		assertEquals(1, result);
	}
	
	@Test
	void ten() {
		long result = Factorial.getFactorial(10);
		assertEquals(3628800, result);
	}
	
	@Test
	void twenty() {
		long result = Factorial.getFactorial(20);
		assertEquals(2432902008176640000L, result);
	}
	
	@Test
	void twentyOne() {
		assertThrows(IllegalArgumentException.class, 
				() -> {
					Factorial.getFactorial(21);
				});
	}
	
}
