package hr.fer.zemris.java.hw01;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static hr.fer.zemris.java.hw01.UniqueNumbers.*;

class UniqueNumbersTest {
	static TreeNode root = null;
	
	/*
	 * The set up method is also a test for the methods
	 * treeSize and containsValue - if they don't work,
	 * nothing can work so we check them at set up.
	 */
	@BeforeAll
	static void setUp() {
		assertEquals(0, treeSize(root));
		root = new TreeNode();
		assertEquals(1, treeSize(root));
		root.value = 10;
		assertEquals(1, treeSize(root));
		root.left = new TreeNode();
		assertEquals(2, treeSize(root));
		root.left.value = 5;
		assertEquals(2, treeSize(root));
		root.right = new TreeNode();
		assertEquals(3, treeSize(root));
		root.right.value = 15;
		assertEquals(3, treeSize(root));
		
		assertTrue(containsValue(root, 10));
		assertTrue(containsValue(root, 5));
		assertTrue(containsValue(root, 15));
		assertFalse(containsValue(root, 91));
		assertFalse(containsValue(root, -10));
	}
	
	@Test
	void addingTheSame() {
		int size = treeSize(root);
		root = addNode(root, 5);
		assertEquals(size, treeSize(root));
	}
	
	@Test
	void addingSmaller() {
		int size = treeSize(root);
		root = addNode(root, 2);
		assertEquals(size + 1, treeSize(root));
		assertEquals(2, root.left.left.value);
	}
	
	@Test
	void addingBigger() {
		int size = treeSize(root);
		root = addNode(root, 18);
		assertEquals(size + 1, treeSize(root));
		assertEquals(18, root.right.right.value);
	}
	
	@Test
	void addInBetween() {
		int size = treeSize(root);
		root = addNode(root, 13);
		root = addNode(root, 7);
		assertEquals(size+2, treeSize(root));
		assertEquals(13, root.right.left.value);
		assertEquals(7 , root.left.right.value);
	}

	@Test
	void addReturnsItself() {
		assertEquals(root, addNode(root, 1));
	}
	
	@Test
	void addToNull() {
		TreeNode tempRoot = null;
		tempRoot = addNode(tempRoot,5);
		assertEquals(1, treeSize(addNode(tempRoot, 5)));
		assertEquals(5, tempRoot.value);
	}
	
	@Test
	void addToNewTreeNode() {
		TreeNode tempRoot = new TreeNode();
		assertEquals(tempRoot, addNode(tempRoot, 5));
		assertEquals(2, treeSize(tempRoot));
		assertEquals(true, containsValue(tempRoot, 0));
		assertEquals(true, containsValue(tempRoot, 5));
		assertEquals(false, containsValue(tempRoot, 7));
		assertEquals(5, tempRoot.right.value);
		assertEquals(0, tempRoot.value);
	}
	
	@Test
	void newTreeNodeContains() {
		assertEquals(false, containsValue(new TreeNode(), 5));
	}
	
	@Test
	void nullNodeContains() {
		assertEquals(false, containsValue(null, 5));
	}
	@Test
	void correctSizeWithAdd() {
		TreeNode tempRoot = new TreeNode();
		tempRoot = addNode(tempRoot, 10);
		tempRoot = addNode(tempRoot, 0);
		tempRoot = addNode(tempRoot, 11);
		tempRoot = addNode(tempRoot, 15);
		tempRoot = addNode(tempRoot, -1);
		tempRoot = addNode(tempRoot, 10);
		tempRoot = addNode(tempRoot, -1);
		assertEquals(5, treeSize(tempRoot));
	}
	@Test
	void testTreeSizeNullAndNew() {
		TreeNode tempRoot=null;
		assertEquals(0, treeSize(tempRoot));
		tempRoot = new TreeNode();
		assertEquals(1, treeSize(tempRoot));
	}
}
