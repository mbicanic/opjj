package hr.fer.zemris.java.zi.jvdraw.tools;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.event.MouseEvent;

import hr.fer.zemris.java.zi.jvdraw.components.IColorProvider;
import hr.fer.zemris.java.zi.jvdraw.components.JDrawingCanvas;
import hr.fer.zemris.java.zi.jvdraw.model.DrawingModel;
import hr.fer.zemris.java.zi.jvdraw.objects.Circle;

/**
 * EmptyCircleTool is a concrete implementation of an {@link AbstractTool}
 * used for drawing empty circles during the user's creation of an empty circle
 * (between the first and the second click).
 * 
 * @author Miroslav Bićanić
 */
public class EmptyCircleTool extends AbstractTool {
	private static final long serialVersionUID = 1L;

	/**
	 * Constructor for an EmptyCircleTool
	 * @param dm the underlying {@link DrawingModel}
	 * @param fg the {@link IColorProvider} providing with the foreground {@link Color}
	 * @param canvas the {@link JDrawingCanvas} on which to paint
	 */
	public EmptyCircleTool(DrawingModel dm, IColorProvider fg, JDrawingCanvas canvas) {
		super(dm, fg, canvas, "Circle", new Dimension(80, 20));
	}
	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void onFirstClick(MouseEvent e) {
		Circle c = new Circle();
		c.setRimColor(fgColor.getCurrentColor());
		c.setCenter(e.getPoint());
		c.setRadius(0);
		this.object=c;
	}
	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void onSecondClick(MouseEvent e) {
		Circle c = (Circle)this.object;
		c.setRadius(calculateRadius(c.getCenter(), e.getPoint()));
		dm.add(c);
		this.object = null;
	}
	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void onMove(MouseEvent e) {
		((Circle)this.object).setRadius(calculateRadius(((Circle)this.object).getCenter(), e.getPoint()));
		canvas.repaint();
	}

	/**
	 * Calculates the radius of the circle using the fixed {@code center} 
	 * {@link Point} and its distance to the {@code point} {@link Point},
	 * which is usually the position of the mouse.
	 * 
	 * @param center the center {@link Point} of the circle
	 * @param point any other {@link Point}
	 * @return the radius of the circle
	 */
	private int calculateRadius(Point center, Point point) {
		return (int)Math.round(Point.distance(center.getX(), center.getY(), point.getX(), point.getY()));
	}
}
