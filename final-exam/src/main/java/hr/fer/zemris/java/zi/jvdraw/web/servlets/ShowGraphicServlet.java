package hr.fer.zemris.java.zi.jvdraw.web.servlets;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import hr.fer.zemris.java.zi.jvdraw.JVDLoader;
import hr.fer.zemris.java.zi.jvdraw.objects.Circle;
import hr.fer.zemris.java.zi.jvdraw.objects.FilledCircle;
import hr.fer.zemris.java.zi.jvdraw.objects.FilledTriangle;
import hr.fer.zemris.java.zi.jvdraw.objects.GeometricalObject;
import hr.fer.zemris.java.zi.jvdraw.objects.Line;

public class ShowGraphicServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String name = req.getParameter("id");
		String path = req.getServletContext().getRealPath("/WEB-INF/images/"+name);
		Path p = Paths.get(path);
		if(!Files.exists(p)) {
			req.setAttribute("error", "Image does not exist");
			req.getRequestDispatcher("/WEB-INF/error.jsp").forward(req, resp);
			return;
		}
		JVDLoader jvdl;
		try {
			jvdl = new JVDLoader(p);
		} catch(Exception ex) {
			req.setAttribute("error", ex.getMessage());
			req.getRequestDispatcher("/WEB-INF/error.jsp").forward(req, resp);
			return;
		}
		String picName = name;
		Integer noLine = 0;
		Integer noCircle = 0;
		Integer noFCircle = 0;
		Integer noTriangle = 0;
		
		for(GeometricalObject go : jvdl.getObjects()) {
			if(go instanceof Line) {
				noLine++;
			} else if(go instanceof Circle) {
				noCircle++;
			} else if(go instanceof FilledCircle) {
				noFCircle++;
			} else if(go instanceof FilledTriangle) {
				noTriangle++;
			}
		}
		
		req.setAttribute("pname", picName);
		req.setAttribute("noline", noLine);
		req.setAttribute("nocircle", noCircle);
		req.setAttribute("nofcircle", noFCircle);
		req.setAttribute("notriangle", noTriangle);
		req.getRequestDispatcher("/WEB-INF/singleImg.jsp").forward(req, resp);
	}
}
