package hr.fer.zemris.java.zi.jvdraw.components;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JColorChooser;
import javax.swing.JComponent;

/**
 * JColorArea is a {@link JComponent} implementing the {@link IColorProvider}
 * interface, providing the user with a button-like component through which 
 * the user can pick a {@link Color}.
 * 
 * @author Miroslav Bićanić
 */
public class JColorArea extends JComponent implements IColorProvider {
	private static final long serialVersionUID = 1L;

	/**
	 * A {@link List} of {@link ColorChangeListener} objects registered
	 * to this {@link JColorArea}
	 */
	private List<ColorChangeListener> colorListeners = new ArrayList<>();
	
	/**
	 * The currently selected {@link Color}
	 */
	private Color selectedColor;
	
	/**
	 * A constructor for a JColorArea
	 * @param initial the initial {@link Color} for the JColorArea
	 */
	public JColorArea(Color initial) {
		this.selectedColor = initial;
		this.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				openDialogue();
			}
		});
	}
	
	/**
	 * Opens a {@link JColorChooser} dialog where the user can choose
	 * a new {@link Color}.
	 */
	private void openDialogue() {
		Color old = selectedColor;
		Color newColor = JColorChooser.showDialog(this, "Choose color", selectedColor);
		if(newColor==null) {
			return;
		}
		if(!old.equals(newColor)) {
			selectedColor = newColor;
			repaint();
			notifyColorChanged(old);
		}
	}

	/**
	 * All JColorArea objects have a preferred size of 15x15 pixels
	 */
	@Override
	public Dimension getPreferredSize() {
		return new Dimension(15,15);
	}
	
	/**
	 * The component has a black outline around an area colored
	 * in this JColorArea's current {@link #selectedColor}
	 */
	@Override
	protected void paintComponent(Graphics g) {
		Graphics2D g2d = (Graphics2D)g;
		g2d.setColor(selectedColor);
		g2d.fillRect(0, 0, this.getWidth(), this.getHeight());
		g2d.setColor(Color.BLACK);
		g2d.drawRect(0, 0, this.getWidth(), this.getHeight());
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public Color getCurrentColor() {
		return selectedColor;
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public void addColorChangeListener(ColorChangeListener l) {
		this.colorListeners.add(l);
	}
	/**
	 * {@inheritDoc}
	 */
	@Override
	public void removeColorChangeListener(ColorChangeListener l) {
		this.colorListeners.remove(l);
	}
	
	/**
	 * Notifies every registered {@link ColorChangeListener} that the {@link Color} 
	 * has been changed from {@code old} to {@link #selectedColor}.
	 * @param old the previous {@link Color}
	 */
	private void notifyColorChanged(Color old) {
		for(ColorChangeListener ccl : colorListeners) {
			ccl.newColorSelected(this, old, selectedColor);
		}
	}
}
