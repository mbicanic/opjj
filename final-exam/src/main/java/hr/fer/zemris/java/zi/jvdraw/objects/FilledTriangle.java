package hr.fer.zemris.java.zi.jvdraw.objects;

import java.awt.Color;
import java.awt.Point;

import javax.swing.JLabel;

import hr.fer.zemris.java.zi.jvdraw.components.JColorArea;
import hr.fer.zemris.java.zi.jvdraw.objects.visitor.GeometricalObjectVisitor;

public class FilledTriangle extends GeometricalObject {
	private Point a;
	private Point b;
	private Point c;
	private Color rimColor;
	private Color inColor;
	
	public FilledTriangle() {
		super();
	}
	
	public FilledTriangle(Point a, Point b, Point c, Color out, Color in) {
		this.a = a;
		this.b = b;
		this.c = c;
		this.inColor = in;
		this.rimColor = out;
	}
	
	@Override
	public String toString() {
		return "Filled triangle "+formatPoint(a)+"-"+formatPoint(b)+"-"+formatPoint(c)
			+leadingZeroes(Integer.toHexString(inColor.getRed())) +
			leadingZeroes(Integer.toHexString(inColor.getGreen())) +
			leadingZeroes(Integer.toHexString(inColor.getBlue()));
	}
	
	/**
	 * Adds leading zeroes to the received {@code hexString} until
	 * the length of the hexString is equal to 2.
	 * @param hexString the string to which to add leading zeroes
	 * @return a hexString with length equal to 2, and value equal
	 * 			to the hexString received as parameter
	 */
	private String leadingZeroes(String hexString) {
		return "0".repeat(2 - hexString.length()) + hexString;
	}
	
	
	public Point getA() {
		return a;
	}

	public void setA(Point a) {
		this.a = a;
	}

	public Point getB() {
		return b;
	}

	public void setB(Point b) {
		this.b = b;
	}

	public Point getC() {
		return c;
	}

	public void setC(Point c) {
		this.c = c;
	}

	public Color getRimColor() {
		return rimColor;
	}

	public void setRimColor(Color rimColor) {
		this.rimColor = rimColor;
	}

	public Color getInColor() {
		return inColor;
	}

	public void setInColor(Color inColor) {
		this.inColor = inColor;
	}

	@Override
	public void accept(GeometricalObjectVisitor v) {
		v.visit(this);
	}

	@Override
	public GeometricalObjectEditor createGeometricalObjectEditor() {
		return new FilledTriangleEditor(this);
	}
	
	private static class FilledTriangleEditor extends GeometricalObjectEditor {
		private static final long serialVersionUID = 1L;
		private FilledTriangle t;
		private JColorArea in;
		private JColorArea out;
		
		
		public FilledTriangleEditor(FilledTriangle filledTriangle) {
			this.t = filledTriangle;
			in = new JColorArea(t.inColor);
			out = new JColorArea(t.rimColor);
			initPanelGUI();
		}
		
		/**
		 * Initializes the GUI components of the LineEditor.
		 */
		private void initPanelGUI() {
			add(new JLabel("Rim color:"));
			add(out);
			add(new JLabel("Fill color:"));
			add(in);
		}

		@Override
		public void checkEditing() {
			if(in.getCurrentColor()==null || out.getCurrentColor()==null) {
				throw new IllegalArgumentException("Color cannot be null");
			}
		}

		@Override
		public void acceptEditing() {
			t.setInColor(in.getCurrentColor());
			t.setRimColor(out.getCurrentColor());
			t.notifyChanged();
		}
	}
}
