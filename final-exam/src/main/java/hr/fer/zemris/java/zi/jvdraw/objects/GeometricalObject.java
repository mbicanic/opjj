package hr.fer.zemris.java.zi.jvdraw.objects;

import java.awt.Point;
import java.util.ArrayList;
import java.util.List;

import hr.fer.zemris.java.zi.jvdraw.objects.visitor.GeometricalObjectVisitor;

/**
 * GeometricalObject is an abstract class modelling any geometrical
 * object.
 * 
 * This class defines several things for every concrete implementation
 * of a geometrical object: <ul>
 * <li> GeometricalObject allows registration and deregistration of listeners,
 * 		meaning its changes can be listened to by a {@link GeometricalObjectListener} </li>
 * <li> GeometricalObject is a part of the Visitor design pattern, so each object
 * 		must contain an {@link #accept(GeometricalObjectVisitor)} method </li>
 * <li> Each concrete GeometricalObject must know how to create a 
 * 		{@link GeometricalObjectEditor} <li></ul>
 * 
 * @author Miroslav Bićanić
 */
public abstract class GeometricalObject {
	/** The list of registered {@link GeometricalObjectListener} listeners */
	protected List<GeometricalObjectListener> listeners = new ArrayList<>();
	
	/**
	 * Registers a {@link GeometricalObjectListener} to this object.
	 * @param l the {@link GeometricalObjectListener} to register
	 */
	public void addGeometricalObjectListener(GeometricalObjectListener l) {
		if(!this.listeners.contains(l)) {
			this.listeners.add(l);
		}
	}
	/**
	 * Removes a {@link GeometricalObjectListener} from this object.
	 * @param l the {@link GeometricalObjectListener} to remove
	 */
	public void removeGeometricalObjectListener(GeometricalObjectListener l) {
		this.listeners.remove(l);
	}
	/**
	 * Notifies all listeners of a change in the object.
	 */
	protected void notifyChanged() {
		for(GeometricalObjectListener l : listeners) {
			l.geometricalObjectChanged(this);
		}
	}
	
	/**
	 * Accepts the given {@link GeometricalObjectVisitor}, calling
	 * its appropriate method with this object as a parameter.
	 * @param v the {@link GeometricalObjectVisitor} to accept
	 */
	public abstract void accept(GeometricalObjectVisitor v);
	
	/**
	 * Creates a {@link GeometricalObjectEditor} for this 
	 * GeometricalObject.
	 * @return a GeometricalObjectEditor
	 */
	public abstract GeometricalObjectEditor createGeometricalObjectEditor();
	
	/**
	 * Creates a string representation of the given {@link Point}
	 * in the format <code> (x,y) </code>.
	 * @param point the {@link Point} to format
	 * @return a string representation of the {@link Point}
	 */
	protected String formatPoint(Point point) {
		return "("+point.x+","+point.y+")";
	}
}
