package hr.fer.zemris.java.zi.jvdraw.tools;

import java.awt.Graphics2D;
import java.awt.event.MouseEvent;

import hr.fer.zemris.java.zi.jvdraw.objects.GeometricalObject;

/**
 * Tool is an interface for objects that know how to adequately 
 * react to various mouse events during the creation of a 
 * {@link GeometricalObject} by painting a temporary version of
 * the object given by its temporary parameters.
 * 
 * @author Miroslav Bićanić
 */
public interface Tool {
	/**
	 * Method invoked whenever a mouse button has been pressed (but not
	 * necessarily released)
	 * @param e a {@link MouseEvent} describing the event
	 */
	void mousePressed(MouseEvent e);
	/**
	 * Method invoked whenever a mouse button has been released.
	 * @param e a {@link MouseEvent} describing the event
	 */
	void mouseReleased(MouseEvent e);
	/**
	 * Method invoked whenever a mouse button has been clicked 
	 * (pressed and released in succession)
	 * @param e a {@link MouseEvent} describing the event
	 */
	void mouseClicked(MouseEvent e);
	/**
	 * Method invoked whenever the mouse pointer has been moved.
	 * @param e a {@link MouseEvent} describing the event
	 */
	void mouseMoved(MouseEvent e);
	/**
	 * Method invoked whenever the mouse pointer has been dragged
	 * (moved while the mouse is pressed).
	 * @param e a {@link MouseEvent} describing the event
	 */
	void mouseDragged(MouseEvent e);
	/**
	 * Paints the object using the received {@link Graphics2D}
	 * @param g2d a {@link Graphics2D} object used to paint
	 */
	void paint(Graphics2D g2d);
}
