package hr.fer.zemris.java.zi.jvdraw.model;

import hr.fer.zemris.java.zi.jvdraw.objects.GeometricalObject;

/**
 * DrawingModel is an interface representing a model of an image 
 * containing {@link GeometricalObject}s which can be removed, added,
 * or their order can be changed.<br>
 * <br>
 * DrawingModel is a subject in the observer pattern, meaning it
 * provides with registration and deregistration methods for 
 * {@link DrawingModelListener} objects interested in changes in
 * the model.
 * 
 * @author Miroslav Bićanić
 *
 */
public interface DrawingModel {
	/**
	 * Returns the number of {@link GeometricalObject}s present in the model
	 * @return the number of objects present in the model
	 */
	int getSize();
	/**
	 * Returns the {@link GeometricalObject} at position {@code index} in 
	 * the model.
	 * @param index the index of the {@link GeometricalObject} to return
	 * @return the {@link GeometricalObject} at the given {@code index}
	 */
	GeometricalObject getObject(int index);
	/**
	 * Returns the index of the given {@link GeometricalObject} in the
	 * model.
	 * @param object the {@link GeometricalObject} whose index to return
	 * @return the index of the {@link GeometricalObject} in the model; -1 if the object doesn't exist
	 */
	int indexOf(GeometricalObject object);
	
	/**
	 * Adds the given {@link GeometricalObject} to the model
	 * @param object the {@link GeometricalObject} to add
	 */
	void add(GeometricalObject object);
	/**
	 * Removes the given {@link GeometricalObject} from the model
	 * @param object the {@link GeometricalObject} to remove
	 */
	void remove(GeometricalObject object);
	/**
	 * Changes the position of the given {@link GeometricalObject} in
	 * the model. If its old position was <code>i</code>, it's new 
	 * position is <code>i+offset</code>
	 * 
	 * @param object the {@link GeometricalObject} whose position to change
	 * @param offset number indicating to which position to move the {@link GeometricalObject}
	 */
	void changeOrder(GeometricalObject object, int offset);
	/**
	 * Removes all {@link GeometricalObject}s from the model
	 */
	void clear();

	/**
	 * Checks if the model has been modified
	 * @return true if the model is modified; false otherwise
	 */
	boolean isModified();
	/**
	 * Clers the modification flag, marking the model as unmodified.
	 */
	void clearModifiedFlag();
	
	/**
	 * Adds a {@link DrawingModelListener} to this model
	 * @param l the {@link DrawingModelListener} to add
	 */
	void addDrawingModelListener(DrawingModelListener l);
	/**
	 * Removes a {@link DrawingModelListener} from this mode
	 * @param l the {@link DrawingModelListener} to remove
	 */
	void removeDrawingModelListener(DrawingModelListener l);
}
