package hr.fer.zemris.java.zi.jvdraw.web.servlets;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.OpenOption;
import java.nio.file.Paths;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class UploadServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		UploadForm uf = new UploadForm();
		uf.fillFromRequest(req);
		uf.validate();
		
		if(uf.hasErrors()) {
			req.setAttribute("form", uf);
			req.getRequestDispatcher("/WEB-INF/index.jsp").forward(req, resp);
			return;
		}
		
		String path = req.getServletContext().getRealPath("/WEB-INF/images/"+uf.getName());
		Files.newBufferedWriter(Paths.get(path), (OpenOption[])null);
		resp.sendRedirect(resp.encodeRedirectURL(
				req.getContextPath() + "/main"));
	}
}
