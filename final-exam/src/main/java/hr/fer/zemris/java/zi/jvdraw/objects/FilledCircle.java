package hr.fer.zemris.java.zi.jvdraw.objects;

import java.awt.Color;
import java.awt.GridLayout;
import java.awt.Point;

import javax.swing.JLabel;
import javax.swing.JTextField;

import hr.fer.zemris.java.zi.jvdraw.components.JColorArea;
import hr.fer.zemris.java.zi.jvdraw.objects.visitor.GeometricalObjectVisitor;

/**
 * FilledCircle is a specification of a {@link Circle} which has
 * its inside area filled with a {@link Color}. <br>
 * <br>
 * A FilledCircle is fully defined with an empty {@link Circle}
 * (center point, radius, rim color) and a {@link Color} with 
 * which to paint the inside of the circle.
 * 
 * @author Miroslav Bićanić
 */
public class FilledCircle extends Circle {
	private Color insideColor;
	
	/**
	 * Default constructor for a FilledCircle
	 */
	public FilledCircle() {}
	/**
	 * Constructor for a filled circle
	 * @param empty a {@link Circle} defining the basic circle
	 * 			properties
	 * @param insideColor the inside {@link Color} of this FilledCircle.
	 */
	public FilledCircle(Circle empty, Color insideColor) {
		super(empty.center, empty.radius, empty.rimColor);
		this.insideColor = insideColor;
	}
	
	/**
	 * @return the inside {@link Color} of this FilledCircle
	 */
	public Color getInsideColor() {
		return insideColor;
	}
	/**
	 * @param insideColor the inside {@link Color} of this FilledCircle
	 */
	public void setInsideColor(Color insideColor) {
		this.insideColor = insideColor;
	}
	
	/**
	 * Returns a string representation of the filled circle, to be used 
	 * in the list of drawn objects.
	 */
	@Override
	public String toString() {
		return "Filled circle "+formatPoint(center)+", "+radius+", #"+
				leadingZeroes(Integer.toHexString(insideColor.getRed())) +
				leadingZeroes(Integer.toHexString(insideColor.getGreen())) +
				leadingZeroes(Integer.toHexString(insideColor.getBlue()));
	}
	
	/**
	 * Adds leading zeroes to the received {@code hexString} until
	 * the length of the hexString is equal to 2.
	 * @param hexString the string to which to add leading zeroes
	 * @return a hexString with length equal to 2, and value equal
	 * 			to the hexString received as parameter
	 */
	private String leadingZeroes(String hexString) {
		return "0".repeat(2 - hexString.length()) + hexString;
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public void accept(GeometricalObjectVisitor v) {
		v.visit(this);
	}
	/**
	 * {@inheritDoc}
	 */
	@Override
	public GeometricalObjectEditor createGeometricalObjectEditor() {
		return new FilledCircleEditor(this);
	};
	
	/**
	 * FilledCircleEditor is a specifiation of a {@link GeometricalObjectEditor},
	 * providing the user with a visual interface to change properties of a filled 
	 * {@link Circle}.
	 * 
	 * @author Miroslav Bićanić
	 */
	private static  class FilledCircleEditor extends GeometricalObjectEditor {
		private static final long serialVersionUID = 1L;
		
		/** The {@link FilledCircle} that is being edited by this EmptyCircleEditor */
		private FilledCircle circle;
		/** A {@link JTextField} to edit the center {@link Point}'s X coordinate */
		private JTextField centerX;
		/** A {@link JTextField} to edit the center {@link Point}'s Y coordinate */
		private JTextField centerY;
		/** A {@link JTextField} to edit the radius of the {@link #circle} */
		private JTextField radius;
		/** A {@link JColorArea} to edit the rim {@link Color} of the {@link #circle} */
		private JColorArea foreground;
		/** A {@link JColorArea} to edit the inside {@link Color} of the {@link #circle} */
		private JColorArea background;
		
		/**
		 * Constructor for a FilledCircleEditor
		 * @param c the {@link FilledCircle} this FilledCircleEditor will edit
		 */
		public FilledCircleEditor(FilledCircle c) {
			this.circle = c;
			setLayout(new GridLayout(5, 2));
			centerX = new JTextField();
			centerY = new JTextField();
			radius = new JTextField();
			foreground = new JColorArea(circle.getRimColor());
			background = new JColorArea(circle.getInsideColor());
			initPanelGUI();
		}
		
		/**
		 * Initializes the GUI components of the LineEditor.
		 */
		private void initPanelGUI() {
			centerX.setText(Integer.valueOf(circle.getCenter().x).toString());
			centerY.setText(Integer.valueOf(circle.getCenter().y).toString());
			radius.setText(Integer.valueOf(circle.getRadius()).toString());
			
			add(new JLabel("Center X:"));
			add(centerX);
			add(new JLabel("Center Y:"));
			add(centerY);
			add(new JLabel("Radius:"));
			add(radius);
			
			add(new JLabel("Rim color:"));
			add(foreground);
			add(new JLabel("Fill color:"));
			add(background);
		}
		
		/**
		 * {@inheritDoc}
		 */
		@Override
		public void checkEditing() {
			test(centerX.getText(), "Center X");
			test(centerY.getText(), "Center Y");
			int rad = test(radius.getText(), "Radius");
			if(rad < 0) {
				throw new IllegalArgumentException("Radius cannot be negative.");
			}
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public void acceptEditing() {
			int xC = test(centerX.getText(), "Center X");
			int yC = test(centerY.getText(), "Center Y");
			int rad = test(radius.getText(), "Radius");
			circle.setCenter(new Point(xC, yC));
			circle.setRadius(rad);
			circle.setRimColor(foreground.getCurrentColor());
			circle.setInsideColor(background.getCurrentColor());
			circle.notifyChanged();
		}
	}
}
