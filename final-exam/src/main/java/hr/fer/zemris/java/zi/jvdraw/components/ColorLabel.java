package hr.fer.zemris.java.zi.jvdraw.components;

import java.awt.Color;

import javax.swing.JLabel;

/**
 * ColorLabel is a specification of a {@link JLabel}, containing
 * text which tells what are the currently used background and
 * foreground {@link Color}s.<br>
 * <br>
 * The exact content of the text is determined dynamically by
 * listening to two {@link IColorProvider} objects which determine
 * a foreground and background {@link Color}, respectively.
 * 
 * @author Miroslav Bićanić
 */
public class ColorLabel extends JLabel {
	private static final long serialVersionUID = 1L;
	
	/**
	 * The {@link IColorProvider} responsible for the foreground color
	 */
	private IColorProvider foreground;
	/**
	 * The {@link IColorProvider} responsible for the background color
	 */
	private IColorProvider background;

	/**
	 * Constructor for a ColorLabel
	 * @param fgColorArea the {@link IColorProvider} for the foreground
	 * @param bgColorArea the {@link IColorProvider} for the background
	 */
	public ColorLabel(IColorProvider fgColorArea, IColorProvider bgColorArea) {
		this.foreground = fgColorArea;
		this.background = bgColorArea;
		foreground.addColorChangeListener((src,oldC,newC)->updateLabel());
		background.addColorChangeListener((src,oldC,newC)->updateLabel());
		updateLabel();
	}
	
	/**
	 * Updates the text of the label to reflect the current state of both
	 * {@link IColorProvider}s.
	 */
	private void updateLabel() {
		this.setText("Foreground color: "+buildRGB(foreground.getCurrentColor())
			+", background color: "+buildRGB(background.getCurrentColor())+".");
	}

	/**
	 * Builds a string containing the RGB components written in decimal
	 * base (three numbers from the range [0,255]) to be displayed as a
	 * part of the ColorLabel.
	 * 
	 * @param c {@link Color} from which to build the string
	 * @return the string representation of an RGB color
	 */
	private String buildRGB(Color c) {
		StringBuilder sb = new StringBuilder(16);
		sb.append("(").append(c.getRed()).append(", ")
			.append(c.getGreen()).append(", ").append(c.getBlue())
			.append(")");
		return sb.toString();
	}
}
