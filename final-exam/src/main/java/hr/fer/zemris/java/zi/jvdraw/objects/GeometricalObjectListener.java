package hr.fer.zemris.java.zi.jvdraw.objects;

/**
 * GeometricalObjectListener is an interface for objects that 
 * are interested to know whenever a {@link GeometricalObject}
 * changes.
 * 
 * @author Miroslav Bićanić
 */
public interface GeometricalObjectListener {
	/**
	 * Method invoked whenever a {@link GeometricalObject} changes
	 * @param o the {@link GeometricalObject} that changed
	 */
	void geometricalObjectChanged(GeometricalObject o);
}
