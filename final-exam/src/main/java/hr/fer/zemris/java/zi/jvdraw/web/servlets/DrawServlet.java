package hr.fer.zemris.java.zi.jvdraw.web.servlets;

import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import javax.imageio.ImageIO;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import hr.fer.zemris.java.zi.jvdraw.JVDLoader;
import hr.fer.zemris.java.zi.jvdraw.objects.GeometricalObject;
import hr.fer.zemris.java.zi.jvdraw.objects.visitor.GeometricalObjectBBCalculator;
import hr.fer.zemris.java.zi.jvdraw.objects.visitor.GeometricalObjectPainter;

public class DrawServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String path = req.getServletContext().getRealPath("/WEB-INF/images/" + req.getAttribute("id"));
		Path p = Paths.get(path);
		JVDLoader jvdl = new JVDLoader(p);
		
		GeometricalObjectBBCalculator gobbc = new GeometricalObjectBBCalculator();
		List<GeometricalObject> l = jvdl.getObjects();
		for(GeometricalObject go : l) {
			go.accept(gobbc);
		}
		Rectangle bbox = gobbc.getBoundingBox();
		
		BufferedImage bim = new BufferedImage(bbox.width, bbox.height, BufferedImage.TYPE_3BYTE_BGR);
		Graphics2D g2 = bim.createGraphics();
		GeometricalObjectPainter gop = new GeometricalObjectPainter(g2);
		for(GeometricalObject go : l) {
			go.accept(gop);
		}
		g2.dispose();
		
		try {
			ImageIO.write(bim, "png", resp.getOutputStream());
		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}
}
