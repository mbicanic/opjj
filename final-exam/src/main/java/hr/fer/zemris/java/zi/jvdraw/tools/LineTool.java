package hr.fer.zemris.java.zi.jvdraw.tools;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.MouseEvent;

import hr.fer.zemris.java.zi.jvdraw.components.IColorProvider;
import hr.fer.zemris.java.zi.jvdraw.components.JDrawingCanvas;
import hr.fer.zemris.java.zi.jvdraw.model.DrawingModel;
import hr.fer.zemris.java.zi.jvdraw.objects.Line;

/**
 * LineTool is a concrete implementation of an {@link AbstractTool}
 * used for drawing lines during the user's creation of a line 
 * (between the first and the second click).
 * 
 * @author Miroslav Bićanić
 */
public class LineTool extends AbstractTool {
	private static final long serialVersionUID = 1L;
	
	/**
	 * Constructor for a LineTool
	 * @param dm the underlying {@link DrawingModel}
	 * @param fg the {@link IColorProvider} providing with the foreground {@link Color}
	 * @param canvas the {@link JDrawingCanvas} on which to paint
	 */
	public LineTool(DrawingModel dm, IColorProvider fg, JDrawingCanvas canvas) {
		super(dm, fg, canvas, "Line", new Dimension(50,20));
	}
	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void onFirstClick(MouseEvent e) {
		Line l = new Line();
		l.setColor(this.fgColor.getCurrentColor());
		l.setStartPoint(e.getPoint());
		this.object = l;
	}
	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void onSecondClick(MouseEvent e) {
		Line l = (Line)this.object;
		l.setEndPoint(e.getPoint());
		dm.add(l);
		this.object=null;
	}
	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void onMove(MouseEvent e) {
		((Line)this.object).setEndPoint(e.getPoint());
		canvas.repaint();
	}
}
