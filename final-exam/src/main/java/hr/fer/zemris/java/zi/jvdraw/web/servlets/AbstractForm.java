package hr.fer.zemris.java.zi.jvdraw.web.servlets;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;

/**
 * AbstractForm is an abstract root class containing the shared
 * functionality across all Form classes in the blog application: <ol>
 * 
 * <li> Error handling - mapping errors to the name of the attribute
 * 		which caused the error, and enabling later retrieval. </li>
 * <li> Transformation of strings - forms must not contain null
 * 		references, so when reading from a {@link HttpServletRequest}
 * 		carrying the parameters, if a parameter is <code>null</code>,
 * 		it is transformed into an empty string. Leading and trailing
 * 		whitespace is also removed. </li>
 * </ul>
 * 
 * The class also demands all child classes implement the method
 * {@link #validate()} according to the rules of validation for
 * each specific form.
 * 
 * @see CommentForm
 * @see EntryForm
 * @see UserForm
 * @author Miroslav Bićanić
 */
public abstract class AbstractForm {
	
	/**
	 * A {@link Pattern} used to validate e-mail format
	 */
	public static final Pattern VALID_EMAIL = 
		    Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);

	/** 
	 * The internal map of errors, mapping error messages to the
	 * name of the attribute where the error was found.
	 */
	protected Map<String, String> errors = new HashMap<>();
	/**
	 * Retrieves the error message stored under the attribute
	 * named {@code name}
	 * @param name the name of the attribute whose error to fetch
	 * @return the error message mapped to the attribute; null if it doesn't exist
	 */
	public String fetchError(String name) {
		return errors.get(name);
	}
	/**
	 * Checks if there are any recognized errors in the submitted
	 * data.
	 * @return true if there are errors; false otherwise
	 */
	public boolean hasErrors() {
		return !errors.isEmpty();
	}
	/**
	 * Checks if there is an error mapped to the attribute named
	 * {@code name}.
	 * @param name the name of the attribute being checked
	 * @return true if there was an error with the specified attribute; false otherwise
	 */
	public boolean hasError(String name) {
		return errors.containsKey(name);
	}
	
	/**
	 * Checks if the submitted data is acceptable. If any errors
	 * are found, they are stored in the {@link #errors} map.
	 */
	public abstract void validate();
	
	/**
	 * If the given string is null, returns an empty string.
	 * Otherwise, returns a string with trailing and leading
	 * whitespace removed.
	 * 
	 * @param s the string to transform
	 * @return a string ready to be used by the CommentForm
	 */
	protected String prepare(String s) {
		if(s==null) return "";
		return s.trim();
	}
}
