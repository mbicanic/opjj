package hr.fer.zemris.java.zi.jvdraw.web.servlets;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class ListGraphicsServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String path = req.getServletContext().getRealPath("/WEB-INF/images/");
		Path p = Paths.get(path);
		if(!Files.exists(p) || !Files.isDirectory(p)) {
			Files.createDirectory(p);
		}
		List<String> result = new ArrayList<String>();
		try (Stream<Path> walk = Files.walk(Paths.get("C:\\projects"))) {

			result = walk.filter(Files::isRegularFile)
					.map(x -> x.getFileName().toString()).collect(Collectors.toList());
		} catch (IOException e) {
			e.printStackTrace();
		}
		req.setAttribute("form", new UploadForm());
		req.getServletContext().setAttribute("graphics", result);
		req.getRequestDispatcher("/WEB-INF/index.jsp").forward(req, resp);
	}
}
