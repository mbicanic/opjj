package hr.fer.zemris.java.zi.jvdraw.web.servlets;

import javax.servlet.http.HttpServletRequest;

public class UploadForm extends AbstractForm{
	private String name;
	private String definition;
	
	public UploadForm(String name, String definition) {
		super();
		this.name = name;
		this.definition = definition;
	}
	
	public UploadForm() {}
	
	/**
	 * {@inheritDoc}
	 */
	public void fillFromRequest(HttpServletRequest req) {
		this.name = prepare(req.getParameter("name"));
		this.definition = prepare(req.getParameter("def"));
	}

	@Override
	public void validate() {
		if(name.isEmpty()) {
			errors.put("name", "File name must be provided");
		}
		if(!name.substring(name.lastIndexOf('.')+1).equals("jvd")){
			errors.put("name", "File name must be in '.jvd' format.");
		}
		for(char c : name.toCharArray()) {
			if(!Character.isAlphabetic(c) && !Character.isDigit(c) && c!='.') {
				errors.put("name", "File name contains illegal character: "+c);
			}
		}
	}

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDefinition() {
		return definition;
	}
	public void setDefinition(String definition) {
		this.definition = definition;
	}
	
}
