package hr.fer.zemris.java.zi.jvdraw.objects;

import java.awt.Color;
import java.awt.GridLayout;
import java.awt.Point;

import javax.swing.JLabel;
import javax.swing.JTextField;

import hr.fer.zemris.java.zi.jvdraw.components.JColorArea;
import hr.fer.zemris.java.zi.jvdraw.objects.visitor.GeometricalObjectVisitor;

/**
 * Line is a specification of a {@link GeometricalObject} representing
 * a line in 2-dimensional space. <br>
 * <br>
 * A Line is fully defined with its color, start {@link Point} and 
 * end {@link Point}.
 * 
 * @author Miroslav Bićanić
 */
public class Line extends GeometricalObject {
	private Point startPoint;
	private Point endPoint;
	private Color color;
	
	/**
	 * Default constructor for a Line.
	 */
	public Line() {
		super();
	}
	
	/**
	 * Constructor for a Line
	 * @param startPoint the start {@link Point} of the line
	 * @param endPoint the end {@link Point} of the line
	 * @param c the {@link Color} of the line
	 */
	public Line(Point startPoint, Point endPoint, Color c) {
		this.startPoint = startPoint;
		this.endPoint = endPoint;
		this.color = c;
	}
	
	/**
	 * @return the start {@link Point} of this Line
	 */
	public Point getStartPoint() {
		return startPoint;
	}
	/**
	 * @param startPoint the start {@link Point} of this Line
	 */
	public void setStartPoint(Point startPoint) {
		this.startPoint = startPoint;
	}
	/**
	 * @return the end {@link Point} of this Line
	 */
	public Point getEndPoint() {
		return endPoint;
	}
	/**
	 * @param endPoint the end {@link Point} of this Line
	 */
	public void setEndPoint(Point endPoint) {
		this.endPoint = endPoint;
	}
	/**
	 * @return the {@link Color} of this Line
	 */
	public Color getColor() {
		return color;
	}
	/**
	 * param color the {@link Color} of this Line
	 */
	public void setColor(Color color) {
		this.color = color;
	}
	
	/**
	 * Returns a string representation of the line, to be used in
	 * the list of drawn objects.
	 */
	@Override
	public String toString() {
		return "Line "+formatPoint(startPoint)+"-"+formatPoint(endPoint);
	}
	
	/** 
	 * {@inheritDoc} 
	 */
	@Override
	public void accept(GeometricalObjectVisitor v) {
		v.visit(this);
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public GeometricalObjectEditor createGeometricalObjectEditor() {
		return new LineEditor(this);
	}

	/**
	 * LineEditor is a specifiation of a {@link GeometricalObjectEditor},
	 * providing the user with a visual interface to change properties
	 * of a Line.
	 * 
	 * @author Miroslav Bićanić
	 */
	private static class LineEditor extends GeometricalObjectEditor {
		private static final long serialVersionUID = 1L;
		
		/** The Line being edited by this LineEditor */
		private Line line;
		/** A {@link JTextField} to edit the start {@link Point}'s X coordinate */
		private JTextField startX;
		/** A {@link JTextField} to edit the start {@link Point}'s Y coordinate */
		private JTextField startY;
		/** A {@link JTextField} to edit the end {@link Point}'s X coordinate */
		private JTextField endX;
		/** A {@link JTextField} to edit the end {@link Point}'s Y coordinate */
		private JTextField endY;
		/** A {@link JColorArea} to edit the {@link Color} of the {@link #line} */
		private JColorArea foreground;
		
		/**
		 * Constructor for a LineEditor
		 * @param line the Line which this LineEditor will edit
		 */
		public LineEditor(Line line) {
			this.line = line;
			setLayout(new GridLayout(5, 2));
			startX = new JTextField();
			startY = new JTextField();
			endX = new JTextField();
			endY = new JTextField();
			foreground = new JColorArea(line.getColor());
			initPanelGUI();
		}
		
		/**
		 * Initializes the GUI components of the LineEditor.
		 */
		private void initPanelGUI() {
			startX.setText(Integer.valueOf(line.getStartPoint().x).toString());
			startY.setText(Integer.valueOf(line.getStartPoint().y).toString());
			endX.setText(Integer.valueOf(line.getEndPoint().x).toString());
			endY.setText(Integer.valueOf(line.getEndPoint().y).toString());
			
			add(new JLabel("Start X: "));
			add(startX);
			add(new JLabel("Start Y: "));
			add(startY);
			add(new JLabel("End X: "));
			add(endX);
			add(new JLabel("End Y: "));
			add(endY);
			add(new JLabel("Color: "));
			add(foreground);
			
		}
		
		/**
		 * {@inheritDoc}
		 */
		@Override
		public void checkEditing() {
			test(startX.getText(), "Start X");
			test(startY.getText(), "Start Y");
			test(endX.getText(), "End X");
			test(endY.getText(), "End Y");
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public void acceptEditing() {
			int xStart = test(startX.getText(), "Start X");
			int yStart = test(startY.getText(), "Start Y");
			int xEnd = test(endX.getText(), "End X");
			int yEnd = test(endY.getText(), "End Y");
			
			line.setStartPoint(new Point(xStart, yStart));
			line.setEndPoint(new Point(xEnd, yEnd));
			line.setColor(foreground.getCurrentColor());
			
			line.notifyChanged();
		}
	}
}
