package hr.fer.zemris.java.zi.jvdraw.model;

import javax.swing.AbstractListModel;

import hr.fer.zemris.java.zi.jvdraw.objects.GeometricalObject;

/**
 * DrawingObjectListModel is a specification of an {@link AbstractListModel}
 * over the {@link GeometricalObject} domain. 
 * 
 * @author Miroslav Bićanić
 */
public class DrawingObjectListModel extends AbstractListModel<GeometricalObject> {
	private static final long serialVersionUID = 1L;
	/**
	 * The underlying {@link DrawingModel} 
	 */
	private DrawingModel dm;

	/**
	 * A constructor for a DrawingObjectListModel
	 * @param dm the {@link DrawingModel} on which to base the list
	 */
	public DrawingObjectListModel(DrawingModel dm) {
		this.dm = dm;
		this.dm.addDrawingModelListener(new DrawingModelListener() {
			@Override
			public void objectsRemoved(DrawingModel source, int index0, int index1) {
				fireIntervalRemoved(this, index0, index1);
			}
			@Override
			public void objectsChanged(DrawingModel source, int index0, int index1) {
				fireContentsChanged(this, index0, index1);
			}
			@Override
			public void objectsAdded(DrawingModel source, int index0, int index1) {
				fireIntervalAdded(source, index0, index1);
			}
		});
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getSize() {
		return dm.getSize();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public GeometricalObject getElementAt(int index) {
		return dm.getObject(index);
	}

}
