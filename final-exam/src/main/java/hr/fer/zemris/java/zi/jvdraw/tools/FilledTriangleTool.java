package hr.fer.zemris.java.zi.jvdraw.tools;

import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.event.MouseEvent;

import javax.swing.JToggleButton;

import hr.fer.zemris.java.zi.jvdraw.components.IColorProvider;
import hr.fer.zemris.java.zi.jvdraw.components.JDrawingCanvas;
import hr.fer.zemris.java.zi.jvdraw.model.DrawingModel;
import hr.fer.zemris.java.zi.jvdraw.objects.FilledTriangle;
import hr.fer.zemris.java.zi.jvdraw.objects.GeometricalObject;
import hr.fer.zemris.java.zi.jvdraw.objects.visitor.GeometricalObjectPainter;

public class FilledTriangleTool extends JToggleButton implements Tool {
private static final long serialVersionUID = 1L;
	
	/**
	 * The underlying {@link DrawingModel} 
	 */
	private DrawingModel dm;
	/**
	 * An {@link IColorProvider} providing with the foreground color
	 */
	private IColorProvider fgColor;
	/**
	 * An {@link IColorProvider} providing with the background color
	 */
	private IColorProvider bgColor;
	/**
	 * A {@link JDrawingCanvas} on which the image is drawn
	 */
	private JDrawingCanvas canvas;
	
	/**
	 * The {@link GeometricalObject} that is being drawn
	 */
	private FilledTriangle t;
	
	/**
	 * A {@link GeometricalObjectPainter} used to paint the object in creation
	 */
	private GeometricalObjectPainter GOP = new GeometricalObjectPainter(null);
	
	
	public FilledTriangleTool(DrawingModel dm, IColorProvider foreground, IColorProvider background, 
			JDrawingCanvas canvas) {
		this.dm = dm;
		this.fgColor = foreground;
		this.bgColor = background;
		this.canvas = canvas;
		
		this.setText("Filled triangle");
		this.setPreferredSize(new Dimension(90, 20));
	}
	
	@Override
	public void mouseClicked(MouseEvent e) {
		if(t==null) {
			t = new FilledTriangle();
			t.setA(e.getPoint());
			t.setInColor(bgColor.getCurrentColor());
			t.setRimColor(fgColor.getCurrentColor());
		} else {
			if(t.getB()==null) {
				t.setB(e.getPoint());
			} else {
				t.setC(e.getPoint());
				dm.add(t);
				t=null;
			}
		}
	}

	@Override
	public void mouseMoved(MouseEvent e) {
		if(t!=null && t.getA()!=null && t.getB()!=null) {
			t.setC(e.getPoint());
			canvas.repaint();
		}
	}


	@Override
	public void paint(Graphics2D g2d) {
		if(t!=null) {
			GOP.setGraphics(g2d);
			t.accept(GOP);
		}
	}
	

	@Override
	public void mousePressed(MouseEvent e) {}
	@Override
	public void mouseDragged(MouseEvent e) {}
	@Override
	public void mouseReleased(MouseEvent e) {}


}
