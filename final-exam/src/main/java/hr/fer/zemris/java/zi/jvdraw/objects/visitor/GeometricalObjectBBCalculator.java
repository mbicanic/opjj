package hr.fer.zemris.java.zi.jvdraw.objects.visitor;

import java.awt.Rectangle;

import hr.fer.zemris.java.zi.jvdraw.objects.Circle;
import hr.fer.zemris.java.zi.jvdraw.objects.FilledCircle;
import hr.fer.zemris.java.zi.jvdraw.objects.FilledTriangle;
import hr.fer.zemris.java.zi.jvdraw.objects.GeometricalObject;
import hr.fer.zemris.java.zi.jvdraw.objects.Line;

/**
 * GeometricalObjectBBCalculator is an implementation of a {@link GeometricalObjectVisitor}
 * which calculates the bounding box of all {@link GeometricalObject}s on the canvas (minimal 
 * rectangle encompassing all objects).
 * 
 * @author Miroslav Bićanić
 */
public class GeometricalObjectBBCalculator implements GeometricalObjectVisitor {
	/**
	 * The {@link Rectangle} representing the bounding box
	 */
	private Rectangle boundingBox;
	
	/**
	 * Once the visiting is complete, the bounding box is built
	 * and this method returns it.
	 * @return the built {@link Rectangle} representing the bounding box
	 */
	public Rectangle getBoundingBox() {
		return boundingBox;
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public void visit(Line line) {
		int lineX = Math.min(line.getStartPoint().x, line.getEndPoint().x);
		int lineY = Math.min(line.getStartPoint().y, line.getEndPoint().y);
		int lineW = Math.abs(line.getStartPoint().x - line.getEndPoint().x);
		int lineH = Math.abs(line.getStartPoint().y - line.getEndPoint().y);
		
		if(boundingBox==null) {
			boundingBox = new Rectangle(lineX, lineY, lineW, lineH);
		} else {
			updateBox(lineX, lineY, lineW, lineH);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void visit(Circle circle) {
		if(boundingBox==null) {
			boundingBox = new Rectangle(circle.getCenter().x - circle.getRadius(),
										circle.getCenter().y - circle.getRadius(),
										2*circle.getRadius(),
										2*circle.getRadius());
		} else {
			updateBox(circle.getCenter().x - circle.getRadius(), 
					circle.getCenter().y - circle.getRadius(), 
					2 * circle.getRadius(), 
					2 * circle.getRadius());
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void visit(FilledCircle circle) {
		if(boundingBox==null) {
			boundingBox = new Rectangle(circle.getCenter().x - circle.getRadius(),
										circle.getCenter().y - circle.getRadius(),
										2*circle.getRadius(),
										2*circle.getRadius());
		} else {
			updateBox(circle.getCenter().x - circle.getRadius(), 
					circle.getCenter().y - circle.getRadius(), 
					2 * circle.getRadius(), 
					2 * circle.getRadius());
		}
	}

	@Override
	public void visit(FilledTriangle t) {
		int leftMost = Math.min(Math.min(t.getA().x, t.getB().x), t.getC().x);
		int upMost = Math.min(Math.min(t.getA().y, t.getB().y), t.getC().y);
		int rightMost = Math.max(Math.max(t.getA().x, t.getB().x), t.getC().x);
		int downMost = Math.max(Math.max(t.getA().y, t.getB().y), t.getC().y);
		if(boundingBox==null) {
			boundingBox = new Rectangle(leftMost, upMost, rightMost-leftMost, downMost-upMost);
		} else {
			updateBox(leftMost, upMost, rightMost-leftMost, downMost-upMost);
		}
		
	}

	/**
	 * Updates the {@link #boundingBox} using the parameters describing
	 * an object's position and dimension on the canvas.
	 * 
	 * @param objectX the leftmost coordinate of the object
	 * @param objectY the highest coordinate of the object
	 * @param objectW the width of the object
	 * @param objectH the height of the object
	 */
	private void updateBox(int objectX, int objectY, int objectW, int objectH) {
		int rectX = boundingBox.x;
		int rectY = boundingBox.y;
		int rectW = boundingBox.width;
		int rectH = boundingBox.height;
		if(objectX < rectX) {
			rectW += rectX - objectX;
			rectX = objectX;
		}
		if(objectX + objectW > rectX + rectW) {
			rectW = objectX + objectW - rectX;
		}
		if(objectY < rectY) {
			rectH += rectY - objectY;
			rectY = objectY;
		}
		if(objectY + objectH > rectY + rectH) {
			rectH = objectY + objectH - rectY;
		}
		boundingBox.setBounds(rectX, rectY, rectW, rectH);
	}

}
