<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" session="true" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
    
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>${pname}</title>
</head>
<body>
	<h1>${pname}</h1>
	<p><b>LINES:</b>${noline}</p>
	<p><b>CIRCLES:</b>${nocircle}</p>
	<p><b>FILLED CIRCLES:</b>${nofcircle}</p>
	<p><b>FILLED TRIANGLES:</b>${notriangle}</p>
	
	<img src="draw?id=${pname}">
</body>
</html>