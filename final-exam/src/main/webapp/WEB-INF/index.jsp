<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" session="true" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
    
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Početna</title>
</head>
<body>
	<h3> Graphic list: </h3>
	<ul>
	<c:forEach var="e" items="${graphics}">
		<li> <a href="img?id=${e}"> ${e} </a></li>
	</c:forEach>
	</ul>
	
	<form action="upload" method="post">
		Image name:<br>
		<input type="text" name="name" value="${form.name}"><br>
		<c:if test="${form.hasError('name')}">
			<p style="color:red"> ${formData.fetchError('name')} </p>
		</c:if>
		<textarea form="upload" name="def" rows="8" cols="50" placeholder="Enter definition..">${
			entryForm.text
			}</textarea> <br>
			<c:if test="${form.hasError('def')}">
				<p style="color:red"> ${form.fetchError('def')} </p>
			</c:if>
		
		<input type="submit" value="Upload">
	</form>
</body>
</html>